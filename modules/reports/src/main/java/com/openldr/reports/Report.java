package com.openldr.reports;


import com.openldr.reports.model.ResultRow;
import com.openldr.reports.service.ReportDataProvider;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by hassan on 4/2/17.
 */
@Setter
@Getter
public class Report {


    private String reportKey;
    private String template;
    private ReportDataProvider reportDataProvider;
    private ResultRow filterOption;


    // report properties used by report design
    private String title;
    private String subTitle;
    private String subBranchTitle;
    private String name;
    private String id;
    private String version;

}
