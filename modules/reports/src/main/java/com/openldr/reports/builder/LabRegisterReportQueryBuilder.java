package com.openldr.reports.builder;

import com.openldr.reports.model.params.LabRegisterReportParam;

import java.util.Map;

import org.apache.commons.lang.StringUtils;


public class LabRegisterReportQueryBuilder {

    public static String getQuery(Map param){

        LabRegisterReportParam filter = (LabRegisterReportParam)param.get("filterCriteria");
        // System.out.println(filter.getEndTime());
        Long userId = (Long) param.get("userId");

        String query = 
            "select patientID, date_part('year',age(dob)) age,p.gender, F.NAME facilityName, district,region,dateofSampleCollection, \n" +
            "sampleRequestDate,\n" +
//            "dateReceived, \n" +
            "(select MAX(createddate) createdDate from hiv_sample_status_changes sc where status ='APPROVED' AND sc.sampLeId =S.ID ) ANALYSISDATE,\n" +
             "(select MAX(createddate) createdDate from hiv_sample_status_changes sc where status ='DISPATCHED' AND sc.sampLeId =S.ID )  DispatchedDate,\n" +
//            "s.createddate DispatchedDate, \n"+
            "(SELECT  FIRSTNAME || ' ' || lASTNAME  FROM USERS U WHERE S.CREATEDBY = U.ID) REVIEWEDBY,\n " +
            "case WHEN unit is null THEN HR.result ELSE hr.result||' ' ||hr.unit END as result \n" +
            "from hiv_samples s\n" +
            "Join hiv_results hr ON s.id = hr.sampleId\n" +
            "join patients p on s.patientautoid = p.id \n" +
            "join facilities f on s.facilityId = F.ID\n" +
            "JOIN view_tz_geographic_zones d ON f.geographiczoneId = D.DISTRICTID\n " +
            "WHERE "+
            "labId= " + filter.getLabId();

        String status=filter.getStatus(), startTime = filter.getStartTime(),endTime=filter.getEndTime();
        
        
            if(!StringUtils.isEmpty(status)){
                query+= " and status='"+filter.getStatus() +"' ";
            }
            if(!StringUtils.isEmpty(startTime)){
                query+= " AND S.createddate >= to_date('"+filter.getStartTime()+"','DD-MM-YYYY')";

            }
            if(!StringUtils.isEmpty(endTime)){

                query+= " and S.createddate <=to_date('"+filter.getEndTime()+"','DD-MM-YYYY') ";
            }

            if(StringUtils.isEmpty(startTime) && StringUtils.isEmpty(endTime))
                query+= "  LIMIT 150";

        return query;

    }

}
