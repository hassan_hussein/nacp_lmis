package com.openldr.reports.service;

import com.openldr.email.service.EmailService;
import com.openldr.reports.mapper.FacilityHIVViralLoadMapper;
import com.openldr.reports.model.dto.TestAttachment;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;
import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * Created by hassan on 8/7/17.
 */

@Service
public class TestAttachmentService {

    @Autowired
    FacilityHIVViralLoadMapper mapper;

    @Autowired
    private EmailService emailService;



 /*
    @Autowired
    private EmailNotificationRepository repository;
*/


   //@Scheduled(fixedDelay=50000)
    private void sed() throws FileNotFoundException {

        try {
            List<TestAttachment>data=mapper.resultList();
            Resource report = new ClassPathResource("attachment_4.jasper");
            JRDataSource datasource = new JRBeanCollectionDataSource(data, true);

            Map ps = new HashMap();
            ps.put(JRParameter.REPORT_LOCALE, "ENGLISH");
            ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");
            ps.put(JRParameter.REPORT_RESOURCE_BUNDLE, resourceBundle);
            ps.put("REPORT_COUNTRY_TITLE", "TANZANIA");
            ps.put("OPERATOR_NAME","MINISTRY OF HEALTH, COMMUNITY DEVELOPMENT, GENDER, ELDERLY AND CHILDREN");
            ps.put("REPORT_SUB_TITLE","NATIONAL AIDS CONTROL PROGRAMME");
            ps.put("CUSTOM_REPORT_TITLE","2017 HIV VIRAL LOAD FOLLOW UP SUMMARY REPORT FOR COUNTRY LEVEL");
            ps.put("DS1", datasource);
           // JasperPrint jasperPrint = JasperFillManager.fillReport(report.getInputStream(), ps, new JREmptyDataSource());

            JasperPrint jasperPrint = JasperFillManager.fillReport(report.getInputStream(), ps,datasource);

          // JasperPrint jasperPrint = JasperFillManager.fillReport(report.getInputStream(),ps,datasource);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, baos);
            DataSource aAttachment =  new ByteArrayDataSource(baos.toByteArray(), "application/pdf");


            String text = "    Please find attached the summary report.\n HVL follow up alert reports sent to national coordinators to track HVL outcome and updates on the NACP  report." +
                    " Please do not share as it has sensitive information…. \n " +
                    " \n\n\n  Thanks. ";


            StringBuilder sb = new StringBuilder("");
            sb.append("Please find attached the summary report.\n");
            sb.append("\n");
            sb.append("HVL follow up alert reports sent to national coordinators to track HVL outcome and updates on the NACP  report.\n");
            sb.append("\n");
            sb.append("\n");
            sb.append("Please do not share as it has sensitive information…. \n\n\n");
            sb.append("\n");
            sb.append("Log in to (http://evlims.nacp.go.tz/public/pages/login.html)  with your Credentials, to download the detailed report in Excel format.\r\n\n");
            sb.append("\nThanks");
            sb.append("\n");
            sb.append("\n");

            sb.append("\n\n\n   HVL Support Team");

            emailService.sendMimeMessage("hhassan.developer@gmail.com", "HVL Lab Information System Report", sb.toString(), "report.pdf",aAttachment);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void sendAttachmentEmail() throws IOException {


        System.out.println("reached here");
        List<TestAttachment> reportList = mapper.getUserList();
   /*     for(TestAttachment dto: reportList){

            System.out.println(dto.getFirstName());
        }
        */
        File file = new File("resources/attachment.jasper");
        String absolutePath = file.getAbsolutePath();

        JRDataSource ds = new JRBeanCollectionDataSource(reportList);
        System.out.println(ds.toString());
        Resource report = new ClassPathResource(absolutePath);
        System.out.println(report.getInputStream());
        System.out.println(report.getFile().getAbsoluteFile().getPath());
        try {

            JRBeanCollectionDataSource jDataSource = new JRBeanCollectionDataSource(reportList,true);

// Load report
            ClassPathResource report2 = new ClassPathResource("template/attachment.jasper");
            InputStream input = report2.getInputStream();

            // Fill the report with data
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(input);
            JasperPrint print = JasperFillManager.fillReport(jasperReport, null, jDataSource);

            // Export the report to the output
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            JRExporter exporter = new JRPdfExporter();

            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, output);
            exporter.exportReport();

            System.out.println("Good year");
            System.out.println(exporter);

            JasperPrint jasperPrint = JasperFillManager.fillReport(report.getInputStream(), Collections.EMPTY_MAP, ds);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, baos);
            DataSource aAttachment =  new ByteArrayDataSource(baos.toByteArray(), "application/pdf");
            System.out.println(aAttachment);

            emailService.testSendAttachment(aAttachment);

        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}
