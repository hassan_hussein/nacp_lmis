package com.openldr.reports.model.params;

import lombok.*;

/**
 * Created by hassan on 6/5/17.
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class LabRegisterReportParam  extends BaseParam {

    private Long labId;

    private String startTime;

    private String endTime;

    private String status;

}
