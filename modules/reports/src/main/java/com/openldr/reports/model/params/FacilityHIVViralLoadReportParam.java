package com.openldr.reports.model.params;

import lombok.*;

/**
 * Created by hassan on 4/4/17.
 */

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class FacilityHIVViralLoadReportParam extends BaseParam{

    private Long district;
    private String startDate;
    private String endDate;

    private Long sampleId;

}
