package com.openldr.reports.model.report;

import com.openldr.reports.model.ResultRow;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hassan on 6/5/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LabRegisterReport implements ResultRow {

    private String patientId;

    private String gender;

    private Integer age;

    private String facilityName;

    private String district;

    private String region;

    private Date dateOfSampleCollection;

    private Date sampleRequestDate;

    // private Date dateReceived;

    private Date analysisDate;

    private Date dispatchedDate;

    private String reviewedBy;

    private String result;

    public String getDateOfSampleCollection() throws ParseException {
        return this.dateOfSampleCollection == null ? null : new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(this.dateOfSampleCollection);
    }

    public String getSampleRequestDate() throws ParseException {
        return this.sampleRequestDate == null ? null : new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(this.sampleRequestDate);
    }

    public String getAnalysisDate() throws ParseException {
        return this.analysisDate == null ? null : new SimpleDateFormat("dd-MM-yyyy").format(this.analysisDate);
    }

    public String getDispatchedDate() throws ParseException {
        return this.dispatchedDate == null ? null : new SimpleDateFormat("dd-MM-yyyy").format(this.dispatchedDate);
    }

    // public String getDateReceived() throws ParseException {
    //     return this.dateReceived == null? null: new SimpleDateFormat("dd-MM-yyyy").format(this.dateReceived);
    // }

}
