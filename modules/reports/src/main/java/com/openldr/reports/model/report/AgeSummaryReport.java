package com.openldr.reports.model.report;

import com.openldr.reports.model.ResultRow;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by hassan on 9/3/17.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AgeSummaryReport implements ResultRow {

    private String facilityName;
    private String region;
    private String district;
    private Integer femaleResultLess;
    private Integer maleResultLess;
    private Integer femaleResult;
    private Integer maleResult;

}
