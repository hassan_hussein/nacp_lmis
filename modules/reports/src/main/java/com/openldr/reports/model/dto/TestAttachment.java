package com.openldr.reports.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by hassan on 8/7/17.
 */

   @Data
   @AllArgsConstructor
   @NoArgsConstructor
   public class TestAttachment {

    private Integer testDone;
    private Integer resultGreater;
    private Integer male;
    private Integer female;
    private Integer adult;
    private Integer pediatric;
    private Integer pregnantWomen;

}
