package com.openldr.reports.model.params;

import groovy.transform.EqualsAndHashCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
public class HVLResultReportParam extends BaseParam {
	private Long sampleId;
	private String startDate;
	private String endDate;

}
