package com.openldr.reports.model.report;

import com.openldr.core.domain.BaseModel;
import com.openldr.reports.model.ResultRow;
import lombok.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hassan on 4/4/17.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FacilityHIVViralLoadResult extends BaseModel implements ResultRow {

    private String facilityCode;
    private String facilityName;
    private String district;
    private String region;
    private String patientId;
    private String sex;
    private Date dateOfBirth;
    private Integer age;
    private Date collectionDate;
    private String sampleType;
    private String labNumber;
    private String result;
    private Date reportingDate;
    private String labName;
    private String testedBy;
    private String  authorizedBy;
    private String gender;

    public String getCollectionDate() throws ParseException {
        return this.collectionDate == null ? null : new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(this.collectionDate);
    }

    public String getReportingDate() throws ParseException {
        return this.reportingDate == null ? null : new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(this.reportingDate);
    }

    public String getDateOfBirth() throws ParseException {
        return this.dateOfBirth == null ? null : new SimpleDateFormat("dd-MM-yyyy").format(this.dateOfBirth);
    }
}
