package com.openldr.reports.builder;

import java.util.Map;

import com.openldr.reports.model.params.HVLResultReportParam;

public class HVLResultQueryBuilder {
	public static String getQuery(Map<?, ?> params) {
		HVLResultReportParam filter = (HVLResultReportParam) params.get("filterCriteria");
		return
				"SELECT f.code AS facilityCode,f.name AS facilityName,district, region, patientid AS patientId, p.gender AS sex,\n" +
				"p.dob AS dateOfBirth, date_part('year', age(p.dob)) age, dateofsamplecollection collectionDate, st.name sampleType,\n" +
				"labNumber, result, unit, l.name labName, gz.region, gz.district, tt.name AS testType, s.status as status, s.datereceived as receivingDate, \n" +
				" nameofpersonrequestingtest AS requestedBy,r.testedby testedBy, r.resultdate resultDate, r.machineName as machine, r.machineSerial as serial, \n"+
				"(SELECT u.firstname || ' ' || u.lastname FROM users u INNER JOIN hiv_sample_status_changes x ON x.createdby=u.id WHERE x.sampleid=s.id AND x.status='APPROVED') AS authorisedBy,\n" +
                "(SELECT sc.createddate FROM hiv_sample_status_changes sc WHERE sc.sampleid=s.id AND sc.status='APPROVED' ORDER BY sc.id desc LIMIT 1) as authorisedDate\n "+
				" FROM  hiv_results r \n"+
				" JOIN hiv_samples s ON s.id=r.sampleid\n "+
				" LEFT JOIN patients p ON p.id=s.patientautoid\n"+
				" LEFT JOIN facilities f ON f.id=s.facilityid\n"+
				" LEFT JOIN facilities l ON l.id=s.labid\n"+
				" LEFT JOIN facilities h ON h.id=s.hubid\n"+
				" LEFT JOIN hiv_test_types tt ON s.testtypeid=tt.id\n "+
				" LEFT JOIN view_tz_geographic_zones gz ON gz.districtid=s.geographiczoneid \n"+
				" LEFT JOIN sample_types st on st.id=s.sampletypeid \n"+
				" WHERE s.status in('DISPATCHED', 'RESULTED','APPROVED') AND s.id= " + filter.getSampleId() +
				" ORDER BY s.labnumber ASC";
	}
}
