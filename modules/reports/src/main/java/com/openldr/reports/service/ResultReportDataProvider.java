package com.openldr.reports.service;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.HIVTestType;
import com.openldr.core.service.FacilityService;
import com.openldr.reports.mapper.LabRegisterReportMapper;
import com.openldr.reports.mapper.ResultReportMapper;
import com.openldr.reports.model.ResultRow;
import com.openldr.reports.model.params.LabRegisterReportParam;
import com.openldr.reports.model.params.ResultReportParam;
import com.openldr.reports.util.SelectedFilterHelper;
import com.openldr.reports.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 7/20/17.
 */
@Service
public class ResultReportDataProvider extends ReportDataProvider{

    @Autowired
    SelectedFilterHelper filterHelper;

    @Autowired
    FacilityService service;

    @Autowired
    ResultReportMapper mapper;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filterCriteria, Map<String, String[]> sorter, int page, int pageSize) {

        return mapper.selectFilteredSortedPagedFacilities(getReportFilterData(filterCriteria), this.getUserId());
    }


    public ResultReportParam getReportFilterData(Map<String, String[]> filterCriteria) {

        ResultReportParam param = new ResultReportParam();

      //  Long sampleId = StringHelper.isBlank(filterCriteria, "labId") ? 0L : Long.parseLong(filterCriteria.get("labId")[0]);
        Facility facility=service.getHomeFacilityId(this.getUserId());
//        System.out.println(this.getUserId());
//        System.out.println(facility.getName());
        param.setLabId(facility.getId());

        Long testTypeId = StringHelper.isBlank(filterCriteria, "testTypeId") ? 0L : Long.parseLong(filterCriteria.get("testTypeId")[0]);
        param.setTestTypeId(testTypeId);

        String startTime = StringHelper.isBlank(filterCriteria, "startDate") ? null : ((String[]) filterCriteria.get("startDate"))[0];
        param.setStartDate(startTime);

        String endDate = StringHelper.isBlank(filterCriteria, "endDate") ? null : ((String[]) filterCriteria.get("endDate"))[0];
        param.setEndDate(endDate);

        String status = StringHelper.isBlank(filterCriteria, "status") ? null : ((String[]) filterCriteria.get("status"))[0];
        param.setStatus(status);
        return param;
    }

}
