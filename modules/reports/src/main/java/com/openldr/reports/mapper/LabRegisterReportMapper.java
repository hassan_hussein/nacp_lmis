package com.openldr.reports.mapper;

import com.openldr.reports.builder.LabRegisterReportQueryBuilder;
import com.openldr.reports.model.params.LabRegisterReportParam;
import com.openldr.reports.model.report.LabRegisterReport;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.ResultSetType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 6/5/17.
 */
@Repository
public interface LabRegisterReportMapper {

    @SelectProvider(type=LabRegisterReportQueryBuilder.class, method="getQuery")
    @Options(resultSetType = ResultSetType.SCROLL_SENSITIVE, fetchSize=10,timeout=0,useCache=true,flushCache=true)
    List<LabRegisterReport> selectFilteredSortedPagedFacilities(
            @Param("filterCriteria") LabRegisterReportParam filterCriteria,
            @Param("userId") Long userId
    );
}
