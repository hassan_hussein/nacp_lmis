package com.openldr.reports;

import com.openldr.core.domain.ConfigurationSetting;
import com.openldr.core.domain.User;
import com.openldr.core.service.ConfigurationSettingService;
import com.openldr.core.service.MessageService;
import com.openldr.core.service.UserService;
import com.openldr.reports.exception.ReportException;
import com.openldr.reports.exporter.ReportExporter;
import com.openldr.reports.model.ResultRow;
import com.openldr.reports.util.Constants;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 4/2/17.
 */
@NoArgsConstructor
@AllArgsConstructor
public class ReportManager {

    private ReportExporter reportExporter;

    private Map<String,Report> reportsByKey;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private ConfigurationSettingService configurationService;

    public ReportManager(ReportExporter reportExporter, List<Report> reports) {
        this(reports);
        this.reportExporter = reportExporter;
    }

    private ReportManager(List<Report> reports){
        if(reports != null){
            reportsByKey = new HashMap<>();
            for (Report report: reports){
                reportsByKey.put(report.getReportKey(),report);
            }
        }
    }

    /**
     *
     * @param report
     * @param params
     * @param outputOption
     * @param response
     */
    public void showReport(Integer userId, Report report, Map<String, String[]> params, ReportOutputOption outputOption, HttpServletResponse response){

        if (report == null){
            throw new ReportException("invalid report");
        }

        List<? extends ResultRow> dataSource = report.getReportDataProvider().getResultSet(params);
        Map<String, Object> extraParams = getReportExtraDataSourceParams(userId, params, outputOption, dataSource, report);

        InputStream reportInputStream =  this.getClass().getClassLoader().getResourceAsStream(report.getTemplate()) ;
        reportExporter.exportReport(reportInputStream, extraParams, dataSource, outputOption, response);
    }

    public ByteArrayOutputStream exportReportBytesStream(Integer userId, Report report, Map<String, String[]> params, ReportOutputOption outputOption){

        if (report == null){
            throw new ReportException("invalid report");
        }

        List<? extends ResultRow> dataSource = report.getReportDataProvider().getResultSet(params);
        Map<String, Object> extraParams = getReportExtraDataSourceParams(userId, params, outputOption, dataSource, report);

        // Read the report template from file.
        InputStream reportInputStream =  this.getClass().getClassLoader().getResourceAsStream(report.getTemplate()) ;

        return reportExporter.exportReportBytesStream(reportInputStream, extraParams, dataSource, outputOption);

    }

    private  Map<String, Object> getReportExtraDataSourceParams(Integer userId, Map<String, String[]> params , ReportOutputOption outputOption, List<? extends ResultRow> dataSource, Report report){

        User currentUser = userService.getById(Long.parseLong(String.valueOf(userId)));
        report.getReportDataProvider().setUserId(userId.longValue());

        Map<String, Object> extraParams = getReportExtraParams(report, currentUser.getFirstName() + " " + currentUser.getLastName(), outputOption.name(), params ) ;

        //Setup message for a report when there is no data found
        if(dataSource != null && dataSource.size() == 0){

            if(extraParams != null){
                extraParams.put(Constants.REPORT_MESSAGE_WHEN_NO_DATA, configurationService.getByKey(Constants.REPORT_MESSAGE_WHEN_NO_DATA).getValue());
            }else {
                extraParams = new HashMap<String, Object>();
                extraParams.put(Constants.REPORT_MESSAGE_WHEN_NO_DATA, configurationService.getByKey(Constants.REPORT_MESSAGE_WHEN_NO_DATA).getValue());
            }
        }

        return extraParams;
    }

    /**
     *
     * @param reportKey
     * @param params
     * @param outputOption
     * @param response
     */
    public void showReport(Integer userId, String reportKey, Map<String, String[]> params, ReportOutputOption outputOption, HttpServletResponse response){
        showReport(userId, getReportByKey(reportKey), params, outputOption, response);
    }

    /**
     *
     * @param userId
     * @param reportKey
     * @param params
     * @param outputOption
     * @return ByteArrayOutputStream
     */
    public ByteArrayOutputStream exportReportBytesStream(Integer userId, String reportKey, Map<String, String[]> params, String outputOption){

        switch (outputOption.toUpperCase()) {
            case "CSV":
                return exportReportBytesStream(userId, getReportByKey(reportKey), params, ReportOutputOption.CSV);
            case "XLS":
                return exportReportBytesStream(userId, getReportByKey(reportKey), params, ReportOutputOption.XLS);
            case "HTML":
                return exportReportBytesStream(userId, getReportByKey(reportKey), params, ReportOutputOption.HTML);
            case "PDF":
            default:
                return exportReportBytesStream(userId, getReportByKey(reportKey), params, ReportOutputOption.PDF);
        }
    }


    /**
     * Used to extract extra parameters that are used by report header and footer.
     *
     *
     * @param report
     * @param outputOption
     * @param filterCriteria
     * @return
     */
    private Map<String, Object> getReportExtraParams(Report report, String generatedBy, String outputOption, Map<String, String[]> filterCriteria){

        if (report == null) {
            return null;
        }

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.REPORT_NAME, report.getName());
        params.put(Constants.REPORT_ID, report.getId());
        params.put(Constants.REPORT_TITLE, messageService.message("report.hiv.title.title"));
        params.put(Constants.REPORT_SUB_BRANCH_TITLE, report.getSubBranchTitle());
        params.put(Constants.REPORT_VERSION, report.getVersion());
        params.put(Constants.REPORT_OUTPUT_OPTION, outputOption);
        ConfigurationSetting configuration =  configurationService.getByKey(Constants.LOGO_FILE_NAME_KEY);
        params.put(Constants.LOGO,this.getClass().getClassLoader().getResourceAsStream(configuration != null ? configuration.getValue() : "logo-tz.png"));
        configuration =  configurationService.getByKey(Constants.REPORT_SUBTITLE_TITLE_KEY);
        params.put(Constants.REPORT_SUB_TITLE,messageService.message("report.title"));
        params.put(Constants.GENERATED_BY, generatedBy);
        configuration =  configurationService.getByKey(Constants.OPERATOR_LOGO_FILE_NAME_KEY);

        params.put(Constants.OPERATOR_LOGO, this.getClass().getClassLoader().getResourceAsStream(configuration != null ? configuration.getValue() : "logo.png"));
        params.put(Constants.REPORT_FILTER_PARAM_VALUES, report.getReportDataProvider().getFilterSummary(filterCriteria));

        configuration =  configurationService.getByKey(Constants.OPERATOR_NAME);
        params.put(Constants.OPERATOR_NAME, configuration.getValue());

        configuration = configurationService.getByKey(Constants.VIMS_LOGO_FILE_NAME_KEY);
        params.put(Constants.VIMS_LOGO,this.getClass().getClassLoader().getResourceAsStream(configuration != null ? configuration.getValue() : "nacp-logo.png"));

        String reportCountryTitle=configurationService.getConfigurationStringValue(Constants.REPORT_COUNTRY_TITLE_KEY);
        params.put(Constants.REPORT_COUNTRY_TITLE,messageService.message("report.country.title"));

        // populate all the rest of the report parameters as overriden by the report data provider
        Map<String, String> values = report.getReportDataProvider().getExtendedHeader(filterCriteria);
        if(values != null){
            for(String key : values.keySet()){
                params.put(key, values.get(key));
            }
        }

        return params;

    }

    public Report getReportByKey(String reportKey){
        return reportsByKey.get(reportKey);
    }
}
