package com.openldr.reports.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;
import com.openldr.reports.builder.HVLResultQueryBuilder;
import org.apache.ibatis.mapping.ResultSetType;

import com.openldr.reports.model.params.HVLResultReportParam;
import com.openldr.reports.model.report.HVLResult;

@Repository
public interface HVLResultMapper {
	@SelectProvider(type=HVLResultQueryBuilder.class, method="getQuery")
	@Options(resultSetType=ResultSetType.SCROLL_SENSITIVE, fetchSize=10, timeout=0, useCache=true,flushCache=true)
	List<HVLResult> selectFilteredSortedPagedResults(
			@Param("filterCriteria") HVLResultReportParam filterCriteria,
			@Param("userId")Long userId
			);
}
