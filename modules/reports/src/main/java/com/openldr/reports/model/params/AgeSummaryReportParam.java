package com.openldr.reports.model.params;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by hassan on 9/3/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AgeSummaryReportParam extends BaseParam {

   private String status;
   private Long startAge;
   private Long endAge;
   private Long facilityId;
   private String startTime;
   private String endTime;


}
