package com.openldr.reports.model.report;

import com.openldr.reports.model.ResultRow;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResultReport implements ResultRow {

    private String facilityCode;
    private String facilityName;
    private String district;
    private String region;
    private String patientId;
    private String sex;
    private Date dateOfBirth;
    private Integer age;
    private Date collectionDate;
    private String sampleType;
    private String labNumber;
    private String result;
    private Date resultDate;
    private String labName;
    private String testedBy;
    private String  authorizedBy;
    private String gender;
    private String status;
    private String sampleID;
    private String breastFeeding;
    private String pregnant;
    private String result1;
    private String result2;
    private boolean isBreastFeeding;
    private boolean isPregnant;

    public String getPatientId(){
        String pId = this.patientId.replace("-","");

        if(pId.length() > 12){
            List<String> strList = Arrays.asList(
                    pId.substring(0,2),
                    pId.substring(2,4) ,
                    pId.substring(4,8),
                    StringUtils.leftPad(pId.substring(8),6,"0"));
            this.patientId = String.join("-", strList);
        }

        return this.patientId;
    }

    public String getBreastFeeding(){
        if(this.sex!=null && this.sex.equalsIgnoreCase("FEMALE")){
//            if(this.breastFeeding!=null && this.breastFeeding.equalsIgnoreCase("false")){
            if(!this.isBreastFeeding){
                this.breastFeeding = "N";
            }else{ //if(this.breastFeeding!=null && this.breastFeeding.equalsIgnoreCase("true")){
                this.breastFeeding = "Y";
            }//else{
               // this.breastFeeding = "";
            //}
        }else{
            this.breastFeeding = "N/A";
        }
        return this.breastFeeding;
    }

    public String getPregnant(){

        if(this.sex!=null && this.sex.equalsIgnoreCase("FEMALE")){
            //if(this.pregnant!=null && this.pregnant.equalsIgnoreCase("false")){
            if(!this.isPregnant){
                this.pregnant = "N";
            }else{// if(this.pregnant!=null && this.pregnant.equalsIgnoreCase("true")){
                this.pregnant = "Y";
            }//else{
               // this.pregnant = "";
            //}
        }else{
            this.pregnant = "N/A";
        }
        return this.pregnant;
    }

    public String getCollectionDate() throws ParseException {
        return this.collectionDate == null ? null : new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(this.collectionDate);
    }

    public String getResultDate() throws ParseException {
        return this.resultDate == null ? null : new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(this.resultDate);
    }

    public String getDateOfBirth() throws ParseException {
        return this.dateOfBirth == null ? null : new SimpleDateFormat("dd-MM-yyyy").format(this.dateOfBirth);
    }
}
