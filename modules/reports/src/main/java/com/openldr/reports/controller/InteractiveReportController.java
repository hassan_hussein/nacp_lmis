/*
 * Electronic Logistics Management Information System (eLMIS) is a supply chain management system for health commodities in a developing country setting.
 *
 * Copyright (C) 2015  John Snow, Inc (JSI). This program was produced for the U.S. Agency for International Development (USAID). It was prepared under the USAID | DELIVER PROJECT, Task Order 4.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.openldr.reports.controller;

import com.openldr.core.web.controller.BaseController;
import com.openldr.reports.Report;
import com.openldr.reports.ReportManager;
import com.openldr.reports.model.Pages;
import com.openldr.reports.model.report.*;
import lombok.NoArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import java.util.List;


import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@NoArgsConstructor
@RequestMapping(value = "/reports")
public class InteractiveReportController extends BaseController {

    @Autowired
    public ReportManager reportManager;


    @RequestMapping(value = "/reportdata/facilitylist", method = GET, headers = BaseController.ACCEPT_JSON)
    public Pages getFacilityLists(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "max", required = false, defaultValue = "10") int max,
            HttpServletRequest request
    ) {
        Report report = reportManager.getReportByKey("facilities");
        report.getReportDataProvider().setUserId(loggedInUserId(request));
        List<FacilityReport> facilityReportList = (List<FacilityReport>) report.getReportDataProvider().getReportBody(request.getParameterMap(), request.getParameterMap(), page, max);

        return new Pages(page, max, facilityReportList);
    }

    @RequestMapping(value = "/reportdata/samples", method = GET, headers = BaseController.ACCEPT_JSON)
    public Pages getAllSamples(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "max", required = false, defaultValue = "10") int max,
            HttpServletRequest request
    ) {
        Report report = reportManager.getReportByKey("result_report");
        report.getReportDataProvider().setUserId(loggedInUserId(request));
        List<FacilityHIVViralLoadResult> facilityReportList = (List<FacilityHIVViralLoadResult>) report.getReportDataProvider().getReportBody(request.getParameterMap(), request.getParameterMap(), page, max);
        return new Pages(page, max, facilityReportList);
    }

    @RequestMapping(value = "/reportdata/registerReport", method = GET, headers = BaseController.ACCEPT_JSON)
    public Pages registerReport(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "max", required = false, defaultValue = "10") int max,
            HttpServletRequest request
    ) {
        Report report = reportManager.getReportByKey("register_report");
        report.getReportDataProvider().setUserId(loggedInUserId(request));
        List<LabRegisterReport> facilityReportList = (List<LabRegisterReport>) report.getReportDataProvider().getReportBody(request.getParameterMap(), request.getParameterMap(), page, max);
        return new Pages(page, max, facilityReportList);
    }

    @RequestMapping(value = "/reportdata/resultReport", method = GET, headers = BaseController.ACCEPT_JSON)
    public Pages resultReport(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "max", required = false, defaultValue = "10") int max,
            HttpServletRequest request
    ) {
        Report report = reportManager.getReportByKey("result2_report");
        report.getReportDataProvider().setUserId(loggedInUserId(request));
        List<ResultReport> facilityReportList = (List<ResultReport>) report.getReportDataProvider().getReportBody(request.getParameterMap(), request.getParameterMap(), page, max);
        return new Pages(page, max, facilityReportList);
    }

    @RequestMapping(value = "/reportdata/summary", method = GET, headers = BaseController.ACCEPT_JSON)
    public Pages getSummaryByAge(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "max", required = false, defaultValue = "10") int max,
            HttpServletRequest request
    ) {
        Report report = reportManager.getReportByKey("report_summary");
        report.getReportDataProvider().setUserId(loggedInUserId(request));
        List<AgeSummaryReport> summaryReportList = (List<AgeSummaryReport>) report.getReportDataProvider().getReportBody(request.getParameterMap(), request.getParameterMap(), page, max);

        return new Pages(page, max, summaryReportList);
    }



}
