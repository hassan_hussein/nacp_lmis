package com.openldr.reports.builder;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.openldr.reports.model.params.ResultReportParam;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by hassan on 7/20/17.s
 */
@Slf4j
public class ResultReportQueryBuilder {

    public static String getQuery(Map params) {

        ResultReportParam param = (ResultReportParam) params.get("filterCriteria");

        String query = "SELECT s.*, s.id sampleID,date_part('year',age(p.dob))  age, p.patientid AS patientId,f.name AS facilityName,l.name AS lab,\n" +
                "s.patientispregnant as isPregnant,s.patientisbreastfeeding as isBreastFeeding,p.gender as sex,\n" +
                "case when left(r.result::varchar,1) in('T','F','I','<')  then r.result else '' end as result1,\n" +
                "case when LEFT(r.result::varchar,1) <> '<' and left(r.result::varchar,1)<>'T' and left(r.result::varchar,1) not in('F','I') then r.result  else '' end as result2,\n" +
                "h.name AS hub, gz.region, gz.district, st.name sample,s.status,r.result,st.name sampleType FROM hiv_results r\n " +
                " JOIN hiv_samples s ON s.id=r.sampleid\n " +
                " LEFT JOIN patients p ON p.id=s.patientautoid \n " +
                " LEFT JOIN facilities f ON f.id=s.facilityid \n " +
                " LEFT JOIN facilities l ON l.id=s.labid \n " +
                " LEFT JOIN facilities h ON h.id=s.hubid \n" +
                " LEFT JOIN view_tz_geographic_zones gz ON gz.districtid=s.geographiczoneid \n " +
                " LEFT JOIN sample_types st ON st.id=s.sampletypeid \n " +
                "WHERE s.testtypeid=1 AND (s.labid=" + param.getLabId() + " OR s.hubId=  " + param.getLabId() + " ) \n";

        String start = param.getStartDate(), end = param.getEndDate(),status =param.getStatus();
        if(end!=null){
            query += " AND r.modifiedDate::DATE <='" + end + "'::DATE \n";
        }
        if(start!=null){
            query += " AND r.modifiedDate::DATE >='" + start + "'::DATE \n";
        }

        if(!StringUtils.isEmpty(status))
            query += " AND status='"+status+"' \n";

        query += " ORDER BY LABNUMBER ASC ";

        if (end==null & start==null){
            query +=" LIMIT 100";
        }

        return query;

    }

}
