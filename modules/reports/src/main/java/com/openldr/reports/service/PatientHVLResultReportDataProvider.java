package com.openldr.reports.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.openldr.reports.mapper.HVLResultMapper;
import com.openldr.reports.model.ResultRow;
import com.openldr.reports.model.params.HVLResultReportParam;
import com.openldr.reports.util.SelectedFilterHelper;
import com.openldr.reports.util.StringHelper;
import org.springframework.stereotype.Service;

@Service
public class PatientHVLResultReportDataProvider extends ReportDataProvider {
	
	@Autowired
	SelectedFilterHelper filterHelper;
	@Autowired
	HVLResultMapper mapper;

	@Override
	public List<? extends ResultRow> getReportBody(Map<String, String[]> filterCriteria, Map<String, String[]> sorter, int page,
			int pageSize) {
		return mapper.selectFilteredSortedPagedResults(getReportFilterData(filterCriteria), this.getUserId());
	}
	
	public HVLResultReportParam getReportFilterData(Map<String, String[]> filterCriteria) {
		HVLResultReportParam params = new HVLResultReportParam();
		Long sampleId = StringHelper.isBlank(filterCriteria, "sampleId")?0L:Long.parseLong(filterCriteria.get("sampleId")[0]);
		params.setSampleId(sampleId);
		return params;
	}
}
