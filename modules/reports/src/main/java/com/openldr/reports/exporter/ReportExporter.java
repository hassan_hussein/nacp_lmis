package com.openldr.reports.exporter;


import com.openldr.reports.ReportOutputOption;
import com.openldr.reports.model.ResultRow;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 *  Defines API for exporting reports.
 *  Implementation can be done for different Java reporting frameworks.(Jasper, BIRT)
 */
public interface ReportExporter {

    /**
     *
     * @param reportInputStream -   <b>The report being exported</b>
     * @param reportExtraParams  -  <b>Extra report parameters that can be passed to the report to fill report header and footer details</b>
     * @param reportData   - <b>DataSource used to fill the report</b>
     * @param outputOption  -   <b>Report out put option </b>
     * @param response - <b>HttpServletResponse for writing the report to OutputStream</b>
     */
    public void exportReport(InputStream reportInputStream, Map<String, Object> reportExtraParams, List<? extends ResultRow> reportData, ReportOutputOption outputOption, HttpServletResponse response);

    /**
     * This method return the exported report byte stream to save the output as a file or any operation
     * @param reportInputStream
     * @param reportExtraParams
     * @param reportData
     * @param outputOption
     * @return
     */
    public ByteArrayOutputStream exportReportBytesStream(InputStream reportInputStream, Map<String, Object> reportExtraParams, List<? extends ResultRow> reportData, ReportOutputOption outputOption);

}