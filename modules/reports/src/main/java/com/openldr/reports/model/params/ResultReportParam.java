package com.openldr.reports.model.params;

import lombok.*;

/**
 * Created by hassan on 7/20/17.
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class ResultReportParam extends BaseParam{

private Long testTypeId;
private Long labId;
private String status;
private String startDate;
private String endDate;

public Long hubId;
}
