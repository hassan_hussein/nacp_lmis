package com.openldr.reports.service;

import com.openldr.core.domain.Program;
import com.openldr.reports.mapper.FacilityHIVViralLoadMapper;
import com.openldr.reports.model.ResultRow;
import com.openldr.reports.model.dto.Facility;
import com.openldr.reports.model.params.FacilityHIVViralLoadReportParam;
import com.openldr.reports.util.ParameterAdaptor;
import com.openldr.reports.util.SelectedFilterHelper;
import com.openldr.reports.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 4/4/17.
 */

@Service
public class FacilityHIVViralLoadDataProvider extends ReportDataProvider {

    @Autowired
    SelectedFilterHelper filterHelper;
    @Autowired
    FacilityHIVViralLoadMapper mapper;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filterCriteria, Map<String, String[]> sorter, int page, int pageSize) {

        return mapper.SelectFilteredSortedPagedFacilities(getReportFilterData(filterCriteria),this.getUserId());
    }


    public FacilityHIVViralLoadReportParam getReportFilterData(Map<String, String[]> filterCriteria) {

        FacilityHIVViralLoadReportParam param = new FacilityHIVViralLoadReportParam();

        Long sampleId = StringHelper.isBlank(filterCriteria, "sampleId") ? 0L : Long.parseLong(filterCriteria.get("sampleId")[0]);
        param.setSampleId(sampleId);

        return param;
    }

 /*   public FacilityHIVViralLoadReportParam getReportFilterData(Map<String, String[]> filterCriteria) {
        return ParameterAdaptor.parse(filterCriteria, FacilityHIVViralLoadReportParam.class);
    }*/
}
