package com.openldr.reports.mapper;

import com.openldr.reports.builder.FacilityHIVViralLoadResultQueryBuilder;

import com.openldr.reports.model.dto.TestAttachment;
import com.openldr.reports.model.params.FacilityHIVViralLoadReportParam;

import com.openldr.reports.model.report.FacilityHIVViralLoadResult;

import com.openldr.reports.service.TestAttachmentService;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.ResultSetType;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 4/4/17.
 */
@Repository
public interface FacilityHIVViralLoadMapper {

    @SelectProvider(type=FacilityHIVViralLoadResultQueryBuilder.class, method="getQuery")
    @Options(resultSetType = ResultSetType.SCROLL_SENSITIVE, fetchSize=10,timeout=0,useCache=true,flushCache=true)
    List<FacilityHIVViralLoadResult> SelectFilteredSortedPagedFacilities(
            @Param("filterCriteria") FacilityHIVViralLoadReportParam filterCriteria,
          @Param("userId") Long userId
    );

    @Select("select firstName,lastName from users")
    List<TestAttachment> getUserList();

    @Select("\n" +
            "\n" +
            "SELECT J.*,jj.* resultGreater,x.* male,y.* female,z.*adult ,k.* pediatric,l.* FROM (\n" +
            "\n" +
            "SELECT COUNT(*)Male FROM hiv_sample_status_changes C\n" +
            "JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "JOIN patients p on s.patientautoid = p.id\n" +
            "where c.status = 'RECEIVED' and p.gender = 'Male'\n" +
            ")X,\n" +
            "(SELECT COUNT(*)Female FROM hiv_sample_status_changes C\n" +
            "JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "JOIN patients p on s.patientautoid = p.id\n" +
            "where c.status = 'RECEIVED' and p.gender = 'Female')y,\n" +
            "\n" +
            "(SELECT COUNT(*)adult   FROM hiv_sample_status_changes C\n" +
            "JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "JOIN patients p on s.patientautoid = p.id\n" +
            "where c.status = 'RECEIVED' AND date_part('year',age(dob)) > 15  )z,\n" +
            "\n" +
            "(SELECT COUNT(*)Pediatric   FROM hiv_sample_status_changes C\n" +
            "JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "JOIN patients p on s.patientautoid = p.id\n" +
            "where c.status = 'RECEIVED' AND date_part('year',age(dob)) <= 15  )k,\n" +
            "\n" +
            "(SELECT COUNT(*)pregnantwomen   FROM hiv_sample_status_changes C\n" +
            "JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "JOIN patients p on s.patientautoid = p.id\n" +
            "where c.status = 'RECEIVED' AND PATIENTISPREGNANT IS TRUE)l,\n" +
            "\n" +
            "(SELECT COUNT(*)firstLine   FROM hiv_sample_status_changes C\n" +
            "JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "join regimens rg ON rg.id = s.patientregimenid \n" +
            "where  rg.regimenlineid =1)m,\n" +
            "(\n" +
            "select  COUNT(*) testDone  from hiv_sample_status_changes c\n" +
            "INNER join Hiv_RESULTS r ON c.sampleid = r.sampleid\n" +
            "where c.status = 'APPROVED')j,\n" +
            "(\n" +
            "SELECT count(*) resultGreater FROM hiv_sample_status_changes c JOIN \n" +
            "(select sampleId,RESULT from (\n" +
            " SELECT sampleId, NULLIF(regexp_replace(result, '\\D','','g'), '')::numeric AS result\n" +
            "FROM   Hiv_RESULTS)y\n" +
            "where result is not null and result > 1000)z ON C.sampleID = Z.sampleID \n" +
            ") jj\n")
        List<TestAttachment>resultList();


}
