package com.openldr.reports.mapper;

import com.openldr.reports.builder.LabRegisterReportQueryBuilder;
import com.openldr.reports.builder.ResultReportQueryBuilder;
import com.openldr.reports.model.params.LabRegisterReportParam;
import com.openldr.reports.model.params.ResultReportParam;
import com.openldr.reports.model.report.LabRegisterReport;
import com.openldr.reports.model.report.ResultReport;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.ResultSetType;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ResultReportMapper {

    @SelectProvider(type=ResultReportQueryBuilder.class, method="getQuery")
    @Options(resultSetType = ResultSetType.SCROLL_SENSITIVE, fetchSize=10,timeout=0,useCache=true,flushCache=true)
    List<ResultReport> selectFilteredSortedPagedFacilities(
            @Param("filterCriteria") ResultReportParam filterCriteria,
            @Param("userId") Long userId
    );
}
