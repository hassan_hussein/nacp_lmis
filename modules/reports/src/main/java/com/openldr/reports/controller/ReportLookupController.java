/*
 * Electronic Logistics Management Information System (eLMIS) is a supply chain management system for health commodities in a developing country setting.
 *
 * Copyright (C) 2015  John Snow, Inc (JSI). This program was produced for the U.S. Agency for International Development (USAID). It was prepared under the USAID | DELIVER PROJECT, Task Order 4.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.openldr.reports.controller;

import com.openldr.core.service.*;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@Controller
@NoArgsConstructor
@RequestMapping(value = "/reports")
public class ReportLookupController extends BaseController {

  public static final String USER_ID = "USER_ID";

  private static final String OPEN_LMIS_OPERATION_YEARS = "years";
  private static final String OPEN_LMIS_OPERATION_MONTHS = "months";
  private static final String PROGRAMS = "programs";
  private static final String SCHEDULES = "schedules";
  private static final String FACILITY_TYPES = "facilityTypes";
  private static final String FACILITY_LEVELS = "facilityLevels";
  private static final String REGIMEN_CATEGORIES = "regimenCategories";
  private static final String GEOGRAPHIC_LEVELS = "geographicLevels";
  private static final String REGIMENS = "regimens";
  private static final String ZONES = "zones";
  private static final String ZONE = "zone";
  private static final String PRODUCT_GROUPS = "productGroups";
  private static final String ALL_FACILITIES = "allFacilities";
  private static final String FACILITIES = "facilities";
  private static final String PERIODS = "periods";
  private static final String SUPERVISORY_NODES = "supervisoryNodes";
  private static final String USER_ROLE_ASSIGNMENTS = "userRoleAssignments";
  private static final String USER_ROLE_ASSIGNMENT_SUMMARY = "userRoleAssignmentSummary";
  private static final String EQUIPMENT_TYPES = "equipmentTypes";
  private static final String EQUIPMENTS = "equipments";
  private static final String PRODUCT_CATEGORY_TREE = "productCategoryTree";
  private static final String YEAR_SCHEDULE_PERIOD = "yearSchedulePeriod";
  private static final String VACCINE_PERIODS = "vaccinePeriods";
  private static final String DONORS = "donors";
  private static final String TIMELINESS_DATA = "timelinessData";
  private static final String TIMELINESS_STATUS_DATA = "timelinessStatusData";
  private static final String REPORTING_DATES = "reportingDates";
  private static final String LAST_PERIODS = "lastPeriods";
  private static final String VACCINE_CUSTOM_PERIODS = "vaccineCustomPeriods";
  private static final String FACILITY_OPERATORS = "facilityOperators";
  private static final String FACILITY_LEVELS_WITHOUT_PROGRAM = "facility_levels_without_programs";


  @Autowired
  private FacilityService facilityService;
  @Autowired
  private RoleRightsService roleRightsService;
  @Autowired
  private ProcessingScheduleService processingScheduleService;

  @Autowired
  private SupervisoryNodeService supervisoryNodeService;
  @Autowired
  private ProgramService programService;

}
