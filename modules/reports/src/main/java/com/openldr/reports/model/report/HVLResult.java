package com.openldr.reports.model.report;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.openldr.core.domain.BaseModel;
import com.openldr.reports.model.ResultRow;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HVLResult extends BaseModel implements ResultRow {
	private String facilityCode;
    private String facilityName;
    private String district;
    private String region;
    private String patientId;
    private String sex;
    private Date dateOfBirth;
    private Integer age;
    private Date collectionDate;
    private String sampleType;
    private String testType;
    private String requestedBy;
    private String labNumber;
    private String result;
    private Date reportingDate;
    private String labName;
    private String testedBy;
    private Date resultDate;
    private String  authorisedBy;
    private Date authorisedDate;
    private String gender;
    private Date receivingDate;
    private String status;
    private String name;
    private String unit;
    private String equipment;
    private String serial;

    public String getResult(){
        if (this.result == null)
            return null;

        if (this.unit !=null)
            return this.result +" " + this.unit;

        return  this.result;
    }

    public String getFacilityCode(){
        String fCode = this.facilityCode.replace("-","");

        if(fCode.length()==8){
            List<String> strList = Arrays.asList(fCode.substring(0,2),fCode.substring(2,4) ,fCode.substring(4,8));
            this.facilityCode = String.join("-", strList);
        }

        return this.facilityCode;
    }


    public String getPatientId(){
        String pId = this.patientId.replace("-","");

        if(pId.length() > 12){
            List<String> strList = Arrays.asList(
                    pId.substring(0,2),
                    pId.substring(2,4) ,
                    pId.substring(4,8),
                    StringUtils.leftPad(pId.substring(8),6,"0"));
            this.patientId = String.join("-", strList);
        }

        return this.patientId;
    }
}
