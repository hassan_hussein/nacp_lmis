package com.openldr.reports.mapper;

import com.openldr.reports.builder.AgeSummaryReportQueryBuilder;
import com.openldr.reports.builder.FacilityHIVViralLoadResultQueryBuilder;
import com.openldr.reports.model.params.AgeSummaryReportParam;
import com.openldr.reports.model.params.FacilityHIVViralLoadReportParam;
import com.openldr.reports.model.report.AgeSummaryReport;
import com.openldr.reports.model.report.FacilityHIVViralLoadResult;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.ResultSetType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 9/4/17.
 */

@Repository
public interface AgeSummaryReportMapper {

    @SelectProvider(type=AgeSummaryReportQueryBuilder.class, method="getQuery")
    @Options(resultSetType = ResultSetType.SCROLL_SENSITIVE, fetchSize=10,timeout=0,useCache=true,flushCache=true)
    List<AgeSummaryReport> SelectFilteredSortedPagedAgeSummary(
            @Param("filterCriteria") AgeSummaryReportParam filterCriteria,
            @Param("userId") Long userId
    );

}
