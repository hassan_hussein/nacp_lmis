package com.openldr.reports.builder;

import com.openldr.reports.model.params.FacilityHIVViralLoadReportParam;

import java.util.Map;

/**
 * Created by hassan on 4/4/17.
 */
public class FacilityHIVViralLoadResultQueryBuilder {

public static String getQuery(Map param){

    FacilityHIVViralLoadReportParam filter = (FacilityHIVViralLoadReportParam) param.get("filterCriteria");
    return "\n" +
            "Select f.code as facilityCode,f.name as facilityName,district,region,p.patientid as patientId,p.gender sex,p.dob dateOfBirth,\n" +
            " date_part('year',age(p.dob))  age,\n" +
            "\n" +
            " dateofsamplecollection collectionDate,\n" +
            "\n" +
            "st.name sampleType,labNumber,result, r.resultDate reportingDate,l.name labName\n" +
            ", gz.region, gz.district, st.name sampleType from hiv_results r\n" +
            " join hiv_samples s on s.id=r.sampleid\n" +
            " left join patients p on p.id=s.patientautoid \n" +
            " left join facilities f on f.id=s.facilityid \n" +
            " left join facilities l on l.id=s.labid \n" +
            " left join facilities h on h.id=s.hubid \n" +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid \n" +
            " left join sample_types st on st.id=s.sampletypeid \n" +
            " where s.status in ('DISPATCHED','RESULTED','APPROVED') AND s.id =  " +filter.getSampleId()+
            " ORDER BY S.LABNUMBER ASC";
}


}
