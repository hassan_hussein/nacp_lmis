package com.openldr.reports.service;

import com.openldr.core.domain.Facility;
import com.openldr.core.service.FacilityService;
import com.openldr.reports.mapper.LabRegisterReportMapper;
import com.openldr.reports.model.ResultRow;
import com.openldr.reports.model.params.LabRegisterReportParam;
import com.openldr.reports.util.SelectedFilterHelper;
import com.openldr.reports.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 6/5/17.
 */

@Service
public class LabRegisterReportDataProvider extends ReportDataProvider {

    @Autowired
    SelectedFilterHelper filterHelper;

    @Autowired
    LabRegisterReportMapper mapper;
    @Autowired
    FacilityService service;
    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filterCriteria, Map<String, String[]> sorter, int page, int pageSize) {

        return mapper.selectFilteredSortedPagedFacilities(getReportFilterData(filterCriteria), this.getUserId());
    }

    public LabRegisterReportParam getReportFilterData(Map<String, String[]> filterCriteria) {

        LabRegisterReportParam param = new LabRegisterReportParam();

        Facility facility=service.getHomeFacilityId(this.getUserId());
        System.out.println(this.getUserId());
        System.out.println(facility.getName());

        //Long sampleId = StringHelper.isBlank(filterCriteria, "labId") ? 0L : Long.parseLong(filterCriteria.get("labId")[0]);
        param.setLabId(facility.getId());

//        String status = StringHelper.isBlank(filterCriteria, "status") ? null : ((String[]) filterCriteria.get("status"))[0];

        if(!StringHelper.isBlank(filterCriteria,"status"))
            param.setStatus(((String[])filterCriteria.get("status"))[0]);

//        String startTime = StringHelper.isBlank(filterCriteria, "startTime") ? null : ((String[]) filterCriteria.get("startTime"))[0];
        if(!StringHelper.isBlank(filterCriteria,"startTime"))
            param.setStartTime(((String[]) filterCriteria.get("startTime"))[0]);

//        String endTime = StringHelper.isBlank(filterCriteria, "endTime") ? null : ((String[]) filterCriteria.get("endTime"))[0];
        if(!StringHelper.isBlank(filterCriteria,"endTime"))
            param.setEndTime(((String[]) filterCriteria.get("endTime"))[0]);
        return param;
    }


}
