package com.openldr.reports.builder;

import com.openldr.reports.model.params.AgeSummaryReportParam;

import java.util.Map;

/**
 * Created by hassan on 9/3/17.
 */
public class AgeSummaryReportQueryBuilder {

    public static String getQuery(Map params) {
        AgeSummaryReportParam filter = (AgeSummaryReportParam) params.get("filterCriteria");
        return
            "WITH Q as (\n" +
            "   SELECT X.facilityID, m.facilityName,m.femaleresult,x.maleResult FROM (\n" +
            "     SELECT f.id facilityId,F.NAME facilityName,count(*) femaleResult FROM hiv_sample_status_changes c \n" +
            "     INNER JOIN \n" +
            "         (SELECT sampleID,REGEXP_REPLACE(COALESCE(result, '0'), '[^0-9]*' ,'0')::integer result \n" +
            "         FROM Hiv_RESULTS where result <> 'DETECTED')y ON C.sampleID = Y.sampleID\n" +
            "          INNER JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "          INNER JOIN patients p on s.patientautoid = p.id\n" +
            "          INNER JOIN facilities f ON s.facilityID = f.id\n" +
            "          WHere result >= 1000 and  c.status =  '" + filter.getStatus() + "' and p.gender IN('Male') and labid in('" + filter.getFacilityId() + "')\n" +
            "          and  (date_part('year',age(dob)) between '" + filter.getStartAge() + "' and '" + filter.getEndAge() + "' )" +
            "   and c.createdDate::date <= '" + filter.getEndTime() + "' and c.createdDate::date >='" + filter.getStartTime() + "' \n" +
            "          group by F.NAME,f.id)M JOIN (\n" +
            "          SELECT f.id facilityId,F.NAME facilityName,count(*) maleResult FROM hiv_sample_status_changes c \n" +
            "          INNER JOIN \n" +
            "         (SELECT sampleID,REGEXP_REPLACE(COALESCE(result, '0'), '[^0-9]*' ,'0')::integer result \n" +
            "         FROM Hiv_RESULTS where result <> 'DETECTED')y ON C.sampleID = Y.sampleID\n" +
            "         INNER JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "          INNER JOIN patients p on s.patientautoid = p.id\n" +
            "          INNER JOIN facilities f ON s.facilityID = f.id\n" +
            "          WHere result >= 1000 and  c.status = '" + filter.getStatus() + "' and p.gender IN('Female') and labid in('" + filter.getFacilityId() + "') and \n" +
            "         (date_part('year',age(dob)) between '" + filter.getStartAge() + "' and '" + filter.getEndAge() + "')\n" +
            "   and c.createdDate::date <= '" + filter.getEndTime() + "' and c.createdDate::date >='" + filter.getStartTime() + "' \n" +
            "  group by F.NAME,f.id)X ON M.facilityid =x.facilityID\n" +
            "          )select * from q \n" +
            "          INNER JOIN (\n" +
            "       SELECT X.facilityID, m.facilityName,m.maleresultless,x.femaleResultless FROM (\n" +
            "      SELECT f.id facilityId,F.NAME facilityName,count(*) maleresultless FROM hiv_sample_status_changes c \n" +
            "      INNER JOIN \n" +
            "         (SELECT sampleID,REGEXP_REPLACE(COALESCE(result, '0'), '[^0-9]*' ,'0')::integer result \n" +
            "         FROM Hiv_RESULTS where result <> 'DETECTED')y ON C.sampleID = Y.sampleID\n" +
            "          INNER JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "          INNER JOIN patients p on s.patientautoid = p.id\n" +
            "          INNER JOIN facilities f ON s.facilityID = f.id\n" +
            "          WHere result < 1000 and  c.status =  '" + filter.getStatus() + "' and p.gender IN('Male') and labid in('" + filter.getFacilityId() + "')\n" +
            "          and  (date_part('year',age(dob)) between '" + filter.getStartAge() + "' and '" + filter.getEndAge() + "')\n" +
            "   and c.createdDate::date <= '" + filter.getEndTime() + "' and c.createdDate::date >='" + filter.getStartTime() + "' \n" +
            "          group by F.NAME,f.id\n" +
            "          )M JOIN (\n" +
            "          SELECT f.id facilityId,F.NAME facilityName,count(*) femaleResultless FROM hiv_sample_status_changes c \n" +
            "          INNER JOIN \n" +
            "         (SELECT sampleID,REGEXP_REPLACE(COALESCE(result, '0'), '[^0-9]*' ,'0')::integer result \n" +
            "         FROM Hiv_RESULTS where result <> 'DETECTED')y ON C.sampleID = Y.sampleID\n" +
            "         INNER JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "          INNER JOIN patients p on s.patientautoid = p.id\n" +
            "          INNER JOIN facilities f ON s.facilityID = f.id\n" +
            "          WHere result < 1000 and  c.status = '" + filter.getStatus() + "' and p.gender IN('Female') and labid in('" + filter.getFacilityId() + "') and \n" +
            "           (date_part('year',age(dob)) between '" + filter.getStartAge() + "' and '" + filter.getEndAge() + "')\n" +
            "   and c.createdDate::date <= '" + filter.getEndTime() + "' and c.createdDate::date >='" + filter.getStartTime() + "' \n" +
            "          group by F.NAME,f.id)X ON M.facilityid =x.facilityID\n" +
            "          )L ON L.facilityID=q.facilityID\n" +
            "       INNER JOIN facilities f ON q.facilityID = f.id\n" +
            "       INNER join  view_tz_geographic_zones gz ON f.geographiczoneid= gz.districtid ";
    }
}
