package com.openldr.reports.service;

import com.openldr.core.domain.Facility;
import com.openldr.core.service.FacilityService;
import com.openldr.reports.mapper.AgeSummaryReportMapper;
import com.openldr.reports.model.ResultRow;
import com.openldr.reports.model.params.AgeSummaryReportParam;
import com.openldr.reports.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 9/4/17.
 */

@Service
public class AgeSummaryReportDataProvider extends ReportDataProvider {

    @Autowired
    private AgeSummaryReportMapper mapper;
    @Autowired
    private FacilityService facilityService;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filterCriteria, Map<String, String[]> sorter, int page, int pageSize) {

        return mapper.SelectFilteredSortedPagedAgeSummary(getReportFilterData(filterCriteria), this.getUserId());
    }

    public AgeSummaryReportParam getReportFilterData(Map<String, String[]> filterCriteria) {

        AgeSummaryReportParam param = new AgeSummaryReportParam();
        Facility f = facilityService.getHomeFacility(this.getUserId());

        Long facilityId = StringHelper.isBlank(filterCriteria, "facilityId") ? 0L : Long.parseLong(filterCriteria.get("facilityId")[0]);
        param.setFacilityId(f.getId());
        Long startAge = StringHelper.isBlank(filterCriteria, "startAge") ? 0L : Long.parseLong(filterCriteria.get("startAge")[0]);
        param.setStartAge(startAge);
        Long endAge = StringHelper.isBlank(filterCriteria, "endAge") ? 0L : Long.parseLong(filterCriteria.get("endAge")[0]);
        param.setEndAge(endAge);
        String status = StringHelper.isBlank(filterCriteria, "status") ? null : ((String[]) filterCriteria.get("status"))[0];
        param.setStatus(status);

        String startTime = StringHelper.isBlank(filterCriteria, "startTime") ? null : ((String[]) filterCriteria.get("startTime"))[0];
        param.setStartTime(startTime);
        String endTime = StringHelper.isBlank(filterCriteria, "endTime") ? null : ((String[]) filterCriteria.get("endTime"))[0];
        param.setEndTime(endTime);

        return param;
    }
}
