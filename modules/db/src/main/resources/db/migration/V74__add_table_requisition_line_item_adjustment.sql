DROP TABLE IF EXISTS inventory_line_item_adjustments;

CREATE TABLE inventory_line_item_adjustments
(
  lineitemid integer,
  type character varying(250) NOT NULL,
  quantity integer,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT inventory_line_item_losses_adjustm_requisitionlineitemid_fkey PRIMARY KEY (lineitemid, type),
  CONSTRAINT requisition_line_item_losses_adjustm_requisitionlineitemid_fkey FOREIGN KEY (lineitemid)
      REFERENCES inventory_report_line_items (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT inventory_line_item_losses_adjustments_type_fkey FOREIGN KEY (type)
      REFERENCES losses_adjustments_types (name) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
