-- Table: requisition_group_members

-- DROP TABLE requisition_group_members;

CREATE TABLE requisition_group_members
(
  id serial NOT NULL,
  requisitiongroupid integer NOT NULL,
  facilityid integer NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT requisition_group_members_pkey PRIMARY KEY (id),
  CONSTRAINT requisition_group_members_facilityid_fkey FOREIGN KEY (facilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT requisition_group_members_requisitiongroupid_fkey FOREIGN KEY (requisitiongroupid)
      REFERENCES requisition_groups (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT requisition_group_members_requisitiongroupid_facilityid_key UNIQUE (requisitiongroupid, facilityid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE requisition_group_members
  OWNER TO postgres;

-- Index: i_requisition_group_member_facilityid

-- DROP INDEX i_requisition_group_member_facilityid;

CREATE INDEX i_requisition_group_member_facilityid
  ON requisition_group_members
  USING btree
  (facilityid);

