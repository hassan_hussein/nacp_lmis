DROP TABLE IF EXISTS geographic_levels;

CREATE TABLE geographic_levels
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(250) NOT NULL,
  levelnumber integer NOT NULL,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT geographic_levels_pkey PRIMARY KEY (id),
  CONSTRAINT geographic_levels_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE geographic_levels
  OWNER TO postgres;


DROP TABLE IF EXISTS geographic_zones;

CREATE TABLE geographic_zones
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(250) NOT NULL,
  levelid integer NOT NULL,
  parentid integer,
  catchmentpopulation integer,
  latitude numeric(8,5),
  longitude numeric(8,5),
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT geographic_zones_pkey PRIMARY KEY (id),
  CONSTRAINT geographic_zones_levelid_fkey FOREIGN KEY (levelid)
      REFERENCES geographic_levels (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT geographic_zones_parentid_fkey FOREIGN KEY (parentid)
      REFERENCES geographic_zones (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT geographic_zones_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE geographic_zones
  OWNER TO postgres;



DROP TABLE IF EXISTS facility_types;

CREATE TABLE facility_types
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(30) NOT NULL,
  description character varying(250),
  levelid integer,
  nominalmaxmonth integer NOT NULL,
  nominaleop numeric(4,2) NOT NULL,
  displayorder integer,
  active boolean,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT facility_types_pkey PRIMARY KEY (id),
  CONSTRAINT facility_types_code_key UNIQUE (code),
  CONSTRAINT facility_types_name_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE facility_types
  OWNER TO postgres;



DROP TABLE IF EXISTS facility_operators;

CREATE TABLE facility_operators
(
  id serial NOT NULL,
  code character varying NOT NULL,
  text character varying(20),
  displayorder integer,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT facility_operators_pkey PRIMARY KEY (id),
  CONSTRAINT facility_operators_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE facility_operators
  OWNER TO postgres;



DROP TABLE IF EXISTS facilities;

CREATE TABLE facilities
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(50) NOT NULL,
  description character varying(250),
  gln character varying(30),
  mainphone character varying(20),
  fax character varying(20),
  address1 character varying(50),
  address2 character varying(50),
  geographiczoneid integer NOT NULL,
  typeid integer NOT NULL,
  catchmentpopulation integer,
  latitude numeric(8,5),
  longitude numeric(8,5),
  altitude numeric(8,4),
  operatedbyid integer,
  coldstoragegrosscapacity numeric(8,4),
  coldstoragenetcapacity numeric(8,4),
  suppliesothers boolean,
  sdp boolean NOT NULL,
  online boolean,
  satellite boolean,
  parentfacilityid integer,
  haselectricity boolean,
  haselectronicscc boolean,
  haselectronicdar boolean,
  active boolean NOT NULL,
  golivedate date NOT NULL,
  godowndate date,
  comment text,
  enabled boolean NOT NULL,
  virtualfacility boolean NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  pricescheduleid integer,
  CONSTRAINT facilities_pkey PRIMARY KEY (id),
  CONSTRAINT facilities_geographiczoneid_fkey FOREIGN KEY (geographiczoneid)
      REFERENCES geographic_zones (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facilities_operatedbyid_fkey FOREIGN KEY (operatedbyid)
      REFERENCES facility_operators (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facilities_parentfacilityid_fkey FOREIGN KEY (parentfacilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facilities_typeid_fkey FOREIGN KEY (typeid)
      REFERENCES facility_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facilities_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE facilities
  OWNER TO postgres;
