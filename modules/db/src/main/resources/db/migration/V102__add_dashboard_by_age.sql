 DROP TABLE if EXISTS dashboard_test_by_ages;

CREATE TABLE dashboard_test_by_ages
(
  id serial NOT NULL,
  location character varying(100),
  total integer,
  lessthanandequalto5 integer,
  greaterthan5lessthanandequalto15 integer,
  greaterthan15 integer,
  year integer,
  month integer,
  createddate timestamp without time zone,
  CONSTRAINT dashboard_test_by_ages_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dashboard_test_by_ages
  OWNER TO postgres;
