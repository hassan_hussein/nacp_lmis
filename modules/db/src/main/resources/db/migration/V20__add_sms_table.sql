DROP TABLE IF EXISTS sms;

CREATE TABLE sms
(
  id serial NOT NULL,
  message character varying(250),
  phonenumber character varying(20),
  direction character varying(40),
  sent boolean DEFAULT false,
  datesaved date,
  CONSTRAINT sms_pkey PRIMARY KEY (id)
)