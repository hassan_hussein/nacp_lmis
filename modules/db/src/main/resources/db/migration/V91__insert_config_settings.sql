DROP TABLE IF EXISTS configuration_settings;
CREATE TABLE configuration_settings (
  id      SERIAL PRIMARY KEY,
  key     VARCHAR(250) NOT NULL,
  value   VARCHAR(250)
);

INSERT INTO configuration_settings(key, value)
   VALUES ('operator_logo_file_name', 'operator_logo_1.png');

   INSERT INTO configuration_settings(key, value)
   VALUES ('REPORTING_PROGRAM_TITLE', 'HIV Viral Load Results Report');

    INSERT INTO configuration_settings(key, value)
   VALUES ('OPERATOR_NAME', 'Ministry of Health, Community Development, Gender, Elderly and Children');

    INSERT INTO configuration_settings(key, value)
   VALUES ('OPERATOR_LOGO_FILE_NAME', 'operator-logo-tz.jpg');

      INSERT INTO configuration_settings(key, value)
   VALUES ('OPERATOR_LOGO_FILE_NAME', 'operator-logo-tz.jpg');

     INSERT INTO configuration_settings(key, value)
   VALUES ('LOGO_FILE_NAME', 'logo-tz.png');

      INSERT INTO configuration_settings(key, value)
   VALUES ('LOGO_FILE_NAME', 'logo-tz.png');

   INSERT INTO configuration_settings(key, value)
   VALUES ('REPORT_MESSAGE_WHEN_NO_DATA', 'No Data found');