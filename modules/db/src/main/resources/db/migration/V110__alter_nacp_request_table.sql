
 DO $$
    BEGIN
        BEGIN
            ALTER TABLE nacp_requests ADD COLUMN ReceivedDateTime  CHARACTER VARYING DEFAULT null;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column ReceivedDateTime already exists in <table_name>.';
        END;
    END;
$$;

 DO $$
    BEGIN
        BEGIN
            ALTER TABLE nacp_requests ADD COLUMN admitAttendDateTime  CHARACTER VARYING DEFAULT null;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column admitAttendDateTime already exists in <table_name>.';
        END;
    END;
$$;