
DROP TABLE IF EXISTS inventory_reports cascade;

CREATE TABLE inventory_reports
(
  id serial NOT NULL,
  periodid integer NOT NULL,
  programid integer NOT NULL,
  status character varying(100) NOT NULL,
  supervisorynodeid integer,
  facilityid integer,
  createdby integer,
  createddate timestamp with time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp with time zone DEFAULT now(),
  emergency boolean DEFAULT false,
  reason character varying(250),
  orderdate timestamp with time zone DEFAULT now(),
  isverified boolean NOT NULL DEFAULT false,
  CONSTRAINT inventory_reports_pkey PRIMARY KEY (id),
  CONSTRAINT facility_fkey FOREIGN KEY (facilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT inventory_report_periodid_fkey FOREIGN KEY (periodid)
      REFERENCES processing_periods (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT inventory_report_programid_fkey FOREIGN KEY (programid)
      REFERENCES programs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


DROP TABLE if exists lab_adjustment_reasons cascade;

CREATE TABLE lab_adjustment_reasons
(
  id serial NOT NULL,
  name character varying(100) NOT NULL,
  requiresexplanation boolean NOT NULL DEFAULT false,
  displayorder integer NOT NULL,
  createdby integer,
  createddate date DEFAULT now(),
  modifiedby integer,
  modifieddate date DEFAULT now(),
  CONSTRAINT vaccine_discarding_reasons_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


DROP TABLE IF EXISTS inventory_report_line_items cascade;

CREATE TABLE inventory_report_line_items
(
  id serial NOT NULL,
  reportId integer NOT NULL,
  productid integer NOT NULL,
  productname character varying(200) NOT NULL,
  productcode character varying(100) NOT NULL,
  displayorder integer NOT NULL,
  openingbalance integer,
  quantityreceived integer,
  quantityissued integer,
  endingbalance integer,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  productcategory character varying(200),
  closingbalance integer,
  daysstockedout integer,
  remarks character varying(500),
  adjustmentreasonid integer,
  discardingreasonexplanation character varying(500),
  CONSTRAINT vaccine_order_requisition_line_items_pkey PRIMARY KEY (id),
  CONSTRAINT inventory_reports_lineitemid_fkey FOREIGN KEY (reportid)
  REFERENCES inventory_reports (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT inventory_report_line_items_productid_fkey FOREIGN KEY (productid)
      REFERENCES products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT invetory_report_logistics_line_items_adjustmentreasonid_fkey FOREIGN KEY (adjustmentreasonid)
      REFERENCES lab_adjustment_reasons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

DROP TABLE IF EXISTS losses_adjustments_types cascade;

CREATE TABLE losses_adjustments_types
(
  name character varying(50) NOT NULL,
  description text NOT NULL,
  additive boolean,
  displayorder integer,
  createddate timestamp without time zone DEFAULT now(),
  isdefault boolean DEFAULT true,
  CONSTRAINT losses_adjustments_types_description_key UNIQUE (description),
  CONSTRAINT losses_adjustments_types_name_key UNIQUE (name)
);


DROP TABLE IF EXISTS inventory_line_item_adjustments cascade;
CREATE TABLE inventory_line_item_adjustments
(
  id serial NOT NULL,
  lineItemId integer,
  adjustmentreason character(50),
  quantity integer,
  createdby integer,
  createddate timestamp with time zone,
  modifiedby integer,
  modifieddate timestamp with time zone,
  effectivedate timestamp with time zone,
  CONSTRAINT adjusment_reasons_pkey PRIMARY KEY (id),
  CONSTRAINT vaccine_adjustment_reasons_fkey FOREIGN KEY (adjustmentreason)
      REFERENCES losses_adjustments_types (name) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
      CONSTRAINT inventory_line_item_losses_adjustm_requisitionlineitemid_fkey FOREIGN KEY (lineitemid)
      REFERENCES inventory_report_line_items (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


DROP TABLE IF EXISTS Inventory_report_status_changes;

CREATE TABLE Inventory_report_status_changes
(
  id serial NOT NULL,
  reportid integer NOT NULL,
  status character varying(50) NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT Inventory_report_status_changes_pkey PRIMARY KEY (id),
  CONSTRAINT Inventory_report_status_changes_reportid_fkey FOREIGN KEY (reportid)
      REFERENCES Inventory_reports (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
