 DO $$
    BEGIN
        BEGIN
            ALTER TABLE hiv_samples ADD COLUMN orderId  CHARACTER VARYING DEFAULT null;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column orderId already exists in <table_name>.';
        END;
    END;
$$