﻿DROP VIEW IF EXISTS view_tz_geographic_zones;
CREATE VIEW view_tz_geographic_zones AS
select l1.id as countryid, l1.name as country , l2.id as regionid,l2.name as region, l3.id as districtid,l3.name district
from geographic_zones l1
join geographic_zones l2 on l1.id=l2.parentid
join geographic_zones l3 on l2.id=l3.parentid;