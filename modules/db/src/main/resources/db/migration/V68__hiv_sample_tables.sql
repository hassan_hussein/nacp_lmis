DROP TABLE IF EXISTS patients;
CREATE TABLE patients
(
  id serial NOT NULL,
  patientid character varying(100),
  dob date,
  gender character varying(7),
  CONSTRAINT patient_pk PRIMARY KEY (id),
  CONSTRAINT unique_patient_id UNIQUE (patientid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patients
  OWNER TO postgres;


DROP TABLE IF EXISTS hiv_samples;
CREATE TABLE hiv_samples
(
  id serial NOT NULL,
  hubid integer,
  testtypeid integer,
  patientautoid integer,
  facilityid integer,
  geographiczoneid integer,
  nameofcellleader character varying(100),
  patientphonenumber character varying(20),
  patientispregnant boolean,
  patientisbreastfeeding boolean,
  patienthastb boolean,
  patientundertbtreatment boolean,
  samplebarcodenumber character varying(100),
  sampletypeid integer,
  patientregimenid integer,
  samplerequestdate timestamp without time zone,
  testreasonid integer,
  reasonforrepeattest character varying,
  otherregimen character varying,
  drugadherenceid integer,
  nameofpersonrequestingtest character varying(100),
  cadreofpersonrequestingtest character varying(100),
  dateofsamplecollection timestamp without time zone,
  namephlebotomist character varying(100),
  dateofsampledispatch timestamp without time zone,
  nameofsampledispatcher character varying(100),
  status vlsamplestatus,
  labid integer,
  labnumber character varying(12),
  reasonforrejection character varying(200),
  datereceived timestamp without time zone,
  testdate timestamp without time zone,
  testedby integer,
  verifiedby integer,
  verifieddate timestamp without time zone,
  resultdate timestamp without time zone,
  createdby integer,
  createddate timestamp without time zone,
  modifiedby integer,
  modifieddate timestamp without time zone,
  formincomplete boolean,
  CONSTRAINT sample_pk PRIMARY KEY (id),
  CONSTRAINT hub_sample_fkey FOREIGN KEY (hubid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT lab_sample_fkey FOREIGN KEY (labid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT sample_facility_fkey FOREIGN KEY (facilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT sample_patient_fkey FOREIGN KEY (patientautoid)
      REFERENCES patients (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT sample_test_type_fkey FOREIGN KEY (testtypeid)
      REFERENCES hiv_test_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT sample_type_fkey FOREIGN KEY (sampletypeid)
      REFERENCES sample_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_samples
  OWNER TO postgres;

DROP TABLE IF EXISTS hiv_test_worksheets;
CREATE TABLE hiv_test_worksheets
(
  id serial NOT NULL,
  worksheetnumber character varying(20),
  equipmentid integer,
  labid integer,
  testtypeid integer,
  createdby integer,
  createddate timestamp without time zone,
  modifiedby integer,
  modifieddate timestamp without time zone,
  CONSTRAINT worksheet_pk PRIMARY KEY (id),
  CONSTRAINT equipment_fkey FOREIGN KEY (equipmentid)
      REFERENCES hiv_test_equipments (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT testtype_fkey FOREIGN KEY (testtypeid)
      REFERENCES hiv_test_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_test_worksheets
  OWNER TO postgres;


DROP TABLE IF EXISTS hiv_test_worksheet_samples;
CREATE TABLE hiv_test_worksheet_samples
(
  worksheetid integer,
  sampleid integer,
  CONSTRAINT worksheet_sample_ids FOREIGN KEY (sampleid)
      REFERENCES hiv_samples (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT worksheet_sample_worksheet_fkey FOREIGN KEY (worksheetid)
      REFERENCES hiv_test_worksheets (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_test_worksheet_samples
  OWNER TO postgres;


DROP TABLE IF EXISTS hiv_results;
CREATE TABLE hiv_results
(
  id serial NOT NULL,
  sampleid integer NOT NULL,
  result character varying,
  resultdate timestamp without time zone,
  createdby integer,
  createddate timestamp without time zone,
  modifiedby integer,
  modifieddate timestamp without time zone,
  testtypeid integer,
  unit character varying(20),
  testdate timestamp without time zone,
  CONSTRAINT vlresult_pk PRIMARY KEY (id),
  CONSTRAINT sample_result_fk FOREIGN KEY (sampleid)
      REFERENCES hiv_samples (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_results
  OWNER TO postgres;



