-- Table: program_products

 DROP TABLE IF EXISTS program_products;

CREATE TABLE program_products
(
  id serial NOT NULL,
  programid integer NOT NULL,
  productid integer NOT NULL,
  dosespermonth integer NOT NULL,
  active boolean NOT NULL,
  currentprice numeric(20,2) DEFAULT 0,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  displayorder integer,
  productcategoryid integer NOT NULL,
  fullsupply boolean,
  isacoefficientsid integer,
  CONSTRAINT program_products_pkey PRIMARY KEY (id),
  CONSTRAINT program_products_productcategoryid_fkey FOREIGN KEY (productcategoryid)
      REFERENCES product_categories (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT program_products_productid_fkey FOREIGN KEY (productid)
      REFERENCES products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT program_products_programid_fkey FOREIGN KEY (programid)
      REFERENCES programs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT program_products_productid_programid_key UNIQUE (productid, programid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE program_products
  OWNER TO postgres;

-- Index: i_program_product_programid_productid

-- DROP INDEX i_program_product_programid_productid;

CREATE INDEX i_program_product_programid_productid
  ON program_products
  USING btree
  (programid, productid);

