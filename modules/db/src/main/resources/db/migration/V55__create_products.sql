-- Table: products

-- DROP TABLE products;

CREATE TABLE products
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  alternateitemcode character varying(20),
  manufacturer character varying(100),
  manufacturercode character varying(30),
  manufacturerbarcode character varying(20),
  mohbarcode character varying(20),
  gtin character varying(20),
  type character varying(100),
  primaryname character varying(150) NOT NULL,
  fullname character varying(250),
  genericname character varying(100),
  alternatename character varying(100),
  description character varying(250),
  strength character varying(14),
  formid integer,
  dosageunitid integer,
  productgroupid integer,
  dispensingunit character varying(20) NOT NULL,
  dosesperdispensingunit smallint NOT NULL,
  packsize smallint NOT NULL,
  alternatepacksize smallint,
  storerefrigerated boolean,
  storeroomtemperature boolean,
  hazardous boolean,
  flammable boolean,
  controlledsubstance boolean,
  lightsensitive boolean,
  approvedbywho boolean,
  contraceptivecyp numeric(8,4),
  packlength numeric(8,4),
  packwidth numeric(8,4),
  packheight numeric(8,4),
  packweight numeric(8,4),
  packspercarton smallint,
  cartonlength numeric(8,4),
  cartonwidth numeric(8,4),
  cartonheight numeric(8,4),
  cartonsperpallet smallint,
  expectedshelflife smallint,
  specialstorageinstructions text,
  specialtransportinstructions text,
  active boolean NOT NULL,
  fullsupply boolean NOT NULL,
  tracer boolean NOT NULL,
  roundtozero boolean NOT NULL,
  archived boolean,
  packroundingthreshold smallint NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT products_pkey PRIMARY KEY (id),
  CONSTRAINT products_dosageunitid_fkey FOREIGN KEY (dosageunitid)
      REFERENCES dosage_units (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT products_formid_fkey FOREIGN KEY (formid)
      REFERENCES product_forms (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT products_productgroupid_fkey FOREIGN KEY (productgroupid)
      REFERENCES product_groups (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT products_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE products
  OWNER TO postgres;

-- Index: uc_products_lower_code

-- DROP INDEX uc_products_lower_code;

CREATE UNIQUE INDEX uc_products_lower_code
  ON products
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");

