 DO $$
    BEGIN
        BEGIN
            ALTER TABLE hiv_samples ADD COLUMN isSentToNationalRep  boolean DEFAULT false;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column isSentToNationalRep already exists in <table_name>.';
        END;
    END;
$$