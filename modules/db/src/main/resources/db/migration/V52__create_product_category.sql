-- Table: product_categories

 DROP TABLE IF EXISTS product_categories;

CREATE TABLE product_categories
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(100) NOT NULL,
  displayorder integer NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT product_categories_pkey PRIMARY KEY (id),
  CONSTRAINT product_categories_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE product_categories
  OWNER TO postgres;

-- Index: uc_product_categories_lower_code

-- DROP INDEX uc_product_categories_lower_code;

CREATE UNIQUE INDEX uc_product_categories_lower_code
  ON product_categories
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");

