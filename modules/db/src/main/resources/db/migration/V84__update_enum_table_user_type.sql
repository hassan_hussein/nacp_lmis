DROP TYPE IF EXISTS usertype CASCADE;

CREATE TYPE USERTYPE AS ENUM
   ('LAB',
    'HUB',
    'ADMIN',
    'NORMAL_USER');
ALTER TYPE usertype
  OWNER TO postgres;

DROP TABLE IF EXISTS user_types cascade;
CREATE TABLE user_types (
    id serial,
    name USERTYPE
);