-- Table: facility_approved_products

-- DROP TABLE facility_approved_products;

CREATE TABLE facility_approved_products
(
  id serial NOT NULL,
  facilitytypeid integer NOT NULL,
  productid integer NOT NULL,
  maxmonthsofstock integer NOT NULL,
  minmonthsofstock numeric(4,2),
  eop numeric(4,2),
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT facility_approved_products_pkey PRIMARY KEY (id),
  CONSTRAINT facility_approved_products_facilitytypeid_fkey FOREIGN KEY (facilitytypeid)
      REFERENCES facility_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facility_approved_products_fkey FOREIGN KEY (productid)
      REFERENCES products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)


