DROP TABLE IF EXISTS account_wallet_transactions;

CREATE TABLE account_wallet_transactions
(
  id serial NOT NULL,
  accountwalletid integer,
  transactiontype character varying(100),
  transactionamout numeric(10,2),
  createddate timestamp without time zone,
  CONSTRAINT account_transaction_pk PRIMARY KEY (id),
  CONSTRAINT transaction_wallet_fk FOREIGN KEY (accountwalletid)
      REFERENCES account_wallet (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE account_wallet_transactions
  OWNER TO postgres;