DO $$
    BEGIN
        BEGIN
            ALTER TABLE inventory_report_line_items ADD COLUMN quantityRequested integer;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column quantityRequested already exists';
        END;
    END;
$$;