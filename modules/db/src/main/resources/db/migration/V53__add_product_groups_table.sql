-- Table: product_groups

-- DROP TABLE product_groups;

CREATE TABLE product_groups
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(250) NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT product_groups_pkey PRIMARY KEY (id),
  CONSTRAINT product_groups_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE product_groups
  OWNER TO postgres;

-- Index: uc_product_groups_lower_code

-- DROP INDEX uc_product_groups_lower_code;

CREATE UNIQUE INDEX uc_product_groups_lower_code
  ON product_groups
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");

