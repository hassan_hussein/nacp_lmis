﻿DROP TABLE IF EXISTS users;
CREATE TABLE users
(
  id serial NOT NULL,
  username character varying(50) NOT NULL,
  password character varying(128) DEFAULT 'not-in-use'::character varying,
  firstname character varying(50) NOT NULL, 
  lastname character varying(50) NOT NULL,
  employeeid character varying(50),
  jobtitle character varying(50),
  primarynotificationmethod character varying(50),
  officephone character varying(30),
  cellphone character varying(30),
  email character varying(50),
  supervisorid integer,
  facilityid integer,
  verified boolean DEFAULT false,
  active boolean DEFAULT true,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  restrictlogin boolean DEFAULT false,
  ismobileuser boolean DEFAULT false,
  CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;


CREATE TABLE rights
(
  name character varying(200) NOT NULL,
  righttype character varying(20) NOT NULL,
  description character varying(200),
  createddate timestamp without time zone DEFAULT now(),
  displayorder integer,
  displaynamekey character varying(150),
  CONSTRAINT rights_pkey PRIMARY KEY (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rights
  OWNER TO postgres;

DROP TABLE IF EXISTS roles;
CREATE TABLE roles
(
  id serial NOT NULL,
  name character varying(50) NOT NULL,
  description character varying(250),
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT roles_pkey PRIMARY KEY (id),
  CONSTRAINT roles_name_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE roles
  OWNER TO postgres;

DROP TABLE IF EXISTS role_rights;
CREATE TABLE role_rights
(
  roleid integer NOT NULL,
  rightname character varying NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT role_rights_rightname_fkey FOREIGN KEY (rightname)
      REFERENCES rights (name) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT role_rights_roleid_fkey FOREIGN KEY (roleid)
      REFERENCES roles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_role_right UNIQUE (roleid, rightname)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE role_rights
  OWNER TO postgres;

DROP TABLE IF EXISTS role_assignments;
CREATE TABLE role_assignments
(
  userid integer NOT NULL,
  roleid integer NOT NULL,
  CONSTRAINT role_assignments_roleid_fkey FOREIGN KEY (roleid)
      REFERENCES roles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT role_assignments_userid_fkey FOREIGN KEY (userid)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_role_assignment UNIQUE (userid, roleid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE role_assignments
  OWNER TO postgres;
