INSERT INTO hiv_test_types(
            code, name, displayorder, defaultview)
    VALUES ('VL', 'Viral Load', 1, true),
           ('EID', 'EID', 2, false);

INSERT INTO hiv_testing_reasons(
            code, name, displayorder)
    VALUES ('FIRST', 'First Test', 1),
           ('REPEAT', 'Repeated Test', 2),
           ('FAILURE', 'Suspected Treatment Failure', 3);

INSERT INTO hiv_test_equipments(
            code, name, displayorder)
    VALUES ( 'Roche', 'Roche', 1);

INSERT INTO hiv_equipment_test_sets(
            testtypeid, equipmentid, numberofsamples, numberofcontrols)
    VALUES ((Select id from hiv_test_types where code='VL'), (Select id from hiv_test_equipments where code='Roche'),21, 3),
           ((Select id from hiv_test_types where code='EID'), (Select id from hiv_test_equipments where code='Roche'),22, 2);

INSERT INTO hiv_equipment_test_controls(
           name, code, testsetid)
    VALUES ('eid control 1', 'eid control 1',(Select id from hiv_equipment_test_sets where numberofcontrols=2)),
           ('eid control 2', 'eid control 2',(Select id from hiv_equipment_test_sets where numberofcontrols=2)),
           ('vl control 1', 'vl control 1',(Select id from hiv_equipment_test_sets where numberofcontrols=3)),
           ('vl control 2', 'vl control 2',(Select id from hiv_equipment_test_sets where numberofcontrols=3)),
           ('vl control 3', 'vl control 3',(Select id from hiv_equipment_test_sets where numberofcontrols=3));


INSERT INTO sample_types(
            code, name, displayorder)
    VALUES ('DBS' ,'Dry Blood Sample', 1),
           ('WB' ,'Whole Blood', 2),
           ('PSM' ,'Plasma', 3);


INSERT INTO regimen_groups(
            code,name,displayorder)
    VALUES ('Adult', 'Adult', 1),
           ('Children', 'Children', 2);

INSERT INTO regimen_lines(
            regimengroupid, code, name, displayorder)
    VALUES ((Select id from regimen_groups where code='Adult'),'AFirstLine','First Line', 1),
           ((Select id from regimen_groups where code='Adult'),'ASecondLine','Second Line', 2),
           ((Select id from regimen_groups where code='Children'),'CFirstLine','First Line', 3),
           ((Select id from regimen_groups where code='Children'),'CSecondLine','Second Line', 4);

 INSERT INTO regimens(
             regimenlineid, code, name, displayorder)
     VALUES ((Select id from regimen_lines where code='AFirstLine' ), 'AF', 'TR+CD+SD', 1),
            ((Select id from regimen_lines where code='ASecondLine' ), 'AS', 'ABC+DBSG', 2),
            ((Select id from regimen_lines where code='CFirstLine' ), 'CF', 'SD+FDG', 3),
            ((Select id from regimen_lines where code='CSecondLine' ), 'CS', 'DFR+DCF', 4);

INSERT INTO patient_drug_adherence(
            code, name, displayorder)
    VALUES ('GOOD','Good >= 95%', 1),
           ('FAIR','Fair (85 -94)%', 2),
           ('POOR','Poor < 85%', 3);