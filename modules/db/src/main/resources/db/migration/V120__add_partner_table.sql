 DROP TABLE IF EXISTS PARTNERS;

CREATE TABLE PARTNERS
(
  id serial NOT NULL,
  code character varying(50),
  name character varying(200) NOT NULL,
  CONSTRAINT PARTNERS_pkey PRIMARY KEY (id),
  CONSTRAINT  PARTNERS_code_key UNIQUE (code)
);


INSERT INTO PARTNERS(id,code,name)
VALUES(1,'BA','Boresha Afya');

INSERT INTO PARTNERS(id,code,name)
VALUES(2,'WRD','Water Read');