
DROP TABLE IF EXISTS nacp_requests;
CREATE TABLE nacp_requests
(
  id serial NOT NULL,
  datetimestamp character varying(150),
  versionstamp character varying(150),
  limsdatetimestamp character varying(150),
  limsversionstamp character varying(250),
  requestid character varying(50),
  obrsetid character varying(50),
  loincpanelcode character varying(150),
  limspanelcode character varying(150),
  limspaneldesc character varying(150),
  hl7prioritycode character varying(150),
  specimendatetime character varying(150),
  registereddatetime character varying(150),
  analysisdatetime character varying(150),
  authoriseddatetime character varying(150),
  collectionvolume character varying(150),
  requestingfacilitycode character varying(150),
  receivingfacilitycode character varying(150),
  limspointofcaredesc character varying(150),
  requesttypecode character varying(150),
  icd10clinicalinfocodes character varying(150),
  clinicalinfo character varying(150),
  hl7specimensourcecode character varying(50),
  limsspecimensourcecode character varying(50),
  limsspecimensourcedesc character varying(50),
  hl7specimensitecode character varying(150),
  limsspecimensitecode character varying(150),
  limsspecimensitedesc character varying(150),
  workunits character varying(150),
  costunits character varying(50),
  hl7sectioncode character varying(50),
  hl7resultstatuscode character varying(50),
  registeredby character varying(150),
  testedby character varying(50),
  authorisedby character varying(50),
  orderingnotes character varying(50),
  encryptedpatientid character varying(50),
  ageinyears integer,
  ageindays integer,
  hl7sexcode character varying(150),
  hl7ethnicgroupcode character varying(150),
  deceased character varying(150),
  newborn character varying(150),
  hl7patientclasscode character varying(150),
  attendingdoctor character varying(150),
  testingfacilitycode character varying(150),
  referringrequestid character varying(150),
  therapy character varying(150),
  limsanalyzercode character varying(150),
  targettimedays character varying(150),
  targettimemins character varying(150),
  limsrejectioncode character varying(150),
  limsrejectiondesc character varying(150),
  limsfacilitycode character varying(150),
  repeated character varying(150),
  receiveddatetime character varying,
  admitattenddatetime character varying,
  CONSTRAINT nacp_requests_pkey PRIMARY KEY (id)
)