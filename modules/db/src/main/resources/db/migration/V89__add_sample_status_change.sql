DROP TABLE IF EXISTS hiv_sample_status_changes;
CREATE TABLE hiv_sample_status_changes
(
  id serial NOT NULL,
  sampleId integer NOT NULL,
  status character varying(20) NOT NULL,
  createdby integer NOT NULL,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer NOT NULL,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT hiv_sample_status_changes_pkey PRIMARY KEY (id),
  CONSTRAINT hiv_sample_status_changes_createdby_fkey FOREIGN KEY (createdby)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT hiv_sample_status_changes_modifiedby_fkey FOREIGN KEY (modifiedby)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT hiv_sample_status_changes_sampleid_fkey FOREIGN KEY (sampleId)
      REFERENCES hiv_samples (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);