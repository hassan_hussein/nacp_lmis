﻿DROP TABLE IF EXISTS user_cv_field_value;
DROP TABLE IF EXISTS user_cv;
DROP TABLE IF EXISTS user_cv_type_field_options;
DROP TABLE IF EXISTS user_cv_type_fields;
DROP TABLE IF EXISTS user_cv_types;

CREATE TABLE user_cv_types
(
  id serial NOT NULL,
  name character varying(100),
  CONSTRAINT cv_types_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_cv_types
  OWNER TO postgres;

CREATE TABLE user_cv_type_fields
(
  id serial NOT NULL,
  usercvtypeid integer,
  fieldname character varying(100),
  fieldtype character varying(100),
  CONSTRAINT cv_type_field_pk PRIMARY KEY (id),
  CONSTRAINT cv_type_field_type_fk FOREIGN KEY (usercvtypeid)
      REFERENCES user_cv_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_cv_type_fields
  OWNER TO postgres;



CREATE TABLE user_cv_type_field_options
(
  id serial NOT NULL,
  fieldtypeid integer,
  optionname character varying(100),
  CONSTRAINT user_cv_field_option_pk PRIMARY KEY (id),
  CONSTRAINT cv_field_option_type_fk FOREIGN KEY (fieldtypeid)
      REFERENCES user_cv_type_fields (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_cv_type_field_options
  OWNER TO postgres;


CREATE TABLE user_cv
(
  id serial NOT NULL,
  userid integer,
  cvtypeid integer,
  CONSTRAINT cv_pk PRIMARY KEY (id),
  CONSTRAINT cv_type_fk FOREIGN KEY (cvtypeid)
      REFERENCES user_cv_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_cv
  OWNER TO postgres;


CREATE TABLE user_cv_field_value
(
  id serial NOT NULL,
  usercvid integer NOT NULL,
  usercvfieldid integer NOT NULL,
  fieldvalue text,
  CONSTRAINT cv_field_value_pk PRIMARY KEY (id),
  CONSTRAINT cv_field_fk FOREIGN KEY (usercvfieldid)
      REFERENCES user_cv_type_fields (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cv_value_fk FOREIGN KEY (usercvid)
      REFERENCES user_cv (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_cv_field_value
  OWNER TO postgres;



  




  