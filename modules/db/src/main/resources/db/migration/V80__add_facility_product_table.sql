/*
DROP SEQUENCE  IF EXISTS facility_program_products_id_seq1;

CREATE SEQUENCE facility_program_products_id_seq1
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 819
  CACHE 1;
ALTER TABLE facility_program_products_id_seq1
  OWNER TO postgres;
*/


DROP TABLE IF EXISTS facility_program_products;

CREATE TABLE facility_program_products
(
  id integer NOT NULL,
  facilityid integer NOT NULL,
  programproductid integer NOT NULL,
  isacoefficientsid integer,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT facility_program_products_pkey1 PRIMARY KEY (id),
  CONSTRAINT facility_program_products_facilityid_fkey1 FOREIGN KEY (facilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facility_program_products_programproductid_fkey1 FOREIGN KEY (programproductid)
      REFERENCES program_products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);






DROP TABLE IF EXISTS facility_approved_products;

CREATE TABLE facility_approved_products
(
  id serial NOT NULL,
  facilitytypeid integer NOT NULL,
  programproductid integer NOT NULL,
  maxmonthsofstock integer NOT NULL,
  minmonthsofstock numeric(4,2),
  eop numeric(4,2),
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT facility_approved_products_pkey PRIMARY KEY (id),
  CONSTRAINT facility_approved_products_facilitytypeid_fkey FOREIGN KEY (facilitytypeid)
      REFERENCES facility_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facility_approved_products_programproductid_fkey FOREIGN KEY (programproductid)
      REFERENCES program_products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facility_approved_products_facilitytypeid_programproductid_key UNIQUE (facilitytypeid, programproductid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE facility_approved_products
  OWNER TO postgres;


CREATE INDEX i_facility_approved_product_programproductid_facilitytypeid
  ON facility_approved_products
  USING btree
  (programproductid, facilitytypeid);

