-- Table: supervisory_nodes

-- DROP TABLE supervisory_nodes;

CREATE TABLE supervisory_nodes
(
  id serial NOT NULL,
  parentid integer,
  facilityid integer NOT NULL,
  name character varying(50) NOT NULL,
  code character varying(50) NOT NULL,
  description character varying(250),
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT supervisory_nodes_pkey PRIMARY KEY (id),
  CONSTRAINT supervisory_nodes_facilityid_fkey FOREIGN KEY (facilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT supervisory_nodes_parentid_fkey FOREIGN KEY (parentid)
      REFERENCES supervisory_nodes (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT supervisory_nodes_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE supervisory_nodes
  OWNER TO postgres;

-- Index: i_supervisory_node_parentid

-- DROP INDEX i_supervisory_node_parentid;

CREATE INDEX i_supervisory_node_parentid
  ON supervisory_nodes
  USING btree
  (parentid);

-- Index: uc_supervisory_nodes_lower_code

-- DROP INDEX uc_supervisory_nodes_lower_code;

CREATE UNIQUE INDEX uc_supervisory_nodes_lower_code
  ON supervisory_nodes
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");

