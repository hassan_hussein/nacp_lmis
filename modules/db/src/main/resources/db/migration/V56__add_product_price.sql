-- Table: product_price_schedules

-- DROP TABLE product_price_schedules;

CREATE TABLE product_price_schedules
(
  id serial NOT NULL,
  pricescheduleid integer NOT NULL,
  productid integer NOT NULL,
  price numeric(12,4) NOT NULL DEFAULT 0,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT product_price_schedules_pkey PRIMARY KEY (id),
  CONSTRAINT product_price_schedules_pricescheduleid_fkey FOREIGN KEY (pricescheduleid)
      REFERENCES price_schedules (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_price_schedules_productid_fkey FOREIGN KEY (productid)
      REFERENCES products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_price_schedules_pricescheduleid_productid_key UNIQUE (pricescheduleid, productid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE product_price_schedules
  OWNER TO postgres;
