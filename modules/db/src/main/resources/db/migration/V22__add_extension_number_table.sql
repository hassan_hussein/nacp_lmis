DROP TABLE IF EXISTS extension_numbers;

CREATE TABLE extension_numbers
(
  id serial NOT NULL,
  extensionNumber Integer NOT NULL,
  description character varying(200),
  displayorder integer,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT extension_numbers_pkey PRIMARY KEY (id),
  CONSTRAINT extension_numbers_key UNIQUE (extensionNumber)
)