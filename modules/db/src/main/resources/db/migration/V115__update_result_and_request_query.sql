DROP TABLE IF EXISTS nacp_results;

CREATE TABLE nacp_results
(
  id serial NOT NULL,
  datetimestamp character varying(250),
  versionstamp character varying(250),
  limsdatetimestamp character varying(250),
  limsversionstamp character varying(250),
  requestid character varying(150),
  obrsetid character varying(150) NOT NULL,
  obxsetid character varying(150) NOT NULL,
  obxsubid character varying(150) NOT NULL,
  loinccode character varying(150),
  hl7resulttypecode character varying(150),
  sivalue character varying(150),
  siunits character varying(150),
  silorange character varying(150),
  sihirange character varying(150),
  hl7abnormalflagcodes character varying(150),
  codedvalue character varying(150),
  resultsemiquantitive character varying(150),
  note character varying(150) NOT NULL,
  limsobservationcode character varying(150),
  limsobservationdesc character varying(150),
  limsrptresult character varying(150),
  limsrptunits character varying(150),
  limsrptflag character varying(150),
  limsrptrange character varying(150),
  limscodedvalue character varying(150),
  workunits character varying(150),
  costunits character varying(150),
  CONSTRAINT nacp_results_pkey PRIMARY KEY (id)
);



DROP TABLE IF EXISTS nacp_results;

CREATE TABLE nacp_results
(
  id serial NOT NULL,
  datetimestamp character varying(250),
  versionstamp character varying(250),
  limsdatetimestamp character varying(250),
  limsversionstamp character varying(250),
  requestid character varying(150),
  obrsetid  character varying(150) NOT NULL,
  obxsetid  character varying(150) NOT NULL,
  obxsubid  character varying(150) NOT NULL,
  loinccode character varying(150),
  hl7resulttypecode character varying(150),
  sivalue character varying(150),
  siunits character varying(150),
  silorange character varying(150),
  sihirange character varying(150),
  hl7abnormalflagcodes character varying(150),
  codedvalue character varying(150),
  resultsemiquantitive character varying(150),
  note character varying(250) NOT NULL,
  limsobservationcode character varying(150),
  limsobservationdesc character varying(150),
  limsrptresult character varying(250),
  limsrptunits character varying(250),
  limsrptflag character varying(250),
  limsrptrange character varying(250),
  limscodedvalue character varying(250),
  workunits character varying(250),
  costunits character varying(250),
  CONSTRAINT nacp_results_pkey PRIMARY KEY (id)
)