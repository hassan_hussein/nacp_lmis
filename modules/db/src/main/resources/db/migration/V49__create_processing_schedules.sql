-- Table: processing_schedules

-- DROP TABLE processing_schedules;

CREATE TABLE processing_schedules
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(50) NOT NULL,
  description character varying(250),
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT processing_schedules_pkey PRIMARY KEY (id),
  CONSTRAINT processing_schedules_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE processing_schedules
  OWNER TO postgres;

-- Index: uc_processing_schedules_lower_code

-- DROP INDEX uc_processing_schedules_lower_code;

CREATE UNIQUE INDEX uc_processing_schedules_lower_code
  ON processing_schedules
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");

