DROP TABLE if EXISTS dashboard_sample_types;

CREATE TABLE dashboard_sample_types
(
  id serial NOT NULL,
  ageinyears integer,
  analysisdatetime timestamp without time zone,
  authoriseddatetime timestamp without time zone,
  council character varying(200),
  district character varying(200) ,
  facilitycode character varying(100),
  facilityname character varying(100),
  hl7sexcode character varying(100),
  limsdatetimestamp timestamp without time zone,
  limsrejectioncode character varying(100),
  limsrptunits character varying(100),
  limsspecimensourcecode character varying(100),
  limsspecimensourcedesc character varying(100),
  labnumber character varying(100),
  observationcode character varying(100),
  observationdescription character varying(100),
  receiveddatetime timestamp without time zone,
  region character varying(100),
  registereddatetime timestamp without time zone,
  rejected character varying(100),
  repeated integer,
  specimendatetime timestamp without time zone,
  testresult character varying(100),
  zone character varying(100),
  createddate timestamp without time zone DEFAULT now(),
  monthreceived character varying(100),
  year character varying(100),
  CONSTRAINT dashboard_sample_types_pkey PRIMARY KEY (id)
)
