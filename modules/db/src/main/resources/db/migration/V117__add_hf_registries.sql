
DROP TABLE IF EXISTS hfr_Facilities;

CREATE TABLE hfr_Facilities
(
  id2 serial NOT NULL,
  id CHARACTER VARYING (100),
  MohswID character varying(100),
  FacilityName character varying(100),
  CommonName character varying(100),
  Zone character varying(100),
  Region character varying(100),
  District character varying(100),
  Council character varying(100),
  Ward character varying(100),
  VillageStreet character varying(100),
  FacilityType character varying(100),
  OperatingStatus character varying(100),
  Ownership character varying(100),
  RegistrationNumber character varying(100),
  Latitude character varying(100),
  Longitude character varying(100),
  Location character varying(100),
  CONSTRAINT  hfr_Facilities_pk PRIMARY KEY (id2)
 );