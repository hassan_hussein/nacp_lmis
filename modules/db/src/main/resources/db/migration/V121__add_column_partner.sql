    DO $$
    BEGIN
        BEGIN
            ALTER TABLE users ADD COLUMN partnerId  integer;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column partnerId already exists in users.';
        END;
    END;
$$