ALTER  TABLE  sample_from_hubs ADD COLUMN isReceivedAtLAB BOOLEAN DEFAULT FALSE;
ALTER  TABLE  sample_from_hubs ADD COLUMN dateReceivedAtLab timestamp without time zone;
