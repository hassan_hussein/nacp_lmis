DROP TABLE IF EXISTS facility_operators CASCADE;

CREATE TABLE facility_operators
(
  id serial NOT NULL,
  code character varying NOT NULL,
  text character varying(20),
  displayorder integer,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT facility_operators_pkey PRIMARY KEY (id),
  CONSTRAINT facility_operators_code_key UNIQUE (code)
)