
DO $$
    BEGIN
        BEGIN
            ALTER TABLE inventory_report_line_items ADD COLUMN totallossesandadjustment integer;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column totallossesandadjustments already exists';
        END;
    END;
$$;