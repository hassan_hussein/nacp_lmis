Drop view if exists top_tiles_view;
Create view top_tiles_view as(
Select count(id) as totalClients,
(select count(u.id) from users u join user_groups ug on ug.id=u.groupid where lower(ug.name)=lower('doctor') ) as totalDoctors,
(Select count(id) from consultations where createddate >= (NOW() - interval '7' day)) as thisWeekCalls,
(Select count(id) from consultations where createddate::DATE = NOW()::DATE) as todayCalls
from account_members
);