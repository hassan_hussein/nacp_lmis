DROP table if exists donors;

CREATE TABLE donors
(
  id serial NOT NULL,
  shortname character varying(200) NOT NULL,
  longname character varying(200) NOT NULL,
  code character varying(50),
  geographiczoneid integer,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT donors_pkey PRIMARY KEY (id),
  CONSTRAINT donors_code_key UNIQUE (code),
    CONSTRAINT donors_geographiczoneid_fkey FOREIGN KEY (geographiczoneid)
      REFERENCES geographic_zones (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)