
DROP TABLE IF EXISTS programs_supported;

CREATE TABLE programs_supported
(
  id serial NOT NULL,
  facilityid integer NOT NULL,
  programid integer NOT NULL,
  startdate timestamp without time zone,
  active boolean NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT programs_supported_pkey PRIMARY KEY (id),
  CONSTRAINT programs_supported_facilityid_fkey FOREIGN KEY (facilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT programs_supported_programid_fkey FOREIGN KEY (programid)
      REFERENCES programs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT programs_supported_facilityid_programid_key UNIQUE (facilityid, programid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE programs_supported
  OWNER TO postgres;

-- Index: i_program_supported_facilityid

-- DROP INDEX i_program_supported_facilityid;

CREATE INDEX i_program_supported_facilityid
  ON programs_supported
  USING btree
  (facilityid);







DROP TABLE IF EXISTS facility_approved_products;

CREATE TABLE facility_approved_products
(
  id serial NOT NULL,
  facilitytypeid integer NOT NULL,
  programproductid integer NOT NULL,
  maxmonthsofstock integer NOT NULL,
  minmonthsofstock numeric(4,2),
  eop numeric(4,2),
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT facility_approved_products_pkey PRIMARY KEY (id),
  CONSTRAINT facility_approved_products_facilitytypeid_fkey FOREIGN KEY (facilitytypeid)
      REFERENCES facility_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facility_approved_products_programproductid_fkey FOREIGN KEY (programproductid)
      REFERENCES program_products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facility_approved_products_facilitytypeid_programproductid_key UNIQUE (facilitytypeid, programproductid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE facility_approved_products
  OWNER TO postgres;

-- Index: i_facility_approved_product_programproductid_facilitytypeid

-- DROP INDEX i_facility_approved_product_programproductid_facilitytypeid;

CREATE INDEX i_facility_approved_product_programproductid_facilitytypeid
  ON facility_approved_products
  USING btree
  (programproductid, facilitytypeid);

