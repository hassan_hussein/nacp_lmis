DROP TYPE IF EXISTS vlsamplestatus;
CREATE TYPE vlsamplestatus AS ENUM
   ('REQUEST',
    'REGISTERED',
    'ACCEPTED',
    'REJECTED',
    'RESULTED',
    'TESTED',
    'APPROVED',
    'VERIFIED',
    'DISPATCHED',
    'RECEIVED',
    'INPROGRESS');
ALTER TYPE vlsamplestatus
  OWNER TO postgres;


DROP TABLE IF EXISTS sample_types;
CREATE TABLE sample_types
(
  id serial NOT NULL,
  code character varying,
  name character varying,
  displayorder integer,
  CONSTRAINT sample_types_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sample_types
  OWNER TO postgres;