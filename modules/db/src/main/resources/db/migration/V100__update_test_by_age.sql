DROP TABLE IF EXISTS dashboard_test_by_ages;

CREATE TABLE dashboard_test_by_ages
(
  id serial NOT NULL,
  location character varying(100),
  total integer ,

  lessThanAndEqualTo5 integer ,

  greaterThan5LessThanAndEqualTo15 INTEGER ,
  greaterThan15 integer,
  year integer,
  month integer,
  createddate timestamp without time zone,

  CONSTRAINT dashboard_test_by_ages_pk PRIMARY KEY (id)

)