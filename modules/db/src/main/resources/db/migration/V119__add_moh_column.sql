DO $$
    BEGIN
        BEGIN
            ALTER TABLE facilities ADD COLUMN mohSwId character varying(100);
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column mohSwId already exists in facilities';
        END;
    END;
$$;