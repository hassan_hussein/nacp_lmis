alter table role_assignments add column programId INTEGER REFERENCES programs(id);
alter table role_assignments add column supervisoryNodeId INTEGER REFERENCES supervisory_nodes(id);
