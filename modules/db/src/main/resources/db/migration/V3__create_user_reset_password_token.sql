﻿ DROP TABLE IF EXISTS user_password_reset_tokens;

CREATE TABLE user_password_reset_tokens
(
  userid integer NOT NULL,
  token character varying(250) NOT NULL,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT user_password_reset_tokens_userid_fkey FOREIGN KEY (userid)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT user_password_reset_tokens_userid_token_key UNIQUE (userid, token)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_password_reset_tokens
  OWNER TO postgres;
