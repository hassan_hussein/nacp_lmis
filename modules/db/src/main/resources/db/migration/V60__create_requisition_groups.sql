-- Table: requisition_groups

-- DROP TABLE requisition_groups;

CREATE TABLE requisition_groups
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(50) NOT NULL,
  description character varying(250),
  supervisorynodeid integer,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT requisition_groups_pkey PRIMARY KEY (id),
  CONSTRAINT requisition_groups_supervisorynodeid_fkey FOREIGN KEY (supervisorynodeid)
      REFERENCES supervisory_nodes (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT requisition_groups_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE requisition_groups
  OWNER TO postgres;

-- Index: i_requisition_group_supervisorynodeid

-- DROP INDEX i_requisition_group_supervisorynodeid;

CREATE INDEX i_requisition_group_supervisorynodeid
  ON requisition_groups
  USING btree
  (supervisorynodeid);

-- Index: uc_requisition_groups_lower_code

-- DROP INDEX uc_requisition_groups_lower_code;

CREATE UNIQUE INDEX uc_requisition_groups_lower_code
  ON requisition_groups
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");

