

DROP TABLE IF EXISTS sample_from_hubs;
CREATE TABLE sample_from_hubs
(
  id serial NOT NULL,
orderID character varying(100),
  testCode character varying(100),
  formType character varying(100),
patientCenter character varying(100),
  testReason character varying(100),
  sampleType character varying(100),
sampleRequestTime character varying(100),
  sampleRequesterName character varying(100),
dateSampleTaken timestamp without time zone,
dateSampleSent timestamp without time zone,
timeSampleSent timestamp without time zone,
  phlebotomistName character varying(100),
  dispatcherName character varying(100),
  testComment character varying(100),
dateReceivedLab timestamp without time zone,
orderStatus character varying(100),
updatedAt timestamp without time zone,
createdAt timestamp without time zone,
 patientCode character varying(100),
 testCenter character varying(100),
  hubCenter character varying(100) ,
orderedAt timestamp without time zone,
resultReceivedAt timestamp without time zone,
registerTurnRound timestamp without time zone,
resultTurnRound timestamp without time zone,
batchCode timestamp without time zone,
  transitStatus character varying(100),
onTransitAt character varying(100),
receivedLabAt character varying(100),
  labComment character varying(100),
  registeredBy character varying(100),
originalLab character varying(100),
diseaseName character varying(100),
isTreatmentStarted boolean default false,
isTreatmentCompleted boolean,
treatmentStartedOn boolean,
symptomStatus character varying(100),
drugType character varying(100),
drugName character varying(100),
drugAdherance character varying(100),
userGroup character varying(100),
gender character varying(100),
pcreatedDate timestamp without time zone,
isPregnancy character varying(100),
isBreastFeeding character varying(100),
patientID character varying(100),
pID integer,
patientPhone character varying(100),
patientNursePhone character varying(100),
wardId character varying(100),
birthDate character varying(100)

);

ALTER  TABLE  sample_from_hubs ADD COLUMN createdDate timestamp without time zone;
ALTER  TABLE  sample_from_hubs ADD COLUMN results character varying(100);
ALTER  TABLE  sample_from_hubs ADD COLUMN createdBy INTEGER;

