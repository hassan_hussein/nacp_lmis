 DROP TABLE IF EXISTS nacp_results;

CREATE TABLE nacp_results
(
  id serial NOT NULL,
  datetimestamp character varying(250),
  versionstamp character varying(250),
  limsdatetimestamp character varying(250),
  limsversionstamp character varying(250),
  requestid character varying(150),
  obrsetid character varying(50) NOT NULL,
  obxsetid character varying(50) NOT NULL,
  obxsubid character varying(50) NOT NULL,
  loinccode character varying(150),
  hl7resulttypecode character varying(150),
  sivalue character varying(150),
  siunits character varying(150),
  silorange character varying(150),
  sihirange character varying(150),
  hl7abnormalflagcodes character varying(150),
  codedvalue character varying(150),
  resultsemiquantitive character varying(150),
  note character varying(50) NOT NULL,
  limsobservationcode character varying(50),
  limsobservationdesc character varying(50),
  limsrptresult character varying(50),
  limsrptunits character varying(50),
  limsrptflag character varying(50),
  limsrptrange character varying(50),
  limscodedvalue character varying(50),
  workunits character varying(50),
  costunits character varying(50),
  CONSTRAINT nacp_results_pkey PRIMARY KEY (id)
)