-- Table: requisition_group_program_schedules

DROP TABLE IF EXISTS requisition_group_program_schedules;

CREATE TABLE requisition_group_program_schedules
(
  id serial NOT NULL,
  requisitiongroupid integer NOT NULL,
  programid integer NOT NULL,
  scheduleid integer NOT NULL,
  directdelivery boolean NOT NULL,
  dropofffacilityid integer,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT requisition_group_program_schedules_pkey PRIMARY KEY (id),
  CONSTRAINT requisition_group_program_schedules_dropofffacilityid_fkey FOREIGN KEY (dropofffacilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT requisition_group_program_schedules_programid_fkey FOREIGN KEY (programid)
      REFERENCES programs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT requisition_group_program_schedules_requisitiongroupid_fkey FOREIGN KEY (requisitiongroupid)
      REFERENCES requisition_groups (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT requisition_group_program_schedules_scheduleid_fkey FOREIGN KEY (scheduleid)
      REFERENCES processing_schedules (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT requisition_group_program_sche_requisitiongroupid_programid_key UNIQUE (requisitiongroupid, programid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE requisition_group_program_schedules
  OWNER TO postgres;

-- Index: i_requisition_group_program_schedules_requisitiongroupid

-- DROP INDEX i_requisition_group_program_schedules_requisitiongroupid;

CREATE INDEX i_requisition_group_program_schedules_requisitiongroupid
  ON requisition_group_program_schedules
  USING btree
  (requisitiongroupid);

