-- Table: price_schedules

-- DROP TABLE price_schedules;

CREATE TABLE price_schedules
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  description character varying(500),
  displayorder integer NOT NULL DEFAULT 0,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT price_schedules_pkey PRIMARY KEY (id),
  CONSTRAINT price_schedules_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE price_schedules
  OWNER TO postgres;
