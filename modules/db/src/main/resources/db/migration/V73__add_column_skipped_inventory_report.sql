DO $$
    BEGIN
        BEGIN
            ALTER TABLE inventory_report_line_items ADD COLUMN skipped boolean default false;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column Termination already exists in skipped';
        END;
    END;
$$;