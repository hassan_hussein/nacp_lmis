DROP TABLE IF EXISTS regimen_groups;
CREATE TABLE regimen_groups
(
  id serial NOT NULL,
  name character varying(100),
  code character varying(100),
  displayorder integer,
  CONSTRAINT regimen_group_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE regimen_groups
  OWNER TO postgres;


DROP TABLE IF EXISTS regimen_lines;
CREATE TABLE regimen_lines
(
  id serial NOT NULL,
  regimengroupid integer,
  code character varying,
  name character varying(100),
  displayorder integer,
  CONSTRAINT regimen_line_pk PRIMARY KEY (id),
  CONSTRAINT regimen_group_line_fky FOREIGN KEY (regimengroupid)
      REFERENCES regimen_groups (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE regimen_lines
  OWNER TO postgres;


DROP TABLE IF EXISTS regimens;
CREATE TABLE regimens
(
  id serial NOT NULL,
  regimenlineid integer,
  code character varying(100),
  name character varying(100),
  displayorder integer,
  CONSTRAINT regimen_pk PRIMARY KEY (id),
  CONSTRAINT regimen_line_fk FOREIGN KEY (regimenlineid)
      REFERENCES regimen_lines (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE regimens
  OWNER TO postgres;

DROP TABLE IF EXISTS patient_drug_adherence;
CREATE TABLE patient_drug_adherence
(
  id serial NOT NULL,
  code character varying,
  name character varying(100),
  displayorder integer,
  CONSTRAINT adrug_adherence_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patient_drug_adherence
  OWNER TO postgres;



