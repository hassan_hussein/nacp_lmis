DROP TABLE IF EXISTS facilities CASCADE ;
CREATE TABLE facilities
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(50) NOT NULL,
  description character varying(250),
  gln character varying(30),
  mainphone character varying(20),
  fax character varying(20),
  address1 character varying(50),
  address2 character varying(50),
  geographiczoneid integer NOT NULL,
  typeid integer NOT NULL,
  catchmentpopulation integer,
  latitude numeric(8,5),
  longitude numeric(8,5),
  altitude numeric(8,4),
  operatedbyid integer,
  coldstoragegrosscapacity numeric(8,4),
  coldstoragenetcapacity numeric(8,4),
  suppliesothers boolean,
  sdp boolean NOT NULL,
  online boolean,
  satellite boolean,
  parentfacilityid integer,
  haselectricity boolean,
  haselectronicscc boolean,
  haselectronicdar boolean,
  active boolean NOT NULL,
  golivedate date NOT NULL,
  godowndate date,
  comment text,
  enabled boolean NOT NULL,
  virtualfacility boolean NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  pricescheduleid integer,
  CONSTRAINT facilities_pkey PRIMARY KEY (id),
  CONSTRAINT facilities_geographiczoneid_fkey FOREIGN KEY (geographiczoneid)
      REFERENCES geographic_zones (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facilities_operatedbyid_fkey FOREIGN KEY (operatedbyid)
      REFERENCES facility_operators (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facilities_parentfacilityid_fkey FOREIGN KEY (parentfacilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facilities_pricescheduleid_fkey FOREIGN KEY (pricescheduleid)
      REFERENCES price_schedules (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facilities_typeid_fkey FOREIGN KEY (typeid)
      REFERENCES facility_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT facilities_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE facilities
  OWNER TO postgres;

-- Index: i_facility_name

-- DROP INDEX i_facility_name;

CREATE INDEX i_facility_name
  ON facilities
  USING btree
  (name COLLATE pg_catalog."default");

-- Index: uc_facilities_lower_code

-- DROP INDEX uc_facilities_lower_code;

CREATE UNIQUE INDEX uc_facilities_lower_code
  ON facilities
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");

