﻿DROP TABLE IF EXISTS account_types;

CREATE TABLE account_types
(
  id serial NOT NULL,
  name character varying(250),
  description character varying(200),
  displayorder integer,
  maximummembers integer default -1,
  isorganization boolean default false,
  CONSTRAINT account_types_pkey PRIMARY KEY (id),
  CONSTRAINT account_types_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE account_types
  OWNER TO postgres;


delete from account_types;
insert into account_types(name,description,displayorder,maximummembers,isorganization) 
values ('Family','',1,-1,false),
       ('Organisation','',2,-1,true),
       ('Individual','',3,1,false);

       