﻿DROP TABLE IF EXISTS organizations;

CREATE TABLE organizations
(
  id serial NOT NULL,
  name character(100),
  CONSTRAINT organization_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE organizations
  OWNER TO postgres;

DROP TABLE IF EXISTS accounts;

CREATE TABLE accounts
(
  id serial NOT NULL,
  accountpackageid integer,
  organizationid integer,
  referencenumber character varying(12),
  registrationdate timestamp without time zone,
  renewdate timestamp without time zone NOT NULL,
  active boolean NOT NULL DEFAULT false,
  createdby integer,
  createddate timestamp without time zone,
  modifiedby integer,
  modifieddate timestamp without time zone,
  accounttypeid integer,
  CONSTRAINT account_pk PRIMARY KEY (id),
  CONSTRAINT account_organization_fkey FOREIGN KEY (organizationid)
      REFERENCES organizations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_account_ref UNIQUE (referencenumber)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE accounts
  OWNER TO postgres;


DROP TABLE IF EXISTS account_members;

CREATE TABLE account_members
(
  id serial NOT NULL,
  userid integer NOT NULL,
  accountid integer NOT NULL,
  active boolean NOT NULL DEFAULT true,
  createdby integer,
  createddate timestamp without time zone,
  modifiedby integer,
  modifieddate timestamp without time zone,
  memberid character varying(100),
  isdefault boolean,
  memberpincode character varying(100),
  CONSTRAINT account_members_pk PRIMARY KEY (id),
  CONSTRAINT account_members_fk FOREIGN KEY (accountid)
      REFERENCES accounts (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT account_members_users_fk FOREIGN KEY (userid)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_mamber UNIQUE (memberid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE account_members
  OWNER TO postgres;

DROP TABLE IF EXISTS account_payments;

CREATE TABLE account_payments
(
  id serial NOT NULL,
  accountid integer NOT NULL,
  amount numeric(10,2),
  currency character varying(2),
  paymentdate timestamp without time zone,
  accountpackageid integer,
  expirationdate timestamp without time zone,
  createdby integer,
  createddate timestamp without time zone,
  modifiedby integer,
  modifieddate timestamp without time zone,
  CONSTRAINT account_payments_pk PRIMARY KEY (id),
  CONSTRAINT account_payment_fkey FOREIGN KEY (accountid)
      REFERENCES accounts (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE account_payments
  OWNER TO postgres;



















  