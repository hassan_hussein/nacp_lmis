﻿DROP TABLE IF EXISTS field_types;

CREATE TABLE field_types
(
  name character varying(50) NOT NULL,
  CONSTRAINT field_types_pk PRIMARY KEY (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE field_types
  OWNER TO postgres;


DROP TABLE IF EXISTS call_logs;

CREATE TABLE call_logs
(
  id serial NOT NULL,
  pin character varying(100),
  uniqueid character varying(225),
  CONSTRAINT pin_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE call_logs
  OWNER TO postgres;



DROP TABLE IF EXISTS consultations;

CREATE TABLE consultations
(
  id serial NOT NULL,
  clientid integer,
  doctorid integer,
  consultationdate timestamp without time zone,
  status character varying(100),
  agentid integer,
  createddate timestamp without time zone,
  createdby integer,
  modifieddate timestamp without time zone,
  modifiedby integer,
  CONSTRAINT consultation_pk PRIMARY KEY (id),
  CONSTRAINT client_user_fk FOREIGN KEY (clientid)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT user_doctor_id_fk FOREIGN KEY (doctorid)
      REFERENCES account_members (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE consultations
  OWNER TO postgres;


DROP TABLE IF EXISTS online_users;

CREATE TABLE online_users
(
  id serial NOT NULL,
  userid integer,
  name character varying(100),
  status character varying(20),
  CONSTRAINT user_online_pk PRIMARY KEY (id),
  CONSTRAINT user_online_user_fk FOREIGN KEY (userid)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_user UNIQUE (userid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE online_users
  OWNER TO postgres;


  





















  