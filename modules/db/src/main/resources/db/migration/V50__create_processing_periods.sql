-- Table: processing_periods

-- DROP TABLE processing_periods;

CREATE TABLE processing_periods
(
  id serial NOT NULL,
  scheduleid integer NOT NULL,
  name character varying(50) NOT NULL,
  description character varying(250),
  startdate timestamp without time zone NOT NULL,
  enddate timestamp without time zone NOT NULL,
  numberofmonths integer,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT processing_periods_pkey PRIMARY KEY (id),
  CONSTRAINT processing_periods_scheduleid_fkey FOREIGN KEY (scheduleid)
      REFERENCES processing_schedules (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE processing_periods
  OWNER TO postgres;

-- Index: i_processing_period_startdate_enddate

-- DROP INDEX i_processing_period_startdate_enddate;

CREATE INDEX i_processing_period_startdate_enddate
  ON processing_periods
  USING btree
  (startdate, enddate);

-- Index: uc_processing_period_name_scheduleid

-- DROP INDEX uc_processing_period_name_scheduleid;

CREATE UNIQUE INDEX uc_processing_period_name_scheduleid
  ON processing_periods
  USING btree
  (lower(name::text) COLLATE pg_catalog."default", scheduleid, date_part('year'::text, startdate));

