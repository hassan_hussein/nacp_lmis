DROP TABLE IF EXISTS programs;

CREATE TABLE programs
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(50),
  description character varying(50),
  active boolean,
  templateconfigured boolean,
  regimentemplateconfigured boolean,
  budgetingapplies boolean NOT NULL DEFAULT false,
  usesdar boolean,
  push boolean DEFAULT false,
  sendfeed boolean DEFAULT false,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  isequipmentconfigured boolean DEFAULT false,
  hideskippedproducts boolean NOT NULL DEFAULT false,
  shownonfullsupplytab boolean NOT NULL DEFAULT true,
  enableskipperiod boolean NOT NULL DEFAULT false,
  enableivdform boolean NOT NULL DEFAULT false,
  usepriceschedule boolean NOT NULL DEFAULT false,
  CONSTRAINT programs_pkey PRIMARY KEY (id),
  CONSTRAINT programs_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE programs
  OWNER TO postgres;

-- Index: uc_programs_lower_code

-- DROP INDEX uc_programs_lower_code;

CREATE UNIQUE INDEX uc_programs_lower_code
  ON programs
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");
