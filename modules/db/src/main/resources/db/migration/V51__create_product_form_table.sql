-- Table: product_forms

-- DROP TABLE product_forms;

CREATE TABLE product_forms
(
  id serial NOT NULL,
  code character varying(20),
  displayorder integer,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT product_forms_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE product_forms
  OWNER TO postgres;

-- Index: uc_product_forms_lower_code

-- DROP INDEX uc_product_forms_lower_code;

CREATE UNIQUE INDEX uc_product_forms_lower_code
  ON product_forms
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");

