DROP TABLE IF EXISTS hiv_test_equipments;
CREATE TABLE hiv_test_equipments
(
  id serial NOT NULL,
  code character varying,
  name character varying(100),
  displayorder integer,
  CONSTRAINT vl_equipment_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_test_equipments
  OWNER TO postgres;

DROP TABLE IF EXISTS hiv_test_types;
CREATE TABLE hiv_test_types
(
  id serial NOT NULL,
  code character varying(50),
  name character varying(50),
  displayorder integer,
  defaultview boolean,
  CONSTRAINT test_types PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_test_types
  OWNER TO postgres;

DROP TABLE IF EXISTS hiv_equipment_test_sets;
CREATE TABLE hiv_equipment_test_sets
(
  id serial NOT NULL,
  testtypeid integer,
  equipmentid integer,
  numberofsamples integer,
  numberofcontrols integer,
  CONSTRAINT test_type_equi_pk PRIMARY KEY (id),
  CONSTRAINT equipment_fkey FOREIGN KEY (equipmentid)
      REFERENCES hiv_test_equipments (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT test_type_fkey FOREIGN KEY (testtypeid)
      REFERENCES hiv_test_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_equipment_test_sets
  OWNER TO postgres;


DROP TABLE IF EXISTS hiv_equipment_test_controls;
CREATE TABLE hiv_equipment_test_controls
(
  id serial NOT NULL,
  name character varying(100),
  code character varying(50),
  testsetid integer,
  CONSTRAINT control_pk PRIMARY KEY (id),
  CONSTRAINT test_set_fkey FOREIGN KEY (testsetid)
      REFERENCES hiv_equipment_test_sets (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_equipment_test_controls
  OWNER TO postgres;

DROP TABLE IF EXISTS hiv_testing_reasons;
CREATE TABLE hiv_testing_reasons
(
  id serial NOT NULL,
  code character varying,
  name character varying,
  displayorder integer,
  CONSTRAINT vl_testing_reasons_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_testing_reasons
  OWNER TO postgres;


DROP TABLE IF EXISTS hiv_facility_default_test_types;
CREATE TABLE hiv_facility_default_test_types
(
  facilityid integer,
  testtypeid integer,
  CONSTRAINT facility_test_type_fkey FOREIGN KEY (facilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT test_test_fkey FOREIGN KEY (testtypeid)
      REFERENCES hiv_test_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hiv_facility_default_test_types
  OWNER TO postgres;




