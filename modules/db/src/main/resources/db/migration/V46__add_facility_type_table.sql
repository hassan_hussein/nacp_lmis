DROP TABLE IF EXISTS facility_types CASCADE ;
CREATE TABLE facility_types
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(30) NOT NULL,
  description character varying(250),
  levelid integer,
  nominalmaxmonth integer NOT NULL,
  nominaleop numeric(4,2) NOT NULL,
  displayorder integer,
  active boolean,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT facility_types_pkey PRIMARY KEY (id),
  CONSTRAINT facility_types_code_key UNIQUE (code),
  CONSTRAINT facility_types_name_key UNIQUE (name)
)