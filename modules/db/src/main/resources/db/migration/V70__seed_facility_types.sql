INSERT INTO facility_types(
            code, name, description, levelid, nominalmaxmonth, nominaleop,
            displayorder, active, createddate)
    VALUES ('LAB', 'Laboratory', '', 3, 0, 0,0, true, NOW()),
           ('HUB', 'Hub', '', 3, 0, 0,1, true, NOW()),
           ('HF', 'HealthFacility', '', 3, 0, 0,2, true, NOW());

INSERT INTO facility_operators(
            code, text, displayorder, createddate)
    VALUES ('MOH','MOH', 1, NOW()),
           ('PRIVATE','Private', 2, NOW());


