-- Table: dosage_units

-- DROP TABLE dosage_units;

CREATE TABLE dosage_units
(
  id serial NOT NULL,
  code character varying(20),
  displayorder integer,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT dosage_units_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dosage_units
  OWNER TO postgres;

-- Index: uc_dosage_units_lower_code

-- DROP INDEX uc_dosage_units_lower_code;

CREATE UNIQUE INDEX uc_dosage_units_lower_code
  ON dosage_units
  USING btree
  (lower(code::text) COLLATE pg_catalog."default");

