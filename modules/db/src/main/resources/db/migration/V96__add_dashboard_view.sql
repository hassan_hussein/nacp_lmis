 drop table IF EXISTS tat_view;

CREATE TABLE tat_view
(
  id serial NOT NULL,

  facility character varying(100),
  labNo character varying(100),
  test character varying(100),
  ObservationCode character varying(100),
  ObservationDescription character varying(100),
  result character varying(100),
  SpecimenDate timestamp without time zone,
  ReceivedDate timestamp without time zone,
  RegisteredDate timestamp without time zone,
  ResultedDate timestamp without time zone,
  AuthorisedDate timestamp without time zone,

  CONSTRAINT tat_view_pk PRIMARY KEY (id)

)