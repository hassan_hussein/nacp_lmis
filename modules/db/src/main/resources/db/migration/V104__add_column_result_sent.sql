 DO $$
    BEGIN
        BEGIN
            ALTER TABLE hiv_results ADD COLUMN isResultSent  boolean DEFAULT false;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column isResultSent already exists in hiv_results.';
        END;
    END;
$$