
package com.openldr.sms.service;


import com.openldr.sms.domain.SMS;
import com.openldr.sms.domain.Sms;
import lombok.NoArgsConstructor;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.Future;
@Service
@NoArgsConstructor
public class SMSService {

  private String smsGatewayUrl;
  /* If this is false, we're disallowing the system from sending sms messages */
  private Boolean smsSendingFlag;
  private String sms_gateway_username;
  private String sms_gateway_password;
  @Autowired
  public SMSService(@Value("${sms.gateway.url}") String smsGatewayUrl, @Value("${sms.sending.flag}") Boolean smsSendingFlag,
    @Value("${sms.gateway.username}") String sms_gateway_username,  @Value("${sms.gateway.password}") String sms_gateway_password) {
    this.smsGatewayUrl = smsGatewayUrl;
    this.smsSendingFlag = smsSendingFlag;
    this.sms_gateway_username = sms_gateway_username;
    this.sms_gateway_password = sms_gateway_password;
  }

  public Future<Boolean> send(SMS sms) {
    if (!smsSendingFlag || sms.getSent()) {
      return new AsyncResult<>(true);
    }

    return new AsyncResult<>(SendSMSMessage(sms));
  }

  @Async
  public Future<Boolean> sendAsync(SMS sms) {
    if (!smsSendingFlag || sms.getSent()) {
      return new AsyncResult<>(true);
    }

    return new AsyncResult<>(SendSMSMessage(sms));
  }

  public void ProcessSMS(@Payload List<SMS> smsList) {
    for (SMS sms : smsList) {
        SendSMSMessage(sms);
    }
  }

  public Boolean SendSMSMessage(SMS sms) {

    RestTemplate template = new RestTemplate();

    String auth = sms_gateway_username.concat(":").concat(sms_gateway_password);
    byte[] encodedAuth = Base64.encodeBase64(
            auth.getBytes(Charset.forName("US-ASCII")));
    String authHeader = "Basic "+ new String(encodedAuth);
    Sms _sms = new Sms();
    _sms.setFrom("afyaCall");
    _sms.setTo(sms.getPhoneNumber());
    _sms.setText(sms.getMessage());
    MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
    headers.add("Authorization", authHeader);
    headers.add("Content-Type", "application/json");
    template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
    HttpEntity<Sms> request = new HttpEntity<Sms>(_sms, headers);
    template.postForObject(smsGatewayUrl, request, Sms.class);
    return true;

  }
}
