package com.openldr.sms.exception;

public class SMSException extends RuntimeException {

  public SMSException(String message){
    super(message);
  }

}
