package com.openldr.restapi.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.openldr.authentication.domain.UserToken;
import com.openldr.authentication.service.UserAuthenticationService;
import com.openldr.core.domain.Facility;
import com.openldr.core.domain.User;
import com.openldr.core.service.FacilityService;
import com.openldr.core.service.MessageService;
import com.openldr.core.service.UserService;
import com.openldr.core.utils.HashUtil;
import com.openldr.restapi.domain.LoginInformation;
//import com.openldr.vl.dto.DashBoardSampleTypes;
//import com.openldr.vl.dto.DashboardDTO;
import com.openldr.vl.dto.SampleFromHubDTO;
//import com.openldr.vl.dto.TestByAge;
import com.openldr.vl.service.DashboardService;
import com.openldr.vl.service.SampleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
//import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
//import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class RestLoginService {

    MessageService messageService = MessageService.getRequestInstance();
    @Autowired
    UserAuthenticationService userAuthenticationService;
    @Autowired
    private UserService userService;
    @Autowired
    private FacilityService facilityService;
    @Autowired
    private SampleService sampleService;
    @Autowired
    private DashboardService dashboardService;

    @Value("${hub.url.get.form}")
    private String url;
    @Value("${hub.prepost.url}")
    private String urlToPost;
    @Value("${tatUrl}")
    private String tatUrl;

    private Logger logger = LoggerFactory.getLogger(RestLoginService.class);


    public LoginInformation login(String username, String password) {
        authenticateUser(username, password);
        return getLoginInformation(username);
    }

    private UserToken authenticateUser(String username, String password) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        String userName = (String) authenticationToken.getPrincipal();
        String pass = (String) authenticationToken.getCredentials();

        User user = new User();
        user.setUserName(userName);
        user.setPassword(pass);

        UserToken userToken = userAuthenticationService.authenticateUser(user);

        if (userToken.isAuthenticated()) {
            return userToken;
        } else {
            throw new BadCredentialsException(messageService.message("error.authentication.failed"));
        }
    }

    private LoginInformation getLoginInformation(String username) {
        User user = userService.getByUserName(username);
        Long facilityId = user.getFacilityId();

        if (facilityId != null) {
            Facility facility = facilityService.getById(facilityId);
            return LoginInformation.prepareForREST(user, facility);
        } else {
            return LoginInformation.prepareForREST(user, null);
        }
    }

    public String encode(String stringToBeEncoded) {
        String end = "";
        try {
            end = URLEncoder.encode(stringToBeEncoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return end;

    }


    //End Ubunifu

    /**
     * Fetch spoke samples from Ubunifu's SRS
     */
     @Scheduled(fixedDelay = 180000)
    public void main2() {
        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String ubu_attr = "{\"request_date\":\"" + currentDate + "\"}";
        String method = "GET";
        String privateKey = "T3B2US1sy6WEHjSept4Fc34er45";
        String publicKey = "NACPLAB001563489034";

        String stringToHash = method + privateKey + ubu_attr + publicKey;

        try {

            String checksum = HashUtil.makeSHA256Hash(stringToHash);
            String urlParam = "?key=" + publicKey + "&checksum=" + checksum + "&ubu_attr=" + encode(ubu_attr);
            String url2 = url + "/0/100/" + urlParam;
            System.out.println(url2);

            ObjectMapper mapper = new ObjectMapper();

            URL url = new URL(url2);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;

            while ((output = br.readLine()) != null) {
                logger.info("{}", output);
                SampleFromHubDTO[] value = mapper.readValue(output, SampleFromHubDTO[].class);
                sampleService.syncHubSamples(Arrays.asList(value));
            }
            conn.disconnect();
        } catch (IOException | NoSuchAlgorithmException e) {
            logger.debug("{}",e);
        }
    }
//
//    //  @Scheduled(cron = "${batch.job.sample.tracking}")
//    public void pullTAT() throws URISyntaxException {
//        ObjectMapper mapper = new ObjectMapper(); // just need one
//        // Got a Java class that data maps to nicely? If so:
//        try {
//            URL RT = new URL(tatUrl).toURI().toURL();
//            DashboardDTO[] graph = mapper.readValue(RT, DashboardDTO[].class);
//            dashboardService.saveTAT(Arrays.asList(graph));
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    //@Scheduled(fixedDelay=5000)
    //@Scheduled(cron = "${batch.job.dashboard.by.sample.type}")
//    public void getYearSampleType() throws URISyntaxException {
//        System.out.println((getPreviousYear()));
//        System.out.println(getNextYear());
//        String url = "http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/results?testcode=hivvl&start=" +
//                "2017-01-01&end=2018-01-01";
//
//        ObjectMapper mapper = new ObjectMapper();
//
//        URL RT = null;
//        try {
//            RT = new URL(url).toURI().toURL();
//            DashBoardSampleTypes[] types = mapper.readValue(RT, DashBoardSampleTypes[].class);
//            dashboardService.saveDashboardBySample(Arrays.asList(types));
//
//            System.out.println("reached here");
//            System.out.println(Arrays.toString(types));
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    //@Scheduled(fixedDelay=5000)
//    //@Scheduled(cron = "${batch.job.dashboard.by.age}")
//    public void getGetTestByYear() throws URISyntaxException {
//
//        String url = "http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/testtotalsbyage?testcode=hivvl&start="
//                + "2016-01-01&end=" + getNextYear() + "-01-01";
//
//        ObjectMapper mapper = new ObjectMapper();
//
//        URL RT = null;
//        try {
//            RT = new URL(url).toURI().toURL();
//            TestByAge[] ages = mapper.readValue(RT, TestByAge[].class);
//            dashboardService.saveTestByAge(Arrays.asList(ages));
//
//            System.out.println("reached here");
//            System.out.println(Arrays.toString(ages));
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public List<HashMap<String, Object>> getAllDasboardBySample(String startDate, String endDate) {
        System.out.println(Calendar.getInstance().get(Calendar.YEAR));
        return dashboardService.getAllDasboardBySample(startDate, endDate);
    }

//    public List<DashBoardSampleTypes> getAllDasboardBySample1() {
//
//        return dashboardService.getAllDasboardBySample2();
//    }
}
