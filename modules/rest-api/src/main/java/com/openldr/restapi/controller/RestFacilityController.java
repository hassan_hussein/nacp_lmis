package com.openldr.restapi.controller;


import com.openldr.core.dto.FacilityFeedDTO;
import com.openldr.core.exception.DataException;
import com.openldr.restapi.response.RestResponse;
import com.openldr.restapi.service.RestFacilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * This controller is responsible for handling API endpoint to get facility details.
 */

@Controller
public class RestFacilityController extends BaseController {

  public static final String FACILITY = "facility";
  @Autowired
  RestFacilityService restFacilityService;

  @RequestMapping(value = "/rest-api/facilities/{facilityCode}", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<RestResponse> getFacilityByCode(@PathVariable String facilityCode) {
    FacilityFeedDTO facilityFeedDTO;
    try {
      facilityFeedDTO = restFacilityService.getFacilityByCode(facilityCode);
    } catch (DataException e) {
      return RestResponse.error(e.getOpenLDRMessage(), BAD_REQUEST);
    }
    return RestResponse.response(FACILITY, facilityFeedDTO);
  }
}
