package com.openldr.restapi.service;

import com.openldr.core.domain.Facility;
import com.openldr.core.dto.FacilityFeedDTO;
import com.openldr.core.service.FacilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service exposes methods for get facility details accepting facility code as input.
 */

@Service
public class RestFacilityService {
  @Autowired
  FacilityService facilityService;

  public FacilityFeedDTO getFacilityByCode(String facilityCode) {
    Facility facility = facilityService.getFacilityByCode(facilityCode);
    Facility parentFacility = null;
    if (facility.getParentFacilityId() != null) {
      parentFacility = facilityService.getById(facility.getParentFacilityId());
    }

    return new FacilityFeedDTO(facility, parentFacility);
  }
}
