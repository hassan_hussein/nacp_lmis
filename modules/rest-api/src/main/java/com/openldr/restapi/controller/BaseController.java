

package com.openldr.restapi.controller;


import com.openldr.core.exception.DataException;
import com.openldr.restapi.response.RestResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.security.Principal;

import static com.openldr.restapi.response.RestResponse.error;
import static java.lang.Long.valueOf;
import static org.springframework.http.HttpStatus.*;

/**
 * Controller which is extended by all controllers handling public API requests. Also contains method for handling
 * any exception that is thrown by its child controller, effectively returning error response.
 */

public class BaseController {
  public static final String ACCEPT_JSON = "Accept=application/json";
  public static final String UNEXPECTED_EXCEPTION = "unexpected.exception";
  public static final String FORBIDDEN_EXCEPTION = "error.authorisation";

  @ExceptionHandler(Exception.class)
  public ResponseEntity<RestResponse> handleException(Exception ex) {
    if (ex instanceof AccessDeniedException) {
      return error(FORBIDDEN_EXCEPTION, FORBIDDEN);
    }
    if (ex instanceof MissingServletRequestParameterException || ex instanceof HttpMessageNotReadableException || ex instanceof DataException) {
      return error(ex.getMessage(), BAD_REQUEST);
    }
    return error(UNEXPECTED_EXCEPTION, INTERNAL_SERVER_ERROR);
  }

  public Long loggedInUserId(Principal principal) {
    return valueOf(principal.getName());
  }
}
