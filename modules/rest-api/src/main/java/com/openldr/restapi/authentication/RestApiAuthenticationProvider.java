package com.openldr.restapi.authentication;

import com.openldr.core.domain.User;
import com.openldr.core.service.MessageService;
import com.openldr.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;


import java.util.Collection;

/**
 * This class extends org.springframework.security.authentication.AuthenticationProvider
 * and is responsible for handling authentication for REST API endpoints.
 */

public class RestApiAuthenticationProvider implements AuthenticationProvider {

  @Autowired
  private UserService userService;

  MessageService messageService = MessageService.getRequestInstance();

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    User user = new User();
    user.setUserName(authentication.getPrincipal().toString());
    user.setPassword(authentication.getCredentials().toString());

    User authenticatedUser = userService.selectUserByUserNameAndPassword(user.getUserName(), user.getPassword());

    if (authenticatedUser == null)
      throw new BadCredentialsException(messageService.message("error.authentication.failed"));

    Collection<? extends GrantedAuthority> authorities = null;

    return new UsernamePasswordAuthenticationToken(authenticatedUser.getId(), user.getPassword(),
      authorities);
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
  }
}
