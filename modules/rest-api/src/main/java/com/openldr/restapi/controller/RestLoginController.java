package com.openldr.restapi.controller;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.restapi.domain.LoginInformation;
import com.openldr.restapi.domain.RestLoginRequest;
import com.openldr.restapi.response.RestResponse;
import com.openldr.restapi.service.RestLoginService;
import lombok.NoArgsConstructor;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;

@Controller
@NoArgsConstructor
public class RestLoginController extends BaseController {

    @Autowired
    private RestLoginService restLoginService;

    @RequestMapping(value = "/rest-api/login", method = RequestMethod.POST, headers = ACCEPT_JSON)
    public ResponseEntity<RestResponse> login(@RequestBody RestLoginRequest restLogin) {
        try {
            LoginInformation loginInformation = restLoginService.login(restLogin.getUsername(), restLogin.getPassword());
            return RestResponse.response("userInformation", loginInformation);
        } catch (BadCredentialsException e) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/rest-api/getAllBySampleType", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllBySampleType(HttpServletRequest request,@Param("searchDate") String searchDate) {
        String startDate ="";
        String endDate = "";


        if(searchDate == null){
           /* Calendar currentYear = Calendar.getInstance();
            currentYear.get(Calendar.YEAR);*/
//            Integer currentYear = new Date().getYear();
            startDate = Calendar.getInstance().get(Calendar.YEAR)+"-01-01";
            endDate = Calendar.getInstance().get(Calendar.YEAR) +"-12-31";
        }

        return OpenLdrResponse.response("types", restLoginService.getAllDasboardBySample(startDate,endDate));
    }


}