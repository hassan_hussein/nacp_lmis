

package com.openldr.authentication.web;

import lombok.NoArgsConstructor;
import com.openldr.core.domain.Right;
import com.openldr.core.service.RightService;
import com.openldr.core.service.RoleRightsService;
import com.openldr.core.utils.RightUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Iterables.any;

/**
 * This class is responsible for checking if the user has the given rights.
 */

@Component
@NoArgsConstructor
public class PermissionEvaluator {

  @Autowired
  private RoleRightsService roleRightService;

  @Autowired
  private RightService rightService;

  public Boolean hasPermission(Long userId, String commaSeparatedRights) {
    List<Right> userRights = roleRightService.getRights(userId);
    return any(userRights, RightUtil.contains(getRightNamesList(commaSeparatedRights)));
  }

  public Boolean hasReportingPermission(Long userId) {
    return rightService.hasReportingRight(userId);
  }

  private List<String> getRightNamesList(String commaSeparatedRights) {
    List<String> rights = new ArrayList<>();
    String[] permissions = commaSeparatedRights.split(",");
    for (String permission : permissions) {
      rights.add(permission.trim());
    }
    return rights;
  }
}
