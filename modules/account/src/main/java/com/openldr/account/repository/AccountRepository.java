package com.openldr.account.repository;

import com.openldr.account.domain.*;
import com.openldr.account.repository.mapper.AccountMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@NoArgsConstructor
public class AccountRepository {

    @Autowired
    private AccountMapper mapper;

    public Integer insertAccount(Account account) {
        return mapper.insertAccount(account);
    }

    public Integer insertMember(AccountMember member) {
        return mapper.insertAccountMember(member);
    }

    public Account getByReferenceNumber(String referenceNumber) {
        return mapper.getByReferenceNumber(referenceNumber);
    }

    public void updateAccount(Account account) {
        mapper.updateAccount(account);
    }

    public List<AccountType> getAccountType() {
        return mapper.getAccountType();
    }

    public Integer insertWallet(AccountWallet wallet) {
        return mapper.insertWallet(wallet);
    }

    public Integer updateWallet(AccountWallet wallet) {
        return mapper.updateWallet(wallet);
    }

    public AccountWallet getWalletByAccount(Long accountId) {
        return mapper.getByAccount(accountId);
    }

    public List<AccountMember> getInactiveMembers(Long accountId) {
        return mapper.getInactiveMembers(accountId);
    }

    public void activateMember(AccountMember member) {
        mapper.activateMember(member);
    }

    public List<AccountMember> searchMembers(String query) {
        return mapper.searchMembers(query);
    }

    public void createTransaction(AccountWalletTransaction transaction) {
        mapper.createTransaction(transaction);
    }

    public void updateMember(AccountMember member) {
        mapper.updateMember(member);
    }

    public Map<String, String> getTopTiles() {
        return mapper.getTopTiles();
    }
}
