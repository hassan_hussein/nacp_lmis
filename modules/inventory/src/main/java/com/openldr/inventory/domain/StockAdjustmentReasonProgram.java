package com.openldr.inventory.domain;

import com.openldr.core.domain.BaseModel;
import com.openldr.core.domain.Program;
import com.openldr.upload.Importable;
import com.openldr.upload.annotation.ImportField;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper=false)
public class StockAdjustmentReasonProgram extends BaseModel implements Importable {
  @ImportField(name = "Program Code", type = "String", nested = "code", mandatory = true)
  private Program program;

  @ImportField(name = "Reason Name", type = "String", nested = "name", mandatory = true)
  private StockAdjustmentReason reason;
}
