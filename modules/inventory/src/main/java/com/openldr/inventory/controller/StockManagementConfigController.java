package com.openldr.inventory.controller;


import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.inventory.domain.StockAdjustmentReason;
import com.openldr.inventory.service.StockAdjustmentReasonService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping(value = "/inventory/report/adjustment/")
public class StockManagementConfigController extends BaseController {

  @Autowired
  StockAdjustmentReasonService service;

  @Transactional
  @RequestMapping(value = "adjustmentReasons", method = GET, headers = ACCEPT_JSON)

  public ResponseEntity getAdjustmentReasons(@RequestParam(value = "additive", required = false) Boolean additive,
                                             @RequestParam(value = "programId", required = false) Long programId,
                                             @RequestParam(value = "category", required = false) String categoryStr)
  {
    StockAdjustmentReason.Category category = StockAdjustmentReason.Category.parse(categoryStr);
    if(false == StringUtils.isBlank(categoryStr) && null == category)
      return OpenLdrResponse.error("Category not found", HttpStatus.BAD_REQUEST);

    List<StockAdjustmentReason> reasons = service.getAdjustmentReasons(additive, programId, category);

    if (reasons != null) {
      return OpenLdrResponse.response("adjustmentReasons", reasons);
    } else {
      return OpenLdrResponse.error("Adjustment reasons do not exist.", HttpStatus.NOT_FOUND);
    }
  }
}
