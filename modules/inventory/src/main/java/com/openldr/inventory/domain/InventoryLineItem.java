package com.openldr.inventory.domain;

import com.openldr.core.domain.BaseModel;
import com.openldr.core.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.Predicate;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.collections.CollectionUtils.find;

/**
 * Created by hassan on 10/15/16.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class InventoryLineItem extends BaseModel {

    private Boolean skipped = false;

    private Long reportId;
    private Long productId;
    private String productCode;
    private String productName;
    private String productCategory;

    private Product product;

    private Integer displayOrder;

    private Long openingBalance;

    private Long quantityReceived;

    private Long quantityIssued;

    private Long closingBalance;

    private Long endingBalance;

    private Long daysStockedOut;

    private String remarks;

    private Long discardingReasonId;
    private String discardingReasonExplanation;

    private Long adjustmentReasonId;

    private Integer totalAdjustments;

    private Long quantityRequested;

    private List<InventoryLineItemAdjustmentReason> adjustmentReasons = new ArrayList<>();


    public void calculateTotalLossesAndAdjustments(List<StockAdjustmentReason> stockAdjustmentReasons) {
        if (adjustmentReasons.isEmpty()) {
            return;
        }
        Integer total = 0;

        for (InventoryLineItemAdjustmentReason adjustmentReasonType : adjustmentReasons) {
            if (getAdditive(adjustmentReasonType, stockAdjustmentReasons)) {
                total += adjustmentReasonType.getQuantity();
            } else {
                total -= adjustmentReasonType.getQuantity();
            }
        }
        totalAdjustments = total;
    }


    private Boolean getAdditive(final InventoryLineItemAdjustmentReason adjustmentReasonType,
                                List<StockAdjustmentReason> stockAdjustmentReasons) {
        Predicate predicate = new Predicate() {
            @Override
            public boolean evaluate(Object o) {
                return adjustmentReasonType.getType().getName().equals(((StockAdjustmentReason) o).getName());
            }
        };

        StockAdjustmentReason lossAndAdjustmentTypeFromList = (StockAdjustmentReason) find(stockAdjustmentReasons,
                predicate);

        return lossAndAdjustmentTypeFromList.getAdditive();
    }
}
