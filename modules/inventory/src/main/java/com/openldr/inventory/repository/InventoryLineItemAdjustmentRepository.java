package com.openldr.inventory.repository;

import com.openldr.inventory.domain.InventoryLineItem;
import com.openldr.inventory.domain.InventoryLineItemAdjustmentReason;
import com.openldr.inventory.repository.mapper.InventoryLineItemAdjustmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by hassan on 10/24/16.
 */
@Repository
public class InventoryLineItemAdjustmentRepository {

    @Autowired
    private InventoryLineItemAdjustmentMapper mapper;


    public void insert(InventoryLineItem lineItem, InventoryLineItemAdjustmentReason adjustmentReason){
        //mapper.insert(lineItem,adjustmentReason);
    }


    public void update(InventoryLineItem lineItem, InventoryLineItemAdjustmentReason adjustmentReason) {
      mapper.update(lineItem,adjustmentReason);

    }

    public InventoryLineItemAdjustmentReason getByLineItemAndType(Long lineItemId, String type){
       return mapper.getByLineItemAndType(lineItemId, type);
    }

    public void deleteByLineItemId(Long lineItemId){
        mapper.deleteByLineItemId(lineItemId);
    }
}
