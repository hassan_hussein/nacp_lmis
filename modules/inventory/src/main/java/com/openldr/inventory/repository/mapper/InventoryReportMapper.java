package com.openldr.inventory.repository.mapper;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.ProcessingPeriod;
import com.openldr.inventory.domain.InventoryReport;
import com.openldr.inventory.dto.ReportStatusDTO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 10/15/16.
 */

@Repository
public interface InventoryReportMapper {

    @Insert(" INSERT INTO inventory_reports(  " +
            "             periodId, programId, status, supervisoryNodeId, facilityId, " +
            "            createdBy, createdDate, modifiedBy, modifiedDate, emergency,  " +
            "            reason, orderDate, isVerified) " +
            "    VALUES ( #{periodId}, #{programId}, #{status}, #{supervisoryNodeId}, #{facilityId},  " +
            "            #{createdBy}, NOW(), #{modifiedBy}, NOW(), #{emergency}, " +
            "            #{reason}, #{orderDate}, #{isVerified}) ")
    @Options(useGeneratedKeys = true)
    Integer insert(InventoryReport report);

 @Update(" UPDATE inventory_reports  " +
         "   SET  periodId=#{periodId}, programId=#{programId}, status=#{status}, supervisoryNodeId=#{supervisoryNodeId},  " +
         "       facilityId=#{facilityId}, createdBy=#{createdBy}, createdDate=NOW(), modifiedBy=#{modifiedBy}, modifiedDate=NOW(),   " +
         "       emergency=#{emergency}, reason=#{reason}, orderDate=#{orderDate}, isVerified=#{isVerified}  "+
         " WHERE id = #{id} ")
    void update(InventoryReport report);


    @Select("SELECT * from inventory_reports where id = #{id}")
    InventoryReport getById(@Param("id") Long id);

    @Select("SELECT * from inventory_reports where facilityId = #{facilityId} and programId = #{programId} and periodId = #{periodId}")
    InventoryReport getByPeriodFacilityProgram(@Param("facilityId") Long facilityId, @Param("periodId") Long periodId, @Param("programId") Long programId);

    @Select("SELECT * from inventory_reports where id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "facilityId", column = "facilityId"),
            @Result(property = "periodId", column = "periodId"),
            @Result(property = "programId", column = "programId"),
            @Result(property = "logisticsLineItems", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.inventory.repository.mapper.InventoryLineItemMapper.getLineItems")),
            @Result(property = "reportStatusChanges", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.inventory.repository.mapper.StatusChangeMapper.getChangeLogByReportId")),
            @Result(property = "period", javaType = ProcessingPeriod.class, column = "periodId",
                    many = @Many(select = "com.openldr.core.repository.mapper.ProcessingPeriodMapper.getById")),
            @Result(property = "facility", javaType = Facility.class, column = "facilityId",
                    many = @Many(select = "com.openldr.core.repository.mapper.FacilityMapper.getById"))
    })
    InventoryReport getByIdWithFullDetails(@Param("id") Long id);


    @Select("select max(s.scheduleId) id from requisition_group_program_schedules s " +
            " join requisition_group_members m " +
            "     on m.requisitionGroupId = s.requisitionGroupId " +
            " where " +
            "   s.programId = #{programId} " +
            "   and m.facilityId = #{facilityId} ")
    Long getScheduleFor(@Param("facilityId") Long facilityId, @Param("programId") Long programId);

    @Select("select * from inventory_reports " +
            "   where " +
            "     facilityId = #{facilityId} and programId = #{programId} order by id desc limit 1")
    @Results(value = {
            @Result(property = "periodId", column = "periodId"),
            @Result(property = "period", javaType = ProcessingPeriod.class, column = "periodId", one = @One(select = "com.openldr.core.repository.mapper.ProcessingPeriodMapper.getById"))
    })
    InventoryReport getLastReport(@Param("facilityId") Long facilityId, @Param("programId") Long programId);


    @Select("select * from inventory_reports " +
            "   where " +
            "     facilityId = #{facilityId} and programId = #{programId} and status = 'REJECTED'" +
            "order by id desc")
    @Results(value = {
            @Result(property = "periodId", column = "periodId"),
            @Result(property = "period", javaType = ProcessingPeriod.class, column = "periodId", one = @One(select = "com.openldr.core.repository.mapper.ProcessingPeriodMapper.getById"))
    })
    List<InventoryReport> getRejectedReports(@Param("facilityId") Long facilityId, @Param("programId") Long programId);


    @Select("select r.id, p.name as periodName, r.facilityId, r.status, r.programId " +
            " from inventory_reports r " +
            "   join processing_periods p on p.id = r.periodId " +
            " where r.facilityId = #{facilityId} and r.programId = #{programId}" +
            " order by p.startDate desc")
    List<ReportStatusDTO> getReportedPeriodsForFacility(@Param("facilityId") Long facilityId, @Param("programId") Long programId);

    @Select("Select id from inventory_reports where facilityId = #{facilityId} and periodid = #{periodId}")
    Long getReportIdForFacilityAndPeriod(@Param("facilityId") Long facilityId, @Param("periodId") Long periodId);

    @Select("SELECT id FROM inventory_reports " +
            "WHERE " +
            "periodId < #{periodId} " +
            "AND facilityId = #{facilityId} " +
            "AND programId = #{programId} " +
            "ORDER BY " +
            "periodId DESC limit 1")
    Long findPreviousReport(@Param("facilityId") Long facilityId, @Param("programId") Long programId, @Param("periodId") Long periodId);


 /*   @Select("SELECT " +
            "   r.id, f.name as facilityName, f.code as facilityCode, z.name as districtName, r.status, r.submissionDate, p.startDate periodStartDate, p.endDate periodEndDate, p.name periodName " +
            "from vaccine_reports r " +
            "join processing_periods p on p.id = r.periodId " +
            "join facilities f on f.id = r.facilityId " +
            "join geographic_zones z on z.id = f.geographicZoneId " +
            "where " +
            "r.status = 'SUBMITTED' " +
            "and facilityId = ANY( #{facilityIds}::INT[] )")
    List<RoutineReportDTO> getApprovalPendingReports(@Param("facilityIds") String facilityIds);
}*/

}
