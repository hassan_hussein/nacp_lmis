package com.openldr.inventory.service;

import com.openldr.core.domain.*;
import com.openldr.core.repository.ProcessingPeriodRepository;
import com.openldr.core.service.*;
import com.openldr.inventory.domain.InventoryReport;
import com.openldr.inventory.domain.ReportStatus;
import com.openldr.inventory.domain.ReportStatusChange;
import com.openldr.inventory.dto.ReportStatusDTO;
import com.openldr.inventory.repository.InventoryReportRepository;
import com.openldr.inventory.repository.StatusChangeRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

/**
 * Created by hassan on 10/15/16.
 */

@Service
public class InventoryReportService {
    @Autowired
    InventoryReportRepository repository;

    @Autowired
    StatusChangeRepository reportStatusChangeRepository;

   @Autowired
    ConfigurationSettingService configService;

    @Autowired
    ProgramProductService programProductService;

    @Autowired
    FacilityService facilityService;

    @Autowired
    SupervisoryNodeService supervisoryNodeService;


    @Autowired
    ProcessingPeriodRepository periodService;

    @Autowired
    ProgramService programService;


    @Transactional
    public InventoryReport initialize(Long facilityId, Long programId, Long periodId, Long userId) {
        InventoryReport report = repository.getByProgramPeriod(facilityId, programId, periodId);
        if (report != null) {
            return report;
        }
        report = createNewVaccineReport(facilityId, programId, periodId);
        repository.insert(report);
        ReportStatusChange change = new ReportStatusChange(report, ReportStatus.DRAFT, userId);
        reportStatusChangeRepository.insert(change);
        return report;
    }

    @Transactional
    public void save(InventoryReport report, Long userId) {
        repository.update(report, userId);
    }


    @Transactional
    public void submit(InventoryReport report, Long userId) {
        report.setStatus(ReportStatus.SUBMITTED);
        repository.update(report, userId);
        ReportStatusChange change = new ReportStatusChange(report, ReportStatus.SUBMITTED, userId);
        reportStatusChangeRepository.insert(change);
        //ivdNotificationService.sendIVDStatusChangeNotification(report, userId);
    }

    private InventoryReport createNewVaccineReport(Long facilityId, Long programId, Long periodId) {
        InventoryReport report;

        SimpleDateFormat form = new SimpleDateFormat("MM-dd-YYYY");

        Facility facility = facilityService.getById(facilityId);

        Date date = new Date();
        SupervisoryNode supervisoryNode = supervisoryNodeService.getFor(facilityService.getFacilityById(facilityId), programService.getById(programId));

        List<ProgramProduct> programProducts = programProductService.getActiveByProgram(programId);

        InventoryReport previousReport = this.getPreviousReport(facilityId, programId, periodId);

        report = new InventoryReport();
        report.setFacilityId(facilityId);
        report.setProgramId(programId);
        report.setPeriodId(periodId);
        report.setSupervisoryNodeId(supervisoryNode.getId());
        report.setStatus(ReportStatus.DRAFT);

        if(facility !=null)
            report.setFacility(facility);

        Boolean defaultFieldsToZero = (configService != null)? configService.getBoolValue(ConfigurationSettingKey.DEFAULT_ZERO) : false;
        // 1. copy the products list and initiate the report.
        report.initializeLineItems(programProducts, previousReport, defaultFieldsToZero);
        return report;
    }

    private InventoryReport getPreviousReport(Long facilityId, Long programId, Long periodId) {
        Long reportId = repository.findLastReportBeforePeriod(facilityId, programId, periodId);
        return repository.getByIdWithFullDetails(reportId);
    }

    public List<ReportStatusDTO> getReportedPeriodsFor(Long facilityId, Long programId) {
        return repository.getReportedPeriodsForFacility(facilityId, programId);
    }

    public List<ReportStatusDTO> getPeriodsFor(Long facilityId, Long programId, Date endDate) {
        Date startDate = programService.getProgramStartDate(facilityId, programId);
        System.out.println(startDate);
        System.out.println("start date");
        // find out which schedule this facility is in?
        Long scheduleId = repository.getScheduleFor(facilityId, programId);
        InventoryReport lastRequest = repository.getLastReport(facilityId, programId);

        if (lastRequest != null) {
            lastRequest.setPeriod(periodService.getById(lastRequest.getPeriodId()));
            startDate = lastRequest.getPeriod().getStartDate();
        }

        List<ReportStatusDTO> results = new ArrayList<>();
        Long lastPeriodId = lastRequest == null ? null : lastRequest.getPeriodId();

        List<ProcessingPeriod> periods = periodService.getAllPeriodsForDateRange(scheduleId, startDate, endDate);
        System.out.println("periods");
        System.out.println(periods);

        if (lastRequest != null && lastRequest.getStatus().equals(ReportStatus.DRAFT)) {

           /* List<InventoryReport> rejectedReports = repository.getRejectedReports(facilityId, programId);
            for (InventoryReport rReport : rejectedReports) {
                results.add(createReportStatusDto(facilityId, programId, rReport));
            }*/

           // if (lastRequest.getStatus().equals(ReportStatus.DRAFT)) {
                results.add(createReportStatusDto(facilityId, programId, lastRequest));
            //}
        }


        for (ProcessingPeriod period : emptyIfNull(periods)) {
            if (lastRequest == null || !lastRequest.getPeriodId().equals(period.getId())) {
                ReportStatusDTO reportStatusDTO = new ReportStatusDTO();

                reportStatusDTO.setPeriodName(period.getName());
                reportStatusDTO.setPeriodId(period.getId());
                reportStatusDTO.setProgramId(programId);
                reportStatusDTO.setFacilityId(facilityId);

                reportStatusDTO.setPeriodName(period.getName());
                reportStatusDTO.setPeriodId(period.getId());
                reportStatusDTO.setStatus(ReportStatus.DRAFT.toString());
                reportStatusDTO.setProgramId(programId);
                reportStatusDTO.setFacilityId(facilityId);

                results.add(reportStatusDTO);
            }
        }
        return results;
    }

    private static ReportStatusDTO createReportStatusDto(Long facilityId, Long programId, InventoryReport report) {
        ReportStatusDTO reportStatusDTO = new ReportStatusDTO();
        reportStatusDTO.setPeriodName(report.getPeriod().getName());
        reportStatusDTO.setPeriodId(report.getPeriod().getId());
        reportStatusDTO.setStatus(report.getStatus().toString());
        reportStatusDTO.setProgramId(programId);
        reportStatusDTO.setFacilityId(facilityId);
        reportStatusDTO.setId(report.getId());
        return reportStatusDTO;
    }

    public InventoryReport getById(Long id) {
        InventoryReport report = repository.getByIdWithFullDetails(id);
        //report.setTabVisibilitySettings(tabVisibilityService.getVisibilityForProgram(report.getProgramId()));
        DateTime periodStartDate = new DateTime(report.getPeriod().getStartDate());
        //report.setFacilityDemographicEstimates(annualFacilityDemographicEstimateService.getEstimateValuesForFacility(report.getFacilityId(), report.getProgramId(), periodStartDate.getYear()));
        return report;
    }

    public Long getReportIdForFacilityAndPeriod(Long facilityId, Long periodId) {
        return repository.getReportIdForFacilityAndPeriod(facilityId, periodId);
    }

    public void reject(InventoryReport report, Long userId) {
        report.setStatus(ReportStatus.REJECTED);
        Long reportSubmitterUserId = getReportSubmitterUserId(report.getId());
        repository.update(report, userId);
        ReportStatusChange change = new ReportStatusChange(report, ReportStatus.REJECTED, userId, null);
        reportStatusChangeRepository.insert(change);
       // ivdNotificationService.sendIVDStatusChangeNotification(report, reportSubmitterUserId);
    }

    private Long getReportSubmitterUserId(Long reportId){
        ReportStatusChange change = reportStatusChangeRepository.getOperation(reportId, ReportStatus.SUBMITTED);
        return (change != null) ? change.getCreatedBy(): null;
    }
    public void approve(InventoryReport report, Long userId) {
        report.setStatus(ReportStatus.APPROVED);
        Long reportSubmitterUserId = getReportSubmitterUserId(report.getId());
        repository.update(report, userId);
        ReportStatusChange change = new ReportStatusChange(report, ReportStatus.APPROVED, userId);
        reportStatusChangeRepository.insert(change);
       // ivdNotificationService.sendIVDStatusChangeNotification(report, reportSubmitterUserId);
    }
}
