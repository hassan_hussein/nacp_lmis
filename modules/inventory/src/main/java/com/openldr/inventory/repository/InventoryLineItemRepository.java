package com.openldr.inventory.repository;

import com.openldr.inventory.domain.InventoryLineItem;
import com.openldr.inventory.repository.mapper.InventoryLineItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by hassan on 10/15/16.
 */
@Component
public class InventoryLineItemRepository {

    @Autowired
    InventoryLineItemMapper mapper;

    public void insert(InventoryLineItem item) {
        mapper.insert(item);
    }

    public void update(InventoryLineItem item) {
        mapper.update(item);
    }

    public InventoryLineItem getById(Long id){
        return mapper.getById(id);
    };


  /*  public InventoryLineItem getApprovedLineItemFor(String programCode, String productCode, String facilityCode, Long periodId){
        return mapper.getApprovedLineItemFor(programCode, productCode,facilityCode, periodId);
    }*/

  /*  public List<InventoryLineItem> getUpTo3PreviousPeriodLineItemsFor(String programCode, String productCode, String facilityCode, Long periodId){
        return mapper.getUpTo3PreviousPeriodLineItemsFor(programCode, productCode,facilityCode, periodId);
    }

    public List<LogisticsLineItem> getApprovedLineItemListFor(String programCode, String facilityCode,  Long periodId) {
        return mapper.getApprovedLineItemListFor(programCode, facilityCode, periodId);
        }
*/

}
