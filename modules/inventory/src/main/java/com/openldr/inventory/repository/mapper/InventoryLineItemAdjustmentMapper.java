package com.openldr.inventory.repository.mapper;

import com.openldr.inventory.domain.InventoryLineItem;
import com.openldr.inventory.domain.InventoryLineItemAdjustmentReason;
import com.openldr.inventory.domain.StockAdjustmentReason;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 10/24/16.
 */

@Repository
public interface InventoryLineItemAdjustmentMapper {


    @Insert("INSERT INTO inventory_line_item_adjustments(lineItemId, type, quantity, modifiedBy) " +
            "VALUES(#{lineItem.id}, #{lossesAndAdjustment.type.name}, #{lossesAndAdjustment.quantity}, #{lineItem.modifiedBy})")
    public Integer insert(@Param(value = "lineItem") InventoryLineItem lineItem, @Param(value = "lossesAndAdjustment") InventoryLineItemAdjustmentReason lossesAndAdjustment);


    @Select("select * from inventory_line_item_adjustments where lineItemId = #{lineItemId}")
    @Results(value = {
            @Result(property = "type", column = "type", javaType = String.class, one = @One(select = "getLossesAndAdjustmentTypeByName"))
    })
    List<InventoryLineItemAdjustmentReason> getByLineItem(Long lineItemId);


    @Select("SELECT * FROM losses_adjustments_types WHERE name = #{lossesAndAdjustmentsTypeName}")
    StockAdjustmentReason getLossesAndAdjustmentTypeByName(String lossesAndAdjustmentsTypeName);

    @Update(" UPDATE inventory_line_item_adjustments  " +
            "   SET quantity=#{adjustmentReason.quantity}, " +
            "       modifieddate=NOW() "   +
            " WHERE lineitemid = #{lineItem.id} and type= #{adjustmentReason.type.name}  ")
    void update(@Param(value = "lineItem") InventoryLineItem lineItem, @Param(value = "adjustmentReason") InventoryLineItemAdjustmentReason adjustmentReason);

    @Select(" select * from inventory_line_item_adjustments where lineItemId = #{lineItemId} and type=#{type}")
   InventoryLineItemAdjustmentReason getByLineItemAndType(@Param("lineItemId")Long lineItemId,@Param("type") String type);

    @Delete("DELETE FROM inventory_line_item_adjustments WHERE lineItemId = #{lineItemId}")
    void deleteByLineItemId(Long lineItemId);
}
