package com.openldr.inventory.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.openldr.core.domain.*;
import com.openldr.core.serializer.DateDeserializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.*;



@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class InventoryReport extends BaseModel {

    public static final SimpleDateFormat form = new SimpleDateFormat("MM-dd-YYYY");
    private Long periodId;
    private Long programId;
    private ReportStatus status;
    private Long supervisoryNodeId;
    private Facility facility;
    private ProcessingPeriod period;
    private Long facilityId;
    private Program program;

    @JsonDeserialize(using = DateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date orderDate;
    private boolean emergency;
    private String reason;
    private boolean isVerified;
    private List<InventoryLineItem> logisticsLineItems;
    private List<ReportStatusChange> reportStatusChanges;

    public void initializeLineItems(List<ProgramProduct> programProducts, InventoryReport previousReport, Boolean defaultFieldsToZero) {
        logisticsLineItems = new ArrayList<>();
        Map<String, InventoryLineItem> previousLineItemMap = new HashMap<>();
        if (previousReport != null) {
            for (InventoryLineItem lineItem : previousReport.getLogisticsLineItems()) {
                previousLineItemMap.put(lineItem.getProductCode(), lineItem);
            }
        }
        for (ProgramProduct pp : programProducts) {
            InventoryLineItem item = new InventoryLineItem();

            item.setReportId(id);

            item.setProductId(pp.getProduct().getId());
            item.setProductCode(pp.getProduct().getCode());
            item.setProductName(pp.getProduct().getPrimaryName());
            item.setProductCategory(pp.getProductCategory().getName());
            item.setDisplayOrder(pp.getDisplayOrder());

            if (previousReport != null) {
                InventoryLineItem lineItem = previousLineItemMap.get(item.getProductCode());
                if (lineItem != null) {
                    item.setOpeningBalance(lineItem.getClosingBalance());
                    item.setClosingBalance(item.getOpeningBalance());
                }
            }
            if(defaultFieldsToZero){
                if(item.getOpeningBalance() == null){
                    item.setOpeningBalance(0L);
                }
                item.setClosingBalance(item.getOpeningBalance());
                item.setQuantityReceived(0L);
                item.setQuantityIssued(0L);
                item.setDaysStockedOut(0L);
            }

            logisticsLineItems.add(item);
        }

    }

}
