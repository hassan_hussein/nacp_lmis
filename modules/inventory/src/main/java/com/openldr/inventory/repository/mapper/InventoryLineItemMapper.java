package com.openldr.inventory.repository.mapper;

import com.openldr.core.domain.Product;
import com.openldr.inventory.domain.InventoryLineItem;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 10/15/16.
 */
@Repository
public interface InventoryLineItemMapper {


    @Insert("INSERT INTO inventory_report_line_items " +
            " (reportId, productId, productCode, productName, productCategory, displayOrder, openingBalance, quantityReceived, quantityIssued, closingBalance, daysStockedOut , adjustmentReasonId, discardingReasonExplanation, remarks, createdBy, createdDate, modifiedBy, modifiedDate,totalAdjustments,quantityRequested) " +
            " values " +
            " (#{reportId}, #{productId}, #{productCode}, #{productName}, #{productCategory} , #{displayOrder}, #{openingBalance}, #{quantityReceived}, #{quantityIssued}, #{closingBalance}, #{daysStockedOut} , #{adjustmentReasonId}, #{discardingReasonExplanation}, #{remarks}, #{createdBy}, NOW(), #{modifiedBy}, NOW(),#{totalAdjustments},#{quantityRequested})")
    @Options(useGeneratedKeys = true)
    Integer insert(InventoryLineItem lineItem);

    @Update("UPDATE inventory_report_line_items " +
            " set " +
            " reportId = #{reportId} " +
            ", skipped = #{skipped}" +
            ", productId = #{productId} " +
            ", productCode = #{productCode} " +
            ", productCategory = #{productCategory} " +
            ", productName = #{productName} " +
            ", displayOrder = #{displayOrder} " +
            ", openingBalance = #{openingBalance} " +
            ", quantityReceived = #{quantityReceived} " +
            ", daysStockedOut = #{daysStockedOut} " +
            ", discardingReasonExplanation = #{discardingReasonExplanation} " +
            ", adjustmentReasonId = #{adjustmentReasonId} " +
            ", remarks = #{remarks} " +
            ", quantityIssued = #{quantityIssued} " +
            ", closingBalance = #{closingBalance} " +
            ", modifiedBy = #{modifiedBy} " +
            ", modifiedDate = NOW() " +
            ", totalAdjustments = #{totalAdjustments} " +
            ", quantityRequested = #{quantityRequested}" +
            "WHERE id = #{id} ")
    void update(InventoryLineItem lineItem);

    @Select("select * from inventory_report_line_items  where reportId = #{reportId} order by id")
    @Results(value = {
            @Result(property = "productId", column = "productId"),
            @Result(property = "product", column = "productId", javaType = Product.class,
                    one = @One(select = "com.openldr.core.repository.mapper.ProductMapper.getById")),
            @Result(property = "adjustmentReasons", column = "id", javaType = List.class,
                    one = @One(select = "com.openldr.inventory.repository.mapper.InventoryLineItemAdjustmentMapper.getByLineItem"))
    })
    List<InventoryLineItem> getLineItems(@Param("reportId") Long reportId);

   @Select("select * from inventory_report_line_items  WHERE ID = #{id}")
    InventoryLineItem getById(@Param("id") Long id);

}
