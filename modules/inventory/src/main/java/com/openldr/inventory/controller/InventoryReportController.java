package com.openldr.inventory.controller;

import com.openldr.core.service.FacilityService;
import com.openldr.core.service.ProgramService;
import com.openldr.core.service.UserService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.inventory.domain.InventoryReport;
import com.openldr.inventory.service.InventoryReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

import static com.openldr.core.web.OpenLdrResponse.response;

/**
 * Created by hassan on 10/16/16.
 */

@Controller
@RequestMapping(value = "/inventory/report/")
public class InventoryReportController extends BaseController{

    private static final String PERIODS = "periods";
    private static final String REPORT = "report";
    private static final String PENDING_SUBMISSIONS = "pending_submissions";

    @Autowired
    InventoryReportService service;

    @Autowired
    ProgramService programService;

    @Autowired
    UserService userService;

    @Autowired
    FacilityService facilityService;


    @RequestMapping(value = "periods/{facilityId}/{programId}", method = RequestMethod.GET)
   // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'CREATE_REPORT')")
    public ResponseEntity<OpenLdrResponse> getPeriods(@PathVariable Long facilityId, @PathVariable Long programId) {
        return response(PERIODS, service.getPeriodsFor(facilityId, programId, new Date()));
    }

    @RequestMapping(value = "view-periods/{facilityId}/{programId}", method = RequestMethod.GET)
  //  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'VIEW_REPORT')")
    public ResponseEntity<OpenLdrResponse> getViewPeriods(@PathVariable Long facilityId, @PathVariable Long programId) {
        return response(PERIODS, service.getReportedPeriodsFor(facilityId, programId));
    }

    @RequestMapping(value = "initialize/{facilityId}/{programId}/{periodId}")
   // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'CREATE_REPORT')")
    public ResponseEntity<OpenLdrResponse> initialize(
            @PathVariable Long facilityId,
            @PathVariable Long programId,
            @PathVariable Long periodId,
            HttpServletRequest request
    ) {
        return response(REPORT, service.initialize(facilityId, programId, periodId, loggedInUserId(request)));
    }

    @RequestMapping(value = "get/{id}.json", method = RequestMethod.GET)
   // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'CREATE_REPORT, VIEW_REPORT, APPROVE_REPORT')")
    public ResponseEntity<OpenLdrResponse> getReport(@PathVariable Long id) {
        return response(REPORT, service.getById(id));
    }

    @RequestMapping(value = "save")
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'CREATE_REPORT')")
    public ResponseEntity<OpenLdrResponse> save(@RequestBody InventoryReport report, HttpServletRequest request) {
        service.save(report, loggedInUserId(request));
        return response(REPORT, report);
    }

    @RequestMapping(value = "submit")
   // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'CREATE_REPORT')")
    public ResponseEntity<OpenLdrResponse> submit(@RequestBody InventoryReport report, HttpServletRequest request) {
        service.submit(report, loggedInUserId(request));
        return response(REPORT, report);
    }

  /*  @RequestMapping(value = "approval-pending")
    @PreAuthorize("@permissionEvaluator.hasPermission(principal,'APPROVE_IVD')")
    public ResponseEntity<OpenLdrResponse> getPendingFormsForApproval(@RequestParam("program") Long programId, HttpServletRequest request){
        return OpenLdrResponse.response(PENDING_SUBMISSIONS, service.getApprovalPendingForms(this.loggedInUserId(request), programId));
    }*/

    @RequestMapping(value = "approve")
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'APPROVE_REPORT')")
    public ResponseEntity<OpenLdrResponse> approve(@RequestBody InventoryReport report, HttpServletRequest request) {
        service.approve(report, loggedInUserId(request));
        return response(REPORT, report);
    }

    @RequestMapping(value = "reject")
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'APPROVE_REPORT')")
    public ResponseEntity<OpenLdrResponse> reject(@RequestBody InventoryReport report, HttpServletRequest request) {
        service.reject(report, loggedInUserId(request));
        return response(REPORT, report);
    }

    @RequestMapping(value = "userHomeFacility.json", method = RequestMethod.GET)
    public ResponseEntity<OpenLdrResponse> getUserHomeFacilities(HttpServletRequest request) {
        return response("homeFacility", facilityService.getHomeFacility(loggedInUserId(request)));
    }

}
