package com.openldr.inventory.service;

import com.openldr.inventory.domain.InventoryLineItem;
import com.openldr.inventory.domain.InventoryLineItemAdjustmentReason;
import com.openldr.inventory.repository.InventoryLineItemAdjustmentRepository;
import com.openldr.inventory.repository.InventoryLineItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

/**
 * Created by hassan on 10/15/16.
 */
@Service
public class InventoryLineItemService {

    @Autowired
    private InventoryLineItemRepository repository;

    @Autowired
    private InventoryLineItemAdjustmentRepository adjustmentRepository;

    @Transactional
    public void saveInventoryLineItems(List<InventoryLineItem> lineItems, Long reportId) {
        for (InventoryLineItem lineItem : emptyIfNull(lineItems)) {

         // InventoryLineItem existing = repository.getById(lineItem.getId());

            if (!lineItem.hasId()) {
                System.out.println("Entered to LineItems");
                System.out.println(lineItem.getId());
                lineItem.setReportId(reportId);
                lineItem.setDisplayOrder(1);
                repository.insert(lineItem);
            }


        }
    }


    public void saveAdjustmentReasonDetails(InventoryLineItem lineItem){
        System.out.println(lineItem.getId());
        for(InventoryLineItemAdjustmentReason adjustmentReason:lineItem.getAdjustmentReasons()){
             // if(lineItem.hasId())
              adjustmentRepository.insert(lineItem,adjustmentReason);
             /*  else
                  adjustmentRepository.update(lineItem,adjustmentReason);*/

       }

    }


    public void updateInventoryLineItems(List<InventoryLineItem> lineItems, Long reportId) {

        for (InventoryLineItem lineItem : emptyIfNull(lineItems)) {
            System.out.println("Updating lineItems");
            System.out.println(lineItem.getId());
            repository.update(lineItem);
            adjustmentRepository.deleteByLineItemId(lineItem.getId());
            saveAdjustmentReasonDetails(lineItem);
        }

    }
}
