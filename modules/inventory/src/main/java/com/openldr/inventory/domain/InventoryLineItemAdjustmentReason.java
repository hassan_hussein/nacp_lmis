package com.openldr.inventory.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by hassan on 10/24/16.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InventoryLineItemAdjustmentReason{

    private StockAdjustmentReason type;

    private Integer quantity;


}
