package com.openldr.inventory.repository;

import com.openldr.core.exception.DataException;
import com.openldr.core.service.GeographicZoneService;
import com.openldr.inventory.domain.InventoryReport;
import com.openldr.inventory.dto.ReportStatusDTO;
import com.openldr.inventory.repository.mapper.InventoryReportMapper;
import com.openldr.inventory.service.InventoryLineItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class InventoryReportRepository{

    @Autowired
    private InventoryReportMapper mapper;

    @Autowired
    GeographicZoneService geographicZoneService;

    @Autowired
    InventoryLineItemService lineItemService;


    public void insert(InventoryReport report) {
        try {

            report.setSupervisoryNodeId(2L);
            report.setCreatedBy(1L);
            report.setOrderDate(new Date());
            mapper.insert(report);
            saveDetails(report);
        }catch (DataException e){
            e.getMessage();
        }
    }

    public void saveDetails(InventoryReport report) {
        lineItemService.saveInventoryLineItems(report.getLogisticsLineItems(), report.getId());
    }

    public void updateDetails(InventoryReport report){
        lineItemService.updateInventoryLineItems(report.getLogisticsLineItems(), report.getId());
    }

    public void update(InventoryReport report, Long userId) {
        report.setModifiedBy(userId);
        mapper.update(report);
        updateDetails(report);
    }

    public InventoryReport getById(Long id) {
        return mapper.getById(id);
    }

    public InventoryReport getByIdWithFullDetails(Long id) {
        return mapper.getByIdWithFullDetails(id);
    }

    public InventoryReport getByProgramPeriod(Long facilityId, Long programId, Long periodId) {
        return mapper.getByPeriodFacilityProgram(facilityId, programId, periodId);
    }

    public InventoryReport getLastReport(Long facilityId, Long programId) {
        return mapper.getLastReport(facilityId, programId);
    }

    public Long getScheduleFor(Long facilityId, Long programId) {
        return mapper.getScheduleFor(facilityId, programId);
    }

    public List<ReportStatusDTO> getReportedPeriodsForFacility(Long facilityId, Long programId) {
        return mapper.getReportedPeriodsForFacility(facilityId, programId);
    }

    public Long getReportIdForFacilityAndPeriod(Long facilityId, Long periodId) {
        return mapper.getReportIdForFacilityAndPeriod(facilityId, periodId);
    }


    public Long findLastReportBeforePeriod(Long facilityId, Long programId, Long periodId) {
        return mapper.findPreviousReport(facilityId, programId, periodId);
    }

/*    public List<RoutineReportDTO> getApprovalPendingForms(String facilityIds) {
        return mapper.getApprovalPendingReports(facilityIds);
    }*/

    public List<InventoryReport> getRejectedReports(Long facilityId, Long programId) {
        return mapper.getRejectedReports(facilityId, programId);
    }


}

