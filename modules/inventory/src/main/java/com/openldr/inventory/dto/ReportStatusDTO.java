package com.openldr.inventory.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by hassan on 10/15/16.
 */
@Getter
@Setter
public class ReportStatusDTO {
    Long id;
    Long periodId;
    Long programId;
    Long facilityId;
    String status;
    String periodName;
}
