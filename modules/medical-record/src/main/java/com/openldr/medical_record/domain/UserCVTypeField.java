

package com.openldr.medical_record.domain;

import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Role represents Role entity which is a set of rights. Also provides methods to validate if a role contains related rights.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserCVTypeField extends BaseModel {

    private Long userCVTypeId;
    private String fieldName;
    private String fieldType;

}
