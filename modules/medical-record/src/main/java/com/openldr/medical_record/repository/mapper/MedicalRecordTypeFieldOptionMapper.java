/*
 * Electronic Logistics Management Information System (eLMIS) is a supply chain management system for health commodities in a developing country setting.
 *
 * Copyright (C) 2015  John Snow, Inc (JSI). This program was produced for the U.S. Agency for International Development (USAID). It was prepared under the USAID | DELIVER PROJECT, Task Order 4.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.openldr.medical_record.repository.mapper;

import com.openldr.medical_record.domain.MedicalRecordTypeField;
import com.openldr.medical_record.domain.MedicalRecordTypeFieldOption;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicalRecordTypeFieldOptionMapper {

    @Insert({"INSERT INTO medical_record_type_field_options(fieldtypeid, optionname) " +
            "values(#{medicalRecordTypeId},#{fieldTypeId},#{optionName})"})
    @Options(useGeneratedKeys = true)
    Integer insert(MedicalRecordTypeFieldOption medicalRecordTypeFieldOption);

    @Update("UPDATE medical_record_type_field_options SET optionname=#{optionName} WHERE id=#{id}")
    Integer update(MedicalRecordTypeFieldOption medicalRecordTypeFieldOption);

    @Select("Select o.* from medical_record_type_field_options o " +
            "join medical_report_type_field f on f.id=o.fieldtypeid where o.fieldtypeid=#{fieldtypeid} ")
    MedicalRecordTypeFieldOption getByFieldId(@Param("fieldtypeid") Long fieldtypeid);

    @Select("Select o.* from medical_record_type_field_options o where o.id=#{id}")
    MedicalRecordTypeField getById(@Param("id") Long id);

}
