package com.openldr.medical_record.repository.mapper;

import com.openldr.core.domain.User;
import com.openldr.medical_record.domain.Consultation;
import com.openldr.medical_record.domain.MedicalRecord;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalRecordMapper {

    @Insert({"INSERT INTO medical_records(userid,recordtypeid,consultationId) values(#{userId},#{recordTypeId},#{consultationId})"})
    @Options(useGeneratedKeys = true)
    Integer insert(MedicalRecord medicalRecord);

    @Update("UPDATE medical_records SET userid=#{userId},recordtypeid=#{recordTypeId} WHERE id=#{id}")
    Integer update(MedicalRecord medicalRecord);

    @Select("Select * from medical_records where id=#{id} ")
    MedicalRecord getById(@Param("id") Long id);

    @Select("Select * from medical_records where userid=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "fieldValues", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.MedicalRecordFieldValueMapper.getByMedicalRecordId"))
    })
    List<MedicalRecord> getByUserId(@Param("userId") Long userId);

    @Select("Select r.* from medical_records r " +
            " join medical_record_types t on t.id=r.recordtypeid " +
            " where (r.userid=(select clientId from consultations where id=#{consultationId} LIMIT 1)) and " +
            "  not ( r.consultationid = #{consultationId} and  t.code = 'CONSULTATION_FORM')")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "consultationId", column = "consultationId"),
            @Result(property = "doctor", column = "consultationId", javaType = User.class,
                    one = @One(select = "getDoctorByConsultationId")),
            @Result(property = "consultation", column = "consultationId", javaType = Consultation.class,
                    one = @One(select = "com.openldr.medical_record.repository.mapper.ConsultationMapper.getByIdLess")),
            @Result(property = "fieldValues", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.MedicalRecordFieldValueMapper.getByMedicalRecordId"))
    })
    List<MedicalRecord> getHistory(@Param("consultationId") Long consultationId);

    @Select("Select r.* from medical_records r " +
            " join medical_record_types t on t.id=r.recordtypeid " +
            " where consultationid=#{consultationId} and t.code = 'CONSULTATION_FORM'")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "fieldValues", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.MedicalRecordFieldValueMapper.getByMedicalRecordId"))
    })
    List<MedicalRecord> getByConsultationId(@Param("consultationId") Long consultationId);

    @Select("Select * from medical_records where userid=#{userId} and recordtypeid=#{recordTypeId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "fieldValues", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.MedicalRecordFieldValueMapper.getByMedicalRecordId"))
    })
    List<MedicalRecord> getByUserAndType(@Param("userId") Long userId, @Param("recordTypeId") Long recordTypeId);

    @Select("Select u.* from users u join consultations c on c.doctorid=u.id where c.id=#{consultationId} LIMIT 1")
    User getDoctorByConsultationId(@Param("consultationId") Long consultationId);
}
