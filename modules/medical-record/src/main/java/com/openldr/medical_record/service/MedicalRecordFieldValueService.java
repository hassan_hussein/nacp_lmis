package com.openldr.medical_record.service;

import com.openldr.medical_record.domain.MedicalRecordFieldValue;
import com.openldr.medical_record.repository.MedicalRecordFieldValueRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class MedicalRecordFieldValueService {

    @Autowired
    private MedicalRecordFieldValueRepository repository;

    public Integer insert(MedicalRecordFieldValue medicalRecordFieldValue) {
        return repository.insert(medicalRecordFieldValue);
    }

    public Integer update(MedicalRecordFieldValue medicalRecordFieldValue) {
        return repository.update(medicalRecordFieldValue);
    }

    public MedicalRecordFieldValue getById(Long id) {
        return repository.getById(id);
    }

    public List<MedicalRecordFieldValue> getByMedicalRecordId(Long medicalRecordId) {
        return repository.getByMedicalRecordId(medicalRecordId);
    }

    public Integer save(MedicalRecordFieldValue medicalRecordFieldValue) {
        if (medicalRecordFieldValue.getId() == null) {
            return insert(medicalRecordFieldValue);
        } else {
            return update(medicalRecordFieldValue);
        }
    }
}
