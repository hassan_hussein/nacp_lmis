/*
 * Electronic Logistics Management Information System (eLMIS) is a supply chain management system for health commodities in a developing country setting.
 *
 * Copyright (C) 2015  John Snow, Inc (JSI). This program was produced for the U.S. Agency for International Development (USAID). It was prepared under the USAID | DELIVER PROJECT, Task Order 4.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.openldr.medical_record.repository.mapper;

import com.openldr.medical_record.domain.UserCVType;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCVTypeMapper {

    @Insert({"INSERT INTO user_cv_types(name) values(#{name})"})
    @Options(useGeneratedKeys = true)
    Integer insert(UserCVType userCVType);

    @Update("UPDATE user_cv_types SET name=#{name} WHERE id=#{id}")
    Integer update(UserCVType userCVType);

    @Select("Select * from user_cv_types where id=#{id} LIMIT 1")
    UserCVType getById(@Param("id") Long id);

    @Select("Select * from user_cv_types")
    @Results({@Result(property = "id", column = "id"),
            @Result(property = "fields", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.UserCVTypeFieldMapper.getByCVType"))})
    List<UserCVType> getAll();


}
