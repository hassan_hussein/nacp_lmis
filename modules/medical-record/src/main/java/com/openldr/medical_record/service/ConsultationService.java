package com.openldr.medical_record.service;

import com.openldr.core.domain.User;
import com.openldr.medical_record.domain.AuthenticatedPin;
import com.openldr.medical_record.domain.Consultation;
import com.openldr.medical_record.domain.MedicalRecord;
import com.openldr.medical_record.dto.ConsultationRecordDTO;
import com.openldr.medical_record.repository.ConsultationRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class ConsultationService {


  /*  @Autowired
    SimpMessagingTemplate messagingTemplate;*/
    @Autowired
    MedicalRecordService medicalRecordService;
    @Autowired
    private ConsultationRepository repository;

    public void initiateCallConsultation(String clientReference, String agentExtension, String callStatus, String pin) {
        //get user id by phone number
        //get argent id by extension
        System.out.println("Initiation stated");

        User client = repository.getUserByPIN(pin);
        //  System.out.println(client.toString());
        User agent = repository.getUserByExtension(agentExtension);
        //  System.out.println(agent.toString());
        Long clientId = (client != null) ? client.getId() : null;
        Long agentId = (agent != null) ? agent.getId() : null;
        if (callStatus.equalsIgnoreCase("link")) {
            System.out.println("Save link");
            Consultation consultation = new Consultation();
            consultation.setStatus("AGENT_INITIATED");
            consultation.setAgentId(agentId);
            consultation.setClientId(clientId);
            consultation.setCreatedBy(agentId);
            System.out.println("Consultation*** " + consultation.toString());
            repository.initiateAgentConsultation(consultation);
           // messagingTemplate.convertAndSend("/topic/connected-client", consultation);
        } else if (callStatus.equalsIgnoreCase("unlink")) {
            System.out.println("Unlink with id:" + pin);
        }
    }

    public void forwardConsultation(ConsultationRecordDTO consultation) {

        repository.forwardConsultation(consultation.getId(), consultation.getDoctorId());

        for (MedicalRecord medicalRecord : consultation.getMedicalRecords()) {
            medicalRecord.setConsultationId(consultation.getId());
            medicalRecord.setUserId(consultation.getClient().getId());
            medicalRecordService.save(medicalRecord);
        }
        for (MedicalRecord history : consultation.getMedicalHistory()) {
            history.setConsultationId(consultation.getId());
            history.setUserId(consultation.getClient().getId());
            medicalRecordService.save(history);
        }

        //messagingTemplate.convertAndSend("/topic/forward", "NEW_QUEUE");
    }

    public ConsultationRecordDTO getClientRecordByAgentId(Long agentId, Long clientId) {
        return null;
    }

    public ConsultationRecordDTO getConsultationById(Long consultationId) {
        return repository.getConsultationById(consultationId);
    }

    public List<ConsultationRecordDTO> getQueue(Long doctorId) {
        return repository.getQueueConsultation(doctorId);
    }

    public void savePIN(String pin, String uniqueId) {
        AuthenticatedPin authenticatedPin = new AuthenticatedPin();
        authenticatedPin.setPin(pin);
        authenticatedPin.setUniqueId(uniqueId);
        repository.savePIN(authenticatedPin);
    }

    public AuthenticatedPin getPIN(String uniqueId) {
        return repository.getPIN(uniqueId);
    }

    public void saveConsultation(ConsultationRecordDTO consultation) {
        repository.saveConsultation(consultation.getId());
        for (MedicalRecord medicalRecord : consultation.getMedicalRecords()) {
            medicalRecord.setConsultationId(consultation.getId());
            medicalRecord.setUserId(consultation.getClient().getId());
            medicalRecordService.save(medicalRecord);
        }
        for (MedicalRecord history : consultation.getMedicalHistory()) {
            history.setConsultationId(consultation.getId());
            history.setUserId(consultation.getClient().getId());
            medicalRecordService.save(history);
        }
      //  messagingTemplate.convertAndSend("/topic/forward", "NEW_QUEUE");

    }
}
