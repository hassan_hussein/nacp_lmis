/*
 * Electronic Logistics Management Information System (eLMIS) is a supply chain management system for health commodities in a developing country setting.
 *
 * Copyright (C) 2015  John Snow, Inc (JSI). This program was produced for the U.S. Agency for International Development (USAID). It was prepared under the USAID | DELIVER PROJECT, Task Order 4.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.openldr.medical_record.repository.mapper;

import com.openldr.medical_record.domain.UserCV;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCVMapper {

    @Insert({"INSERT INTO user_cv(userid,cvtypeid) values(#{userId},#{cvTypeId})"})
    @Options(useGeneratedKeys = true)
    Integer insert(UserCV userCV);

    @Update("UPDATE user_cv SET userid=#{userId},cvtypeid=#{cvTypeId} WHERE id=#{id}")
    Integer update(UserCV userCV);

    @Select("Select * from user_cv where id=#{id} ")
    UserCV getById(@Param("id") Long id);

    @Select("Select * from user_cv where userid=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "fieldValues", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.UserCVFieldValueMapper.getByCVId"))
    })
    List<UserCV> getByUserId(@Param("userId") Long userId);

    @Select("Select * from user_cv where userid=#{userId} and cvtypeid=#{cvTypeId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "fieldValues", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.UserCVFieldValueMapper.getByCVId"))
    })
    List<UserCV> getByUserAndType(@Param("userId") Long userId, @Param("cvTypeId") Long cvTypeId);


}
