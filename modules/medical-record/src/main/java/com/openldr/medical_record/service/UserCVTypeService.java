package com.openldr.medical_record.service;

import com.openldr.medical_record.domain.UserCVType;
import com.openldr.medical_record.domain.UserCVTypeField;
import com.openldr.medical_record.repository.UserCVTypeRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@NoArgsConstructor
public class UserCVTypeService {

    @Autowired
    private UserCVTypeRepository repository;

    @Autowired
    private UserCVTypeFieldService fieldService;

    public Integer insert(UserCVType userCVType) {
        return repository.insert(userCVType);
    }

    public Integer update(UserCVType userCVType) {
        return repository.update(userCVType);
    }

    public UserCVType getById(Long id) {
        return repository.getById(id);
    }

    public List<UserCVType> getAll() {
        return repository.getAll();
    }

    @Transactional
    public Integer save(UserCVType userCVType) {
        if (userCVType.getId() == null) {
            Integer result = insert(userCVType);
            saveFields(userCVType);
            return result;
        } else {
            Integer result = update(userCVType);
            saveFields(userCVType);
            return result;
        }
    }

    private void saveFields(UserCVType userCVType) {
        for (UserCVTypeField field : userCVType.getFields()) {
            field.setUserCVTypeId(userCVType.getId());
            fieldService.save(field);
        }
    }
}
