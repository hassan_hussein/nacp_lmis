

package com.openldr.medical_record.domain;

import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserCVFieldValue extends BaseModel {

    private Long userCVId;

    private UserCV userCV;

    private Long userCVFieldId;

    private UserCVTypeField userCVField;

    private String fieldValue;
}
