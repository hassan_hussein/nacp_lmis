

package com.openldr.medical_record.domain;

import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Role represents Role entity which is a set of rights. Also provides methods to validate if a role contains related rights.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MedicalRecordTypeField extends BaseModel {

    List<MedicalRecordTypeFieldOption> fieldOptions;
    private Long medicalRecordTypeId;
    private Long fieldGroupId;
    private String fieldName;
    private String fieldType;
    private String style;
    private int displayOrder;
    private String group;
}
