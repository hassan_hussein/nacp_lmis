package com.openldr.email.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by hassan on 8/7/17.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestAttachment {

    private String firstName;
    private String lastName;
}
