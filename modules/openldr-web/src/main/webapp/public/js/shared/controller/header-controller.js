
function HeaderController($scope,$rootScope,OnlineLogOut,GetFacilityDefaultHIVTestType,AllHIVTestTypes,SetFacilityDefaultHIVTestType,LoggedInUser,$q,$timeout,localStorageService,UserContext, loginConfig, ConfigSettingsByKey, $window) {

  $scope.selectedTestType=undefined;
  LoggedInUser.get({}, function(data){
    $scope.prof = data.user.firstName;
  });

  AllHIVTestTypes.get({},function(data){
     $scope.allTestTypes=data.testTypes;
  });

  GetFacilityDefaultHIVTestType.get({},function(data){
     $scope.selectedTestType=data.testType;
    $rootScope.selectedTestType=data.testType;
  });

  $scope.setSelectedTestType=function(type){
        SetFacilityDefaultHIVTestType.get({id:type.id},function(data){
            $scope.selectedTestType=data.testType;
            $rootScope.selectedTestType=data.testType;
            $window.location="/public/pages/vl/index.html/#/list-samples";
        });
  };


  var userProfile = function(q, scope,timeout,UserContext) {
    var deferred = q.defer();
    timeout(function () {
      UserContext.get({}, function(data){
        deferred.resolve(data);
      });

    },100);
    var promise = deferred.promise;

    promise.then(function(message) {
      scope.message = message;
      $scope.userInfo = message.users;

      $scope.myColor= getRandomColor(message.users);
     // console.log(getRandomColor(message.users));
    /*  $scope.colors = scope.color;*/
      $scope.numbers = [{"id":2, "name":"19"}];
      $scope.firstLetter = message.users.firstName.charAt(0).toUpperCase();

    });
    return promise;
  };
   $scope.u = function(){
     return 'M';
   };
   userProfile($q,$scope,$timeout,UserContext);

  function getRandomColor(users) {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 10; i++ ) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return  color;
  }


  $scope.loginConfig = loginConfig;
  $scope.user = localStorageService.get(localStorageKeys.FULLNAME);
  $scope.userId = localStorageService.get(localStorageKeys.USER_ID);


  $scope.logout = function () {
//    OnlineLogOut.update({userId:$scope.userId,status:"OFFLINE"},function(data){
//
//    });


    localStorageService.remove(localStorageKeys.RIGHT);
    localStorageService.remove(localStorageKeys.USERNAME);
    localStorageService.remove(localStorageKeys.USER_ID);
    localStorageService.remove(localStorageKeys.FULLNAME);

    $.each(localStorageKeys.REPORTS, function(itm,idx){

          localStorageService.remove(idx);
      });
      $.each(localStorageKeys.PREFERENCE, function(item, idx){
          localStorageService.remove(idx);

      });
      $.each(localStorageKeys.DASHBOARD_FILTERS, function(item, idx){
          localStorageService.remove(idx);

      });
   // document.cookie = 'JSESSIONID' + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
    $window.location="";
    $window.location = "/j_spring_security_logout";

  };
}
