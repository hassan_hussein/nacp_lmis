var dashboard = angular.module('dashboard_orig', ['afyacall'/*,'ui.bootstrap.modal', 'ui.bootstrap.dialog','ngRoute'*/]);

dashboard.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/home', {controller: HomeController, templateUrl: '/public/pages/dashboard/dashboard.html',reloadOnSearch:true, resolve: HomeController.resolve}).
       // when('/registration', {controller: RegistrationController, templateUrl: '/public/pages/site-registration.html', resolve: RegistrationController.resolve}).
        otherwise({redirectTo: '/home'});
}]);
