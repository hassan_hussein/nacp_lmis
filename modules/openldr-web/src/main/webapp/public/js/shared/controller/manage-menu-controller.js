
function ManageMenuController($scope, AuthorizationService) {
  $scope.hasPermission = AuthorizationService.hasPermission;
}
