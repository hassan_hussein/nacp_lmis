/**
 * Created by hassan on 10/16/16.
 */
services.factory('inventoryReportPrograms', function ($resource) {
    return $resource('/inventory/report/programs.json', {}, {});
});

services.factory('InventoryReportFacilities', function ($resource) {
    return $resource('/inventory/report/ivd-form/facilities/:programId.json', { programId: '@programId'}, {});
});

services.factory('InventoryReportPeriods', function ($resource) {
    return $resource('/inventory/report/periods/:facilityId/:programId.json', {facilityId: '@facilityId', programId: '@programId'}, {});
});

services.factory('ViewInventoryReportPeriods', function ($resource) {
    return $resource('/inventory/report/view-periods/:facilityId/:programId.json', {facilityId: '@facilityId', programId: '@programId'}, {});
});

services.factory('ApprovalPending', function($resource){
    return $resource('/inventory/report/approval-pending.json',{}, {});
});

services.factory('InventoryReportInitiate', function ($resource) {
    return $resource('/inventory/report/initialize/:facilityId/:programId/:periodId.json', {facilityId: '@facilityId', programId: '@programId', periodId: '@periodId'}, {});
});

services.factory('InventoryReport', function ($resource) {
    return $resource('/inventory/report/get/:id.json', {id: '@id'}, {});
});

services.factory('InventoryReportSave', function ($resource) {
    return $resource('/inventory/report/save.json', {}, update);
});

services.factory('InventoryReportSubmit', function ($resource) {
    return $resource('/inventory/report/submit.json', {}, update);
});


services.factory('InventoryReportApprove', function ($resource) {
    return $resource('/inventory/report/approve.json', {}, update);
});

services.factory('InventoryReportReject', function ($resource) {
    return $resource('/inventory/report/reject.json', {}, update);
});


services.factory('UserHomeFacility', function ($resource) {
    return $resource('/inventory/report/userHomeFacility.json', {}, {});
});


services.factory('SaveVaccineInventoryAdjustment',function($resource){
    return $resource('/inventory/report/adjustment.json',{},{update:{method:'PUT'}});
});

services.factory('AdjustmentReasons',function($resource){
    return $resource('/inventory/report/adjustment/adjustmentReasons.json',{programId:'@programId'},{});
});