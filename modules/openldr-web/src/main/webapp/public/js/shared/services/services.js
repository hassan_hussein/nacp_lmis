
var services = angular.module('afyacall.services', ['ngResource']);
var update = {update: {method: 'PUT'}};

services.value('version', '@version@');

services.factory('Locales', function ($resource) {
  return $resource('/locales.json', {}, {});
});

services.factory('ChangeLocale', function ($resource) {
  return $resource('/changeLocale.json', {}, update);
});

services.factory('Facility', function ($resource) {
  var resource = $resource('/facilities/:id.json', {id: '@id'}, update);

  resource.restore = function (pathParams, success, error) {
    $resource('/facilities/:id/restore.json', {}, update).update(pathParams, {}, success, error);
  };

  return resource;
});

services.factory("Facilities", function ($resource) {
  return $resource('/filter-facilities.json', {}, {});
});

services.factory("FacilityTypes", function ($resource) {
  return $resource('/facility-types.json', {}, {});
});

services.factory('FacilityReferenceData', function ($resource) {
  return $resource('/facilities/reference-data.json', {}, {});
});


services.factory("ELMISInterface",function($resource)  {
  return   {
    getInterface : function(){
      return $resource('/ELMISInterface/:id.json', {}, {});
    },

    getInterfacesReference : function(){
      return $resource('/ELMISAllActiveInterfaces.json', {}, {});
    },

    getFacilityMapping : function(){
      return $resource('/ELMISInterfacesMapping/{facilityId}.json', {}, {});
    },

    getAllinterfaces : function(){
      return $resource('/ELMISAllInterfaces.json');
    }

  };
});

services.factory('ELMISInterfaceSave', function ($resource) {
  return $resource('/ELMISInterface.json', {}, {save:{method:'POST'}});
});

services.factory('UserContext', function ($resource) {
  return $resource('/user-context.json', {}, {});
});

services.factory('Users', function ($resource) {
  var resource = $resource('/users/:id.json', {id: '@id'}, update);

  resource.disable = function (pathParams, success, error) {
    $resource('/users/:id.json', {}, {update: {method: 'DELETE'}}).update(pathParams, {}, success, error);
  };

  return resource;
});

services.factory('leaflet', function ($resource) {
  return $resource('/public/coordinate.json', {}, {});
});

services.factory('Authenticate', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/00000000000000000000000000000000/v1/json/authenticate', {}, {});
});

services.factory('Regions', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/regions', {}, {});
});

services.factory('DistrictsByRegion', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/districts', {}, {});
});

services.factory('FacilitiesByDistrict', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/facilities', {}, {});
});

services.factory('TestTypes', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/tests', {}, {});
});

services.factory('Tests', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/tat', {}, {});
});

services.factory('TestsResults', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/results', {}, {});
});

services.factory('MapData', function ($resource) {
  return $resource('/public/js/shared/directives/mapData.json', {}, {});
});

services.factory('GetYearTests', function ($resource) {
  return $resource('/public/data/tests/:year.json', {year:'@year'}, {});
});

services.factory('GetYearResults', function ($resource) {
  return $resource('/public/data/results/:year.json', {year:'@year'}, {});
});



services.factory('Messages', function ($resource) {
  return $resource('/messages.json', {}, {});
});

services.factory('ForgotPassword', function ($resource) {
  return $resource('/forgot-password.json', {}, {});
});

services.factory('ConfigSettingsByKey',function($resource){
  return $resource('/settings/:key.json',{},{});
});

services.factory('Rights', function ($resource) {
  return $resource('/rights.json', {}, {});
});

services.factory('Roles', function ($resource) {
  return $resource('/roles/:id.json', {id: '@id'}, update);
});

services.factory('RolesFlat', function ($resource) {
  return $resource('/roles-flat', {id: '@id'}, update);
});

services.factory('UpdatePassword', function ($resource) {
  return $resource('/admin/resetPassword/:userId.json', {}, update);
});

services.factory('updateUserProfile', function ($resource) {
  return $resource('/userProfile/:id.json', {id:'@id'},update);
});

services.factory('SaveDistribution', function ($resource) {
  return $resource('/vaccine/inventory/distribution/save.json', {}, {save:{method:'POST'}});
});

services.factory('GetUserType', function ($resource) {
  return $resource('/userType/getAll.json', {}, {});
});
services.factory('GetAccountPackages', function ($resource) {
  return $resource('/accountPackage/getAll.json', {}, {});
});

services.factory('SaveMedicalRecordTypes', function ($resource) {
  return $resource('/medical-record-type/save', {}, update);
});

services.factory('MedicalRecordTypes', function ($resource) {
  return $resource('/medical-record-type/get-all', {}, {});
});

services.factory('SaveCVTypes', function ($resource) {
  return $resource('/user-cv-type/save', {}, update);
});

services.factory('CVTypes', function ($resource) {
  return $resource('/user-cv-type/get-all', {}, {});
});

services.factory('Account', function ($resource) {
  return $resource('/account/get-by-reference-number', {}, {});
});

services.factory('CreateAccount', function ($resource) {
  return $resource('/account/save', {}, update);
});

services.factory('Organization', function ($resource) {
  return $resource('/organization/get-all', {}, {});
});

services.factory('SaveRights', function ($resource) {
  return  $resource('/rights/save.json', {}, update);

});

services.factory('UpdateRights', function ($resource) {
  return  $resource('/rights/:name.json', {name:'@name'},update);

});

services.factory('AllRights', function ($resource) {
  return $resource('/rights/allRights.json', {}, {});
});

services.factory('GetRightsByName', function ($resource) {
  return $resource('/rights/getByName/:name.json', {name:'@name'}, {});
});

services.factory('GeoLevels', function ($resource) {
  return $resource('/geographicLevels.json', {}, {});
});


services.factory('GeographicZones', function ($resource) {
  return $resource('/geographicZones/:id.json', {id: '@id'}, update);
});

services.factory("GeographicZoneSearch", function ($resource) {
  return $resource('/filtered-geographicZones.json', {}, {});
});


services.factory('GeographicZonesAboveLevel', function ($resource) {
  return $resource('/parentGeographicZones/:geoLevelCode.json', {}, {});
});

services.factory('FlatGeographicZoneList',function ($resource){
  return $resource('/reports//geographic-zones/flat.json', {}, {});
});

services.factory('GeographicLevels', function($resource) {
  return $resource('/geographicLevels.json',{},{});
});

services.factory('SaveGeographicInfo', function($resource){
  return $resource('/geographic-zone/save-gis.json',{}, {post:{method:'POST'}});
});

services.factory('GeographicLevelById', function($resource) {
  return $resource('/geographicLevelBy/:id.json',{id : "@id"},{});
});

services.factory('GeographicLevelsInfo', function ($resource) {
  var resource =  $resource('/geographicLevels/:id.json', {id: '@id'}, update);

  resource.delete = function (pathParams, success, error) {
    $resource('/geoLevel/delete/:id.json', {}, {update: {method: 'DELETE'}}).update(pathParams, {}, success, error);
  };

  return resource;

});

services.factory('DeleteGeoLevelInfo', function($resource){
  return $resource('/geoLevel/delete/:id',{},{update:{method:'DELETE'}});
});

services.factory('SaveRightsInfo', function ($resource) {
  return  $resource('/rights/:name.json', {name:'@name'},update);

});

services.factory('SaveRIGHTSInformation', function($resource){
  return $resource('/rights/save.json',{}, {post:{method:'POST'}});
});

services.factory('DeleteRights', function($resource){
  return $resource('/rights/delete/:name.json',{}, {update:{method:'DELETE'}});
});

services.factory('UpdateUserPassword', function ($resource) {
  return $resource('/user/resetPassword/:token.json', {}, update);
});

services.factory('ValidatePasswordToken', function ($resource) {
  return $resource('/user/validatePasswordResetToken/:token.json', {}, {});
});

services.factory('SaveUserTypes', function ($resource) {
  return $resource('/userType/userType.json', {}, {save:{method:'POST'}});
});

services.factory('UpdateUserTypes', function ($resource) {
  return $resource('/userType/userType/:id.json', {}, {update:{method:'PUT'}});
});

services.factory('GetUserTypeById', function($resource){
  return $resource('/userType/getUserTypesById/:id.json', {id:'@id'},{});
});

services.factory('DeleteClientType', function($resource){
  return $resource('/userType/userType/delete/:id.json',{}, {update:{method:'DELETE'}});
});

/*start of sms Factories*/

services.factory('SMSCompleteList',function($resource){
  return $resource('/sms/MessageList.json',{},{});
});

services.factory('GetSMSInfo', function($resource){
  return $resource('/sms/setDetails',{}, {get:{method:'GET'}});
});

services.factory('GetMessagesForMobile', function($resource){
  return $resource('/sms/MessagesForMobile',{}, {get:{method:'GET'}});
});

services.factory('GetReplyMessages', function($resource){
  return $resource('/sms/getSMS',{}, {get:{method:'GET'}});
});

/*End SMS data Factories*/


services.factory('DeleteExtensionNumbers', function($resource){
  return $resource('/extensionNumbers/delete/:id.json',{}, {update:{method:'DELETE'}});
});

services.factory('GetAllExtensionNumbers', function($resource){
  return $resource('/getAllExtensionNumbers.json',{},{});
});

services.factory('GetExtensionNumbersById', function($resource){
  return $resource('/extensionNumbersBy/:id.json',{id:'@id'},{});
});

services.factory('ExtensionNumbersInfo', function ($resource) {
 return $resource('/extensionNumbers/:id.json', {id: '@id'}, update);
});

services.factory('SaveExtensionNumbersInfo', function($resource){
  return $resource('/extensionNumbers.json',{}, {post:{method:'POST'}});
});

services.factory('UserGroups', function($resource){
  return $resource('/user/userGroups.json',{},{});
});

services.factory('LoggedInUser', function($resource){
  return $resource('/getLoggedInUser.json',{},{});
});
services.factory('GetAccountPackageById', function($resource){
  return $resource('/accountPackage/GetById.json',{},{});
});

services.factory('GetAccountPackageById', function($resource){
  return $resource('/accountPackage/GetById/:id.json',{id:'@id'},{});
});

services.factory('SaveAccountPackageInfo', function($resource){
  return $resource('/accountPackage/accountPackages.json',{}, {post:{method:'POST'}});
});

services.factory('UpdateAccountPackageInfo', function ($resource) {
  return $resource('/accountPackage/accountPackages/:id.json', {id: '@id'}, update);
});


services.factory('DeleteAccountPackage', function ($resource) {
  return $resource('/accountPackage/delete/:id.json', {}, {update: {method: 'DELETE'}});
});
services.factory('Queue', function($resource){
  return $resource('/consultation/get-queue/:doctorId.json',{doctorId:'@doctorId'},{});
});

services.factory('FieldTypes', function($resource){
  return $resource('/medical-record-type-field/get-field-types.json',{}, {});
});

services.factory('FieldGroups', function($resource){
  return $resource('/medical-record-type-field/get-field-groups.json',{}, {});
});

services.factory('OnlineLogOut', function($resource){
  return $resource('/message/logout/:userId/:status.json',{userId:'@userId',status:'@status'},update);
});

services.factory('GetUserConsultationRecords', function($resource){
  return $resource('/consultation/get-user-consultation-records/:consultationId.json',{consultationId:'@consultationId'},{});
});

services.factory('ForwardConsultation', function($resource){
  return $resource('/consultation/forward.json',{},update);
});

services.factory('SaveConsultation', function($resource){
  return $resource('/consultation/save.json',{},update);
});

services.factory('GetAccountType', function ($resource) {
  return $resource('/account/types.json', {}, {});
});

services.factory('GetCountries', function ($resource) {
  return $resource('/geographic-zone/get-countries.json', {}, {});
});

services.factory('GetRegions', function ($resource) {
  return $resource('/geographic-zone/get-regions.json', {}, {});
});

services.factory('GetDistricts', function ($resource) {
  return $resource('/geographic-zone/get-districts.json', {}, {});
});


services.factory('GetLandingPages', function ($resource) {
  return $resource('/landingPages/getAll.json', {}, {});
});

services.factory('GetLandingPageById', function ($resource) {
  return $resource('/landingPageGetBy/:id.json', {}, {});
});


services.factory('SaveLandingPage', function($resource){
  return $resource('/landingPages.json',{},{post:{method:'POST'}});
});

services.factory('UpdateLandingPage', function ($resource) {
  return $resource('/landingPages/:id.json', {id: '@id'}, update);
});

services.factory('DeleteLandingPage', function ($resource) {
  return $resource('/landingPages/delete/:id.json', {id: '@id'}, update);
});

services.factory('Members', function ($resource) {
  return $resource('/account/get-members', {}, {});
});

services.factory('SupportedUploads', function ($resource) {
  return $resource('/supported-uploads.json', {}, {});
});

services.factory('Settings',function($resource){
  return $resource('/settings.json',{},{});
});

services.factory('SettingsByKey',function($resource){
  return $resource('/settings/:key.json',{},{});
});

services.factory('SettingUpdator', function($resource){
  return $resource('/saveSettings.json', {} , { post: {method:'POST'} } );
});

services.factory('SendInvoiceSMS', function($resource){
  return $resource('/account/send-invoice-sms.json', {} , {} );
});

services.factory('SendPINSMS', function($resource){
  return $resource('/account/send-pin-sms.json', {} , {} );
});

services.factory('SaveOrganization', function ($resource) {
  return $resource('/organization/save', {}, update);
});

services.factory('TopTiles', function($resource){
  return $resource('/account/top-tiles.json', {} , {} );
});

services.factory('Sample', function ($resource) {
  return $resource('/vl/sample/save', {}, update);
});

services.factory('SampleTypes', function ($resource) {
  return $resource('/vl/sampleType/getAll', {}, update);
});

services.factory('VLTestingReasons', function ($resource) {
  return $resource('/vl/vlTestingReason/getAll', {}, update);
});
services.factory('DrugAdherence', function ($resource) {
  return $resource('/vl/drugAdherence/getAll', {}, update);
});

services.factory('RegimenGroup', function ($resource) {
  return $resource('/vl/regimenGroup/getAll', {}, {});
});

services.factory('RegimenLine', function ($resource) {
  return $resource('/vl/regimenLine/getByGroup', {}, {});
});

services.factory('Regimen', function ($resource) {
  return $resource('/vl/regimen/getByLine', {}, {});
});

services.factory('GetSamplesForLab', function ($resource) {
  return $resource('/vl/sample/get-for-lab', {}, {});
});
services.factory('GetSamplesForLabByBatch', function ($resource) {
  return $resource('/vl/sample/get-for-lab-by-batch', {}, {});
});
services.factory('ReceiveSamplesForLabByBatch', function ($resource) {
  return $resource('/vl/sample/receive-by-batch', {}, {});
});

services.factory('AcceptSample', function ($resource) {
  return $resource('/vl/sample/accept-sample', {}, {});
});

//HIV Test Types services
services.factory('HIVTestType', function ($resource) {
  return $resource('/vl/testType/save', {},update);
});

services.factory('HIVTestTypeById', function ($resource) {
  return $resource('/vl/testType/getById', {}, {});
});

services.factory('AllHIVTestTypes', function ($resource) {
  return $resource('/vl/testType/getAll', {}, {});
});

//HIV Test Equipment services
services.factory('HIVTestEquipment', function ($resource) {
  return $resource('/vl/testEquipment/save', {},update);
});

services.factory('HIVTestEquipmentById', function ($resource) {
  return $resource('/vl/testEquipment/getById', {}, {});
});

services.factory('AllHIVTestEquipments', function ($resource) {
  return $resource('/vl/testEquipment/getAll', {}, {});
});

//HIV Test Equipment services
services.factory('HIVEquipmentTestSet', function ($resource) {
  return $resource('/vl/equipmentTestTypeSet/save', {},update);
});

services.factory('HIVEquipmentTestSetById', function ($resource) {
  return $resource('/vl/equipmentTestTypeSet/getById', {}, {});
});

services.factory('AllHIVEquipmentTestSets', function ($resource) {
  return $resource('/vl/equipmentTestTypeSet/getById', {}, {});
});

services.factory('WorkSheet', function ($resource) {
  return $resource('/vl/workSheet/create', {}, update);
});

services.factory('PrepareWorkSheet', function ($resource) {
  return $resource('/vl/workSheet/prepare', {}, {});
});





services.factory('SupervisoryNodes', function ($resource) {
  return $resource('/supervisory-nodes/:id.json', {}, update);
});

services.factory('SupplyLines', function ($resource) {
  return $resource('/supplyLines/:id.json', {}, update);
});

services.factory('ParentSupervisoryNodes', function ($resource) {
  return $resource('/search-supervisory-nodes.json', {}, {});
});

services.factory('RequisitionGroups', function ($resource) {
  return $resource('/requisitionGroups/:id.json', {id: '@id'}, update);
});

services.factory('SupervisoryNodesSearch', function ($resource) {
  return $resource('/search-supervisory-nodes.json', {}, {});
});

services.factory('TopLevelSupervisoryNodes', function ($resource) {
  return $resource('/topLevelSupervisoryNodes.json', {}, {});
});

services.factory('SupplyLinesSearch', function ($resource) {
  return $resource('/supplyLines/search.json', {}, {});
});


services.factory('Schedule', function ($resource) {
  return $resource('/schedules/:id.json', {id: '@id'}, update);
});

services.factory('Periods', function ($resource) {
  return $resource('/schedules/:scheduleId/periods.json', {}, {});
});

services.factory('PeriodsForFacilityAndProgram', function ($resource) {
  return $resource('/logistics/periods.json', {}, {});
});

services.factory('Period', function ($resource) {
  return $resource('/periods/:id.json', {}, {});
});


services.factory('SaveRequisitionGroupMember',function($resource){
  return $resource('/requisitionGroupMember/insert.json',{},{});
});

services.factory('RemoveRequisitionGroupMember',function($resource){
  return $resource('/requisitionGroupMember/remove/:rgId/:facId.json',{},{});
});


services.factory('ProgramProducts', function ($resource) {
  return $resource('/programProducts/programId/:programId.json', {}, {});
});

services.factory('FacilityProgramProducts', function ($resource) {
  return $resource('/facility/:facilityId/program/:programId.json', {}, {update: {method: 'PUT'}});
});

services.factory('ProgramProductsISA', function ($resource) {
  return $resource('/programProducts/:programProductId/isa/:isaId.json', {isaId: '@isaId'}, update);
});

services.factory('FacilityProgramProductsISA', function ($resource)
{
  //return $resource('/facility/:facilityId/programProducts/:programProductId/isa/:isaId.json', {isaId: '@isaId', facilityId: '@facilityId'}, update);
  return $resource('/facility/:facilityId/programProducts/:programProductId/isa.json', {}, update);
});

services.factory('AllocationProgramProducts', function ($resource) {
  return $resource('/facility/:facilityId/programProduct/:programProductId.json', {}, update);
});

services.factory('ProgramProductsFilter', function ($resource) {
  return $resource('/programProducts/filter/programId/:programId/facilityTypeId/:facilityTypeId.json',
      {programId: '@programId', facilityTypeId: '@facilityTypeId'}, {}, {});
});

services.factory('FacilityTypeApprovedProducts', function ($resource) {
  return $resource('/facilityApprovedProducts/:id.json', {id: '@id'}, update);
});

services.factory('ProgramProductsSearch', function ($resource) {
  return $resource('/programProducts/search.json', {}, {});
});

services.factory('Reports', function ($resource) {
  return $resource('/reports/:id/:format.json', {}, {});
});

services.factory('ProductGroups', function ($resource) {
  return $resource('/products/groups.json', {}, {});
});

services.factory('ProductForms', function ($resource) {
  return $resource('/products/forms.json', {}, {});
});

services.factory('DosageUnits', function ($resource) {
  return $resource('/products/dosageUnits.json', {}, {});
});

services.factory('Products', function ($resource) {
  return $resource('/products/:id.json', {id: '@id'}, update);
});

services.factory('ProductCategories', function ($resource) {
  return $resource('/products/categories.json', {}, {});
});


services.factory('Programs', function ($resource) {
  return $resource('/programs/:type.json', {type: '@type'}, {});
});

services.factory('Programs', function ($resource) {
  return $resource('/programs.json', {}, {});
});

services.factory('UpdateProgram', function ($resource) {
  return $resource('/programs/save.json', {}, update);
});


services.factory('Facility', function ($resource) {
  var resource = $resource('/facilities/:id.json', {id: '@id'}, update);

  resource.restore = function (pathParams, success, error) {
    $resource('/facilities/:id/restore.json', {}, update).update(pathParams, {}, success, error);
  };

  return resource;
});

services.factory("Facilities", function ($resource) {
  return $resource('/filter-facilities.json', {}, {});
});
services.factory("SampleFacilities", function ($resource) {
  return $resource('/vl/sample/get-facilities.json', {}, {});
});

services.factory("AcceptedSamples", function ($resource) {
  return $resource('/vl/sample/accepted-samples.json', {}, {});
});

services.factory("RejectSample", function ($resource) {
  return $resource('/vl/sample/reject-sample', {}, {});
});

services.factory('SearchFacilities',function($resource){
    return $resource('/facility/search-facility.json',{},{});
});

services.factory('GetSampleById', function ($resource) {
  return $resource('/vl/sample/get-by-id', {}, {});
});

services.factory('GetSampleAllById', function ($resource) {
  return $resource('/vl/sample/:id.json', {id: '@id'}, {});
});

services.factory('GetWorkSheet', function ($resource) {
  return $resource('/vl/workSheet/get-by-id', {}, {});
});

services.factory('GetResults', function ($resource) {
  return $resource('/vl/result/get-result', {}, {});
});
services.factory('GetResultsList', function ($resource) {
  return $resource('/vl/result/get-result-list', {}, {});
});

services.factory('GetResultsList', function ($resource) {
  return $resource('/vl/result/get-result-list-for-all', {}, {});
});

services.factory('HomeFacility', function ($resource) {
  return $resource('/vl/sample/getHomeFacility', {}, {});
});

services.factory('SupervisedLab', function ($resource) {
  return $resource('/vl/sample/getSupervisedLabs', {}, {});
});

services.factory('GetSampleTestByAge', function ($resource) {
  return $resource('/vl/sample/getSampleTestByAge.json', {}, {});
});

services.factory('FacilitySupervisors', function ($resource) {
  return $resource('/vl/sample/getFacilitySupervisors', {}, {});
});

services.factory('GetSampleForHub', function ($resource) {
  return $resource('/vl/sample/get-for-hub', {}, {});
});

services.factory('GetSampleForHubByLabNumber', function($resource){
  return $resource('/vl/sample/get-for-lab-by-number', {},{});
});

services.factory('SetFacilityDefaultHIVTestType', function ($resource) {
  return $resource('/vl/testType/setFacilityDefault', {}, {});
});

services.factory('GetFacilityDefaultHIVTestType', function ($resource) {
  return $resource('/vl/testType/getFacilityDefault', {}, {});
});

services.factory('TestSample', function ($resource) {
  return $resource('/vl/sample/test-sample', {}, {});
});

services.factory('ApproveSample', function ($resource) {
  return $resource('/vl/sample/approve-sample', {}, {});
});
services.factory('DispatchSample', function ($resource) {
  return $resource('/vl/sample/dispatch-sample', {}, {});
});

services.factory('GetTotalTestDoneForDashboard', function ($resource) {
  return $resource('/vl/sample/getTotalTestDoneForDashboard', {}, {});
});

services.factory('GetTestBySampleForDashboard', function ($resource) {
  return $resource('/vl/sample/getTestBySampleForDashboard', {}, {});
});

services.factory('GetSampleByFacilityForDashboard', function ($resource) {
  return $resource('/vl/sample/getSampleByFacilityForDashboard', {}, {});
});

services.factory('Schedule', function ($resource) {
  return $resource('/schedules/:id.json', {id: '@id'}, update);
});

services.factory('Periods', function ($resource) {
  return $resource('/schedules/:scheduleId/periods.json', {}, {});
});

services.factory('PeriodsForFacilityAndProgram', function ($resource) {
  return $resource('/logistics/periods.json', {}, {});
});

services.factory('Period', function ($resource) {
  return $resource('/periods/:id.json', {}, {});
});

services.factory('Program', function ($resource) {
  return $resource('/programs.json', {}, {});
});

services.factory("GetRejectedSamples", function ($resource) {
  return $resource('/vl/sample/rejected-sample-list', {}, {});
});

//Reports
services.factory("FacilityResultReport", function ($resource) {
  return $resource('/reports/reportdata/samples', {}, {});
});


services.factory('saveRegimen', function ($resource) {
  return $resource('/vl/regimen/save', {}, {save:{method:'PUT'}});
});

services.factory('GetRegimenLine', function ($resource) {
  return $resource('/vl/regimenLine/getAll', {}, {});
});

services.factory('GetAllRegimen', function ($resource) {
  return $resource('/vl/regimen/getAllInFull', {}, {});
});

services.factory('Delete', function($resource){
  return $resource('/vl/regimen/delete/:id.json',{},{update:{method:'DELETE'}});
});

services.factory('DeleteSample', function($resource){
  return $resource('/vl/sample/deleteSample/:id.json',{},{update:{method:'DELETE'}});
});


services.factory('GetRegimenById', function($resource){
  return $resource('/vl/regimen/getAllInFullById/:id.json',{id: '@id'},{});
});

services.factory('GetLabTatSummary', function($resource){
  return $resource('/vl/dashboard/labTatSummary.json',{},{});
});

services.factory('GetTestTrendForLab', function($resource){
  return $resource('/vl/dashboard/testTrendForLab.json',{},{});
});

services.factory('GetVLResults', function($resource){
  return $resource('/vl/dashboard/getVLResults.json',{},{});
});
services.factory('GetByAgeLessThan2', function($resource){
  return $resource('/vl/dashboard/getByAgeLessthan2.json',{},{});
});

services.factory('GetSampleForm', function($resource){
  return $resource('/vl/sample/getHubForm.json',{},{});
});

services.factory('GetRegisterReport', function($resource){
  return $resource('/reports/reportdata/registerReport.json',{},{});
});

services.factory('NextBackBasicService', function($route, $location) {
  //array for keeping defined routes
  var routes = [];

  angular.forEach($route.routes, function(config, route) {
    //not to add same route twice
    if (angular.isUndefined(config.redirectTo)) {
      routes.push(route);
    }
  });

  return {
    goNext: function() {
      var nextIndex = routes.indexOf($location.path()) + 1;
      if (nextIndex === routes.length) {
        $location.path(routes[0]);
      } else {
        $location.path(routes[nextIndex]);
      }
    },
    goBack: function() {
      var backIndex = routes.indexOf($location.path()) - 1;
      if (backIndex === -1) {
        $location.path(routes[routes.length - 1]);
      } else {
        $location.path(routes[backIndex]);
      }
    }
  };

});


services.factory('GetTotalSampleForLab', function($resource){
  return $resource('/vl/sample/get-total-samples-for-lab',{},{});
});
services.factory('GetTotalSampleForHub', function($resource){
  return $resource('/vl/sample/get-total-samples-for-hub',{},{});
});

services.factory('GetDispatchedResults', function($resource){
  return $resource('/vl/result/get-dispatched-result-list-for-hub',{},{});
});
services.factory('GetResultReport', function($resource){
  return $resource('/reports/reportdata/resultReport.json',{},{});
});

services.factory('GetSamplesByBatch', function ($resource) {
  return $resource('/vl/sample/get-by-batch', {}, {});
});

services.factory('GetSamplesByBatchOneRow', function ($resource) {
  return $resource('/vl/sample/get-by-batch-one-row', {}, {});
});

services.factory('GetAgeSummaryReport', function($resource){
  return $resource('/reports/reportdata/summary.json',{},{});
});

services.factory('GetAllLabs', function($resource){
  return $resource('/lab-list.json',{},{});
});

services.factory('Partners', function($resource){
  return $resource('/getAllPartners.json',{},{});
});

services.factory('FacilityListInterface', function($resource){
  return $resource('/facility-list.json',{},{});
});