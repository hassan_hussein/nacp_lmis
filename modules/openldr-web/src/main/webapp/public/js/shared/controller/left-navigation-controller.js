function LeftNavigationController($scope, localStorageService) {

  $scope.loadRights = function () {
      $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
  }();

  $scope.hasPermission = function (permission) {
              if ($scope.rights !== undefined && $scope.rights !== null) {
                var rights = JSON.parse($scope.rights);
                var rightNames = _.pluck(rights, 'name');
                return rightNames.indexOf(permission) > -1;
              }
              return false;
  };

}

LeftNavigationController.resolve={};
