/**
 * Created by hassan on 4/18/16.
 */

function DashFuctBoardController($scope,$log,TestingTrend, $filter,TestsResults,Tests,$q,GetData,GetData2, $timeout,GetYearTotals,GetYearResults,GetYearBySampleType,GetTestTotalByAge) {

    $scope.labs = [{'code':'bugando','name':'Bugando Medical Centre (BMC)','facility_id': '100438-1'},
        {'code':'kcmc',"name":'KCMC','facility_id':'102525-3'},
        {'code':'mbeya','name':'Mbeya Zonal Referral Hospital','facility_id':'104601-0'},
        {'code':'nhlqatc','name':'NHLQATC','facility_id':'111317-4'}
    ];


    $scope.defaultYear = new Date().getFullYear();
    $scope.defaultMonth = 0; //new Date().getMonth() +1;
    $scope.map = {openPanel: true};
    $scope.filter = {openPanel: true};
    $scope.byAge = {openPanel: true};


    $scope.formatString = function(format) {
        var day   = parseInt(format.substring(0,2));
        var month  = parseInt(format.substring(3,5));
        var year   = parseInt(format.substring(6,10));

        return  new Date(year, month, day);
    };

    $scope.dayDiff = function(firstDate,secondDate){
        var date2 = new Date(secondDate);
        var date1 = new Date(firstDate);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        return Math.ceil(timeDiff / (1000 * 3600 * 24));
    };
    $scope.calculateAverage = function(MyData){
        var sum = 0;
        // if (angular.isNumber(MyData)) {

        for(var i = 0; i < MyData.length; i++){
           //  if(angular.isNumber(MyData[i])) {
            if(!isNaN(MyData[i]))
            sum += parseInt(MyData[i], 10);
           // }
        }
        return sum/MyData.length;

        //}


    };

    var collectionToLab;
    var receivedToProcessing;
    var processingToDispature;
    var collectionToDispature;

    GetData.get({}, function(data){

        var cr=[];
        var cr2=[];
        var cr3 =[];
        var cr4 =[];
        console.log(data.data);


        var date = new Date(new Date().setDate(new Date().getDate() - 60));

        $scope.filterAuthorizedDate = _.filter(data.data, function(n){
            return ((new Date($scope.formatString(n.AuthorisedDate)) >= new Date(date)) && ( n.AuthorisedDate !='Pending'));
        });
        console.log($scope.filterAuthorizedDate);
        /*
         console.log( $scope.filterAuthorizedDate);
         */

        $scope.filterAuthorizedDate.forEach(function (t) {

            var data1= $scope.dayDiff(t.ReceivedDate,t.SpecimenDate);
            var data2= $scope.dayDiff(t.ResultedDate,t.ReceivedDate);
            //  var data2= $scope.dayDiff(t.ResultedDate,t.ReceivedDate);
            var data3= $scope.dayDiff(t.AuthorisedDate,t.ResultedDate);
            var data4= $scope.dayDiff(t.AuthorisedDate,t.SpecimenDate);

            if(data1 <75)
                cr.push(data1);
            if(data2 <75)
                cr2.push(data2);
            if(data3 <75)
                cr3.push(data3);
            if(data4 <75)
                cr4.push(data4);

        });
        collectionToLab = $scope.calculateAverage(cr);
        receivedToProcessing= $scope.calculateAverage(cr2);
        processingToDispature= $scope.calculateAverage(cr3);
        collectionToDispature= $scope.calculateAverage(cr4);
        var summary = {"cr":collectionToLab.toFixed(0),"rp":receivedToProcessing.toFixed(0),"pd":processingToDispature.toFixed(0),"cd":collectionToDispature.toFixed(0)};
        console.log(summary);
       // getTATDataToDisplay(summary);
        //   console.log(collectionToLab.toFixed(0),receivedToProcessing.toFixed(0),processingToDispature.toFixed(0),collectionToDispature.toFixed(0));

    });



    var getSuspectedTreatmentFailure = function (results) {
        var resultsWithCPL = _.where(results, {ObservationCode: "HIVVM"});
        var targetNotDetected = [];

        resultsCPLGreaterThan1000 = _.filter(resultsWithCPL,
            function (r) {
                var testResult = r.TestResult.replace(/\D/g, '');
                testResult = testResult.trim();
                if (testResult === '') {
                    targetNotDetected.push(testResult);
                } else {
                    return parseInt(testResult, 10) > 1000;
                }
            });
        return resultsCPLGreaterThan1000;
    };

    var getDateRange = function (year, month) {
        var dateRange = {};

        var lastDate = new Date(year, month, 0);
        $scope.selectMonth = _.findWhere($scope.monthsToDisplay, {id: month});

        if (month >= 1 && month <= 9) {
            dateRange.startDate = '01-0' + month + '-' + year;
            dateRange.endDate = lastDate.getDate() + '-0' + month + '-' + year;
        }
        else if (month > 9) {
            dateRange.startDate = '01-' + month + '-' + year;
            dateRange.endDate = lastDate.getDate() + '-' + month + '-' + year;
        }
        else {
            dateRange.startDate = '01-01-' + year;
            dateRange.endDate = '31-12-' + year;
            $scope.selectMonth = {"name": "Jan-Dec", "id": 0};
        }
        return dateRange;

    };

    var applyFilter = function (filter) {

        //filter {location: "all", year: 2016, month: 0, dateRange:{endDate:"31-12-2016",startDate:"01-01-2016"
        // console.log($scope.staticAllData);
        var allData = [];
        angular.copy($scope.staticAllData, allData);
        //console.log(allData);
        if (filter.year !== undefined) {
            $scope.filteredData = {};

            $scope.filteredYearData = _.findWhere(allData, {year: filter.year});
            console.log($scope.filteredYearData);
            $scope.filteredData = $scope.filteredYearData;
            //  $scope.filteredData.month=0;
            $scope.filteredData.location = "all";
        }


        if (filter.month > 0) {
            console.log('apply month');
            var mon = ['January  ','February ','March','April','May','June','July','August','September','October','November','December'];

            var testingTrend = _.where($scope.filteredData.testingTrends, {monthreceived:mon[filter.month-1]});

            var totals = _.where($scope.filteredData.totals, {Month: filter.month});
            var results = _.where($scope.filteredData.results, {Month: filter.month});
            var testByAge = _.where($scope.filteredData.testByAge, {Month: filter.month});

            var mapSuppressionTAT = _.where($scope.filteredData.mapSuppressionTAT, {Month: filter.month});

            $scope.filteredData.totals = totals;
            $scope.filteredData.results = results;
            $scope.filteredData.testByAge = testByAge;
            $scope.filteredData.testingTrends = testingTrend;
            $scope.filteredData.tatToDisplay = testingTrend;
            $scope.filteredData.ageOutCome= testingTrend;
            $scope.filteredData.mapSuppressionTAT = mapSuppressionTAT;

            $scope.filteredData.month = filter.month;

            console.log(testingTrend);

        }


        if (filter.location !== "all") {
            console.log('apply location');


            var totals = _.filter($scope.filteredData.totals, function (t) {
                console.log(filter);
                return t.Location.toLowerCase() == filter.location.toLowerCase();
            });

            var results = _.filter($scope.filteredData.results, function (r) {
                return r.Location.toLowerCase() == filter.location.toLowerCase();
            });
            /*var testingTrends = _.filter($scope.filteredData.testingTrends, function (r) {
             var getFacilityId = $scope.LAB
             _.contains(r,)

             return r.Location.toLowerCase() == filter.location.toLowerCase();
             });*/

            var testByAge = _.filter($scope.filteredData.testByAge, function (r) {
                return r.Location.toLowerCase() == filter.location.toLowerCase();
            });


            var lab2 = {'bugando':'TBG','iringa':'TDS'};


            var testByTrend = _.filter($scope.filteredData.testingTrends, function (r) {
                return r.labnumber.substring(0,3) == lab2[filter.location.toLowerCase()];
            });
            $scope.filteredData.totals = totals;
            $scope.filteredData.results = results;
            $scope.filteredData.testByAge = testByAge;

            $scope.filteredData.location = filter.location;
            $scope.filteredData.testingTrends=testByTrend;
            $scope.filteredData.tatToDisplay = testByTrend;
            $scope.filteredData.ageOutCome= testByTrend;

            $scope.filteredData.mapSuppressionTAT=mapSuppressionTAT;

            console.log(testByTrend);

        }
       // console.log($scope.filteredData);
    };

    function getTestingTrendData(ordered) {
console.log(ordered);
  /*      var vals = Object.keys(ordered[0].total).map(function (key) {
            return obj[key];
        });*/
      var months = ['January','February','March'];


        console.log(ordered[0].total);
        console.log(ordered[1].total);

        var firstV = ordered[0].total;
        var secV = ordered[1].total;

        var s=[];
        var sc=[];
        for(var i =0;i <months.length;i++) {
            s.push(firstV[months[i]]);
            sc.push(secV[months[i]]);
        }




        var m=[];
        var n =[];
        $.each(ordered, function (index, value) {
            m.push(value.total);

            $.each(m, function(ind, v){
                n.push(v[ind]);
            });

        });


        var filteredData = [];

        var valu=[];
        for(key in ordered) {
            if(ordered.hasOwnProperty(key)) {
                valu.push(ordered[key]);
            }
        }
        console.log(valu);

        console.log(_.uniq( _.pluck(ordered,'month')));

        var keys = [];
        var total = [];
       var obj = _.uniq( _.pluck(ordered,'month'));
        for(var keyName in obj){
            var key=keyName ;
            var value= obj[keyName ];

            _.each( value, function( val, key ) {
                if ( val ) {
                    keys.push(key);
                    total.push(val.length);
                }
            });
        }

    /*    for(var keyName1 in obj){
            var key2=keyName1 ;
            var value2= obj[keyName1 ];

            _.each( value, function( val, key ) {
                if ( val ) {
                    keys.push(key);
                    total.push(val.length);
                }
            });
        }*/
    /*  var j = _.pluck(ordered,'total');
       var da= $.map(j, function (value, index) {
            return value;
        });

        function toObject(arr) {
            var rv = {};
            for (var i = 0; i < arr.length; ++i)
                rv[i] = arr[i];
            return rv;
        }
*/





      var samples=  _.pluck(ordered,'samples');

        var year = ordered[0].year;



        console.log(samples[0]);
        console.log(sc);

        var testingTrend= [

            {"name":samples[0],"data":s},
            {"name":samples[1],"data":sc}

        ];

        console.log(testingTrend);
        //var stackedValueByAge= [{"name":" <1000cpl/ml","data":supp},{"name":">1000cpl/ml","data":sup}];

        /*     var DataValue = _.groupBy(data, _.property('month'), function(n){
         return n;
         });
         var mappedData= $.map(DataValue, function (value, index) {
         var d = [];
         value.forEach(function(index){

         d.push(index.suspected,index.suppression);

         });
         return [{"name":index,'data':d}];

         });*/
        // console.log(mappedData);


        var chart1;
        $(function () {

            chart1=  new Highcharts.Chart('trendTrend', {
                credits:{
                    enabled:false
                },
                chart: {
                    type: 'column',
                    width: 700
                },

                rangeSelector: {
                    selected: 1
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: _.uniq(keys)
                    ,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Samples'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 10,
                    floating: false,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: true,
                        reversed: true

                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series:testingTrend
            });
            $scope.trendLoading = true;
        });
    }

    var groupByRegionMapDataCallBack= function (filter) {
        if ($scope.allData !== undefined) {
            $scope.mapSuppressionTat= $scope.filteredData.mapSuppressionTat;
            console.log( $scope.mapSuppressionTat);


        }
    };

   var groupByAgeFunction = function(originalData,name){
          if(originalData.length > 0 ) {
              var l = [];
              var m = [];

              originalData.forEach(function (index) {
                  if (parseInt($.trim(index.testresult), 10) > 1000) {
                      l.push(index);
                  } else {
                      m.push(index);

                  }
              });

              return {"total": originalData.length, 'suppressed': m.length, 'notSuppressed': l.length};
          }else
          return null;
   };


    $scope.showAgeModal= function(){
           $scope.agemodal = true;
    };


    function getByAgeData(all) {

        console.log(all);
        var suppressed = _.pluck(all, 'suppressed');
        var notsuppressed = _.pluck(all,'notSuppressed');

       var ageGroups =[ {
           'name':'Suppressed',
           'data':suppressed

       },{'name':'Not Suppressed','data':notsuppressed}];

/*
        new Highcharts.chart('#ageGroups_stacked',{
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: ["No Data", "Less 2", "2-9", "10-14", "15-19", "20-24", "25+"],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Tests'
                },
                stackLabels: {
                    rotation: 0,
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    },
                    y: -10
                }
            },
            legend: {
                align: 'right',
                verticalAlign: 'bottom',
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: true
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>% contribution: {point.percentage:.1f}%'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: false,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black'
                        }
                    }
                }
            }, colors: [
                '#F2784B',
                '#1BA39C'
            ],
            series: [{
                "name": "Not Suppresed",
                "data": [95, 17, 261, 243, 214, 190, 2290],
                "drilldown": {"color": "#913D88"}
            }, {
                "name": "Suppresed",
                "data": [361, 20, 352, 345, 253, 455, 10007],
                "drilldown": {"color": "#96281B"}
            }]
        });
*/




        new Highcharts.chart('ByAge', {
            credits:{
                enabled:false
            },
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: [ "Less 2", "2-9", "10-14", "15-19", "20-24", "25+"]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Tests'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                verticalAlign: 'bottom',
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: true
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>% contribution: {point.percentage:.1f}%'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series:ageGroups
        });





    }

    function getVlOutCome(data) {

        var suppress = [];
        var notSuppress = [];

        data.forEach(function(data){

            if(parseInt($.trim(data.testresult),10)>1000){
                notSuppress.push(data);
            }else{
                suppress.push(data);
            }
        });

        console.log(suppress.length,notSuppress.length);
      var pieChart =[{'name':'VL OutCome', colorByPoint: true,

          'data':[{'name':'Suppressed','y':suppress.length},{'name':'Not Suppressed','y':notSuppress.length
          ,sliced: true,
              selected: true}],innerSize: '60%'
      }
      ];

        //VL OUTCOME


       new Highcharts.chart('vlOutCome', {
           credits:{
               enabled:false
           },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'

            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series:pieChart
        });



    }

    function getByGender(data) {

        var groupByGender = _.groupBy(data, function(data){
            return data.hl7sexcode
        });
        var suppress = [];
        var notSuppress = [];

        var allMappedGender= $.map(groupByGender, function(value, index){


            value.forEach(function(data){

                if(parseInt($.trim(data.testresult),10)>1000){
                    notSuppress.push(data);
                }else{
                    suppress.push(data);
                }
            });

            return [{'index':index,'suppressed':suppress.length,'notSuppressed':notSuppress.length}];



    });

        var supp = _.pluck(allMappedGender,'suppressed');
        var notSupp = _.pluck(allMappedGender,'notSuppressed');
        var index = _.pluck(allMappedGender,'index');
        var g =['Female','Male','No Data'];
        var genderGraph =[ {
            'name':'Suppressed',
            'data':supp
        }, {'name':'Not Suppressed','data':notSupp}
        ];


      new  Highcharts.chart('genderGraph', {
            credits:{enabled:false},
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: g
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Tests'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                verticalAlign: 'bottom',
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: true
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>% contribution: {point.percentage:.1f}%'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series:genderGraph
        });



    }

    function getByRegionalOutCome(data) {

        var groupedByRegion = _.groupBy(data, function (t) {
            return t.region;
        });


        var suppress = [];
        var notSuppress = [];

        var allMappedByRegions= $.map(groupedByRegion, function(value, index){


            value.forEach(function(data){

                if(parseInt($.trim(data.testresult),10)>1000){
                    notSuppress.push(data);
                }else{
                    suppress.push(data);
                }
            });

            return [{'index':index,'suppressed':suppress.length,'notSuppressed':notSuppress.length}];

        });

        console.log(allMappedByRegions);


        var supp = _.pluck(allMappedByRegions,'suppressed');
        var notSupp = _.pluck(allMappedByRegions,'notSuppressed');
        var index = _.pluck(allMappedByRegions,'index');
        var g =['Female','Male','No Data'];
        var genderGraph =[ {
            'name':'Suppressed',
            'data':supp
        }, {'name':'Not Suppressed','data':notSupp}
        ];


        Highcharts.chart('regionGraph', {
            credits:{enabled:false},
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: index
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Tests'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                verticalAlign: 'bottom',
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: true
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>% contribution: {point.percentage:.1f}%'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series:genderGraph
        });


    }

    var testTrendCallBack = function (filter) {

        $scope.trendLoading = false;

        if ($scope.allData !== undefined && $scope.filteredData.testingTrends.length> 0) {
            console.log($scope.filteredData);
            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

            var months1 = ["January", "February", "March", "April", "May",
                "June", "July", "August", "September", "October",
                "November", "December"];

          /*  var bySampleType=_.groupBy($scope.filteredData.testingTrends,function(r){
                return r.LIMSSpecimenSourceDesc;
            });*/
       /*     $scope.bySampleType = $.map(bySampleType, function(value, index) {
               var resultsRepeated=_.filter(value,function(result){
                    return result.Repeated > 0;
                });
                return [{"sample":value.length,"type":value.LIMSSpecimenSourceDesc}];
            });*/
            //$scope.testBySample.dataPoints=$scope.bySampleType;

//start
            var filteredData = _.where($scope.filteredData.testingTrends,{'monthreceived':months1[filter.month-1]});
             console.log($scope.filteredData.testingTrends);


            var dataValue2 = _.groupBy($scope.filteredData.testingTrends,function(x){
                return x.monthreceived;
            });
            var dataValue3 = $.map(dataValue2,function(value,index){
               var val3 = _.groupBy(value,function(m){

                   return m.limsspecimensourcedesc;
               });
               return {'type': $.trim(index),'value':value.length};
           });


            var DataValue = _.groupBy($scope.filteredData.testingTrends, _.property('limsspecimensourcedesc'), function(n){
                return n;
            });
            var mappedData= $.map(DataValue, function (value, index) {
                var d = [];
                value.forEach(function(index){

                    d.push($.trim(index.monthreceived))

                });

                var DataValue1 = _.groupBy(d,function(n){
                    return n.monthreceived;
                });


                return [{"name":index,'data':d,'dataV':DataValue1}];

            });

            console.log(mappedData);



            //end


            var testsDoneByMonth1 = _.groupBy($scope.filteredData.testingTrends, function (t) {
                return t.limsspecimensourcedesc;
            });
         var dataT =   $.map(testsDoneByMonth1, function (value, index) {
            var data=  _.groupBy(value, function (r) {
                 return $.trim(r.monthreceived);
             });
 console.log(_.allKeys(data));
           var val2=  _.mapObject(data, function(val, key) {
                 return val.length;
             });
console.log(val2);
             var data2=  $.map(data, function (value, index) {
                 var data1;
              /*   value.forEach(function(data){
                     data1 = data;
                 });*/
             /*   _.each(value, function( val, key ) {
                    if ( val ) {
                        keys.push(val.length);
                    }
                });*/

             //keys.push(index);
                return value.length;
             });

             return [{"month": data,'total':val2, "samples": index}];
            });
            console.log(_.uniq( _.pluck(dataT,'month')));

            var keys = [];
            _.each(_.uniq( _.pluck(dataT,'month')), function( val, key ) {
                if ( val ) {
                    keys.push(key);
                }
            });

            console.log(keys);
            var testsDoneByMonth = _.groupBy($scope.filteredData.totals, function (t) {
                return (months[t.Month - 1]);
            });
            var resultsByMonth = _.groupBy($scope.filteredData.results, function (r) {
                return (months[r.Month - 1]);
            });

            var resultsByMonthArray = $.map(resultsByMonth, function (value, index) {
                return [{"month": index, "results": value}];
            });
            $scope.testsDoneByMonth1 = $.map(testsDoneByMonth1, function (value, index) {
                //rejected=getRejected(value);
                var received = 0;
                var rejected = 0;
                var tested = 0;
                var greaterThan = 0;
                var lessThan = 0;
                var requests = 0;
                var authorized=0;
                value.forEach(function (t) {
                    // console.log(t);
                    received = received + t.Received;
                    requests = requests + t.Requests;
                    authorized = authorized + t.Authorized;
                    rejected = rejected + t.Rejected;

                });
                /*    var results = _.findWhere(resultsByMonthArray, {month: index});
                 if (results != undefined) {
                 results.results.forEach(function (r) {
                 lessThan = lessThan + r.Less;
                 greaterThan = greaterThan + r.Greater;
                 });
                 }*/

                return [{
                    "month": index,
                    "test": authorized,
                    "received":received,
                    "rejected": rejected,
                    "year":filter.year
                }];
            });

            console.log($scope.testsDoneByMonth1);


            $scope.testsDoneByMonth = $.map(testsDoneByMonth, function (value, index) {
                //rejected=getRejected(value);
                var rejected = 0;
                var tested = 0;
                var greaterThan = 0;
                var lessThan = 0;
                value.forEach(function (t) {
                    //   console.log(t);
                    tested = tested + t.Tested;
                    rejected = rejected + t.Rejected;
                });
                var results = _.findWhere(resultsByMonthArray, {month: index});
                if (results != undefined) {
                    results.results.forEach(function (r) {
                        lessThan = lessThan + r.Less;
                        greaterThan = greaterThan + r.Greater;
                    });
                }

                return [{
                    "month": index,
                    "test": tested,
                    "suspected": greaterThan,
                    "rejected": rejected,
                    "suppression": lessThan
                }];
            });

            /*  $scope.getMonthValue = function(mon) {
             return new Date(Date.parse(mon.value +" 1, 2000")).getMonth()+1;
             };*/

            var ordered = $scope.testsDoneByMonth1.sort(function (a, b) {
                if (months1.indexOf(a.month) < months1.indexOf(b.month)) return -1;
                if (months1.indexOf(b.month) < months1.indexOf(a.month)) return 1;
                return 0;
            });
            console.log(ordered);
//getData(ordered);
            getTestingTrendData(dataT);
            getVlOutCome($scope.filteredData.testingTrends);
            getByGender($scope.filteredData.testingTrends);
            getByRegionalOutCome($scope.filteredData.testingTrends);
            $scope.testingTrends = ordered;
            $scope.testTrend.dataPoints = ordered;

        }

    };
    var getData = function (data){
        console.log(data);
        var supp=  _.pluck(data,'suppression');
        var sup=  _.pluck(data,'suspected');
        var stackedValueByAge= [{"name":" <1000cpl/ml","data":supp},{"name":">1000cpl/ml","data":sup}];

        /*     var DataValue = _.groupBy(data, _.property('month'), function(n){
         return n;
         });
         var mappedData= $.map(DataValue, function (value, index) {
         var d = [];
         value.forEach(function(index){

         d.push(index.suspected,index.suppression);

         });
         return [{"name":index,'data':d}];

         });*/
        // console.log(mappedData);

        $(function () {

            new Highcharts.Chart('container2', {
                credits:{
                    enabled:false
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: _.pluck(data,'month')
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Tests'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 10,
                    floating: false,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: true
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series:stackedValueByAge
            });

        });
    };

    var suppressionCallBack = function(filter){

        if($scope.allData !== undefined)
        {
            var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            /*
             var totalLess=0;
             $scope.filteredData.results.forEach(function(r){
             totalLess=totalLess+r.Less;
             });
             var statistic={"name":"HIV viral suppression (<1000cpl/ml)  ( "+$scope.selectMonth.name+" )","value":totalLess };*/


            var testsDoneByMonth=_.groupBy($scope.filteredData.totals, function(t){
                return (months[t.Month-1]);
            });
            var resultsByMonth=_.groupBy($scope.filteredData.results, function(r){
                return (months[r.Month-1]);
            });

            var resultsByMonthArray=$.map(resultsByMonth, function(value,index){
                return [{"month":index,"results":value}];
            });

            function percentage(num, per)
            {
                return (num/100)*per;
            }
            $scope.testsDoneByMonth = $.map(testsDoneByMonth, function(value, index) {
                //rejected=getRejected(value);
                var rejected=0;
                var tested=0;
                var lessThan=0;
                var greaterThan =0;
                var Total = 0;
                var totalValue =0;
                var totalRejection =0;
                value.forEach(function(t){
                    tested=tested+t.Tested;
                    rejected=rejected+t.Rejected;
                    Total = Total + t.Total;
                });
                var results=_.findWhere(resultsByMonthArray,{month:index});
                if(results != undefined){
                    results.results.forEach(function(r){
                        lessThan=lessThan+r.Less;
                        greaterThan=greaterThan+r.Greater;

                    });
                }
                totalValue = ((lessThan / tested)* 100).toFixed(0);

                /*
                 "test":tested,"suppressed":lessThan,"suspected":greaterThan
                 */
                return [{"month":index,"percentageSuppressed":totalValue}];
            });

            var ordered=$scope.testsDoneByMonth.sort(function (a, b) {
                if (months.indexOf(a.month) < months.indexOf(b.month)) return -1;
                if (months.indexOf(b.month) < months.indexOf(a.month)) return 1;
                return 0;
            });

            $scope.testTrendSuppression.dataPoints=ordered;


        }

    };
    var rejectionCallBack = function(filter){

        if($scope.allData !== undefined)
        {
            var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            /*
             var totalLess=0;
             $scope.filteredData.results.forEach(function(r){
             totalLess=totalLess+r.Less;
             });
             var statistic={"name":"HIV viral suppression (<1000cpl/ml)  ( "+$scope.selectMonth.name+" )","value":totalLess };*/


            var testsDoneByMonth=_.groupBy($scope.filteredData.totals, function(t){
                return (months[t.Month-1]);
            });
            var resultsByMonth=_.groupBy($scope.filteredData.results, function(r){
                return (months[r.Month-1]);
            });

            console.log(resultsByMonth);

            var resultsByMonthArray=$.map(resultsByMonth, function(value,index){
                return [{"month":index,"results":value}];
            });

            function percentage(num, per)
            {
                return (num/100)*per;
            }

            $scope.testsDoneByMonth = $.map(testsDoneByMonth, function(value, index) {
                //rejected=getRejected(value);
                var rejected=0;
                var tested=0;
                var lessThan=0;
                var greaterThan =0;
                var Total = 0;
                var totalValue =0;
                var totalRejection =0;
                value.forEach(function(t){
                    tested=tested+t.Tested;
                    rejected=rejected+t.Rejected;
                    Total = Total + t.Total;
                });
                var results=_.findWhere(resultsByMonthArray,{month:index});
                if(results != undefined){
                    results.results.forEach(function(r){
                        lessThan=lessThan+r.Less;
                        greaterThan=greaterThan+r.Greater;

                    });
                }
                totalRejection = ((rejected/tested) * 100).toFixed(0);

                /*
                 "test":tested,"suppressed":lessThan,"suspected":greaterThan
                 */
                return [{"month":index,"rejectionRate":totalRejection}];
            });


            var ordered=$scope.testsDoneByMonth.sort(function (a, b) {
                if (months.indexOf(a.month) < months.indexOf(b.month)) return -1;
                if (months.indexOf(b.month) < months.indexOf(a.month)) return 1;
                return 0;
            });


            $scope.testTrendRejection.dataPoints=ordered;


        }

    };





    var nationalStatisticCallback=function(filter){
        if($scope.allData !== undefined){
            //get commulative
            var cummulativeTests=0;
            $scope.nationStatistics=[];

            $scope.staticAllData.forEach(function(yearData){

                if(yearData.year <=filter.year)
                {
                    console.log(yearData.year);
                    yearData.totals.forEach(function(t){
                        cummulativeTests=cummulativeTests+t.Tested;
                    });
                }
            });
            //ART
            var statistic={"name":"Total Patients on ART (By Sept 2015)", "value":731016};
            $scope.nationStatistics.push(statistic);
            var statistic={"name":"Cummulative Tests", "value":cummulativeTests};
            $scope.nationStatistics.push(statistic);



            ///Total Test Done
            var total=0;
            $scope.filteredData.totals.forEach(function(t){
                total=total+t.Tested;
            });
            /* ( "+$scope.selectMonth.name+" )"*/
            var statistic={"name":"Total Tests Done( "+filter.year+" )","value":total};
            $scope.nationStatistics.push(statistic);


            var totalLess=0;
            $scope.filteredData.results.forEach(function(r){
                totalLess=totalLess+r.Less;
            });
            var statistic={"name":"HIV viral suppression (<1000cpl/ml)","value":totalLess };
            $scope.nationStatistics.push(statistic);
            var statistic={"name":"Total Viral Load Suppression (<50cpl/ml)","value":0 };
            $scope.nationStatistics.push(statistic);

            var totalGreater=0;
            $scope.filteredData.results.forEach(function(r){
                totalGreater=totalGreater+r.Greater;
            });
            var statistic={"name":"Suspected treatment failure (>1000cpl/ml)","value":totalGreater};
            $scope.nationStatistics.push(statistic);

//
//            Total Rejected
            var totalRejected=0;
            $scope.filteredData.totals.forEach(function(t){
                totalRejected=totalRejected+t.Rejected;
            });

            var statistic={"name":"Rejected","value":totalRejected};
            $scope.nationStatistics.push(statistic);

            var pending = 0;
            $scope.filteredData.totals.forEach(function(t){
                pending=pending+t.Pending;
            });

            var statistic= {"name":"Pending", "value":pending};
            $scope.nationStatistics.push(statistic);
//
//           //Total Repeated
//            var resultsRepeated=_.filter($scope.totalRequests,function(result){
//                   return result.Repeated > 0;
//            });
//            var statistic={"name":"Total Repeated test ( "+$scope.selectMonth.name+" )","value":resultsRepeated.length};
//            $scope.nationStatistics.push(statistic);
//
//            //Total site sending sample
//            //TODO compare with request
//            var byFacility=_.groupBy($scope.totalRequests,function(r){
//                               return r.FacilityName;
//            });
//            $scope.byFacility = $.map(byFacility, function(value, index) {
//                 return [{"facility":index,"results":value}];
//            });
//
//            var statistic={"name":"Total sites sending sample  ( "+$scope.selectMonth.name+" )","value": $scope.byFacility.length};
//            $scope.nationStatistics.push(statistic);

            //Total ART site
            var statistic={"name":"Facilities Sending Samples","value":0 };
            $scope.nationStatistics.push(statistic);
        }
    };

    $scope.resultIsLoading=false;
    var groupByRegion=function(filter){
        if($scope.totalTestDone !== undefined && $scope.totalTestDone.length >0)
        {
            var requestsByRegion=_.groupBy($scope.totalRequests,function(r){
                return r.Region;
            });
            $scope.requestsByRegion = $.map(requestsByRegion, function(value, index) {
                return [{"region":index,"requests":value}];
            });

            var byRegion=_.groupBy($scope.totalTestDone,function(d){
                return d.Region;
            });

            $scope.testDoneByRegion = $.map(byRegion, function(value, index) {
                var rRegion=_.findWhere($scope.requestsByRegion,{region:index});
                return [{"region":index,"tests":value,"requests":rRegion.requests}];
            });
        }
    };

    function getTestByAgePie(data) {

        var chart= new Highcharts.chart('byAge', {
            credits:{
                enabled:false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'HIV Viral Load By Age'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    size:'70%',
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series:data
        });
        if(data.length > 0){
            $scope.showLoader = false;
        }

    }

    var testsByAgeCallBack=function(filter)
    {
        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

        var testsDoneByMonth=_.groupBy($scope.filteredData.testByAge, function(t){
            return (months[t.Month-1]);
        });
        var testsByMonth=_.groupBy($scope.filteredData.testByAge, function(r){
            return (months[r.Month-1]);
        });

        var testsByMonthArray=$.map(testsByMonth, function(value,index){
            return [{"month":index,"results":value}];
        });

        console.log(testsByMonthArray);


        $scope.testsDoneByMonth = $.map(testsDoneByMonth, function(value, index) {
            //rejected=getRejected(value);
            var rejected=0;
            var tested=0;
            var greaterThan=0;
            var lessThan = 0;
            value.forEach(function(t){
                //console.log(t);
                tested=tested+t.Tested;
                rejected=rejected+t.Rejected;
            });
            var results=_.findWhere(testsByMonthArray,{month:index});
            if(results != undefined){
                results.results.forEach(function(r){
                    lessThan = lessThan+ r.Less;
                    greaterThan=greaterThan+r.Greater;
                });
            }

            return [{"month":index,"test":tested,"suspected":greaterThan,"rejected":rejected,"suppression":lessThan}];
        });


        Array.prototype.sum = function (prop) {
            var pr = prop;
            var total = 0;
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop]
            }

            return total;
        };
        var data1 ={'adults': $scope.filteredData.testByAge.sum("GreaterThan15")};
        var data2 = {'lessThan15':$scope.filteredData.testByAge.sum("GreaterThan5LessThanAndEqualTo15")};
        var data3 = {'LessThanAndEqualTo5':$scope.filteredData.testByAge.sum("LessThanAndEqualTo5")};

        var object = angular.extend({},data1, data2,data3);

        console.log(object);

        $scope.testByAge.dataPoints=[object];

        var d=$scope.filteredData.testByAge.sum("GreaterThan15");
        var  d2= $scope.filteredData.testByAge.sum("GreaterThan5LessThanAndEqualTo15");
        var d3 = $scope.filteredData.testByAge.sum("LessThanAndEqualTo5");

        var values=[{"name": 'Age',
            "colorByPoint": true,"data":[{"name":"Adults",'y':d},{"name":"Less than 15 Years",'y':d2},{"name":"Less Than 5 Years",'y':d3,
                "sliced": true,
                "selected": true}]}];

        getTestByAgePie(values);

    };


    function getLabPerformanceDataForm(data) {
        console.log(data);
        var request = _.pluck(data,'request');
        var pending = _.pluck(data,'pending');
        var rejected = _.pluck(data,'rejected');
        var test = _.pluck(data,'test');

        var allPerformance= [{'name':'request', 'data':request},{'name':'pending','data':pending},{'name':'rejected',
            'data':rejected},{'name':'test','data':test}];


        var chart= new Highcharts.Chart('labPerformance', {
            credits:{
                enabled:false
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Lab Performance'
            },
            xAxis: {
                categories: _.pluck(data,'name')
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Tests'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'bottom',
                y: 10,
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: true
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: false,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series:allPerformance
        });


    }

    var labPerformanceCallback=function(filter){


//         var resultsByLab=_.groupBy($scope.filteredData.requests,function(r){
//              return r.Location;
//         });
//         $scope.resultsByLabArray = $.map(resultsByLab, function(value, index) {
//             return [{"name":index,"results":value}];
//          });

        var totalsByLab=_.groupBy($scope.filteredData.totals,function(t){
            return t.Location;
        });
        $scope.totalsByLabArray = $.map(totalsByLab, function(value, index) {
            var requests=0;
            var tested=0;
            var pending=0;
            var rejected=0;
            value.forEach(function(t){
                requests=requests+t.Total;
                tested=tested+t.Tested;
                pending=pending+t.Pending;
                rejected=rejected+t.Rejected;
            });
            return [{"name":index,"request":requests,"test":tested,"pending":pending,"rejected":rejected}];
        });

        $scope.labPerformance.dataPoints=$scope.totalsByLabArray;


        getLabPerformanceDataForm($scope.totalsByLabArray);


    };
    var testBySampleCallBack=function(filter){
        if($scope.allData !== undefined){
            var bySampleType=_.groupBy($scope.totalTestDone,function(r){
                return r.SpecimenType;
            });
            $scope.bySampleType = $.map(bySampleType, function(value, index) {
                var resultsRepeated=_.filter(value,function(result){
                    return result.Repeated > 0;
                });
                return [{"sample":index,"request":value.length,"repeated":resultsRepeated.length}];
            });
            $scope.testBySample.dataPoints=$scope.bySampleType;
        }
    };
    var testBreakDownCallBack=function(){
        if($scope.allData !== undefined){
            var dataPoints=[];
            $scope.staticAllData.forEach(function(data){
                var yearDataPoint={};
                yearDataPoint.year=data.year;

                var totalTests=0;
                data.totals.forEach(function(t){
                    totalTests=totalTests + t.Tested;
                });
                yearDataPoint.test=totalTests;

                var greaterThan=0;
                data.results.forEach(function(r){
                    greaterThan=greaterThan + r.Greater;
                });
                yearDataPoint.test1000=greaterThan;

                if(yearDataPoint.test > 0 || yearDataPoint.test1000 >0)
                    dataPoints.push(yearDataPoint);
            });
            $scope.testBreakDown.dataPoints=dataPoints;
        }
    };
    $scope.resultIsLoading=false;
    $scope.init=true;

    function getAgeOutCome(filter) {


        //Start group by age

        var lessThan2 = _.groupBy($scope.filteredData.ageOutCome,function(t){
            return t.ageinyears < 2;
        });
        console.log(lessThan2);
        var lessThan9 = _.groupBy(lessThan2.false,function(t){
            return (t.ageinyears >=2 & t.ageinyears <= 9)  ;
        });
        var lessThan10To14 = _.groupBy(lessThan9[0],function(t){
            return (t.ageinyears >=10 & t.ageinyears < 15)  ;
        });
        var lessThan15To19 = _.groupBy(lessThan10To14[0],function(t){
            return (t.ageinyears >=15 & t.ageinyears <=19)  ;
        });
        var lessThan20To24 = _.groupBy(lessThan15To19[0],function(t){
            return (t.ageinyears >=20 & t.ageinyears <=24)  ;
        });
        var above25 = _.groupBy(lessThan20To24[0],function(t){
            return (t.ageinyears >=25)  ;
        });



        //first API
        /* groupByAgeFunction(lessThan2.true,'Less 2');
         //Second API
         groupByAgeFunction(lessThan9[1],'2-9');
         //Third
         groupByAgeFunction(lessThan10To14[1],'10-14');
         //fourth
         groupByAgeFunction(lessThan15To19[1],'15-19');
         //Fifth
         // groupByAgeFunction(lessThan15To19[1],'15-19');
         //Sixth
         groupByAgeFunction(lessThan20To24[1],'20-24');
         //seventh
         groupByAgeFunction(above25.true,'25+');

         console.log(groupByAgeFunction(above25.true,'25+'));*/


        var all = [groupByAgeFunction(lessThan2.true,'Less 2'),
            groupByAgeFunction(lessThan9[1],'2-9'),
            groupByAgeFunction(lessThan10To14[1],'10-14'),
            groupByAgeFunction(lessThan15To19[1],'15-19'),
            groupByAgeFunction(lessThan20To24[1],'20-24'),
            groupByAgeFunction(above25.true,'25+')];

        var suppressed = _.pluck(all, 'suppressed');
        var notsuppressed = _.pluck(all,'notSuppressed');

        var ageGroups =[ {
            'name':'Suppressed',
            'data':suppressed

        },{'name':'Not Suppressed','data':notsuppressed}];



        new Highcharts.chart('ByAge', {
            credits:{
                enabled:false
            },
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: [ "Less 2", "2-9", "10-14", "15-19", "20-24", "25+"]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Tests'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                verticalAlign: 'bottom',
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: true
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>% contribution: {point.percentage:.1f}%'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series:ageGroups
        });


        //end group by age
    }

    $scope.filterChange=function(filter){
        if(!$scope.init){
            $scope.resultIsLoading=false;
        }
        if(filter.year !== undefined && filter.month !== undefined && !$scope.resultIsLoading){
            $scope.resultIsLoading=true;

            var dateRange=getDateRange(filter.year,filter.month);
            filter.dateRange=dateRange;
            $scope.filter=filter;

            angular.element('#inProgress').show();
            $scope.showLoader = true;
            $timeout(function(){
                applyFilter($scope.filter);
                testTrendCallBack($scope.filter);
                getTATDataToDisplay($scope.filter);
                getAgeOutCome($scope.filter);
            /*    testBreakDownCallBack();
                nationalStatisticCallback($scope.filter);
                testsByAgeCallBack($scope.filter);
                labPerformanceCallback($scope.filter);
                // testBySampleCallBack($scope.filter);
                suppressionCallBack($scope.filter);
                rejectionCallBack(filter);
                $scope.resultIsLoading = false;
                groupByRegionMapDataCallBack(filter);
                groupByRegion($scope.filter);*/
                angular.element('#inProgress').hide();
                $scope.showLoader = false;

            },100);

        }

    };
    $scope.data={};
    $scope.testBreakDown = {
        dataPoints:[],
        dataColumns: [
            {"id": "test", "name":"All Test", "type": "bar"},
            {"id": "test1000", "name":"> 1000cpl/ml", "type": "bar"}
        ],
        dataX: {"id": "year"}
    };

    $scope.testByAge = {
        dataPoints:[],
        dataColumns: [
            {"id": "lessThan15", "name":"Less than 15 yrs", "type": "pie"},
            {"id": "adults", "name":"Adults", "type": "pie"},
            {"id": "LessThanAndEqualTo5", "name":"less or equal to 5", "type": "pie"}
        ],
        dataX: {"id": "year"}
    };

    $scope.testTrend = {
        dataPoints:[],
        dataColumns: [
            {"id": "test", "name":"Tests", "type": "line"},
            {"id": "suspected", "name":">1000 cp/ml", "type": "bar"},
            {"id": "suppression", "name":"<1000 cp/ml", "type": "bar"}
        ],
        dataX: {"id": "month"}
    };

    $scope.testTrendSuppression = {
        dataPoints:[],
        dataColumns: [
            /*  {"id": "test", "name":"Tests", "type": "bar"},
             {"id": "suppressed", "name":"suppressed (<1000cpl/ml)", "type": "bar"},
             {"id": "suspected", "name":"Not suppressed (>1000cpl/ml)", "type": "bar"},*/
            {"id": "percentageSuppressed", "name":" ", "type": "spline"}
        ],
        dataX: {"id": "month"}
    };

    $scope.testTrendRejection = {
        dataPoints:[],
        dataColumns: [
            /*  {"id": "test", "name":"Tests", "type": "bar"},
             {"id": "suppressed", "name":"suppressed (<1000cpl/ml)", "type": "bar"},
             {"id": "suspected", "name":"Not suppressed (>1000cpl/ml)", "type": "bar"},*/
            {"id": "rejectionRate", "name":"", "type": "spline"}
        ],
        dataX: {"id": "month"}
    };


    $scope.nationStatistics=[];

    $scope.labPerformance={
        dataPoints:[],
        dataColumns: [
            {"id": "request", "name":"Requests", "type": "bar"},
            {"id": "test", "name":"Tests", "type": "bar"},
            {"id": "pending", "name":"Pending Tests", "type": "bar"}
        ],
        dataX: {"id": "name"}
    };

    $scope.testBySample = {
        dataPoints:[],
        dataColumns: [
            {"id": "request", "name":"All Test", "type": "bar"},
            {"id": "repeated", "name":"Repeated", "type": "bar"}
        ],
        dataX: {"id": "sample"}
    };


    var loadData=function () {
        var qAll = $q.defer();
        var fetchData=function(year){
            var deferred = $q.defer();
            var yearTests={};
            yearTests.year=year;
            var start='01-01-'+year;
            var end ='31-12-'+year;
            var testCode='hivvl';
            var observationCode='hivvm';

            GetYearBySampleType.get({start:start, end:end}, function (data) {

                yearTests.testingTrends = data.types;
                yearTests.tatToDisplay = data.types;

                console.log(yearTests.testingTrends);

                deferred.resolve(yearTests);



            });


       /*     GetYearTotals.query({start:start,end:end,testcode:testCode},function (totals) {

                GetYearResults.query({testcode:testCode,start:start,end:end,observationCode:observationCode}, function(results){

                    GetTestTotalByAge.query({start:start,end:end,testcode:testCode}, function(byAge){

                          GetYearBySampleType.query({start:start, end:end}, function (data) {


                        TestingTrend.query({start:start,end:end,testcode:testCode}, function(testingTrend){
                            yearTests.testingTrends=testingTrend;
                            yearTests.totals = totals;
                            console.log(data.types);
                            console.log(results);

                            yearTests.results = results;
                            yearTests.testByAge = byAge;
                            deferred.resolve(yearTests);


                            /!* GetData2.query({start:start,end:end,testcode:testCode}, function(mapTat){
                             yearTests.mapSuppressionTat =mapTat;


                             });*!/



                        });
                          });

                    })
                });
            });*/

            return deferred.promise;
        };
        var arrayFunctions=[];
        var thisYear=new Date().getFullYear();
        for(i=0;i <=1; i++)
        {
            var year=thisYear-i;
            arrayFunctions.push(fetchData(year));
        }
        $q.all(arrayFunctions).then(function(value) {
            $scope.staticAllData=value;
            $scope.allData=value;
            applyFilter($scope.filter);
            testTrendCallBack($scope.filter);
            getTATDataToDisplay($scope.filter);
            getAgeOutCome($scope.filter);
           /* testBreakDownCallBack();
            nationalStatisticCallback($scope.filter);
            testsByAgeCallBack($scope.filter);
            labPerformanceCallback($scope.filter);
             testBySampleCallBack($scope.filter);
            suppressionCallBack($scope.filter);
            rejectionCallBack($filter);*/
//                        groupByRegion($scope.filter);
        }, function(reason) {
            $scope.loadError=reason;
        });
    };
    loadData();
    angular.element('#loader').hide();


//TAT AREA

    var filterDateHelper = function(date){
        return $filter('date')(new Date(date), "yyyy-MM-dd");
    };

    var getTATDataToDisplay = function(data){
        console.log(data);





        var cr=[];
        var cr2=[];
        var cr3 =[];
        var cr4 =[];
        console.log($scope.filteredData.tatToDisplay);


        var date = new Date(new Date().setDate(new Date().getDate() - 120));
         // console.log( new Date(1487538000000));
        var tempDate=$filter('date')(new Date(date), "yyyy-MM-dd");

        console.log(tempDate);


        $scope.filterAuthorizedDate = _.filter($scope.filteredData.tatToDisplay, function(n){
         return (new Date(filterDateHelper(n.authoriseddatetime)) >= new Date(filterDateHelper(date))) && n.authoriseddatetime !='Pending' ;
       });
        console.log($scope.filterAuthorizedDate);
        /*
         console.log( $scope.filterAuthorizedDate);
         */

        if($scope.filterAuthorizedDate.length > 0 && $scope.filterAuthorizedDate !==undefined){
            var data1,data2,data3,data4;
            $scope.filterAuthorizedDate.forEach(function (t) {

               data1 = $scope.dayDiff(filterDateHelper(t.specimendatetime),filterDateHelper(t.registereddatetime));

                   //filterDateHelper(t.specimendatetime);


                data2= $scope.dayDiff(filterDateHelper(t.registereddatetime),filterDateHelper(t.analysisdatetime));
                //  var data2= $scope.dayDiff(t.ResultedDate,t.ReceivedDate);
                 data3= $scope.dayDiff(filterDateHelper(t.analysisdatetime),filterDateHelper(t.authoriseddatetime));
                 data4= $scope.dayDiff(filterDateHelper(t.receiveddatetime),filterDateHelper(t.authoriseddatetime));
                cr.push(data1);
                if(data1 <75)
                    cr.push(data1);
                if(data2 <75)
                    cr2.push(data2);
                if(data3 <75)
                    cr3.push(data3);

                cr4.push(data4);
                if(data4 <75)
                    cr4.push(data4);

            });
             console.log((cr[0]));
            collectionToLab = $scope.calculateAverage(cr);
            receivedToProcessing= $scope.calculateAverage(cr2);
            processingToDispature= $scope.calculateAverage(cr3);
            collectionToDispature= $scope.calculateAverage(cr4);
            var summary = {"cr":collectionToLab.toFixed(0),"rp":receivedToProcessing.toFixed(0),"pd":processingToDispature.toFixed(0),"cd":collectionToDispature.toFixed(0)};

            console.log(summary);



        }

    /*    $scope.filterAuthorizedDate.forEach(function (t) {

            var data1= $scope.dayDiff(t.ReceivedDate,t.SpecimenDate);
            var data2= $scope.dayDiff(t.ResultedDate,t.ReceivedDate);
            //  var data2= $scope.dayDiff(t.ResultedDate,t.ReceivedDate);
            var data3= $scope.dayDiff(t.AuthorisedDate,t.ResultedDate);
            var data4= $scope.dayDiff(t.AuthorisedDate,t.SpecimenDate);

            if(data1 <75)
                cr.push(data1);
            if(data2 <75)
                cr2.push(data2);
            if(data3 <75)
                cr3.push(data3);
            if(data4 <75)
                cr4.push(data4);

        });
        collectionToLab = $scope.calculateAverage(cr);
        receivedToProcessing= $scope.calculateAverage(cr2);
        processingToDispature= $scope.calculateAverage(cr3);
        collectionToDispature= $scope.calculateAverage(cr4);
        var summary = {"cr":collectionToLab.toFixed(0),"rp":receivedToProcessing.toFixed(0),"pd":processingToDispature.toFixed(0),"cd":collectionToDispature.toFixed(0)};
        console.log(summary);
        getTATDataToDisplay(summary);
       */ //   console.log(collectionToLab.toFixed(0),receivedToProcessing.toFixed(0),processingToDispature.toFixed(0),collectionToDispature.toFixed(0));




        console.log(summary);

        cr = parseInt(summary.cr,10);
        var rp=parseInt(summary.rp,10) + parseInt(cr);
        console.log(rp);
        var pd = parseInt(summary.pd,10) + parseInt(rp,10);
        var cd = parseInt(summary.cd,10)+ parseInt(pd,10);
        var color;
        if(cd <= 14)color="black"; else color='red';
        /**
         * Highcharts Linear-Gauge series plugin
         */
        (function (H) {
            H.seriesType('lineargauge', 'column', null, {
                setVisible: function () {
                    H.seriesTypes.column.prototype.setVisible.apply(this, arguments);
                    if (this.markLine) {
                        this.markLine[this.visible ? 'show' : 'hide']();
                    }
                },
                drawPoints: function () {
                    // Draw the Column like always
                    H.seriesTypes.column.prototype.drawPoints.apply(this, arguments);

                    // Add a Marker
                    var series = this,
                        chart = this.chart,
                        inverted = chart.inverted,
                        xAxis = this.xAxis,
                        yAxis = this.yAxis,
                        point = this.points[0], // we know there is only 1 point
                        markLine = this.markLine,
                        ani = markLine ? 'animate' : 'attr';

                    // Hide column
                    point.graphic.hide();

                    if (!markLine) {
                        var path = inverted ? ['M', 0, 0, 'L', -5, -5, 'L', 5, -5, 'L', 0, 0, 'L', 0, 0 + xAxis.len] : ['M', 0, 0, 'L', -5, -5, 'L', -5, 5, 'L', 0, 0, 'L', xAxis.len, 0];
                        markLine = this.markLine = chart.renderer.path(path)
                            .attr({
                                'fill': series.color,
                                'stroke': series.color,
                                'stroke-width': 1
                            }).add();
                    }
                    markLine[ani]({
                        translateX: inverted ? xAxis.left + yAxis.translate(point.y) : xAxis.left,
                        translateY: inverted ? xAxis.top : yAxis.top + yAxis.len -  yAxis.translate(point.y)
                    });
                }
            });
        }(Highcharts));

        Highcharts.chart('tat', {
                exporting: { enabled: false },
                credits:{
                    enabled:false
                },

                chart: {
                    type: 'lineargauge',
                    inverted: true,
                    height: 70,
                    width:350
                },
                title: {
                    text: ''
                },
                xAxis: {
                    lineColor: '#C0C0C0',
                    labels: {
                        enabled: false
                    },
                    tickLength: 0
                },
                yAxis: {
                    min: 0,
                    max:pd,
                    tickLength: 5,
                    tickWidth: 1,
                    tickColor: color,
                    gridLineColor: '#C0C0C0',
                    gridLineWidth: 1,
                    /*   minorTickInterval: 5,
                     minorTickWidth: 1,*/
                    minorTickLength: 5,
                    minorGridLineWidth: 0,

                    title: null,
                    labels: {
                        format: '{value}'
                    },
                    plotBands: [{
                        from: 0,
                        to: cr,
                        color:'rgba(0,255,0,0.5)'
                    }, {
                        from: cr,
                        to: rp,
                        color: 'rgba(255,255,0,0.5)'
                    }, {
                        from: rp,
                        to: pd,
                        color: 'rgb(149, 206, 255)'
                    }/*, {
                     from: pd,
                     to: cd,
                     color: '#95CEFF'
                     }*/]
                },
                legend: {
                    enabled: false
                },

                series: [{
                    data: [cd],
                    color: color,
                    dataLabels: {
                        enabled: true,
                        align: 'center',
                        format: '{point.y}',
                        color:color
                    }
                }]

            }, // Add some life
            function (chart) {
                setInterval(function () {
                   new Highcharts.each(chart.series, function (serie) {
                        var point = serie.points[0];
                        /*     newVal,
                         inc = (Math.random() - 0.5) * 20;

                         newVal = point.y + inc;
                         if (newVal < 0 || newVal > pd) {
                         newVal = point.y - inc;
                         }*/

                        point.update(Math.floor(pd));
                    });
                }, 2000);

            });


    };


}

DashFuctBoardController.resolve = {


};