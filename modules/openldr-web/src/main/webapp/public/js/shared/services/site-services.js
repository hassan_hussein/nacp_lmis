var siteServices=angular.module('site.services', ['ngResource']);
var update = {update: {method: 'PUT'}};

siteServices.factory('RegisterClient', function ($resource) {
  return $resource('/site/register/client.json', {},update);
});

siteServices.factory('GetByUsername', function ($resource) {
  return $resource('/site/client/get-by-username.json', {},{});
});

siteServices.factory('GetByCellPhone', function ($resource) {
  return $resource('/site/client/get-by-cellphone.json', {},{});
});

siteServices.factory('GetByEmail', function ($resource) {
  return $resource('/site/client/get-by-email.json', {},{});
});

siteServices.factory('VerifyAccount', function ($resource) {
  return $resource('/site/client/verify.json', {},{});
});

siteServices.value('version', '@version@');

siteServices.factory('Locales', function ($resource) {
  return $resource('/locales.json', {}, {});
});

siteServices.factory('ChangeLocale', function ($resource) {
  return $resource('/changeLocale.json', {}, update);
});

siteServices.factory('GetData', function ($resource) {
  return $resource('/public/js/shared/directives/tatData.json', {}, {});
});

siteServices.factory('GetData2', function ($resource) {
  return $resource('/public/js/shared/directives/tatData.json', {}, {});
});



siteServices.factory('ChangeLocale', function ($resource) {
  return $resource('/changeLocale.json', {}, update);
});

siteServices.factory('leaflet', function ($resource) {
  return $resource('/public/coordinate.json', {}, {});
});

siteServices.factory('Authenticate', function ($resource) {
  return $resource('http://197.149.178.42/mohsw/00000000000000000000000000000000/v1/json/authenticate', {}, {});
});

siteServices.factory('Regions', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/regions', {}, {});
});

siteServices.factory('DistrictsByRegion', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/districts', {}, {});
});

siteServices.factory('FacilitiesByDistrict', function ($resource) {
  return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/facilities', {}, {});
});

siteServices.factory('TestTypes', function ($resource) {
  return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/tests', {}, {});
});

siteServices.factory('Tests', function ($resource) {
  return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/tat', {}, {});
});

siteServices.factory('TestingTrend', function ($resource) {
  return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/testingtrends', {testcode:'@testCode',start:'@start',end:'end'}, {});
});

siteServices.factory('TestsResults', function ($resource) {
  return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/results', {}, {});
});

siteServices.factory('MapData', function ($resource) {
  return $resource('/public/js/shared/directives/mapData.json', {}, {});
});

siteServices.factory('GetYearTotals', function ($resource) {
  return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/testtotals', {testcode:'@testCode',start:'@start',end:'end'}, {});
});

siteServices.factory('GetYearResults', function ($resource) {
  return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/testbreakdown', {testcode:'@testCode',start:'@start',end:'end',observationcode:'@observationCode'}, {});
});

siteServices.factory('GetYearBySampleType1', function($resource){
  return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/results', {testcode:'@testCode',start:'2017-05-01',end:'2017-05-30'},{});
});

siteServices.factory('GetTestTotalByAge', function($resource){
  return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/testtotalsbyage',{testcode:'@testCode',start:'@start',end:'end'},{});
});

//From our server after synchronization from the repository
siteServices.factory('GetYearBySampleType', function($resource){
  return $resource('/rest-api/getAllBySampleType',{start:'2017-05-01',end:'2017-05-30'},{});
});

