
function FacilitySearchFilterController($scope, Facilities) {

  $scope.type = {};
  $scope.zone = {};

  $scope.showFacilitySearchResults = function () {
    if (!$scope.facilitySearchParam) return;

    $scope.facilityQuery = $scope.facilitySearchParam.trim();
    Facilities.get({
        "searchParam": $scope.facilityQuery,
        "facilityTypeId": $scope.type.id,
        "geoZoneId": $scope.zone.id,
        "virtualFacility": $scope.extraParams.virtualFacility,
        "enabled": $scope.extraParams.enabled},
      function (data) {
        $scope.facilityList = data.facilityList;
        $scope.facilityResultCount = isUndefined($scope.facilityList) ? 0 : $scope.facilityList.length;
        $scope.resultCount = $scope.facilityResultCount;
        $scope.message = data.message;
      });
  };

  $scope.triggerSearch = function (event) {
    if (event.keyCode === 13) {
      $scope.showFacilitySearchResults();
    }
  };

  $scope.clearFacilitySearch = function () {
    $scope.facilitySearchParam = undefined;
    $scope.facilityList = undefined;
    $scope.facilityResultCount = undefined;
    angular.element('#searchFacility').focus();
  };

  $scope.clearVisibleFilters = function () {
    $scope.type = {};
    $scope.zone = {};
  };

  $scope.associate = function (facility) {
    console.log(facility);
    $scope.$parent.associate(facility);
    $scope.clearFacilitySearch();
    $scope.clearVisibleFilters();
    $scope.$broadcast('singleSelectSearchCleared');
  };
}
