/**
 * Created by hassan on 4/18/16.
 */

function TrendFunctController($scope, $filter,TestsResults,Tests,$q, $timeout,GetYearTotals,GetYearResults,GetYearBySampleType,GetTestTotalByAge) {

    $scope.defaultYear=new Date().getFullYear();
    $scope.defaultMonth=0; //new Date().getMonth() +1;

    var  getSuspectedTreatmentFailure = function (results)
    {
        var resultsWithCPL=_.where(results,{ObservationCode:"HIVVM"});
        var targetNotDetected=[];

        resultsCPLGreaterThan1000= _.filter(resultsWithCPL,
            function(r) {
                var testResult=r.TestResult.replace(/\D/g,'');
                testResult=testResult.trim();
                if(testResult ==='')
                {
                    targetNotDetected.push(testResult);
                }else
                {
                    return parseInt(testResult,10) > 1000;
                }
            });
        return resultsCPLGreaterThan1000;
    };

    var getDateRange=function (year,month){
        var dateRange={};
        var lastDate = new Date(year, month, 0);
        $scope.selectMonth=_.findWhere($scope.monthsToDisplay,{id:month});
        if(month >=1 && month <=9)
        {
            dateRange.startDate='01-0'+month+'-'+year;
            dateRange.endDate=lastDate.getDate()+'-0'+month+'-'+year;
        }
        else if(month >9)
        {
            dateRange.startDate='01-'+month+'-'+year;
            dateRange.endDate=lastDate.getDate()+'-'+month+'-'+year;
        }
        else{
            dateRange.startDate='01-01-'+year;
            dateRange.endDate='31-12-'+year;
            $scope.selectMonth={"name":"Jan-Dec", "id":0};
        }
        return dateRange;

    };

    var applyFilter=function(filter){
        //filter {location: "all", year: 2016, month: 0, dateRange:{endDate:"31-12-2016",startDate:"01-01-2016"
        console.log($scope.staticAllData);
        var allData=[];
        angular.copy($scope.staticAllData,allData);
        console.log(allData);
        if(filter.year !==undefined){
            $scope.filteredData={};
            $scope.filteredYearData=_.findWhere(allData,{year:filter.year});
            $scope.filteredData=$scope.filteredYearData;
            $scope.filteredData.month=0;
            $scope.filteredData.location="all";
        }


        if(filter.month > 0){
            console.log('apply month');
            var totals=_.where($scope.filteredData.totals,{Month:filter.month});
            var results=_.where($scope.filteredData.results,{Month:filter.month});
            var testByAge=_.where($scope.filteredData.testByAge,{Month:filter.month});
            $scope.filteredData.totals=totals;
            $scope.filteredData.results=results;
            $scope.filteredData.testByAge= testByAge;
            $scope.filteredData.month=filter.month;
        }

        if(filter.location !=="all"){
            console.log('apply location');
            var totals=_.filter($scope.filteredData.totals,function(t){
                return t.Location.toLowerCase() == filter.location.toLowerCase();
            });

            var results=_.filter($scope.filteredData.results,function(r){
                return r.Location.toLowerCase() == filter.location.toLowerCase();
            });

            var testByAge=_.filter($scope.filteredData.testByAge,function(r){
                return r.Location.toLowerCase() == filter.location.toLowerCase();
            });
            $scope.filteredData.totals=totals;
            $scope.filteredData.results=results;
            $scope.filteredData.testByAge=testByAge;

            $scope.filteredData.location=filter.location;
        }
        console.log($scope.filteredData);

    };

    var testTrendCallBack=function(filter){
        if($scope.allData !== undefined)
        {
            var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

            var testsDoneByMonth=_.groupBy($scope.filteredData.totals, function(t){
                return (months[t.Month-1]);
            });
            var resultsByMonth=_.groupBy($scope.filteredData.results, function(r){
                return (months[r.Month-1]);
            });

            var resultsByMonthArray=$.map(resultsByMonth, function(value,index){
                return [{"month":index,"results":value}];
            });
            console.log(resultsByMonthArray);

            $scope.testsDoneByMonth = $.map(testsDoneByMonth, function(value, index) {
                //rejected=getRejected(value);
                var rejected=0;
                var tested=0;
                var greaterThan=0;
                value.forEach(function(t){
                    tested=tested+t.Tested;
                    rejected=rejected+t.Rejected;
                });
                var results=_.findWhere(resultsByMonthArray,{month:index});
                if(results != undefined){
                    results.results.forEach(function(r){
                        greaterThan=greaterThan+r.Greater;
                    });
                }

                return [{"month":index,"test":tested,"suspected":greaterThan,"rejected":rejected}];
            });

            var ordered=$scope.testsDoneByMonth.sort(function (a, b) {
                if (months.indexOf(a.month) < months.indexOf(b.month)) return -1;
                if (months.indexOf(b.month) < months.indexOf(a.month)) return 1;
                return 0;
            });
            $scope.testTrend.dataPoints=ordered;

        }
    };

    var nationalStatisticCallback=function(filter){
        if($scope.allData !== undefined){
            //get commulative
            var cummulativeTests=0;
            $scope.nationStatistics=[];

            $scope.staticAllData.forEach(function(yearData){

                if(yearData.year <=filter.year)
                {
                    console.log(yearData.year);
                    yearData.totals.forEach(function(t){
                        cummulativeTests=cummulativeTests+t.Tested;
                    });
                }
            });
            var statistic={"name":"Cummulative Tests", "value":cummulativeTests};
            $scope.nationStatistics.push(statistic);

            //ART
            var statistic={"name":"Total Patients on ART (By Sept 2015)", "value":731016};
            $scope.nationStatistics.push(statistic);

            ///Total Test Done
            var total=0;
            $scope.filteredData.totals.forEach(function(t){
                total=total+t.Tested;
            });
            var statistic={"name":"Total Tests Done    ( "+$scope.selectMonth.name+" )","value":total};
            $scope.nationStatistics.push(statistic);


            var totalLess=0;
            $scope.filteredData.results.forEach(function(r){
                totalLess=totalLess+r.Less;
            });
            var statistic={"name":"HIV viral suppression (<1000cpl/ml)  ( "+$scope.selectMonth.name+" )","value":totalLess };
            $scope.nationStatistics.push(statistic);

            var totalGreater=0;
            $scope.filteredData.results.forEach(function(r){
                totalGreater=totalGreater+r.Greater;
            });
            var statistic={"name":"Suspected treatment failure (>1000cpl/ml)  ( "+$scope.selectMonth.name+" )","value":totalGreater};
            $scope.nationStatistics.push(statistic);

//
//            Total Rejected
            var totalRejected=0;
            $scope.filteredData.totals.forEach(function(t){
                totalRejected=totalRejected+t.Rejected;
            });

            var statistic={"name":"Rejected ( "+$scope.selectMonth.name+" )","value":totalRejected};
            $scope.nationStatistics.push(statistic);

            var pending = 0;
            $scope.filteredData.totals.forEach(function(t){
                pending=pending+t.Pending;
            });

            var statistic= {"name":"Pending (" +$scope.selectMonth.name+" )", "value":pending};
            $scope.nationStatistics.push(statistic);
//
//           //Total Repeated
//            var resultsRepeated=_.filter($scope.totalRequests,function(result){
//                   return result.Repeated > 0;
//            });
//            var statistic={"name":"Total Repeated test ( "+$scope.selectMonth.name+" )","value":resultsRepeated.length};
//            $scope.nationStatistics.push(statistic);
//
//            //Total site sending sample
//            //TODO compare with request
//            var byFacility=_.groupBy($scope.totalRequests,function(r){
//                               return r.FacilityName;
//            });
//            $scope.byFacility = $.map(byFacility, function(value, index) {
//                 return [{"facility":index,"results":value}];
//            });
//
//            var statistic={"name":"Total sites sending sample  ( "+$scope.selectMonth.name+" )","value": $scope.byFacility.length};
//            $scope.nationStatistics.push(statistic);

            //Total ART site
            var statistic={"name":"Total ART site (by Dec 2015)","value":1523 };
            $scope.nationStatistics.push(statistic);
        }
    };

    $scope.resultIsLoading=false;
    var groupByRegion=function(filter){
        if($scope.totalTestDone !== undefined && $scope.totalTestDone.length >0)
        {
            var requestsByRegion=_.groupBy($scope.totalRequests,function(r){
                return r.Region;
            });
            $scope.requestsByRegion = $.map(requestsByRegion, function(value, index) {
                return [{"region":index,"requests":value}];
            });

            var byRegion=_.groupBy($scope.totalTestDone,function(d){
                return d.Region;
            });

            $scope.testDoneByRegion = $.map(byRegion, function(value, index) {
                var rRegion=_.findWhere($scope.requestsByRegion,{region:index});
                return [{"region":index,"tests":value,"requests":rRegion.requests}];
            });
        }
    };
    var testsByAgeCallBack=function(filter)
    {

        Array.prototype.sum = function (prop) {
            var pr = prop;
            var total = 0;
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop]
            }

            return total;
        };

        var data1 ={'adults': $scope.filteredData.testByAge.sum("GreaterThan15")};
        var data2 = {'lessThan15':$scope.filteredData.testByAge.sum("GreaterThan5LessThanAndEqualTo15")};
        var data3 = {'LessThanAndEqualTo5':$scope.filteredData.testByAge.sum("LessThanAndEqualTo5")};

        var object = angular.extend({},data1, data2,data3);

        console.log(object);

        $scope.testByAge.dataPoints=[object];
    };
    var labPerformanceCallback=function(filter){


//         var resultsByLab=_.groupBy($scope.filteredData.requests,function(r){
//              return r.Location;
//         });
//         $scope.resultsByLabArray = $.map(resultsByLab, function(value, index) {
//             return [{"name":index,"results":value}];
//          });

        var totalsByLab=_.groupBy($scope.filteredData.totals,function(t){
            return t.Location;
        });
        $scope.totalsByLabArray = $.map(totalsByLab, function(value, index) {
            var requests=0;
            var tested=0;
            var pending=0;
            value.forEach(function(t){
                requests=requests+t.Total;
                tested=tested+t.Tested;
                pending=pending+t.Pending;
            });
            return [{"name":index,"request":requests,"test":tested,"pending":pending}];
        });

        $scope.labPerformance.dataPoints=$scope.totalsByLabArray;

    };
    var testBySampleCallBack=function(filter){
        if($scope.allData !== undefined){
            var bySampleType=_.groupBy($scope.totalTestDone,function(r){
                return r.SpecimenType;
            });
            $scope.bySampleType = $.map(bySampleType, function(value, index) {
                var resultsRepeated=_.filter(value,function(result){
                    return result.Repeated > 0;
                });
                return [{"sample":index,"request":value.length,"repeated":resultsRepeated.length}];
            });
            $scope.testBySample.dataPoints=$scope.bySampleType;
        }
    };
    var testBreakDownCallBack=function(){
        if($scope.allData !== undefined){
            var dataPoints=[];
            $scope.staticAllData.forEach(function(data){
                var yearDataPoint={};
                yearDataPoint.year=data.year;

                var totalTests=0;
                data.totals.forEach(function(t){
                    totalTests=totalTests + t.Tested;
                });
                yearDataPoint.test=totalTests;

                var greaterThan=0;
                data.results.forEach(function(r){
                    greaterThan=greaterThan + r.Greater;
                });
                yearDataPoint.test1000=greaterThan;

                if(yearDataPoint.test > 0 || yearDataPoint.test1000 >0)
                    dataPoints.push(yearDataPoint);
            });
            $scope.testBreakDown.dataPoints=dataPoints;
        }
    };
    $scope.resultIsLoading=false;
    $scope.init=true;

    $scope.filterChange=function(filter){
        if(!$scope.init){
            $scope.resultIsLoading=false;
        }
        if(filter.year !== undefined && filter.month !== undefined && !$scope.resultIsLoading){

            $scope.resultIsLoading=true;
            var dateRange=getDateRange(filter.year,filter.month);
            filter.dateRange=dateRange;
            $scope.filter=filter;
            angular.element('#loader').show();
            $timeout(function(){
                applyFilter($scope.filter);
                testBreakDownCallBack();
                nationalStatisticCallback($scope.filter);
                testsByAgeCallBack($scope.filter);
                labPerformanceCallback($scope.filter);
                // testBySampleCallBack($scope.filter);
                testTrendCallBack($scope.filter);
//               groupByRegion($scope.filter);
                angular.element('#loader').hide();
            },100);

        }

    };
    $scope.data={};
    $scope.testBreakDown = {
        dataPoints:[],
        dataColumns: [
            {"id": "test", "name":"All Test", "type": "bar"},
            {"id": "test1000", "name":"> 1000cpl/ml", "type": "bar"}
        ],
        dataX: {"id": "year"}
    };

    $scope.testByAge = {
        dataPoints:[],
        dataColumns: [
            {"id": "lessThan15", "name":"Less than 15 yrs", "type": "pie"},
            {"id": "adults", "name":"Adults", "type": "pie"},
            {"id": "LessThanAndEqualTo5", "name":"less or equal to 5", "type": "pie"}
        ],
        dataX: {"id": "year"}
    };

    $scope.testTrend = {
        dataPoints:[],
        dataColumns: [
            {"id": "test", "name":"Tests", "type": "line"},
            {"id": "suspected", "name":"Suspected treatment failure", "type": "line"},
            {"id": "rejected", "name":"Rejected", "type": "line"}
        ],
        dataX: {"id": "month"}
    };


    $scope.nationStatistics=[];

    $scope.labPerformance={
        dataPoints:[],
        dataColumns: [
            {"id": "request", "name":"Requests", "type": "bar"},
            {"id": "test", "name":"Tests", "type": "bar"},
            {"id": "pending", "name":"Pending Tests", "type": "bar"}
        ],
        dataX: {"id": "name"}
    };

    $scope.testBySample = {
        dataPoints:[],
        dataColumns: [
            {"id": "request", "name":"All Test", "type": "bar"},
            {"id": "repeated", "name":"Repeated", "type": "bar"}
        ],
        dataX: {"id": "sample"}
    };


    var loadData=function () {
        var qAll = $q.defer();
        var fetchData=function(year){
            var deferred = $q.defer();
            var yearTests={};
            yearTests.year=year;
            var start='01-01-'+year;
            var end ='31-12-'+year;
            var testCode='hivvl';
            var observationCode='hivvm';

            GetYearTotals.query({start:start,end:end,testcode:testCode},function (totals) {
                GetYearResults.query({testcode:testCode,start:start,end:end,observationCode:observationCode}, function(results){
                    GetTestTotalByAge.query({start:start,end:end,testcode:testCode}, function(byAge){
                        /*  GetYearBySampleType.query({start:start, end:end}, function (data) {
                         console.log("data dada");
                         console.log(data);
                         });*/

                        yearTests.totals = totals;
                        yearTests.results = results;
                        yearTests.testByAge = byAge;
                        deferred.resolve(yearTests);
                    })
                });
            });

            return deferred.promise;
        };
        var arrayFunctions=[];
        var thisYear=new Date().getFullYear();
        for(i=0;i <=1; i++)
        {
            var year=thisYear-i;
            arrayFunctions.push(fetchData(year));
        }
        $q.all(arrayFunctions).then(function(value) {
            $scope.staticAllData=value;
            $scope.allData=value;
            applyFilter($scope.filter);
            testBreakDownCallBack();
            nationalStatisticCallback($scope.filter);
            testsByAgeCallBack($scope.filter);
            labPerformanceCallback($scope.filter);
            // testBySampleCallBack($scope.filter);
            testTrendCallBack($scope.filter);
//                        groupByRegion($scope.filter);
        }, function(reason) {
            $scope.loadError=reason;
        });
    };
    loadData();

}

TrendFunctController.resolve = {


};