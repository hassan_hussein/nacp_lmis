site.directive('mapFilter', ['MapData',
    function (MapData) {
        return {
            restrict: 'E',
            scope: {
                region: '=cmModel',
                onChange: '&'
            },
            controller: function ($scope) {

                function drawMap(){
                    console.log($scope.$parent.mapData);
                    var chart = new Highcharts.Map({
                        credits: {
                            enabled: false
                        },
                        chart: {
                                 renderTo: 'container',
                                 backgroundColor:"#EEF1F5"
                        },
                        mapNavigation: {
                             enabled: true,
                             buttonOptions: {
                                   verticalAlign: 'bottom'
                             }
                            },

                            colorAxis: {
                                   dataClasses: [{color:'#083D78',from:'bugando',name:'Bugando',to:'bugando'},
                                                 {color:'#61B861',from:'kcmc',name:'KCMC',to:'kcmc'},
                                                 {color:'#FD7E0E',from:'muhimbili',name:'Muhimbili',to:'muhimbili'},
                                                 {color:'#7A91B1',from:'mbeya',name:'Mbeya RH',to:'mbeya'},
                                                 {color:'#17888f',from:'temeke',name:'Temeke',to:'temeke'}
                                       ,
                                       {color:'#6EBFD8',from:'mtmeru',name:'Mount Meru',to:'mtmeru'},
                                       {color:'purple',from:'iringa',name:'Iringa',to:'iringa'},
                                       {color:'orchid',from:'morogoro',name:'Morogoro',to:'morogoro'},
                                       {color:'silver',from:'ligula',name:'Ligula',to:'ligula'},
                                       {color:'tan',from:'songea',name:'Songea',to:'songea'}
                                                 ]
                            },
                            title : {
                                        text : ''
                            },


                            series : [{
                                     data : $scope.$parent.mapData,
                                     mapData: Highcharts.maps['countries/tz/tz-all'],
                                     joinBy: 'hc-key',
                                     name: 'Test',
                                     states: {
                                           hover: {
                                           color: '#BADA55'
                                    }

                                  },
                                  tooltip: {
                                           pointFormat:
                                           '<b>Suppression Rate: {point.suppression}</b><br/>' ,

                                          /* 'Tests Done:<b>{point.tests} </b><br /> Rejected:<b>{point.rejected} </b><br /> Number of Sites:<b>{point.sites}</b>',*/
                                           hideDelay: 20000
                                        },
                                    dataLabels: {
                                             enabled: true,
                                             format: '{point.name}'
                                         }
                                     }],
                           plotOptions:{

                              series:{
                                cursor: 'pointer',
                                 point:{

                                  events:{
                                  click: function(){
                                             $scope.region=this;
                                             $scope.$apply();
                                         }
                                      }
                                    }
                               }
                           }
                     });
                 }

                var loadData= function(){
                    MapData.query(function(data){
                         $scope.$parent.mapData =data;
                         drawMap();
                    });
                };

                var addTestResults=function(request){
                    if($scope.$parent.mapData !== undefined)
                    {
                        $scope.$parent.mapData.forEach(function(d){
                            var regionTestsResults=_.findWhere(request,{region:d.region});

                            console.log(d.region);

                            if(regionTestsResults !== undefined)
                            {
                              d.tests=regionTestsResults.tests.length;

                              //Rejected
                              var rejected=_.where(regionTestsResults.tests,{Rejected:'Yes'});
                              if(rejected !== undefined)
                                d.rejected=rejected.length;
                               else d.rejected=0;

                              //No of sites
                              var byFacility=_.groupBy(regionTestsResults.tests,function(r){
                                      return r.FacilityName;
                              });
                              var byFacilityArray = $.map(byFacility, function(value, index) {
                                    return [{"facility":index,"results":value}];
                              });

                              if(byFacilityArray !== undefined)
                                d.sites=byFacilityArray.length;
                              else
                                d.sites=0;

                            }
                            else{d.tests=0}


                        });
                        drawMap();
                    }
                };
                loadData();
                $scope.$watch('region', function (newValues, oldValues) {
                   $scope.onChange();
                });

                $scope.$parent.$watch('mapSuppressionTat', function(value){
                    var groupByRegion = _.groupBy($scope.$parent.mapSuppressionTat,'Region');

                    $scope.requestsByRegion = $.map(groupByRegion, function(value, index) {

                        var totalFacility = 0;

                        totalFacility = _.uniq(_.pluck(value,'Facility'));
                        var res =_.pluck(value,'Result');
                        var evens = _.filter(res, function(value,index){
                        /*    var extracted = data.replace(/ /g, '');
                            if(extracted.charAt(0) === '<')
                            {
                                result = extracted.substr(1);
                            }else
                                result= extracted ;*/


                                return value.trim();

                        });



                        var rejected = 0;
                        var received =0;
                        var authorized = 0;
                        var requests =0;
                        var result=[];
                        value.forEach(function (t) {
                            // console.log(t);


                            var extracted = t.Result.replace(/ /g, '');
                            var myString = extracted.replace(/</g,'') || extracted.replace(/>/g,'') || extracted.replace(/cp\/\mL/g,'');

                                if(myString.charAt(0) === '<')
                                {
                                    result.push(myString.substr(1));
                                }else
                                 result.push(myString);

                            received = received + t.Received;
                            requests = requests + t.Requests;
                            authorized = extracted;
                            rejected = rejected + t.Rejected;

                        });

                        function isNumeric(n) {
                            return !isNaN(parseInt(n, 10));
                        }
                        var suppressed =0;
                        var notSuppressed = 0;
                        result.forEach(function(data,index){
                            if(isNumeric(data)){
                                if(parseInt(data,10) < 1000){
                                    suppressed = suppressed +parseInt(data,10);
                                }else
                                {
                                    notSuppressed = notSuppressed +parseInt(data);
                                }
                            }

                        });


                        var tot = parseInt(suppressed) + parseInt(notSuppressed);
                        var percentage =  ((parseInt(suppressed)/parseInt(tot)));

                        return [{"region":index,"requests":value,
                            "facilitySendingSample":totalFacility.length,
                        "rejected":rejected,"received":received,"result":result,"total":Math.round(percentage*100, 2)}];
                    });
                      addTestResults($scope.requestsByRegion);
                });
            },
            templateUrl: 'filter-map-template'
        };
    }
]);

site.directive('yearFilter', [
    function () {
        return {
            restrict: 'E',
            scope: {
                year: '=cmModel',
                range: '=range',
                default :'=default',
                onChange: '&'
            },
            controller: function ($scope) {
                var thisYear=new Date().getFullYear();
                var years=[thisYear];
                var range=($scope.range !== undefined)?parseInt($scope.range,10):5;
                for(i=1;i < range;i++){
                    years.push(thisYear-i);
                }

                $scope.years=years.reverse();
                $scope.setYear=function(year){
                   $scope.$parent.filter.period=undefined;
                   $scope.year=year;
                   $scope.$parent.init=false;
                };
                if($scope.default !==undefined){
                     $scope.year = $scope.default;
                }else{
                     $scope.year = new Date().getFullYear();
                }
                $scope.$watch('year', function (newValues, oldValues) {
                   $scope.onChange();
                });
            },
            templateUrl: 'filter-year-template'
        };
    }
]);

site.directive('monthFilter', [
    function () {
        return {
            restrict: 'E',
            scope: {
                month: '=cmModel',
                default :'=default',
                onChange: '&'
            },
            controller: function ($scope) {
                $scope.months=[{"name":"All", "id":0},{"name":"Jan", "id":1},{"name":"Feb", "id":2},{"name":"Mar", "id":3},
                               {"name":"Apr", "id":4},{"name":"May", "id":5},{"name":"Jun", "id":6},
                               {"name":"Jul", "id":7},{"name":"Aug", "id":8},{"name":"Sep", "id":9},
                               {"name":"Oct", "id":10},{"name":"Nov", "id":11},{"name":"Dec", "id":12}];
                $scope.$parent.monthsToDisplay=$scope.months;
                $scope.setMonth=function(month){
                    console.log(month);
                   $scope.$parent.filter.period=undefined;
                   $scope.$parent.init=false;
                   $scope.month=month;
                };
                if($scope.default !==undefined){
                     $scope.month = $scope.default;
                }else{
                     $scope.month = new Date().getMonth() +1;
                }
                $scope.$watch('month', function (newValues, oldValues) {
                   $scope.onChange();
                });
            },
            templateUrl: 'filter-month-template'
        };
    }
]);

site.directive('periodFilter', [
    function () {
        return {
            restrict: 'E',
            scope: {
                period: '=cmModel',
                onChange: '&'
            },
            controller: function ($scope) {
                $scope.periods=[{"name":"Last 6 months", "id":6},{"name":"Last 3 months", "id":3},{"name":"All", "id":0}];
                $scope.setPeriod=function(period){
                   $scope.$parent.filter.year=undefined;
                   $scope.$parent.filter.month=undefined;
                   $scope.period=period;
                };

                $scope.$watch('period', function (newValues, oldValues) {
                   $scope.onChange();
                });
            },
            templateUrl: 'filter-period-template'
        };
    }
]);

site.directive('locationFilter', [
    function () {
        return {
            restrict: 'E',
            scope: {
                location: '=cmModel',
                onChange: '&',
                default: '=default'
            },
            controller: function ($scope) {
                $scope.locations=[{"name":"All Labs","id":"all"},
                                  {"name":"Bugando","id":"bugando"},
                                  {"name":"KCMC","id":"kcmc"},
                                  {"name":"Temeke","id":"temeke"},
                                  {"name":"NHL-QATC","id":"nhl-qatc"},
                                  {"name":"Mbeya","id":"mbeya"},
                                  {"name":"Songea","id":"songea"},
                                  {"name":"Morogoro","id":"morogoro"},
                                  {"name":"Ligula","id":"ligula"},
                                  {"name":"Mount Meru","id":"mtmeru"},
                                  {"name":"Iringa","id":"iringa"}

                                            ];


                $scope.setLocation=function(location){
                   $scope.location=location;
                   $scope.$parent.init=false;
                };
                if($scope.default !==undefined){
                    $scope.location = $scope.location;
                }else{
                    $scope.location = "all";
                }

                $scope.$watch('location', function (newValues, oldValues) {
                   $scope.onChange();
                });
            },
            templateUrl: 'filter-location-template'
        };
    }
]);
site.filter('ageFilter', function() {
     function calculateAge(birthday) { // birthday is a date
//     console.log(birthday);
       if(birthday !== undefined)
       {
         var ageDifMs = Date.now() - new Date(birthday).getTime();
         var ageDate = new Date(ageDifMs); // miliseconds from epoch
         return Math.abs(ageDate.getUTCFullYear() - 1970)+ ' years';
         }
         else{ return '';}
     }
     function monthDiff(d1, d2) {
       if (d1 < d2){
        var months = d2.getMonth() - d1.getMonth();
        return months <= 0 ? 0 : months;
       }
       return 0;
     }
     return function(birthdate) {
           var age = calculateAge(birthdate);
           if (age == 0)
             return monthDiff(birthdate, new Date()) + ' months';
           return age;
     };
});
