
var site = angular.module('site', ['site.services','ui.bootstrap.modal', 'ui.bootstrap.dialog','ngRoute',
    'ngCookies','angularCombine','ui.bootstrap','gridshore.c3js.chart','textAngular','leaflet-directive'
]);

site.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/home', {controller: DashFuctBoardController, templateUrl: '/public/pages/dashboard/partials/dashboard2.html',reloadOnSearch:true, resolve: DashFuctBoardController.resolve}).
        when('/trend', {controller: DashFuctBoardController, templateUrl: '/public/pages/dashboard/partials/trend2.html',reloadOnSearch:true, resolve: DashFuctBoardController.resolve}).
        when('/age', {controller: DashFuctBoardController, templateUrl: '/public/pages/dashboard/partials/age.html',reloadOnSearch:true, resolve: DashFuctBoardController.resolve}).
        when('/newDash', {controller: DashFuctBoardController, templateUrl: '/public/pages/dashboard/partials/new-dash.html',reloadOnSearch:true, resolve: DashFuctBoardController.resolve}).
        otherwise({redirectTo: '/newDash'});
}]).config(function(angularCombineConfigProvider) {
    angularCombineConfigProvider.addConf(/filter-/, '/public/pages/shared/filters.html');
});
