
function RegimenController($scope,$location,regimenLine,saveRegimen,regimenById){

    $scope.regimen = regimenById;
    $scope.regimen = {};
    $scope.regimenId=null;
    $scope.regimenLines= _.uniq(regimenLine, 'name');

    $scope.cancel = function(){
        $scope.$parent.regimenId = null;
        $scope.$parent.deleteRegimenItem = false;
        $location.path('/');
    };

    var success = function (data) {

        $scope.error = "";
        $scope.$parent.message = data.success;
        $scope.$parent.regimenId = data.status.id;

        $scope.$parent.deleteRegimenItem = false;
        $scope.showError = false;
        $location.path('');

    };

    var error = function (data) {
        $scope.$parent.message = "";
        $scope.error = data.data.error;
        $scope.showError = true;
    };


    $scope.save = function () {

        if ($scope.regimenForm.$error.name || $scope.regimenForm.$error.code) {
            $scope.showError = true;
            $scope.error = 'form.error';
            $scope.message = "";
            return;
        }

        if ($scope.regimen.id !== undefined ) {

            saveRegimen.update({id: $scope.regimen.id}, $scope.regimen, success, error);
        }
        else {
            saveRegimen.save({}, $scope.regimen, success, error);
        }
    };



}

RegimenController.resolve= {

    regimenLine: function ($q, GetRegimenLine, $route, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            GetRegimenLine.get({}, function (data) {
               // console.log(data);

                deferred.resolve(data.regimenLines);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    },

    regimenById: function ($q, GetRegimenById, $route, $timeout) {
        console.log("called");
        var id = $route.current.params.id;
        if (!id) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            GetRegimenById.get({id: parseInt(id,10)}, function (data) {
                console.log(data);
                deferred.resolve(data.regimens);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }

};