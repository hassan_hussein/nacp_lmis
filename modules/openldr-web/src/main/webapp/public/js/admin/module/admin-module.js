var user = angular.module('admin', ['afyacall','ngLetterAvatar','ngStomp','ui.bootstrap.modal']);

user.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/health-settings', {controller: HealthSettingsController, templateUrl: '/public/pages/admin/partials/health-settings.html', resolve: HealthSettingsController.resolve}).
        when('/cv-settings', {controller: CVSettingsController, templateUrl: '/public/pages/admin/partials/cv-settings.html', resolve: CVSettingsController.resolve}).
        otherwise({redirectTo: '/health-settings'});
}])

.directive('onKeyup', function () {
    return function (scope, elm, attrs) {
        elm.bind("keyup", function () {
            scope.$apply(attrs.onKeyup);
        });
    };
})
    .directive('select2Blur', function () {
        return function (scope, elm, attrs) {
            angular.element("body").on('mousedown', function (e) {
                $('.select2-dropdown-open').each(function () {
                    if (!$(this).hasClass('select2-container-active')) {
                        $(this).data("select2").blur();
                    }
                });
            });
        };
    });
   /* .run(function ($rootScope, AuthorizationService) {

    });*/

