 angular.module('regimen', ['afyacall', 'ui.bootstrap.modal', 'ui.bootstrap.dialog', 'ui.bootstrap.dropdownToggle', 'ui.bootstrap.pagination','ngLetterAvatar']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/search', {controller: RegimenSearchController, templateUrl: 'partials/search.html', reloadOnSearch: false}).
            when('/create-regimen',
            {controller: RegimenController, templateUrl: 'partials/create.html', resolve: RegimenController.resolve}).
            when('/edit/:id',
            {controller: RegimenController, templateUrl: 'partials/create.html', resolve: RegimenController.resolve}).
            otherwise({redirectTo: '/search'});
    }])

    .run(function ($rootScope, AuthorizationService) {
        $rootScope.regmenSelected = "selected";
        //AuthorizationService.preAuthorize('MANAGE_USERS');
    });

