var user = angular.module('admin_user', ['afyacall','ngGrid', 'ui.bootstrap.modal', 'ui.bootstrap.dialog','ui.bootstrap.pagination','ngAvatar','ngLetterAvatar','ngStomp']);

user.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/search', {controller: AdminUserSearchController, templateUrl: '/public/pages/admin/user/partials/search.html', resolve: AdminUserSearchController.resolve}).
        when('/create-user', {controller: UserControllerFunction, templateUrl: 'partials/create.html', resolve: UserControllerFunction.resolve}).
        when('/profile', {controller: UserProfileController, templateUrl: '/public/pages/admin/user/partials/profile.html', resolve: UserProfileController.resolve}).
        when('/payment', {controller: UserPaymentController, templateUrl: '/public/pages/admin/user/partials/payment.html', resolve: UserPaymentController.resolve}).
        when('/edit/:userId', {controller: UserControllerFunction, templateUrl: 'partials/create.html', resolve: UserControllerFunction.resolve}).

        otherwise({redirectTo: '/search'});
}])

    .directive('onKeyup', function () {
        return function (scope, elm, attrs) {
            elm.bind("keyup", function () {
                scope.$apply(attrs.onKeyup);
            });
        };
    })
    .directive('select2Blur', function () {
        return function (scope, elm, attrs) {
            angular.element("body").on('mousedown', function (e) {
                $('.select2-dropdown-open').each(function () {
                    if (!$(this).hasClass('select2-container-active')) {
                        $(this).data("select2").blur();
                    }
                });
            });
        };
    }).directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    })
 .run(function ($rootScope, AuthorizationService) {
 $rootScope.userSelected = "selected";
// AuthorizationService.preAuthorize('MANAGE_USER');
 });

