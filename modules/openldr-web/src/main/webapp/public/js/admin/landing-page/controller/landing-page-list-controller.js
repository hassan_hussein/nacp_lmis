function LandingPageListController($scope,GetLandingPages,$location,DeleteLandingPage){
    $scope.landingPages = [];

    GetLandingPages.get({}, function(data){
        if(!isUndefined(data.landingPages))
        $scope.landingPages = data.landingPages;
        console.log(data);
    });


    var deleteSuccessFunc = function (data) {
        $scope.error = "";
        $scope.$parent.message = "deleted Successfully";
        $scope.$parent.landingPages = data.landingPages;
        $scope.showError = false;
        $location.path('');

    };

    var error = function (data) {
        $scope.message = "";
        $scope.error = " Can not be deleted ";
        $scope.showError = true;
    };

    $scope.delete = function (id) {
        if (!id) return;
        DeleteLandingPage.delete({id: parseInt(id, 10)}, deleteSuccessFunc, error);
    };

    $scope.edit = function(id){
        $location.path('edit/' + id);
    }


}
