function UserRoleAssignmentController($scope, $dialog, messageService) {
    $scope.programsToDisplay = [];
    $scope.selectSuperviseProgramMessage = 'label.select.program';
    $scope.selectSupervisoryNodeMessage = 'label.select.node';
    $scope.selectWarehouseMessage = 'label.select.warehouse';

    $("#adminRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteAdminRolesModal",
                header: "create.user.deleteAdminRoleHeader",
                body: "create.user.deleteAdminRoles"
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreAdminRole, $dialog);

            window.lastAdminRoleRemoved = e.removed;
        }
    });

    $("#labRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteDoctorRolesModal",
                header: "create.user.deleteLabRoleHeader",
                body: "create.user.deleteLabRoleHeader"
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreLabRole, $dialog);

            window.lastLabRoleRemoved = e.removed;
        }
    });

    $("#hubRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteHubRolesModal",
                header: "create.user.deleteHubRoleHeader",
                body: "create.user.deleteHubRoleHeader"
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreHubRole, $dialog);

            window.lastHubRoleRemoved = e.removed;
        }
    });

    $("#reportRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteReportRolesModal",
                header: messageService.get("create.user.deleteReportRoleHeader"),
                body: messageService.get("create.user.deleteReportRoles")
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreReportRole, $dialog, messageService);

            window.lastReportRoleRemoved = e.removed;
        }
    });

    $scope.restoreLabRole = function (result) {
        if (result) return;

        if (window.lastLabRoleRemoved) {
            $scope.user.labRoles.roleIds.push(window.lastLabRoleRemoved.id);
        }
    };


    $scope.restoreHubRole = function (result) {
        if (result) return;

        if (window.lastHubRoleRemoved) {
            $scope.user.hubRoles.roleIds.push(window.lastHubRoleRemoved.id);
        }
    };


    $scope.restoreReportRole = function (result) {
        if (result) return;

        if (window.lastReportRoleRemoved) {
            $scope.user.reportRoles.roleIds.push(window.lastReportRoleRemoved.id);
        }
    };

    $scope.restoreAdminRole = function (result) {
        if (result) return;

        if (window.lastAdminRoleRemoved) {
            $scope.user.adminRole.roleIds.push(window.lastAdminRoleRemoved.id);
        }
    };

    $scope.hasMappingError = function (mappingErrorFlag, field) {
        return mappingErrorFlag && !isPresent(field);
    };

    var isPresent = function (obj) {
        return obj !== undefined && obj !== null && obj !== "" && !(obj instanceof Array && obj.length === 0) ;
    };
}