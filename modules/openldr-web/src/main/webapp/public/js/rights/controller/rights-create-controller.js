function RightsController($scope,$location,UpdateRights,SaveRights,SaveRIGHTSInformation,SaveRightsInfo,messageService,RightsByName){
    $scope.rights = null;
    $scope.right1 = null;

    $scope.rights = RightsByName;
    $scope.right1 = RightsByName;


    $scope.rightTypes = [{

        "id":1,"name":'ADMIN'
    }, {"id":2,"name": "LAB"},
        {"id":3,"name":"HUB"}

    ];


    $scope.saveRight = function () {

        var successHandler = function (msgKey) {
            $scope.showError = false;
            $scope.error = "";
            $scope.$parent.message = messageService.get(msgKey);
            //$scope.$parent.name = $scope.rights.name;
            $location.path('');
        };

        var saveSuccessHandler = function (response) {
            $scope.rights = response.rights;
            successHandler("message.rights.saved.success");
        };

        var updateSuccessHandler = function () {
            successHandler("message.rights.updated.success");
        };

        var errorHandler = function (response) {
            $scope.showError = true;
            $scope.message = "";
            $scope.error = response.data.error;
        };

        var requiredFieldsPresent = function (user) {
            if ($scope.rightForm.$error.required) {
                $scope.error = messageService.get("form.error");
                $scope.showError = true;
                return false;
            } else {
                return true;
            }
        };

      //  if (!requiredFieldsPresent($scope.rights))  return false;

        if($scope.right1){
            SaveRightsInfo.update({name: $scope.rights.name}, $scope.rights, updateSuccessHandler, errorHandler);

        }else{
            SaveRIGHTSInformation.save({}, $scope.rights, saveSuccessHandler, errorHandler);

        }
/*
        if ($scope.rights.name) {
            UpdateRights.update({name: $scope.rights.name}, $scope.rights, updateSuccessHandler, errorHandler);
        } else {
            SaveRights.save({}, $scope.rights, saveSuccessHandler, errorHandler);
        }*/

        return true;
    };

  $scope.cancel = function(){

      $location.path('/');
  }


}

RightsController.resolve ={

    RightsByName: function ($q, GetRightsByName, $route, $timeout) {
        var name = $route.current.params.name;
        if (!name) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            GetRightsByName.get({name: name}, function (data) {
               // if(!isUndefined(data.rights))
                deferred.resolve(data.rights);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }

};