function ListRightsController($scope,AllRights,DeleteRights,$location){
    $scope.rights = [];
    AllRights.get({}, function(data){
        if(!isUndefined(data)){
            $scope.rights = data.rights;
        }

    });

    var deleteSuccessFunc = function (data) {
        $scope.$parent.message = "";
        $scope.$parent.geoLevelId = null;
        $scope.$parent.message = data.success;
        $scope.$parent.deleteRightItem = true;
        $scope.showError = false;

        $location.path('/');
    };

    var error = function (data) {
        $scope.message = "";
        $scope.error = " This Right has already been assigned to ROLE ";
        $scope.showError = true;
    };

    $scope.deleteRight = function (result) {
        if (!result) return;
        DeleteRights.delete({name: result}, deleteSuccessFunc, error);
    };

}