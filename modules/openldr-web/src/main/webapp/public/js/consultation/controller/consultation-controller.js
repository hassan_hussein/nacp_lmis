
function ConsultationController($scope,$q,$timeout,localStorageService,SaveConsultation,OnlineLogOut,Queue,ForwardConsultation,GetUserConsultationRecords,$rootScope,$filter, MedicalRecordTypes,$location,CreateAccount, Account, navigateBackService) {

    $scope.showResults = false;
    $scope.currentPage = 1;
    $scope.fieldToAdd = [];
    $scope.Plans=[];
    $scope.Appointments=[];
    $scope.userId = localStorageService.get(localStorageKeys.USER_ID);

    $scope.getAccount=function(){
        Account.get({referenceNumber:$scope.referenceNumber},function(data){
            $scope.account=data.account;
        });
    };

    $scope.saveAccount = function () {
           CreateAccount.update($scope.account,function(data){
               if(data.Account !== undefined){
                    $scope.$parent.showAccountSaveSuccessFull=true;
                    $scope.getAccount();
               }
           });
        };

    $scope.addAccountMember=function(){
       var member={};
           member.user=$scope.memberToAdd;
           member.unsaved=true;
           if($scope.account.members.length === 0)
             member.isDefault=true;
             $scope.account.members.push(member);
             $scope.memberToAdd={};
             $scope.newMemberModal=false;
    };

    var getConsultationRecord=function(q,scope,timeout,GetUserConsultationRecords,consultationId)
    {
        var deferred=q.defer();
        timeout(function(){
            GetUserConsultationRecords.get({consultationId:consultationId},function(data){
                        deferred.resolve(data);
            });
        },100);

        return deferred.promise;
    };

   $rootScope.$watch('newQueueFound',function(){
       if($rootScope.newQueueFound === true)
       {
          $scope.getQueue();
       }
   })
    $scope.getQueue=function(){
       Queue.get({doctorId:$rootScope.doctorId},function(data){
         $scope.Queue=data.queue;
         $rootScope.newQueueFound=false;
       });
    }
    $scope.refreshQueue=function(){
        if($rootScope.doctorId === undefined)
        $rootScope.doctorId=$scope.userId;
        $scope.getQueue();
    };

   $rootScope.showConsultationModal=function(){
       //Get Medical Consultation Form fields
      MedicalRecordTypes.get({},function(data){
           $scope.medicalRecordTypes=data.recordTypes;
           //console.log( $scope.medicalRecordTypes);
      });
      if($rootScope.consultationId !== undefined)
      {
          getConsultationRecord($q,$scope,$timeout,GetUserConsultationRecords,$rootScope.consultationId).then(function(data){
            $scope.consultationRecords=data.consultationRecords;
            console.log($scope.consultationRecords);
            //Consultation form
            if($scope.consultationRecords.medicalRecords.length ===0){
               var consultation=_.findWhere($scope.medicalRecordTypes,{code:'CONSULTATION_FORM'});
               if(consultation !== undefined){
                  var record={};
                  record.recordTypeId=consultation.id;
                  record.fieldValues=[];
                  consultation.fieldGroups.forEach(function(group){
                      group.fields.forEach(function(f){
                          var field={};
                          field.group=group.name;
                          field.medicalRecordFieldId=f.id;
                          field.fieldType=f.fieldType;
                          field.fieldName=f.fieldName;
                          record.fieldValues.push(field);
                      });
                  });
                 $scope.consultationRecords.medicalRecords.push(record);
                 $scope.consultationRecordsToDisplay=_.groupBy($scope.consultationRecords.medicalRecords[0].fieldValues,
                                                       function(f){
                                                         return f.group;
                  });
                  $scope.consultationRecordsToDisplay=$.map($scope.consultationRecordsToDisplay,function(value,index){
                      return [{"name":index,"fields":value}];
                  });

               }
            }
            else{
                $scope.consultationRecordsToDisplay=_.groupBy($scope.consultationRecords.medicalRecords[0].fieldValues,
                                                                         function(f){
                                                                           return f.group;
                });
                $scope.consultationRecordsToDisplay=$.map($scope.consultationRecordsToDisplay,function(value,index){
                                   return [{"name":index,"fields":value}];
               });
            }

            if($scope.consultationRecords.medicalHistory.length ===0)
            {
                var history=_.findWhere($scope.medicalRecordTypes,{code:'MEDICAL_HISTORY'});
                if(history !== undefined){
                    var record={};
                    record.recordTypeId=history.id;
                    record.fieldValues=[];
                    history.fieldGroups.forEach(function(group){
                                      group.fields.forEach(function(f){
                                          var field={};
                                          field.group=group.name;
                                          field.medicalRecordFieldId=f.id;
                                          field.fieldType=f.fieldType;
                                          field.fieldName=f.fieldName;
                                          record.fieldValues.push(field);
                                      });
                                  });
                    $scope.consultationRecords.medicalHistory.push(record);
                    $scope.medicalHistoryToDisplay=_.groupBy($scope.consultationRecords.medicalHistory[0].fieldValues,
                                                                       function(f){
                                                                         return f.group;
                     });
                    $scope.medicalHistoryToDisplay=$.map($scope.medicalHistoryToDisplay,function(value,index){
                          return [{"name":index,"fields":value}];
                    });
                    $scope.consultationRecords.medicalHistory[0].medicalHistoryToDisplay=$scope.medicalHistoryToDisplay;

                   }
            }
            else{
                  $scope.consultationRecords.medicalHistory.forEach(function(history){
                     var medicalHistoryToDisplay=_.groupBy(history.fieldValues,
                                                                                           function(f){
                                                                                             return f.group;
                     });
                     $scope.medicalHistoryToDisplay=$.map(medicalHistoryToDisplay,function(value,index){
                           return [{"name":index,"fields":value}];
                     });
                     history.medicalHistoryToDisplay= $scope.medicalHistoryToDisplay;
                  });
            }

            $scope.consultationModal=true;
          });
      };

   };

   $scope.showDoctorConsultationModal=function(consultationId){
      OnlineLogOut.update({userId:$rootScope.doctorId,status:'BUSY'},function(data){

         });
     $rootScope.consultationId=consultationId;
     $scope.showConsultationModal();

   };

   $scope.saveFinalConsultation=function(){
   //TODO save consultaion data;
       OnlineLogOut.update({userId:$rootScope.doctorId,status:"ONLINE"},function(data){

        });
        SaveConsultation.update($scope.consultationRecords,function(data){
            if(data.success ==='Success')
            {
                      $scope.consultationForwardSuccess=true;
                             $scope.consultationModal=false;
                             $scope.consultationRecords=undefined;
                             $scope.refreshQueue();
            }

        });

       $scope.doctorConsultationModal=false;
   };
   $scope.refreshQueue();
   $scope.connectToDoctor=function(){
       $scope.consultationRecords.doctorId=parseInt($scope.doctorToForward,10);
      // return;
      if($scope.consultationRecords !== undefined){

         ForwardConsultation.update($scope.consultationRecords,function(data){
             if(data.success ==='Success')
             {
                 $scope.consultationForwardSuccess=true;
                 $scope.consultationModal=false;
                 $scope.consultationRecords=undefined;
             }
         });
      }
   }

   $scope.cancelConsultation=function(){
      OnlineLogOut.update({userId:$scope.userId,status:"ONLINE"},function(data){

              });
       $scope.consultationModal=false;
   };

   $scope.hasViewPermissionByFieldName=function(fieldName)
   {
        var permission='VIEW_'+fieldName.replace(/\s+/g, '_').toUpperCase();
        return $scope.hasPermission(permission);
   }
   $scope.hasModifyPermissionByFieldName=function(fieldName)
   {
        var permission='MODIFY_'+fieldName.replace(/\s+/g, '_').toUpperCase();
        return $scope.hasPermission(permission);
   }

   $scope.triggerSearch = function (event) {
        console.log(event.keyCode);
        if (event.keyCode === 13) {
            $scope.getAccount();
        }
    };

    $scope.clearSearch = function () {
        $scope.query = "";
        $scope.totalItems = 0;
        $scope.userList = [];
        $scope.showResults = false;
        angular.element("#searchUser").focus();
    };

    $scope.edit = function (id) {
        var data = {query: $scope.query};
        navigateBackService.setData(data);
        $location.path('edit/' + id);
    };

    $scope.$watch('currentPage', function () {
        if ($scope.currentPage !== 0) {
            //$scope.loadUsers($scope.currentPage, $scope.searchedQuery);
        }
    });

   $scope.loadRights = function () {
         $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
     }();

     $scope.hasPermission = function (permission) {
                 if ($scope.rights !== undefined && $scope.rights !== null) {
                   var rights = JSON.parse($scope.rights);
                   var rightNames = _.pluck(rights, 'name');
                   return rightNames.indexOf(permission) > -1;
                 }
                 return false;
     };

}
