function RegisterReportController($scope,GetRegisterReport,$filter){

    $scope.exportReport = function (type) {
        $scope.filter.pdformat = 1;
        var params = jQuery.param($scope.getSanitizedParameter());
        var url = '/reports/download/hvl_report/' + type + '?' + params;
        $window.open(url, "_BLANK");
    };

    $scope.OnFilterChanged = function () {
        // clear old data if there was any
        $scope.data = $scope.datarows = [];
        $scope.filter.max = 10000;

        $scope.filter.startDate = $filter('date')($scope.filter.startTime, "yyyy-MM-dd");

        $scope.filter.endDate = $filter('date')($scope.filter.endTime, "yyyy-MM-dd");

        GetRegisterReport.get($scope.getSanitizedParameter(), function (data) {
            if (data.pages !== undefined && data.pages.rows !== undefined) {
                $scope.data = data.pages.rows;
                $scope.paramsChanged($scope.tableParams);
            }
        });

    };

    $scope.formatNumber = function (value) {
        return utils.formatNumber(value, '0,0.00');
    };


    $scope.showPopover=false;

    $scope.popover = {
        title: 'Manufacturer Name',
        message: 'Message'
    };
}