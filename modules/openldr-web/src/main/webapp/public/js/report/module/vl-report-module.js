angular.module('report', ['afyacall', 'ngTable','ui.bootstrap.tpls', 'angularCombine', 'ui.bootstrap.modal','gridshore.c3js.chart', 'ui.bootstrap.dropdownToggle','ui.bootstrap.pagination', 'tree.dropdown'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/register-report', {controller: RegisterReportController, templateUrl:'partials/register-report.html',reloadOnSearch:false}).
            when('/age-summary', {controller: AgeSummaryReportController, templateUrl:'partials/age-summary.html',reloadOnSearch:false}).
      otherwise({redirectTo:'/register-report'});
    }]).config(function(angularCombineConfigProvider) {
        angularCombineConfigProvider.addConf(/filter-/, '/public/pages/reports/shared/filters.html');
    });