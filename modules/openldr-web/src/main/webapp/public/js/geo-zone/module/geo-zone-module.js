
var geoZoneModule = angular.module('geo-zone', ['afyacall','ngStomp', 'ui.bootstrap.modal','leaflet-directive', 'ui.bootstrap.dialog', 'ui.bootstrap.dropdownToggle', 'ui.bootstrap.pagination', 'ngDraggable','ngLetterAvatar']).
    config(['$routeProvider', function ($routeProvider) {
      $routeProvider.
          when('/search', {controller: GeoZoneSearchController, templateUrl: 'partials/search.html', reloadOnSearch: false}).
          when('/create-geo-zone', {controller: GeoZoneController, templateUrl: 'partials/create.html', resolve: GeoZoneController.resolve}).
          when('/json', {controller: GeographicZonesJsonController, templateUrl: 'partials/json.html'}).
          when('/edit/:id', {controller: GeoZoneController, templateUrl: 'partials/create.html', resolve: GeoZoneController.resolve}).
          otherwise({redirectTo: '/search'});
    }]).run(function ($rootScope, AuthorizationService) {
      $rootScope.geoZoneSelected = "selected";
      //AuthorizationService.preAuthorize('MANAGE_GEOGRAPHIC_ZONE');
    });
