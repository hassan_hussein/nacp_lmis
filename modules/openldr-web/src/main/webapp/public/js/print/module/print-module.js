angular.module('print', ['afyacall','ui.bootstrap.modal','io-barcode', 'ui.bootstrap.dialog','gridshore.c3js.chart','ngLetterAvatar','rorymadden.date-dropdowns']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/print-sample/:sampleId', {controller: PrintSampleController, templateUrl: '/public/pages/print/partials/print-sample.html',resolve: PrintSampleController.resolve}).
            when('/print-batch/:batchNumber', {controller: PrintSampleController, templateUrl: '/public/pages/print/partials/print-sample.html',resolve: PrintSampleController.resolve}).
            when('/print-worksheet/:workSheetId', {controller: PrintWorkSheetController, templateUrl: '/public/pages/print/partials/print-worksheet.html',resolve: PrintWorkSheetController.resolve}).
            otherwise({redirectTo: '/dashboard'});
    }]).run(function ($rootScope, AuthorizationService) {
        $rootScope.sampleRegistrationSelected = "selected";
    }).filter('patientFilter', function($filter) {
        return function(patientId) {
            if (patientId == undefined || patientId.length < 12)
                return patientId;
            var c = '-',p = patientId.replace(/-/g,'');

            return [p.slice(0,2),c,p.slice(2,4),c,p.slice(4,8),c,p.slice(8).padStart(6,'0')].join('');
        };
    });
    
