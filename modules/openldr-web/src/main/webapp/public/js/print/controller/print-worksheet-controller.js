function PrintWorkSheetController($scope,$dialog,$timeout,$window, GetWorkSheet,$routeParams,IO_BARCODE_TYPES){

    $scope.workSheetId=$routeParams.workSheetId;

    GetWorkSheet.get({workSheetId:parseInt($scope.workSheetId,10)},function(data){
        $scope.workSheet=data.workSheet;

        var _samples = data.workSheet.samples;

        var x = _.sortBy(_samples, function(o) { console.log(o.orderId); return o.orderId; });

        var n = 2;
        var samples = _.groupBy(x, function(element, index){
            return Math.floor(index/n);
        });
        $scope.workSheet.samples = _.toArray(samples);
        $scope.sampleCount = $scope.workSheet.samples.reduce((a, c) => a + _.toArray(c).length,0);
        console.log($scope.workSheet);

        $scope.$parent.title="WorkSheet-"+$scope.workSheet.workSheetNumber;
        $scope.type = 'CODE128B';
        $scope.options = {
            width: 2,
            height: 90,
            displayValue: true,
            font: 'monospace',
            textAlign: 'center',
            fontSize: 12,
            lineColor: '#000'
        };
        // $timeout($window.print(),100);
    });
}

PrintWorkSheetController.resolve = {
};

