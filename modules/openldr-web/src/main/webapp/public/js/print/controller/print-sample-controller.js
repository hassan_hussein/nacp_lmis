function PrintSampleController($scope,$dialog,$timeout,$window,GetSamplesByBatchOneRow,GetSamplesByBatch, GetSampleById,$routeParams,IO_BARCODE_TYPES){

    $scope.sampleId=$routeParams.sampleId;
    $scope.batchNumber = $routeParams.batchNumber;

    GetSampleById.get({sampleId:parseInt($scope.sampleId,10)},function(data){
        $scope.sample=data.sample;
        $scope.$parent.title="Sample-"+$scope.sample.labNumber;
        $scope.type = 'CODE128B';
        $scope.options = {
            width: 1,
            height: 50,
            displayValue: true,
            font: 'monospace',
            textAlign: 'center',
            fontSize: 2,
            lineColor: '#000'
        };
        //  $timeout($window.print,0);
    });

    GetSamplesByBatchOneRow.get({batchNumber:$scope.batchNumber},function(data){
        $scope.sample=data.samples;
        $scope.$parent.title="Batch-"+$scope.sample.batchNumber;
        $scope.type = 'CODE128B';
        $scope.options = {
            width: 1,
            height: 50,
            displayValue: true,
            font: 'monospace',
            textAlign: 'center',
            fontSize: 2,
            lineColor: '#000'
        };
        //  $timeout($window.print,0);
    });
}

PrintSampleController.resolve = {
};

