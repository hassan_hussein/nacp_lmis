
angular.module('role', ['afyacall', 'ui.bootstrap.modal', 'ui.bootstrap.dialog','ngLetterAvatar','ngStomp'])
    .config(['$routeProvider', function ($routeProvider) {
  $routeProvider.
    when('/create', {controller:RoleController, templateUrl:'/public/pages/role/partials/create.html'}).
    when('/list', {controller:ListRoleController, templateUrl:'partials/list.html'}).
    when('/edit/:id', {controller:RoleController, templateUrl:'/public/pages/role/partials/create.html'}).
    otherwise({redirectTo:'/list'});
}]).run(function($rootScope, AuthorizationService) {
    $rootScope.roleSelected = "selected";
    //AuthorizationService.preAuthorize('MANAGE_ROLE');
  });