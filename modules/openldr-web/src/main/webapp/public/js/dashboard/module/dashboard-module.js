var dashboard = angular.module('openl_dr', ['afyacall', 'gridshore.c3js.chart','textAngular','leaflet-directive'
    ]);

dashboard.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/search',
        {controller: DashBoardController, templateUrl: 'partials/search.html', reloadOnSearch: false}).
        when('/edit/:id', {controller: DashBoardController, templateUrl: 'partials/create.html', resolve: DashBoardController.resolve}).
        when('/create', {controller: DashBoardController, templateUrl: 'partials/create.html', resolve: DashBoardController.resolve}).
        otherwise({redirectTo: '/search'});
}]).run(function ($rootScope, AuthorizationService) {
    $rootScope.dashboardSelected = "selected";
   // AuthorizationService.preAuthorize('MANAGE_REQUISITION_GROUP');
});
    /*    when('/dashbord', {controller: DashBoardController,
            templateUrl: '/public/pages/dashboard/partials/dashboard',
            resolve: DashBoardController.resolve}).
        otherwise({redirectTo: '/dashboard'});
}]).config(function(angularCombineConfigProvider) {
    angularCombineConfigProvider.addConf(/filter-/, '/public/pages/shared/filters.html');
});*/
