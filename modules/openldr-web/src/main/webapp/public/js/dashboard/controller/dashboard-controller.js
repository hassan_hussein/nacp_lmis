

function DashBoardController($scope) {


    $scope.data={};
    $scope.testBreakDown = {
              dataPoints:[],
              dataColumns: [
                           {"id": "test", "name":"All Test", "type": "bar"},
                           {"id": "test1000", "name":"> 1000cpl/ml", "type": "bar"}
              ],
              dataX: {"id": "year"}
      };

      $scope.testByAge = {
              dataPoints:[],
              dataColumns: [
                           {"id": "lessThan15", "name":"Less than 15 yrs", "type": "pie"},
                           {"id": "adults", "name":"Adults", "type": "pie"}
              ],
              dataX: {"id": "year"}
      };

     $scope.testTrend = {
         dataPoints:[{"month":"Jan", "test":1000,"suspected":300,"rejected":10},
                     {"month":"Feb", "test":1100,"suspected":400,"rejected":15},
                     {"month":"Mar", "test":1200,"suspected":350,"rejected":10},
                     {"month":"Apr", "test":1000,"suspected":470,"rejected":9},
                     {"month":"May", "test":1300,"suspected":500,"rejected":9},
                     {"month":"Jun", "test":1500,"suspected":600,"rejected":11},
                     {"month":"Jul", "test":1600,"suspected":520,"rejected":10},
                     {"month":"Aug", "test":1400,"suspected":510,"rejected":9},
                     {"month":"Sep", "test":1600,"suspected":620,"rejected":9},
                     {"month":"Oct","test":1700,"suspected":700,"rejected":10},
                     {"month":"Nov", "test":1800,"suspected":800,"rejected":10},
                     {"month":"Dec", "test":1900,"suspected":900,"rejected":9}],
         dataColumns: [
                   {"id": "test", "name":"Results", "type": "line"},
                   {"id": "suspected", "name":"Suspected treatment failure", "type": "line"},
                   {"id": "rejected", "name":"Rejected", "type": "line"}
         ],
                  dataX: {"id": "month"}
     };


$scope.nationStatistics=[];

    $scope.labPerformance={
               dataPoints:[],
                dataColumns: [
                                  {"id": "request", "name":"Requests", "type": "bar"},
                                  {"id": "test", "name":"Tests", "type": "bar"},
                                  {"id": "pending", "name":"Pending Tests", "type": "bar"}
                        ],
               dataX: {"id": "name"}
             };

  $scope.testBySample = {
                dataPoints:[],
                dataColumns: [
                             {"id": "request", "name":"All Test", "type": "bar"},
                             {"id": "repeated", "name":"Repeated", "type": "bar"}
                ],
                dataX: {"id": "sample"}
  };


}

DashBoardController.resolve = {


};