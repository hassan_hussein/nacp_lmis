
var app = angular.module('openldr3',['afyacall','ngRoute','angularCombine','ui.bootstrap','gridshore.c3js.chart','textAngular','leaflet-directive','ngLetterAvatar'],
 function ($httpProvider) {
    var interceptor = ['$q', '$window', function ($q, $window) {
      var requestCount = 0;

      function responseSuccess(response) {
        if (!(--requestCount))
          angular.element('#loader').hide();
        return response;
      }

      function responseError(response) {
        if (!(--requestCount))
          angular.element('#loader').hide();
          return $q.reject(response);
      }

      function request(config) {
        if ((++requestCount) > 0)
          angular.element('#loader').show();
       // config.headers["X-Requested-With"] = "XMLHttpRequest";
        return config;
      }

      return {
        'request': request,
        'response': responseSuccess,
        'responseError': responseError
      };
    }];
    $httpProvider.interceptors.push(interceptor);
  }
);

app.config(['$routeProvider',
        function ($routeProvider) {

    $routeProvider.
        when('/dashbord', {controller: DashBoardController,
            templateUrl: '/public/pages/admin/index.html',
            resolve: DashBoardController.resolve}).
        otherwise({redirectTo: '/dashboard'});
}]).config(function(angularCombineConfigProvider) {
    angularCombineConfigProvider.addConf(/filter-/, '/public/pages/shared/filters.html');
});