function AdminUserController($scope,extensionNumber,userGroup,Facility, $location, $dialog, Users,accountPackages,userContext,UserType, messageService, user, roles_map, $timeout, UpdatePassword) {
    $scope.user=userContext;
    $scope.userTypes = UserType;
    $scope.accountPackages = accountPackages;

    $scope.client = user;

    $scope.extensionNumbers = extensionNumber;
    $scope.userGroups = userGroup;
    $scope.userNameInvalid = false;
    $scope.showHomeFacilityRoleMappingError = false;
    $scope.showSupervisorRoleMappingError = false;
    $scope.user = user || {homeFacilityRoles: [], restrictLogin: false};
  //  $scope.supervisoryNodes = supervisoryNodes;
    //$scope.warehouses = enabledWarehouses;
    //$scope.deliveryZones = deliveryZones;
    $scope.$parent.userId = null;
    $scope.message = "";
    $scope.$parent.message = "";

    var originalUser = $.extend(true, {}, user);

   loadUserFacility();
    //preparePrograms(programs);

    $scope.rolesMap = roles_map;


    $scope.saveUser = function () {
        console.log($scope.client);

        var successHandler = function (msgKey) {
            $scope.showError = false;
            $scope.error = "";
            $scope.$parent.message = messageService.get(msgKey, $scope.client.firstName, $scope.client.lastName);
            $scope.$parent.userId = $scope.client.id;
            $location.path('');
        };

        var saveSuccessHandler = function (response) {
            $scope.user = response.user;
            successHandler(response.success);
        };

        var updateSuccessHandler = function () {
            successHandler("message.user.updated.success");
        };

        var errorHandler = function (response) {
            $scope.showError = true;
            $scope.message = "";
            $scope.error = response.data.error;
        };

        var requiredFieldsPresent = function (user) {
            if ($scope.userForm.$error.required) {
                $scope.error = messageService.get("form.error");
                $scope.showError = true;
                return false;
            } else {
                return true;
            }
        };

/*
        if (!requiredFieldsPresent($scope.client))  return false;
*/
        if ($scope.client.email === "") {
            $scope.client.email = null;
        }

        if ($scope.client.id) {
            Users.update({id: $scope.client.id}, $scope.client, updateSuccessHandler, errorHandler);
        } else {
            Users.save({}, $scope.client, saveSuccessHandler, errorHandler);
        }
        return true;
    };

    $scope.validateUserName = function () {
        $scope.userNameInvalid = $scope.client.userName !== null && $scope.client.userName.trim().indexOf(' ') >= 0;
    };

    $scope.toggleSlider = function () {
        if (!$scope.facilitySelected) {
            $scope.showSlider = !$scope.showSlider;
            $scope.extraParams = {"virtualFacility": false, "enabled": null };
        }
    };

    $scope.associate = function (facility) {
        $scope.client.facilityId = facility.id;
        $scope.facilitySelected = facility;
        loadUserFacility();
        $scope.query = null;
        $scope.showSlider = !$scope.showSlider;
    };

    $scope.clearSelectedFacility = function (result) {
        if (!result) return;

        $scope.facilitySelected = null;
        $scope.allSupportedPrograms = null;
        $scope.user.homeFacilityRoles = null;
        $scope.user.facilityId = null;

        $timeout(function () {
            angular.element("#searchFacility").focus();
        });
    };

    $scope.confirmFacilityDelete = function () {
        var dialogOpts = {
            id: "deleteFacilityModal",
            header: 'delete.facility.header',
            body: 'confirm.programRole.deletion'
        };
        OpenLmisDialog.newDialog(dialogOpts, $scope.clearSelectedFacility, $dialog);
    };

    function loadUserFacility() {
        if (!$scope.client) return;

        if (!utils.isNullOrUndefined($scope.client.facilityId)) {
            //if (utils.isNullOrUndefined($scope.allSupportedPrograms)) {
                Facility.get({id: $scope.client.facilityId}, function (data) {
                /*    $scope.allSupportedPrograms = _.filter(data.facility.supportedPrograms, function (supportedProgram) {
                        return !supportedProgram.program.push;
                    });
*/
                    $scope.facilitySelected = data.facility;
                }, {});
            //}
        }
    }


    $scope.cancel = function () {
        $scope.$parent.message = "";
        $scope.$parent.userId = undefined;
        $location.path('#/search');
    };

    var clearErrorAndSetMessage = function (msgKey) {
        $scope.showError = "false";
        $scope.error = "";
        $scope.message = messageService.get(msgKey, $scope.user.firstName, $scope.user.lastName);
    };

    var errorFunc = function (data) {
        $scope.showError = "true";
        $scope.message = "";
        $scope.error = data.error;
    };
    //$scope.showDisableModal = false;

    $scope.showConfirmUserDisableModal = function () {

       // $scope.showDisableModal = true;
        var dialogOpts = {
            id: "disableUserDialog",
            header: 'disable.user.header',
            body: messageService.get('disable.user.confirm', $scope.client.firstName, $scope.client.lastName)
        };
        OpenLmisDialog.newDialog(dialogOpts, $scope.disableUserCallback, $dialog);

       /* var options = {
            id: "confirmDialog",
            header: "label.confirm.order.submit.action",
            body: "msg.question.submit.order.confirmation"
        };
        OpenLmisDialog.newDialog(options, callBack, $dialog);*/
    };

    $scope.disableUserCallback = function (result) {
        if (!result) return;
        Users.disable({id: $scope.client.id}, disableSuccessFunc, errorFunc);
    };

    var disableSuccessFunc = function (data) {
        clearErrorAndSetMessage(data.success);
        $scope.user = originalUser;
        $scope.user.active = false;
    };

    $scope.showConfirmUserRestoreModal = function () {
        var dialogOpts = {
            id: "restoreUserDialog",
            header: 'enable.user.header',
            body: messageService.get('enable.user.confirm', $scope.user.firstName, $scope.user.lastName)
        };
        OpenLmisDialog.newDialog(dialogOpts, $scope.restoreUserCallback, $dialog);
    };

    $scope.restoreUserCallback = function (result) {
        if (!result) return;
        $scope.user.active = true;
        Users.update({id: $scope.user.id}, $scope.user, restoreSuccessFunc, errorFunc);
    };

    $scope.getMessage = function (key) {
        return messageService.get(key);
    };

    $scope.changePassword = function (user) {
        if (user.active) {
            $scope.user.password1 = $scope.user.password2 = $scope.message = $scope.passwordError = "";
            $scope.changePasswordModal = true;
        }
        else {
            $scope.message = "";
            $scope.error = messageService.get("user.is.disabled");
        }
    };

    $scope.updatePassword = function () {
        var reWhiteSpace = new RegExp("\\s");
        var digits = new RegExp("\\d");
        if ($scope.user.password1.length < 8 || $scope.user.password1.length > 16 || !digits.test($scope.user.password1) ||
            reWhiteSpace.test($scope.user.password1)) {
            $scope.passwordError = messageService.get("error.password.invalid");
            return;
        }

        if ($scope.user.password1 != $scope.user.password2) {
            $scope.passwordError = messageService.get('error.password.mismatch');
            return;
        }

        UpdatePassword.update({userId: $scope.user.id}, $scope.user.password1, function (data) {
            $scope.message = data.success;
            $scope.error = "";
            $scope.resetPasswordModal();
        }, {});
    };

    $scope.resetPasswordModal = function () {
        $scope.changePasswordModal = false;
    };

    var restoreSuccessFunc = function (data) {
        clearErrorAndSetMessage("msg.user.restore.success");
        $('.form-group').find(':input').removeAttr('disabled');
    };

    $timeout(function () {
        accordion.expandCollapseToggle($('.accordion-section:first .heading'));
    });
}

AdminUserController.resolve = {

    user: function ($q, Users, $route, $timeout) {
        var userId = $route.current.params.userId;
        if (!userId) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            Users.get({id: userId}, function (data) {
                deferred.resolve(data.user);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    },

    roles_map: function ($q, Roles, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            Roles.get({}, function (data) {
                deferred.resolve(data.roles_map);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },

    userContext: function ($q, UserContext, $route, $timeout) {
        /*  var userId = $route.current.params.userId;
         if (!userId) return undefined;*/
        var deferred = $q.defer();
        $timeout(function () {
            UserContext.get({}, function (data) {
                deferred.resolve(data);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    },

    UserType: function ($q, GetUserType, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetUserType.get({}, function (data) {
                deferred.resolve(data.users);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },


    accountPackages: function ($q, GetAccountPackages, $route, $timeout) {
        /*  var userId = $route.current.params.userId;
         if (!userId) return undefined;*/
        var deferred = $q.defer();
        $timeout(function () {

            GetAccountPackages.get({}, function (data) {
                if(data.allPackages !== null)
                    deferred.resolve(data.allPackages);
                else
                    return [];
            }, function () {
            });
        }, 100);
        return deferred.promise;

    },
    extensionNumber : function($q, GetAllExtensionNumbers,$route,$timeout){
        var deferred = $q.defer();
        $timeout(function(){
            GetAllExtensionNumbers.get({}, function (data) {
                if(data.allExtensionNumbers !== null){
                    deferred.resolve(data.allExtensionNumbers);
                }
                else
                return [];
            })

        },100);
        return deferred.promise;
    },


    userGroup : function($q, UserGroups,$route,$timeout){
        var deferred = $q.defer();
        $timeout(function(){
            UserGroups.get({}, function (data) {
                if(data.groups !== null){
                    deferred.resolve(data.groups);
                }
                else
                    return [];
            })

        },100);
        return deferred.promise;
    }


};
