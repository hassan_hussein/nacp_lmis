function AccountPackageController($location,$scope,GetAccountPackageBy,SaveAccountPackageInfo,UpdateAccountPackageInfo){
    $scope.accountPackages = GetAccountPackageBy;
    $scope.$parent.message = "";


    var success = function (data) {
        console.log(data.packages);
        $scope.error = "";
        $scope.$parent.message = data.success;
        $scope.$parent.extension = data.packages;
        $scope.showError = false;
        $location.path('');
    };

    var error = function (data) {
        $scope.$parent.message = "";
        $scope.error = data.data.error;
        $scope.showError = true;
    };


    $scope.saveAccountPackage = function () {


        if ($scope.accountPackageForm.$error.name || $scope.accountPackageForm.$error.required) {
            $scope.showError = true;
            $scope.error = 'form.error';
            $scope.message = "";
            return;
        }


        if ($scope.accountPackages.id) {
            UpdateAccountPackageInfo.update({id:  $scope.accountPackages.id}, $scope.accountPackages, success, error);
        }
        else {
            SaveAccountPackageInfo.save({}, $scope.accountPackages, success, error);
        }

        return true;
    };








    $scope.cancel = function(){
        $location.path('/');
    }
}

AccountPackageController.resolve = {

    GetAccountPackageBy: function ($q, GetAccountPackageById, $route, $timeout) {
        var id = $route.current.params.id;
        if (!id) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            GetAccountPackageById.get({id: parseInt(id,10)}, function (data) {
                deferred.resolve(data.allById);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }

};