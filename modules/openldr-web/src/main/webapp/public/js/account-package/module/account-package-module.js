
angular.module('account_package', ['afyacall', 'ui.bootstrap.modal', 'ui.bootstrap.dialog','ngLetterAvatar','ngStomp'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/create', {controller:AccountPackageController, templateUrl:'partials/create.html', resolve:AccountPackageController.resolve}).
            when('/list', {controller:ListAccountPackageController, templateUrl:'partials/list.html'}).
            when('/edit/:id', {controller:AccountPackageController, templateUrl:'partials/create.html', resolve:AccountPackageController.resolve}).
            otherwise({redirectTo:'/list'});
    }]).run(function($rootScope, AuthorizationService) {
        $rootScope.packageSelected = "selected";
        //AuthorizationService.preAuthorize('MANAGE_ROLE');
    });