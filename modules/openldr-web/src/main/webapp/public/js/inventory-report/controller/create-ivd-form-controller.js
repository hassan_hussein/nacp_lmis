function CreateIvdFormController($scope,adjustmentReasons, $location, $dialog, report, InventoryReportSave, InventoryReportSubmit) {

    $scope.currentStockLot = undefined;
    $scope.adjustmentReasonsDialogModal = false;
    $scope.adjustmentReason={};
    var  AdjustmentReasons = [];

  // initial state of the display
  $scope.report = new VaccineReport(report);
  console.log($scope.report);

  //$scope.manufacturers = manufacturers;
  //$scope.adjustmentReasons = adjustmentReasons;

  $scope.adjustmentTypes = [];
    $scope.adjustmentTypes = adjustmentReasons;


  $scope.columnTemplate = [{"id":1,"name":"openingBalance","label":"Beginning Balance","visible":"true"},
    {"id":2,"name":"quantityReceived","label":"Quantity Received","visible":"true"},
    {"id":3,"name":"quantityIssued","label":"Quantity Used","visible":"true"},
    {"id":4,"name":"adjustmentReason","label":"Adjustment Reason","visible":"true"},

    {"id":5,"name":"closingBalance","label":"Closing Balance","visible":"true"},
    {"id":6,"name":"quantityRequested","label":"Quantity Requested","visible":"true"}


  ];



  $scope.showAdjustmentReason=function(product)
  {

    $scope.oldAdjustmentReason = angular.copy(product.adjustmentReasons);
    $scope.currentStockLot = product;
    $scope.currentStockLot.adjustmentReasons=((product.adjustmentReasons === undefined)?[]:product.adjustmentReasons);
    //Remove reason already exist from drop down
    reEvaluateTotalAdjustmentReasons();
    updateAdjustmentReasonForLot(product.adjustmentReasons);
    $scope.adjustmentReasonsDialogModal = true;

  };

    function reEvaluateTotalAdjustmentReasons()
    {
        var totalAdjustments = 0;
        $($scope.currentStockLot.adjustmentReasons).each(function (index, adjustmentObject) {
            if(adjustmentObject.type.additive)
            {
                totalAdjustments = totalAdjustments + parseInt(adjustmentObject.quantity,10);
            }else{
                totalAdjustments = totalAdjustments - parseInt(adjustmentObject.quantity,10);
            }

        });
        $scope.currentStockLot.totalAdjustments=totalAdjustments;
        $scope.currentStockLot.calculateClosingBalance();

    }
    $scope.reEvaluateTotalAdjustmentReasons= function() {reEvaluateTotalAdjustmentReasons();};

    function updateAdjustmentReasonForLot(adjustmentReasons)
    {

        var additive;
        if($scope.currentStockLot !==undefined){
            additive=($scope.currentStockLot.quantity - $scope.currentStockLot.quantityOnHand >=0)?true:false;
        }
      /*  else  if($scope.currentStockLot.lot ===undefined)
        {
            additive=($scope.currentStockLot.quantity - $scope.currentStockLot.totalQuantityOnHand >=0)?true:false;
        }*/
        var adjustmentReasonsForLot = _.pluck(_.pluck(adjustmentReasons, 'type'), 'name');
        $scope.adjustmentReasonsToDisplay = $.grep($scope.adjustmentTypes, function (adjustmentTypeObject) {
            return $.inArray(adjustmentTypeObject.name, adjustmentReasonsForLot) == -1 /*&& adjustmentTypeObject.additive === additive*/;
        });
    }

  $scope.addAdjustmentReason=function(newAdjustmentReason)
  {
    var adjustmentReason={};
    adjustmentReason.type = newAdjustmentReason.type;
    adjustmentReason.name = newAdjustmentReason.type.name;
    adjustmentReason.quantity= newAdjustmentReason.quantity;

    $scope.currentStockLot.adjustmentReasons.push(adjustmentReason);
    updateAdjustmentReasonForLot($scope.currentStockLot.adjustmentReasons);
    reEvaluateTotalAdjustmentReasons();
    newAdjustmentReason.type = undefined;
    newAdjustmentReason.quantity = undefined;

  };


  $scope.closeModal=function(){
    $scope.currentStockLot.adjustmentReasons = $scope.oldAdjustmentReason;
    reEvaluateTotalAdjustmentReasons();
    $scope.adjustmentReasonsDialogModal=false;
  };
  //Save Adjustment
  $scope.saveAdjustmentReasons = function () {
    $scope.modalError = '';
    $scope.clearAndCloseAdjustmentModal();
  };
  $scope.clearAndCloseAdjustmentModal = function () {
    reEvaluateTotalAdjustmentReasons();
    $scope.adjustmentReason = undefined;
    $scope.adjustmentReasonsDialogModal=false;

  };

  $scope.removeAdjustmentReason=function(adjustment)
  {
    $scope.currentStockLot.adjustmentReasons = $.grep($scope.currentStockLot.adjustmentReasons, function (reasonObj) {
      return (adjustment !== reasonObj);
    });
    updateAdjustmentReasonForLot($scope.currentStockLot.adjustmentReasons);
    reEvaluateTotalAdjustmentReasons();
  };


  //prepare tab visibility settings
  $scope.tabVisibility = {};
  _.chain(report.tabVisibilitySettings).groupBy('tab').map(function (key, value) {
    $scope.tabVisibility[value] = key[0].visible;
  });

  //$scope.operationalStatuses = operationalStatuses;

  $scope.highlightRequired = function (showError, value, skipped) {
    if (showError && isUndefined(value) && !skipped) {
      return "required-error";
    }
    return null;
  };

  $scope.changeTab = function( tabName ){
    $scope.visibleTab = tabName;
    if($scope.ivdForm.$dirty){
      $scope.save();
    }else{
      $scope.message = '';
    }
  };

  $scope.save = function () {
    console.log($scope.report);
    InventoryReportSave.update($scope.report, function () {
      $scope.error = '';
      $scope.message = "msg.ivd.saved.successfully";
      $scope.ivdForm.$setPristine();
    });
  };

  $scope.submit = function () {
    $scope.message = '';
    $scope.error = '';

    if (!$scope.report.isValid($scope)) {
      $scope.error = 'You are attempting to save invalid values. Please make sure all information is valid. ';
      return;
    }

    var callBack = function (result) {
      if (result) {
        InventoryReportSubmit.update($scope.report, function () {
          $scope.message = "msg.ivd.submitted.successfully";
          $location.path('/');
        });
      }
    };
    var options = {
      id: "confirmDialog",
      header: "Confirm Submit Action",
      body: "Are you sure? Please confirm."
    };
    OpenLmisDialog.newDialog(options, callBack, $dialog);
  };

  $scope.cancel = function () {
    $location.path('/');
  };

  $scope.addAefiProduct = function(effect){
    if(isUndefined(effect.relatedLineItems)){
      effect.relatedLineItems = [];
    }
    effect.relatedLineItems.push({});
  };

  $scope.showAdverseEffect = function (effect, editMode) {
    $scope.currentEffect = effect;
    $scope.currentEffectMode = editMode;
    $scope.adverseEffectModal = true;
  };

  $scope.applyAdverseEffect = function (adverseEffectForm) {
    if(adverseEffectForm.$valid){
      var product = _.findWhere($scope.report.products, {'id': utils.parseIntWithBaseTen($scope.currentEffect.productId)});
      $scope.currentEffect.productName = product.primaryName;
      if (!$scope.currentEffectMode) {
        $scope.report.adverseEffectLineItems.push($scope.currentEffect);
      }
      $scope.adverseEffectModal = false;
    }

  };

  $scope.closeAdverseEffectsModal = function () {
    $scope.adverseEffectModal = false;
  };

  $scope.showCampaignForm = function (campaign, editMode) {
    $scope.currentCampaign = campaign;
    $scope.currentCampaignMode = editMode;

    $scope.campaignsModal = true;
  };

  $scope.deleteAdverseEffectLineItem = function (lineItem) {

    var callBack = function (result) {
      if (result) {
        $scope.report.adverseEffectLineItems = _.without($scope.report.adverseEffectLineItems, lineItem);
      }
    };
    var options = {
      id: "confirmDialog",
      header: "label.confirm.delete.adverse.effect.action",
      body: "msg.question.delete.adverse.effect.confirmation"
    };
    OpenLmisDialog.newDialog(options, callBack, $dialog);
  };

  $scope.deleteProductFromAdverseEffects = function(parentLineItem, lineItemToRemove){
    parentLineItem.relatedLineItems = _.without(parentLineItem.relatedLineItems, lineItemToRemove);
  };

  $scope.applyCampaign = function () {
    if (!$scope.currentCampaignMode) {
      $scope.report.campaignLineItems.push($scope.currentCampaign);
    }
    $scope.campaignsModal = false;
  };

  $scope.closeCampaign = function () {
    $scope.campaignsModal = false;
  };

  $scope.toNumber = function (val) {
    if (utils.isNumber(val)) {
      return utils.parseIntWithBaseTen(val);
    }
    return 0;
  };


 /* $scope.rowRequiresExplanation = function (product) {
    if (!isUndefined(product.discardingReasonId)) {
      var reason = _.findWhere($scope.discardingReasons, {id: utils.parseIntWithBaseTen(product.discardingReasonId)});
      return reason.requiresExplanation;
    }
    return false;
  };*/

}

CreateIvdFormController.resolve = {

  report: function ($q, $timeout, $route, InventoryReport) {
    var deferred = $q.defer();
    $timeout(function () {
      InventoryReport.get({id: $route.current.params.id}, function (data) {
        deferred.resolve(data.report);
      });
    }, 100);
    return deferred.promise;
  },
    adjustmentReasons: function ($q, $timeout, $route, AdjustmentReasons) {
    var deferred = $q.defer();

    $timeout(function () {
        AdjustmentReasons.get(function (data) {
        deferred.resolve(data.adjustmentReasons);
      });
    }, 100);
    return deferred.promise;
  }
/*  manufacturers: function ($q, $timeout, $route, ManufacturerList) {
    var deferred = $q.defer();

    $timeout(function () {
      ManufacturerList.get(function (data) {
        deferred.resolve(data.manufacturers);
      });
    }, 100);
    return deferred.promise;
  },*/
/*  operationalStatuses: function ($q, $timeout, $route, ColdChainOperationalStatus) {
    var deferred = $q.defer();

    $timeout(function () {
      ColdChainOperationalStatus.get(function (data) {
        deferred.resolve(_.filter(data.status, function (d) {
          return d.category.indexOf('CCE') >= 0;
        }));
      });
    }, 100);
    return deferred.promise;
  }*/
};
