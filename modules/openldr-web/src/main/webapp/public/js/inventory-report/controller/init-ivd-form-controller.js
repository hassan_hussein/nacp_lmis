
function InitIvdFormController($scope,facility,programs, InventoryReportFacilities, InventoryReportPeriods, InventoryReportInitiate, messageService, $location) {
     var x = {"id":1,"name":"lab"};
  $scope.programs = programs;


 var programId = parseInt($scope.programs[0].id,10);
 var facilityId =  parseInt(facility.id,10);

  $scope.onProgramChanged = function () {
    $scope.facilities = facility;
   /* InventoryReportFacilities.get({programId: $scope.filter.program}, function (data) {
      $scope.facilities = data.facilities;
    });*/
  };


  //$scope.onFacilityChanged = function () {
    if (isUndefined(facilityId)) {
      return;
    }else {
      InventoryReportPeriods.get({
        facilityId: facilityId,
        programId: programId
      }, function (data) {
          console.log(data);
        $scope.periodGridData = data.periods;
          if ($scope.periodGridData.length > 0) {
          $scope.periodGridData[0].showButton = true;
        }
        angular.forEach($scope.periodGridData, function (period) {
            console.log(period);
          period.statusMessage = getStatusText(period.status);
        });
      });
    }
  //};

  $scope.initiate = function (period) {
    if (!angular.isUndefined(period.id) && (period.id !== null)) {
      $location.path('/create/' + period.id);
    } else {
      InventoryReportInitiate.get({
        periodId: period.periodId,
        facilityId: period.facilityId,
        programId: period.programId
      }, function (data) {
        $location.path('/create/' + data.report.id);
      });
    }
  };

  function getActionButton(showButton) {
    return '<input type="button" ng-click="initiate(row.entity)" openlmis-message="button.proceed" class="btn btn-primary btn-small grid-btn" ng-show="' + showButton + '"/>';
  }

  function getStatusText(status){
    return  (status === null ? 'NOT_INITIATED' : status );
  }

  $scope.periodGridOptions = {
    data: 'periodGridData',
    canSelectRows: false,
    displayFooter: false,
    displaySelectionCheckbox: false,
    enableColumnResize: true,
    enableColumnReordering: true,
    enableSorting: false,
    showColumnMenu: false,
    showFilter: false,
    columnDefs: [
      {field: 'periodName', displayName: 'Periods'},
      {field: 'statusMessage', displayName:'Status'},
      {field: '', displayName: 'Action', cellTemplate: getActionButton('row.entity.showButton')}
    ]
  };

  if ($scope.programs.length === 1) {
    $scope.filter = {program: $scope.programs[0].id};
    $scope.onProgramChanged();
  }
}

InitIvdFormController.resolve = {

  programs: function ($q, $timeout, Programs) {
    var deferred = $q.defer();

    $timeout(function () {
      Programs.get({}, function (data) {
        deferred.resolve(data.programs);
      }, {});
    }, 100);
    return deferred.promise;
  },

  facility: function ($q, $timeout, UserHomeFacility) {
    var deferred = $q.defer();

    $timeout(function () {
      UserHomeFacility.get({}, function (data) {
        deferred.resolve(data.homeFacility);
      });
    }, 100);

    return deferred.promise;
  }



};
