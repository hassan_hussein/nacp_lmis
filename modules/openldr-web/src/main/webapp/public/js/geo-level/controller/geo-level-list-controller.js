function GeoLevelSearchController($scope, GeographicLevels, $location, DeleteGeoLevelInfo) {

    GeographicLevels.get({}, function (data) {
        $scope.geoLevels = data.geographicLevelList;
    });

    var deleteSuccessFunc = function (data) {
        $scope.$parent.message = "";
        $scope.$parent.geoLevelId = null;
        $scope.$parent.message = data.success;
        $scope.$parent.deleteGeoLevelItem = true;
        $scope.showError = false;

        $location.path('/');
    };

    var error = function (data) {
        $scope.message = "";
        $scope.error = data.data.error;
        $scope.showError = true;
    };

    $scope.deleteGeoLevel = function (result) {
        if (!result) return;
        DeleteGeoLevelInfo.delete({id: parseInt(result, 10)}, deleteSuccessFunc, error);
    };


}
