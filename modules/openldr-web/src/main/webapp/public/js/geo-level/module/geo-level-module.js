
var geoZoneModule = angular.module('geo-level', ['afyacall','ngStomp', 'ui.bootstrap.modal','leaflet-directive', 'ui.bootstrap.dialog', 'ui.bootstrap.dropdownToggle', 'ui.bootstrap.pagination', 'ngDraggable','ngLetterAvatar']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/search', {controller: GeoLevelSearchController, templateUrl: 'partials/search.html', reloadOnSearch: false}).
            when('/create-geo-level', {controller: GeoLevelController, templateUrl: 'partials/create.html', resolve: GeoLevelController.resolve}).
            when('/edit/:id', {controller: GeoLevelController, templateUrl: 'partials/create.html', resolve: GeoLevelController.resolve}).
            otherwise({redirectTo: '/search'});
    }]).run(function ($rootScope, AuthorizationService) {
        $rootScope.geoLevelSelected = "selected";
        //AuthorizationService.preAuthorize('MANAGE_GEOGRAPHIC_ZONE');
    });
