function SamplesController($scope,$dialog,$window,GetSamplesForLabByBatch,ReceiveSamplesForLabByBatch,WorkSheet,PrepareWorkSheet,$location,
                           ApproveSample,GetSamplesByBatch,GetSamplesByBatchOneRow,DispatchSample,TestSample,localStorageService,GetSampleForHub,GetSampleForHubByLabNumber,DeleteSample,homeFacility, GetSamplesForLab,AcceptSample,RejectSample,$rootScope,GetRejectedSamples,IO_BARCODE_TYPES){

//    $scope.types = IO_BARCODE_TYPES
   /* $scope.testvalue = null*/

    $scope.searchOptions = [
        {value: "facility", name: "Facility",placeholder:"Enter facility name"},
        {value: "patient", name: "Patient ID",placeholder:"Enter Patient ID"}
    ];
    $scope.selectedSearchOption = {value:"",name:"Select Option",placeholder:""};
    $scope.selectSearchType = function (searchOption) {
        $scope.selectedSearchOption = searchOption;
        // $scope.account=undefined;
        // $scope.members=undefined;

    };

    $scope.search = function (param) {

        if(!!param){
            GetSampleForHubByLabNumber.get({status:$scope.status,page: $scope.currentPage, labNumber: param},function(data){
                $scope.samples=data.samples;
                $scope.pagination = data.pagination;
                $scope.totalSamples = $scope.pagination.totalRecords;
                $scope.currentPage = $scope.pagination.page;
                $scope.$parent.totalSampleReceived=$scope.totalSamples;
            });
        }else{
            $scope.getSamples();
        }
    };


    $scope.getByBatch=function (batchNumber) {
        GetSamplesForLabByBatch.get({batchNumber:batchNumber},function(data){
            $scope.samples=data.samples;
            // $scope.pagination = data.pagination;
            // $scope.totalSamples = $scope.pagination.totalRecords;
            // $scope.currentPage = $scope.pagination.page;
            // $scope.$parent.totalSampleReceived=$scope.totalSamples;
            // console.log($scope.samples);

        });
    };
    $scope.receiveByBatch=function (batchNumber) {
        ReceiveSamplesForLabByBatch.get({batchNumber:batchNumber},function (data) {
            $scope.message=data.message;
            $scope.changeTab('REGISTERED');
        });
    };


    $scope.printResultByPatient = function(sampleId,type){

         // $scope.filter.pdformat = 1;
            var params = jQuery.param( {'sampleId':parseInt(sampleId,10)});
            var url = '/reports/download/hvl_report/' + type +'?'+ params;
            $window.open(url, "_BLANK");


    };

    $scope.deleteSample=function(id){
        var callBack=function(result){
            if(result){
                DeleteSample.delete({id:parseInt(id,10)},function(response){
                $scope.response = 'Deleted';
                });
            }
            // $location.path('#/list-samples');
            $window.location="/public/pages/vl/index.html/#/list-samples";
        };

        var options = {
            id: "confirmDialog",
            header: "Delete Sample",
            body: "Are you sure you want to delete?"
        };
        OpenLmisDialog.newDialog(options, callBack, $dialog);
    };

    $scope.edit = function (id) {
       // var data = {query: $scope.query, selectedSearchOption: $scope.selectedSearchOption};
      //  navigateBackService.setData(data);
        console.log(id);
        $location.path('/edit/' + id);
    };

/*$scope.deleteSample = function(id){
    DeleteSample.delete({id:parseInt(id)}, function(response){
        $scope.deleted = "Deleted";
    });
};*/


    $scope.customcondition= true;

    $scope.scan_options = {

       //or false
        onReceive:function(data){
            $scope.getBarcodeData(data);
        },
        onComplete:function(data){

        }, //or false
        onError:function(data){


        },//or false
        timeBeforeScanTest: 100,
        avgTimeByChar: 30,
        minLength: 6,
        endChar: [9, 13],
        startChar: [],
        scanButtonKeyCode: true,
        scanButtonLongPressThreshold: 3,
        onScanButtonLongPressed: function(){
            alert("got It");
        }
    };
    $scope.getBarcodeData = function(data){

    };
    var barcode;

    function getBarcode(barcode) {
        
    }

    $scope.getData = function(data){
       // console.log(data);
        barcode = data;
        $scope.testScan2 = barcode;
        getBarcode(barcode);
    };


    $scope.barcodeScanned = function(barcode) {

        console.log('callback received barcode: ' + barcode);
        $scope.model.barcode = barcode;
    };

    $scope.status='REGISTERED';
    $scope.homeFacility=homeFacility;

    $scope.getSamples=function(){
       if($scope.homeFacility.facilityType.code == 'HUB' || !$scope.homeFacility.sdp ){
           //$scope.status='REGISTERED';
        //    console.log('callled');
           GetSampleForHub.get({status:$scope.status,page: $scope.currentPage},function(data){
            //    console.log(data);
                $scope.samples=data.samples;
                $scope.pagination = data.pagination;
                $scope.totalSamples = $scope.pagination.totalRecords;
                $scope.currentPage = $scope.pagination.page;
                $scope.$parent.totalSampleReceived=$scope.totalSamples;
           });
       }
       else{
           GetSamplesForLab.get({status:$scope.status,page: $scope.currentPage},function(data){
                $scope.samples=data.samples;
                $scope.pagination = data.pagination;
                $scope.totalSamples = $scope.pagination.totalRecords;
                $scope.currentPage = $scope.pagination.page;
                $scope.$parent.totalSampleReceived=$scope.totalSamples;
                console.log($scope.samples);

            });

       }

    };

    $scope.changeTab=function(status){
      $scope.status=status;
        $scope.message=undefined;
      $scope.getSamples();
    };

    $scope.acceptSample=function(sampleId){

         var callBack=function(result){
               if(result){
                    AcceptSample.get({sampleId:sampleId},function(data){
                         $scope.getSamples();
                         $scope.message="Sample accepted successful";
                    });
                }
         };

         var options = {
               id: "confirmDialog",
               header: "label.confirm.accept.sample",
               body: "msg.sample.accept.confirmation"
         };
         OpenLmisDialog.newDialog(options, callBack, $dialog);
    };

    $scope.testSample=function(sampleId){
         console.log(sampleId);
         var callBack=function(result){
               if(result){
                    TestSample.get({sampleId:sampleId},function(data){
                         $scope.getSamples();
                          $scope.printSample(data.sample);
                    });
                }
         };

         var options = {
               id: "confirmDialog",
               header: "label.confirm.test.sample",
               body: "msg.sample.test.confirmation"
         };
         OpenLmisDialog.newDialog(options, callBack, $dialog);
    };
    $scope.approveSample=function(sampleId){
         var callBack=function(result){
               if(result){
                    ApproveSample.get({sampleId:sampleId},function(data){
                         $scope.getSamples();
                         // $scope.printSample(data.sample);
                    });
                }
        };
        var options = {
                       id: "confirmDialog",
                       header: "label.confirm.approve.sample",
                       body: "msg.sample.approve.confirmation"
                 };
                 OpenLmisDialog.newDialog(options, callBack, $dialog);

    };

    $scope.dispatchSample=function(sampleId){
         var callBack=function(result){
               if(result){
                    DispatchSample.get({sampleId:sampleId},function(data){
                         $scope.getSamples();
                         $scope.printSample(data.sample);
                    });
                }
           };

         var options = {
               id: "confirmDialog",
               header: "label.confirm.dispatch.sample",
               body: "msg.sample.dispatch.confirmation"
         };
         OpenLmisDialog.newDialog(options, callBack, $dialog);
    };

    $scope.rejectSample=function(sampleId){

        var callBack=function(result){
           if(result)
           {
              $scope.showRejectedReasonModal(sampleId);
           }
        };

        var options = {
                                      id: "confirmDialog",
                                      header: "label.confirm.reject.sample",
                                      body: "msg.sample.reject.confirmation"
                      };
         OpenLmisDialog.newDialog(options, callBack, $dialog);
    };

    $scope.showRejectedReasonModal=function(sampleId){
       $scope.sampleToReject={sampleId:sampleId,reason:undefined};
       $scope.rejectSampleModal=true;
    };

    $scope.saveRejected=function(){
     if($scope.sampleToReject !== undefined)
       RejectSample.get({sampleId:$scope.sampleToReject.sampleId,reason:$scope.sampleToReject.reason},function(data){
             $scope.rejectSampleModal=false;
             $scope.getSamples();
        });
    };
    $scope.hideRejectedReasonModal=function(){
       $scope.sampleToReject=undefined;
       $scope.rejectSampleModal=false;
    };

    $scope.showSampleModal=function(sample){
        $scope.sampleToShow=sample;
        $scope.code = sample.labNumber;
        
       // $scope.code = "268803";
            $scope.type = 'CODE128B';
            $scope.options = {
                        width: 3,
                        height: 100,
                        displayValue: true,
                        font: 'monospace',
                        textAlign: 'center',
                        fontSize: 15,
                        lineColor: '#000'
            };
        $scope.sampleModal=true;
    };
    
    $scope.hideSampleModal=function(){
      $scope.sampleModal=false;
    };

    $scope.showSampleModal2=function(sample){
        console.log(sample);
        $scope.sampleToShow=sample;
        $scope.code = sample.batchNumber;
        // $scope.code = "268803";
        $scope.type = 'CODE128B';
        $scope.options = {
            width: 3,
            height: 100,
            displayValue: true,
            font: 'monospace',
            textAlign: 'center',
            fontSize: 15,
            lineColor: '#000'
        };
        $scope.sampleModal=true;
    };
    $scope.hideSampleModal=function(){
        $scope.sampleModal=false;
    };
    $scope.printSample=function(sample){
        var url="/public/pages/print/index.html/#/print-sample/"+sample.id;
        $window.open(url, '_blank');
    };

  $scope.printSampleByBatch=function(sample){
        var url="/public/pages/print/index.html/#/print-sample/"+sample.batchNumber;
        $window.open(url, '_blank');
    };

    $scope.getSamples();
     $scope.$watch('currentPage', function () {
         if ($scope.currentPage > 0) {
            $scope.getSamples();
         }
     });

    $scope.loadRights = function () {
                $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
            }();

        $scope.hasPermission = function (permission) {
                        if ($scope.rights !== undefined && $scope.rights !== null) {
                          var rights = JSON.parse($scope.rights);
                          var rightNames = _.pluck(rights, 'name');
                          return rightNames.indexOf(permission) > -1;
                        }
                        return false;
        };

    $scope.sampleModal2 = false;

    $scope.uploadResult = function(sample){

        $location.path("/upload-result");

    };

    $scope.generateWorksheet = function(samples){
       // console.log(samples);
        $location.path("/create-worksheet");

        /*$scope.testTypes=testTypes;
        $scope.testEquipments=testEquipments;

        $scope.prepareWorkSheet=function(){

            $scope.preWorkSheet = [];
            if($scope.equipmentId !== undefined)
                PrepareWorkSheet.get({equipmentId:$scope.equipmentId},function(data){
                    $scope.type = 'CODE128B';
                    $scope.options = {
                        width: 10,
                        height: 800,
                        displayValue: true,
                        font: 'monospace',
                        textAlign: 'center',
                        fontSize: 160,
                        lineColor: '#000'
                    };
                    $scope.preWorkSheet=data.workSheet;
                    console.log(data.workSheet);

                });
        };

        $scope.sampleModal2 = true;
*/
    };

    $scope.hideWorksheetModal=function(){
        $scope.sampleModal2 = false;
        $scope.preWorkSheet = undefined;
        $scope.equipmentId = undefined;


    };

    $scope.hideWorksheetButton = false;

    $scope.toggleSingle = function() {

      $scope.selectedSamples=[];
        $scope.selectedSamples=_.where($scope.samples,{isSelected:true});
       $scope.hideWorksheetButton = $scope.selectedSamples.length > 0;

    };


    $scope.generateWorksheetForSelectedItems = function (samples) {
        $rootScope.selectedSamples =samples;
        $location.path("/create-worksheet/");
      //  console.log(samples);
    };

    $scope.checkAll = function () {

        $scope.selectedAll = !$scope.selectedAll;
        angular.forEach($scope.samples, function (sample) {
            sample.isSelected = $scope.selectedAll;
        });
    };


    $scope.createWorkSheet=function(sample){

        $scope.preWorkSheet.samples = [];

        var samples=_.where($scope.preWorkSheet.samples,{isSelected:true});

        $scope.preWorkSheet.samples=samples;
        WorkSheet.update($scope.preWorkSheet,function(data){
            $scope.workSheet=data.workSheet;
            $scope.printWorkSheet($scope.workSheet);
            $window.location="/public/pages/vl/index.html/#/list-worksheet";
        });
    };

    $scope.printWorkSheet=function(workSheet){
        var url="/public/pages/print/index.html/#/print-worksheet/"+workSheet.id;
        $window.open(url, '_blank');
    };



}

SamplesController.resolve = {
    homeFacility: function ($q, HomeFacility, $timeout) {
           var deferred = $q.defer();

           $timeout(function () {
               HomeFacility.get({}, function (data) {
                   deferred.resolve(data.homeFacility);
               }, function () {
               });
           }, 100);

           return deferred.promise;
       },

    testTypes: function ($q, AllHIVTestTypes, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            AllHIVTestTypes.get({}, function (data) {
                deferred.resolve(data.testTypes);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    } ,

    testEquipments: function ($q, AllHIVTestEquipments, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            AllHIVTestEquipments.get({}, function (data) {
                deferred.resolve(data.equipments);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    }
};

