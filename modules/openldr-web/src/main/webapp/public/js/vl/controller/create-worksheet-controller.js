function CreateWorkSheetController($scope, $rootScope, $window, testEquipments, testTypes, WorkSheet, PrepareWorkSheet) {
    console.log($rootScope.selectedSamples);

    $scope.testTypes = testTypes;
    $scope.testEquipments = testEquipments;

    $scope.prepareWorkSheet = function () {
        if ($scope.equipmentId !== undefined)
            PrepareWorkSheet.get({
                equipmentId: $scope.equipmentId
            }, function (data) {
                $scope.type = 'CODE128B';
                $scope.options = {
                    width: 3,
                    height: 95,
                    displayValue: true,
                    // font: 'monospace',
                    textAlign: 'center',
                    fontSize: 30,
                    lineColor: '#000'
                };
                if (($rootScope.selectedSamples !== undefined) && $rootScope.selectedSamples.length > 0) {
                    data.workSheet.samples = [];
                    data.workSheet.samples = $rootScope.selectedSamples;
                    $rootScope.selectedSamples = [];
                    $scope.preWorkSheet = data.workSheet;
                } else {
                    $scope.preWorkSheet = data.workSheet;
                }
            });
    };

    $scope.createWorkSheet = function () {

        var samples = _.where($scope.preWorkSheet.samples, {
            isSelected: true
        });

        $scope.preWorkSheet.samples = samples;
        WorkSheet.update($scope.preWorkSheet, function (data) {
            $scope.workSheet = data.workSheet;
            $scope.printWorkSheet($scope.workSheet);
            $window.location = "/public/pages/vl/index.html/#/list-worksheet";
        });
    };

    $scope.printWorkSheet = function (workSheet) {
        var url = "/public/pages/print/index.html/#/print-worksheet/" + workSheet.id;
        $window.open(url, '_blank');
    };
}

CreateWorkSheetController.resolve = {

    testTypes: function ($q, AllHIVTestTypes, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            AllHIVTestTypes.get({}, function (data) {
                deferred.resolve(data.testTypes);
            }, function () {});
        }, 100);

        return deferred.promise;
    },

    testEquipments: function ($q, AllHIVTestEquipments, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            AllHIVTestEquipments.get({}, function (data) {
                deferred.resolve(data.equipments);
            }, function () {});
        }, 100);

        return deferred.promise;
    }
};