function AgeSummaryReportController($scope,$filter,GetAgeSummaryReport,GetRegisterReport,$window){
    $scope.statusFilter = [{'name':'APPROVED'},{'name':'RESULTED'},{'name':'DISPATCHED'},{'name':'RECEIVED'}];

    $scope.startAges= [1,2,3,4,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30
                 ,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,
                  57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,
                  89,90,91,92,93,94,95,96,97,98,99,100];

    $scope.endAges=  [1,2,3,4,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30
        ,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,
        57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,
        89,90,91,92,93,94,95,96,97,98,99,100];

    $scope.exportReport = function (type) {

        // $scope.startDate = $filter('date')($scope.getSanitizedParameter().startTime, "yyyy-MM-dd");
        $scope.startDate = $filter('date')($scope.getSanitizedParameter().startTime, "dd-MM-yyyy")
        console.log($scope.startDate);

        // $scope.endDate = $filter('date')($scope.getSanitizedParameter().endTime, "yyyy-MM-dd");
        $scope.endDate = $filter('date')($scope.getSanitizedParameter().endTime, "dd-MM-yyyy");
        var params = {'startTime':$scope.startDate,'endTime':$scope.endDate,'status':$scope.getSanitizedParameter().status,


            'pdformat':1};

        $scope.getSanitizedParameter().pdformat = 1;
        console.log($scope.getSanitizedParameter());
        var params = jQuery.param(params);
        var url = '/reports/download/register_report/' + type + '?' + params;
        $window.open(url, "_BLANK");
    };

    $scope.OnFilterChanged = function () {
        // clear old data if there was any
        $scope.data = $scope.datarows = [];

        console.log($scope.getSanitizedParameter());

        //  $scope.filter.max = 10000;

        $scope.startDate = $filter('date')($scope.getSanitizedParameter().startTime, "yyyy-MM-dd");

        $scope.endDate = $filter('date')($scope.getSanitizedParameter().endTime, "yyyy-MM-dd");

        var params = {'startTime':$scope.startDate,'endTime':$scope.endDate,'status':$scope.getSanitizedParameter().status,
        'startAge':$scope.getSanitizedParameter().startAge,'endAge':$scope.getSanitizedParameter().endAge
        };
        console.log(params);

        GetAgeSummaryReport.get(params, function (data) {
            console.log(data);
            if (data.pages !== undefined && data.pages.rows !== undefined) {

                $scope.data = data.pages.rows;
                $scope.paramsChanged($scope.tableParams);
            }
        });

    };


}