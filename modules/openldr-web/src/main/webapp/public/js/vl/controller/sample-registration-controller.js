function SampleRegistrationController($scope,$timeout,$rootScope,GetTotalSampleForHub,GetTotalSamplesForLab, NextBackBasicService,AllSamplesById,FacilitySupervisors,$location,$dialog,homeFacility,SupervisedLab,facilities,SearchFacilities,Sample,testingReasons,Regimen,RegimenLine,regimenGroups,drugAdherence,countries,GetRegions,GetDistricts,sampleTypes){

  $scope.sample={patient:{},facility:{}};
  $scope.sampleTypes=sampleTypes;
  $scope.testingReasons=testingReasons;
  $scope.drugAdherence=drugAdherence;
  $scope.regimenGroups=regimenGroups;
  $scope.facilities=facilities;
  $scope.invalidPatientId= false;
  $scope.showError=false;
  $scope.homeFacility=homeFacility;
    $scope.message = $location.path();

    $scope.allTotal= null;
 if(homeFacility.facilityType.code !== 'HUB')
   if(GetTotalSamplesForLab !==null)
    $rootScope.totalL = parseInt(GetTotalSamplesForLab,10) +1;
  else{
       if(GetTotalSampleForHub !==null)
           $rootScope.totalL = parseInt(GetTotalSampleForHub,10) +1;

   }

    $scope.sampleForm = {
        data :{},
        state:{}

    };



if(AllSamplesById !== null){
    $scope.sample= AllSamplesById;
    console.log($scope.sample);
}
  $scope.getSupervisedFacilities=function(){
      if(homeFacility != undefined)
          FacilitySupervisors.get({},function(data){
          $scope.labs=data.labs;
      }); /*SupervisedLab.get({},function(data){
          $scope.labs=data.labs;
          console.log(data);
      });*/
  };


   $scope.showFacilitySearchResults=function(){
       if ($scope.query === undefined || $scope.query.length < 3) return;

       if (compareQuery()) {
            SearchFacilities.get({query:$scope.query}, function(data){
               console.log(data);
                $scope.facilities = data.facilities;
                $scope.filteredFacilityList = $scope.facilities;
                $scope.previousQuery = $scope.query;
                $scope.facilityResultCount = $scope.filteredFacilityList.length;
                $scope.hasSearch=true;
            });

       }
       else {
           $scope.filteredFacilityList = _.filter($scope.facilities, function (facility) {
                  return facility.name.toLowerCase().indexOf($scope.query.toLowerCase()) !== -1;
           });
           $scope.facilityResultCount = $scope.filteredFacilityList.length;
           $scope.hasSearch=true;
       }
   };

   var compareQuery=function(){
              if (!isUndefined($scope.previousQuery)) {
                   return $scope.query.substr(0, 3) !== $scope.previousQuery.substr(0, 3);
              }
              return true;
   };

   $scope.setSelectedFacility = function (facility) {
           $scope.facilitySelected = facility;
           console.log(facility);
           $scope.sample.facility=$scope.facilitySelected;
           $scope.query = undefined;
           $scope.hasSearch=false;
   };

   $scope.clearSelectedFacility = function () {
           $scope.facilitySelected = undefined;
           $scope.sample.facility=$scope.facilitySelected;
           $scope.hasSearch=false;
   };
    $scope.callSave= function(){
        $scope.saveSample();
        console.log($scope.showError);

        if($scope.showError === true){
            alert("please fill all the field");
            return;
        }else{

            NextBackBasicService.goNext();

        }

    };

    $scope.updateSample = function(){

    };
    
    $scope.saveSample=function(){

        if($scope.sampleForm.$invalid || $scope.sample.facility == undefined){
          $scope.showError=true;
          console.log($scope.sampleForm);
          return;
        }

        var pattern = RegExp('\\d{2}-\\d{2}-\\d{4}-\\d{6}$');
        // var pattern = RegExp('^\\d{6}$');
        if(!pattern.test($scope.sample.patient.patientId)){
            $scope.showError=true;
            $scope.invalidPatientId=true;
            return;
        }

        //Validate dates [RecievedDate(sample.dateReceived),DOB(sample.patient.dob),
        //SampleRequestDate(sample.sampleRequestDate),
        //DateSampleCollection(sampleCollectionDate),DateSampleDispatch(sample.dateOfSampleDispatch)]
         
        // dob can't be in future
        var now = new Date();
        if(now < $scope.sample.patient.dob){
            $scope.showError = true;
            $scope.invalidDob = true;
            console.log("dob");
            return;
        }
        // sample collection date can't be greater than received
        if( $scope.sample.sampleCollectionDate > $scope.sample.dateReceived){
            $scope.showError = true;
            $scope.invalidCollectionDate = true;
            console.log("collection date is invalid");
            return;
        }
        // sample request date can't be greater than dispatch date,
        if( $scope.sample.dateOfSampleDispatch !== undefined && $scope.sample.sampleRequestDate > $scope.sample.dateOfSampleDispatch){
            $scope.showError = true;
            $scope.invalidRequestDate = true;
            console.log("dispatch/request date is invalid");
            return;
        }

       var callBack=function(result){
            if(result){
                $scope.sample.facilityId=$scope.sample.facility.id;
                $scope.sample.regionId = $scope.facilitySelected.location.regionId;
                $scope.sample.geographicZoneId = $scope.facilitySelected.location.districtId;


            if(homeFacility.facilityType.code === 'HUB') {
                $scope.sample.hubId = homeFacility.id;
                if(GetTotalSampleForHub !== null){
                    $rootScope.allTotal = GetTotalSampleForHub;
                    console.log(  $rootScope.allTotal);
                }
            }
              if(
                  $scope.sample.patientHasTb === undefined ||
                  $scope.sample.patientRegimenId === undefined
              ){
                  $scope.sample.formIncomplete=true;
              }
             $timeout(function(){

              Sample.update($scope.sample,function(data){
                  if(data.sample){
                     // $location.path('');

                      $scope.sample = [];
                        $location.path('/');
                   }
                  $scope.sample ={};
                  $rootScope.message2 = 'Saved Successfully';

                  $timeout(
                function() {
                    $rootScope.message2 =' ';
                },2000);
                //  $location.path('/');

                //  alert("timed-Out");
              })},1000);

             if(GetTotalSamplesForLab !== null)
                 $rootScope.allTotal = GetTotalSamplesForLab;
             console.log(  $rootScope.allTotal);
         }

      };

       var options = {
                              id: "confirmDialog",
                              header: "label.confirm.save.sample",
                              body: "msg.create.sample.confirmation"
              };
       OpenLmisDialog.newDialog(options,callBack,$dialog);

  };
  $scope.cancel=function(){
      $location.path('/list-samples');
  };

  $scope.GetRegions=function(){
         $scope.regions=[];
         if($scope.countryId !== undefined)
         {
             GetRegions.get({countryId:$scope.countryId},function(data){
                 $scope.regions=data.regions;
             });
         }
  };
  $scope.GetDistricts=function(){
         $scope.districts=[];
          if($scope.sample.regionId !== undefined)
          {
             GetDistricts.get({regionId:$scope.sample.regionId},function(data){
                 $scope.districts=data.districts;
             });
         }
  };
  if(countries.length >0){
     $scope.countryId=countries[0].id;
     $scope.GetRegions();
  }

  $scope.getRegimenLines=function(){
    RegimenLine.get({regimenGroupId:$scope.regimenGroupId},function(data){
         $scope.regimenLines=data.regimenLines;
    });
  };

  $scope.getRegimens=function(){
  //  console.log($scope.regimenLineId);
    Regimen.get({regimenLineId:$scope.regimenLineId},function(data){
         $scope.regimens=data.regimens;
         console.log(data);
    });
  };

  $scope.getSupervisedFacilities();


}

SampleRegistrationController.resolve = {

    countries: function ($q, GetCountries, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetCountries.get({}, function (data) {
                deferred.resolve(data.countries);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },
    sampleTypes: function ($q, SampleTypes, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            SampleTypes.get({}, function (data) {
                deferred.resolve(data.sampleTypes);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    } ,
    testingReasons: function ($q, VLTestingReasons, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            VLTestingReasons.get({}, function (data) {
                deferred.resolve(data.testingReasons);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },
    drugAdherence: function ($q, DrugAdherence, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            DrugAdherence.get({}, function (data) {
                deferred.resolve(data.drugAdherence);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },
    regimenGroups: function ($q, RegimenGroup, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            RegimenGroup.get({}, function (data) {
                deferred.resolve(data.regimenGroups);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },
    facilities: function ($q, SampleFacilities, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            SampleFacilities.get({}, function (data) {
                deferred.resolve(data.facilities);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },
    homeFacility: function ($q, HomeFacility, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            HomeFacility.get({}, function (data) {
                deferred.resolve(data.homeFacility);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },

    AllSamplesById: function ($q, GetSampleAllById, $route, $timeout) {
        var sampleId = $route.current.params.id;
        console.log(sampleId);
        if (!sampleId) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            GetSampleAllById.get({id: parseInt(sampleId,10)}, function (data) {
                // if(!isUndefined(data.rights))
                console.log(data.sample);
                deferred.resolve(data.sample);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    } , GetTotalSamplesForLab: function ($q, GetTotalSampleForLab, $route, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            GetTotalSampleForLab.get({status:'RECEIVED'}, function (data) {
                // if(!isUndefined(data.rights))
                console.log(data.totalSample);
                deferred.resolve(data.totalSample);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }, GetTotalSampleForHub: function ($q, GetTotalSampleForHub, $route, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            GetTotalSampleForHub.get({status:'REGISTERED'}, function (data) {
                // if(!isUndefined(data.rights))
                console.log(data.totalSample);
                deferred.resolve(data.totalSample);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }

};

