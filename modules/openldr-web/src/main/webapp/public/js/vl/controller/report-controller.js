function RegistryReportControllerFunction($scope,GetRegisterReport, $filter, $window){
    $scope.statusFilter = [{'name':'APPROVED'},{'name':'RESULTED'},{'name':'DISPATCHED'},{'name':'RECEIVED'}];

    $scope.exportReport = function (type) {

        $scope.startDate = $filter('date')($scope.getSanitizedParameter().startTime, "yyyy-MM-dd");
        console.log($scope.startDate);

        $scope.endDate = $filter('date')($scope.getSanitizedParameter().endTime, "yyyy-MM-dd");
        var params = {'startTime':$scope.startDate,'endTime':$scope.endDate,'status':$scope.getSanitizedParameter().status,'pdformat':1};

        console.log(type);
        $scope.getSanitizedParameter().pdformat = 1;
        console.log($scope.getSanitizedParameter());
        var params = jQuery.param(params);
        var url = '/reports/download/register_report/' + type + '?' + params;
        $window.open(url, "_BLANK");
    };

    $scope.OnFilterChanged = function () {
        // clear old data if there was any
        $scope.data = $scope.datarows = [];

        console.log($scope.getSanitizedParameter().startTime);

      //  $scope.filter.max = 10000;

       $scope.startDate = $filter('date')($scope.getSanitizedParameter().startTime, "yyyy-MM-dd");

        $scope.endDate = $filter('date')($scope.getSanitizedParameter().endTime, "yyyy-MM-dd");
        console.log($scope.endDate);

        var params = {'startTime':$scope.startDate,'endTime':$scope.endDate,'status':$scope.getSanitizedParameter().status};
        GetRegisterReport.get(params, function (data) {
            console.log(data);
            if (data.pages !== undefined && data.pages.rows !== undefined) {

                $scope.data = data.pages.rows;
                $scope.paramsChanged($scope.tableParams);
            }
        });

    };

    $scope.formatNumber = function (value) {
        return utils.formatNumber(value, '0,0.00');
    };


    $scope.showPopover=false;

    $scope.popover = {
        title: 'Manufacturer Name',
        message: 'Message'
    };




}