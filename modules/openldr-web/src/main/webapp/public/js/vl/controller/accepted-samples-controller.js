function AcceptedSamplesController($scope,$window, AcceptedSamples,IO_BARCODE_TYPES){

//    $scope.types = IO_BARCODE_TYPES

    $scope.getSamples=function(){
        AcceptedSamples.get({},function(data){
             $scope.samples=data.samples;
             $scope.$parent.totalSampleAccepted=data.samples.length;
             console.log($scope.samples);
        });
    };


    $scope.showAcceptedSampleModal=function(sample){
            $scope.acceptedSample=sample;
            $scope.code = sample.labNumber;
           // $scope.code = "268803";
                $scope.type = 'CODE128B';
                $scope.options = {
                            width: 3,
                            height: 100,
                            displayValue: true,
                            font: 'monospace',
                            textAlign: 'center',
                            fontSize: 15,
                            lineColor: '#000'
                };
            $scope.acceptedSampleModal=true;
    };
     $scope.hideAcceptedSampleModal=function(){
          $scope.acceptedSampleModal=false;
    };
     $scope.printSample=function(sample){
            var url="/public/pages/print/index.html/#/print-sample/"+sample.id;
            $window.open(url, '_blank');
     };

    $scope.getSamples();
}

AcceptedSamplesController.resolve = {
};

