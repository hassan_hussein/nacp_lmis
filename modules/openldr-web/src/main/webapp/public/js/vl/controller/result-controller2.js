function Result2Controller($scope,homeFacility,GetResultReport,$http,$filter,$window,GetResultsList,GetDispatchedResults) {
$scope.statusFilter = [{'name':'APPROVED'},{'name':'RESULTED'},{'name':'DISPATCHED'}];
 /*   $scope.exportReport = function(type){
        // $scope.filter.pdformat = 1;
      //  var params = jQuery.param( {'sampleId':parseInt(sampleId,10)});
        var url = '/reports/download/result2_report/' + type ;
        $window.open(url, "_BLANK");


    };
*/

    $scope.exportReport = function (type) {

        $scope.startDate = $filter('date')($scope.getSanitizedParameter().startTime, "yyyy-MM-dd");
        console.log($scope.startDate);

        $scope.endDate = $filter('date')($scope.getSanitizedParameter().endTime, "yyyy-MM-dd");
        var params = {'startDate':$scope.startDate,'endDate':$scope.endDate,'status':$scope.getSanitizedParameter().status,'pdformat':1};

        console.log(type);
        $scope.getSanitizedParameter().pdformat = 1;
        console.log($scope.getSanitizedParameter());
         params = jQuery.param(params);
        var url = '/reports/download/result2_report/' + type + '?' + params;
        $window.open(url, "_BLANK");
    };

    $scope.OnFilterChanged = function () {
        // clear old data if there was any

        $scope.data = $scope.datarows = [];

        console.log($scope.getSanitizedParameter());

        //  $scope.filter.max = 10000;

        $scope.startDate = $filter('date')($scope.getSanitizedParameter().startTime, "yyyy-MM-dd");

        $scope.endDate = $filter('date')($scope.getSanitizedParameter().endTime, "yyyy-MM-dd");
        console.log($scope.endDate);

        var params = {'startDate':$scope.startDate,'endDate':$scope.endDate,'status':$scope.getSanitizedParameter().status};
        GetResultReport.get(params, function (data) {
            if (data.pages !== undefined && data.pages.rows !== undefined) {
                $scope.changePresentation = true;
                $scope.results = data.pages.rows;
                console.log($scope.results);

                //$scope.paramsChanged($scope.tableParams);
            }
        });

    };




    $scope.homeFacility = homeFacility;
    console.log($scope.currentPage);

    $scope.status = 'DISPATCHED';

    $scope.viewResults = function (){

    if ($scope.homeFacility.facilityType.code == 'HUB' || !$scope.homeFacility.sdp) {

        GetDispatchedResults.get({status: $scope.status, page: $scope.currentPage}, function (data) {
            // if(!isUndefined(data.rights))
            console.log(data.results);

            $scope.changePresentation = false;
            $scope.results = data.results;
            $scope.pagination = data.pagination;
            $scope.totalSamples = $scope.pagination.totalRecords;
            $scope.currentPage = $scope.pagination.page;
            $scope.$parent.totalSampleReceived = $scope.totalSamples;
            //  deferred.resolve(data.results);
        }, function () {
        });


    }else{

        GetResultsList.get({status: $scope.status, page: $scope.currentPage},function (data) {
            if(data.results.length> 0)
                $scope.results = data.results;
            $scope.changePresentation = false;
            $scope.stopResultForLabManager = true;
            console.log($scope.results);
        });


    }
};

    $scope.viewResults();
    $scope.$watch('currentPage', function () {
        if ($scope.currentPage > 0) {
            $scope.viewResults();
        }
    });




/*   if(GetResultsForHub !==  null)
    $scope.results=GetResultsForHub;*/


    $scope.printResultByPatient = function(sampleId,type){
        // $scope.filter.pdformat = 1;
        var params = jQuery.param( {'sampleId':parseInt(sampleId,10)});
        var url = '/reports/download/hvl_report/' + type +'?'+ params;
        $window.open(url, "_BLANK");


    };
}

Result2Controller.resolve = {

    homeFacility: function ($q, HomeFacility, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            HomeFacility.get({}, function (data) {
                deferred.resolve(data.homeFacility);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },
     GetResultsForHub: function ($q, GetDispatchedResults, $route, $timeout) {
    var deferred = $q.defer();
    $timeout(function () {
        GetDispatchedResults.get({status:'DISPATCHED'}, function (data) {
            // if(!isUndefined(data.rights))
            console.log(data.results);
            deferred.resolve(data.results);
        }, function () {
        });
    }, 100);
    return deferred.promise;
}
};