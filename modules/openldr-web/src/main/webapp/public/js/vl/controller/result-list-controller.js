function ResultListController($scope,GetResultsList,$window,GetDispatchedResults,homeFacility) {
    $scope.homeFacility = homeFacility;
    $scope.exportReport = function (type) {
        // $scope.filter.pdformat = 1;
        var params = jQuery.param($scope.reportParams);
        var url = '/reports/download/hvl_report/' + type + '?' + params;
        $window.open(url, "_BLANK");
    };

    $scope.reportParams = {
        district: 2,
        startDate: '2017-03-01',
        endDate: '2017-10-2017'
    };

    $scope.status = 'DISPATCHED';

    $scope.viewResults = function () {

        if ($scope.homeFacility.facilityType.code == 'HUB' || !$scope.homeFacility.sdp) {

            GetDispatchedResults.get({status: $scope.status, page: $scope.currentPage}, function (data) {
                // if(!isUndefined(data.rights))
                console.log(data.results);
                $scope.results = data.results;
                $scope.pagination = data.pagination;
                $scope.totalSamples = $scope.pagination.totalRecords;
                $scope.currentPage = $scope.pagination.page;
                $scope.$parent.totalSampleReceived = $scope.totalSamples;
                //  deferred.resolve(data.results);
            }, function () {
            });


        } else {

            GetResultsList.get({status: $scope.status, page: $scope.currentPage}, function (data) {
                if (data.results.length > 0)
                    $scope.results = data.results;
                $scope.stopResultForLabManager = true;

            });

        }
    };


    $scope.viewResults();
    $scope.$watch('currentPage', function () {
        if ($scope.currentPage > 0) {
            $scope.viewResults();
        }
    });

    $scope.printResultByPatient = function(sampleId,type){
        // $scope.filter.pdformat = 1;
        var params = jQuery.param( {'sampleId':parseInt(sampleId,10)});
        var url = '/reports/download/result_report/' + type +'?'+ params;
        $window.open(url, "_BLANK");


    };

}


ResultListController.resolve = {

    homeFacility: function ($q, HomeFacility, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            HomeFacility.get({}, function (data) {
                deferred.resolve(data.homeFacility);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },

    GetResultsForHub: function ($q, GetDispatchedResults, $route, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            GetDispatchedResults.get({status:'DISPATCHED'}, function (data) {
                // if(!isUndefined(data.rights))
                console.log(data.results);
                deferred.resolve(data.results);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }

};