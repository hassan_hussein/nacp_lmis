function RejectedSampleController($scope,GetRejectedSamples){

    $scope.status= 'RECEIVED';
    GetRejectedSamples.get({}, function(data){
        $scope.rejectedSamples=data.sample;
        $scope.pagination = data.pagination;
        $scope.totalSamples = $scope.pagination.totalRecords;
        $scope.currentPage = $scope.pagination.page;
        $scope.$parent.totalSampleReceived=$scope.totalSamples;
    })


}

RejectedSampleController.resolve = {


};
