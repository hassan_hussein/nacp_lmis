function DashboardController($scope,DataPoint,SampleByType,SampleByFacility,GetSampleByAge,getTATLABSummary,
                             getTestTrendForLab,getByAgeLessThan2,getVLResults
) {

    $scope.actionBar = {openPanel: true};


 /*   $scope.chartOptions = {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'LAB TAT'
        },
        xAxis: {
            categories: ['TAT']
            ,
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color:'gray'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total fruit consumption'
            }
            ,
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color:'gray'
                }
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{ 'name':'TAT-Collection to LAB',
            data: [getTATLABSummary.tatinlab]},
            {   'name':'TAT-Inside Lab - (Receipt-Result)',
                data:[getTATLABSummary.collectiontodispatch]},
            {   'name':'TAT on Result -Result to Dispatch',
                data:[getTATLABSummary.resulttodispatch]} ,
            { 'name':'Total TAT Collection to Result',
                data:[getTATLABSummary.collectiontolab]}
        ]
    };*/







   /* GetTotalTestDoneForDashboard.get({}, function(data){
          var dataPoints = data.dataPoints;
        console.log(JSON.stringify(dataPoints));
        $scope.testTrend ={
            dataPoints:[{"test":5,"months":"Feb","rejected":1,"suspected":1},
            {"test":3,"months":"Mar","rejected":1,"suspected":1}],
            dataColumns: [
            {"id": "test", "name": "Results", "type": "line"},
            {"id": "rejected", "name": "Rejected", "type": "line"},
            {"id": "suspected", "name": "Suspected treatment failure", "type": "line"}
        ],
            dataX: {
            "id": "months"

        }

    };*/
      /*  $scope.testTrend = {
            dataPoints: dataPoints,
            dataColumns: [
                {"id": "test", "name": "Results", "type": "line"},
                {"id": "rejected", "name": "Rejected", "type": "line"},
                {"id": "suspected", "name": "Suspected treatment failure", "type": "line"}
            ],
            dataX: {
                "id": "months"

            }
        }*/

      //  });

$scope.changeColor = function(data){
    console.log(data);
};




     $scope.testingTrend = {

         dataPoints:getTestTrendForLab,
         dataColumns: [
                   {"id": "wholeblood", "name":"Whole Blood", "type": "bar"},
                   {"id": "plasma", "name":"Plasma", "type": "bar"}
         ],
                  dataX: {"id": "months"}
     };


    $scope.vlResult = {

         dataPoints:getVLResults,
        dataColumns: [
            {"id": "lessthan2", "name":"suppressed", "type": "pie"},
            {"id": 'above', "name":"Above Cut Off", "type": "pie"},
            {"id": "belowcutoff", "name":"Not Suppressed", "type": "pie"}
        ],
            dataX: {"id": "totalResult"}
     };

    var filteredData = _.uniq(getByAgeLessThan2,'age');
    $scope.getByAgeLessThan2 = {

         dataPoints:filteredData,
         dataColumns: [
                   {"id": "suppressed", "name":"Suppressed (<=50 copies/ml)", "type": "bar"},
                   {"id": "notsuppressed", "name":"Not Suppressed (>50 copies/ml)", "type": "bar"}
         ],
                  dataX: {"id": "age"}
     };

    var genderTest = [
        {"status":"Less than 50cp/ml","male":200,"female":400} ,
        {"status":"Greater Than 50cp/ml","male":300,"female":500}

    ];

    $scope.getTestByGender = {
        dataPoints:genderTest,
        dataColumns: [
            {"id": "male", "name":"Male", "type": "bar"},
            {"id": "female", "name":"Female", "type": "bar"}
        ],
        dataX: {"id": "status"}


    };


    var rgimenTest = [{"indicator":"suppressed", "firstLine":400, "secondLine":300},
                          {"indicator":"Not Suppressed","firstLine":300,"secondLine":500}];

     $scope.getRegimenTest = {

         dataPoints:rgimenTest,

         dataColumns: [
             {"id": "firstLine", "name":"First Line", "type": "bar"},
             {"id": "secondLine", "name":"Second Line", "type": "bar"}
         ],
         dataX: {"id": "indicator"}
     };

$scope.testTrend = {

         dataPoints:DataPoint,
         dataColumns: [
                   {"id": "test", "name":"Results", "type": "line"},
                   {"id": "suspected", "name":"Suspected treatment failure", "type": "line"},
                   {"id": "rejected", "name":"Rejected", "type": "line"}
         ],
                  dataX: {"id": "months"}
     };

  $scope.testBySample = {
                dataPoints:SampleByType,
                dataColumns: [
                             {"id": "request", "name":"All Test", "type": "bar"},
                             {"id": "repeated", "name":"Requested", "type": "bar"}
                ],
                dataX: {"id": "sample"}
  };

    $scope.testByAge = {
                dataPoints:GetSampleByAge,
                dataColumns: [
                             {"id": "totalTest", "name":"All Tests", "type": "spline"},
                             {"id": "age", "name":"Age", "type": "spline"}
                ],
                dataX: {"id": "totalTest"}
  };
 $scope.labPerformance = {
                dataPoints:SampleByFacility ,
                dataColumns: [
                             {"id": "request", "name":"All Test", "type": "bar"},
                             {"id": "repeated", "name":"Requested", "type": "bar"}
                ],
                dataX: {"id": "facilityName"}
  };

    var rejectionRateData = [{"period":"January","insufficiencyVolume":2, "poorSampleQuality":5,"hermolizedBlood":5,
        "exceedingStorageTime":4,"wrongTube":20,"poorDocumentation":60},
        {"period":"February","insufficiencyVolume":2, "poorSampleQuality":10,"hermolizedBlood":5,
        "exceedingStorageTime":0,"wrongTube":30,"poorDocumentation":45},
        {"period":"March","insufficiencyVolume":2, "poorSampleQuality":10,"hermolizedBlood":5,
        "exceedingStorageTime":8,"wrongTube":30,"poorDocumentation":45},
        {"period":"April","insufficiencyVolume":2, "poorSampleQuality":10,"hermolizedBlood":5,
        "exceedingStorageTime":8,"wrongTube":30,"poorDocumentation":45},
        {"period":"May","insufficiencyVolume":2, "poorSampleQuality":10,"hermolizedBlood":5,
        "exceedingStorageTime":8,"wrongTube":30,"poorDocumentation":45}];

    $scope.rejectionRate = {

        dataPoints:rejectionRateData ,
        dataColumns: [
            {"id": "insufficiencyVolume", "name":"Insufficiency Volume", "type": "spline"},
            {"id": "poorSampleQuality", "name":"Poor Sample Quality", "type": "spline"},
            {"id": "hermolizedBlood", "name":"Hermolized Blood", "type": "spline"},
            {"id": "exceedingStorageTime", "name":"Exceeding Storage Time", "type": "spline"},
            {"id": "wrongTube", "name":"Wrong Tube", "type": "spline"},
            {"id": "poorDocumentation", "name":"Poor Documentation", "type": "spline"}

        ],
        dataX: {"id": "period"}
    };



/*console.log(JSON.stringify(getTATLABSummary));
    var getTATLABSummary ={
        "tatinlab":12,"collectiontodispatch":3,"resulttodispatch":30,"collectiontolab":10};*/
    //Start from here
    $scope.getTAT = {};

    $scope.getTAT = getTATLABSummary;
    if(!isUndefined(getTATLABSummary)){
        $scope.getTAT = getTATLABSummary;
        console.log(getTATLABSummary);

    }

    if(!isUndefined(getTATLABSummary)) {

        $(function () {

            /**
             * Highcharts Linear-Gauge series plugin
             */
            (function (H) {
                var defaultPlotOptions = H.getOptions().plotOptions,
                    columnType = H.seriesTypes.column,
                    wrap = H.wrap,
                    each = H.each;

                defaultPlotOptions.lineargauge = H.merge(defaultPlotOptions.column, {});
                H.seriesTypes.lineargauge = H.extendClass(columnType, {
                    type: 'lineargauge',
                    //inverted: true,
                    setVisible: function () {
                        columnType.prototype.setVisible.apply(this, arguments);
                        if (this.markLine) {
                            this.markLine[this.visible ? 'show' : 'hide']();
                        }
                    },
                    drawPoints: function () {
                        // Draw the Column like always
                        columnType.prototype.drawPoints.apply(this, arguments);

                        // Add a Marker
                        var series = this,
                            chart = this.chart,
                            inverted = chart.inverted,
                            xAxis = this.xAxis,
                            yAxis = this.yAxis,
                            point = this.points[0], // we know there is only 1 point
                            markLine = this.markLine,
                            ani = markLine ? 'animate' : 'attr';

                        // Hide column
                        point.graphic.hide();

                        if (!markLine) {
                            var path = inverted ? ['M', 0, 0, 'L', -3, -3, 'L', 3, -3, 'L', 0, 0, 'L', 0, 0 + xAxis.len] : ['M', 0, 0, 'L', -3, -3, 'L', -3, 3, 'L', 0, 0, 'L', xAxis.len, 0];
                            markLine = this.markLine = chart.renderer.path(path)
                                .attr({
                                    'fill': series.color,
                                    'stroke': series.color,
                                    'stroke-width': 1
                                }).add();
                        }
                        markLine[ani]({
                            translateX: inverted ? xAxis.left + yAxis.translate(point.y) : xAxis.left,
                            translateY: inverted ? xAxis.top : yAxis.top + yAxis.len - yAxis.translate(point.y)
                        });
                    }
                });
            }(Highcharts));

            $('#tat').highcharts({
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'lineargauge',
                        inverted: true
                    },
                    title: {
                        text: null
                    },
                    xAxis: {
                        lineColor: '#C0C0C0',
                        labels: {
                            enabled: false
                        },
                        tickLength: 0
                    },
                    yAxis: {
                        min: 0,
                        max: 30,
                        tickLength: 4,
                        tickWidth: 1,
                        tickColor: '#C0C0C0',
                        gridLineColor: '#C0C0C0',
                        gridLineWidth: 1,
                        minorTickInterval: 4,
                        minorTickWidth: 1,
                        minorTickLength: 4,
                        minorGridLineWidth: 0,

                        title: null,
                        labels: {
                            format: '{value}'
                        },
                        plotBands: [
                            {
                                from: 0,
                                to: getTATLABSummary.collectiontolab,
                                color: 'rgba(0,255,0,0.5)'
                            }, {
                                from: getTATLABSummary.collectiontolab,
                                to: getTATLABSummary.tatinlab,
                                color: 'rgba(255,0,255,0.5)'
                            }, {
                                from: getTATLABSummary.tatinlab,
                                to: getTATLABSummary.resulttodispatch,
                                color: 'rgba(255,255,0,0.5)'
                            }, {
                                from: getTATLABSummary.resulttodispatch,
                                to: getTATLABSummary.collectiontodispatch,
                                color: 'rgba(255,255,255,255)'
                            }]
                    },
                    legend: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    }, colors: [
                        '#F64747',
                        '#F9BF3B',
                        '#26C281',
                        '#115539'
                    ],
                    series: [{
                        data: [60],
                        color: '#000000',
                        dataLabels: {
                            enabled: true,
                            align: 'center',
                            format: '{point.y}',
                            y: 10
                        }
                    }]

                },
                // Add some life
                function (chart) {
                    Highcharts.each(chart.series, function (serie) {
                        var point = serie.points[0];
                        point.update(30);
                    });

                });
        });
    }

}

DashboardController.resolve = {
    DataPoint: function ($q, GetTotalTestDoneForDashboard, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetTotalTestDoneForDashboard.get({}, function (data) {
                deferred.resolve(data.dataPoints);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    } ,

    SampleByFacility: function ($q, GetSampleByFacilityForDashboard, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetSampleByFacilityForDashboard.get({}, function (data) {
                deferred.resolve(data.testByLabs);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },

    SampleByType: function ($q, GetTestBySampleForDashboard, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetTestBySampleForDashboard.get({}, function (data) {

                deferred.resolve(data.testBySample);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },

    GetSampleByAge: function ($q, GetSampleTestByAge, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetSampleTestByAge.get({}, function (data) {
                if(data != null) {
                    deferred.resolve(data.testByAge);
                }
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },

    getTATLABSummary: function ($q, GetLabTatSummary, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetLabTatSummary.get({}, function (data) {
                var summary = {};
                if (!isUndefined(data.labTatSummary)) {
                    summary = data.labTatSummary;

                }
                console.log(summary);
                deferred.resolve(summary);

            }, function () {
            });
        }, 100);

        return deferred.promise;
    } ,
    getTestTrendForLab: function ($q, GetTestTrendForLab, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetTestTrendForLab.get({}, function (data) {
                if(data != null) {
                    deferred.resolve(data.testTrendForLab);
                }
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },
    getVLResults: function ($q, GetVLResults, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetVLResults.get({}, function (data) {
                if(data != null) {
                    deferred.resolve(data.vlResults);
                }
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },
    getByAgeLessThan2: function ($q, GetByAgeLessThan2, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetByAgeLessThan2.get({}, function (data) {
                var summary = [];
                if (!isUndefined(data.vlResults)) {
                    summary = data.vlResults;

                }
                console.log(summary);
                deferred.resolve(data.vlResults);


            }, function () {
            });
        }, 100);

        return deferred.promise;
    }

};