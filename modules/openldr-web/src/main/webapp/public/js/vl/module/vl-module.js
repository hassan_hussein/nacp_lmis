angular.module('vl', ['afyacall','ui.bootstrap.modal',
    'angularCombine','io-barcode','scanner.detection','ui.bootstrap.pagination',
    'ui.bootstrap.dialog','gridshore.c3js.chart','ngLetterAvatar',
    'ngTable','ngRoute',
    'rorymadden.date-dropdowns','export.csv']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/list-samples', {controller: SamplesController, templateUrl: '/public/pages/vl/partials/list-samples.html',resolve: SamplesController.resolve}).
            when('/accepted-samples', {controller: AcceptedSamplesController, templateUrl: '/public/pages/vl/partials/accepted-samples.html',resolve: AcceptedSamplesController.resolve}).
            when('/inprogress-samples', {controller: InprogressSampleController, templateUrl: '/public/pages/vl/partials/accepted-samples.html',resolve: InprogressSampleController.resolve}).
            when('/create-sample', {controller: SampleRegistrationController, templateUrl: '/public/pages/vl/partials/create-sample.html',resolve: SampleRegistrationController.resolve}).
            when('/edit/:id', {controller: SampleRegistrationController, templateUrl: '/public/pages/vl/partials/edit-sample.html',resolve: SampleRegistrationController.resolve}).
            when('/create-worksheet', {controller: CreateWorkSheetController, templateUrl: '/public/pages/vl/partials/create-worksheet.html',resolve: CreateWorkSheetController.resolve}).
            when('/rejected-sample', {controller: RejectedSampleController, templateUrl: '/public/pages/vl/partials/rejected-samples.html',resolve: RejectedSampleController.resolve}).
            when('/dashboard', {controller: DashboardController, templateUrl: '/public/pages/vl/partials/dashboard2.html',resolve: DashboardController.resolve, refreshOnLoad:true}).
            when('/upload-result', {controller: UploadResultController, templateUrl: '/public/pages/vl/partials/upload-result.html'}).
            when('/results', {controller: ResultController, templateUrl: '/public/pages/vl/partials/results.html',resolve: ResultController.resolve}).
            when('/results2', {controller: Result2Controller, templateUrl: '/public/pages/vl/partials/results2.html',resolve: Result2Controller.resolve}).
            when('/results-list', {controller: ResultListController, templateUrl: '/public/pages/vl/partials/results.html',resolve: ResultListController.resolve}).
            when('/commodities', {controller: CommoditiesController, templateUrl: '/public/pages/vl/partials/commodities.html',resolve: CommoditiesController.resolve}).
            when('/registry-report', {controller: RegistryReportControllerFunction, templateUrl: '/public/pages/vl/partials/registry.html',resolve: RegistryReportControllerFunction.resolve}).
            when('/age-summary', {controller: AgeSummaryReportController, templateUrl:'/public/pages/vl/partials/age-summary.html',reloadOnSearch:false}).
            when('/facility-list', {controller: FacilityListReportController, templateUrl:'/public/pages/vl/partials/facility-list.html',reloadOnSearch:false}).

            otherwise({redirectTo: '/create-sample'});
    }]).config(function(angularCombineConfigProvider) {
        angularCombineConfigProvider.addConf(/filter-/, '/public/pages/reports/shared/filters.html');
    }).run(function ($rootScope, AuthorizationService) {
        $rootScope.sampleRegistrationSelected = "selected";
    }).run(function($rootScope, NextBackBasicService){
    $rootScope.goNext = function() {
        NextBackBasicService.goNext();
    };

    $rootScope.goBack = function() {
        NextBackBasicService.goBack();
    };
}).directive('autoSaveForm', function($timeout) {

        return {
            require: ['^form'],
            link: function($scope, $element, $attrs, $ctrls) {

                var $formCtrl = $ctrls[0];
                var savePromise = null;
                var expression = $attrs.autoSaveForm || 'true';

                $scope.$watch(function() {

                    if($formCtrl.$valid && $formCtrl.$dirty) {

                        if(savePromise) {
                            $timeout.cancel(savePromise);
                        }

                        savePromise = $timeout(function() {

                            savePromise = null;

                            // Still valid?

                            if($formCtrl.$valid) {

                                if($scope.$eval(expression) !== false) {
                                    console.log('Form data persisted -- setting prestine flag');
                                    $formCtrl.$setPristine();
                                }

                            }

                        }, 500);
                    }

                });
            }
        };

    }).directive('showFocus', function($timeout) {
        return function(scope, element, attrs) {
            scope.$watch(attrs.showFocus,
                function (newValue) {
                    $timeout(function() {
                        newValue && element.focus();
                    });
                },true);
        };
    }).directive('hcChart', function () {
        return {
            restrict: 'E',
            template: '<div></div>',
            scope: {
                options: '='
            },
            link: function (scope, element) {
                Highcharts.chart(element[0], scope.options);
            }
        };
    }).filter('positive', function() {
        return function(input) {
            if (!input) {
                return 0;
            }

            return Math.abs(input);
        };
    }).filter('patientFilter', function($filter) {
        return function(patientId) {
            if (patientId == undefined || patientId.length < 12)
                return patientId;
            var c = '-',p = patientId.replace(/-/g,'');
            // console.log(p);
            return [p.slice(0,2),c,p.slice(2,4),c,p.slice(4,8),c,p.slice(8).padStart(6,'0')].join('');
        };
    });

    /*.directive('barcodeScanner', function() {
        return {
            restrict: 'A',
            scope: {
                callback: '=barcodeScanner',
            },
            link:    function postLink(scope, iElement, iAttrs){
                // Settings
                var zeroCode = 48;
                var nineCode = 57;
                var enterCode = 13;
                var minLength = 3;
                var delay = 300; // ms

                // Variables
                var pressed = false;
                var chars = [];
                var enterPressedLast = false;

                // Timing
                var startTime = undefined;
                var endTime = undefined;

                jQuery(document).keypress(function(e) {
                    if (chars.length === 0) {
                        startTime = new Date().getTime();
                    } else {
                        endTime = new Date().getTime();
                    }

                    // Register characters and enter key
                    if (e.which >= zeroCode && e.which <= nineCode) {
                        chars.push(String.fromCharCode(e.which));
                    }

                    enterPressedLast = (e.which === enterCode);

                    if (pressed == false) {
                        setTimeout(function(){
                            if (chars.length >= minLength && enterPressedLast) {
                                var barcode = chars.join('');
                                //console.log('barcode : ' + barcode + ', scan time (ms): ' + (endTime - startTime));

                                if (angular.isFunction(scope.callback)) {
                                    scope.$apply(function() {
                                        scope.callback(barcode);
                                    });
                                }
                            }
                            chars = [];
                            pressed = false;
                        },delay);
                    }
                    pressed = true;
                });
            }
        };
    });
*/