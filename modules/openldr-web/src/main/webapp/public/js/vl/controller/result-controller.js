function ResultController($scope,$http,$window,GetResults) {

    $scope.printResultByPatient = function(sampleId,type){
        // $scope.filter.pdformat = 1;
        var params = jQuery.param( {'sampleId':parseInt(sampleId,10)});
        // var url = '/reports/download/result_report/' + type +'?'+ params;
        var url = '/reports/download/hvl_report/' + type +'?'+ params;
        $window.open(url, "_BLANK");


    };
    GetResults.get(function(data){
         $scope.results=data.results;
        $scope.stopResultForLabManager = true;
         console.log($scope.results);
    });

}

ResultController.resolve = {


};