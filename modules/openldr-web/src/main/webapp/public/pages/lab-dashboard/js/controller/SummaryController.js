/**
 * Created by user on 8/10/2017.
 */
(function(){
'use strict';

    angular
        .module('nacpDashboard')
        .controller('SummaryController', ['$scope','$timeout','$filter','DashBoardService',SummaryController
        ]);

    function SummaryController($scope,$timeout,$filter,DashBoardService) {
        $scope.region = "%";
        $scope.filterChange = function (filter) {
            $scope.filter = filter;
            if($scope.loadReady) {
                setDate($scope.filter);
                loadData();
                console.log('called');
            }
        };
        $scope.regionChange = function () {
            console.log($scope.region);
            loadData();
        };

        $scope.searchByDate =function () {
            $scope.startDate = $filter('date')($scope.fStartDate, "yyyy-MM-dd");
            $scope.endDate = $filter('date')($scope.fEndDate, "yyyy-MM-dd");
            loadData();

        };

        DashBoardService.getRegions(function (data) {
            $scope.regions = data.regions;
        });
        var loadData = function () {
            loadTAT();
            loadTrendData();
            loadByAge();
            loadTestByGender();
            loadTestByResult();
            loadTestByJustification();
            loadByRegionData();
        };

        var setDate = function (filter) {
            if(filter.month ==0){
                $scope.startDate = filter.year +'-'+'01'+'-'+'01';
                $scope.endDate = filter.year +'-'+'12'+'-'+'30';
            }
            else{
                var m =(filter.month <=9)?('0'+filter.month):filter.month;
                $scope.startDate = filter.year +'-'+m+'-'+'01';
                $scope.endDate = filter.year +'-'+m+'-'+'31';
            }
            $scope.yearStartDate = filter.year +'-'+'01'+'-'+'02';
            $scope.yearEndDate = filter.year +'-'+'12'+'-'+'30';


        };

        var loadTAT = function () {
            $scope.tatIsLoading = true;
            DashBoardService.getTAT({startDate: $scope.startDate,endDate:$scope.endDate,region:$scope.region},function (data) {
                $scope.tat = data.tat;
                $scope.tat = $.map(data.tat, function(value, index) {
                    var x ={};
                    x[index]=value;
                    return [x];
                });

                setTat();
                $scope.tatIsLoading = false;
            });
        };
        var setTat =function () {
            $scope.tatDataSource.pointers.pointer = [];
            $scope.tatDataSource.colorRange.color = [];
            $scope.tatDataSource.chart.upperLimit = undefined;
            var min =0;
            var max =0;
            var colors =[];
            var codes = ['#0075C2','#1AAF5D','#33BDDA'];
            var i = 0;
            angular.forEach($scope.tat, function (value,key) {
                if(key != $scope.tat.length -1) {
                    var color = {};
                    var labelName = Object.keys(value)[0];
                    color.label =labelName + ' ('+value[labelName]+'days)';
                    max =min+ value[labelName];
                    color.minValue = min;
                    color.maxValue = max;
                    color.code = codes[i];
                    i++;
                    colors.push(color);
                    min = angular.copy(max);
                }
            });
            var last =$scope.tat[$scope.tat.length -1];
            if(last) {
                var lastKey = Object.keys(last)[0];
                var lastValue = last[lastKey];
                var pointerColor = (lastValue > 14) ? "#FF0000" : "#F2C500";
                var pointer = {value: lastValue, tooltext: "c-d: $value days", "bgColor": pointerColor};
                $scope.tatDataSource.pointers.pointer = [pointer];
                $scope.tatDataSource.colorRange.color = colors;
                $scope.tatDataSource.chart.upperLimit = max;
            }
            $scope.tatDataSource.caption = (($scope.region =="%")?"National":$scope.region)+
                " Turn Around Time ("+ $filter('date')($scope.startDate, "dd/MM/yyyy") +"-"+$filter('date')($scope.endDate, "dd/MM/yyyy")+")";


        };

        var loadTrendData = function () {
            $scope.trendIsLoading = true;

            DashBoardService.getTrendData({startDate: $scope.yearStartDate,endDate:$scope.yearEndDate,region:$scope.region},function (data) {
                $scope.testTrends = data.testTrends;
                console.log("****************");
                console.log(data);
                setTrendData();
                $scope.trendIsLoading = false;

            });
        };
        var setTrendData =function () {
            $scope.testTrendsDataSource.categories=[];
            $scope.testTrendsDataSource.dataset=[];
            var category =[];
            var _byMonth = _.groupBy( $scope.testTrends,function (i) {
                return i.mon+'-'+i.year;
            });

            var byMonth = $.map(_byMonth, function(value, index) {
                return [{"month":index,"results":value}];
            });
            var _specimen = _.groupBy( $scope.testTrends,function (i) {
                return i.specimen;
            });

            var specimen = $.map(_specimen, function(value, index) {
                return [{"seriesname":index,"data":[]}];
            });

            angular.forEach(byMonth,function (value1) {
                var c = {label:(value1.month)};
                category.push(c);
                angular.forEach(specimen,function (value2) {
                    var total = 0;
                    var x = _.filter($scope.testTrends,function (d) {
                         var m = d.mon+'-'+d.year;
                        return m === value1.month && d.specimen === value2.seriesname;
                    });
                    if(x !== undefined && x.length >=1) {
                        total = x[0].total;
                    }
                    value2.data.push({value:total});
                });
            });
            $scope.testTrendsDataSource.categories.push({category:category});
            $scope.testTrendsDataSource.dataset=specimen;
            $scope.testTrendsDataSource.caption = (($scope.region =="%")?"National":$scope.region)+
                                                   " Test Trend ("+ $filter('date')($scope.yearStartDate, "dd/MM/yyyy") +"-"+$filter('date')($scope.yearEndDate, "dd/MM/yyyy")+")";

        };

        var loadByAge = function () {
            $scope.byAgeIsLoading = true;
            DashBoardService.getByAge({startDate: $scope.startDate,endDate:$scope.endDate,region:$scope.region},function (data) {
                $scope.testByAge = data.testByAge;
                setByAgeData();
                $scope.byAgeIsLoading = false;
            },function (error) {
                $scope.byAgeIsLoading = false;
            });
        };
        var setByAgeData = function () {
            $scope.byAgeDataSource.categories=[];
            $scope.byAgeDataSource.dataset=[];
            var category =[];
            var _byAge = _.groupBy( $scope.testByAge,function (i) {
                return i.age;
            });

            var byAge = $.map(_byAge, function(value, index) {
                return [{"age":index,"results":value}];
            });
            var _byResultType = _.groupBy( $scope.testByAge,function (i) {
                return i.resulttype;
            });

            var byResultType = $.map(_byResultType, function(value, index) {
                return [{"seriesname":index,"data":[]}];
            });
            angular.forEach(byAge,function (value1) {
                var c = {label:(value1.age)};
                category.push(c);
                angular.forEach(byResultType,function (value2) {
                    var total = 0;
                    var x = _.filter($scope.testByAge,function (d) {
                        return d.age == value1.age && d.resulttype == value2.seriesname;
                    });
                    if(x !== undefined && x.length >=1) {
                        total = x[0].total;
                    }
                    value2.data.push({value:total});
                });
            });

            $scope.byAgeDataSource.categories.push({category:category});
            $scope.byAgeDataSource.dataset=byResultType;
            $scope.byAgeDataSource.caption = (($scope.region =="%")?"National":$scope.region)+
                " Test by Age ("+ $filter('date')($scope.startDate, "dd/MM/yyyy") +"-"+$filter('date')($scope.endDate, "dd/MM/yyyy")+")";

        };

        var loadTestByGender = function () {
            $scope.byGenderIsLoading = true;
            DashBoardService.getByGender({startDate: $scope.startDate,endDate:$scope.endDate,region:$scope.region},function (data) {
                $scope.testByGender = data.testByGender;
                setByGenderData();
                $scope.byGenderIsLoading = false;
            });
        };
        var setByGenderData =function () {
            $scope.byGenderDataSource.categories=[];
            $scope.byGenderDataSource.dataset = [];
            var category =[];
            var _byGender = _.groupBy( $scope.testByGender,function (i) {
                return i.gender;
            });

            var byGender = $.map(_byGender, function(value, index) {
                return [{"gender":index,"results":value}];
            });
            var _byResultType = _.groupBy( $scope.testByGender,function (i) {
                return i.resulttype;
            });

            var byResultType = $.map(_byResultType, function(value, index) {
                return [{"seriesname":index,"data":[]}];
            });
            angular.forEach(byGender,function (value1) {
                var c = {label:(value1.gender)};
                category.push(c);
                angular.forEach(byResultType,function (value2) {
                    var total = 0;
                    var x = _.filter($scope.testByGender,function (d) {
                        return d.gender == value1.gender && d.resulttype == value2.seriesname;
                    });
                    if(x !== undefined && x.length >=1) {
                        total = x[0].total;
                    }
                    value2.data.push({value:total});
                });
            });

            $scope.byGenderDataSource.categories.push({category:category});
            $scope.byGenderDataSource.dataset = byResultType;
            $scope.byGenderDataSource.caption = (($scope.region =="%")?"National":$scope.region)+
                " Test by Gender ("+ $filter('date')($scope.startDate, "dd/MM/yyyy") +"-"+$filter('date')($scope.endDate, "dd/MM/yyyy")+")";
        };

        var loadTestByResult = function () {
            $scope.byResultIsLoading = true;
            DashBoardService.getByResult({startDate: $scope.startDate,endDate:$scope.endDate,region:$scope.region},function (data) {
                $scope.testByResult = data.byResult;
                setByResult();
                $scope.byResultIsLoading = false;
            });
        };
        var setByResult = function () {
            var data =[];
            angular.forEach($scope.testByResult, function (value) {
                var d ={};
                d.label = value.resulttype;
                d.value = value.total;
                data.push(d);
            });
            $scope.vlOutComesDataSource.data = data;
            $scope.vlOutComesDataSource.caption = (($scope.region =="%")?"National":$scope.region)+
                " Test OutCome ("+ $filter('date')($scope.startDate, "dd/MM/yyyy") +"-"+$filter('date')($scope.endDate, "dd/MM/yyyy")+")";

        };

        var loadTestByJustification = function () {
            $scope.byJustificationIsLoading = true;
            DashBoardService.getByJustification({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                // $scope.testByJustification = [
                //     {justification:"Pregnant Mothers", total:2000},
                //     {justification:"Baseline", total:1000},
                //     {justification:"Breast feeding mothers", total:1000},
                //     {justification:"Routine VL", total:1000}
                // ];
                $scope.testByJustification = [];
                setByJustification();
                $scope.byJustificationIsLoading = false;
            });
        };
        var setByJustification = function () {
            var data =[];
            angular.forEach($scope.testByJustification, function (value) {
                var d ={};
                d.label = value.justification;
                d.value = value.total;
                data.push(d);
            });
            $scope.justficationForTestDataSource.data = data;
            $scope.justficationForTestDataSource.caption = (($scope.region =="%")?"National":$scope.region)+
                " Justification for test ("+ $filter('date')($scope.startDate, "dd/MM/yyyy") +"-"+$filter('date')($scope.endDate, "dd/MM/yyyy")+")";

        };

        var loadByRegionData =function () {
            $scope.byRegionIsLoading = true;
            DashBoardService.getByRegion({startDate: $scope.startDate,endDate:$scope.endDate,region:$scope.region},function (data) {
                $scope.byRegion = data.byRegion;
                setByRegionData();
                $scope.byRegionIsLoading = false;

            });
        };
        // var setByRegionData =function () {
        //     var category =[];
        //     var _byRegion = _.groupBy( $scope.byRegion,function (i) {
        //         return i.region;
        //     });
        //
        //     var byRegion = $.map(_byRegion, function(value, index) {
        //         return [{"region":index,"results":value}];
        //     });
        //
        //     var _resultType = _.groupBy( $scope.byRegion,function (i) {
        //         return i.resulttype;
        //     });
        //
        //     var resultType = $.map(_resultType, function(value, index) {
        //         if(index == 'suppression') {
        //             return [{"seriesname": index,"parentyaxis": "S", "renderAs": "Line", "data": []}];
        //         }else{
        //             return [{"seriesname": index, "data": []}];
        //         }
        //     });
        //
        //     angular.forEach(byRegion,function (value1) {
        //         var c = {label:(value1.region)};
        //         category.push(c);
        //         angular.forEach(resultType,function (value2) {
        //             var total = 0;
        //             var x = _.filter($scope.byRegion,function (d) {
        //                 return d.region === value1.region && d.resulttype === value2.seriesname;
        //             });
        //             if(x !== undefined && x.length >=1) {
        //                 total = x[0].total;
        //             }
        //             value2.data.push({value:total});
        //         });
        //     });
        //
        //     $scope.byRegionDataSource.categories.push({category:category});
        //     $scope.byRegionDataSource.dataset=resultType;
        //
        // };
        var setByRegionData =function () {
            $scope.byRegionDataSource.lineset = [];
            $scope.byRegionDataSource.dataset={dataset:[]};
            $scope.byRegionDataSource.categories=[];
            var category =[];
            var _byRegion = _.groupBy( $scope.byRegion,function (i) {
                return i.region;
            });

            var byRegion = $.map(_byRegion, function(value, index) {
                return [{"region":index,"results":value}];
            });

            var _resultType = _.groupBy( $scope.byRegion,function (i) {
                return i.resulttype;
            });
            var lineseries =[];
            var resultType = $.map(_resultType, function(value, index) {
                    return [{"seriesname": index, "data": []}];
            });
            var series ={seriesname: 'suppression',showValues:0,data:[]};
            lineseries.push(series);
            angular.forEach(byRegion,function (value1) {
                var c = {label:(value1.region)};
                category.push(c);
                angular.forEach(lineseries,function (value2) {
                    var total = 0;
                    var suppressed = _.filter($scope.byRegion,function (d) {
                        return d.region === value1.region && d.resulttype === 'suppressed';
                    });
                    var not_suppressed = _.filter($scope.byRegion,function (d) {
                        return d.region === value1.region && d.resulttype === 'not suppressed';
                    });
                    var t =suppressed[0].total + not_suppressed[0].total;
                    if(t !== 0) {
                        total = (suppressed[0].total/t)*100;
                    }
                    value2.data.push({value:total});
                });
            });

            angular.forEach(byRegion,function (value1) {
                    angular.forEach(resultType,function (value2) {
                        var total = 0;
                        var x = _.filter($scope.byRegion,function (d) {
                            return d.region === value1.region && d.resulttype === value2.seriesname;
                        });
                        if(x !== undefined && x.length >=1) {
                            total = x[0].total;
                        }
                        value2.data.push({value:total});
                    });
                });
                $scope.byRegionDataSource.lineset = lineseries;
                $scope.byRegionDataSource.dataset={dataset:resultType};
                $scope.byRegionDataSource.categories.push({category:category});
                $scope.byRegionDataSource.caption = " Test by Region ("+ $filter('date')($scope.startDate, "dd/MM/yyyy") +"-"+$filter('date')($scope.endDate, "dd/MM/yyyy")+")";
        };

        $timeout(function () {
            $scope.loadReady = true;
            setDate($scope.filter);
            loadData();
        },1000);


        $scope.tatDataSource ={
            "chart": {
                "theme": "fint",
                "numberSuffix": "days",
                "chartBottomMargin": "10",
                "valueFontSize": "11",
                "baseFont": "Inherit",
                "valueFontBold": "0",
                "gaugeFillMix": "{light-10},{light-70},{dark-10}",
                "gaugeFillRatio": "40,20,40"
            },
            "colorRange": {
                "color": [
                ]
            },
            "pointers": {
                "pointer": [
                    {
                        "value": "200"
                    }
                ]
            }
        };

        $scope.testTrendsDataSource = {
            "chart": {
                "xAxisname": "Months",
                "yAxisName": "Tests",
                "showSum": "1",
                "formatNumberScale":0,
                "yFormatNumberScale":0,
                "xFormatNumberScale":0,
                "numberPrefix": "",
                "labelDisplay":"rotate",
                "slantLabels": "1",
                "theme": "fint",
                "baseFont": "Inherit",
                "exportEnabled": "1",
                "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                "exportTargetWindow": "_self",
            },
            "categories": [
            ],
            "dataset": [

            ]
        };

        $scope.byAgeDataSource = {
            "chart": {
                "xAxisname": "Months",
                "yAxisName": "Tests",
                "formatNumberScale":0,
                "yFormatNumberScale":0,
                "xFormatNumberScale":0,
                "showSum": "1",
                "numberPrefix": "",
                "theme": "fint",
                "baseFont": "Inherit",
                "exportEnabled": "1",
                "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                "exportTargetWindow": "_self",
            },
            "categories": [

            ],
            "dataset": [

            ]
        };

        $scope.byGenderDataSource = {
            "chart": {
                "xAxisname": "Months",
                "yAxisName": "Tests",
                "formatNumberScale":0,
                "yFormatNumberScale":0,
                "xFormatNumberScale":0,
                "showSum": "1",
                "numberPrefix": "",
                "theme": "fint",
                "baseFont": "Inherit",
                "exportEnabled": "1",
                "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                "exportTargetWindow": "_self",
            },
            "categories": [

            ],
            "dataset": [

            ]
        };

        $scope.vlOutComesDataSource={
            "chart": {
                "numberPrefix": "",
                "formatNumberScale":0,
                "yFormatNumberScale":0,
                "xFormatNumberScale":0,
                "showPercentInTooltip": "0",
                "decimals": "1",
                "useDataPlotColorForLabels": "1",
                "theme": "fint",
                "baseFont": "Inherit",
                "exportEnabled": "1",
                "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                "exportTargetWindow": "_self",
            },
            "data": []
        };

        $scope.justficationForTestDataSource = {
            "chart": {
                "numberPrefix": "",
                "showPercentInTooltip": "0",
                "formatNumberScale":0,
                "yFormatNumberScale":0,
                "xFormatNumberScale":0,
                "decimals": "1",
                "useDataPlotColorForLabels": "1",
                "theme": "fint",
                "baseFont": "Inherit",
                "exportEnabled": "1",
                "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                "exportTargetWindow": "_self",
            },
            "data": [
            ]
        };

        $scope.byRegionDataSource = {
            "chart": {
                "xAxisname": "Region",
                "pYAxisName": "Tests",
                "sYAxisName": "Suppression %",
                "sNumberSuffix": "%",
                "showSum": "1",
                "formatNumberScale":0,
                "yFormatNumberScale":0,
                "xFormatNumberScale":0,
                "labelDisplay":"rotate",
                "slantLabels": "1",
                "numberPrefix": "",
                "theme": "fint",
                "baseFont": "Inherit",
                "exportEnabled": "1",
                "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                "exportTargetWindow": "_self",
            },
            "categories": [
            ],
            "dataset": [

            ]
        };



    }

})();