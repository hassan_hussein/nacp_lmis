    'use strict';

var app  =    angular.module('nacpDashboard', ['ngSanitize', 'ng-fusioncharts', 'ui.router','DashBoardService']);

        app.config(function ($stateProvider, $urlRouterProvider
                          ) {
            $stateProvider
                .state('vl', {
                    url: '',
                    templateUrl: '/views/main.html',
                    controller: 'MainController',
                    abstract: true
                })
                .state('vl.summary', {
                    url: '/home',
                    templateUrl: '/public/pages/lab-dashboard/views/summary.html',
                    controller: 'SummaryController',
                    data: {
                        title: 'home'
                    }
                }).state('vl.trend', {
                    url: '/trend',
                    templateUrl: '/public/pages/lab-dashboard/views/trend.html',
                    controller: 'TrendController',
                    data: {
                        title: 'Trend'
                    }
                }).state('vl.lab', {
                    url: '/lab',
                    templateUrl: '/public/pages/lab-dashboard/views/lab.html',
                    controller: 'LabPerformanceController',
                    data: {
                        title: 'Lab Performance'
                    }
                });

            $urlRouterProvider.otherwise('/home');
        });