
var dashServices  =    angular.module('DashBoardService', ['ngResource']);
dashServices.factory('GetTAT', function ($resource) {
    return $resource('/main-dashboard/getTAT', {}, {});
});

dashServices.factory('GetSampleTypesByYear', function ($resource) {
    return $resource('/main-dashboard/getSampleTypesByYear/:year', {}, {});
});

dashServices.factory('DashBoardService', function ($resource) {
    return $resource('', {}, {
        getTAT:{url:'/main-dashboard/getTAT',method:'GET'},
        getTrendData:{url:'/main-dashboard/getTrendData',method:'GET'},
        getByGender:{url:'/main-dashboard/getByGender',method:'GET'},
        getByAge:{url:'/main-dashboard/getByAge',method:'GET'},
        getByResult:{url:'/main-dashboard/getByResult',method:'GET'},
        getByJustification:{url:'/main-dashboard/getByJustification',method:'GET'},
        getByRegion:{url:'/main-dashboard/getByRegion',method:'GET'},
        getRegions:{url:'/main-dashboard/getRegions',method:'GET'}
    });
});