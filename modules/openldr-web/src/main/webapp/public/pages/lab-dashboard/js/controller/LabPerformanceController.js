/**
 * Created by user on 8/10/2017.
 */
(function(){
'use strict';

    angular
        .module('DashBoardService')
        .controller('LabPerformanceController', ['$scope','$timeout','DashBoardService',LabPerformanceController
        ]);

    function LabPerformanceController($scope,$timeout,DashBoardService) {

        $scope.filterChange = function (filter) {
            $scope.filter = filter;
            if($scope.loadReady) {

                loadData();
            }
        };
        var loadData = function () {
            setDate($scope.filter);
            loadTAT();
            loadTrendData();
            loadByAge();
            loadTestByGender();
            loadTestByResult();
            loadTestByJustification();
        };

        var setDate = function (filter) {
            $scope.startDate = filter.year +'-'+'01'+'-'+'01';
            var m =(filter.month <=9)?('0'+filter.month):filter.month;
            $scope.endDate = filter.year +'-'+m+'-'+'31';
        };

        var loadTAT = function () {
            $scope.tatIsLoading = true;
            DashBoardService.getTAT({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                $scope.tat = [
                           {C_R:2},
                           {R_P:4},
                           {P_D:6},
                           {C_D:8}
                    ];
                setTat();
                $scope.tatIsLoading = false;
            });
        };
        var setTat =function () {
            var min =0;
            var max =0;
            var colors =[];
            angular.forEach($scope.tat, function (value,key) {
                if(key != $scope.tat.length -1) {
                    var color = {};
                    color.label = Object.keys(value)[0];
                    max = value[color.label];

                    color.minValue = min;
                    color.maxValue = max;

                    colors.push(color);

                    min = max;
                }
            });
            var last =$scope.tat[$scope.tat.length -1];
            var lastKey =Object.keys(last)[0];
            var lastValue = last[lastKey];
            console.log(lastValue);
            $scope.tatDataSource.colorRange.color = colors;
            $scope.tatDataSource.chart.upperLimit = max;
            $scope.tatDataSource.pointers.pointer = lastValue;



        };

        var loadTrendData =function () {
            $scope.trendIsLoading = true;
            DashBoardService.getTrendData({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                $scope.testTrends = [
                    {year:'2017', month:'Jan','specimen':'Whole Blood',total:"2000"},
                    {year:'2017', month:'Jan','specimen':'DBS',total:"1000"},
                    {year:'2017', month:'Feb','specimen':'Plasma',total:"1500"}
                ];
                setTrendData();
                $scope.trendIsLoading = false;

            });
        };
        var setTrendData =function () {
            var category =[];
            var _byMonth = _.groupBy( $scope.testTrends,function (i) {
                return i.month;
            });

            var byMonth = $.map(_byMonth, function(value, index) {
                return [{"month":index,"results":value}];
            });
            var _specimen = _.groupBy( $scope.testTrends,function (i) {
                return i.specimen;
            });

            var specimen = $.map(_specimen, function(value, index) {
                return [{"seriesname":index,"data":[]}];
            });

            angular.forEach(byMonth,function (value1) {
                var c = {label:(value1.month +'-')};
                category.push(c);
                angular.forEach(specimen,function (value2) {
                    var total = 0;
                    var x = _.filter($scope.testTrends,function (d) {
                        return d.month === value1.month && d.specimen === value2.seriesname;
                    });
                    if(x !== undefined && x.length >=1) {
                        total = x[0].total;
                    }
                    value2.data.push({value:total});
                });
            });

            $scope.testTrendsDataSource.categories.push({category:category});
            $scope.testTrendsDataSource.dataset=specimen;

        };

        var loadByAge = function () {
            $scope.byAgeIsLoading = true;
            DashBoardService.getByAge({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                $scope.testByAge = [
                                      {age:'less 1 year', resultType:"suppressed" ,total:"2000"},
                                      {age:'less 1 year', resultType:"not suppressed" ,total:"1000"},
                                      {age:'1-4 years', resultType:"suppressed" ,total:"3000"},
                                      {age:'1-4 years', resultType:"not suppressed" ,total:"2000"}
                                   ];
                setByAgeData();
                $scope.byAgeIsLoading = false;
            });
        };
        var setByAgeData = function () {
            var category =[];
            var _byAge = _.groupBy( $scope.testByAge,function (i) {
                return i.age;
            });

            var byAge = $.map(_byAge, function(value, index) {
                return [{"age":index,"results":value}];
            });
            var _byResultType = _.groupBy( $scope.testByAge,function (i) {
                return i.resultType;
            });

            var byResultType = $.map(_byResultType, function(value, index) {
                return [{"seriesname":index,"data":[]}];
            });
            angular.forEach(byAge,function (value1) {
                var c = {label:(value1.age)};
                category.push(c);
                angular.forEach(byResultType,function (value2) {
                    var total = 0;
                    var x = _.filter($scope.testByAge,function (d) {
                        return d.age == value1.age && d.resultType == value2.seriesname;
                    });
                    if(x !== undefined && x.length >=1) {
                        total = x[0].total;
                    }
                    value2.data.push({value:total});
                });
            });

            $scope.byAgeDataSource.categories.push({category:category});
            $scope.byAgeDataSource.dataset=byResultType;
        };

        var loadTestByGender = function () {
            $scope.byGenderIsLoading = true;
            DashBoardService.getByGender({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                $scope.testByGender = [
                    {gender:'Female', resultType:"suppressed" ,total:"2000"},
                    {gender:'Female', resultType:"not suppressed" ,total:"1000"},
                    {gender:'Male', resultType:"suppressed" ,total:"3000"},
                    {gender:'Male', resultType:"not suppressed" ,total:"2000"},
                    {gender:'No Data', resultType:"suppressed" ,total:"400"},
                    {gender:'No Data', resultType:"not suppressed" ,total:"200"}
                ];
                setByGenderData();
                $scope.byGenderIsLoading = false;
            });
        };
        var setByGenderData =function () {
            var category =[];
            var _byGender = _.groupBy( $scope.testByGender,function (i) {
                return i.gender;
            });

            var byGender = $.map(_byGender, function(value, index) {
                return [{"gender":index,"results":value}];
            });
            var _byResultType = _.groupBy( $scope.testByGender,function (i) {
                return i.resultType;
            });

            var byResultType = $.map(_byResultType, function(value, index) {
                return [{"seriesname":index,"data":[]}];
            });
            angular.forEach(byGender,function (value1) {
                var c = {label:(value1.gender)};
                category.push(c);
                angular.forEach(byResultType,function (value2) {
                    var total = 0;
                    var x = _.filter($scope.testByGender,function (d) {
                        return d.gender == value1.gender && d.resultType == value2.seriesname;
                    });
                    if(x !== undefined && x.length >=1) {
                        total = x[0].total;
                    }
                    value2.data.push({value:total});
                });
            });

            $scope.byGenderDataSource.categories.push({category:category});
            $scope.byGenderDataSource.dataset = byResultType;
        };

        var loadTestByResult = function () {
            $scope.byResultIsLoading = true;
            DashBoardService.getByResult({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                $scope.testByResult = [
                    {resultType:"suppressed", total:2000},
                    {resultType:"not suppressed", total:1000}
                    ];
                setByResult();
                $scope.byResultIsLoading = false;
            });
        };
        var setByResult = function () {
            var data =[];
            angular.forEach($scope.testByResult, function (value) {
                var d ={};
                d.label = value.resultType;
                d.value = value.total;
                data.push(d);
            });
            $scope.vlOutComesDataSource.data = data;

        };

        var loadTestByJustification = function () {
            $scope.byJustificationIsLoading = true;
            DashBoardService.getByJustification({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                $scope.testByJustification = [
                    {justification:"Pregnant Mothers", total:2000},
                    {justification:"Baseline", total:1000},
                    {justification:"Breast feeding mothers", total:1000},
                    {justification:"Routine VL", total:1000}
                ];
                setByJustification();
                $scope.byJustificationIsLoading = false;
            });
        };
        var setByJustification = function () {
            var data =[];
            angular.forEach($scope.testByJustification, function (value) {
                var d ={};
                d.label = value.justification;
                d.value = value.total;
                data.push(d);
            });
            $scope.justficationForTestDataSource.data = data;

        };

        var loadByRegionData =function () {
            $scope.byRegionIsLoading = true;
            DashBoardService.getByRegionData({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                $scope.byRegion = [
                    {region:'Arusha',resultType:'Suppressed',total:"2000"},
                    {region:'Arusha',resultType:'Not suppressed',total:"200"},
                    {region:'Arusha',resultType:'Suppression',total:"100"},
                    {region:'Dodoma',resultType:'Suppressed',total:"2000"},
                    {region:'Dodoma',resultType:'Not suppressed',total:"200"},
                    {region:'Dodoma',resultType:'Suppression',total:"100"},
                    {region:'Mwanza',resultType:'Suppressed',total:"2000"},
                    {region:'Mwanza',resultType:'Not suppressed',total:"200"},
                    {region:'Mwanza',resultType:'Suppression',total:"100"},


                ];
                setByRegionData();
                $scope.trendIsLoading = false;

            });
        };
        var setByRegionData =function () {
            var category =[];
            var _byRegion = _.groupBy( $scope.byRegion,function (i) {
                return i.region;
            });

            var byRegion = $.map(_byRegion, function(value, index) {
                return [{"region":index,"results":value}];
            });

            var _resultType = _.groupBy( $scope.byRegion,function (i) {
                return i.resultType;
            });

            var resultType = $.map(_resultType, function(value, index) {
                return [{"seriesname":index,"data":[]}];
            });

            angular.forEach(byRegion,function (value1) {
                var c = {label:(value1.region)};
                category.push(c);
                angular.forEach(resultType,function (value2) {
                    var total = 0;
                    var x = _.filter($scope.byRegion,function (d) {
                        return d.region === value1.region && d.resultType === value2.seriesname;
                    });
                    if(x !== undefined && x.length >=1) {
                        total = x[0].total;
                    }
                    value2.data.push({value:total});
                });
            });

            $scope.byRegionDataSource.categories.push({category:category});
            $scope.byRegionDataSource.dataset=resultType;

        };

        $timeout(function () {
            $scope.loadReady = true;
            loadData();
        },1000);


        $scope.tatDataSource ={
            "chart": {
                "theme": "ocean",
                "caption": "National Turn Around Time",
                "lowerLimit": "0",
                "numberSuffix": "days",
                "chartBottomMargin": "10",
                "valueFontSize": "11",
                "valueFontBold": "0"
            },
            "colorRange": {
                "color": [
                ]
            },
            "pointers": {
                "pointer": [
                    {
                        "value": "22"
                    }
                ]
            },
            "trendPoints": {
                "point": [
                    {
                        "startValue": "0",
                        "displayValue": " ",
                        "dashed": "1",
                        "showValues": "0"
                    },
                    {
                        "startValue": "0",
                        "displayValue": " ",
                        "dashed": "1",
                        "showValues": "0"
                    },
                    {
                        "startValue": "0",
                        "endValue": "0",
                        "displayValue": " ",
                        "alpha": "40"
                    }
                ]
            },
            "annotations": {
                "origw": "400",
                "origh": "190",
                "autoscale": "1",
                "groups": [
                    {
                        "id": "range",
                        "items": [
                            {
                                "id": "rangeBg",
                                "type": "rectangle",
                                "x": "$chartCenterX-115",
                                "y": "$chartEndY-35",
                                "tox": "$chartCenterX +115",
                                "toy": "$chartEndY-15",
                                "fillcolor": "#0075c2"
                            },
                            {
                                "id": "rangeText",
                                "type": "Text",
                                "fontSize": "11",
                                "fillcolor": "#ffffff",
                                "x": "$chartCenterX",
                                "y": "$chartEndY-25"
                            }
                        ]
                    }
                ]
            }
        };

        $scope.testTrendsDataSource = {
            "chart": {
                "caption": "Test Trends",
                "subCaption": "2017",
                "xAxisname": "Months",
                "yAxisName": "Tests",
                "showSum": "1",
                "numberPrefix": "",
                "theme": "ocean"
            },
            "categories": [
            ],
            "dataset": [

            ]
        };

        $scope.byAgeDataSource = {
            "chart": {
                "caption": "Age",
                "subCaption": "2017",
                "xAxisname": "Months",
                "yAxisName": "Tests",
                "showSum": "1",
                "numberPrefix": "",
                "theme": "ocean"
            },
            "categories": [

            ],
            "dataset": [

            ]
        };

        $scope.byGenderDataSource = {
            "chart": {
                "caption": "Gender",
                "subCaption": "2017",
                "xAxisname": "Months",
                "yAxisName": "Tests",
                "showSum": "1",
                "numberPrefix": "",
                "theme": "ocean"
            },
            "categories": [

            ],
            "dataset": [

            ]
        };

        $scope.vlOutComesDataSource={
            "chart": {
                "caption": "VL OutComes",
                "subCaption": "2017",
                "numberPrefix": "",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "useDataPlotColorForLabels": "1",
                "theme": "ocean"
            },
            "data": []
        };

        $scope.justficationForTestDataSource = {
            "chart": {
                "caption": "Justification for test",
                "subCaption": "2017",
                "numberPrefix": "",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "useDataPlotColorForLabels": "1",
                "theme": "ocean"
            },
            "data": [
            ]
        };

        $scope.byRegionDataSource = {
            "chart": {
                "caption": "Region Outcomes",
                "subCaption": "2017",
                "xAxisname": "Region",
                "yAxisName": "Tests",
                "showSum": "1",
                "numberPrefix": "",
                "theme": "ocean"
            },
            "categories": [
            ],
            "dataset": [

            ]
        };


    }

})();