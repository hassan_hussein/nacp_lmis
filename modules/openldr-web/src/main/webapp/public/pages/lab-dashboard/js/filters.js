app.directive('mapFilter', ['MapData',
    function (MapData) {
        return {
            restrict: 'E',
            scope: {
                region: '=cmModel',
                onChange: '&'
            },
            controller: function ($scope) {

                function drawMap(){
                    var chart = new Highcharts.Map({
                        chart: {
                                 renderTo: 'container',
                                 backgroundColor:"#EEF1F5"
                        },
                        mapNavigation: {
                             enabled: true,
                             buttonOptions: {
                                   verticalAlign: 'bottom'
                             }
                            },

                            colorAxis: {
                                   dataClasses: [{color:'#083D78',from:'bugando',name:'Bugando',to:'bugando'},
                                                 {color:'#61B861',from:'kcmc',name:'KCMC',to:'kcmc'},
                                                 {color:'#FD7E0E',from:'muhimbili',name:'Muhimbili',to:'muhimbili'},
                                                 {color:'#7A91B1',from:'mbeya',name:'Mbeya RH',to:'mbeya'},
                                                 {color:'#17888f',from:'temeke',name:'Temeke',to:'temeke'},
                                                 {color:'#6EBFD8',from:'mtmeru',name:'Mount Meru',to:'mtmeru'},
                                                 {color:'purple',from:'iringa',name:'Iringa',to:'iringa'},
                                                 {color:'pink',from:'morogoro',name:'Morogoro',to:'morogoro'},
                                                 {color:'silver',from:'ligula',name:'Ligula',to:'ligula'},
                                                 {color:'red',from:'songea',name:'Songea',to:'songea'}
                                                 ]
                            },
                            title : {
                                        text : ''
                            },


                            series : [{
                                     data : $scope.$parent.mapData,
                                     mapData: Highcharts.maps['countries/tz/tz-all'],
                                     joinBy: 'hc-key',
                                     name: 'Test',
                                     states: {
                                           hover: {
                                           color: '#BADA55'
                                    }

                                  },
                                  tooltip: {
                                           pointFormat: 'Region Name:<br> {point.name}</br> <br />Tests Done:<b>{point.tests} </b><br /> Rejected:<b>{point.rejected} </b><br /> Number of Sites:<b>{point.sites}</b>',
                                           hideDelay: 20000
                                        },
                                    dataLabels: {
                                             enabled: true,
                                             format: '{point.name}'
                                         }
                                     }],
                           plotOptions:{

                              series:{
                                cursor: 'pointer',
                                 point:{

                                  events:{
                                  click: function(){
                                             $scope.region=this;
                                             $scope.$apply();
                                         }
                                      }
                                    }
                               }
                           }
                     });
                 }

                var loadData= function(){
                    MapData.query(function(data){
                         $scope.$parent.mapData =data;
                         drawMap();
                    });
                };

                var addTestResults=function(){
                    if($scope.$parent.mapData !== undefined)
                    {
                        $scope.$parent.mapData.forEach(function(d){
                            var regionTestsResults=_.findWhere($scope.$parent.testResultsByRegion,{region:d.region});
                            if(regionTestsResults !== undefined)
                            {
                              d.tests=regionTestsResults.tests.length;

                              //Rejected
                              var rejected=_.where(regionTestsResults.tests,{Rejected:'Yes'});
                              if(rejected !== undefined)
                                d.rejected=rejected.length;
                               else d.rejected=0;

                              //No of sites
                              var byFacility=_.groupBy(regionTestsResults.tests,function(r){
                                      return r.FacilityName;
                              });
                              var byFacilityArray = $.map(byFacility, function(value, index) {
                                    return [{"facility":index,"results":value}];
                              });

                              if(byFacilityArray !== undefined)
                                d.sites=byFacilityArray.length;
                              else
                                d.sites=0;

                            }
                            else{d.tests=0}


                        });
                        drawMap();
                    }
                };
                loadData();
                $scope.$watch('region', function (newValues, oldValues) {
                   $scope.onChange();
                });

                $scope.$parent.$watch('testResultsByRegion', function(value){
                      addTestResults();
                });
            },
            templateUrl: 'filter-map-template'
        };
    }
]);

app.directive('yearFilter', [
    function () {
        return {
            restrict: 'E',
            scope: {
                year: '=cmModel',
                range: '=range',
                default :'=default',
                onChange: '&'
            },
            controller: function ($scope) {
                var thisYear=new Date().getFullYear();
                var years=[thisYear];
                var range=($scope.range !== undefined)?parseInt($scope.range,10):5;
                for(i=1;i < range;i++){
                    years.push(thisYear-i);
                }

                $scope.years=years.reverse();
                $scope.$parent.yearToDisplay=$scope.years;

                $scope.setYear=function(year){
                   $scope.$parent.filter.period=undefined;
                   $scope.year=year;
                   $scope.$parent.init=false;
                };
                if($scope.default !==undefined){
                     $scope.year = $scope.default;
                }else{
                     $scope.year = new Date().getFullYear();
                }
                $scope.$watch('year', function (newValues, oldValues) {
                   $scope.onChange();
                });
            },
            templateUrl: 'filter-year-template'
        };
    }
]);

app.directive('monthFilter', [
    function () {
        return {
            restrict: 'E',
            scope: {
                month: '=cmModel',
                default :'=default',
                onChange: '&'
            },
            controller: function ($scope) {
                $scope.months=[{"name":"All", "id":0},{"name":"Jan", "id":1},{"name":"Feb", "id":2},{"name":"Mar", "id":3},
                               {"name":"Apr", "id":4},{"name":"May", "id":5},{"name":"Jun", "id":6},
                               {"name":"Jul", "id":7},{"name":"Aug", "id":8},{"name":"Sep", "id":9},
                               {"name":"Oct", "id":10},{"name":"Nov", "id":11},{"name":"Dec", "id":12}];
                $scope.$parent.monthsToDisplay=$scope.months;
                $scope.setMonth=function(month){
                   $scope.$parent.filter.period=undefined;
                   $scope.$parent.init=false;
                   $scope.month=month;
                };
                if($scope.default !==undefined){
                     $scope.month = $scope.default;
                }else{
                     $scope.month = new Date().getMonth() +1;
                }
                $scope.$watch('month', function (newValues, oldValues) {
                   $scope.onChange();
                });
            },
            templateUrl: 'filter-month-template'
        };
    }
]);

app.directive('periodFilter', [
    function () {
        return {
            restrict: 'E',
            scope: {
                period: '=cmModel',
                onChange: '&'
            },
            controller: function ($scope) {
                $scope.periods=[{"name":"Last 6 months", "id":6},{"name":"Last 3 months", "id":3},{"name":"All", "id":0}];
                $scope.setPeriod=function(period){
                   $scope.$parent.filter.year=undefined;
                   $scope.$parent.filter.month=undefined;
                   $scope.period=period;
                };

                $scope.$watch('period', function (newValues, oldValues) {
                   $scope.onChange();
                });
            },
            templateUrl: 'filter-period-template'
        };
    }
]);

app.directive('locationFilter', [
    function () {
        return {
            restrict: 'E',
            scope: {
                location: '=cmModel',
                onChange: '&',
                default: '=default'
            },
            controller: function ($scope) {
                $scope.locations=[{"name":"All","id":"all"},
                                  {"name":"Bugando","id":"bugando"},
                                  {"name":"KCMC","id":"kcmc"},
                                  {"name":"Temeke","id":"temeke"},
                                  {"name":"NHL-QATC","id":"nhl-qatc"},
                                  {"name":"Mbeya","id":"mbeya"}];
                $scope.setLocation=function(location){
                   $scope.location=location;
                   $scope.$parent.init=false;
                };
                if($scope.default !==undefined){
                    $scope.location = $scope.location;
                }else{
                    $scope.location = "all";
                }

                $scope.$watch('location', function (newValues, oldValues) {
                   $scope.onChange();
                });
            },
            templateUrl: 'filter-location-template'
        };
    }
]);
app.filter('ageFilter', function() {
     function calculateAge(birthday) { // birthday is a date
//     console.log(birthday);
       if(birthday !== undefined)
       {
         var ageDifMs = Date.now() - new Date(birthday).getTime();
         var ageDate = new Date(ageDifMs); // miliseconds from epoch
         return Math.abs(ageDate.getUTCFullYear() - 1970)+ ' years';
         }
         else{ return '';}
     }
     function monthDiff(d1, d2) {
       if (d1 < d2){
        var months = d2.getMonth() - d1.getMonth();
        return months <= 0 ? 0 : months;
       }
       return 0;
     }
     return function(birthdate) {
           var age = calculateAge(birthdate);
           if (age == 0)
             return monthDiff(birthdate, new Date()) + ' months';
           return age;
     };
});
