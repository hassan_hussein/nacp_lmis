
(function(){
    'use strict';
    
        angular
            .module('mainDashboard')
            .controller('LabPerformanceController', ['$scope','$timeout','$filter','LabDashboardService',LabPerformanceController
            ]);
    
        function LabPerformanceController($scope,$timeout,$filter, LabDashboardService) {
            $scope.lab = "%";
            $scope.defaultMonth =0;
            $scope.filterChange = function (filter) {
                $scope.fStartDate = undefined;
                $scope.fEndDate = undefined;
                $scope.filter = filter;
                if($scope.loadReady) {
                    setDate($scope.filter);
                    loadData();
                }
            };
    
            $scope.searchByDate =function () {
                $scope.$parent.month = undefined;
                $scope.$parent.year = undefined;
                $scope.startDate = $filter('date')(new Date($scope.fStartDate), 'yyyy-MM-dd');
                $scope.endDate = $filter('date')(new Date($scope.fEndDate), 'yyyy-MM-dd');
                $scope.selectedDate = "("+$filter('date')($scope.startDate, 'dd-MMM-yyyy')+" to "+$filter('date')($scope.endDate, 'dd-MMM-yyyy')+")";
                loadData();
            };

            $timeout(function () {
                $scope.loadReady = true;
                setDate($scope.filter);
                loadData();
            },1000);

            LabDashboardService.getLabs(function (data) {
                $scope.labs = data.labs;
            });

            $scope.labChange = function () {
                loadData();
            };
            var loadData = function () {
                setDate($scope.filter);
                loadTAT();
                loadTrendData();
                loadByAge();
                loadTestByGender();
                loadTestByResult();
                loadTestByJustification();
                loadLabsData();
            };

            function daysInMonth(month,year) {
                return new Date(year, month, 0).getDate();
            };

            var setDate = function (filter) {

                if(filter.month ==0){

                    $scope.startDate = filter.year +'-'+'01'+'-'+'01';
                    $scope.endDate = filter.year +'-'+'12'+'-'+daysInMonth(12,filter.year);
                    $scope.selectedDate = "("+filter.year+")";
                }
                else{
                    var m =(filter.month <=9)?('0'+filter.month):filter.month;
                    $scope.startDate = filter.year +'-'+m+'-'+'01';
                    $scope.endDate = filter.year +'-'+m+'-'+daysInMonth(m,filter.year);
                    $scope.selectedDate = "("+$filter('date')($scope.startDate, 'MMM-yyyy')+")";

                }
                $scope.yearStartDate = filter.year +'-'+'01'+'-'+'02';
                $scope.yearEndDate = filter.year +'-'+'12'+'-'+daysInMonth(12,filter.year);
                $scope.selectedYear = "("+filter.year+")";


            };

            var loadTAT = function () {
                $scope.tatIsLoading =true;
                LabDashboardService.getTAT({startDate:$scope.startDate, endDate:$scope.endDate,lab:$scope.lab},function(data){
                    $scope.tat = data.tat;
                    $scope.tat = $.map(data.tat, function(value, index){
                        var x = [];
                        x[index]=value;
                        return [x]
                    });

                    setTat();
                    $scope.tatIsLoading=false;
                });
            };

            var setTat= function () {
                $scope.pointerColor = '#333333';
                $scope.tatDataSource.pointers.pointer = [];
                $scope.tatDataSource.colorRange.color = [];
                $scope.tatDataSource.chart.upperLimit = 0;
                var min =0;
                var max =0;
                var colors =[];
                var codes = ['#01B8AA','#F2C80F', '#FD625E','#F2C80F'];
                var i = 0;
                angular.forEach($scope.tat, function (value,key) {
                    if(key != $scope.tat.length -1) {
                        var color = {};
                        var labelName = Object.keys(value)[0];
                        color.label =labelName + ' ('+value[labelName]+'days)';
                        max =min+ value[labelName];
                        color.minValue = min;
                        color.maxValue = max;
                        color.code = codes[i];
                        i++;
                        colors.push(color);
                        min = angular.copy(max);
                    }
                });
                var last =$scope.tat[$scope.tat.length -1];
                if(last) {
                    var lastKey = Object.keys(last)[0];
                    var lastValue = last[lastKey];
                    $scope.pointerColor = (lastValue > 14) ? "#FF0000" : "#333333";
                    var pointer = {value: lastValue, tooltext: "c-d: $value days", "bgColor":$scope.pointerColor};

                }
                $scope.tatDataSource.pointers.pointer = [pointer];
                $scope.tatDataSource.colorRange.color = colors;
                $scope.tatDataSource.chart.upperLimit = max;
                $scope.tatDataSource.caption = (($scope.lab =="%")?"All labs":$scope.lab)+
                    " Turn Around Time" + $scope.selectedDate;

            };

            var loadTrendData = function () {
              $scope.trendIsLoading = true;

              LabDashboardService.getTrendData({
                  startDate: $scope.yearStartDate,
                  endDate:$scope.yearEndDate,
                  lab:$scope.lab}, function(data){
                     $scope.testTrends = data.testTrends;
                     setTrendData();
                     $scope.trendIsLoading = false;
                  });
            };

            var setTrendData =function () {
                $scope.testTrendsDataSource.categories=[];
                $scope.testTrendsDataSource.dataset=[];
                var category =[];
                var _byMonth = _.groupBy( $scope.testTrends,function (i) {
                    return i.mon+'-'+i.year;
                });

                var byMonth = $.map(_byMonth, function(value, index) {
                    return [{"month":index,"results":value}];
                });
                var _specimen = _.groupBy( $scope.testTrends,function (i) {
                    return i.specimen;
                });

                var specimen = $.map(_specimen, function(value, index) {
                    return [{"seriesname":index,"data":[]}];
                });

                angular.forEach(byMonth,function (value1) {
                    var c = {label:(value1.month)};
                    category.push(c);
                    angular.forEach(specimen,function (value2) {
                        var total = 0;
                        var x = _.filter($scope.testTrends,function (d) {
                            var m = d.mon+'-'+d.year;
                            return m === value1.month && d.specimen === value2.seriesname;
                        });
                        if(x !== undefined && x.length >=1) {
                            total = x[0].total;
                        }
                        value2.data.push({value:total});
                    });
                });
                angular.forEach(specimen,function (spec) {
                    var sum = 0;
                    angular.forEach(spec.data,function (d) {
                        sum +=d.value;
                    });
                    spec.sum = -1*sum;
                });
                $scope.testTrendsDataSource.categories.push({category:category});
                $scope.testTrendsDataSource.dataset = _.sortBy(specimen,'sum');
                // console.log($scope.testTrendsDataSource.dataset);
                $scope.testTrendsDataSource.caption = (($scope.lab =="%")?"All Labs":$scope.lab)+ " Sample Registration Trend "+$scope.selectedYear;

            };

            var loadTestByGender = function () {
                $scope.byGenderIsLoading = true;
                LabDashboardService.getByGender({
                    startDate: $scope.startDate,
                    endDate:$scope.endDate,
                    lab:$scope.lab},function (data) {
                        $scope.testByGender = data.testByGender;
                        setByGenderData();
                        $scope.byGenderIsLoading = false;
                    });
            };


            var setByGenderData =function () {
                $scope.byGenderDataSource.categories=[];
                $scope.byGenderDataSource.dataset = [];
                var category =[];
                var _byGender = _.groupBy( $scope.testByGender,function (i) {
                    return i.gender;
                });

                var byGender = $.map(_byGender, function(value, index) {
                    return [{"gender":index,"results":value}];
                });
                var _byResultType = _.groupBy( $scope.testByGender,function (i) {
                    return i.resulttype;
                });

                var byResultType = $.map(_byResultType, function(value, index) {
                    return [{"seriesname":index,"data":[]}];
                });
                angular.forEach(byGender,function (value1) {
                    var c = {label:(value1.gender)};
                    category.push(c);
                    angular.forEach(byResultType,function (value2) {
                        var total = 0;
                        var x = _.filter($scope.testByGender,function (d) {
                            return d.gender == value1.gender && d.resulttype == value2.seriesname;
                        });
                        if(x !== undefined && x.length >=1) {
                            total = x[0].total;
                        }
                        value2.data.push({value:total});
                    });
                });

                $scope.byGenderDataSource.categories.push({category:category});
                $scope.byGenderDataSource.dataset = byResultType;
                $scope.byGenderDataSource.caption = (($scope.lab =="%")?"All labs":$scope.lab)+
                    " Test by Gender "+$scope.selectedDate;
            };

            var loadByAge = function () {
                $scope.byAgeIsLoading = true;
                LabDashboardService.getByAge({
                    startDate: $scope.startDate,
                    endDate:$scope.endDate,
                    lab:$scope.lab},function (data) {
                        $scope.testByAge = data.testByAge;
                        setByAgeData();
                        $scope.byAgeIsLoading = false;
                    },function (error) {
                        $scope.byAgeIsLoading = false;
                    });
            };

            var setByAgeData = function () {
                $scope.byAgeDataSource.categories=[];
                $scope.byAgeDataSource.dataset=[];
                var category =[];
                var _byAge = _.groupBy( $scope.testByAge,function (i) {
                    return i.age;
                });

                var byAge = $.map(_byAge, function(value, index) {
                    return [{"age":index,"results":value}];
                });
                var _byResultType = _.groupBy( $scope.testByAge,function (i) {
                    return i.resulttype;
                });

                var byResultType = $.map(_byResultType, function(value, index) {
                    return [{"seriesname":index,"data":[]}];
                });
                angular.forEach(byAge,function (value1) {
                    var c = {label:(value1.age)};
                    category.push(c);
                    angular.forEach(byResultType,function (value2) {
                        var total = 0;
                        // var color ='#008ee4';
                        // if(value2.seriesname == 'not suppressed')
                        //     color = '#9b59b6';
                        var x = _.filter($scope.testByAge,function (d) {
                            return d.age == value1.age && d.resulttype == value2.seriesname;
                        });
                        if(x !== undefined && x.length >=1) {
                            total = x[0].total;
                        }
                        value2.data.push({value:total});
                    });
                });

                $scope.byAgeDataSource.categories.push({category:category});
                $scope.byAgeDataSource.dataset=byResultType;
                $scope.byAgeDataSource.caption = (($scope.lab =="%")?"All Labs":$scope.lab)+
                    " Test by Age "+$scope.selectedDate;

            };

            var loadTestByResult = function () {
                $scope.byResultIsLoading = true;
                LabDashboardService.getByResult({
                    startDate: $scope.startDate,
                    endDate:$scope.endDate,
                    lab:$scope.lab},function (data) {
                        $scope.testByResult = data.payload;
                        setByResult();
                        $scope.byResultIsLoading = false;
                });
            };

            var setByResult = function () {
                var data =[];

                var total = 0;
                _.filter($scope.testByResult,function (d) {
                    total= total+d.total;
                });

                angular.forEach($scope.testByResult, function (value) {
                    var d ={};
                    if (total !== 0){
                        var label = (value.total/total)*100;
                        d.label = value.resulttype +'('+label.toFixed(2)+'%)';
                    }else {
                        d.label = value.resulttype;
                    }
                    d.value = value.total;
                    data.push(d);
                });
                $scope.vlOutComesDataSource.data = data;
                $scope.vlOutComesDataSource.caption = (($scope.lab =="%")?"All Labs":$scope.lab)+
                    " Test OutCome "+ $scope.selectedDate;

            };

            var loadTestByJustification = function () {
                $scope.byJustificationIsLoading = true;
                LabDashboardService.getByJustification({
                    startDate: $scope.startDate,
                    endDate:$scope.endDate,
                    lab:$scope.lab},function (data) {
                        $scope.testByJustification = data.payload;
                        setByJustification();
                        $scope.byJustificationIsLoading = false;
                });
            };

            var setByJustification = function () {
                var data =[];
                var total = 0;
                _.filter($scope.testByJustification,function (d) {
                    total= total+d.total;
                });

                angular.forEach($scope.testByJustification, function (value) {
                    var d ={};
                    if(total !== 0) {
                        var label = (value.total/total)*100;
                        d.label = value.justification +'('+label.toFixed(2)+'%)';
                    }else{
                        d.label = value.justification;
                    }
                    d.value = value.total;
                    data.push(d);
                });
                $scope.justficationForTestDataSource.data = data;
                $scope.justficationForTestDataSource.caption = (($scope.lab =="%")?"All Labs":$scope.lab)+
                    " Justification for test "+$scope.selectedDate;

            };

            var loadLabsData =function () {
                $scope.byRegionIsLoading = true;
                LabDashboardService.getByLabs({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                    $scope.byRegion = data.payload;
                    setLabsData();
                    $scope.byRegionIsLoading = false;

                });
            };

            var setLabsData =function () {
                $scope.byRegionDataSource.lineset = [];
                $scope.byRegionDataSource.dataset={dataset:[]};
                $scope.byRegionDataSource.categories=[];
                var category =[];
                var _byRegion = _.groupBy( $scope.byRegion,function (i) {
                    return i.laboratory;
                });

                var byRegion = $.map(_byRegion, function(value, index) {
                    return [{"laboratory":index,"results":value}];
                });

                var _resultType = _.groupBy( $scope.byRegion,function (i) {
                    return i.resulttype;
                });
                var lineseries =[];
                var resultType = $.map(_resultType, function(value, index) {
                    return [{"seriesname": index, "data": []}];
                });
                var series ={seriesname: 'suppression',showValues:0,data:[]};
                lineseries.push(series);
                angular.forEach(byRegion,function (value1) {
                    var c = {label:(value1.laboratory)};
                    category.push(c);
                    angular.forEach(lineseries,function (value2) {
                        var total = 0;
                        var not_suppressed = _.filter($scope.byRegion,function (d) {
                            return d.laboratory === value1.laboratory && d.resulttype === 'not suppressed';
                        });
                        var suppressed = _.filter($scope.byRegion,function (d) {
                            return d.laboratory === value1.laboratory && d.resulttype === 'suppressed';
                        });
                        var t =suppressed[0].total + not_suppressed[0].total;
                        if(t !== 0) {
                            total = (suppressed[0].total/t)*100;
                        }
                        value2.data.push({value:total});
                    });
                });

                angular.forEach(byRegion,function (value1) {
                    angular.forEach(resultType,function (value2) {
                        var total = 0;
                        var x = _.filter($scope.byRegion,function (d) {
                            return d.laboratory === value1.laboratory && d.resulttype === value2.seriesname;
                        });
                        if(x !== undefined && x.length >=1) {
                            total = x[0].total;
                        }
                        value2.data.push({value:total});
                    });
                });
                $scope.byRegionDataSource.lineset = lineseries;
                $scope.byRegionDataSource.dataset={dataset:resultType};
                $scope.byRegionDataSource.categories.push({category:category});
                $scope.byRegionDataSource.caption = " Test by Laboratory "+$scope.selectedDate;
            };

            $scope.tatDataSource ={
                "chart": {
                    "theme": "fint",
                    "numberSuffix": "days",
                    "chartBottomMargin": "10",
                    "valueFontSize": "11",
                    "baseFont": "Inherit",
                    "valueFontBold": "0",
                    "gaugeFillMix": "{light-10},{light-70},{dark-10}",
                    "gaugeFillRatio": "40,20,40"

                },
                "colorRange": {
                    "color": [
                    ]
                },
                "pointers": {
                    "pointer": [
                        {
                            "value": "200"
                        }
                    ]
                }
            };

            $scope.testTrendsDataSource = {
                "chart": {
                    "xAxisname": "Months",
                    "yAxisName": "Samples",
                    "showSum": "1",
                    "formatNumberScale":0,
                    "yFormatNumberScale":0,
                    "xFormatNumberScale":0,
                    "numberPrefix": "",
                    "labelDisplay":"rotate",
                    "slantLabels": "1",
                    "theme": "fint",
                    "baseFont": "Inherit",
                    "exportEnabled": "1",
                    "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                    "exportTargetWindow": "_self",
                    "paletteColors" : "#01B8AA,#FD625E,#F2C80F,#374649"
                },
                "categories": [
                ],
                "dataset": [

                ]
            };

            $scope.byGenderDataSource = {
                "chart": {
                    "xAxisname": "Gender",
                    "yAxisName": "Tests",
                    "formatNumberScale":0,
                    "yFormatNumberScale":0,
                    "xFormatNumberScale":0,
                    "showSum": "1",
                    "numberPrefix": "",
                    "theme": "fint",
                    "baseFont": "Inherit",
                    "exportEnabled": "1",
                    "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                    "exportTargetWindow": "_self",
                    "paletteColors" : "#01B8AA, #FD625E, #374649,#F2C80F"
                },
                "categories": [

                ],
                "dataset": [

                ]
            };

            $scope.byAgeDataSource = {
                "chart": {
                    "xAxisname": "Age(Years)",
                    "yAxisName": "Tests",
                    "formatNumberScale":0,
                    "yFormatNumberScale":0,
                    "xFormatNumberScale":0,
                    "showSum": "1",
                    "numberPrefix": "",
                    "theme": "fint",
                    "baseFont": "Inherit",
                    "exportEnabled": "1",
                    "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                    "exportTargetWindow": "_self",
                    "paletteColors" : "#01B8AA, #FD625E, #374649,#F2C80F"
                },
                "categories": [

                ],
                "dataset": [

                ]
            };

            $scope.vlOutComesDataSource={
                "chart": {
                    "numberPrefix": "",
                    "formatNumberScale":0,
                    "yFormatNumberScale":0,
                    "xFormatNumberScale":0,
                    "showPercentInTooltip": "0",
                    "decimals": "1",
                    "useDataPlotColorForLabels": "1",
                    "theme": "fint",
                    "baseFont": "Inherit",
                    "exportEnabled": "1",
                    "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                    "exportTargetWindow": "_self",
                    "paletteColors" : "#01B8AA, #FD625E, #374649,#F2C80F"
                },
                "data": []
            };

            $scope.justficationForTestDataSource = {
                "chart": {
                    "numberPrefix": "",
                    "showPercentInTooltip": "0",
                    "formatNumberScale":0,
                    "yFormatNumberScale":0,
                    "xFormatNumberScale":0,
                    "decimals": "1",
                    "labelDisplay":"rotate",
                    "slantLabels": "1",
                    "useDataPlotColorForLabels": "1",
                    "theme": "fint",
                    "baseFont": "Inherit",
                    "exportEnabled": "1",
                    "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                    "exportTargetWindow": "_self",
                    "paletteColors" : "#01B8AA, #FD625E, #374649,#F2C80F,5F6B6D"
                },
                "data": [
                ]
            };

            $scope.byRegionDataSource = {
                "chart": {
                    "xAxisname": "Region",
                    "pYAxisName": "Tests",
                    "sYAxisName": "Suppression %",
                    "sNumberSuffix": "%",
                    "showSum": "1",
                    "formatNumberScale":0,
                    "yFormatNumberScale":0,
                    "xFormatNumberScale":0,
                    "labelDisplay":"rotate",
                    "slantLabels": "1",
                    "numberPrefix": "",
                    "theme": "fint",
                    "baseFont": "Inherit",
                    "exportEnabled": "1",
                    "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLS=Export Chart Data",
                    "exportTargetWindow": "_self",
                    "paletteColors" : "#01B8AA, #FD625E, #F2C80F,#374649"
                },
                "categories": [
                ],
                "dataset": [

                ]
            };
        }
    })();