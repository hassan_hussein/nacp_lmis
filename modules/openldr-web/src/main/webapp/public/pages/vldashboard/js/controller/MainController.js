/**
 * Created by user on 8/10/2017.
 */
(function(){
'use strict';

    angular
        .module('mainDashboard')
        .controller('MainController', ['$scope',MainController]);

    function MainController($scope) {
        $scope.themes = ['fint','ocean','zune','carbon'];
        $scope.chartMode = '2d';

        $scope.regions =[{name:'Arusha', id:1},{name:'Kilimanjaro', id:2}];

        $scope.changeTheme = function (theme) {
            $scope.testTrendsDataSource.chart.theme = theme;
            $scope.tatDataSource.chart.theme = theme;
            $scope.vlOutComesDataSource.chart.theme = theme;
            $scope.byGenderDataSource.chart.theme = theme;
            $scope.byAgeDataSource.chart.theme = theme;
            $scope.justficationForTestDataSource.chart.theme = theme;
        };
        $scope.loadReady =false;
        $scope.setMode = function (mode) {
            $scope.chartMode = mode;
        };

    }

})();