/**
 * Created by user on 8/10/2017.
 */
(function(){
'use strict';

    angular
        .module('mainDashboard')
        .controller('TrendController', ['$scope','$timeout','DashBoardService',TrendController
        ]);

    function TrendController($scope,$timeout,DashBoardService) {

        $scope.filterChange = function (filter) {
            $scope.filter = filter;
            if($scope.loadReady) {

                loadData();

            }
        };
        var loadData = function () {
            outComeTrend();
            suppressionTrend();
        };


        var outComeTrend = function () {
            $scope.outComeTrendIsLoading = true;
            DashBoardService.getTAT({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                $scope.outComeTrendByYear = [
                    {year:'2016',resulttype:'Suppressed',total:"230"},
                    {year:'2016',resulttype:'Not suppressed',total:"110"},
                    {year:'2016',resulttype:'Suppression',total:"67.6"},
                    {year:'2017',resulttype:'Suppressed',total:"200"},
                    {year:'2017',resulttype:'Not suppressed',total:"50"},
                    {year:'2017',resulttype:'Suppression',total:"80"},
                    {year:'2018',resulttype:'Suppressed',total:"75"},
                    {year:'2018',resulttype:'Not suppressed',total:"150"},
                    {year:'2018',resulttype:'Suppression',total:"33.3"},

                ];
                setOutComeTrend();
                $scope.outComeTrendIsLoading = false;
            });
        };
        var setOutComeTrend =function () {
            var category =[];
            var _byYear = _.groupBy( $scope.outComeTrendByYear,function (i) {
                return i.year;
            });

            var byYear = $.map(_byYear, function(value, index) {
                return [{"year":index,"results":value}];
            });

            var _resultType = _.groupBy( $scope.outComeTrendByYear,function (i) {
                return i.resulttype;
            });
            var lineseries =[];
            var resultType = $.map(_resultType, function(value, index) {
                if(index == 'Suppression' && $scope.$parent.chartMode == '3d') {
                     return [{"seriesname": index, "parentYAxis": "S", "renderAs": "line", "data": []}];
                }
                else if(index == 'Suppression' && $scope.$parent.chartMode == '2d'){
                    var series ={seriesname: index,showValues:0,data:[]};
                    lineseries.push(series);
                }
                else{
                    return [{"seriesname": index, "data": []}];
                }
            });
            angular.forEach(byYear,function (value1) {
                var c = {label:(value1.year)};
                category.push(c);
                angular.forEach(lineseries,function (value2) {
                    var total = 0;
                    var x = _.filter($scope.outComeTrendByYear,function (d) {
                        return d.year === value1.year && d.resulttype === value2.seriesname;
                    });
                    if(x !== undefined && x.length >=1) {
                        total = x[0].total;
                    }
                    value2.data.push({value:total});
                });
            });

            if($scope.$parent.chartMode == '2d'){
                angular.forEach(byYear,function (value1) {
                    angular.forEach(resultType,function (value2) {
                        var total = 0;
                        var x = _.filter($scope.outComeTrendByYear,function (d) {
                            return d.year === value1.year && d.resulttype === value2.seriesname;
                        });
                        if(x !== undefined && x.length >=1) {
                            total = x[0].total;
                        }
                        value2.data.push({value:total});
                    });
                });
                $scope.byYearTrendDataSource2d.lineset = lineseries;
                $scope.byYearTrendDataSource2d.dataset={dataset:resultType};
            }else{
                $scope.byYearTrendDataSource3d.dataset=resultType;
            }

            $scope.byYearTrendDataSource2d.categories.push({category:category});
            $scope.byYearTrendDataSource3d.categories.push({category:category});
        };

        var suppressionTrend = function () {
            $scope.suppressionTrendIsLoading = true;
            DashBoardService.getTAT({startDate: $scope.startDate,endDate:$scope.endDate},function (data) {
                $scope.suppressionTrend = [
                    {year:'2016',month:'jan',suppression:"20"},
                    {year:'2016',month:'feb',suppression:"30"},
                    {year:'2016',month:'march',suppression:"40"},
                    {year:'2017',month:'jan',suppression:"30"},
                    {year:'2017',month:'feb',suppression:"40"},
                    {year:'2017',month:'march',suppression:"50"},
                    {year:'2018',month:'jan',suppression:"40"},
                    {year:'2018',month:'feb',suppression:"50"},
                    {year:'2018',month:'march',suppression:"60"},

                ];
                setSuppressionTrend();
                $scope.suppressionTrendIsLoading = false;
            });
        };
        var setSuppressionTrend =function () {
            var category =[];
            var _byMonth = _.groupBy( $scope.suppressionTrend,function (i) {
                return i.month;
            });

            var byMonth = $.map(_byMonth, function(value, index) {
                return [{"month":index,"results":value}];
            });

            var _resultYear = _.groupBy( $scope.suppressionTrend,function (i) {
                return i.year;
            });

            var resultYear = $.map(_resultYear, function(value, index) {
                    return [{"seriesname": index, "data": []}];
            });
            angular.forEach(byMonth,function (value1) {
                var c = {label:value1.month, "stepSkipped": false, "appliedSmartLabel": true};
                category.push(c);
                angular.forEach(resultYear,function (value2) {
                    var suppression = 0;
                    var x = _.filter($scope.suppressionTrend,function (y) {
                        return y.month === value1.month && y.year === value2.seriesname;
                    });
                    if(x !== undefined && x.length >=1) {
                        suppression = x[0].suppression;
                    }
                    value2.data.push({value:suppression});
                });
            });


            $scope.suppressionTrendDataSource.categories.push({category:category});
            $scope.suppressionTrendDataSource.dataset= resultYear;
        };

        $timeout(function () {
            $scope.loadReady = true;
            loadData();
        },1000);


        $scope.byYearTrendDataSource2d = {
            "chart": {
                "caption": "OutCome",
                "subCaption": "2017",
                "xAxisname": "Region",
                "pYAxisName": "Tests",
                "sYAxisName": "Suppression %",
                "sNumberSuffix": "%",
                "showSum": "1",
                "numberPrefix": "",
                "theme": "ocean"
            },
            "categories": [
            ],
            "dataset": [

            ]
        };
        $scope.byYearTrendDataSource3d = angular.copy( $scope.byYearTrendDataSource2d);

        $scope.suppressionTrendDataSource=   {
            "chart": {
            "caption": "Suppression Trend",
                "numberprefix": "%",
                "plotgradientcolor": "",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showvalues": "0",
                "showcanvasborder": "0",
                "canvasborderalpha": "0",
                "canvasbordercolor": "CCCCCC",
                "canvasborderthickness": "1",
                "captionpadding": "30",
                "linethickness": "3",
                "yaxisvaluespadding": "15",
                "legendshadow": "0",
                "legendborderalpha": "0",
                "palettecolors": "#f8bd19,#008ee4,#33bdda,#e44a00,#6baa01,#583e78",
                "showborder": "0"
        },
            "categories": [

        ],
            "dataset":[

        ]
        }


    }

})();