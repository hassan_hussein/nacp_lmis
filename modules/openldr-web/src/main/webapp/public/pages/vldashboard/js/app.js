    'use strict';

    var app = angular.module('mainDashboard', ['ngSanitize', 'ng-fusioncharts', 'ui.router', 'dashServices']);

    app.config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/summary');

        $stateProvider
            .state('vl', {
                url: '',
                templateUrl: 'views/main.html',
                controller: 'MainController',
                abstract: true
            })
            .state('vl.summary', {
                url: '/summary',
                templateUrl: 'views/summary.html',
                controller: 'SummaryController',
                data: {
                    title: 'Summary'
                }
            }).state('vl.trend', {
                url: '/trend',
                templateUrl: 'views/trend.html',
                controller: 'TrendController',
                data: {
                    title: 'Trend'
                }
            }).state('vl.lab', {
                url: '/lab',
                templateUrl: 'views/lab.html',
                controller: 'LabPerformanceController',
                data: {
                    title: 'Lab Performance'
                }
            });
    });