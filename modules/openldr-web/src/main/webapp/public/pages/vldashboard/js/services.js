
var dashServices  =    angular.module('dashServices', ['ngResource']);
dashServices.factory('GetTAT', function ($resource) {
    return $resource('/main-dashboard/getTAT', {}, {});
});

dashServices.factory('GetSampleTypesByYear', function ($resource) {
    return $resource('/main-dashboard/getSampleTypesByYear/:year', {}, {});
});

dashServices.factory('DashBoardService', function ($resource) {
    return $resource('', {}, {
        getTAT:{url:'/main-dashboard/getTAT',method:'GET'},
        getTrendData:{url:'/main-dashboard/getTrendData',method:'GET'},
        getByGender:{url:'/main-dashboard/getByGender',method:'GET'},
        getByAge:{url:'/main-dashboard/getByAge',method:'GET'},
        getByResult:{url:'/main-dashboard/getByResult',method:'GET'},
        getByJustification:{url:'/main-dashboard/getByJustification',method:'GET'},
        getByRegion:{url:'/main-dashboard/getByRegion',method:'GET'},
        getRegions:{url:'/main-dashboard/getRegions',method:'GET'}
    });
});

dashServices.factory('LabDashboardService', function ($resource) {
    return $resource('', {}, {
        getLabs: {url:'/lab-dashboard/labs', method: 'GET'},
        getTAT: {url:'/lab-dashboard/tat', method:'GET'},
        getTrendData: {url:'/lab-dashboard/trendData', method:'GET'},
        getByGender: {url:'/lab-dashboard/getByGender', method: 'GET'},
        getByAge: {url: '/lab-dashboard/getByAge', method: 'GET'},
        getByResult: {url: '/lab-dashboard/getByResult', method: 'GET'},
        getByJustification: {url: '/lab-dashboard/getByJustification', method: 'GET'},
        getByLabs: {url:'/lab-dashboard/getByLaboratories', method: 'GET'}
    });
});