
var service=angular.module('service', ['ngResource']);
var update = {update: {method: 'PUT'}};

service.factory('RegisterClient', function ($resource) {
    return $resource('/site/register/client.json', {},update);
});

service.factory('GetByUsername', function ($resource) {
    return $resource('/site/client/get-by-username.json', {},{});
});

service.factory('GetByCellPhone', function ($resource) {
    return $resource('/site/client/get-by-cellphone.json', {},{});
});

service.factory('GetByEmail', function ($resource) {
    return $resource('/site/client/get-by-email.json', {},{});
});

service.factory('VerifyAccount', function ($resource) {
    return $resource('/site/client/verify.json', {},{});
});

service.value('version', '@version@');

service.factory('Locales', function ($resource) {
    return $resource('/locales.json', {}, {});
});

service.factory('ChangeLocale', function ($resource) {
    return $resource('/changeLocale.json', {}, update);
});

service.factory('GetData', function ($resource) {
    return $resource('/public/js/shared/directives/tatData.json', {}, {});
});

service.factory('GetData2', function ($resource) {
    return $resource('/public/js/shared/directives/tatData.json', {}, {});
});



service.factory('ChangeLocale', function ($resource) {
    return $resource('/changeLocale.json', {}, update);
});

service.factory('leaflet', function ($resource) {
    return $resource('/public/coordinate.json', {}, {});
});

service.factory('Authenticate', function ($resource) {
    return $resource('http://197.149.178.42/mohsw/00000000000000000000000000000000/v1/json/authenticate', {}, {});
});

service.factory('Regions', function ($resource) {
    return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/regions', {}, {});
});

service.factory('DistrictsByRegion', function ($resource) {
    return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/districts', {}, {});
});

service.factory('FacilitiesByDistrict', function ($resource) {
    return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/facilities', {}, {});
});

service.factory('TestTypes', function ($resource) {
    return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/tests', {}, {});
});

service.factory('Tests', function ($resource) {
    return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/tat', {}, {});
});

service.factory('TestingTrend', function ($resource) {
    return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/testingtrends', {testcode:'@testCode',start:'@start',end:'end'}, {});
});

service.factory('TestsResults', function ($resource) {
    return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/results', {}, {});
});

service.factory('MapData', function ($resource) {
    return $resource('/public/js/shared/directives/mapData.json', {}, {});
});

service.factory('GetYearTotals', function ($resource) {
    return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/testtotals', {testcode:'@testCode',start:'@start',end:'end'}, {});
});

service.factory('GetYearResults', function ($resource) {
    return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/testbreakdown', {testcode:'@testCode',start:'@start',end:'end',observationcode:'@observationCode'}, {});
});

service.factory('GetYearBySampleType1', function($resource){
    return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/results', {testcode:'@testCode',start:'@start',end:'end'},{});
});

service.factory('GetTestTotalByAge', function($resource){
    return $resource('http://197.149.178.42/mohsw/bcb911e686c642d666d869443a30bd77/v1/json/testtotalsbyage',{testcode:'@testCode',start:'@start',end:'end'},{});
});

siteServices.factory('GetYearBySampleType', function($resource){
    return $resource('/rest-api/getAllBySampleType',{start:'2017-05-01',end:'2017-05-30'},{});
});