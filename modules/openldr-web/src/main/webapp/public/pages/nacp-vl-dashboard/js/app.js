/**
 * INSPINIA - Responsive Admin Theme
 *
 */
//(function () {
  var service = angular.module('inspinia', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'pascalprecht.translate',       // Angular Translate
        'ngIdle',                       // Idle timer
        'ngSanitize'                    // ngSanitize
    ]);
    /*.
    config(['$routeProvider', function ($routeProvider) {
            $routeProvider.
                when('/dashboard_2', {controller: TestController, templateUrl: 'views/dashboard_2.html',resolve: TestController.resolve}).

                otherwise("/dashboards/dashboard_1");

        }]);*/
//})();

// Other libraries are loaded dynamically in the config.js file using the library ocLazyLoad