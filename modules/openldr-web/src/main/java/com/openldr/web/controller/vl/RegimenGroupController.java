package com.openldr.web.controller.vl;

import com.openldr.core.exception.DataException;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.domain.RegimenGroup;
import com.openldr.vl.service.RegimenGroupService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping(value = "/vl/regimenGroup")
@Controller
@NoArgsConstructor
public class RegimenGroupController extends BaseController {

    @Autowired
    private RegimenGroupService service;

    @RequestMapping(value = "/save", method = RequestMethod.PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody RegimenGroup regimenGroup) {
        try {
            service.save(regimenGroup);
            return success("status","Regimen group saved");

        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/getById", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimenGroup", service.getById(id));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getAll", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll() {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimenGroups", service.getAll());
        return openLdrResponse.response(OK);
    }


}
