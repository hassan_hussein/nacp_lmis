package com.openldr.web.controller.vl;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.Pagination;
import com.openldr.core.exception.DataException;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.domain.Sample;
import com.openldr.vl.domain.SampleDTO;
import com.openldr.vl.domain.SampleStatus;
import com.openldr.vl.domain.SampleStatusChange;
import com.openldr.vl.service.NationalDashboardApiService;
import com.openldr.vl.service.NotificationService;
import com.openldr.vl.service.SampleService;
import com.openldr.vl.service.SampleStatusChangeService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

import static com.openldr.core.web.OpenLdrResponse.error;
import static java.lang.Integer.parseInt;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping(value = "/vl/sample")
@Controller
@NoArgsConstructor
public class SampleController extends BaseController {

    @Autowired
    private SampleService service;
    @Autowired
    private NationalDashboardApiService nationalDashboardApiService;

    @Autowired
    private SampleStatusChangeService statusChangeService;


    @Autowired
    private NotificationService notificationService;

    @Value("${search.page.size}")
    String limit;

    public static final String receivedStatus="RECEIVED";
    public static final String acceptedStatus="ACCEPTED";
    public static final String rejectedStatus="REJECTED";
    public static final String inprogressStatus="INPROGRESS";
    public static final String approveStatus="APPROVED";
    public static final String dispatchStatus="DISPATCHED";
    public static final String registered = "REGISTERED";


    @RequestMapping(value = "/save", method = RequestMethod.PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody Sample sample, HttpServletRequest httpRequest) {
        Long userId=loggedInUserId(httpRequest);
        try {
            service.insertSample(sample,userId);
            OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample", sample);
            return openLdrResponse.response(OK);

        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/get-for-lab", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getReceivedSamples(HttpServletRequest httpRequest,
                                                              @RequestParam("status") String status,
                                                              @RequestParam(value = "page", defaultValue = "1") Integer page
                                                              ){
        Long userId=loggedInUserId(httpRequest);
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("samples", service.getSamplesForLab(userId, status,null,pagination));
        pagination.setTotalRecords(service.getTotalSamplesForLab(userId, status));
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-for-lab-by-number", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByLabNumber(HttpServletRequest httpRequest,
                                                              @RequestParam("status") String status,
                                                                @RequestParam("labNumber") String labNumber,
                                                              @RequestParam(value = "page", defaultValue = "1") Integer page
    ){
        Long userId=loggedInUserId(httpRequest);
        Pagination pagination = new Pagination(page, parseInt(limit));
        List<SampleDTO> samples = service.getSamplesForLab(userId, status, labNumber, pagination);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("samples", samples);
        pagination.setTotalRecords(samples.size());
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }


    @RequestMapping(value = "/get-total-samples-for-lab", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTotalSamplesCreated(HttpServletRequest httpRequest,
                                                              @RequestParam("status") String status
                                                              ){
        Long userId=loggedInUserId(httpRequest);
        return OpenLdrResponse.response("totalSample",service.getTotalSamplesForLab(userId, status));
    }
    @RequestMapping(value = "/get-total-samples-for-hub", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTotalSamplesCreatedForHub(HttpServletRequest httpRequest,
                                                              @RequestParam("status") String status
                                                              ){
        Long userId=loggedInUserId(httpRequest);
        return OpenLdrResponse.response("totalSample",service.getTotalSamplesForHub(userId, status));
    }


    @RequestMapping(value = "/get-for-lab-by-batch", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getReceivedSamples(HttpServletRequest httpRequest,
                                                              @RequestParam("batchNumber") String batchNumber
                                                              ){
        Long userId=loggedInUserId(httpRequest);
        String status=registered;
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("samples", service.getSamplesForLabByBatch(userId,batchNumber, status));
        return openLdrResponse.response(OK);
    }

  @RequestMapping(value = "/get-by-batch", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByStatusAndBatch(HttpServletRequest httpRequest,
                                                              @RequestParam("batchNumber") String batchNumber
                                                              ){
        Long userId=loggedInUserId(httpRequest);
        String status=registered;
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("samples", service.getByStatusAndBatch(userId, batchNumber, status));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/receive-by-batch", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> receiveByBatch(@RequestParam("batchNumber")String batchNumber,
                                                           HttpServletRequest httpRequest){
        Long userId=loggedInUserId(httpRequest);
        String status=registered;
        service.receiveSamplesByBatch(userId,batchNumber, status);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("message","Sample Received Successful");
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/accept-sample", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAcceptSample(@RequestParam("sampleId")Long sampleId,
                                                            HttpServletRequest httpRequest){
        Long userId=loggedInUserId(httpRequest);
        service.acceptSample(userId,sampleId);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample",service.getById(sampleId));
        return openLdrResponse.response(OK);
    }
    @RequestMapping(value = "/test-sample", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> testSample(@RequestParam("sampleId")Long sampleId,
                                                            HttpServletRequest httpRequest){
        Long userId=loggedInUserId(httpRequest);
        service.updateSampleStatus(inprogressStatus, sampleId,userId);

        Sample sample = service.getById(sampleId);
        SampleStatusChange change = new SampleStatusChange(sample, SampleStatus.INPROGRESS,userId);
        statusChangeService.insert(change);

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample",service.getById(sampleId));
        return openLdrResponse.response(OK);
    }
    @RequestMapping(value = "/approve-sample", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> approveSample(@RequestParam("sampleId")Long sampleId,
                                                            HttpServletRequest httpRequest){
        Long userId=loggedInUserId(httpRequest);
        service.updateSampleApprovedStatus(approveStatus, sampleId, userId);

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample",service.getById(sampleId));
        return openLdrResponse.response(OK);
    }
    @RequestMapping(value = "/dispatch-sample", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> dispatchSample(@RequestParam("sampleId")Long sampleId,
                                                            HttpServletRequest httpRequest){
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample",service.getById(sampleId));
        String _res = notificationService.sendResults(sampleId);
	    System.out.println(_res);
        ResponseEntity<OpenLdrResponse> response;
        if(_res.equals("200") || _res.equals("301")){
            service.updateDispatchSampleStatus(dispatchStatus, sampleId, userId);
            response = openLdrResponse.response(OK);
        }else {
            response = openLdrResponse.response(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/get-facilities", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getFacilities(HttpServletRequest httpRequest){
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("facilities", service.getFacilities());
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/reject-sample", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> rejectSample(@RequestParam("sampleId")Long sampleId,
                                                        @RequestParam("reason") String reason,
                                                           HttpServletRequest httpRequest){
        Long userId=loggedInUserId(httpRequest);
        service.rejectSample(userId,sampleId,reason);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample",service.getById(sampleId));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-by-id", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@RequestParam("sampleId")Long sampleId
                                                   ){
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample",service.getById(sampleId));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getSupervisedLabs", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getSupervisedLabs(HttpServletRequest httpRequest
                                                   ){

        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("labs",service.getSupervisedLabs(userId));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getFacilitySupervisors", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getFacilitySupervisors(HttpServletRequest httpRequest
                                                   ){

        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("labs",service.getFacilitySupervisors(userId));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getHomeFacility", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getHomeFacility(HttpServletRequest httpRequest
    ){
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("homeFacility",service.getHomeFacility(userId));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-for-hub", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getSamplesForHub(HttpServletRequest httpRequest,
                                                            @RequestParam("status") String status,
                                                            @RequestParam(value = "page", defaultValue = "1") Integer page
                                                            ){
        Long userId=loggedInUserId(httpRequest);
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("samples", service.getSamplesForHub(userId,status,pagination));
        pagination.setTotalRecords(service.getTotalSamplesForHub(userId, status));
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getTotalTestDoneForDashboard", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTotalTestForDashboard(HttpServletRequest httpRequest
    ){
        Long userId=loggedInUserId(httpRequest);
        Facility f = service.getHomeFacility(userId);

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("dataPoints",service.getTotalTestDoneForDashboard(userId));
        return openLdrResponse.response(OK);
    }
  @RequestMapping(value = "/getTestBySampleForDashboard", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTestBySampleForDashboard(HttpServletRequest httpRequest
    ){
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testBySample",service.getTestBySampleForDashboard(userId));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getSampleByFacilityForDashboard", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getSampleByFacilityForDashboard(HttpServletRequest httpRequest
    ){
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testByLabs",service.getSampleByFacilityForDashboard(userId));
        return openLdrResponse.response(OK);
    }


    @RequestMapping(value = "/rejected-sample-list", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getRejectedSample(
                                                  @RequestParam(value = "page", defaultValue = "1") Integer page,

                                                  HttpServletRequest httpRequest){
        Pagination pagination = new Pagination(page,parseInt(limit));
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample",service.getRejectedSamplesForLab(userId,"REJECTED",pagination));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getHubForm", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getHubForm(
                                                  @RequestParam(value = "page", defaultValue = "1") Integer page,

                                                  HttpServletRequest httpRequest){
        Pagination pagination = new Pagination(page,parseInt(limit));
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("samples",service.getAllFrom());
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getSampleTestByAge", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getSampleTestByAge(HttpServletRequest httpRequest
    ){
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testByAge",service.getSampleTestByAge(userId));
        return openLdrResponse.response(OK);
    }


    @RequestMapping(value = "/deleteSample/{id}", method = RequestMethod.DELETE, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getSampleTestByAge(HttpServletRequest httpRequest, @PathVariable Long id
    ){
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("deleted",service.deleteSample(id));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllById(HttpServletRequest httpRequest, @PathVariable Long id
    ){
        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample",service.getAllById(id));
        return openLdrResponse.response(OK);
    }


    @RequestMapping(value = "/get-result-for-hub", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getResultsForHub(HttpServletRequest httpRequest,
                                                            @RequestParam("status") String status,
                                                            @RequestParam(value = "page", defaultValue = "1") Integer page
    ){
        Long userId=loggedInUserId(httpRequest);
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("samples", service.getSamplesForHub(userId,status,pagination));
        pagination.setTotalRecords(service.getTotalSamplesForHub(userId, status));
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-by-batch-one-row", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByBatch(HttpServletRequest httpRequest,
                                                               @RequestParam("batchNumber") String batchNumber
    ){
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("samples", service.getByBatch(batchNumber));
        return openLdrResponse.response(OK);
    }


    /*@RequestMapping(value = "/list", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getWorkSheets(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("data", nationalDashboardApiService.getAllRequestSamples());
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }*/

}
