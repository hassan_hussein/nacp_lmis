
package com.openldr.web.controller.medical_record;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.medical_record.dto.ConsultationRecordDTO;
import com.openldr.medical_record.service.ConsultationService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@NoArgsConstructor
@RequestMapping(value = "/consultation")
public class ConsultationController extends BaseController {

    @Autowired
    ConsultationService service;

    @RequestMapping(value = "/get-user-consultation-records/{consultationId}", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getConsultationRecords(@PathVariable Long consultationId) {

        return OpenLdrResponse.response("consultationRecords", service.getConsultationById(consultationId));
    }

    @RequestMapping(value = "/forward", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> forwardConsultation(@RequestBody ConsultationRecordDTO consultation) {

        service.forwardConsultation(consultation);
        return OpenLdrResponse.response("success", "Success");
    }

    @RequestMapping(value = "/save", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> saveConsultation(@RequestBody ConsultationRecordDTO consultation) {

        service.saveConsultation(consultation);
        return OpenLdrResponse.response("success", "Success");
    }

    @RequestMapping(value = "/get-queue/{doctorId}", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getDoctorQueue(@PathVariable Long doctorId) {

        return OpenLdrResponse.response("queue", service.getQueue(doctorId));
    }


}