package com.openldr.web.controller.vl;

import com.openldr.core.exception.DataException;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.domain.VLTestingReason;
import com.openldr.vl.service.VLTestingReasonService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping(value = "/vl/vlTestingReason")
@Controller
@NoArgsConstructor
public class VLTestingReasonController extends BaseController {

    @Autowired
    private VLTestingReasonService service;

    @RequestMapping(value = "/save", method = RequestMethod.PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody VLTestingReason reason) {
        try {
            service.save(reason);
            return success("status","Reason saved" );

        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/getById", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testingReason", service.getReasonById(id));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getAll", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll() {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testingReasons", service.getAllReasons());
        return openLdrResponse.response(OK);
    }


}
