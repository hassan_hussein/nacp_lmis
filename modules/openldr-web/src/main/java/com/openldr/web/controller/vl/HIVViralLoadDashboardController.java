package com.openldr.web.controller.vl;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.service.VlDashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by hassan on 4/9/17.
 */
@Controller
@RequestMapping(value = "/vl/dashboard/")
public class HIVViralLoadDashboardController extends BaseController{
    @Autowired
    private VlDashboardService service;

    @RequestMapping(value = "labTatSummary.json", method = RequestMethod.GET)
    public ResponseEntity<OpenLdrResponse> getLabTatSummary(HttpServletRequest request){
        Long userId = this.loggedInUserId(request);
        return OpenLdrResponse.response("labTatSummary", service.getLabTAT(userId));
    }

    @RequestMapping(value = "testTrendForLab.json", method = RequestMethod.GET)
    public ResponseEntity<OpenLdrResponse> getTestTrendForLab(HttpServletRequest request){
        Long userId = this.loggedInUserId(request);
        return OpenLdrResponse.response("testTrendForLab", service.getTestTrendForLab(userId));
    }

    @RequestMapping(value = "getVLResults.json", method = RequestMethod.GET)
    public ResponseEntity<OpenLdrResponse> getVLResults(HttpServletRequest request){
        Long userId = this.loggedInUserId(request);
        return OpenLdrResponse.response("vlResults", service.getVLResults(userId));
    }

    @RequestMapping(value = "getByAgeLessthan2.json", method = RequestMethod.GET)
    public ResponseEntity<OpenLdrResponse> getByAgeLessthan2(HttpServletRequest request){
        Long userId = this.loggedInUserId(request);
        return OpenLdrResponse.response("vlResults", service.getTrack(userId));
    }
}
