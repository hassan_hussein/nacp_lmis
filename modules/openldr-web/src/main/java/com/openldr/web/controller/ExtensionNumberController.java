package com.openldr.web.controller;

import com.openldr.core.domain.ExtensionNumber;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.ExtensionNumberService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by hassan on 7/28/16.
 */

@Controller
@NoArgsConstructor
public class ExtensionNumberController extends BaseController {
    @Autowired
    ExtensionNumberService service;

    @RequestMapping(value = "/extensionNumbersBy/{id}", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllBy(@PathVariable("id") Long id, HttpServletRequest request) {
        return OpenLdrResponse.response("allExtensionNumbers", service.getAllById(id));
    }

    @RequestMapping(value = "/getAllExtensionNumbers", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll(HttpServletRequest request) {
        return OpenLdrResponse.response("allExtensionNumbers", service.getAll());
    }


    @RequestMapping(value = "/extensionNumbers", method = POST, headers = ACCEPT_JSON)
    // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
    public ResponseEntity<OpenLdrResponse> insert(@RequestBody ExtensionNumber number,
                                                  HttpServletRequest request) {
        try {
            service.save(number);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }
        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.extension.number.created.success", number.getExtensionNumber()));
        success.getBody().addData("extension_numbers", number);
        return success;
    }


    @RequestMapping(value = "/extensionNumbers/{id}", method = PUT, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
    public ResponseEntity<OpenLdrResponse> update(@RequestBody ExtensionNumber number,
                                                  @PathVariable("id") Long id,
                                                  HttpServletRequest request) {
        Long userId = loggedInUserId(request);
        number.setId(id);
        try {
            service.save(number);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }
        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.extension.number.updated.success", number.getExtensionNumber()));
        success.getBody().addData("extension_numbers", number);
        return success;
    }


    @RequestMapping(value = "/extensionNumbers/delete/{id}", method = DELETE, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
    public ResponseEntity<OpenLdrResponse> delete(@PathVariable("id") Long id,
                                                  HttpServletRequest request) {
        ExtensionNumber extensionNumber = service.getAllById(id);
        try {

            service.delete(id);
        } catch (DataException e) {
            //throw new DataException("error.mandatory.fields.missing");
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }

        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.extension.number.deleted.success", extensionNumber.getExtensionNumber()));
        success.getBody().addData("extension_numbers", extensionNumber.getExtensionNumber());
        return success;
    }


}
