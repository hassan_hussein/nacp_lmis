package com.openldr.web.controller.vl;

import com.openldr.core.domain.Pagination;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.domain.HIVTestWorkSheet;
import com.openldr.vl.service.HIVTestWorkSheetService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import static java.lang.Integer.parseInt;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RequestMapping(value = "/vl/workSheet")
@Controller
@NoArgsConstructor
public class HIVTestWorkSheetController extends BaseController {

    @Autowired
    private HIVTestWorkSheetService service;

    @Value("${search.page.size}")
    private String limit;

    @Transactional
    @RequestMapping(value = "/create", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> create(@RequestBody HIVTestWorkSheet workSheet,
                                                  HttpServletRequest httpRequest) {


        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("workSheet", service.create(userId,workSheet));
        return openLdrResponse.response(OK);
    }

    @Transactional
    @RequestMapping(value = "/prepare", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> prepare(@Param("equipmentId") Long equipmentId,
                                                  HttpServletRequest httpRequest) {

        Long userId=loggedInUserId(httpRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("workSheet", service.prepare(userId, equipmentId));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/workSheets", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getWorkSheets(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("workSheets", service.getWorkSheet(pagination));
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }

    @Transactional
    @RequestMapping(value = "/get-by-id", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@RequestParam("workSheetId") Long workSheetId) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("workSheet", service.getById(workSheetId));
        return openLdrResponse.response(OK);
    }

}
