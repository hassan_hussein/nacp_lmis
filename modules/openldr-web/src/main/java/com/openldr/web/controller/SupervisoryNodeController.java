
package com.openldr.web.controller;

import com.openldr.core.domain.Pagination;
import com.openldr.core.domain.SupervisoryNode;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.SupervisoryNodeService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * This controller handle endpoints to list, search, create, update supervisory nodes.
 */

@Controller
@NoArgsConstructor
public class SupervisoryNodeController extends BaseController {
  public static final String SUPERVISORY_NODES = "supervisoryNodes";

  @Autowired
  private SupervisoryNodeService supervisoryNodeService;

  @RequestMapping(value = "/supervisory-nodes", method = GET)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
  public ResponseEntity<OpenLdrResponse> getAll() {
    return OpenLdrResponse.response(SUPERVISORY_NODES, supervisoryNodeService.getAll());
  }

  @RequestMapping(value = "/supervisory-nodes/list", method = GET)
  public ResponseEntity<OpenLdrResponse> getAllReadOnly() {
    return OpenLdrResponse.response(SUPERVISORY_NODES, supervisoryNodeService.getAll());
  }

  @RequestMapping(value = "/paged-search-supervisory-nodes", method = GET)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_SUPERVISORY_NODE')")
  public ResponseEntity<OpenLdrResponse> searchSupervisoryNode(@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
                                                                @RequestParam(required = true) String param,
                                                                @RequestParam(required = true) Boolean parent) {
    ResponseEntity<OpenLdrResponse> response = OpenLdrResponse.response(SUPERVISORY_NODES,
      supervisoryNodeService.getSupervisoryNodesBy(page, param, parent));
    Pagination pagination = supervisoryNodeService.getPagination(page);
    pagination.setTotalRecords(supervisoryNodeService.getTotalSearchResultCount(param, parent));
    response.getBody().addData("pagination", pagination);
    return response;
  }

  @RequestMapping(value = "/supervisory-nodes/{id}", method = GET)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_SUPERVISORY_NODE')")
  public SupervisoryNode getById(@PathVariable(value = "id") Long id) {
    return supervisoryNodeService.getSupervisoryNode(id);
  }

  @RequestMapping(value = "/search-supervisory-nodes", method = GET, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_SUPERVISORY_NODE, MANAGE_REQUISITION_GROUP')")
  public List<SupervisoryNode> getFilteredNodes(@RequestParam(value = "searchParam") String param) {
    return supervisoryNodeService.getFilteredSupervisoryNodesByName(param);
  }

  @RequestMapping(value = "/supervisory-nodes", method = POST)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_SUPERVISORY_NODE')")
  public ResponseEntity<OpenLdrResponse> insert(@RequestBody SupervisoryNode supervisoryNode,
                                                 HttpServletRequest request) {
    ResponseEntity<OpenLdrResponse> response;
    Long userId = loggedInUserId(request);
    supervisoryNode.setCreatedBy(userId);
    supervisoryNode.setModifiedBy(userId);
    try {
      supervisoryNodeService.save(supervisoryNode);
    } catch (DataException de) {
      response = OpenLdrResponse.error(de, BAD_REQUEST);
      return response;
    }
    response = OpenLdrResponse.success(
      messageService.message("message.supervisory.node.created.success", supervisoryNode.getName()));
    response.getBody().addData("supervisoryNodeId", supervisoryNode.getId());
    return response;
  }

  @RequestMapping(value = "/supervisory-nodes/{id}", method = PUT, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_SUPERVISORY_NODE')")
  public ResponseEntity<OpenLdrResponse> update(@RequestBody SupervisoryNode supervisoryNode,
                                                 @PathVariable(value = "id") Long supervisoryNodeId,
                                                 HttpServletRequest request) {
    ResponseEntity<OpenLdrResponse> response;
    Long userId = loggedInUserId(request);
    supervisoryNode.setModifiedBy(userId);
    supervisoryNode.setId(supervisoryNodeId);
    try {
      supervisoryNodeService.save(supervisoryNode);
    } catch (DataException de) {
      response = OpenLdrResponse.error(de, BAD_REQUEST);
      return response;
    }
    response = OpenLdrResponse.success(
      messageService.message("message.supervisory.node.updated.success", supervisoryNode.getName()));
    response.getBody().addData("supervisoryNodeId", supervisoryNode.getId());
    return response;
  }

  @RequestMapping(value = "/topLevelSupervisoryNodes", method = GET, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_SUPPLY_LINE')")
  public List<SupervisoryNode> searchTopLevelSupervisoryNodesByName(@RequestParam(value = "searchParam") String param) {
    return supervisoryNodeService.searchTopLevelSupervisoryNodesByName(param);
  }
}
