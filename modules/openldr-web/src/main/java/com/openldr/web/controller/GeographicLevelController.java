package com.openldr.web.controller;

import com.openldr.core.domain.GeographicLevel;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.GeographicLevelService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by hassan on 7/18/16.
 */

@Controller
public class GeographicLevelController extends BaseController {

    @Autowired
    GeographicLevelService service;

    @RequestMapping(value = "geographicLevelBy/{id}", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllBy(@PathVariable("id") Long id, HttpServletRequest request) {
        return OpenLdrResponse.response("geo_levels", service.getGeographicLevel(id));
    }


    @RequestMapping(value = "/geographicLevels", method = POST, headers = ACCEPT_JSON)
    // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
    public ResponseEntity<OpenLdrResponse> insert(@RequestBody GeographicLevel geographicLevel,
                                                  HttpServletRequest request) {
        try {
            service.save(geographicLevel);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }
        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.geo.level.created.success", geographicLevel.getName()));
        success.getBody().addData("geoLevel", geographicLevel);
        return success;
    }


    @RequestMapping(value = "/geographicLevels/{id}", method = PUT, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
    public ResponseEntity<OpenLdrResponse> update(@RequestBody GeographicLevel geographicLevel,
                                                  @PathVariable("id") Long id,
                                                  HttpServletRequest request) {
        Long userId = loggedInUserId(request);
        geographicLevel.setId(id);
        geographicLevel.setModifiedBy(userId);
        try {
            service.save(geographicLevel);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }
        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.geo.level.updated.success", geographicLevel.getName()));
        success.getBody().addData("geoLevel", geographicLevel);
        return success;
    }


    @RequestMapping(value = "/geoLevel/delete/{id}", method = DELETE, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
    public ResponseEntity<OpenLdrResponse> delete(@PathVariable("id") Long id,
                                                  HttpServletRequest request) {
        GeographicLevel geographicLevel = service.getGeographicLevel(id);
        try {

            service.delete(id);
        } catch (DataException e) {
            //throw new DataException("error.mandatory.fields.missing");
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }

        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.geo.level.deleted.success", geographicLevel.getName()));
        success.getBody().addData("geoLevel", geographicLevel);
        return success;
    }


}
