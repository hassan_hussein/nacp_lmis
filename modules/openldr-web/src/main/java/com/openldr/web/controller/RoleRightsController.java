package com.openldr.web.controller;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.Program;
import com.openldr.core.domain.Role;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.RightService;
import com.openldr.core.service.RoleRightsService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by hassan on 7/1/16.
 */
@Controller
@NoArgsConstructor
public class RoleRightsController extends BaseController {

    public static final String ROLE = "role";
    public static final String ROLES_MAP = "roles_map";
    public static final String RIGHTS = "rights";
    public static final String RIGHT_TYPE = "right_type";
    @Autowired
    private RoleRightsService roleRightsService;
    @Autowired
    private RightService rightService;

    @RequestMapping(value = "/rights", method = GET)
    //  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_ROLE')")
    public ResponseEntity<OpenLdrResponse> getAllRights() {
        return response(RIGHTS, rightService.getAll());
    }


    @RequestMapping(value = "/roles", method = POST, headers = ACCEPT_JSON)
    // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_ROLE')")
    public ResponseEntity<OpenLdrResponse> createRole(@RequestBody Role role, HttpServletRequest request) {
        role.setCreatedBy(loggedInUserId(request));
        role.setModifiedBy(loggedInUserId(request));
        try {
            roleRightsService.saveRole(role);
            return success(messageService.message("message.role.created.success", role.getName()));
        } catch (DataException e) {
            return error(e, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/roles", method = GET)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_ROLE, MANAGE_USER')")
    public ResponseEntity<OpenLdrResponse> getAll() {
        OpenLdrResponse response = new OpenLdrResponse(ROLES_MAP, roleRightsService.getAllRolesMap());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/roles/list", method = GET)
    public ResponseEntity<OpenLdrResponse> getAllReadonly() {
        OpenLdrResponse response = new OpenLdrResponse(ROLES_MAP, roleRightsService.getAllRolesMap());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/roles-flat", method = GET)
    // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_ROLE, MANAGE_USER')")
    public ResponseEntity<OpenLdrResponse> getAllRolesFlat() {
        OpenLdrResponse response = new OpenLdrResponse(ROLES_MAP, roleRightsService.getAllRoles());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/roles/{id}", method = GET)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_ROLE')")
    public ResponseEntity<OpenLdrResponse> get(@PathVariable("id") Long id) {
        ResponseEntity<OpenLdrResponse> response = response(ROLE, roleRightsService.getRole(id));
        response.getBody().addData(RIGHT_TYPE, roleRightsService.getRightTypeForRoleId(id));
        return response;
    }

    @RequestMapping(value = "/roles/{id}", method = PUT, headers = ACCEPT_JSON)
    // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_ROLE')")
    public ResponseEntity<OpenLdrResponse> updateRole(@PathVariable("id") Long id, @RequestBody Role role, HttpServletRequest request) {
        role.setModifiedBy(loggedInUserId(request));
        try {
            role.setId(id);
            roleRightsService.updateRole(role);
        } catch (DataException e) {
            return error(e, HttpStatus.CONFLICT);
        }
        return success(messageService.message("message.role.updated.success", role.getName()));
    }

    @RequestMapping(value = "facility/{facilityId}/program/{programId}/rights", method = GET)
    public ResponseEntity<OpenLdrResponse> getRightsForUserAndFacilityProgram(@PathVariable("facilityId") Long facilityId, @PathVariable("programId") Long programId, HttpServletRequest httpServletRequest) {
        return response(RIGHTS, roleRightsService.getRightsForUserAndFacilityProgram(loggedInUserId(httpServletRequest), new Facility(facilityId), new Program(programId)));
    }


    @RequestMapping(value = "/roles/getList", method = RequestMethod.GET, headers = ACCEPT_JSON)
    // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_ROLE')")
    public ResponseEntity<OpenLdrResponse> getRoleList(HttpServletRequest request) {
        return OpenLdrResponse.response("roles", roleRightsService.getAllRoles());
    }

}
