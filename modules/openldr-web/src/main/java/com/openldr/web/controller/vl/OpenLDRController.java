package com.openldr.web.controller.vl;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.dto.NACPResultAPI;
import com.openldr.vl.service.OpenLDRService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.openldr.core.web.OpenLdrResponse.success;

/**
 * Created by dev2 on 2/13/2018.
 */
@RequestMapping(value="/v1/openldr/result")
@Controller
@NoArgsConstructor
public class OpenLDRController  extends BaseController {
    @Autowired
    private OpenLDRService openLDRService;

    @RequestMapping(value = "/save", method= RequestMethod.POST,headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> saveSampleOpenLDRResult(@RequestBody NACPResultAPI resultApi) {
        openLDRService.processResult(resultApi);

        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("Successfully Saved", resultApi.requestID));
        success.getBody().addData("status", resultApi.isSaved);
        return success;
    }
}
