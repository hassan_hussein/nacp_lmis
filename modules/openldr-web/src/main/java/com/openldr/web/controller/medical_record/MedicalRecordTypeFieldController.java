
package com.openldr.web.controller.medical_record;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.medical_record.domain.FieldType;
import com.openldr.medical_record.domain.MedicalRecordTypeField;
import com.openldr.medical_record.domain.MedicalRecordTypeFieldGroup;
import com.openldr.medical_record.service.MedicalRecordTypeFieldGroupService;
import com.openldr.medical_record.service.MedicalRecordTypeFieldService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@NoArgsConstructor
@RequestMapping(value = "/medical-record-type-field")
public class MedicalRecordTypeFieldController extends BaseController {

    @Autowired
    MedicalRecordTypeFieldService service;

    @Autowired
    MedicalRecordTypeFieldGroupService groupService;

    @RequestMapping(value = "/save", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody MedicalRecordTypeField medicalRecordTypeField) {
        service.save(medicalRecordTypeField);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("medicalRecordType", medicalRecordTypeField);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-by_record", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByRecordId(@Param("recordTypeId") Long recordTypeId) {

        List<MedicalRecordTypeField> recordTypesFields = service.getByRecordType(recordTypeId);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("recordTypeFields", recordTypesFields);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-by-id", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        MedicalRecordTypeField recordTypeField = service.getById(id);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("recordTypeField", recordTypeField);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-field-types", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getFieldTypes() {

        List<FieldType> fieldTypes = service.getFieldTypes();
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("fieldTypes", fieldTypes);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-field-groups", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getFieldGroups(@Param("medicalRecordTypeId") Long medicalRecordTypeId) {

        List<MedicalRecordTypeFieldGroup> fieldGroups = groupService.getByRecordType(medicalRecordTypeId);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("fieldGroups", fieldGroups);
        return openLdrResponse.response(OK);
    }

}