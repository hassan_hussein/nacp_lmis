package com.openldr.web.controller;

import com.openldr.core.domain.Organization;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.OrganizationService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;

@RequestMapping(value = "/organization")
@Controller
public class OrganizationController extends BaseController {


    @Autowired
    private OrganizationService service;

    @RequestMapping(value = "/get-all", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll(HttpServletRequest request) {
        return OpenLdrResponse.response("organizations", service.getAll());
    }


    @RequestMapping(value = "/get-by-Id", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllById(HttpServletRequest request, @Param("id") Long id) {
        return OpenLdrResponse.response("organization", service.getById(id));
    }

    @RequestMapping(value = "save", method = RequestMethod.PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> create(@RequestBody Organization organization, HttpServletRequest request) {
        try {
            service.Insert(organization);
            return success(messageService.message("message.user.type.created.success", organization));
        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);

        }

    }


}
