
package com.openldr.web.controller.medical_record;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.medical_record.domain.UserCVTypeField;
import com.openldr.medical_record.service.UserCVTypeFieldService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@NoArgsConstructor
@RequestMapping(value = "/user-cv-type-field")
public class UserCVTypeFieldController extends BaseController {

    @Autowired
    UserCVTypeFieldService service;

    @RequestMapping(value = "/save", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody UserCVTypeField userCVTypeField) {
        service.save(userCVTypeField);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("cvTypeField", userCVTypeField);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-by_cv", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByCVId(@Param("cvTypeId") Long cvTypeId) {

        List<UserCVTypeField> cvTypesFields = service.getByCVType(cvTypeId);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("cvTypeFields", cvTypesFields);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-by-id", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        UserCVTypeField cvTypeField = service.getById(id);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("cvTypeField", cvTypeField);
        return openLdrResponse.response(OK);
    }

}