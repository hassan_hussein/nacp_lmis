
package com.openldr.web.controller;

import com.openldr.core.domain.Program;
import com.openldr.core.service.ProgramService;
import com.openldr.core.service.UserService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * This controller handles endpoint related to listing products for a different criterias, like products related to a facility,
 * program for which requisition can be created, pull based programs, push based programs, details of a program, all programs.
 */

@Controller
@NoArgsConstructor
public class ProgramController extends BaseController {

  public static final String PROGRAM = "program";
  public static final String PROGRAMS = "programs";

  @Autowired
  private ProgramService programService;

  @Autowired
  private UserService userService;

  @RequestMapping(value = "/facilities/{facilityId}/programs", method = GET, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'CREATE_REQUISITION, AUTHORIZE_REQUISITION, MANAGE_USER')")
  public List<Program> getProgramsForFacility(@PathVariable(value = "facilityId") Long facilityId) {
    return programService.getByFacility(facilityId);
  }



  @RequestMapping(value = "/programs/pull", method = GET, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER, CONFIGURE_RNR, MANAGE_SUPPLY_LINE, MANAGE_FACILITY_APPROVED_PRODUCT, MANAGE_REQUISITION_GROUP')")
  public ResponseEntity<OpenLdrResponse> getAllPullPrograms() {
    return OpenLdrResponse.response(PROGRAMS, programService.getAllPullPrograms());
  }

  @RequestMapping(value = "/programs/push", method = GET, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_PROGRAM_PRODUCT')")
  public ResponseEntity<OpenLdrResponse> getAllPushPrograms() {
    return OpenLdrResponse.response(PROGRAMS, programService.getAllPushPrograms());
  }

  @RequestMapping(value = "/programs/{id}", method = GET, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'CONFIGURE_RNR, MANAGE_REGIMEN_TEMPLATE')")
  public ResponseEntity<OpenLdrResponse> get(@PathVariable Long id) {
    return OpenLdrResponse.response(PROGRAM, programService.getById(id));
  }

  @RequestMapping(value = "/programs", method = GET, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal, 'MANAGE_REGIMEN_TEMPLATE, MANAGE_USER, MANAGE_PRODUCT')")
  public ResponseEntity<OpenLdrResponse> getAllPrograms() {
    return OpenLdrResponse.response(PROGRAMS, programService.getAll());
  }

  @RequestMapping(value = "/programs/save", method = PUT, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal, 'MANAGE_REGIMEN_TEMPLATE, MANAGE_USER, MANAGE_PRODUCT')")
  public ResponseEntity<OpenLdrResponse> saveUpdates(@RequestBody Program program) {
    return OpenLdrResponse.response(PROGRAMS, programService.update(program));
  }

  @RequestMapping(value = "/facilities/{facilityId}/programsList", method = GET, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_PRODUCT_ALLOWED_FOR_FACILITY')")
  public ResponseEntity<OpenLdrResponse> getProgramsForFacilityCompleteList(@PathVariable(value = "facilityId") Long facilityId) {
      return OpenLdrResponse.response(PROGRAMS,programService.getByFacility(facilityId));
  }


}
