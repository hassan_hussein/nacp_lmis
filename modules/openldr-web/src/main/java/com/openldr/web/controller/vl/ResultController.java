package com.openldr.web.controller.vl;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.openldr.core.domain.Pagination;
import com.openldr.core.dto.AutomaticUploadResultDTO;
import com.openldr.core.exception.DataException;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.service.ResultService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;

import static com.openldr.core.web.OpenLdrResponse.error;
import static java.lang.Integer.parseInt;
import static org.springframework.http.HttpStatus.OK;

@RequestMapping(value = "/vl/result")
@Controller
@NoArgsConstructor
@Slf4j
public class ResultController extends BaseController {

    @Autowired
    private ResultService service;

    public static final String RESULT_STATUS="RESULTED";

    @Value("${search.page.size}")
    String limit;

    @RequestMapping(value = "/get-result", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getResultSamples(HttpServletRequest httpRequest,
                                                            @RequestParam(value = "page", defaultValue = "1") Integer page
                                                            ){
        Long userId=loggedInUserId(httpRequest);
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("results", service.getResultedSamples(userId,pagination));
        pagination.setTotalRecords(service.getTotalResulted(userId, RESULT_STATUS));
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }
    @RequestMapping(value = "/get-result-list", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getResultSamplesList(HttpServletRequest httpRequest,
                                                            @RequestParam(value = "page", defaultValue = "1") Integer page
                                                            ){
        Long userId=loggedInUserId(httpRequest);
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("results", service.getResultedSamplesList(userId,pagination));
        pagination.setTotalRecords(service.getTotalResulted(userId, RESULT_STATUS));
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-result-list-for-all", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getResultListSamples(HttpServletRequest httpRequest,
                                                            @RequestParam(value = "page", defaultValue = "1") Integer page
                                                            ){
        Long userId=loggedInUserId(httpRequest);
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("results", service.getResultedListSamples(userId,pagination));
        pagination.setTotalRecords(service.getTotalResulted(userId, RESULT_STATUS));
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/automaticUploadResult", method = RequestMethod.POST, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> automaticUploadResult(@RequestBody String resultDTO) {
        List<AutomaticUploadResultDTO> resultDTOs,results;
        resultDTOs = new Gson().fromJson(resultDTO, new TypeToken<ArrayList<AutomaticUploadResultDTO>>() {}.getType());
        results = new ArrayList<>();

        for(AutomaticUploadResultDTO result: resultDTOs){
            try{
                service.automaticUpload(result);
                results.add(result);
            }catch(DataException e){
                log.debug("Error saving results ",e);
            }
        }

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample", results);
        return openLdrResponse.response(OK);

//        try {
//            log.debug("",resultDTO);
//            service.automaticUpload(resultDTOs);
//            OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample", resultDTOs);
//            return openLdrResponse.response(OK);
//        } catch (DataException ex) {
//            log.debug("Error saving",ex);
//            OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample", resultDTOs);
//            return openLdrResponse.response(OK);
////             return error(ex, HttpStatus.CONFLICT);
//        }F

    }

    @RequestMapping(value = "/get-dispatched-result-list-for-hub", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getDispatchedResultsForHub(HttpServletRequest httpRequest,
                                                                @RequestParam(value = "page", defaultValue = "1") Integer page
    ){
        Long userId=loggedInUserId(httpRequest);
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("results", service.getDispatchedResultsForHub(userId,pagination));
        pagination.setTotalRecords(service.getTotalResultedHub(userId, RESULT_STATUS));
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }
}
