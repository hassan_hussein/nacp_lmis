package com.openldr.web.controller.labDashboard;

import com.openldr.core.domain.Facility;
import com.openldr.core.service.FacilityService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.service.LabDashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by hassan on 9/12/17.
 */
@Controller
@RequestMapping(value = "/lab-dashboard/")
public class LabDashboardController extends BaseController{
    @Autowired
    private LabDashboardService service;

    @Autowired
    FacilityService facilityService;

    @RequestMapping(value = "labs", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getRegions(HttpServletRequest httpRequest
    )  {
        return OpenLdrResponse.response("labs", service.getLaboratories());
    }

    @RequestMapping(value = "tat", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTAT( @RequestParam("startDate")String startDate,
                                                   @RequestParam("endDate")String endDate,
                                                   @RequestParam("lab")String lab,
                                                   HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _endDate = format.parse(endDate);
        Date _startDate = format.parse(startDate);
        lab=(lab.isEmpty())?"%":lab;
        return OpenLdrResponse.response("tat", service.getTAT(_startDate,_endDate,lab));
    }


    @RequestMapping(value = "getTAT/{{facilityId}}", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTAT(HttpServletRequest httpRequest,
                                                  @PathVariable Long facilityId
    ) {

        Long facId;
        if(facilityId == null){
        Facility facility = facilityService.getHomeFacility(loggedInUserId(httpRequest));
        facId = facility.getId();
        }else
        facId=facilityId;
        return OpenLdrResponse.response("tat", service.getTAT(facId));
    }

    @RequestMapping(value = "trendData", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTrendData(
            @RequestParam("startDate")String startDate,
            @RequestParam("endDate")String endDate,
            @RequestParam("lab")String lab,
            HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        lab=(lab.isEmpty())?"%":lab;
        return OpenLdrResponse.response("testTrends", service.getTrendData(startDate,endDate,lab));
    }


    @RequestMapping(value = "getByGender", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByGender(
            @RequestParam("startDate")String startDate,
            @RequestParam("endDate")String endDate,
            @RequestParam("lab")String lab,
            HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        lab=(lab.isEmpty())?"%":lab;
        return OpenLdrResponse.response("testByGender", service.getByGender(_startDate,_endDate,lab));
    }

    @RequestMapping(value = "getByAge", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByAge(
            @RequestParam("startDate")String startDate,
            @RequestParam("endDate")String endDate,
            @RequestParam("lab")String lab,
            HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        lab=(lab.isEmpty())?"%":lab;
        return OpenLdrResponse.response("testByAge", service.getByAge(_startDate,_endDate,lab));
    }

    @RequestMapping(value = "getByResult", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByResult(
            @RequestParam("startDate")String startDate,
            @RequestParam("endDate")String endDate,
            @RequestParam("lab")String lab
    ) throws ParseException {
        return process(startDate,endDate,lab,"BY-RESULT");
    }

    @RequestMapping(value = "getByJustification", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByJustification(
            @RequestParam("startDate")String startDate,
            @RequestParam("endDate")String endDate,
            @RequestParam("lab")String lab
    ) throws ParseException {
        return process(startDate,endDate,lab,"BY-JUSTIFICATION");
    }

    @RequestMapping(value = "getByLaboratories", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByLaboratories(
            @RequestParam("startDate")String startDate,
            @RequestParam("endDate")String endDate
    ) throws ParseException {
        return process(startDate,endDate,"","BY-LABORATORY");
    }

    private ResponseEntity<OpenLdrResponse> process(String startDate, String endDate, String lab, String type) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        lab=(lab.isEmpty())?"%":lab;
        return OpenLdrResponse.response("payload", service.load(_startDate,_endDate,lab,type));
    }




}
