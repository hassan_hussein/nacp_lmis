package com.openldr.web.controller.vl;

import com.openldr.core.exception.DataException;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.domain.Regimen;
import com.openldr.vl.service.RegimenService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping(value = "/vl/regimen")
@Controller
@NoArgsConstructor
public class RegimenController extends BaseController {

    @Autowired
    private RegimenService service;

    @RequestMapping(value = "/save", method = RequestMethod.PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody Regimen regimen) {
        try {
            service.save(regimen);

        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);
        }

        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("Successfully Saved", regimen.getName()));
        success.getBody().addData("status", regimen);
        return success;
    }

    @RequestMapping(value = "/getById", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimen", service.getById(id));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getAll", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById() {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimens", service.getAll());
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getByLine", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByLine(@Param("regimenLineId")Long regimenLineId) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimens", service.getByLine(regimenLineId));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/delete/{id}", method = DELETE, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> delete(@PathVariable Long id) {
        try {

            service.delete(id);
        } catch (DataException e) {
            //throw new DataException("error.mandatory.fields.missing");
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }

        return success(messageService.message("Successful Deleted"));

    }

    @RequestMapping(value = "/getAllInFull", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllInFull() {
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimens", service.getAllInFull());
        return openLdrResponse.response(OK);
    }

   @RequestMapping(value = "/getAllInFullById/{id}", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getBy(@Param("id") Long id) {
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimens", service.geAllInFullById(id));
        return openLdrResponse.response(OK);
    }

}
