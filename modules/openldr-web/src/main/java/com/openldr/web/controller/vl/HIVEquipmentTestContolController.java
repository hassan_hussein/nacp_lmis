package com.openldr.web.controller.vl;

import com.openldr.core.exception.DataException;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.domain.HIVEquipmentTestControl;
import com.openldr.vl.service.HIVEquipmentTestControlService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping(value = "/vl/equipmentTestControl")
@Controller
@NoArgsConstructor
public class HIVEquipmentTestContolController extends BaseController {

    @Autowired
    private HIVEquipmentTestControlService service;

    @RequestMapping(value = "/save", method = RequestMethod.PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody HIVEquipmentTestControl control) {
        try {
            service.save(control);
            return success("status","Set saved" );

        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/getById", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("set", service.getById(id));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getAll", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById() {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sets", service.getAll());
        return openLdrResponse.response(OK);
    }


}
