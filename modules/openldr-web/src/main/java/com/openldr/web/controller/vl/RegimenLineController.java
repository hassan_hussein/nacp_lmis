package com.openldr.web.controller.vl;

import com.openldr.core.exception.DataException;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.domain.RegimenLine;
import com.openldr.vl.service.RegimenLineService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping(value = "/vl/regimenLine")
@Controller
@NoArgsConstructor
public class RegimenLineController extends BaseController {

    @Autowired
    private RegimenLineService service;

    @RequestMapping(value = "/save", method = RequestMethod.PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody RegimenLine regimenLine) {
        try {
            service.save(regimenLine);

        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);
        }

        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("Successifully Saved", regimenLine.getName()));
        success.getBody().addData("status", regimenLine);
        return success;
    }

    @RequestMapping(value = "/getById", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimenLine", service.getById(id));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getAll", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById() {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimenLines", service.getAll());
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getByGroup", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByGroup(@Param("regimenGroupId") Long regimenGroupId) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("regimenLines", service.getByGroup(regimenGroupId));
        return openLdrResponse.response(OK);
    }


}
