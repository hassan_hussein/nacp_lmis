package com.openldr.web.controller.vl;

import com.openldr.core.exception.DataException;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.core.domain.HIVTestType;
import com.openldr.core.service.HIVTestTypeService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping(value = "/vl/testType")
@Controller
@NoArgsConstructor
public class HIVTestTypeController extends BaseController {

    @Autowired
    private HIVTestTypeService service;

    @RequestMapping(value = "/save", method = RequestMethod.PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody HIVTestType type) {
        try {
            service.save(type);
            return success("status","Test type saved" );

        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/getById", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testType", service.getById(id));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getAll", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById() {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testTypes", service.getAll());
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/setDefault", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> setDefault(@RequestParam("id") Long id) {

        service.setDefault(id);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testTypes",service.getAll());
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/setFacilityDefault", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> setFacilityDefault(@RequestParam("id") Long id,HttpServletRequest httpServletRequest) {

        Long userId=loggedInUserId(httpServletRequest);
        service.setFacilityDefault(userId,id);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testType",service.getFacilityDefault(userId));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/getFacilityDefault", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getFacilityDefault(HttpServletRequest httpServletRequest) {

        Long userId=loggedInUserId(httpServletRequest);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("testType",service.getFacilityDefault(userId));
        return openLdrResponse.response(OK);
    }


}
