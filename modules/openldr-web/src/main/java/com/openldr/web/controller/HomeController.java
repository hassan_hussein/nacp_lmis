package com.openldr.web.controller;

/**
 * Created by hassan on 4/17/16.
 */

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.response;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;


/**
 * This controller handles endpoint related list locales, also to change the current locale.
 */

@Controller
public class HomeController extends BaseController {

    @RequestMapping(value = "", method = GET)
    public String homeDefault() {
        return homePageUrl();
    }

    @RequestMapping(value = "/locales", method = GET)
    public ResponseEntity<OpenLdrResponse> getLocales(HttpServletRequest request) {
        messageService.setCurrentLocale(RequestContextUtils.getLocale(request));
        return response("locales", messageService.getLocales());
    }

    @RequestMapping(value = "/changeLocale", method = PUT, headers = ACCEPT_JSON)
    public void changeLocale(HttpServletRequest request) {
        messageService.setCurrentLocale(RequestContextUtils.getLocale(request));
    }
}
