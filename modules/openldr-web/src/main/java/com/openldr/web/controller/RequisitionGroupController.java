package com.openldr.web.controller;

import com.openldr.core.domain.Pagination;
import com.openldr.core.domain.RequisitionGroup;
import com.openldr.core.domain.RequisitionGroupMember;
import com.openldr.core.domain.RequisitionGroupProgramSchedule;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.RequisitionGroupProgramScheduleService;
import com.openldr.core.service.RequisitionGroupService;
import com.openldr.core.service.StaticReferenceDataService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.web.form.RequisitionGroupFormDTO;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * This controller handles endpoint related to GET, PUT, POST and other operations on RequisitionGroup.
 */

@Controller
@NoArgsConstructor
public class RequisitionGroupController extends BaseController {

  public static final String SEARCH_PAGE_SIZE = "search.page.size";

  @Autowired
  private RequisitionGroupService requisitionGroupService;

  @Autowired
  private StaticReferenceDataService staticReferenceDataService;

  @Autowired
  private RequisitionGroupProgramScheduleService requisitionGroupProgramScheduleService;

  @RequestMapping(value = "/requisitionGroups", method = GET)
//  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_REQUISITION_GROUP')")
  public ResponseEntity<OpenLdrResponse> search(@RequestParam(value = "searchParam") String searchParam,
                                                 @RequestParam(value = "columnName") String columnName,
                                                 @RequestParam(value = "page", defaultValue = "1") Integer page) {
    Integer pageSize = Integer.parseInt(staticReferenceDataService.getPropertyValue(SEARCH_PAGE_SIZE));
    Pagination pagination = new Pagination(page, pageSize);

    List<RequisitionGroup> requisitionGroupList = requisitionGroupService.search(searchParam, columnName, pagination);
    pagination.setTotalRecords(requisitionGroupService.getTotalRecords(searchParam, columnName));

    ResponseEntity<OpenLdrResponse> response = OpenLdrResponse.response("requisitionGroupList", requisitionGroupList);
    response.getBody().addData("pagination", pagination);
    return response;
  }

  @RequestMapping(value = "/requisitionGroups/{id}", method = GET)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_REQUISITION_GROUP')")
  public ResponseEntity<OpenLdrResponse> getById(@PathVariable(value = "id") Long id) {
    RequisitionGroup requisitionGroup = requisitionGroupService.getBy(id);
    List<RequisitionGroupMember> requisitionGroupMembers = requisitionGroupService.getMembersBy(id);
    List<RequisitionGroupProgramSchedule> requisitionGroupProgramSchedules = requisitionGroupProgramScheduleService.getByRequisitionGroupId(
      id);
    RequisitionGroupFormDTO requisitionGroupFormDTO = new RequisitionGroupFormDTO(requisitionGroup,
      requisitionGroupMembers, requisitionGroupProgramSchedules);
    return OpenLdrResponse.response("requisitionGroupData", requisitionGroupFormDTO);
  }

  @RequestMapping(value = "/requisitionGroups", method = POST, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_REQUISITION_GROUP')")
  public ResponseEntity<OpenLdrResponse> insert(@RequestBody RequisitionGroupFormDTO requisitionGroupFormDTO,
                                                 HttpServletRequest request) {
    try {
      Long userId = loggedInUserId(request);
      requisitionGroupService.saveWithMembersAndSchedules(requisitionGroupFormDTO.getRequisitionGroup(),
        requisitionGroupFormDTO.getRequisitionGroupMembers(),null
        /*requisitionGroupFormDTO.getRequisitionGroupProgramSchedules()*/, userId);
    } catch (DataException e) {
      return error(e, BAD_REQUEST);
    }

    ResponseEntity<OpenLdrResponse> responseEntity = success(
      messageService.message("message.requisition.group.created.success",
        requisitionGroupFormDTO.getRequisitionGroup().getName()));
    responseEntity.getBody().addData("requisitionGroupId", requisitionGroupFormDTO.getRequisitionGroup().getId());
    return responseEntity;
  }

  @RequestMapping(value = "/requisitionGroups/{id}", method = PUT, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_REQUISITION_GROUP')")
  public ResponseEntity<OpenLdrResponse> update(@PathVariable(value = "id") Long id,
                                                 @RequestBody RequisitionGroupFormDTO requisitionGroupFormDTO,
                                                 HttpServletRequest request) {
    try {
      Long userId = loggedInUserId(request);
      requisitionGroupFormDTO.getRequisitionGroup().setId(id);
      requisitionGroupService.updateWithMembersAndSchedules(requisitionGroupFormDTO.getRequisitionGroup(),
        requisitionGroupFormDTO.getRequisitionGroupMembers(),
        requisitionGroupFormDTO.getRequisitionGroupProgramSchedules(), userId);

    } catch (DataException e) {
      return error(e, BAD_REQUEST);
    }
    ResponseEntity<OpenLdrResponse> responseEntity = success(
      messageService.message("message.requisition.group.updated.success",
        requisitionGroupFormDTO.getRequisitionGroup().getName()));
    responseEntity.getBody().addData("requisitionGroupId", requisitionGroupFormDTO.getRequisitionGroup().getId());
    return responseEntity;
  }

}
