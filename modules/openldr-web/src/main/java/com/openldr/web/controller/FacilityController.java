package com.openldr.web.controller;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.FacilityType;
import com.openldr.core.domain.Pagination;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.FacilityService;
import com.openldr.core.service.ProgramService;
import com.openldr.core.service.RequisitionGroupService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.web.model.FacilityReferenceData;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.openldr.core.domain.Facility.createFacilityToBeDeleted;
import static com.openldr.core.domain.Facility.createFacilityToBeRestored;
import static com.openldr.core.web.OpenLdrResponse.response;
import static com.openldr.core.web.OpenLdrResponse.success;
import static java.lang.Integer.parseInt;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * This controller handles endpoint related to create, get, update, disable facility, also has endpoints to return home facility,
 * supervised facility for a user.
 */

@Controller
@NoArgsConstructor
public class FacilityController extends BaseController {

  public static final String FACILITIES = "facilities";
  @Autowired
  private FacilityService facilityService;

  @Autowired
  private ProgramService programService;

  @Autowired
  RequisitionGroupService requisitionGroupService;

  @RequestMapping(value = "/facilities", method = GET, headers = ACCEPT_JSON)
//  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_FACILITY, MANAGE_USER')")
  public ResponseEntity<OpenLdrResponse> get(@RequestParam(value = "searchParam", required = false) String searchParam,
                                              @RequestParam(value = "columnName") String columnName,
                                              @RequestParam(value = "page", defaultValue = "1") Integer page,
                                              @Value("${search.page.size}") String limit) {
    Pagination pagination = new Pagination(page, parseInt(limit));
    pagination.setTotalRecords(facilityService.getTotalSearchResultCountByColumnName(searchParam, columnName));
    List<Facility> facilities = facilityService.searchBy(searchParam, columnName, pagination);
    ResponseEntity<OpenLdrResponse> response = response(FACILITIES, facilities);
    response.getBody().addData("pagination", pagination);
    return response;
  }

  @RequestMapping(value = "/filter-facilities", method = GET, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_FACILITY, MANAGE_SUPERVISORY_NODE, MANAGE_REQUISITION_GROUP, MANAGE_SUPPLY_LINE, MANAGE_USER')")
  public ResponseEntity<OpenLdrResponse> getFilteredFacilities(@RequestParam(value = "searchParam", required = false) String searchParam,
                                                                @RequestParam(value = "facilityTypeId", required = false) Long facilityTypeId,
                                                                @RequestParam(value = "geoZoneId", required = false) Long geoZoneId,
                                                                @RequestParam(value = "virtualFacility", required = false) Boolean virtualFacility,
                                                                @RequestParam(value = "enabled", required = false) Boolean enabled,
                                                                @Value("${search.results.limit}") String facilitySearchLimit) {
    Integer count = facilityService.getFacilitiesCountBy(searchParam, facilityTypeId, geoZoneId, virtualFacility, enabled);
    if (count <= Integer.parseInt(facilitySearchLimit)) {
      List<Facility> facilities = facilityService.searchFacilitiesBy(searchParam, facilityTypeId, geoZoneId, virtualFacility, enabled);
      return response("facilityList", facilities);
    } else {
      return response("message", "too.many.results.found");
    }
  }

  @RequestMapping(value = "/user/facilities", method = GET)
  public List<Facility> getHomeFacility(HttpServletRequest httpServletRequest) {
    return Arrays.asList(facilityService.getHomeFacility(loggedInUserId(httpServletRequest)));
  }

  @RequestMapping(value = "/facilities/reference-data", method = GET)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_FACILITY')")
  public Map getFacilityReferenceDataReferenceData() {
   FacilityReferenceData  facilityReferenceData = new FacilityReferenceData();
    return facilityReferenceData.addFacilityTypes(facilityService.getAllTypes()).
      addFacilityOperators(facilityService.getAllOperators()).
      addGeographicZones(facilityService.getAllZones()).
      addPrograms(programService.getAll()).get();
  }

  private ModelMap getFacilityResponse(@PathVariable(value = "programId") Long programId, Long userId, String... rights) {
    ModelMap modelMap = new ModelMap();
    List<Facility> facilities = facilityService.getUserSupervisedFacilities(userId, programId, rights);
    modelMap.put("facilities", facilities);
    return modelMap;
  }

  @RequestMapping(value = "/facilities/{id}", method = GET, headers = ACCEPT_JSON)
  @PreAuthorize("@permissionEvaluator.hasPermission(principal, 'MANAGE_FACILITY, MANAGE_USER')")
  public ResponseEntity<OpenLdrResponse> getFacility(@PathVariable(value = "id") Long id) {
    return response("facility", facilityService.getById(id));
  }

 /* @RequestMapping(value = "/create/requisition/supervised/{programId}/facilities.json", method = GET)
  public ResponseEntity<ModelMap> getUserSupervisedFacilitiesSupportingProgram(@PathVariable(value = "programId") Long programId,
                                                                               HttpServletRequest request) {
    Long userId = loggedInUserId(request);
    return new ResponseEntity<>(getFacilityResponse(programId, userId,  CREATE_REQUISITION,  AUTHORIZE_REQUISITION), HttpStatus.OK);
  }*/



/*
  @RequestMapping(value = "/manage-pod/supervised/{programId}/facilities.json", method = GET)
  public ResponseEntity<ModelMap> getUserSupervisedFacilitiesForPOD(@PathVariable(value = "programId") Long programId,
                                                                               HttpServletRequest request) {
    Long userId = loggedInUserId(request);
    ModelMap modelMap = new ModelMap();
    List<Facility> facilities = facilityService.getUserSupervisedFacilities(userId, programId, MANAGE_POD,  COMPLETE_POD);
    Facility homeFacility = facilityService.getHomeFacilityForRights(userId, programId, COMPLETE_POD);
    if(homeFacility != null){
      facilities.add(homeFacility);
      facilities = new ArrayList<>(new HashSet<>(facilities));
    }
    modelMap.put("facilities", facilities);
    return new ResponseEntity<>(modelMap, HttpStatus.OK);
  }
*/


/*  @RequestMapping(value = "/users/{userId}/supervised/{programId}/facilities.json", method = GET)
  public ResponseEntity<ModelMap> getUserSupervisedFacilitiesSupportingProgram(@PathVariable(
        value = "programId") Long programId, @PathVariable("userId") Long userId) {
    return new ResponseEntity<>(getFacilityResponse(programId, userId, CREATE_REQUISITION, AUTHORIZE_REQUISITION), HttpStatus.OK);
  }*/

  @RequestMapping(value = "/facilities", method = POST, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_FACILITY')")
  public ResponseEntity insert(@RequestBody Facility facility, HttpServletRequest request) {
    facility.setCreatedBy(loggedInUserId(request));
    facility.setModifiedBy(loggedInUserId(request));
    ResponseEntity<OpenLdrResponse> response;
    try {
        System.out.println("save facility");
        System.out.println(facility);
      facilityService.update(facility);
    } catch (DataException exception) {
      return createErrorResponse(facility, exception);
    }
    response = success(messageService.message("message.facility.created.success", facility.getName()));
    response.getBody().addData("facility", facility);
    return response;
  }

  @RequestMapping(value = "/facilities/{id}", method = PUT, headers = ACCEPT_JSON)
//  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_FACILITY')")
  public ResponseEntity<OpenLdrResponse> update(@PathVariable("id") Long id,
                                                 @RequestBody Facility facility,
                                                 HttpServletRequest request) {
    facility.setId(id);
    facility.setModifiedBy(loggedInUserId(request));

    try {
      facilityService.update(facility);
    } catch (DataException exception) {
      return createErrorResponse(facility, exception);
    }

    String successMessage = messageService.message("message.facility.updated.success", facility.getName());
    OpenLdrResponse openLmisResponse = new OpenLdrResponse("facility", facility);
    return openLmisResponse.successEntity(successMessage);
  }

  @RequestMapping(value = "/user/facilities/view", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> listForViewing(HttpServletRequest request) {
    return response("facilities",
      facilityService.getForUserAndRights(loggedInUserId(request),  new String[]{} /*VIEW_REQUISITION*/));
  }

  @RequestMapping(value = "/facilities/{facilityId}", method = DELETE, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_FACILITY')")
  public ResponseEntity<OpenLdrResponse> softDelete(HttpServletRequest httpServletRequest,
                                                     @PathVariable Long facilityId) {
    Facility facilityToBeDeleted = createFacilityToBeDeleted(facilityId, loggedInUserId(httpServletRequest));
    facilityService.updateEnabledAndActiveFor(facilityToBeDeleted);
    Facility deletedFacility = facilityService.getById(facilityId);

    String successMessage = messageService.message("disable.facility.success", deletedFacility.getName(), deletedFacility.getCode());
    OpenLdrResponse response = new OpenLdrResponse("facility", deletedFacility);
    return response.successEntity(successMessage);
  }

  @RequestMapping(value = "/facilities/{id}/restore", method = PUT, headers = ACCEPT_JSON)
 // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_FACILITY')")
  public ResponseEntity<OpenLdrResponse> restore(HttpServletRequest request,
                                                  @PathVariable("id") Long facilityId) {
    Facility facilityToBeDeleted = createFacilityToBeRestored(facilityId, loggedInUserId(request));

    facilityService.updateEnabledAndActiveFor(facilityToBeDeleted);

    Facility restoredFacility = facilityService.getById(facilityId);

    String successMessage = messageService.message("enable.facility.success", restoredFacility.getName(),
      restoredFacility.getCode());

    OpenLdrResponse response = new OpenLdrResponse("facility", restoredFacility);
    return response.successEntity(successMessage);
  }

  @RequestMapping(value = "/deliveryZones/{deliveryZoneId}/programs/{programId}/facilities", method = GET, headers = ACCEPT_JSON)
//  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_DISTRIBUTION')")
  public ResponseEntity<OpenLdrResponse> getFacilitiesForDeliveryZoneAndProgram(@PathVariable("deliveryZoneId") Long deliveryZoneId,
                                                                                 @PathVariable("programId") Long programId) {
    List<Facility> facilities = facilityService.getAllForDeliveryZoneAndProgram(deliveryZoneId, programId);
    return response("facilities", Facility.filterForActiveProducts(facilities));
  }

  @RequestMapping(value = "/enabledWarehouses", method = GET, headers = ACCEPT_JSON)
//  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
  public ResponseEntity<OpenLdrResponse> getEnabledWarehouses() {
    List<Facility> enabledWarehouses = facilityService.getEnabledWarehouses();
    return response("enabledWarehouses", enabledWarehouses);
  }

  @RequestMapping(value = "/facility-types", method = GET, headers = ACCEPT_JSON)
//  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_FACILITY, MANAGE_SUPERVISORY_NODE, MANAGE_REQUISITION_GROUP, MANAGE_SUPPLY_LINE, MANAGE_FACILITY_APPROVED_PRODUCT, MANAGE_USER')")
  public List<FacilityType> getFacilityTypes() {
    return facilityService.getAllTypes();
  }

  private ResponseEntity<OpenLdrResponse> createErrorResponse(Facility facility, DataException exception) {
    OpenLdrResponse openLmisResponse = new OpenLdrResponse("facility", facility);
    return openLmisResponse.errorEntity(exception, BAD_REQUEST);
  }

  @RequestMapping(value = "/facility-contacts", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> getContactsForFacility(@RequestParam("type") String type, @RequestParam("facilityId") Long facilityId) {
    return OpenLdrResponse.response("contacts",facilityService.getContactList(facilityId, type));
  }

    @RequestMapping(value = "/facility-supervisors", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getFacilitySupervisors(@RequestParam("facilityId") Long facilityId) {
        return OpenLdrResponse.response("supervisors",facilityService.getFacilitySupervisors(facilityId));
    }
  
  @RequestMapping(value = "/facilityType/{facilityTypeId}/requisitionGroup/{requisitionGroupId}/facilities", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> getFacilityByTypeAndRequisitionGroupId(@PathVariable("facilityTypeId") Long facilityTypeId, @PathVariable("requisitionGroupId") Long requisitionGroupId) {
    return OpenLdrResponse.response("facilities",facilityService.getFacilityByTypeAndRequisitionGroupId(facilityTypeId, requisitionGroupId));
  }

    @RequestMapping(value = "/geoFacilityTree", method = GET)
    public ResponseEntity<OpenLdrResponse> getGeoTreeFacility(HttpServletRequest httpServletRequest) {

        ResponseEntity<OpenLdrResponse> response;
        response = OpenLdrResponse.success("");
        response.getBody().addData("regionFacilityTree", facilityService.getGeoRegionFacilityTree(loggedInUserId(httpServletRequest)));
        response.getBody().addData("districtFacility", facilityService.getGeoDistrictFacility(loggedInUserId(httpServletRequest)));
        response.getBody().addData("flatFacility", facilityService.getGeoFlatFacilityTree(loggedInUserId(httpServletRequest)));
        return response;
    }


    @RequestMapping(value = "facility/search-facility", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getFacilities(@RequestParam("query") String query,
                                                         HttpServletRequest httpServletRequest) {
        Long userId=loggedInUserId(httpServletRequest);
        return OpenLdrResponse.response("facilities",facilityService.searchFacilities(userId,query));
    }

  @RequestMapping(value = "lab-list", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllLabs(@RequestParam("query") String query,
                                                         HttpServletRequest httpServletRequest) {
        return OpenLdrResponse.response("labs",facilityService.getAllLabs());
    }

  @RequestMapping(value = "facility-list", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllFacilitiesForExport(HttpServletRequest httpServletRequest) {
        return OpenLdrResponse.response("facilityList",facilityService.getAllFacilitiesForExport());
    }
}
