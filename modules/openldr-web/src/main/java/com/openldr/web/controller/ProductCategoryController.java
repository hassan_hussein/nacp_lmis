package com.openldr.web.controller;


import com.openldr.core.domain.ProductCategory;
import com.openldr.core.service.ProductCategoryService;
import com.openldr.core.web.OpenLdrResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Created by chrispinus on 3/15/16.
 */
@RequestMapping(value = "/products/category")
@Controller
public class ProductCategoryController {

    private static Logger logger = LoggerFactory.getLogger(ProductCategoryController.class);
    @Autowired
    ProductCategoryService service;

    @RequestMapping(value = "/get-all", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<ProductCategory> getAll() {
        return service.getAll();
    }

    @RequestMapping(value = "save", method = RequestMethod.PUT, consumes = "application/json")
    public
    @ResponseBody
    ProductCategory save(@RequestBody ProductCategory productCategory, HttpServletRequest request) {
        //logger.info("Creating a new product category "+request.getSession());
        service.save(productCategory);

        return service.getById(productCategory.getId());
    }

    @RequestMapping(value = "update", method = RequestMethod.PUT, headers = "Accept=application/json")
    public ResponseEntity<OpenLdrResponse> update(@RequestBody ProductCategory productCategory) {
        if (productCategory.getId() != null) {
            service.save(productCategory);
            return OpenLdrResponse.response("category", service.getById(productCategory.getId()));
        } else
            return OpenLdrResponse.response(null, null, HttpStatus.BAD_REQUEST);
    }


}
