package com.openldr.web;


/*import com.openldr.message.service.OnlineUserService;*/
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by chrispinus on 7/29/16.
 */

@Service
public class SessionEndedListenerService implements HttpSessionListener {


    @Override
    public void sessionCreated(HttpSessionEvent se) {
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

        updateUser(se);
    }

    private void updateUser(HttpSessionEvent sessionEvent) {

        Long id = Long.parseLong(sessionEvent.getSession().getAttribute("USER_ID").toString());
        HttpSession session = sessionEvent.getSession();

        ApplicationContext ctx =
                WebApplicationContextUtils.
                        getWebApplicationContext(session.getServletContext());

      /*OnlineUserService OnlineUserService onlineUserService =
                (OnlineUserService) ctx.getBean("onlineUserService");

        onlineUserService.updateLogoutUser(id, "OFFLINE");*/
    }


}