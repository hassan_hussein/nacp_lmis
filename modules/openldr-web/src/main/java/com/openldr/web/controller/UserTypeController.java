package com.openldr.web.controller;

import com.openldr.core.domain.UserType;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.UserTypeService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;

@RequestMapping(value = "/userType")
@Controller
public class UserTypeController extends BaseController {

    private static final String UserTypes = "AllUserTypes";
    private static final String GetAllByID = "allById";

    @Autowired
    private UserTypeService service;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll(HttpServletRequest request) {
        return OpenLdrResponse.response("users", service.getAll());
    }


    @RequestMapping(value = "/get-all-by-ID", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllById(HttpServletRequest request, UserType userType) {
        return OpenLdrResponse.response(GetAllByID, service.getById(1L));
    }

    @RequestMapping(value = "userType", method = RequestMethod.POST, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> create(@RequestBody UserType type, HttpServletRequest request) {
        try {
            service.Insert(type);
            return success(messageService.message("message.user.type.created.success", type));
        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);

        }

    }


}
