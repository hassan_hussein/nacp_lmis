package com.openldr.web.controller.mainDashboard;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.service.MainDashBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by hassan on 9/3/17.
 */

@Controller
@RequestMapping(value = "/main-dashboard/")
public class MainDashboardController extends BaseController {

    @Autowired
    private MainDashBoardService service;

    @RequestMapping(value = "getTAT", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTAT( @RequestParam("startDate")String startDate,
                                                   @RequestParam("endDate")String endDate,
                                                   @RequestParam("region")String region,
                                                   HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        region=(region.isEmpty())?"%":region;
       return OpenLdrResponse.response("tat", service.getTAT(_startDate,_endDate,region));
    }

    @RequestMapping(value = "getSampleTypesByYear/{year}", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTAT(@PathVariable("year")Long year,
            HttpServletRequest httpRequest
    ) {
        return OpenLdrResponse.response("sampleTypes", service.getSampleTypesByYear(year));
    }

    @RequestMapping(value = "getTrendData", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTrendData(
                                                  @RequestParam("startDate")String startDate,
                                                  @RequestParam("endDate")String endDate,
                                                  @RequestParam("region")String region,
                                                  HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        region=(region.isEmpty())?"%":region;
        return OpenLdrResponse.response("testTrends", service.getTrendData(startDate,endDate,region));
//        return OpenLdrResponse.response("testTrends", _startDate);
    }
    @RequestMapping(value = "getByGender", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByGender(
                                                  @RequestParam("startDate")String startDate,
                                                  @RequestParam("endDate")String endDate,
                                                  @RequestParam("region")String region,
                                                  HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        region=(region.isEmpty())?"%":region;
        return OpenLdrResponse.response("testByGender", service.getByGender(_startDate,_endDate,region));
    }

    @RequestMapping(value = "getByAge", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByAge(
                                                  @RequestParam("startDate")String startDate,
                                                  @RequestParam("endDate")String endDate,
                                                  @RequestParam("region")String region,
                                                  HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        region=(region.isEmpty())?"%":region;
        return OpenLdrResponse.response("testByAge", service.getByAge(_startDate,_endDate,region));
    }
    @RequestMapping(value = "getByResult", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByResult(
                                                  @RequestParam("startDate")String startDate,
                                                  @RequestParam("endDate")String endDate,
                                                  @RequestParam("region")String region,
                                                  HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        region=(region.isEmpty())?"%":region;
        return OpenLdrResponse.response("byResult", service.getByResult(_startDate,_endDate,region));
    }
    @RequestMapping(value = "getByJustification", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByJustification(
                                                  @RequestParam("startDate")String startDate,
                                                  @RequestParam("endDate")String endDate,
                                                  @RequestParam("region")String region,
                                                  HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        region=(region.isEmpty())?"%":region;
        return OpenLdrResponse.response("byJustification", service.getByJustification(_startDate,_endDate,region));
    }

  @RequestMapping(value = "getByRegion", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByRegion(
                                                  @RequestParam("startDate")String startDate,
                                                  @RequestParam("endDate")String endDate,
                                                  @RequestParam("region")String region,
                                                  HttpServletRequest httpRequest
    ) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date _startDate = format.parse(startDate);
        Date _endDate = format.parse(endDate);
        region=(region.isEmpty())?"%":region;
        return OpenLdrResponse.response("byRegion", service.getByRegion(_startDate,_endDate,region));
    }

    @RequestMapping(value = "getRegions", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getRegions(HttpServletRequest httpRequest
    ) throws ParseException {

        return OpenLdrResponse.response("regions", service.getRegions());
    }

}
