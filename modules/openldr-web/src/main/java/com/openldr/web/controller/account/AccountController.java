
package com.openldr.web.controller.account;

import com.openldr.account.domain.Account;
import com.openldr.account.service.AccountService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@NoArgsConstructor
@RequestMapping(value = "/account")
public class AccountController extends BaseController {

    @Autowired
    AccountService service;

    @RequestMapping(value = "/save", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody Account account, HttpServletRequest request) {
        Long userId = loggedInUserId(request);
        service.createAccount(account, userId);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("Account", account);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/types", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAccountTypes(HttpServletRequest request) {
        return OpenLdrResponse.response("types", service.getAccountType());
    }



    @RequestMapping(value = "/get-by-reference-number", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByUserId(@Param("referenceNumber") String referenceNumber) {

        Account account = service.getByReferenceNumber(referenceNumber);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("account", account);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-members", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> searchMembers(@Param("query") String query) {

        OpenLdrResponse openLdrResponse = new OpenLdrResponse("members", service.searchMembers(query));
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/send-invoice-sms", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> sendInvoiceSmS(@Param("referenceNumber") String referenceNumber) {

        service.sendInvoiceSMS(referenceNumber);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sms", "SUCCESSFUL");
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/send-pin-sms", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> sendPinSmS(@Param("referenceNumber") String referenceNumber) {

        service.sendAllMembersPin(referenceNumber);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("sms", "SUCCESSFUL");
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/top-tiles", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTopTiles() {
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("topTiles", service.getTopTiles());
        return openLdrResponse.response(OK);
    }



}
