
package com.openldr.web.controller;

import com.openldr.core.domain.Pagination;
import com.openldr.core.domain.User;
import com.openldr.core.domain.UserLandingPage;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.*;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.openldr.authentication.web.UserAuthenticationSuccessHandler.USER;
import static com.openldr.authentication.web.UserAuthenticationSuccessHandler.USER_ID;
import static com.openldr.core.domain.RightName.MANAGE_USER;
import static com.openldr.core.web.OpenLdrResponse.*;
import static java.lang.Boolean.TRUE;
import static java.lang.Integer.numberOfLeadingZeros;
import static java.lang.Integer.parseInt;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * This controller handles endpoints to related to user management like resetting password, disabling a user, etc.
 */

@Controller
@NoArgsConstructor
public class UserController extends BaseController {


  public static final String TOKEN_VALID = "TOKEN_VALID";
  public static final String USERS = "userList";
  static final String MSG_USER_DISABLE_SUCCESS = "msg.user.disable.success";
  static final String USER_CREATED_SUCCESS_MSG = "message.user.created.success.email.sent";
  private static final String RESET_PASSWORD_PATH = "/public/pages/reset-password.html#/token/";
  @Value("${mail.base.url}")
  public String baseUrl;
  @Autowired
  private RoleRightsService roleRightService;
  @Autowired
  private UserService userService;
  @Autowired
  private SessionRegistry sessionRegistry;
  @Autowired
  private ConfigurationSettingService settingService;

  @Autowired
  private UserGroupService userGroupService;

  @Autowired
  private UserLandingPageService landingPageService;

  @RequestMapping(value = "/user-context", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> user(HttpServletRequest httpServletRequest) {
    Long userId = (Long) httpServletRequest.getSession().getAttribute(USER_ID);
    if (userId != null) {

      String userName = (String) httpServletRequest.getSession().getAttribute(USER);

      OpenLdrResponse openLdrResponse = new OpenLdrResponse("name", userName);
      openLdrResponse.addData("authenticated", TRUE);
      openLdrResponse.addData("userId", userId);
      openLdrResponse.addData("loggedInUser", loggedInUserId(httpServletRequest));
      openLdrResponse.addData("rights", roleRightService.getRights(userId));
      openLdrResponse.addData("users", userService.getById(userId));
      /*openLdrResponse.addData("preferences", userService.getPreferences(userId));*/
      // openLdrResponse.addData("preferences", userService.getPreferences(userId));
      UserLandingPage page = landingPageService.getLandingPageById(userService.getById(userId).getLandingPageId());
      String landingPage = (page != null) ? page.getUrl() : null;
      String homePage = "/public/pages/vl/index.html/#/list-samples";
      openLdrResponse.addData("homePage", homePage);
      //String homePage = "/public/pages/consultation/index.html#/search";
      // settingService.getConfigurationStringValue(ConfigurationSettingKey.LOGIN_SUCCESS_DEFAULT_LANDING_PAGE)
      return openLdrResponse.response(OK);
    } else {
      return authenticationError();
    }
  }


  @RequestMapping(value = "/authentication-error", method = POST, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> authenticationError() {
    return error(messageService.message("user.login.error"), UNAUTHORIZED);
  }

  @RequestMapping(value = "/forgot-password", method = POST, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> sendPasswordTokenEmail(@RequestBody User user) {
    try {
      String resetPasswordLink = baseUrl + RESET_PASSWORD_PATH;
      userService.sendForgotPasswordEmail(user, resetPasswordLink);
      return success(messageService.message("email.sent"));
    } catch (DataException e) {
      return error(e, BAD_REQUEST);
    }
  }

  @RequestMapping(value = "/users", method = POST, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
  public ResponseEntity<OpenLdrResponse> create(@RequestBody User user, HttpServletRequest request) {
    user.setCreatedBy(loggedInUserId(request));
    user.setModifiedBy(loggedInUserId(request));
    try {
      String resetPasswordBaseLink = baseUrl + RESET_PASSWORD_PATH;
      userService.clearcreate(user, resetPasswordBaseLink);
    } catch (DataException e) {
      return error(e, BAD_REQUEST);
    }
    ResponseEntity<OpenLdrResponse> success = success(USER_CREATED_SUCCESS_MSG);
    success.getBody().addData("user", user);
    return success;
  }


  @RequestMapping(value = "/users/{id}", method = PUT, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
  public ResponseEntity<OpenLdrResponse> update2(@RequestBody User user,
                                                 @PathVariable("id") Long id,
                                                 HttpServletRequest request) {
    user.setModifiedBy(loggedInUserId(request));
    user.setId(id);
    try {
      userService.update(user);
    } catch (DataException e) {
      return error(e, BAD_REQUEST);
    }
    return new OpenLdrResponse().response(OK);
  }

  @RequestMapping(value = "/userProfile/{id}", method = PUT, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> update(@RequestBody User user,
                                                @PathVariable("id") Long id,
                                                HttpServletRequest request) {
    user.setModifiedBy(loggedInUserId(request));
    user.setId(id);

    try {
      userService.updateUserProfile(user);
    } catch (DataException e) {
      return error(e, BAD_REQUEST);
    }

    ResponseEntity<OpenLdrResponse> success = success("Created Successfully");
    success.getBody().addData("user", user);
    return success;

  }

  @RequestMapping(value = "/users", method = GET)
//  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
  public ResponseEntity<OpenLdrResponse> searchUser(@RequestParam(value = "searchParam", required = false) String searchParam,
                                                    @RequestParam(value = "page", defaultValue = "1") Integer page,
                                                    @Value("${search.page.size}") String limit) {

    Pagination pagination = new Pagination(page, parseInt(limit));
    pagination.setTotalRecords(userService.getTotalSearchResultCount(searchParam));
    List<User> users = userService.searchUser(searchParam, pagination);
    ResponseEntity<OpenLdrResponse> response = OpenLdrResponse.response(USERS, users);
    response.getBody().addData("pagination", pagination);
    return response;
  }

  @RequestMapping(value = "/users/{id}", method = GET)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
  public User get(@PathVariable(value = "id") Long id) {
    return userService.getUserWithRolesById(id);
  }

  @RequestMapping(value = "/users/{id}", method = DELETE, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> disable(@PathVariable("id") Long id,
                                                 HttpServletRequest request) {
    userService.disable(id, loggedInUserId(request));
    deactivateUserSessions(id);

    return success(MSG_USER_DISABLE_SUCCESS);
  }

  private void deactivateUserSessions(Long id) {
    List<Object> principals = sessionRegistry.getAllPrincipals();
    List<SessionInformation> disabledUserSessions = new ArrayList<>();

    if (principals.contains(id)) {
      Object disabledUserPrincipal = getDisabledUserPrincipal(principals, id);
      disabledUserSessions = sessionRegistry.getAllSessions(disabledUserPrincipal, false);
    }

    for (SessionInformation disabledUserSession : disabledUserSessions) {
      sessionRegistry.getSessionInformation(disabledUserSession.getSessionId()).expireNow();
    }
  }

  private Object getDisabledUserPrincipal(List<Object> principals, Long id) {
    Object disabledUserPrincipal = null;
    for (Object principal : principals) {
      if (principal.equals(id)) {
        disabledUserPrincipal = principal;
      }
    }
    return disabledUserPrincipal;
  }

  @RequestMapping(value = "/user/validatePasswordResetToken/{token}", method = GET)
  public ResponseEntity<OpenLdrResponse> validatePasswordResetToken(@PathVariable(value = "token") String token) throws IOException, ServletException {
    try {
      userService.getUserIdByPasswordResetToken(token);
    } catch (DataException e) {
      return error(e, BAD_REQUEST);
    }
    return response(TOKEN_VALID, true);
  }

  @RequestMapping(value = "/user/resetPassword/{token}", method = PUT)
  public ResponseEntity<OpenLdrResponse> resetPassword(@PathVariable(value = "token") String token, @RequestBody String password) {
    try {
      userService.updateUserPassword(token, password);
    } catch (DataException e) {
      return error(e, BAD_REQUEST);
    }
    return success(messageService.message("password.reset"));
  }

  @RequestMapping(value = "/admin/resetPassword/{userId}", method = PUT)
  public ResponseEntity<OpenLdrResponse> updateUserPassword(@PathVariable(value = "userId") Long userId, @RequestBody String password) {
    try {
      userService.updateUserPassword(userId, password);
    } catch (DataException e) {
      return error(e, BAD_REQUEST);
    }
    return success(messageService.message("password.reset.success"));
  }


  @RequestMapping(value = "/user/preferences", method = GET)
  public ResponseEntity<OpenLdrResponse> getUserPreferences(HttpServletRequest request){
    return response("preferences", userService.getPreferences(this.loggedInUserId(request)));
  }

  @RequestMapping(value = "/users/{userId}/preferences", method = GET)
  public ResponseEntity<OpenLdrResponse> getUserPreferences(@PathVariable("userId") Long userId){
    return response("preferences", userService.getPreferences(userId));
  }

  @RequestMapping(value = "/users/{userId}/preferences", method = PUT, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> updateUserPreferences(@PathVariable(value = "userId") Long userId, @RequestParam("programId") Long programId,
                                                               @RequestParam("facilityId") Long facilityId, @RequestParam("products") List<Long> productListId,
                                                               @RequestBody User user,
                                                               HttpServletRequest request) {
    Long currentUser = loggedInUserId(request);
    if (userId.equals(currentUser) || roleRightService.getRights(currentUser).contains(MANAGE_USER)) {
      user.setModifiedBy(currentUser);
      user.setId(userId);
      try {
        userService.updateUserPreferences(userId, user, programId, facilityId, productListId);
      } catch (DataException e) {
        return error(e, BAD_REQUEST);
      }
      return success(messageService.message("user.preference.set.successfully"));
    }
    return new OpenLdrResponse().errorEntity(FORBIDDEN.getReasonPhrase(), FORBIDDEN);
  }


  @RequestMapping(value = "/preference/users/{id}", method = GET)
  public User getUser(@PathVariable(value = "id") Long id, HttpServletRequest request) {
    Long userId = loggedInUserId(request);
    if (id == userId || roleRightService.getRights(userId).contains(MANAGE_USER)){
      return userService.getUserWithRolesById(id);
    }
    return null;
  }

  @RequestMapping(value = "/user/userGroups", method = GET)
  public ResponseEntity<OpenLdrResponse> getAllUserGroups(HttpServletRequest request) {
    return response("groups", userGroupService.getAll());
  }

  @RequestMapping(value = "/getLoggedInUser", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> getLoggedInUser(HttpServletRequest request) {
    return response("user", userService.getById(loggedInUserId(request)));
  }
 @RequestMapping(value = "/getAllPartners", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> getAllPartners(HttpServletRequest request) {
    return response("partners", userService.getAllPartners());
  }


}
