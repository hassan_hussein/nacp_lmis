package com.openldr.web.controller;

import com.openldr.core.exception.DataException;
import com.openldr.core.message.OpenLDRMessage;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.db.service.DbService;
import com.openldr.upload.RecordHandler;
import com.openldr.upload.exception.UploadException;
import com.openldr.upload.model.AuditFields;
import com.openldr.upload.model.ModelClass;
import com.openldr.upload.parser.CSVParser;
import com.openldr.web.model.UploadBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.openldr.core.web.OpenLdrResponse.response;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class UploadController extends BaseController {

    public static final String SELECT_UPLOAD_TYPE = "upload.select.type";
    public static final String INCORRECT_FILE = "upload.incorrect.file";
    public static final String FILE_IS_EMPTY = "upload.file.empty";
    public static final String INCORRECT_FILE_FORMAT = "upload.incorrect.file.format";
    public static final String UPLOAD_FILE_SUCCESS = "upload.file.successful";
    public static final String SUCCESS = "success";
    public static final String ERROR = "error";
    public static final String SUPPORTED_UPLOADS = "supportedUploads";

    //@Autowired
    private CSVParser csvParser;

    @Autowired
    private DbService dbService;

    @Autowired
    private HashMap<String, UploadBean> uploadBeansMap;

    @RequestMapping(value = "/upload", method = POST)
   // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'UPLOADS')")
    public ResponseEntity<OpenLdrResponse> upload(MultipartFile csvFile, String model, HttpServletRequest request) {
        try {
            OpenLDRMessage errorMessage = validateFile(model, csvFile);
            if (errorMessage != null) {
                return errorResponse(errorMessage);
            }

            Date currentTimestamp = dbService.getCurrentTimestamp();

            RecordHandler recordHandler = uploadBeansMap.get(model).getRecordHandler();
            ModelClass modelClass = new ModelClass(uploadBeansMap.get(model).getImportableClass());
            AuditFields auditFields = new AuditFields(loggedInUserId(request), currentTimestamp);

            csvParser =new CSVParser();
            int recordsToBeUploaded = csvParser.process(csvFile.getInputStream(), modelClass, recordHandler, auditFields);

            return successPage(recordsToBeUploaded);
        } catch (DataException dataException) {
            return errorResponse(dataException.getOpenLDRMessage());
        } catch (UploadException e) {
            return errorResponse(new OpenLDRMessage(messageService.message(e.getCode(), (Object[])e.getParams())));
        } catch (IOException e) {
            return errorResponse(new OpenLDRMessage(e.getMessage()));
        }
    }

    @RequestMapping(value = "/supported-uploads", method = GET, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'UPLOADS')")
    public ResponseEntity<OpenLdrResponse> getSupportedUploads() {
        // this is a hack to make the new version of jackson to work
        // fasterxml jackson does currenly was failing to serialize
        HashMap<String, UploadBean> beanDefinitions = new HashMap<>();
        for(String key :uploadBeansMap.keySet()){
            UploadBean proxy = uploadBeansMap.get(key);
            UploadBean bean = new UploadBean();
            bean.setDisplayName(proxy.getDisplayName());
            beanDefinitions.put(key,bean);
        }

        return response(SUPPORTED_UPLOADS, beanDefinitions);
    }

    private OpenLDRMessage validateFile(String model, MultipartFile csvFile) {
        OpenLDRMessage errorMessage = null;
        if (model.isEmpty()) {
            errorMessage = new OpenLDRMessage(SELECT_UPLOAD_TYPE);
        } else if (!uploadBeansMap.containsKey(model)) {
            errorMessage = new OpenLDRMessage(INCORRECT_FILE);
        } else if (csvFile == null || csvFile.isEmpty()) {
            errorMessage = new OpenLDRMessage(FILE_IS_EMPTY);
        } else if (!csvFile.getOriginalFilename().endsWith(".csv") && !csvFile.getOriginalFilename().endsWith(".CSV")) {
            errorMessage = new OpenLDRMessage(messageService.message(INCORRECT_FILE_FORMAT, messageService.message(uploadBeansMap.get(model).getDisplayName())));
        }
        return errorMessage;
    }

    private ResponseEntity<OpenLdrResponse> successPage(int recordsProcessed) {
        Map<String, String> responseMessages = new HashMap<>();
        String message = messageService.message(UPLOAD_FILE_SUCCESS, recordsProcessed);
        responseMessages.put(SUCCESS, message);
        return response(responseMessages, OK, TEXT_HTML_VALUE);
    }

    private ResponseEntity<OpenLdrResponse> errorResponse(OpenLDRMessage errorMessage) {
        Map<String, String> responseMessages = new HashMap<>();
        String message = messageService.message(errorMessage);
        responseMessages.put(ERROR, message);
        return response(responseMessages, OK, TEXT_HTML_VALUE);
    }

}
