package com.openldr.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openldr.core.domain.Pagination;
import com.openldr.core.dto.AutomaticUploadResultDTO;
import com.openldr.core.dto.LogTagDTO;
import com.openldr.core.exception.DataException;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.vl.dto.DashboardDTO;
import com.openldr.vl.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.openldr.core.web.OpenLdrResponse.error;
import static java.lang.Integer.parseInt;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by hassan on 5/28/17.
 */


    @Controller
    @RequestMapping(value = "/log-tag-api/")
    public class LogTagController extends BaseController {
     @Autowired
     private ResultService service;

    @Value("10")
    String limit;
        @RequestMapping(value="insert.json",method=POST, headers = ACCEPT_JSON)
        public String save(@RequestBody String donor, HttpServletRequest request){


            // String successResponse = StringszDC.format("Donor '%s' has been successfully saved");
          //  System.out.println(donor);
            System.out.println("Got the Rest API");

            ObjectMapper mapper = new ObjectMapper();
            try {
                LogTagDTO[] graph = mapper.readValue(donor, LogTagDTO[].class);
                service.uploadLogTag(Arrays.asList(graph));
               // System.out.println(Arrays.asList(graph));
            } catch (IOException e) {
                e.printStackTrace();
            }

            return donor;
        }


    @RequestMapping(value="getAllLogTags.json",method=GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getLogTags(HttpServletRequest httpRequest,
                                                                      @RequestParam(value = "page", defaultValue = "1") Integer page,
                                                      @RequestParam(value = "starDate") String startDate,
                                                      @RequestParam(value = "endDate") String endDate
    ){
        Long userId=loggedInUserId(httpRequest);
        Pagination pagination = new Pagination(page, parseInt(limit));
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("logs", service.getLogTags(startDate,endDate , pagination));
       // pagination.setTotalRecords(service.getTotalRows(userId));
        openLdrResponse.addData("pagination", pagination);
        return openLdrResponse.response(OK);
    }

        @RequestMapping(value="insert2.json",method=POST, headers = ACCEPT_JSON)
        public String saveSample(@RequestBody String upload, HttpServletRequest request){
            System.out.println(upload);
            ObjectMapper mapper = new ObjectMapper();
            try {
                AutomaticUploadResultDTO[] graph = mapper.readValue(upload, AutomaticUploadResultDTO[].class);
                service.automaticUpload(Arrays.asList(graph));
                System.out.println(Arrays.asList(graph));
            } catch (IOException e) {
                e.printStackTrace();
            }

          /*  try {
                service.automaticUpload(resultDTO);
                OpenLdrResponse openLdrResponse = new OpenLdrResponse("sample", resultDTO.get(0).getOrderNumber());
                return openLdrResponse.response(OK);
            } catch (DataException ex) {
                return error(ex, HttpStatus.CONFLICT);
            }*/
            // String successResponse = String.format("Donor '%s' has been successfully saved");
           /* System.out.println(donor);
            System.out.println("Got sample API");*/
            return upload;
        }






    }





