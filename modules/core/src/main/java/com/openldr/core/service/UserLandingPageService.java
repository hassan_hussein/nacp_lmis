package com.openldr.core.service;

import com.openldr.core.domain.UserLandingPage;
import com.openldr.core.repository.UserLandingPageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hassan on 8/15/16.
 */

@Service
public class UserLandingPageService {

    @Autowired
    private UserLandingPageRepository repository;

    public void save(UserLandingPage landingPage) {
        if (landingPage.getId() == null)
            repository.insert(landingPage);
        else
            repository.update(landingPage);
    }

    public UserLandingPage getLandingPageById(Long id) {
        return repository.getLandingPageById(id);
    }

    public UserLandingPage getByUserId(Long userId) {
        return repository.getByUserId(userId);
    }

    public List<UserLandingPage> getAll() {
        return repository.getAll();
    }

    public Long delete(Long id) {
        return repository.delete(id);
    }
}
