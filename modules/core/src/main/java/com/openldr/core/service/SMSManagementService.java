package com.openldr.core.service;

import com.openldr.core.domain.ShortMessage;
import com.openldr.core.repository.SMSRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;

@Service
@NoArgsConstructor
public class SMSManagementService {

    @Autowired
    ConfigurationSettingService configSetting;

    @Autowired
    private SMSRepository smsRepository;
    private @Value("${sms.gateway.url}") String relayWebsite;

    public void SaveIncomingSMSMessage(String message, String phoneNumber){
        smsRepository.SaveSMSMessage("I", message,phoneNumber, new Date(),true);
    }

    public void SendSMSMessage (String message, String phoneNumber) {
        String relayUrl = String.format("%s?message=%s&phone_number=%s",relayWebsite, message.replaceAll(" ","%20"), phoneNumber);
        try{
            URL url = new URL(relayUrl);
            url.getContent();
            smsRepository.SaveSMSMessage("O",message,phoneNumber,new Date(),true);
        }
        catch(IOException e) {
            //TODO: handle this.
        }

    }
/*
    public boolean sendSMS(String number, String message) {


        RestTemplate template = new RestTemplate();

        String auth = "SunAdmin" + ":" + "@#TeChn16&";
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        String url = "https://api.infobip.com/sms/1/text/single";
        Sms sms = new Sms();
        sms.setFrom("Afyacal");
        sms.setTo(number);
        sms.setText("Activation Code:" + message);

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        headers.add("Authorization", authHeader);
        headers.add("Content-Type", "application/json");
        template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpEntity<Sms> request = new HttpEntity<Sms>(sms, headers);
        template.postForObject(url, request, Sms.class);

        return true;
    }*/

    // sending sms
    public void sendSms(String content,String phoneNumber) throws IOException{

        String pushSmsUrl =  configSetting.getConfigurationStringValue("KANNEL_SETTINGS").toString();

        String urlString = pushSmsUrl+"&text="+content.replaceAll(" ","+")+"&to="+phoneNumber.toString();

        try {
            smsRepository.SaveSMSMessage("Outgoing", content, phoneNumber, new Date(),false);
            URL url = new URL(urlString.toString());
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            BufferedReader reader;
            reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = "";
            StringBuilder buffer = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                buffer = buffer.append(line).append("\n");
            }
            
            System.out.println("Submit request= " + urlString.toString());
            System.out.println("response : "+buffer.toString());
            System.out.println("INFO : all sent disconnect.");

        } catch (Exception e){
            e.fillInStackTrace();
        }
    }

    //Get all sms
    public List<ShortMessage>getSmsMessages(){
        return smsRepository.getAllSMS();
    }

    public List<ShortMessage> getMessagesForMobile(String mobile){
        return smsRepository.getForMobile(mobile);
    }



}
