package com.openldr.core.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.domain.*;
import lombok.Delegate;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.Transformer;


import java.util.List;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;
import static lombok.AccessLevel.NONE;
import static org.apache.commons.collections.CollectionUtils.collect;

/**
 * FacilityFeedDTO consolidates facility information like facility code, name, type, geographic zone, etc.
 * to be used while displaying facility information to user,  for eg. in feed.
 */
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
public class FacilityFeedDTO extends BaseFeedDTO {
  private interface ExcludedDelegates {
    public GeographicZone getGeographicZone();

    public FacilityType getFacilityType();

    public FacilityOperator getOperatedBy();

    public List<ProgramSupported> getSupportedPrograms();
  }

  @Delegate(excludes = ExcludedDelegates.class)
  @Getter(NONE)
  private Facility facility;
  private String geographicZone;
  private String facilityType;
  private String parentFacility;
  private String operatedBy;
  private List<String> programsSupported;

  @SuppressWarnings("unchecked")
  public FacilityFeedDTO(Facility facility, Facility parentFacility) {
    this.facility = facility;
    this.facilityType = facility.getFacilityType().getName();
    this.geographicZone = facility.getGeographicZone().getName();
    this.operatedBy = (facility.getOperatedBy() != null) ? facility.getOperatedBy().getText() : null;
    this.parentFacility = parentFacility != null ? parentFacility.getCode() : null;
    this.programsSupported = (List<String>) collect(facility.getSupportedPrograms(), new Transformer() {
      @Override
      public Object transform(Object o) {
        return ((ProgramSupported) o).getProgram().getCode();
      }
    });
  }
}
