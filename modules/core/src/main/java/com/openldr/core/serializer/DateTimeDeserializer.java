

package com.openldr.core.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.IOException;

import static org.apache.commons.lang.StringUtils.isBlank;


public class DateTimeDeserializer extends JsonDeserializer<DateTime> {
    private static final DateTimeFormatter formatter = ISODateTimeFormat.basicDateTimeNoMillis();

    @Override
    public DateTime deserialize(JsonParser jsonparser,
                                DeserializationContext deserializationcontext) throws IOException {

        String dtToParse = jsonparser.getText();
        if (isBlank(dtToParse)) return null;
        return formatter.parseLocalDateTime(jsonparser.getText()).toDateTime();
    }
}
