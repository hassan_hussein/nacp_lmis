
package com.openldr.core.repository;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.Program;
import com.openldr.core.domain.RequisitionGroupProgramSchedule;
import com.openldr.core.exception.DataException;
import com.openldr.core.repository.mapper.FacilityMapper;
import com.openldr.core.repository.mapper.ProcessingScheduleMapper;
import com.openldr.core.repository.mapper.RequisitionGroupMapper;
import com.openldr.core.repository.mapper.RequisitionGroupProgramScheduleMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * RequisitionGroupProgramScheduleRepository is Repository class for RequisitionGroupProgramSchedule related database operations.
 */

@Repository
@NoArgsConstructor
public class RequisitionGroupProgramScheduleRepository {

  private RequisitionGroupProgramScheduleMapper mapper;
  private RequisitionGroupMapper requisitionGroupMapper;
  private ProgramRepository programRepository;
  private ProcessingScheduleMapper processingScheduleMapper;
  private FacilityMapper facilityMapper;

  @Autowired
  public RequisitionGroupProgramScheduleRepository(
          RequisitionGroupProgramScheduleMapper requisitionGroupProgramScheduleMapper,
          RequisitionGroupMapper requisitionGroupMapper,
          ProgramRepository programRepository,
          ProcessingScheduleMapper processingScheduleMapper,
          FacilityMapper facilityMapper) {

    this.mapper = requisitionGroupProgramScheduleMapper;
    this.requisitionGroupMapper = requisitionGroupMapper;
    this.programRepository = programRepository;
    this.processingScheduleMapper = processingScheduleMapper;
    this.facilityMapper = facilityMapper;
  }

  public void insert(RequisitionGroupProgramSchedule requisitionGroupProgramSchedule) {
    populateIdsForRequisitionProgramScheduleEntities(requisitionGroupProgramSchedule);
    validateRequisitionGroupSchedule(requisitionGroupProgramSchedule);
   validateProgramType(requisitionGroupProgramSchedule);
    try {
      mapper.insert(requisitionGroupProgramSchedule);
    } catch (DuplicateKeyException e) {
      throw new DataException("error.duplicate.requisition.group.program.combination");
    }
  }

  private void validateProgramType(RequisitionGroupProgramSchedule requisitionGroupProgramSchedule) {
    Program program = programRepository.getById(requisitionGroupProgramSchedule.getProgram().getId());
    if (program.getPush()) {
      throw new DataException("error.program.type.not.supported.requisitions");
    }
  }

  public void update(RequisitionGroupProgramSchedule requisitionGroupProgramSchedule) {
    populateIdsForRequisitionProgramScheduleEntities(requisitionGroupProgramSchedule);
    validateRequisitionGroupSchedule(requisitionGroupProgramSchedule);
    validateProgramType(requisitionGroupProgramSchedule);
    mapper.update(requisitionGroupProgramSchedule);
  }

  private void populateIdsForRequisitionProgramScheduleEntities(RequisitionGroupProgramSchedule requisitionGroupProgramSchedule) {
    requisitionGroupProgramSchedule.getRequisitionGroup().setId(
      requisitionGroupMapper.getIdForCode(
        requisitionGroupProgramSchedule.getRequisitionGroup().getCode()));

  requisitionGroupProgramSchedule.getProgram().setId(
      programRepository.getIdByCode(
        requisitionGroupProgramSchedule.getProgram().getCode()));

    requisitionGroupProgramSchedule.getProcessingSchedule().setId(
      processingScheduleMapper.getIdForCode(
        requisitionGroupProgramSchedule.getProcessingSchedule().getCode()));

    Facility dropOffFacility = requisitionGroupProgramSchedule.getDropOffFacility();

    if (dropOffFacility != null)
      requisitionGroupProgramSchedule.getDropOffFacility().setId(
        facilityMapper.getIdForCode(
          dropOffFacility.getCode()));
  }

  private void validateRequisitionGroupSchedule(RequisitionGroupProgramSchedule requisitionGroupProgramSchedule) {
    if (requisitionGroupProgramSchedule.getRequisitionGroup().getId() == null)
      throw new DataException("error.requisition.group.not.exists");

    if (requisitionGroupProgramSchedule.getProcessingSchedule().getId() == null)
      throw new DataException("error.schedule.not.exists");

    if (!requisitionGroupProgramSchedule.isDirectDelivery() && requisitionGroupProgramSchedule.getDropOffFacility() == null)
      throw new DataException("error.drop.off.facility.not.defined");

    if (requisitionGroupProgramSchedule.getDropOffFacility() != null && requisitionGroupProgramSchedule.getDropOffFacility().getId() == null)
      throw new DataException("error.drop.off.facility.not.present");
  }

  public RequisitionGroupProgramSchedule getScheduleForRequisitionGroupAndProgram(Long requisitionGroupId, Long programId) {
    return mapper.getScheduleForRequisitionGroupIdAndProgramId(requisitionGroupId, programId);
  }

  public List<Long> getProgramIDsForRequisitionGroup(Long requisitionGroupId) {
    return mapper.getProgramIDsById(requisitionGroupId);
  }

  public RequisitionGroupProgramSchedule getScheduleForRequisitionGroupCodeAndProgramCode(String requisitionGroupCode, String programCode) {
    return mapper.getScheduleForRequisitionGroupCodeAndProgramCode(requisitionGroupCode, programCode);
  }

  public List<RequisitionGroupProgramSchedule> getByRequisitionGroupId(Long requisitionGroupId) {
    return mapper.getByRequisitionGroupId(requisitionGroupId);
  }

  public void deleteRequisitionGroupProgramSchedulesFor(Long requisitionGroupId) {
    mapper.deleteRequisitionGroupProgramSchedulesFor(requisitionGroupId);
  }
}
