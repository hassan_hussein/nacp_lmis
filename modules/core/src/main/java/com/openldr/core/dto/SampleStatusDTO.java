package com.openldr.core.dto;

/**
 * Created by hassan on 6/10/17.
 */
public enum SampleStatusDTO {
    REQUEST,
    REGISTERED,
    PENDING,
    RECEIVED,
    REJECTED,
    ACCEPTED,
    TESTED,
    INPROGRESS,
    APPROVED,
    VERIFIED,
    DISPATCHED,
    RESULTED

}