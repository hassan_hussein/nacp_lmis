package com.openldr.core.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.awt.*;
import java.awt.event.KeyEvent;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_NULL;

/**
 * Created by hassan on 3/26/17.
 */

@Getter
@Setter
@NoArgsConstructor
@JsonSerialize(include = NON_NULL)
public class BarcodeScanner extends BaseModel{


}
