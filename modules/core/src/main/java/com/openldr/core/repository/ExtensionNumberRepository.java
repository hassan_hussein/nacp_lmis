package com.openldr.core.repository;

import com.openldr.core.domain.ExtensionNumber;
import com.openldr.core.exception.DataException;
import com.openldr.core.repository.mapper.ExtensionNumberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 7/28/16.
 */
@Repository
public class ExtensionNumberRepository {

    @Autowired
    private ExtensionNumberMapper mapper;

    public void save(ExtensionNumber extensionNumber) {

        try {
            validateExtensionNumber(extensionNumber);

            if (extensionNumber.getId() == null) {
                mapper.insert(extensionNumber);
            } else {
                mapper.update(extensionNumber);
            }

        } catch (DuplicateKeyException duplicateKeyException) {
            throw new DataException("Dublicate Extension Number");
        } catch (DataIntegrityViolationException integrityViolationException) {
            throw new DataException("error.incorrect.length");
        }

    }

    private void validateExtensionNumber(ExtensionNumber extensionNumber) {
        if (extensionNumber.getExtensionNumber() == null) return;
    }

    public ExtensionNumber getAllById(Long id) {
        return mapper.getById(id);
    }

    public List<ExtensionNumber> getAll() {
        return mapper.getAll();
    }

    public Long delete(Long id) {
        return mapper.delete(id);
    }
}
