package com.openldr.core.repository;

import com.openldr.core.dto.CounselorRecord;
import com.openldr.core.repository.mapper.CounselorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 10/14/17.
 */

@Component
public class CounselorRepository {

    @Autowired
    private CounselorMapper mapper;

    public Integer insert(CounselorRecord record){
//     return mapper.insert(record);
    	return -1;
    }

    public void update(CounselorRecord record){
//        mapper.update(record);
    }

    public CounselorRecord getById(Long id){
        return mapper.getAllById(id);
    }

    public List<CounselorRecord>getAll(){
        return mapper.getAll();
    }

    public void getByFirstNameAndPhone(String phoneNumber,String firstName) {
        mapper.getByFirstNameAndPhone(phoneNumber,firstName);
    }
}
