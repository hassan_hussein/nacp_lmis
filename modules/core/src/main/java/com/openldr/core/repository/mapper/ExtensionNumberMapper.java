package com.openldr.core.repository.mapper;

import com.openldr.core.domain.ExtensionNumber;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 7/28/16.
 */

@Repository
public interface ExtensionNumberMapper {

    @Insert({" INSERT INTO extension_numbers(    ",
            "            extensionNumber, description,  displayorder,createddate)  ",
            "    VALUES (#{extensionNumber}, #{description}, #{displayOrder}, NOW())  "})
    @Options(useGeneratedKeys = true)
    void insert(ExtensionNumber number);

    @Select({" SELECT * ",
            "  FROM extension_numbers"})
    List<ExtensionNumber> getAll();

    @Update({" UPDATE extension_numbers  ",
            "   SET extensionNumber= #{extensionNumber}, description=#{description}, displayorder= #{displayOrder}  ",
            " WHERE id = #{id} "})
    void update(ExtensionNumber number);

    @Select(" select * from extension_numbers where id = #{id}")
    ExtensionNumber getById(Long id);

    @Select(" DELETE FROM extension_numbers where id = #{id}")
    Long delete(Long id);

}
