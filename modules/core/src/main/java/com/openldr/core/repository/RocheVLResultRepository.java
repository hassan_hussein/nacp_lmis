/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2013 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

package com.openldr.core.repository;

import com.openldr.core.domain.Pagination;
import com.openldr.core.domain.RocheVLResult;
import com.openldr.core.dto.AutomaticUploadResultDTO;
import com.openldr.core.dto.LogTagDTO;
import com.openldr.core.dto.SampleStatusChangeDTO;
import com.openldr.core.exception.DataException;
import com.openldr.core.repository.mapper.RocheVLResultMapper;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * GeographicZoneRepository is repository class for GeographicZone related database operations.
 */

@Repository
@NoArgsConstructor
@Slf4j
public class RocheVLResultRepository {

    private RocheVLResultMapper mapper;


    @Autowired
    public RocheVLResultRepository(RocheVLResultMapper mapper) {
        this.mapper = mapper;
    }


    public void save(RocheVLResult result) {
        RocheVLResult existing = mapper.getByLabNumber(result.getOrderNumber());
        try {
            if (existing == null) {
                mapper.insert(result);
                return;
            } else {
                mapper.update(result);
            }
        } catch (DuplicateKeyException e) {
            throw new DataException("error.duplicate.geographic.zone.code");
        } catch (DataIntegrityViolationException e) {
            throw new DataException("error.incorrect.length");
        }
    }

    public Long getSampleId(String labNumber) {
        return mapper.getSampleId(labNumber);
    }

    @Transactional
    public void updateSampleStatus(String status, Date testDate, Date resultDate, Long sampleId) {
        mapper.updateSampleStatus(status, testDate, resultDate, sampleId);
        mapper.updateSampleResultStatus(testDate, resultDate, sampleId);
    }

    public RocheVLResult getByLabNumber(String orderNumber) {
        return mapper.getByLabNumber(orderNumber);
    }

    public void saveAutoUpload(AutomaticUploadResultDTO resultDTO) {

        RocheVLResult existing = mapper.getByLabNumber(resultDTO.getSampleId());
        try {
            if (existing == null) {
                mapper.insertAutoUpload(resultDTO);
                return;
            } else
                mapper.updateAutoUpload(resultDTO);
        } catch (DuplicateKeyException e) {
            log.debug("",e);
            throw new DataException("error.duplicate.geographic.zone.code");
        } catch (DataIntegrityViolationException e) {
            log.debug("", e);
            throw new DataException("error.incorrect.length");
        }

    }

    public Long getStatusById(Long sampleId) {
        return mapper.getStatusById(sampleId);
    }

    public void updateStatus(SampleStatusChangeDTO dto) {
        mapper.updateStatus(dto);
    }

    public void insertStatus(SampleStatusChangeDTO dto) {
        mapper.insertStatus(dto);
    }

    public void saveLogTagData(LogTagDTO dto) {
        mapper.saveLogTagData(dto);
    }

    public LogTagDTO getByDateAndSerialNumber(String logDate, String serialNumber) {
        return mapper.geByDateAndSerialNumber(logDate, serialNumber);
    }

    public void updateLogTag(LogTagDTO dto) {
        mapper.updateLogTag(dto);
    }

    public List<LogTagDTO> geLogTags(String startDate, String endDate, Pagination pagination) {
        return mapper.getLogTags(startDate, endDate, pagination);
    }
}
