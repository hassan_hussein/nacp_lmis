package com.openldr.core.service;

import com.openldr.core.domain.ExtensionNumber;
import com.openldr.core.repository.ExtensionNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hassan on 7/28/16.
 */
@Service
public class ExtensionNumberService {

    @Autowired
    ExtensionNumberRepository repository;

    public void save(ExtensionNumber number) {
        repository.save(number);
    }

    public List<ExtensionNumber> getAll() {
        return repository.getAll();
    }

    public ExtensionNumber getAllById(Long id) {
        return repository.getAllById(id);
    }

    public Long delete(Long id) {
        return repository.delete(id);
    }


}
