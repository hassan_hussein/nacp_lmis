package com.openldr.core.repository.mapper;

import com.openldr.core.domain.HIVTestType;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HIVTestTypeMapper {

    @Insert({"INSERT INTO hiv_test_types (code, name,displayOrder) " +
            "                   Values (#{code},#{name},#{displayOrder})"})
    @Options(useGeneratedKeys = true)
    Integer insert(HIVTestType testType);

    @Insert({"UPDATE hiv_test_types set code=#{code}, name=#{name}, displayOrder=#{displayOrder} WHERE id=#{id}"})
    Integer update(HIVTestType testType);

    @Select("Select * from hiv_test_types where id=#{id}")
    HIVTestType getById(@Param("id") Long id);

    @Select("Select * from hiv_test_types where lower(name)=lower(#{name})")
    HIVTestType getByName(@Param("name") String name);

    @Select("Select * from hiv_test_types")
    List<HIVTestType> getAll();

    @Update("Update hiv_test_types set defaultview=false")
    Integer resetDefault();

    @Update("Update hiv_test_types set defaultview=true where id=#{typeId}")
    Integer setDefault(@Param("typeId")Long typeId);

    @Select("Select * from hiv_test_types where defaultview=true limit 1")
    HIVTestType getDefault();

    @Select("Select testtypeid from hiv_facility_default_test_types where facilityid=#{facilityId} limit 1")
    Long getFacilityDefault(@Param("facilityId")Long facilityId);

   @Insert("Insert into hiv_facility_default_test_types(facilityid,testtypeid) VALUES(#{facilityId},#{typeId}) ")
    void setFacilityDefault(@Param("facilityId")Long facilityId, @Param("typeId")Long typeId);

    @Update("Update hiv_facility_default_test_types set testtypeid=#{typeId}  where facilityid=#{facilityId}")
    void updateFacilityDefault(@Param("facilityId")Long facilityId, @Param("typeId")Long typeId);
}
