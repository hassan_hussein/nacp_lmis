/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2013 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

package com.openldr.core.repository.mapper;

import com.openldr.core.domain.RocheVLResult;
import com.openldr.core.dto.AutomaticUploadResultDTO;
import com.openldr.core.dto.LogTagDTO;
import com.openldr.core.dto.SampleStatusChangeDTO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * GeographicZoneMapper maps the GeographicZone entity to corresponding representation in database.
 */
@Repository
public interface RocheVLResultMapper {

  @Insert("INSERT INTO hiv_results (sampleId, testdate, result,unit, resultdate, createdBy,createdDate, modifiedBy, modifiedDate) " +
    "VALUES (#{vlSampleId},#{testDate}, #{result}, #{unit}, #{resultDate}, #{createdBy},NOW() ,#{modifiedBy}, NOW())")
  @Options(useGeneratedKeys = true)
  Integer insert(RocheVLResult result);

  @Select("SELECT r.* FROM hiv_results r " +
          " JOIN hiv_samples s on r.sampleid=s.id" +
          " WHERE LOWER(s.labnumber) = LOWER(#{labNumber}) limit 1")
  RocheVLResult getByLabNumber(@Param("labNumber") String labNumber);


  @Update({"UPDATE hiv_results set testdate=#{testDate}, result = #{result}, unit=#{unit},resultdate=#{resultDate} ",
    " WHERE id = #{id}"})
  void update(RocheVLResult result);


  @Select("Select id from hiv_samples where labnumber=#{labNumber} LIMIT 1")
  Long getSampleId(@Param("labNumber")String labNumber);

  @Update("Update hiv_samples set status=#{status}::vlsamplestatus,testdate=#{testDate}, resultdate=#{resultDate} where id=#{sampleId}")
  Integer updateSampleStatus(@Param("status") String status,
                             @Param("testDate")Date testDate,
                             @Param("resultDate")Date resultDate,
                             @Param("sampleId")Long sampleId);

  @Update("Update hiv_results set resultdate=#{resultDate},testdate=#{testDate} where sampleid =#{sampleId}")
  Integer updateSampleResultStatus(@Param("testDate")Date testDate,
                             @Param("resultDate")Date resultDate,
                             @Param("sampleId")Long sampleId);


  @Insert("INSERT INTO hiv_results (sampleId, testdate, result,unit, resultdate, createdBy,createdDate, modifiedBy, modifiedDate,\n"+
          " testedby,  machinename,machineserial ) \n" +
          "VALUES (#{vlSampleId},#{testDate}, #{result}, #{unit}, #{resultDate}, #{createdBy},NOW() ,#{modifiedBy}, NOW(),\n"+
          " #{orderedBy}, #{machineName}, #{machineSerial} )")
  @Options(useGeneratedKeys = true)
  void insertAutoUpload(AutomaticUploadResultDTO resultDTO);

    @Insert("INSERT INTO hiv_results (sampleId, testdate, result,unit, resultdate, createdBy,createdDate, modifiedBy, modifiedDate) " +
            "VALUES (#{vlSampleId},#{testDate}, #{result}, #{unit}, #{resultDate}, #{createdBy},NOW() ,#{modifiedBy}, NOW())")
    @Options(useGeneratedKeys = true)
    void insertAutoUplaod(AutomaticUploadResultDTO resultDTO);

    @Update({"UPDATE hiv_results set testdate=#{testDate}, result = #{result}, unit=#{unit},resultdate=#{resultDate} ",
            " WHERE id = #{id}"})
    void updateAutoUpload(AutomaticUploadResultDTO resultDTO);


    @Select("select id from hiv_sample_status_changes where sampleId = #{sampleId} AND STATUS NOT IN ('RESULTED') LIMIT 1")
    Long getStatusById(@Param("sampleId") Long sampleId);


    @Update("UPDATE hiv_sample_status_changes\n" +
            "   SET status=#{status}, modifiedby=#{modifiedBy}, \n" +
            "       modifieddate=NOW()\n" +
            " WHERE ID = #{id}")
    void updateStatus(SampleStatusChangeDTO change);

    @Insert("  INSERT INTO hiv_sample_status_changes(\n" +
            "             sampleId, status, createdBy, createdDate, modifiedBy, modifiedDate)\n" +
            "    VALUES (#{sampleId}, #{status}, #{createdBy}, NOW(),#{modifiedBy}, NOW()) ")
    @Options(useGeneratedKeys = true)
    void insertStatus(SampleStatusChangeDTO dto);

    //Update Temperature
    @Insert("INSERT INTO log_tags(logDate,logTime,temperature,serialNumber,createdDate)  " +
            "VALUES(#{logDate},#{logTime},#{temperature},#{serialNumber},NOW())")
    @Options(useGeneratedKeys = true)
    void saveLogTagData(LogTagDTO dto);

    @Select("select * from log_tags WHERE logDate = #{logDate} AND serialNumber = #{serialNumber}")
    LogTagDTO geByDateAndSerialNumber(@Param("logDate")String logDate, @Param("serialNumber") String serialNumber);

    @Update("UPDATE log_tags\n" +
            "   SET  logDate=#{logDate}, logTime=#{logTime},\n" +
            "    temperature=#{temperature}, serialNumber=#{serialNumber},\n" +
            "     createdDate=NOW() where id =#{id} ")
    void updateLogTag(LogTagDTO dto);

    @Select("select * from log_tags where createdDate >=#{startDate}::DATE AND createdDate <=#{endDate}::DATE ")
    List<LogTagDTO> getLogTags(@Param("startDate") String startDate,@Param("endDate") String endDate, RowBounds rowBounds);

//End LogTag
}
