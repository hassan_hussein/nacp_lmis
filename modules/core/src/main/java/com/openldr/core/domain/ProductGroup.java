
package com.openldr.core.domain;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.upload.Importable;
import com.openldr.upload.annotation.ImportField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * ProductGroup represents the group for product. Also defines the contract for creation/upload of ProductGroup like
 * code and name mandatory, their respective data types and headers in upload csv.
 */
@EqualsAndHashCode(callSuper = false)
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize(include = NON_EMPTY)
public class ProductGroup extends BaseModel implements Importable {

  @ImportField(mandatory = true, name = "Product Group Code")
  private String code;

  @ImportField(mandatory = true, name = "Product Group Name")
  private String name;
}
