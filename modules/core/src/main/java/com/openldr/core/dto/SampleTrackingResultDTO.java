package com.openldr.core.dto;

import lombok.Data;

/**
 * Created by hassan on 9/29/17.
 */@Data
public class SampleTrackingResultDTO {

    private String labno;
    private String trackingID;
    private String TestName;
    private String testCode;
    private String testDescription;
    private String testResult;
    private String location;
    private String specimenDateTime;
    private String registeredDateTime;
    private String analysisDateTime;
    private String authorisedDateTime;
    private String registeredBy;
    private String authorisedBy;
    private String testedBy;
    private String repeated;
    private String timestamp;

}
