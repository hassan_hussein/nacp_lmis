package com.openldr.core.service;

import com.openldr.core.domain.Organization;
import com.openldr.core.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hassan on 7/9/16.
 */

@Service
public class OrganizationService {

    @Autowired
    private OrganizationRepository repository;

    public void Insert(Organization organization) {
        if (organization.getId() == null)
            repository.Insert(organization);
        else
            repository.update(organization);

    }

    public List<Organization> getAll() {
        return repository.getAll();
    }

    public Organization getById(Long Id) {
        return repository.getById(Id);
    }

}
