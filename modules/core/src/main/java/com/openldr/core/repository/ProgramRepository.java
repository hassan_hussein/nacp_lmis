
package com.openldr.core.repository;

import com.openldr.core.domain.Program;
import com.openldr.core.exception.DataException;
import com.openldr.core.message.OpenLDRMessage;
import com.openldr.core.repository.mapper.ProgramMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.openldr.core.domain.RightName.commaSeparateRightNames;


/**
 * ProgramRepository is Repository class for Program related database operations.
 */

@Component
@NoArgsConstructor
public class ProgramRepository {

  private ProgramMapper mapper;

  public static String PROGRAM_CODE_INVALID = "program.code.invalid";

  @Autowired
  public ProgramRepository(ProgramMapper programMapper) {
    this.mapper = programMapper;
  }

  public List<Program> getByFacility(Long facilityId) {
    return mapper.getByFacilityId(facilityId);
  }

  public List<Program> getAllPullPrograms() {
    return mapper.getAllPullPrograms();
  }

  public List<Program> getAllPushPrograms() {
    return mapper.getAllPushPrograms();
  }


  public List<Program> getUserSupervisedActiveProgramsWithRights(Long userId, String... rightNames) {
    return mapper.getUserSupervisedActivePrograms(userId, commaSeparateRightNames(rightNames));
  }

  public List<Program> getUserSupervisedActiveIvdProgramsWithRights(Long userId, String... rightNames) {
    return mapper.getUserSupervisedActiveIvdPrograms(userId, commaSeparateRightNames(rightNames));
  }

  public List<Program> getProgramsSupportedByUserHomeFacilityWithRights(Long facilityId, Long userId, String... rightNames) {
    return mapper.getProgramsSupportedByUserHomeFacilityWithRights(facilityId, userId, commaSeparateRightNames(rightNames));
  }

  public List<Program> getIvdProgramsSupportedByUserHomeFacilityWithRights(Long facilityId, Long userId, String... rightNames) {
    return mapper.getIvdProgramsSupportedByUserHomeFacilityWithRights(facilityId, userId, commaSeparateRightNames(rightNames));
  }


  public Long getIdByCode(String code) {
    Long programId = mapper.getIdForCode(code);

    if (programId == null) {
      throw new DataException(new OpenLDRMessage(PROGRAM_CODE_INVALID));
    }

    return programId;
  }

  public List<Program> getActiveProgramsForUserWithRights(Long userId, String... rightNames) {
    return mapper.getActiveProgramsForUserWithRights(userId, commaSeparateRightNames(rightNames));
  }

  public Program getById(Long id) {
    return mapper.getById(id);
  }

  public void setTemplateConfigured(Long id) {
    mapper.setTemplateConfigured(id);
  }

  public List<Program> getProgramsForUserByFacilityAndRights(Long facilityId, Long userId, String... rightNames) {
    return mapper.getProgramsForUserByFacilityAndRights(facilityId, userId, commaSeparateRightNames(rightNames));
  }

  public List<Program> getAll() {
    return mapper.getAll();
  }

  public Program getByCode(String code) {
    return mapper.getByCode(code);
  }

  public void setRegimenTemplateConfigured(Long programId) {
    mapper.setRegimenTemplateConfigured(programId);
  }

  public void setFeedSendFlag(Program program, Boolean sendFeed) {
    mapper.setFeedSendFlag(program, sendFeed);
  }

  public List<Program> getProgramsForNotification() {
    return mapper.getProgramsForNotification();
  }

  public Program update(Program program) {

      if(program.getBudgetingApplies() == false)
          program.setUsePriceSchedule(false);

    mapper.update(program);
    return mapper.getById(program.getId());
  }

  public List<Program> getAllIvdPrograms() {
    return mapper.getAllIvdPrograms();

  }
}
