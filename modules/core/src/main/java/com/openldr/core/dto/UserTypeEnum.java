package com.openldr.core.dto;

/**
 * Created by hassan on 7/9/16.
 */
public enum UserTypeEnum {
    LAB,
    HUB,
    ADMIN,
    NORMAL_USER
}
