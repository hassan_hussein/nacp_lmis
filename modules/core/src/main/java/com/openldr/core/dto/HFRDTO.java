package com.openldr.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.openldr.core.domain.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by hassan on 10/14/17.
 */

@Data
@EqualsAndHashCode
public class HFRDTO extends BaseModel{

    private String mohSwId;
    private String facilityName;
    private Object commonName;
    private String zone;
    private String region;
    private String district;
    private String council;
    private String ward;
    private String villageStreet;
    private String facilityType;
    private String operatingStatus;
    private String ownership;
    private Object registrationNumber;
    private Object latitude;
    private Object longitude;
    private String location;
    private String hfrCode;

}
