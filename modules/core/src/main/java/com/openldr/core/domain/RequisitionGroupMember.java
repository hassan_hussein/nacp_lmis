

package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.upload.Importable;
import com.openldr.upload.annotation.ImportField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_NULL;

/**
 * RequisitionGroupMember represents a facility which is a member of a Requisition Group. It also defines the contract
 * for upload of RequisitionGroupMember.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequisitionGroupMember extends BaseModel implements Importable {

  @ImportField(mandatory = true, name = "RG Code", nested = "code")
  private RequisitionGroup requisitionGroup;

  @ImportField(mandatory = true, name = "Member Facility", nested = "code")
  private Facility facility;

  public RequisitionGroupMember(RequisitionGroup requisitionGroup, Facility facility) {
    this.requisitionGroup = requisitionGroup;
    this.facility = facility;
  }
}
