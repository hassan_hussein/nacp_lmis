
package com.openldr.core.message;

import org.springframework.context.MessageSource;

import java.util.Locale;
import java.util.Map;

/**
 * Extends {@link MessageSource} interface to provide a way to get all key/message pairs
 * known.
 */
public interface ExposedMessageSource extends MessageSource {
    /**
     * Returns all key/message pairs for the given locale.
     * @param locale the desired locale of the messages.
     * @return a map of all key/message pairs for the given locale.
     */
    public Map<String,String> getAll(Locale locale);
}
