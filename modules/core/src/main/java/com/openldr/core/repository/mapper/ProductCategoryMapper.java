package com.openldr.core.repository.mapper;

import com.openldr.core.domain.ProductCategory;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chrispinus on 3/14/16.
 */
@Repository
public interface ProductCategoryMapper {


    @Insert({"INSERT INTO product_categories",
            "(code, name, displayOrder, createdBy, modifiedBy, modifiedDate)",
            "VALUES",
            "(#{code}, #{name}, #{displayOrder}, #{createdBy}, #{modifiedBy}, COALESCE(#{modifiedDate}, NOW()))"})
    @Options(useGeneratedKeys = true)
    public void insert(ProductCategory productCategory);

    @Delete("DELETE FROM product_categories WHERE code=#{code}")
    public void deleteByCode(String code);

    @Select("SELECT * FROM product_categories WHERE id = #{id}")
    public ProductCategory getById(Long id);

    @Select("SELECT * FROM product_categories WHERE LOWER(code) = LOWER(#{code})")
    public ProductCategory getByCode(String code);

    @Update({"UPDATE product_categories SET name = #{name}, modifiedBy = #{modifiedBy},",
            "displayOrder = #{displayOrder}, modifiedDate =#{modifiedDate} where id = #{id}"})
    public void update(ProductCategory category);

    @Select("SELECT id FROM product_categories WHERE LOWER(code) = LOWER(#{code})")
    public Long getIdByCode(String categoryCode);

    @Select("SELECT * FROM product_categories")
    public List<ProductCategory> getAll();


}
