package com.openldr.core.repository.mapper;

import com.openldr.core.dto.HFRDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * Created by hassan on 10/14/17.
 */

@Repository
public interface HFRMapper {

    @Select("select * from hfr_facilities where mohSwId = #{mohSwId}")
    HFRDTO getById(@Param("mohSwId") Long id);
}
