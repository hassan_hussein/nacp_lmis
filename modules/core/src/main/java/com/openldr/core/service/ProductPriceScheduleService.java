

package com.openldr.core.service;

import com.openldr.core.domain.BaseModel;
import com.openldr.core.domain.Product;
import com.openldr.core.domain.ProductPriceSchedule;
import com.openldr.core.repository.ProductPriceScheduleRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@NoArgsConstructor
public class ProductPriceScheduleService {

  @Autowired
  private ProductPriceScheduleRepository repository;

  public void save(ProductPriceSchedule productPriceSchedule) {
    if (productPriceSchedule.getId() == null) {
      repository.insert(productPriceSchedule);
    } else {
      repository.update(productPriceSchedule);
    }
  }

  public BaseModel getByProductCodePriceScheduleCategory(ProductPriceSchedule productPriceSchedule) {
    return repository.getByProductCodeAndPriceSchedule(productPriceSchedule);
  }

  public List<ProductPriceSchedule> getByProductId(Long id) {
    return repository.getByProductId(id);
  }

  @Transactional
  public void saveAll(List<ProductPriceSchedule> productPriceSchedules, Product product) {

    for (ProductPriceSchedule productPriceSchedule : productPriceSchedules) {
      if (productPriceSchedule.getId() == null) {
        productPriceSchedule.setProduct(product);
      }
      productPriceSchedule.setModifiedBy(product.getModifiedBy());
      productPriceSchedule.setCreatedBy(product.getModifiedBy());
      productPriceSchedule.setModifiedDate(product.getModifiedDate());
      save(productPriceSchedule);
    }
  }

  public List<ProductPriceSchedule> getPriceScheduleFullSupplyFacilityApprovedProduct(Long programId, Long facilityId) {
    return repository.getPriceScheduleFullSupplyFacilityApprovedProduct(programId, facilityId);
  }
}
