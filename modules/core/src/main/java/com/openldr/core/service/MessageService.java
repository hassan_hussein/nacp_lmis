package com.openldr.core.service;

import com.openldr.core.message.ExposedMessageSource;
import com.openldr.core.message.ExposedMessageSourceImpl;
import com.openldr.core.message.OpenLDRMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;
import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;
import static org.springframework.web.context.WebApplicationContext.SCOPE_SESSION;

/**
 * Created by hassan on 4/18/16.
 */

   @Service
   @NoArgsConstructor
   @Scope(value = SCOPE_SESSION, proxyMode = TARGET_CLASS)
   public class MessageService {

    private ExposedMessageSource messageSource;

    @Setter
    @Getter
    private Locale currentLocale;

    private String locales;

    @Scope(SCOPE_REQUEST)
    public static MessageService getRequestInstance() {
        ExposedMessageSourceImpl exposedMessageSource = new ExposedMessageSourceImpl();
        exposedMessageSource.setBasename("messages");
        return new MessageService(exposedMessageSource, "en");
    }

    @Autowired
    public MessageService(ExposedMessageSource messageSource, @Value("${locales.supported}") String locales) {
        this.messageSource = messageSource;
        this.locales = locales;
        this.currentLocale = new Locale("en");
    }

    public String message(String key) {
        return message(key, currentLocale, (Object) null);
    }

    public String message(OpenLDRMessage openLDRMessage) {
        return message(openLDRMessage.getCode(), (Object[]) openLDRMessage.getParams());
    }

    @SuppressWarnings("non-varargs")
    public String message(String key, Object... args) {
        return message(key, currentLocale, args);
    }

    private String message(String key, Locale locale, Object... args) {
        return messageSource.getMessage(key, args, key, locale);
    }

    public Set<String> getLocales() {
        Set<String> localeSet = new HashSet<>();

        String[] localeCodes = locales.split(",");

        for (String locale : localeCodes) {
            localeSet.add(locale.trim());
        }

        return localeSet;
    }

    /**
     * Return all messages using the current locale.
     *
     * @return a map of all messages.
     */
    public Map<String, String> allMessages() {
        return allMessages(currentLocale);
    }


    /**
     * Return all messages of the given locale.
     *
     * @param locale the locale of the messages to return.
     * @return a map of all messages for the given locale.
     */
    public Map<String, String> allMessages(Locale locale) {
        return messageSource.getAll(currentLocale);
    }


}
