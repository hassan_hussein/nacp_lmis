package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.exception.DataException;
import com.openldr.upload.Importable;
import com.openldr.upload.annotation.ImportField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * ProgramProduct represents a product available under a program and program-product specific attributes like currentPrice and dosesPerMonth.
 * Also defines contract for upload of a ProgramProduct mapping.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = NON_EMPTY)
public class ProgramProduct extends BaseModel implements Importable {

  @ImportField(name = "Program Code", type = "String", nested = "code", mandatory = true)
  private Program program;

  @ImportField(name = "Product Code", type = "String", nested = "code", mandatory = true)
  private Product product;

  @ImportField(name = "Doses Per Month", type = "int", mandatory = true)
  private Integer dosesPerMonth;

  @ImportField(name = "Is Active", type = "boolean", mandatory = true)
  private Boolean active;

  @ImportField(mandatory = true, type = "String", name = "Product Category", nested = "code")
  private ProductCategory productCategory;

  private Long productCategoryId;

  @ImportField(name = "Full Supply", type = "boolean", mandatory = true)
  private boolean fullSupply;

  @ImportField(name = "Display Order", type = "int")
  private Integer displayOrder;

 // ProgramProductISA programProductIsa;

  private Money currentPrice;

  public ProgramProduct(Long id) {
    super(id);
  }

  public ProgramProduct(Program program, Product product, Integer dosesPerMonth, Boolean active) {
    this.program = program;
    this.product = product;
    this.dosesPerMonth = dosesPerMonth;
    this.active = active;
  }

  public ProgramProduct(Program program, Product product, Integer dosesPerMonth, Boolean active, Money currentPrice) {
    this.program = program;
    this.product = product;
    this.dosesPerMonth = dosesPerMonth;
    this.active = active;
    this.currentPrice = currentPrice;
  }

  public void validate() {
    if (currentPrice.isNegative()) throw new DataException("programProduct.invalid.current.price");
  }

  public ProgramProduct(ProgramProduct programProduct) {
    this.id = programProduct.id;
    this.program = programProduct.program;
    this.product = programProduct.product;
    this.dosesPerMonth = programProduct.dosesPerMonth;
    this.active = programProduct.active;
    this.currentPrice = programProduct.currentPrice;
    //this.programProductIsa = programProduct.programProductIsa;
    this.displayOrder = programProduct.displayOrder;
    this.fullSupply = programProduct.fullSupply;
  }
}
