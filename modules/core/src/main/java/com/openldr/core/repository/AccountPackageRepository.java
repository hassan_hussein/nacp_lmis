package com.openldr.core.repository;

import com.openldr.core.domain.AccountPackage;
import com.openldr.core.exception.DataException;
import com.openldr.core.repository.mapper.AccountPackageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 7/9/16.
 */

@Repository
public class AccountPackageRepository {

    @Autowired
    private AccountPackageMapper mapper;

    public Integer Insert(AccountPackage accountPackage) {
        return mapper.Insert(accountPackage);
    }

    public AccountPackage getAllById(Long Id) {
        return mapper.getAllById(Id);
    }

    public Integer update(AccountPackage accountPackage) {
        return mapper.update(accountPackage);
    }

    public List<AccountPackage> getAll() {
        return mapper.getAll();
    }

    public void save(AccountPackage accountPackage) {
        try {
            if (accountPackage.getId() == null) {
                mapper.Insert(accountPackage);
                return;
            }
            mapper.update(accountPackage);
        } catch (DuplicateKeyException e) {
            throw new DataException("Duplicate Name");
        } catch (DataIntegrityViolationException e) {
            throw new DataException("error.incorrect.length");
        }

    }

    public Long delete(Long id) {
        return mapper.delete(id);
    }

    public AccountPackage getById(Long accountPackageId) {
        return mapper.getById(accountPackageId);
    }
}
