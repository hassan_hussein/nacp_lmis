
package com.openldr.core.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.domain.ProgramSupported;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * ProgramSupportedEventDTO consolidates information about programs supported by a facility
 * like facilityCode, and list of programs to be used while displaying ProgramSupported
 * information to user, for eg. in feed.
 */
@Getter
@Setter
@JsonSerialize(include = NON_EMPTY)
public class ProgramSupportedEventDTO extends BaseFeedDTO {

  private String facilityCode;
  private List<ProgramSupportedDTO> programsSupported = new ArrayList<>();

  public static final String CATEGORY = "programs-supported";
  public static final String TITLE = "Programs Supported";


  public ProgramSupportedEventDTO(String facilityCode, List<ProgramSupported> programSupportedList) {
    this.facilityCode = facilityCode;

    for (ProgramSupported ps : programSupportedList) {
      SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
      String stringStartDate = (ps.getStartDate() == null ? null : dateFormat.format(ps.getStartDate()));
      ProgramSupportedDTO psDTO = new ProgramSupportedDTO(ps.getProgram().getCode(), ps.getProgram().getName(), ps.getActive(), ps.getStartDate(), stringStartDate);

      programsSupported.add(psDTO);
    }
  }

  /**
   * ProgramSupportedDTO consolidates information about program like code, name, active etc. which is more user readable.
   */
  @AllArgsConstructor
  @Data
  @JsonSerialize(include = NON_EMPTY)
  class ProgramSupportedDTO {
    private String code;
    private String name;
    private Boolean active;
    private Date startDate;
    private String stringStartDate;
  }

  public Event createEvent() throws URISyntaxException {
    return null;//new Event(UUID.randomUUID().toString(), TITLE, DateTime.now(), "", getSerializedContents(), CATEGORY);
  }

}
