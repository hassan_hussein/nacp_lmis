package com.openldr.core.repository.mapper;

import com.openldr.core.domain.Donor;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

/**
 * Created by hassan on 11/4/16.
 */

@Repository
public interface DonorMapper {

    @Insert("INSERT INTO donors(\n" +
            "           shortName, longName, code, geographiczoneid, createdBy, createddate, \n" +
            "            modifiedBy, modifieddate)\n" +
            "    VALUES (#{shortName}, #{longName}, #{code},#{geographicZone.id} , {createdBy}, NOW(), \n" +
            "            #{modifiedBy}, NOW())  ")
    Integer insert(Donor donor);


    @Select("select * from donors where id = #{id}")
    @Results(value = {
            @Result(property = "geographicZone", column = "geographicZoneId", javaType = Integer.class,
            one = @One(select = "com.openldr.core.repository.mapper.GeographicZoneMapper.getWithParentById"))})
    Donor getAllById(@Param("id") Long id);



}
