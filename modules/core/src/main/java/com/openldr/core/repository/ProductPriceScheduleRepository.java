
package com.openldr.core.repository;

import com.openldr.core.domain.PriceSchedule;
import com.openldr.core.domain.Product;
import com.openldr.core.domain.ProductPriceSchedule;
import com.openldr.core.repository.mapper.PriceScheduleMapper;
import com.openldr.core.repository.mapper.ProductMapper;
import com.openldr.core.repository.mapper.ProductPriceScheduleMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class ProductPriceScheduleRepository {

  @Autowired
  private ProductPriceScheduleMapper mapper;

  @Autowired
  private PriceScheduleMapper priceScheduleMapper;

  @Autowired
  private ProductMapper productMapper;

  public void insert(ProductPriceSchedule productPriceSchedule) {
    mapper.insert(productPriceSchedule);
  }

  public void update(ProductPriceSchedule productPriceSchedule) {
    mapper.update(productPriceSchedule);
  }

  //TODO: QUESTION? does this belong here? should this not move to a service?
  public ProductPriceSchedule getByProductCodeAndPriceSchedule(ProductPriceSchedule productPriceSchedule) {
    Product product = productMapper.getByCode(productPriceSchedule.getProduct().getCode());
    PriceSchedule schedule = priceScheduleMapper.getByCode(productPriceSchedule.getPriceSchedule().getCode());
    productPriceSchedule.getPriceSchedule().setId(schedule.getId());
    productPriceSchedule.getProduct().setId(product.getId());
    return mapper.getByProductCodePriceSchedule(productPriceSchedule.getProduct().getId(), schedule.getId());
  }

  public List<ProductPriceSchedule> getByProductId(Long id) {
    return mapper.getByProductId(id);
  }


  public List<ProductPriceSchedule> getPriceScheduleFullSupplyFacilityApprovedProduct(Long programId, Long facilityId) {
    return mapper.getPriceScheduleFullSupplyFacilityApprovedProduct(programId, facilityId);
  }
}
