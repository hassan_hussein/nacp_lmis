package com.openldr.core.repository.mapper;

import com.openldr.core.dto.CounselorServiceProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * Created by hassan on 10/14/17.
 */
@Repository
public interface CounselorServiceMapper {

    @Select(" select * from councellor_service_providers where id = #{id} ")
    CounselorServiceProvider getById(@Param("id") Long id);


}
