

package com.openldr.core.service;

import com.openldr.core.domain.PriceSchedule;
import com.openldr.core.repository.PriceScheduleRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class PriceScheduleService {

  @Autowired
  private PriceScheduleRepository repository;

  public List<PriceSchedule> getAllPriceSchedules() {
    return repository.getAllPriceSchedules();
  }

  public PriceSchedule getByCode(String code){
    return repository.getByCode(code);
  }

}
