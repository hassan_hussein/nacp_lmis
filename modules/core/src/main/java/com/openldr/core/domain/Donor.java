package com.openldr.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by hassan on 11/4/16.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Donor extends BaseModel{

    private String shortName;
    private String longName;
    private String code;
    private GeographicZone geographicZone;


}
