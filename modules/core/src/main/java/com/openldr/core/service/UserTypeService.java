package com.openldr.core.service;

import com.openldr.core.domain.UserType;
import com.openldr.core.repository.UserTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hassan on 7/9/16.
 */

@Service
public class UserTypeService {

    @Autowired
    private UserTypeRepository repository;

    public void Insert(UserType userType) {
        if (userType.getId() == null)
            repository.Insert(userType);
        else
            repository.update(userType);

    }

    public List<UserType> getAll() {
        return repository.getAll();
    }

    public UserType getById(Long Id) {
        return repository.getAllById(Id);
    }

}
