package com.openldr.core.service;

import com.openldr.core.domain.*;
import com.openldr.core.exception.DataException;
import com.openldr.core.hash.Encoder;
import com.openldr.core.message.OpenLDRMessage;
import com.openldr.core.repository.UserRepository;
import com.openldr.email.exception.EmailException;
import com.openldr.email.service.EmailService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.codec.binary.Base64;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Exposes the services for handling User entity.
 */

@Service
public class UserService {
  static final String USER_EMAIL_NOT_FOUND = "user.email.not.found";
  static final String USER_EMAIL_INCORRECT = "user.email.incorrect";
  static final String PASSWORD_RESET_TOKEN_INVALID = "user.password.reset.token.invalid";
  static final String USER_USERNAME_INCORRECT = "user.username.incorrect";
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private EmailService emailService;

  @Autowired
  private RoleAssignmentService roleAssignmentService;

  @Autowired
  private MessageService messageService;

  public static String getCommaSeparatedIds(List<Long> idList) {

    return idList == null ? "{}" : idList.toString().replace("[", "").replace("]", "").replace(", ", ",");
  }

  @Transactional
  public void clearcreate(User user, String resetPasswordLink) {
    save(user);
    sendUserCreationEmail(user, resetPasswordLink);

  }

  public void createUser(User user, String passwordResetLink) {
    save(user);
    if (!user.isMobileUser()) {
      prepareForEmailNotification(user, passwordResetLink);
    }
  }

  public Long createNewUser(User user) {
    try {
      userRepository.createUser(user);
    } catch (DuplicateKeyException e) {
      throw new DataException(new OpenLDRMessage(e.getMessage()));
    }
    return user.getId();
  }

  @Transactional
  public void update(User user) {
    user.validate();
    userRepository.update(user);
    roleAssignmentService.saveRolesForUser(user);
  }

  //Update user profile
  @Transactional
  public void updateUserProfile(User user) {
    userRepository.updateUserProfile(user);
  }

  public LinkedHashMap getPreferences(Long userId){
    List<LinkedHashMap> preferences = userRepository.getPreferences(userId);
    LinkedHashMap preference = new LinkedHashMap();
    // transform the shape of the list
    for (LinkedHashMap map : preferences) {
      preference.put(map.get("key"), map.get("value"));
    }

    return preference;
  }

  public void sendForgotPasswordEmail(User user, String resetPasswordLink) {
    user = getValidatedUser(user);

    userRepository.deletePasswordResetTokenForUser(user.getId());

    String subject = messageService.message("forgot.password.email.subject");
    SimpleMailMessage emailMessage = createEmailMessage(user, resetPasswordLink, subject);

    sendEmail(emailMessage);
  }

  public List<User> searchUser(String searchParam, Pagination pagination) {
    return userRepository.searchUser(searchParam,pagination);
  }

  public User getUserWithRolesById(Long id) {
    User user = userRepository.getById(id);
    user.setHomeFacilityRoles(roleAssignmentService.getHomeFacilityRoles(id));
    user.setSupervisorRoles(roleAssignmentService.getSupervisorRoles(id));
    user.setAdminRole(roleAssignmentService.getAdminRole(id));
    user.setReportingRole(roleAssignmentService.getReportingRole(id));
    user.setLabRole(roleAssignmentService.getLabRole(id));
    user.setHubRole(roleAssignmentService.getHubRole(id));
    user.setReportRoles(roleAssignmentService.getReportRole(id));
   // user.setAllocationRoles(roleAssignmentService.getAllocationRoles(id));
    user.setReportRoles(roleAssignmentService.getReportRole(id));
    return user;
  }

  public User getById(Long id) {
    return userRepository.getById(id);
  }

  public Long getUserIdByPasswordResetToken(String token) {
    Long userId = userRepository.getUserIdForPasswordResetToken(token);
    if (userId == null) {
      throw new DataException(PASSWORD_RESET_TOKEN_INVALID);
    }
    return userId;
  }

  @Transactional
  public void updateUserPassword(String token, String password) {
    Long userId = getUserIdByPasswordResetToken(token);
    userRepository.updateUserPasswordAndActivate(userId, Encoder.hash(password));
    userRepository.deletePasswordResetTokenForUser(userId);
  }

  public User getByUserName(String userName) {
    return userRepository.getByUserName(userName);
  }

  public User selectUserByUserNameAndPassword(String userName, String password) {
    return userRepository.selectUserByUserNameAndPassword(userName, password);
  }

  private void prepareForEmailNotification(User user, String passwordResetLink) {
    String subject = messageService.message("account.created.email.subject");
    SimpleMailMessage emailMessage = createEmailMessage(user, passwordResetLink, subject);
    userRepository.insertEmailNotification(emailMessage);
  }

  public void updateUserPassword(Long userId, String password) {
    userRepository.updateUserPassword(userId, Encoder.hash(password));
  }

  @Transactional
  public void disable(Long userId, Long modifiedBy) {
    userRepository.disable(userId, modifiedBy);
    userRepository.deletePasswordResetTokenForUser(userId);
  }

  public List<User> getUsersWithRightInNodeForProgram(Program program, SupervisoryNode node, String rightName) {
    return userRepository.getUsersWithRightInNodeForProgram(program, node, rightName);
  }

  public List<User> getUsersWithRightInHierarchyUsingBaseNode(Long nodeId, Program program, String rightName) {
    return userRepository.getUsersWithRightInHierarchyUsingBaseNode(nodeId, program.getId(), rightName);
  }

//  public List<User> getUsersWithRightOnWarehouse(Long id, String rightName) {
//    return userRepository.getUsersWithRightOnWarehouse(id, rightName);
//  }

  private void sendUserCreationEmail(User user, String resetPasswordLink) {
    String subject = messageService.message("account.created.email.subject");
    SimpleMailMessage emailMessage = createEmailMessage(user, resetPasswordLink, subject);
    sendEmail(emailMessage);
  }

  private void save(User user) {
    user.validate();
    userRepository.create(user);
    roleAssignmentService.saveRolesForUser(user);
  }

  private void sendEmail(SimpleMailMessage emailMessage) {
    try {
      emailService.queueMessage(emailMessage);
    } catch (EmailException e) {
      throw new DataException(USER_EMAIL_NOT_FOUND);
    }
  }

  private User getValidatedUser(User user) {
    if (user.getEmail() != null && !user.getEmail().equals("")) {
      user = userRepository.getByEmail(user.getEmail());
      if (user == null || !user.getActive()) throw new DataException(USER_EMAIL_INCORRECT);
    } else {
      user = userRepository.getByUserName(user.getUserName());
      if (user == null || !user.getActive()) throw new DataException(USER_USERNAME_INCORRECT);
    }
    return user;
  }

  private SimpleMailMessage createEmailMessage(User user, String resetPasswordLink, String subject) {
    String passwordResetToken = generateUUID();
    String[] passwordResetLink = new String[]{user.getFirstName(), user.getLastName(), user.getUserName(), resetPasswordLink + passwordResetToken};
    String mailBody = messageService.message("password.reset.email.body", (Object[]) passwordResetLink);

    userRepository.insertPasswordResetToken(user, passwordResetToken);

    SimpleMailMessage emailMessage = new SimpleMailMessage();
    emailMessage.setSubject(subject);
    emailMessage.setText(mailBody);
    emailMessage.setTo(user.getEmail());

    return emailMessage;
  }


 public SimpleMailMessage createEmailMessageForSamples(User user, String message,String subject) {

    SimpleMailMessage emailMessage = new SimpleMailMessage();
    emailMessage.setSubject(subject);
    emailMessage.setText(message);
    emailMessage.setTo(user.getEmail());
    emailService.queueMessage(emailMessage);

   sendEmail(emailMessage);
    return emailMessage;
  }

  private String generateUUID() {
    return Encoder.hash(UUID.randomUUID().toString());
  }

  public ArrayList<User> filterForActiveUsers(List<User> userList) {
    Set<User> users = new LinkedHashSet<>(userList);
    CollectionUtils.filter(users, new Predicate() {
      @Override
      public boolean evaluate(Object o) {
        return ((User) o).getActive();
      }
    });
    return new ArrayList<>(users);
  }

  public Integer getTotalSearchResultCount(String searchParam) {
    return userRepository.getTotalSearchResultCount(searchParam);
  }

  @Transactional
  public String updateUserPreferences(Long userId, User user, Long programId, Long facilityId, List<Long> products) {
    return userRepository.updateUserPreferences(userId, user, programId, facilityId, getCommaSeparatedIds(products));

  }

//  public List<String> getSupervisoryRights(Long userId) {
//    return userRepository.getSupervisoryRights(userId);
//  }


  public String createSiteUser(User user) {
    if (userRepository.createSiteUser(user) == 1) {
      roleAssignmentService.assignRolesToRegisteredUser(user.getId());
      String token = getSMSValidationCode(user.getCellPhone());
      userRepository.saveVerificationToken(user.getId(), token);
      sendSMS(user.getCellPhone(),null, token);
      return "SUCCESS";
    } else {
      return "ERROR";
    }
  }

  public boolean sendSMS(String number,String labName, String message) {


    RestTemplate template = new RestTemplate();

    String auth = "SunAdmin" + ":" + "@#TeChn16&";
    byte[] encodedAuth = Base64.encodeBase64(
            auth.getBytes(Charset.forName("US-ASCII")));
    String authHeader = "Basic " + new String(encodedAuth);
    String url = "https://api.infobip.com/sms/1/text/single";
    Sms sms = new Sms();
    sms.setFrom(labName);
    sms.setTo(number);
    sms.setText(message);

    MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
    headers.add("Authorization", authHeader);
    headers.add("Content-Type", "application/json");
    template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

    HttpEntity<Sms> request = new HttpEntity<Sms>(sms, headers);
    template.postForObject(url, request, Sms.class);

    return true;

  }

  private String getSMSValidationCode(String cellPhone) {
    String[] numbers = cellPhone.split("");
    int j = numbers.length;
    int x = 0;
    for (String number : numbers) {
      x = x + Integer.parseInt(number);
    }
    String token = new Integer(x).toString();
    return token.concat(token);
  }

  public Integer verify(String token) {
    return userRepository.verify(token);
  }

  public User getByUsername(String username) {
    return userRepository.getByUserName(username);
  }

  public User getEmail(String email) {
    return userRepository.getByEmail(email);
  }

  public User getByCellPhone(String cellPhone) {
    return userRepository.getByCellPhone(cellPhone);
  }

  public User getByFacilityId(Long facilityId) {
    return userRepository.getByFacilityId(facilityId);
  }
  public List<Partner>getAllPartners(){
    return userRepository.getAllPartners();
  }
}