package com.openldr.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by hassan on 7/9/16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AccountPackage extends BaseModel {

    String name;
    Date terminationDate;
    Boolean status;
    String description;
    Integer displayOrder;
    Integer period;
    Double price;
}
