
package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.upload.Importable;
import com.openldr.upload.annotation.ImportField;
import com.openldr.upload.annotation.ImportFields;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * FacilityTypeApprovedProduct represents the product approved by the facility type for a particular program. Also defines contract for upload of this
 * mapping. Facility type, program code, product code and maximum months of stock that can be stocked for this product by
 * the facility type is mandatory for upload of such mapping.
 */
@Data
@NoArgsConstructor
@JsonSerialize(include = NON_EMPTY)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FacilityTypeApprovedProduct extends BaseModel implements Importable {

  @ImportField(mandatory = true, name = "Facility Type Code", nested = "code")
  private FacilityType facilityType;
  private Long facilityTypeId;

  @ImportFields(importFields = {
    @ImportField(name = "Program Code", nested = "program.code", mandatory = true),
    @ImportField(name = "Product Code", nested = "product.code", mandatory = true)})
  private ProgramProduct programProduct;
  private Long programProductId;

  @ImportField(name = "Max months of stock", mandatory = true, type = "double")
  private Double maxMonthsOfStock;

  @ImportField(name = "Min months of stock", type = "double")
  private Double minMonthsOfStock;

  @ImportField(name = "Emergency order point", type = "double")
  private Double eop;

  public FacilityTypeApprovedProduct(FacilityType facilityType,
                                     ProgramProduct programProduct,
                                     Double maxMonthsOfStock) {
    this.facilityType = facilityType;
    this.maxMonthsOfStock = maxMonthsOfStock;
    this.setProgramProduct(programProduct);
  }

  public FacilityTypeApprovedProduct(String facilityTypeCode, ProgramProduct programProduct, Double maxMonthsOfStock) {
    this(new FacilityType(facilityTypeCode), programProduct, maxMonthsOfStock);
  }
}
