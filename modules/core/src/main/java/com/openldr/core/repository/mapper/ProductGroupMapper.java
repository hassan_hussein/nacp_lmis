
package com.openldr.core.repository.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import com.openldr.core.domain.ProductGroup;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ProductGroupMapper maps the ProductGroup entity to corresponding representation in database.
 */
@Repository
public interface ProductGroupMapper {

  @Insert("INSERT INTO product_groups(code, name, createdBy, modifiedDate, modifiedBy) VALUES (#{code}, #{name}, #{createdBy}, #{modifiedDate}, #{modifiedBy})")
  @Options(useGeneratedKeys = true)
  public void insert(ProductGroup productGroup);

  @Select("SELECT * FROM product_groups WHERE code=#{code}")
  ProductGroup getByCode(String code);

  @Update("UPDATE product_groups SET code = #{code}, name = #{name}, modifiedDate = #{modifiedDate}, modifiedBy = #{modifiedBy} WHERE id = #{id}")
  void update(ProductGroup productGroup);

  @Select("SELECT * FROM product_groups WHERE id=#{id}")
  ProductGroup getById(Long id);

  @Select("SELECT * FROM product_groups")
  List<ProductGroup> getAll();
}
