package com.openldr.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by hassan on 3/25/17.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Sms extends BaseModel {

    private String from;
    private String to;
    private String text;

    private String message;
    private String phoneNumber;
    private String direction;
    private Date dateSaved;
    private Boolean sent;
}
