
package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.upload.Importable;
import com.openldr.upload.annotation.ImportField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPriceSchedule extends BaseModel implements Importable {

  @ImportField(mandatory = true, name = "Product code", nested = "code")
  private Product product;

  @ImportField(mandatory = true, name = "Price Schedule", nested = "code")
  private PriceSchedule priceSchedule;

  @ImportField(mandatory = false, name = "Product name")
  private String productName;

  @ImportField(mandatory = false, name = "Price")
  private Double price;

}
