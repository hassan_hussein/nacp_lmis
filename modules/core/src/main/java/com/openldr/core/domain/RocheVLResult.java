
package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.exception.DataException;
import com.openldr.upload.Importable;
import com.openldr.upload.annotation.ImportField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;
import static org.apache.commons.lang.StringUtils.isBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RocheVLResult extends BaseModel implements Importable {

  @ImportField(name = "Order Number", mandatory = true)
  private String orderNumber;

  @ImportField( name = "Order Date/Time")
  private String stringTestDate;

  @ImportField( name = "Sample ID", mandatory = false)
  private String sampleId;

  @ImportField(mandatory = true, name = "Result")
  private String result;

  @ImportField( name = "Unit")
  private String unit;

  @ImportField( name = "Accepted Date/Time")
  private String stringResultDate;

  @ImportField(name = "Batch ID")
  private String batchID;

  private Long vlSampleId;

  private Date testDate;

  private Date resultDate;


  public RocheVLResult(String orderNumber, String sampleId, String result, String unit,String stringTestDate,String stringResultDate,String batchID) {
    this.orderNumber = orderNumber;
    this.sampleId = sampleId;
    this.result = result;
    this.unit = unit;
    this.stringTestDate = stringTestDate;
    this.stringResultDate = stringResultDate;
    this.batchID = batchID;
  }

  public Date getTestDate() {
    try {
        DateFormat df = new SimpleDateFormat("yyyy/mm/dd HH:mm:ss");
        Date testDate = df.parse(getStringTestDate());
        return testDate;
    }catch (Exception e){
      return null;
    }
  }

  public Date getResultDate()  {
    try {
        DateFormat df = new SimpleDateFormat("yyyy/mm/dd HH:mm:ss");
        Date resultDate = df.parse(getStringResultDate());
        return resultDate;
    }catch (Exception e){
      return null;
    }
  }

  public void validateMandatoryFields() {
    if (isBlank(this.orderNumber)  || isBlank(this.result)) {
      throw new DataException("error.mandatory.fields.missing");
    }
  }
}
