package com.openldr.core.repository.mapper;

import com.openldr.core.dto.CounselorRecord;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 10/14/17.
 */

@Repository
public interface CounselorMapper {

//    @Insert(" INSERT INTO counselor_records(\n" +
//            "             firstName, middleName, lastName, phoneNumber, providerid, \n" +
//            "            mohSwId, createdBy, createddate)\n" +
//            "    VALUES (#{firstName}, #{middleName}, #{lastName}, #{phoneNumber}, #{providerId}, \n" +
//            "            #{mohSwId}, #{createdBy}, NOW()) ")
//    @Options(useGeneratedKeys = true)
//    Integer insert(CounselorRecord record);
//
//    @Update(" update counselor_records set firstName = #{firstName}, middleName = #{middleName}," +
//            " lastName = #{lastName},phoneNumber = #{phoneNumber}, providerid = #{providerId}" +
//            " where id = #{id} ")
//    void update(CounselorRecord record);

    @Select(" select * from counselor_records")
    List<CounselorRecord>getAll();

    @Select(" select * from  counselor_records where id = #{id}")
    @Results(value = {
            @Result(property = "providerId", column = "providerId", javaType = Long.class,
                    one = @One(select = "com.openldr.core.repository.mapper.CounselorServiceMapper.getById")),
            @Result(property = "mohSwId", column = "mohSwId", javaType = Long.class,
                    one = @One(select = "com.openldr.core.repository.mapper.HFRMapper.getById"))
    })
    CounselorRecord getAllById(Long id);
     @Select(" select * from  counselor_records where phoneNumber = #{phoneNumber} and firstName=#{firstName} ")
    void getByFirstNameAndPhone(@Param("phoneNumber") String phoneNumber, @Param("firstName") String firstName);
}
