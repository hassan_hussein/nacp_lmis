

package com.openldr.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import com.openldr.core.exception.DataException;

import java.util.List;

import static com.google.common.collect.Iterables.any;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.isBlank;
import static com.openldr.core.domain.RightName.*;
import static com.openldr.core.utils.RightUtil.contains;
import static com.openldr.core.utils.RightUtil.with;

/**
 * Role represents Role entity which is a set of rights. Also provides methods to validate if a role contains related rights.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Role extends BaseModel {
  private String name;
  private String description;
  private List<Right> rights;

  public Role(String name, String description) {
    this(name, description, null);
  }

  public void validate() {
    if (isBlank(name)) throw new DataException("error.role.without.name");
    if (rights == null || rights.isEmpty())
      throw new DataException("error.role.without.rights");
    validateForRelatedRights();
  }

  private void validateForRelatedRights() {
    if (any(rights, contains(asList(CREATE_REQUISITION, APPROVE_REQUISITION, AUTHORIZE_REQUISITION)))
      && (!any(rights, with(VIEW_REQUISITION)))) {
      throw new DataException("error.role.related.right.not.selected");
    }
    if (any(rights, contains(asList(CONVERT_TO_ORDER, MANAGE_POD, FACILITY_FILL_SHIPMENT)))
      && (!any(rights,with(VIEW_ORDER)))) {
      throw new DataException("error.role.related.right.not.selected");
    }
  }
}
