package com.openldr.core.upload;

import com.openldr.core.domain.BaseModel;
import com.openldr.core.service.CounselorService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by hassan on 10/14/17.
 */

@Component
@NoArgsConstructor
public class CounselorPersistenceHandler extends AbstractModelPersistenceHandler {
@Autowired
private CounselorService service;

    @Override
    protected BaseModel getExisting(BaseModel record) {
        return null;
    }

    @Override
    protected void save(BaseModel record) {

    }
}
