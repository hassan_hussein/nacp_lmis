package com.openldr.core.service;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.HIVTestType;
import com.openldr.core.repository.HIVTestTypeRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class HIVTestTypeService {

    @Autowired
    HIVTestTypeRepository repository;

    @Autowired
    FacilityService facilityService;

    public void insert(HIVTestType type){
        repository.insert(type);
    }

    public void update(HIVTestType type)
    {
        repository.update(type);
    }

    public HIVTestType getById(Long id)
    {
        return  repository.getById(id);
    }
 public HIVTestType getByName(String name)
    {
        return  repository.getByName(name);
    }

    public List<HIVTestType> getAll(){
        return repository.getAll();
    }

    public void save(HIVTestType type) {
        if(type.getId() ==null)
            insert(type);
        else
            update(type);
    }

    public void setDefault(Long typeId){
        repository.resetDefault();
        repository.setDefault(typeId);
    }

    public void setFacilityDefault(Long userId,Long typeId){
        Facility facility=facilityService.getHomeFacility(userId);
        repository.updateFacilityDefault(facility.getId(),typeId);
    }

    public HIVTestType getDefault(){
        return repository.getDefault();
    }
    public HIVTestType getFacilityDefault(Long userId){
        HIVTestType hivTestType;
        Facility homeFacility=facilityService.getHomeFacility(userId);
        Long typeId=repository.getFacilityDefault(homeFacility.getId());
        if(typeId == null) {
            hivTestType = getDefault();
            repository.setFacilityDefault(homeFacility.getId(),hivTestType.getId());
            return hivTestType;
        }
        else {
            return getById(typeId);
        }
    }

}
