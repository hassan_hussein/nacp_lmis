/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2013 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

package com.openldr.core.service;

import com.openldr.core.domain.Pagination;
import com.openldr.core.domain.RocheVLResult;
import com.openldr.core.dto.AutomaticUploadResultDTO;
import com.openldr.core.dto.LogTagDTO;
import com.openldr.core.dto.SampleStatusChangeDTO;
import com.openldr.core.dto.SampleStatusDTO;
import com.openldr.core.repository.RocheVLResultRepository;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Exposes the services for handling GeographicZone entity.
 */

@Service
@NoArgsConstructor
@Slf4j
public class RocheVLResultService {


  @Autowired
  RocheVLResultRepository repository;


  public static final String RESULTED_STATUS="RESULTED";


  @Transactional
  public void save(RocheVLResult rocheVLResult) {
    System.out.println(rocheVLResult);

    rocheVLResult.validateMandatoryFields();
    Long sampleId=repository.getSampleId(rocheVLResult.getSampleId());
    if(sampleId != null) {
      rocheVLResult.setVlSampleId(sampleId);
      repository.save(rocheVLResult);
      repository.updateSampleStatus(RESULTED_STATUS,rocheVLResult.getTestDate(),rocheVLResult.getResultDate(), sampleId);
      Long sampleStatusId = repository.getStatusById(sampleId);
      if(sampleStatusId == null){
        SampleStatusChangeDTO dto = new SampleStatusChangeDTO();
        dto.setSampleId(sampleId);
        dto.setStatus(SampleStatusDTO.RESULTED);
        dto.setCreatedBy(1L);
        dto.setModifiedBy(1L);
        repository.insertStatus(dto);
      }else {
        SampleStatusChangeDTO dto = new SampleStatusChangeDTO();
        dto.setStatus(SampleStatusDTO.RESULTED);
        dto.setSampleId(sampleId);
        dto.setCreatedBy(1L);
        dto.setModifiedBy(1L);
        dto.setId(sampleStatusId);
        repository.updateStatus(dto);
      }
    }
  }

  public RocheVLResult getByLabNumber(RocheVLResult record) {
    return repository.getByLabNumber(record.getSampleId());
  }

  @Transactional
  public void saveAutoUpload(AutomaticUploadResultDTO resultDto){
    log.info("{}", resultDto);
    resultDto.validateMandatoryFields();
    Long sampleId = repository.getSampleId(resultDto.getSampleId());
    if(sampleId!=null){
      resultDto.setVlSampleId(sampleId);
      repository.saveAutoUpload(resultDto);
      repository.updateSampleStatus(RESULTED_STATUS, resultDto.getTestDate(), resultDto.getResultDate(), sampleId);

        Long sampleStatusId = repository.getStatusById(sampleId);
        SampleStatusChangeDTO dto = new SampleStatusChangeDTO();
        dto.setSampleId(sampleId);
        dto.setStatus(SampleStatusDTO.RESULTED);
        dto.setCreatedBy(1L);
        dto.setModifiedBy(1L);
        if(sampleStatusId == null){
            repository.insertStatus(dto);
        }else {
            repository.updateStatus(dto);
        }
    }
  }
 @Transactional
  public void saveAutoUplaod(AutomaticUploadResultDTO resultDTO){
    log.info("",resultDTO);
    resultDTO.validateMandatoryFields();
    Long sampleId=repository.getSampleId(resultDTO.getSampleId());
    if(sampleId != null) {
      resultDTO.setVlSampleId(sampleId);
      repository.saveAutoUpload(resultDTO);
      repository.updateSampleStatus(RESULTED_STATUS,resultDTO.getTestDate(),resultDTO.getResultDate(), sampleId);
      Long sampleStatusId = repository.getStatusById(sampleId);
      if(sampleStatusId == null){
         SampleStatusChangeDTO dto = new SampleStatusChangeDTO();
         dto.setSampleId(sampleId);
         dto.setStatus(SampleStatusDTO.RESULTED);
        dto.setCreatedBy(1L);
        dto.setModifiedBy(1L);
        repository.insertStatus(dto);
      }else {
        SampleStatusChangeDTO dto = new SampleStatusChangeDTO();
        dto.setStatus(SampleStatusDTO.RESULTED);
        dto.setSampleId(sampleId);
        dto.setCreatedBy(1L);
        dto.setModifiedBy(1L);
        dto.setId(sampleStatusId);
        repository.updateStatus(dto);
      }
    }

  }

  public void sendResultToSampleCollection(AutomaticUploadResultDTO resultDTO){
    System.out.println(resultDTO);
    resultDTO.validateMandatoryFields();
    Long sampleId=repository.getSampleId(resultDTO.getSampleId());
    if(sampleId !=null){
      resultDTO.setVlSampleId(sampleId);

    }

  }

@Transactional
  public void saveLogTag(List<LogTagDTO> dtos) {

  for (LogTagDTO dto :dtos) {

    if (dto != null) {
    /*  LogTagDTO logData;
      logData = repository.getByDateAndSerialNumber(dto.getLogDate(), dto.getSerialNumber());
      if (logData != null) {
        logData.setId(logData.getId());
        repository.updateLogTag(dto);
      } else*/
        repository.saveLogTagData(dto);

    }

  }

  }


  public List<LogTagDTO> getLogTags(String startDate, String endDate, Pagination pagination) {
    return repository.geLogTags(startDate, endDate,pagination);
  }
}
