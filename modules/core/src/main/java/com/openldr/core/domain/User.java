/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2013 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.exception.DataException;
import com.openldr.core.hash.Encoder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * User represents User of the system and its attributes. Defines the contract for creation/upload of users, mandatory
 * attributes, their data type etc. Also provides methods to validate user and user email.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends BaseModel{

  private String userName;

  private String password;

  private String firstName;

  private String lastName;

  private String employeeId;

  private String jobTitle;

  private String primaryNotificationMethod;

  private String officePhone;

  private String cellPhone;

  private String email;

  private User supervisor;

  private Long geographicZoneId;

  private UserGeographicZone geographicZone;

  private Boolean restrictLogin;

  private Long facilityId;
  private List<RoleAssignment> supervisorRoles;
  private List<RoleAssignment> homeFacilityRoles;
  private List<RoleAssignment> allocationRoles;

  private RoleAssignment reportRoles;

  private RoleAssignment adminRole;

  private RoleAssignment reportingRole;

  private RoleAssignment labRole;

  private RoleAssignment hubRole;

  private RoleAssignment clientRole;

  private Boolean verified;

  private Boolean active;

  private Boolean isMobileUser = false;

  private Boolean isFacilityUser = false;

  private Long packageId;

  private Long userTypeId;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  private Date dateOfBirth;

  private String userNumber;

  private String gender;

  private Integer altenativeMobileNumber;

  private Boolean isAgent;

  private Integer extensionNumber;

  private Integer extensionNumberId;

  private Integer groupId;

  private Long landingPageId;

  private Integer partnerId;

  public User(Long id, String userName) {
    this.id = id;
    this.userName = userName;
  }

  public User(String userName) {
    this(null, userName);
  }

  public Boolean isMobileUser() {
    return this.isMobileUser;
  }

  public Boolean isAgent() {
    return this.isAgent;
  }

  public String dateOfBirth() {
    return getFormattedDate(this.dateOfBirth);
  }

  public void validate() {
    validateEmail();
    validateUserName();
  }

  public void setPassword(String password) {
    this.password = Encoder.hash(password);
  }

  private void validateEmail() {
    final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    if (email != null && !pattern.matcher(email).matches())
      throw new DataException("user.email.invalid");
  }

  private void validateUserName() {
    if (userName != null && userName.trim().contains(" "))
      throw new DataException("user.userName.invalid");
  }

  public User basicInformation() {
    return new User(id, userName);
  }


}
