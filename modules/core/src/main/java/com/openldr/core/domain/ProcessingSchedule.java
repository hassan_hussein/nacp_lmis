package com.openldr.core.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import com.openldr.core.exception.DataException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * ProcessingSchedule represents time schedule requisition life cycle will follow. Each processing schedule consists
 * of periods for which requisitions are initiated and submitted.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessingSchedule extends BaseModel {
  private String code;
  private String name;
  private String description;

  public ProcessingSchedule(String code, String name) {
    this(code, name, null);
  }

  public ProcessingSchedule(Long id) {
    this.id = id;
  }

  public void validate() {
    if (code == null || code.isEmpty()) {
      throw new DataException("schedule.without.code");
    }
    if (name == null || name.isEmpty()) {
      throw new DataException("schedule.without.name");
    }
  }

  @SuppressWarnings("unused")
  public String getStringModifiedDate() throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    return this.modifiedDate == null ? null : simpleDateFormat.format(this.modifiedDate);
  }

}
