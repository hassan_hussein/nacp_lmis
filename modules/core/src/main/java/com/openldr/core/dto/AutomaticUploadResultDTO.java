package com.openldr.core.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.domain.BaseModel;
import com.openldr.core.exception.DataException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;
import static org.apache.commons.lang.StringUtils.isBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AutomaticUploadResultDTO extends BaseModel {

    private String orderNumber;

    private String stringTestDate;

//    @JsonProperty("sampleId")
    private String sampleId;

    private String result;

    private String unit;

    private String stringResultDate;

    private String batchID;

    private String orderedBy;

    private String machineName;

    private String machineSerial;

    @JsonIgnoreProperties
    private Long vlSampleId;

//    @JsonProperty("orderDate")
//    private String testDateStr;
//
//    private Date testDate;
//
//    @JsonProperty("acceptedDate")
//    private String resultDateStr;

//    private Date resultDate;


    @JsonIgnore
    private Date resultDate;
    @JsonIgnore
    private Date testDate;
    private String orderDate;
    private String acceptedDate;

//    public AutomaticUploadResultDTO(
//            String orderNumber,
//            String sampleId,
//            String result,
//            String unit,
//            String stringTestDate,
//            String stringResultDate,
//            String batchID,
//            int l) {
//        this.orderNumber = orderNumber;
//        this.sampleId = sampleId;
//        this.result = result;
//        this.unit = unit;
//        this.stringTestDate = stringTestDate;
//        this.stringResultDate = stringResultDate;
//        this.batchID = batchID;
////        this.machineName = machineName;
////        this.machineSerial = machineSerial;
////        this.orderedBy = orderedBy;
//    }

    public Date getTestDate() {
        try {
            DateFormat df = null;
            if(getMachineName().equalsIgnoreCase("m2000")){

                df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            }else{
                df = new SimpleDateFormat("yyyyMMddHHmmss");
            }
            return df.parse(getOrderDate());
        }catch (Exception e){return null;
        }
    }

    public Date getResultDate()  {
        try {
            DateFormat df = null;
            if(getMachineName().equalsIgnoreCase("m2000")){
                df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            }else{
                df = new SimpleDateFormat("yyyyMMddHHmmss");
            }
           return df.parse(getAcceptedDate());
        }catch (Exception e){
            return new Date();
        }
    }

    public String getMachineName(){
        return this.machineName
                .replace("\r","")
                .replace("\u0003","")
                .replace("\n","")
                .replace("\u0002","");
    }

    public void validateMandatoryFields() {
        if (isBlank(this.orderNumber)  || isBlank(this.result)) {
            throw new DataException("error.mandatory.fields.missing");
        }
    }

}
