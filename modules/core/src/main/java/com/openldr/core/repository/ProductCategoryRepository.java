package com.openldr.core.repository;

import com.openldr.core.domain.ProductCategory;
import com.openldr.core.exception.DataException;
import com.openldr.core.repository.mapper.ProductCategoryMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chrispinus on 3/15/16.
 */
@Repository
@NoArgsConstructor
public class ProductCategoryRepository {

    private ProductCategoryMapper mapper;

    @Autowired
    public ProductCategoryRepository(ProductCategoryMapper mapper) {
        this.mapper = mapper;
    }

    public void insert(ProductCategory productCategory) {
        try {
            mapper.insert(productCategory);
        } catch (DuplicateKeyException duplicateKeyException) {
            throw new DataException("product.category.name.duplicate");
        } catch (DataIntegrityViolationException dataIntegrityViolationException) {
            String errorMessage = dataIntegrityViolationException.getMessage().toLowerCase();
            if (errorMessage.contains("foreign key") || errorMessage.contains("violates not-null constraint")) {
                throw new DataException("error.reference.data.missing");
            } else {
                throw new DataException("error.incorrect.length");
            }
        }
    }

    public void update(ProductCategory productCategory) {
        mapper.update(productCategory);
    }

    public ProductCategory getExisting(ProductCategory productCategory) {
        return mapper.getByCode(productCategory.getCode());
    }

    public Long getIdByCode(String categoryCode) {
        return mapper.getIdByCode(categoryCode);
    }

    public ProductCategory getByCode(String code) {
        return mapper.getByCode(code);
    }

    public List<ProductCategory> getAll() {
        return mapper.getAll();
    }

    public ProductCategory getById(Long id){
        return mapper.getById(id);
    }
}
