package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.utils.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Created by chrispinus on 3/14/16.
 */
@Getter
@Setter
@NoArgsConstructor
@JsonSerialize
public abstract class BaseModel {
    protected Long id;

    @JsonIgnore
    protected Long createdBy;

    @JsonIgnore
    protected Long modifiedBy;

    @JsonIgnore
    protected Date createdDate;

    @JsonIgnore
    protected Date modifiedDate;

    public BaseModel(Long id) {
        this.id = id;
    }

    protected static String getFormattedDate(Date date) {
        return DateUtil.getFormattedDate(date, "yyyy-dd-MM");
    }


    public boolean hasId() {
        return (null == id) ? false : true;
    }
}
