package com.openldr.core.repository.mapper;

import com.openldr.core.domain.UserGroup;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 7/30/16.
 */
@Repository
public interface UserGroupMapper {

    @Select(" select * from user_groups ")
    List<UserGroup> getAll();

}
