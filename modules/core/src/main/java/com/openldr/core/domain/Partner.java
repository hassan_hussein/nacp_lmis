package com.openldr.core.domain;

import lombok.Data;

/**
 * Created by hassan on 11/12/17.
 */
@Data
public class Partner extends BaseModel {

    private String code;
    private String name;

}
