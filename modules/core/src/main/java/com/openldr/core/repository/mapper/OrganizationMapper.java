package com.openldr.core.repository.mapper;

import com.openldr.core.domain.Organization;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 7/9/16.
 */

@Repository
public interface OrganizationMapper {

    @Insert({"INSERT INTO organizations(name)  " +
            "    VALUES (#{name}) "})
    @Options(useGeneratedKeys = true)
    Integer Insert(Organization organization);

    @Select(" SELECT  * " +
            "  FROM organizations WHERE id = #{Id}")
    Organization getById(Long Id);

    @Update("  UPDATE organizations  " +
            "  name=#{name}  " +
            " WHERE id = #{id} ")
    void update(Organization organization);

    @Select(" select * from organizations ")
    List<Organization> getAll();
}
