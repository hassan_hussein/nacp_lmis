package com.openldr.core.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_NULL;

/**
 * Created by hassan on 7/28/16.
 */
@Getter
@Setter
@NoArgsConstructor
@JsonSerialize(include = NON_NULL)
public class ExtensionNumber extends BaseModel {

    private Long extensionNumber;
    private String description;
    private Long displayOrder;
}
