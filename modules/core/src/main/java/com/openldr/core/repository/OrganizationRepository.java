package com.openldr.core.repository;

import com.openldr.core.domain.Organization;
import com.openldr.core.repository.mapper.OrganizationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 7/9/16.
 */
@Repository
public class OrganizationRepository {

    @Autowired
    public OrganizationMapper mapper;

    public Integer Insert(Organization organization) {
        return mapper.Insert(organization);
    }

    public Organization getById(Long Id) {
        return mapper.getById(Id);
    }

    public void update(Organization organization) {
        mapper.update(organization);
    }

    public List<Organization> getAll() {
        return mapper.getAll();
    }


}
