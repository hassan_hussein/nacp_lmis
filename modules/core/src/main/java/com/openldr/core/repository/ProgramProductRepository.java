package com.openldr.core.repository;

import com.openldr.core.domain.Pagination;
import com.openldr.core.domain.Program;
import com.openldr.core.domain.ProgramProduct;
import com.openldr.core.exception.DataException;
import com.openldr.core.repository.mapper.ProgramProductMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ProgramProductRepository is Repository class for DeliveryZoneMember related database operations.
 */

/*@Repository
@NoArgsConstructor*/

//@NoArgsConstructor
@Repository
/*
@NoArgsConstructor
*/
public class ProgramProductRepository {

  @Autowired
  private ProgramProductMapper mapper;

  @Autowired
  private ProgramRepository programRepository;

  @Autowired
  private ProductRepository productRepository;

 /* @Autowired
  private ProgramProductPriceMapper programProductPriceMapper;

  @Autowired
  private ProgramProductIsaMapper programProductIsaMapper;*/

  public void save(ProgramProduct programProduct) {
    Long programId = programRepository.getIdByCode(programProduct.getProgram().getCode());
    programProduct.getProgram().setId(programId);

    validateProductCode(programProduct.getProduct().getCode());

    Long productId = productRepository.getIdByCode(programProduct.getProduct().getCode());
    programProduct.getProduct().setId(productId);

    try {
      if (programProduct.getId() == null) {
          System.out.println("-------------------");
          System.out.println(programProduct);
        mapper.Insert(programProduct);
      } else {
        mapper.update(programProduct);
      }
    } catch (DuplicateKeyException duplicateKeyException) {
      throw new DataException("error.duplicate.product.code.program.code");
    }
  }

  public Long getIdByProgramIdAndProductId(Long programId, Long productId) {
    Long programProductId = mapper.getIdByProgramAndProductId(programId, productId);

    if (programProductId == null)
      throw new DataException("programProduct.product.program.invalid");

    return programProductId;
  }

  private void validateProductCode(String code) {
    if (code == null || code.isEmpty() || productRepository.getIdByCode(code) == null) {
      throw new DataException("product.code.invalid");
    }
  }

  public void updateCurrentPrice(ProgramProduct programProduct) {
    mapper.updateCurrentPrice(programProduct);
  }

  public ProgramProduct getByProgramAndProductCode(ProgramProduct programProduct) {
    return getByProgramAndProductId(programRepository.getIdByCode(programProduct.getProgram().getCode()),
      productRepository.getIdByCode(programProduct.getProduct().getCode()));
  }

  public ProgramProduct getByProgramAndProductId(Long programId, Long productId) {
    return mapper.getByProgramAndProductId(programId, productId);
  }

 /* public void updatePriceHistory(ProgramProductPrice programProductPrice) {
    programProductPriceMapper.closeLastActivePrice(programProductPrice);
    programProductPriceMapper.insertNewCurrentPrice(programProductPrice);
  }
*/
  public void updateProgramProduct(ProgramProduct programProduct) {
    mapper.update(programProduct);
  }

  //public ProgramProductPrice getProgramProductPrice(ProgramProduct programProduct) {
   // return programProductPriceMapper.get(programProduct);
  //}

  public List<ProgramProduct> getByProgram(Program program) {
    return mapper.getByProgram(program);
  }

  public ProgramProduct getById(Long id) {
    return mapper.getById(id);
  }

  public List<ProgramProduct> getByProductCode(String code) {
    return mapper.getByProductCode(code);
  }

  public List<ProgramProduct> getProgramProductsBy(Long programId, String facilityTypeCode) {
    return mapper.getByProgramIdAndFacilityTypeCode(programId, facilityTypeCode);
  }

  public List<ProgramProduct> getNonFullSupplyProductsForProgram(Program program) {
    return mapper.getNonFullSupplyProductsForProgram(program);
  }

  public List<ProgramProduct> searchByProgram(String searchParam, Pagination pagination) {
    return mapper.searchByProgram(searchParam, pagination);
  }

  public Integer getTotalSearchResultCount(String searchParam) {
    return mapper.getTotalSearchResultCount(searchParam);
  }

  public List<ProgramProduct> searchByProduct(String searchParam, Pagination pagination) {
    return mapper.searchByProduct(searchParam, pagination);
  }

  public List<ProgramProduct> getActiveByProgram(Long programId) {
    return mapper.getActiveByProgram(programId);
  }

/*  @Transactional
  public void insertISA(ProgramProductISA programProductISA)
  {
    programProductIsaMapper.insert(programProductISA);
  }

  public void updateISA(ProgramProductISA programProductISA) {
    programProductIsaMapper.update(programProductISA);
  }*/
}
