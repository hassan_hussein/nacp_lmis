package com.openldr.core.upload;

import com.openldr.core.domain.BaseModel;
import com.openldr.core.exception.DataException;
import com.openldr.core.message.OpenLDRMessage;
import com.openldr.core.service.MessageService;
import com.openldr.upload.Importable;
import com.openldr.upload.RecordHandler;
import com.openldr.upload.model.AuditFields;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

/**
 * AbstractModelPersistenceHandler is a base class used for persisting each record of the uploaded file.
 */
@Component
@NoArgsConstructor
public abstract class AbstractModelPersistenceHandler implements RecordHandler<Importable> {

  @Autowired
  MessageService messageService;

  protected abstract BaseModel getExisting(BaseModel record);

  protected abstract void save(BaseModel record);

  @Getter
  @Setter
  String messageKey;

  @Override
  public void execute(Importable importable, int rowNumber, AuditFields auditFields) {
    BaseModel currentRecord = (BaseModel) importable;
    BaseModel existing = getExisting(currentRecord);

    try {
      throwExceptionIfProcessedInCurrentUpload(auditFields, existing);
      currentRecord.setModifiedBy(auditFields.getUser());
      currentRecord.setModifiedDate(auditFields.getCurrentTimestamp());
      if (existing != null) {
        currentRecord.setId(existing.getId());
      } else {
        currentRecord.setCreatedBy(auditFields.getUser());
      }

      save(currentRecord);

    } catch (DataIntegrityViolationException dataIntegrityViolationException) {
      throwException("upload.record.error", "error.incorrect.length", rowNumber);
    } catch (DataException exception) {
      throwException("upload.record.error", exception.getOpenLDRMessage().getCode(), rowNumber);
    }
  }

  private void throwException(String key1, String key2, int rowNumber) {
    String param1 = messageService.message(key2);
    String param2 = Integer.toString(rowNumber - 1);
    throw new DataException(new OpenLDRMessage(messageService.message(key1, param1, param2)));
  }

  private void throwExceptionIfProcessedInCurrentUpload(AuditFields auditFields, BaseModel existing) {
    if (existing != null) {
      if (auditFields.getCurrentTimestamp().equals(existing.getModifiedDate())) {
        throw new DataException(getMessageKey());
      }
    }
  }

  @Override
  public void postProcess(AuditFields auditFields) {
  }

}
