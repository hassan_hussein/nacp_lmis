package com.openldr.core.web.controller;

import com.openldr.core.domain.ConfigurationSettingKey;
import com.openldr.core.logging.ApplicationLogger;
import com.openldr.core.service.ConfigurationSettingService;
import com.openldr.core.service.MessageService;
import com.openldr.core.web.OpenLdrResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by hassan on 4/17/16.
 *
 */

public class BaseController {
    public static final String UNEXPECTED_EXCEPTION = "unexpected.exception";
    public static final String FORBIDDEN_EXCEPTION = "error.authorisation";
    public static final String ACCEPT_JSON = "Accept=application/json";
    public static final String ACCEPT_PDF = "Accept=application/pdf";
    public static final String ACCEPT_CSV = "Accept=*/*";
    private static Logger logger = LoggerFactory.getLogger(ApplicationLogger.class);

    @Autowired
    public MessageService messageService;

    @Autowired
    private ConfigurationSettingService settingService;

    protected Long loggedInUserId(HttpServletRequest request) {
        return (Long) request.getSession().getAttribute("USER_ID");
    }

    protected String homePageUrl() {
        String homePage = settingService.getConfigurationStringValue(ConfigurationSettingKey.LOGIN_SUCCESS_DEFAULT_LANDING_PAGE);
               return "redirect:" + homePage;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<OpenLdrResponse> handleException(Exception ex) {
        logger.error("something broke with following exception... ", ex);
        if (ex instanceof AccessDeniedException) {
            return OpenLdrResponse.error(messageService.message(FORBIDDEN_EXCEPTION), HttpStatus.FORBIDDEN);
        }
        return OpenLdrResponse.error(messageService.message(UNEXPECTED_EXCEPTION), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}