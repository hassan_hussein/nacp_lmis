package com.openldr.core.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SampleStatusChangeDTO extends BaseModel {

    public SampleStatusDTO status;
    private Long sampleId;

    public SampleStatusChangeDTO(Long sampleId, SampleStatusDTO sampleStatusToSave,Long userId){
        sampleId = sampleId;
        status = sampleStatusToSave;
        createdBy = userId;
        modifiedBy = userId;
    }


}
