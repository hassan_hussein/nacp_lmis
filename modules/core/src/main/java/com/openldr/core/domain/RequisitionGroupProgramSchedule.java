

package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.openldr.upload.Importable;
import com.openldr.upload.annotation.ImportField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/**
 * RequisitionGroupProgramSchedule represents the schedule to be mapped for a given program and requisition group
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequisitionGroupProgramSchedule extends BaseModel implements Importable {

  @ImportField(mandatory = true, name = "RG Code", nested = "code")
  private RequisitionGroup requisitionGroup;

  @ImportField(mandatory = true, name = "Program", nested = "code")
  private Program program;

  @ImportField(mandatory = true, name = "Schedule", nested = "code")
  private ProcessingSchedule processingSchedule;

  @ImportField(mandatory = true, name = "Direct Delivery")
  private boolean directDelivery;

  @ImportField(name = "Drop off Facility", nested = "code")
  private Facility dropOffFacility;
}
