
package com.openldr.core.service;

import com.openldr.core.domain.FacilityType;
import com.openldr.core.domain.FacilityTypeApprovedProduct;
import com.openldr.core.domain.Pagination;
import com.openldr.core.exception.DataException;
import com.openldr.core.repository.FacilityApprovedProductRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Exposes the services for handling FacilityApprovedProduct entity.
 */

@Service
@NoArgsConstructor
public class FacilityApprovedProductService {

  public static final String FACILITY_TYPE_DOES_NOT_EXIST = "facilityType.invalid";
  public static final String FACILITY_APPROVED_PRODUCT_DOES_NOT_EXIST = "facility.approved.product.does.not.exist";

  @Autowired
  private FacilityApprovedProductRepository repository;

  @Autowired
  private ProgramService programService;

  @Autowired
  private ProductService productService;

  @Autowired
  private ProgramProductService programProductService;

  @Autowired
  private FacilityService facilityService;

  public List<FacilityTypeApprovedProduct> getFullSupplyFacilityApprovedProductByFacilityAndProgram(Long facilityId, Long programId) {
    return repository.getFullSupplyProductsByFacilityAndProgram(facilityId, programId);
  }

  public List<FacilityTypeApprovedProduct> getNonFullSupplyFacilityApprovedProductByFacilityAndProgram(Long facilityId, Long programId) {
    return repository.getNonFullSupplyProductsByFacilityAndProgram(facilityId, programId);
  }

  public void save(FacilityTypeApprovedProduct facilityTypeApprovedProduct) {
    fillProgramProductIds(facilityTypeApprovedProduct);
    FacilityType facilityType = facilityService.getFacilityTypeByCode(facilityTypeApprovedProduct.getFacilityType());
    if (facilityType == null) throw new DataException(FACILITY_TYPE_DOES_NOT_EXIST);

    facilityTypeApprovedProduct.getFacilityType().setId(facilityType.getId());

    if (facilityTypeApprovedProduct.getId() != null) {
      if(repository.get(facilityTypeApprovedProduct.getId()) == null){
        throw new DataException(FACILITY_APPROVED_PRODUCT_DOES_NOT_EXIST);
      }
      repository.update(facilityTypeApprovedProduct);
    } else {
       repository.insert(facilityTypeApprovedProduct);
    }
  }

  public FacilityTypeApprovedProduct getFacilityApprovedProductByProgramProductAndFacilityTypeCode(FacilityTypeApprovedProduct facilityTypeApprovedProduct) {
    fillProgramProductIds(facilityTypeApprovedProduct);
    return repository.getFacilityApprovedProductByProgramProductAndFacilityTypeCode(facilityTypeApprovedProduct);
  }

  public List<FacilityTypeApprovedProduct> getAllBy(Long facilityTypeId, Long programId, String searchParam, Pagination pagination) {
    return repository.getAllBy(facilityTypeId, programId, searchParam, pagination);
  }

  public Integer getTotalSearchResultCount(Long facilityTypeId, Long programId, String searchParam) {
    return repository.getTotalSearchResultCount(facilityTypeId, programId, searchParam);
  }

  private void fillProgramProductIds(FacilityTypeApprovedProduct facilityTypeApprovedProduct) {
    Long programId = programService.getIdForCode(facilityTypeApprovedProduct.getProgramProduct().getProgram().getCode());
    Long productId = productService.getIdForCode(facilityTypeApprovedProduct.getProgramProduct().getProduct().getCode());
    Long programProductId = programProductService.getIdByProgramIdAndProductId(programId, productId);
    facilityTypeApprovedProduct.getProgramProduct().getProgram().setId(programId);
    facilityTypeApprovedProduct.getProgramProduct().getProduct().setId(productId);
    facilityTypeApprovedProduct.getProgramProduct().setId(programProductId);
  }

  public void saveAll(List<FacilityTypeApprovedProduct> facilityTypeApprovedProducts, Long userId) {
    for (FacilityTypeApprovedProduct facilityTypeApprovedProduct : facilityTypeApprovedProducts) {
      facilityTypeApprovedProduct.setCreatedBy(userId);
      save(facilityTypeApprovedProduct);
    }
  }

  public void delete(Long id) {
    repository.delete(id);
  }
}
