

package com.openldr.core.event;


/**
 * This class is responsible for generating a feed on the event of change in supported programs of a facility.
 */

public class ProgramChangeEvent {

  public static final String FEED_CATEGORY = "program-catalog-changes";


}
