package com.openldr.core.repository;

import com.openldr.core.domain.HIVTestType;
import com.openldr.core.repository.mapper.HIVTestTypeMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class HIVTestTypeRepository {

    @Autowired
    private HIVTestTypeMapper mapper;

    public void insert(HIVTestType type){
        mapper.insert(type);
    }

    public void update(HIVTestType type)
    {
        mapper.update(type);
    }

    public HIVTestType getById(Long id)
    {
        return  mapper.getById(id);
    }

    public HIVTestType getByName(String name)
    {
        return  mapper.getByName(name);
    }

    public List<HIVTestType> getAll(){
        return mapper.getAll();
    }


    public void resetDefault(){
        mapper.resetDefault();
    }

    public void setDefault(Long typeId)
    {
        mapper.setDefault(typeId);
    }

    public HIVTestType getDefault(){
        return mapper.getDefault();
    }

    public Long getFacilityDefault(Long facilityId){
        return mapper.getFacilityDefault(facilityId);
    }


    public void setFacilityDefault(Long facilityId, Long typeId) {
        mapper.setFacilityDefault(facilityId,typeId);
    }

    public void updateFacilityDefault(Long facilityId, Long typeId) {
        mapper.updateFacilityDefault(facilityId,typeId);
    }


}
