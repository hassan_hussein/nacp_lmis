package com.openldr.core.service;

import com.openldr.core.dto.CounselorRecord;
import com.openldr.core.repository.CounselorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by hassan on 10/14/17.
 */
@Service
public class CounselorService {

    @Autowired
    private CounselorRepository repository;

    public void save(CounselorRecord record){
        if(record.getId() == null){
            repository.insert(record);
        }else
            repository.update(record);

    }

    public void getByFirstNameAndPhone(String phoneNumber,String firstName){
       repository.getByFirstNameAndPhone(phoneNumber,firstName);
        
    }


}
