package com.openldr.core.upload;


import com.openldr.core.domain.BaseModel;
import com.openldr.core.domain.RocheVLResult;
import com.openldr.core.service.RocheVLResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * GeographicZonePersistenceHandler is used for uploads of GeographicZone. It uploads each GeographicZone
 * record by record.
 */
@Component
public class RocheVLResultPersistenceHandler extends AbstractModelPersistenceHandler {

  RocheVLResultService rocheVLResultService;

  @Autowired
  public RocheVLResultPersistenceHandler(RocheVLResultService rocheVLResultService) {
    this.rocheVLResultService = rocheVLResultService;
  }


  @Override
  protected BaseModel getExisting(BaseModel record) {
    return rocheVLResultService.getByLabNumber((RocheVLResult) record);
  }

  @Override
  protected void save(BaseModel record) {
    rocheVLResultService.save((RocheVLResult) record);
  }

  @Override
  public String getMessageKey() {
    return "Duplicate Code";
  }


}