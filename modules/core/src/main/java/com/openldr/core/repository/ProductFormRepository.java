
package com.openldr.core.repository;

import com.openldr.core.domain.ProductForm;
import com.openldr.core.repository.mapper.ProductFormMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ProductFormRepository is Repository class for ProductForm related database operations.
 */

@Repository
@NoArgsConstructor
public class ProductFormRepository {

  @Autowired
  ProductFormMapper mapper;

  public List<ProductForm> getAll() {
    return mapper.getAll();
  }

  public ProductForm getByCode(String code) {
    return mapper.getByCode(code);
  }
}
