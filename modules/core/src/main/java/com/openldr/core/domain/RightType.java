
package com.openldr.core.domain;


public enum RightType {

  ADMIN,
  LAB,
  HUB,
  REPORTING,
  REPORT

}
