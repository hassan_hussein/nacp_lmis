package com.openldr.core.service;

import com.openldr.core.domain.ProductCategory;
import com.openldr.core.repository.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chrispinus on 3/15/16.
 */
@Service
public class ProductCategoryService {

    private ProductCategoryRepository repository;

    @Autowired
    public ProductCategoryService(ProductCategoryRepository repository) {
        this.repository = repository;
    }

    public Long getProductCategoryIdByCode(String code) {
        return repository.getIdByCode(code);
    }

    public ProductCategory getByCode(String code) {
        return repository.getByCode(code);
    }

    public void save(ProductCategory productCategory) {
        if (productCategory.getId() != null) {
            repository.update(productCategory);
            return;
        }
        repository.insert(productCategory);
    }

    public ProductCategory getExisting(ProductCategory productCategory) {
        return repository.getExisting(productCategory);
    }

    public ProductCategory getById(Long id){
        return repository.getById(id);
    }

    public List<ProductCategory> getAll() {
        return repository.getAll();
    }


}
