package com.openldr.core.domain;

/**
 * Created by hassan on 4/17/16.
 */
public class ConfigurationSettingKey {

    public static final String LOGIN_SUCCESS_DEFAULT_LANDING_PAGE = "LOGIN_SUCCESS_DEFAULT_LANDING_PAGE";
    public static final String DEFAULT_ZERO = "DEFAULT_ZERO";



    private ConfigurationSettingKey(){

    }
}
