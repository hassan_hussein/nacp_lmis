
package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import java.util.List;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * FacilityProgramProduct represents product supported by given facility for a given program. This mapping is used by distribution module
 * to identify products supported by a facility and overriddenISA for that product in that facility.
 */
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = NON_EMPTY)
public class FacilityProgramProduct extends ProgramProduct {

  Long facilityId;

 /* public FacilityProgramProduct(ProgramProduct programProduct, Long facilityId)
  {
    this(programProduct, facilityId);
  }
*/
  public FacilityProgramProduct(ProgramProduct programProduct, Long facilityId)
  {
    super(programProduct);
    this.facilityId = facilityId;

  }

  @JsonIgnore
  public ProductGroup getActiveProductGroup() {
    if (this.getActive() && this.getProduct().getActive()) {
      return this.getProduct().getProductGroup();
    }
    return null;
  }

 /* public Integer calculateIsa(Long population, Integer numberOfMonthsInPeriod)
  {
    if(population == null)
      return null;

    Integer idealQuantity;
    if (this.overriddenIsa != null)
      idealQuantity = this.overriddenIsa.calculate(population);
    else if (this.programProductIsa != null)
      idealQuantity = this.programProductIsa.calculate(population);
    else
      return null;

    idealQuantity = Math.round(idealQuantity * ((float) numberOfMonthsInPeriod / this.getProduct().getPackSize()));
    return idealQuantity < 0 ? 0 : idealQuantity;
  }*/


 /* @JsonIgnore
  public Double getWhoRatio()
  {
    if(this.overriddenIsa != null)
      return overriddenIsa.getWhoRatio();
    else if(this.programProductIsa != null)
      return programProductIsa.getWhoRatio();
    else
      return null;
  }
*/
  public static List<FacilityProgramProduct> filterActiveProducts(List<FacilityProgramProduct> programProducts) {
    List<FacilityProgramProduct> activeProgramProducts = (List<FacilityProgramProduct>) CollectionUtils.select(programProducts, new Predicate() {
      @Override
      public boolean evaluate(Object o) {
        FacilityProgramProduct programProduct = (FacilityProgramProduct) o;
        return programProduct.getActive() && programProduct.getProduct().getActive();
      }
    });
    return activeProgramProducts;
  }
}
