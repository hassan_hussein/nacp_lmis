package com.openldr.vl.service;

import com.openldr.vl.domain.Regimen;
import com.openldr.vl.repository.RegimenRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class RegimenService {

    @Autowired
    RegimenRepository repository;

    public void insert(Regimen regimen){
        repository.insert(regimen);
    }

    public void update(Regimen regimen)
    {
        repository.update(regimen);
    }

    public Regimen getById(Long id)
    {
        return  repository.getById(id);
    }

    public List<Regimen> getAll(){
        return repository.getAll();
    }

    public void save(Regimen regimen) {
        if(regimen.getId() ==null)
            insert(regimen);
        else
            update(regimen);
    }

    public List<Regimen> getByLine(Long regimenLineId){
        return repository.getByLine(regimenLineId);
    }

    public Long delete(Long id) {
        System.out.println("deleted  "+id);
        return repository.delete(id);
    }
    public List<Regimen>getAllInFull(){
        return repository.geAllInFull();
    }

    public List<Regimen>geAllInFullById(Long id){
        return repository.geAllInFullById(id);
    }

    public Regimen getByName(String name) {
        return repository.getByName(name);
    }

    public Regimen getByNameAndType(String name,int drugType ){
        return repository.loadByNameAndType(name, drugType);
    }
}
