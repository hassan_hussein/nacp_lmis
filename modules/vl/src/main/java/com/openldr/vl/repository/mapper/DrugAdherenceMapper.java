package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.DrugAdherence;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DrugAdherenceMapper {

    @Insert({"INSERT INTO patient_drug_adherence (code, name,displayOrder) " +
            "                   Values (#{code},#{name},#{displayOrder})"})
    @Options(useGeneratedKeys = true)
    Integer insert(DrugAdherence drugAdherence);

    @Insert({"UPDATE patient_drug_adherence set code=#{code}, name=#{name}, displayOrder=#{displayOrder} WHERE id=#{id}"})
    Integer update(DrugAdherence reason);

    @Select("Select * from patient_drug_adherence where id=#{id}")
    DrugAdherence getById(@Param("id") Long id);

    @Select("Select * from patient_drug_adherence order by displayorder")
    List<DrugAdherence> getAll();

    @Select("SELECT * FROM patient_drug_adherence WHERE  name=#{name}")
    DrugAdherence getByName(@Param("name") String name);

    @Select("SELECT * FROM patient_drug_adherence WHERE  code=#{code}")
    DrugAdherence loadByCode(@Param("code") String code);
}
