
package com.openldr.vl.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "DateTimeStamp",
        "Versionstamp",
        "LIMSDateTimeStamp",
        "LIMSVersionStamp",
        "RequestID",
        "OBRSetID",
        "OBXSetID",
        "OBXSubID",
        "LOINCCode",
        "HL7ResultTypeCode",
        "SIValue",
        "SIUnits",
        "SILoRange",
        "SIHiRange",
        "HL7AbnormalFlagCodes",
        "DateTimeValue",
        "CodedValue",
        "ResultSemiquantitive",
        "Note",
        "LIMSObservationCode",
        "LIMSObservationDesc",
        "LIMSRptResult",
        "LIMSRptUnits",
        "LIMSRptFlag",
        "LIMSRptRange",
        "LIMSCodedValue",
        "WorkUnits",
        "CostUnits"
})
public class NACPResultAPI {

    @JsonProperty("DateTimeStamp")
    public String dateTimeStamp;
    @JsonProperty("Versionstamp")
    public String versionStamp;
    @JsonProperty("LIMSDateTimeStamp")
    public String lIMSDateTimeStamp;
    @JsonProperty("LIMSVersionStamp")
    public String lIMSVersionStamp;
    @JsonProperty("RequestID")
    public String requestID;
    @JsonProperty("OBRSetID")
    public String oBRSetID;
    @JsonProperty("OBXSetID")
    public String oBXSetID;
    @JsonProperty("OBXSubID")
    public String oBXSubID;
    @JsonProperty("LOINCCode")
    public String lOINCCode;
    @JsonProperty("HL7ResultTypeCode")
    public String hL7ResultTypeCode;
    @JsonProperty("SIValue")
    public String sIValue;
    @JsonProperty("SIUnits")
    public String sIUnits;
    @JsonProperty("SILoRange")
    public String sILoRange;
    @JsonProperty("SIHiRange")
    public String sIHiRange;
    @JsonProperty("HL7AbnormalFlagCodes")
    public String hL7AbnormalFlagCodes;
    @JsonProperty("DateTimeValue")
    public Object dateTimeValue;
    @JsonProperty("CodedValue")
    public String codedValue;
    @JsonProperty("ResultSemiquantitive")
    public String resultSemiquantitive;
    @JsonProperty("Note")
    public String note;
    @JsonProperty("LIMSObservationCode")
    public String lIMSObservationCode;
    @JsonProperty("LIMSObservationDesc")
    public String lIMSObservationDesc;
    @JsonProperty("LIMSRptResult")
    public String lIMSRptResult;
    @JsonProperty("LIMSRptUnits")
    public String lIMSRptUnits;
    @JsonProperty("LIMSRptFlag")
    public String lIMSRptFlag;
    @JsonProperty("LIMSRptRange")
    public String lIMSRptRange;
    @JsonProperty("LIMSCodedValue")
    public String lIMSCodedValue;
    @JsonProperty("WorkUnits")
    public String workUnits;
    @JsonProperty("CostUnits")
    public String costUnits;
    @JsonIgnore
    public Boolean isSaved;

}