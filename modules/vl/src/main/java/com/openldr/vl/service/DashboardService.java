package com.openldr.vl.service;

import com.openldr.vl.dto.DashBoardSampleTypes;
import com.openldr.vl.dto.DashboardDTO;
import com.openldr.vl.dto.TestByAge;
import com.openldr.vl.repository.DashboardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by hassan on 5/6/17.
 */

@Service
public class DashboardService {
  @Autowired
  private DashboardRepository repository;


    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    public String getStringDate(String startDateString){

        Date startDate;
        String da= "";
        try {
            startDate = df.parse(startDateString);
            System.out.println(df.format(startDate));
          da=df.format(startDate);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return da;
    }


  public void saveTAT(List<DashboardDTO>tat){
      DashboardDTO dt= new DashboardDTO();
     if(!tat.isEmpty()) {
         for (DashboardDTO dto : tat) {

             dt.setFacility(dto.getFacility());
             dt.setAuthorisedDate(getStringDate(dto.getAuthorisedDate()));
             dt.setLabNo(dto.getLabNo());
             dt.setObservationCode(dto.getObservationCode());
             dt.setObservationDescription(dto.getObservationDescription());
             dt.setSpecimenDate(getStringDate(dto.getSpecimenDate()));
             dt.setReceivedDate(getStringDate(dto.getReceivedDate()));
             dt.setRegisteredDate(getStringDate(dto.getRegisteredDate()));
             dt.setResultedDate(getStringDate(dto.getResultedDate()));
             dt.setResult(dto.getResult());
             dt.setTest(dto.getTest());

         }
         System.out.println(dt);
         repository.insert(dt);
     }

  }

    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            month = months[num];
        }
        return month;
    }
    public Integer getMonth(String date) {

        String newDateString = " ";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss UTC");
        Date startDate;
        try {
            startDate = df.parse(date);
             newDateString = df.format(startDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(newDateString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(newDateString));
       // cal.get(Calendar.YEAR); //returns the year
      return  cal.get(Calendar.MONTH);
    }

    public void saveDashboardBySample(List<DashBoardSampleTypes> s) {

        for(DashBoardSampleTypes sam: s) {
          if(sam.getLabNumber()!=null && sam.getAgeInYears() !=null) {

              DashBoardSampleTypes sampleTypes = repository.getByLabNumber(sam.getLabNumber(),sam.getAgeInYears());
              if(sampleTypes == null){
               /*   if(sam.getRegisteredDateTime().getDate() != null){
                      sam.setReceivedMonth(getMonthForInt(getMonth(sam.getRegisteredDateTime().getDate())));
                  }*/
                  repository.saveDashBoardBySampleType(sam);
              }
          }

      }
    }


    public List<HashMap<String, Object>> getAllDasboardBySample(String startDate,String endDate) {
        return repository.getAllDasboardBySample(startDate,endDate);
    }

    public List<DashBoardSampleTypes> getAllDasboardBySample2() {
        return repository.getAllDasboardBySample2();
    }


    public List<TestByAge>getByLocationAndYear(String location,Long month,Long year){
        return repository.getByLocationAndYear(location,month,year);
    }

    public List<HashMap<Object,String>>getAllByYearAndMonth(Long year){
        return repository.getAllByYearAndMonth(year);
    }

    public void saveTestByAge(List<TestByAge>testByAges){
        for(TestByAge ag: testByAges){
            if(ag.getLocation() != null && ag.getYear()!=null && ag.getMonth() !=null){

                if(repository.getByLocationAndYear(ag.getLocation(),Long.valueOf(ag.getMonth()),Long.valueOf(ag.getYear()))==null){
                    repository.insertData(ag);
                }
            }

        }

    }

}
