package com.openldr.vl.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SampleDTO extends Sample {

    private String patientId;
    private String patientCTCId;
    private String facility;
    private String lab;
    private String hub;
    private String orderId;
    private String district;
    private String region;
    private String sample;
    private Integer age;
    private Date rejectedDate;
    private Long facilityId;
    private List<TestResult> results;
    private String reasonForRejection;
    private Boolean isPending= false;


}
