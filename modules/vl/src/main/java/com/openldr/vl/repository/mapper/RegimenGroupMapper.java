package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.RegimenGroup;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegimenGroupMapper {

    @Insert({"INSERT INTO regimen_groups (code, name,displayOrder) " +
            "                   Values (#{code},#{name},#{displayOrder})"})
    @Options(useGeneratedKeys = true)
    Integer insert(RegimenGroup regimenGroup);

    @Insert({"UPDATE regimen_groups set code=#{code}, name=#{name}, displayOrder=#{displayOrder} WHERE id=#{id}"})
    Integer update(RegimenGroup regimenGroup);

    @Select("Select * from regimen_groups where id=#{id}")
    RegimenGroup getById(@Param("id") Long id);

    @Select("Select * from regimen_groups")
    List<RegimenGroup> getAll();
}
