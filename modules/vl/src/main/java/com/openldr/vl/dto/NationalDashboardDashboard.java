package com.openldr.vl.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by hassan on 8/9/17.
 */

@Data
public class NationalDashboardDashboard {


    @Getter
    @Setter
    private int page;

    @Getter
    private int pages;

    @Getter
    @Setter
    private int total;

    @Getter
    @Setter
    private int limit;

    List<NationalAPI>data;
}
