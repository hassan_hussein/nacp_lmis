package com.openldr.vl.service;

import com.openldr.vl.domain.VLTestingReason;
import com.openldr.vl.repository.VLTestingReasonRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class VLTestingReasonService {

    @Autowired
    public VLTestingReasonRepository repository;

    public void insertReason(VLTestingReason reason){
        repository.insertReason(reason);
    }

    public void updateReason(VLTestingReason reason){
        repository.updateReason(reason);
    }

    public VLTestingReason getReasonById(Long id){
        return repository.getReasonById(id);
    }

    public List<VLTestingReason> getAllReasons(){
        return repository.getAllReasons();
    }

    public VLTestingReason getByCode(String name){
        return repository.loadByCode(name);
    }
    public void save(VLTestingReason reason) {
        if(reason.getId() == null)
            insertReason(reason);
        else
            updateReason(reason);
    }
}
