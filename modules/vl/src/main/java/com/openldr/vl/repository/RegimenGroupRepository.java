package com.openldr.vl.repository;

import com.openldr.vl.domain.RegimenGroup;
import com.openldr.vl.repository.mapper.RegimenGroupMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class RegimenGroupRepository {

    @Autowired
    private RegimenGroupMapper mapper;

    public void insert(RegimenGroup regimenGroup){
        mapper.insert(regimenGroup);
    }

    public void update(RegimenGroup regimenGroup)
    {
        mapper.update(regimenGroup);
    }

    public RegimenGroup getById(Long id)
    {
        return  mapper.getById(id);
    }

    public List<RegimenGroup> getAll(){
        return mapper.getAll();
    }


}
