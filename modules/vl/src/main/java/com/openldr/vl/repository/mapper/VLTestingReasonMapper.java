package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.VLTestingReason;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VLTestingReasonMapper {

    @Insert({"INSERT INTO hiv_testing_reasons (code, name,displayOrder) " +
            "                   Values (#{code},#{name},#{displayOrder})"})
    @Options(useGeneratedKeys = true)
    Integer insertReason(VLTestingReason reason);

    @Insert({"UPDATE hiv_testing_reasons set code=#{code}, name=#{name}, displayOrder=#{displayOrder} WHERE id=#{id}"})
    Integer updateReason(VLTestingReason reason);

    @Select("Select * from hiv_testing_reasons where id=#{id}")
    VLTestingReason getReasonById(@Param("id") Long id);

    @Select("SELECT * FROM hiv_testing_reasons WHERE code=#{code}")
    VLTestingReason loadReasonByCode(@Param("code") String code);

    @Select("Select * from hiv_testing_reasons order by displayorder")
    List<VLTestingReason> getAllReasons();
}
