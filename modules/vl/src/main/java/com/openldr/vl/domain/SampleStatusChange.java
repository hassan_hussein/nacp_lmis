package com.openldr.vl.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * Created by hassan on 4/3/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SampleStatusChange extends BaseModel {

    public SampleStatus status;
    private Long sampleId;
    private Boolean synced = false;

    public SampleStatusChange(Sample sample, SampleStatus sampleStatusToSave,Long userId){
        sampleId = sample.getId();
        status = sampleStatusToSave;
        createdBy = userId;
        modifiedBy = userId;
    }


}
