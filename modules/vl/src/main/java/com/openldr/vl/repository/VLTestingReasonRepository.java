package com.openldr.vl.repository;

import com.openldr.vl.domain.VLTestingReason;
import com.openldr.vl.repository.mapper.VLTestingReasonMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class VLTestingReasonRepository {

    @Autowired
    private VLTestingReasonMapper mapper;

    public void insertReason(VLTestingReason reason) {
        mapper.insertReason(reason);
    }

    public void updateReason(VLTestingReason reason){
        mapper.updateReason(reason);
    }

    public VLTestingReason getReasonById(Long id){
        return mapper.getReasonById(id);
    }

    public VLTestingReason loadByCode(String name){
        return mapper.loadReasonByCode(name);
    }

    public List<VLTestingReason> getAllReasons(){
       return mapper.getAllReasons();
    }

}
