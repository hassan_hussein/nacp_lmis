package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.Sample;
import com.openldr.vl.domain.SampleType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SampleTypeMapper {

    @Insert({"INSERT INTO sample_types (code, name,displayOrder) " +
            "                   Values (#{code},#{name},#{displayOrder})"})
    @Options(useGeneratedKeys = true)
    Integer insertSampleType(SampleType sampleType);

    @Insert({"UPDATE sample_types set code=#{code}, name=#{name}, displayOrder=#{displayOrder} WHERE id=#{id}"})
    Integer updateSampleType(SampleType sampleType);

    @Select("Select * from sample_types where id=#{id}")
    SampleType getSampleTypeById(@Param("id") Long id);

    @Select("Select * from sample_types")
    List<SampleType> getAllSampleTypes();

    @Select("Select * from sample_types where lower(name)=lower(#{name})")
    SampleType getSampleTypeByName(@Param("name") String name);

    @Select("SELECT * FROM sample_types WHERE UPPER(code)=UPPER(#{code})")
    SampleType loadByCode(@Param("code") String code);
}
