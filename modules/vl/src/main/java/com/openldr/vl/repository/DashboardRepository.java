package com.openldr.vl.repository;

import com.openldr.vl.dto.DashBoardSampleTypes;
import com.openldr.vl.dto.DashboardDTO;
import com.openldr.vl.dto.TestByAge;
import com.openldr.vl.repository.mapper.DashboardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hassan on 5/6/17.
 */
@Component
public class DashboardRepository {

    @Autowired
    private DashboardMapper mapper;

    public void insert(DashboardDTO dashboardDTO){
        mapper.insert(dashboardDTO);
    }


    public Integer saveDashBoardBySampleType(DashBoardSampleTypes s) {
        System.out.println("reached here");
        return mapper.saveDashBoardBySampleType(s);
        }


  public Integer insertData(TestByAge s) {
        System.out.println("reached here");
        return mapper.insertTestByAge(s);
        }

    public List<TestByAge>getByLocationAndYear(String location,Long month,Long year){
        return mapper.getByLocationAndYear(location,month,year);
    }

    public List<HashMap<Object,String>>getAllByYearAndMonth(Long year){
        return mapper.getAllByYearAndMonth(year);
    }

    public DashBoardSampleTypes getByLabNumber(String labNumber, String ageByYear){
        return mapper.getDashboardSampleTypesBy(labNumber,ageByYear);
    }


    public List<HashMap<String, Object>> getAllDasboardBySample(String startDate,String endDate) {
        System.out.println(startDate);
        System.out.println(endDate);
        return mapper.getAllDasboardBySample(startDate,endDate);
    }
    public List<DashBoardSampleTypes> getAllDasboardBySample2() {
        return mapper.getAllDasboardBySample2();
    }
}
