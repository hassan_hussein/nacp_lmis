package com.openldr.vl.repository.mapper;

import com.openldr.core.domain.Pagination;
import com.openldr.vl.domain.SampleDTO;
import com.openldr.vl.domain.TestResult;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultMapper {


    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub, gz.region, gz.district, st.name sample,s.status from hiv_results r" +
            " join hiv_samples s on s.id=r.sampleid" +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and s.status =#{status}::vlsamplestatus  and s.labnumber LIKE  #{labNumber} || '%' " +
            " ORDER BY s.labnumber DESC")
    @Results(value = {
            @Result(property = "id",column = "id"),
            @Result(property = "results",column = "id",javaType = List.class,
                    many =@Many(select = "getResultForASample") )
    })
    List<SampleDTO> getResultedSamples2(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId, @Param("status")String status,@Param("labNumber")String labNumber,RowBounds rowBounds);

    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub, gz.region, gz.district, st.name sample,s.status from hiv_results r" +
            " join hiv_samples s on s.id=r.sampleid" +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and s.status =#{status}::vlsamplestatus" +
            " ORDER BY labnumber DESC")
    @Results(value = {
            @Result(property = "id",column = "id"),
            @Result(property = "results",column = "id",javaType = List.class,
                    many =@Many(select = "getResultForASample") )
    })
    List<SampleDTO> getResultedSamples(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId, @Param("status")String status,RowBounds rowBounds);

    @Select("Select count(*) from hiv_results r" +
            " join hiv_samples s on s.id=r.sampleid" +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and s.status =#{status}::vlsamplestatus and s.labnumber LIKE  #{labNumber} || '%' " +
            " ORDER BY labnumber DESC")
    @Results(value = {
            @Result(property = "id",column = "id"),
            @Result(column = "id",property = "results",javaType = List.class,
                    many =@Many(select = "getResultForASample") )
    })
    Integer getResultedSamplesCount2(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId, @Param("status")String status,@Param("labNumber")String labNumber);


    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub, gz.region, gz.district, st.name sample,s.status from hiv_results r" +
            " join hiv_samples s on s.id=r.sampleid" +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " LEFT JOIN hiv_sample_status_changes sc ON s.id = sc.sampleId" +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and sc.status =#{status}::vlsamplestatus" +
            " ORDER BY LABNUMBER ASC")
    @Results(value = {
            @Result(property = "id",column = "id"),
            @Result(column = "id",property = "results",javaType = List.class,
                    many =@Many(select = "getResultForASample") )
    })
    List<SampleDTO> getResultedSamplesList(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId, @Param("status")String status,RowBounds rowBounds);

    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub, gz.region, gz.district, st.name sample,s.status from hiv_results r" +
            " join hiv_samples s on s.id=r.sampleid" +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} and s.hubId=#{hubId} and s.status =#{status}::vlsamplestatus" +
            " ORDER BY LABNUMBER ASC")
    @Results(value = {
            @Result(property = "id",column = "id"),
            @Result(column = "id",property = "results",javaType = List.class,
                    many =@Many(select = "getResultForASample") )
    })
    List<SampleDTO> getResultedSamplesForHub(@Param("testTypeId")Long testTypeId,@Param("hubId")Long hubId, @Param("status")String status,RowBounds rowBounds);


    @Select("Select * from hiv_results where sampleId=#{sampleId}")
    List<TestResult> getResultForASample(@Param("sampleId")Long sampleId);


    @Select("Select count(s.id) as total from hiv_results r " +
            " join hiv_samples s on s.id=r.sampleid " +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and s.status=#{status}::vlsamplestatus")
    Integer getTotalResulted(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId,@Param("status") String resultStatus);

   @Select("Select count(s.id) as total from hiv_results r " +
            " join hiv_samples s on s.id=r.sampleid " +
            " where s.testtypeid=#{testTypeId} and s.hubId=#{hubId} and s.status=#{status}::vlsamplestatus")
    Integer getTotalResultedHub(@Param("testTypeId")Long testTypeId,@Param("hubId")Long hubId,@Param("status") String resultStatus);

    @Select("Select count(s.id) as total from hiv_results r " +
            " join hiv_samples s on s.id=r.sampleid " +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and s.status=#{status}::vlsamplestatus")
    Integer getTotalResultedList(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId,@Param("status") String resultStatus);


    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub, gz.region, gz.district, st.name sample,s.status from hiv_results r" +
            " join hiv_samples s on s.id=r.sampleid" +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and s.status in ('DISPATCHED')" +
            " ORDER BY S.LABNUMBER ASC")
    @Results(value = {
            @Result(property = "id",column = "id"),
            @Result(column = "id",property = "results",javaType = List.class,
                    many =@Many(select = "getResultForASample") )
    })
    List<SampleDTO> getResultedListSamples(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId,@Param("status") String status, RowBounds rowBounds);

    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub, gz.region, gz.district, st.name sample,s.status from hiv_results r" +
            " join hiv_samples s on s.id=r.sampleid" +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where  s.testtypeid=1 and s.hubId=474 and s.status in ('DISPATCHED')" +
            " ORDER BY S.LABNUMBER ASC")
    @Results(value = {
            @Result(property = "id",column = "id"),
            @Result(column = "id",property = "results",javaType = List.class,
                    many =@Many(select = "getResultForASample") )
    })
    List<SampleDTO> getDispatchedResultsForHub(@Param("testTypeId")Long testTypeId,@Param("hubId")Long labId,@Param("status") String status, RowBounds rowBounds);

    @Select("select * from hiv_results where sampleId=#{sampleId}")
    TestResult getResultBySampleId(@Param("sampleId") Long sampleId);
}
