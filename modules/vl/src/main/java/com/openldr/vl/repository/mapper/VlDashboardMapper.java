package com.openldr.vl.repository.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 4/9/17.
 */
@Repository
public interface VlDashboardMapper {

    @Select("  \n" +
            "            WITH Q  AS (  \n" +
            "            select * from (\n" +
            "            SELECT dateofsamplecollection,datereceived,resultDate\n" +
            "             , (datereceived::DATE - dateofsamplecollection::DATE) DATERANGE\n" +
            "              FROM hiv_samples where labid = #{labId} AND hiv_samples.testTypeId = #{testTypeId} and (datereceived::DATE - dateofsamplecollection::DATE)::INT > 0\n" +
            "              )x,\n" +
            "              (\n" +
            "             select distinct datereceived,resultDate,( datereceived::date - resultDate::date) tatinlab\n" +
            "             from hiv_samples" +
            "              where labid = #{labId} AND hiv_samples.testTypeId = #{testTypeId} and (datereceived::date-resultDate::date) is not null\n" +
            "              )y,\n" +
            "              (\n" +
            "              SELECT DISTINCT RESULTDATE,modifieddate,  ( modifieddate::DATE - RESULTDATE::DATE) resulttodispatch,id FROM hiv_samples WHERE \n" +
            "              labid = #{labId} AND hiv_samples.testTypeId = #{testTypeId} and status = 'DISPATCHED'\n" +
            "               \n" +
            "              )m,\n" +
            "              (\n" +
            "              SELECT DISTINCT dateofsamplecollection,modifieddate,  (modifieddate::DATE - dateofsamplecollection::DATE) collectiontodispatch,id FROM hiv_samples WHERE \n" +
            "              labid = #{labId} AND hiv_samples.testTypeId = #{testTypeId} and status = 'DISPATCHED'\n" +
            "              ) L\n" +
            "              )\n" +
            "              SELECT ROUND(AVG(DATERANGE),0) collectionToLab, ROUND(AVG(tatinlab),0) TATINLAB, ROUND(AVG(resulttodispatch),0) resulttodispatch,\n" +
            "              ROUND( AVG(collectiontodispatch),0) collectiontodispatch\n" +
            "               FROM Q   ")
    Map<String, Object> getLabTATSummary(@Param("testTypeId")Long testTypeId,@Param("labId") Long labId);

    @Select(" select * from crosstab (\n" +
            "\n" +
            " $$SELECT months, sampletype, Max(total) FROM \n" +
            "(\n" +
            "select stt.name sampletype,count(s.id) total,\n" +
            " MAX(to_char(st.createdDate, 'Month')) \n" +
            "months from hiv_samples S\n" +
            "JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
            "join sample_types stt ON s.sampletypeid = stt.id\n" +
            "WHERE st.status = 'RECEIVED' and labId =${labId} AND s.testTypeId =${testTypeId} \n" +
            "group by stt.name\n" +
            "order by stt.name\n" +
            ")m\n" +
            " group by 1,2  ORDER BY 1,2$$,\n" +
            " $$VALUES('Dry Blood Sample'),('Plasma'),('Whole Blood')$$\n" +
            " )\n" +
            " as newtable (\n" +
            " months varchar,drySample integer,plasma integer,wholeBlood integer\n" +
            ")  ")
    List<HashMap<String, Object>> getTestTrendForLab(@Param("testTypeId")Long testTypeId, @Param("labId") Long labId);


      /*  @Select("with q as (\n" +
                "\n" +
                "select r.result ||''|| unit result,count(r.id) totalResult\n" +
                "\n" +
                "                from  hiv_samples s\n" +
                "                JOIN hiv_results r ON s.id = r.sampleid\n" +
                "                JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
                "                join sample_types stt ON s.sampletypeid = stt.id\n" +
                "                WHERE st.status = 'APPROVED' AND labId =10\n" +
                "                group by r.result,unit\n" +
                "                order by r.result\n" +
                ") select * from q ")*/

  /*      @Select("SELECT SUM (totalresult) totalresult, status from(\n" +
                "with q as (\n" +
                "\n" +
                "select r.result ||''|| unit result,count(r.id) totalResult\n" +
                "\n" +
                "                from  hiv_samples s\n" +
                "                JOIN hiv_results r ON s.id = r.sampleid\n" +
                "                JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
                "                join sample_types stt ON s.sampletypeid = stt.id\n" +
                "                WHERE st.status = 'APPROVED' AND labId =10\n" +
                "                group by r.result,unit\n" +
                "                order by r.result\n" +
                ")select totalResult,case when result='<=1000cpl/ml' then 'Bellow Cut-Off point' \n" +
                "when result = '>1000cp/ml' then 'Above Cut-off' when (result in ('<20cp/ml' ,'<50cp/ml'))\n" +
                "then 'Suppressed' else 'Not suppressed' end as Status\n" +
                "\n" +
                "from q\n" +
                ")m\n" +
                "group by status ")*/

    @Select("SELECT * FROM \n" +
            "      ( with q as(select  count(r.id) as totalResult,r.result result\n" +
            "                from  hiv_samples s\n" +
            "                JOIN hiv_results r ON s.id = r.sampleid\n" +
            "                JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
            "                join sample_types stt ON s.sampletypeid = stt.id\n" +
            "                WHERE st.status = 'APPROVED' AND labId = #{labId} AND S.testTypeId = #{testTypeId}\n" +
            "                group by r.result\n" +
            "                order by r.result\n" +
            "                )\n" +
            "                SELECT TOTALRESULT Above FROM Q\n" +
            "                 WHERE RESULT= '>1000')M,\n" +
            "                 (\n" +
            "  WITH Q as (select  count(r.id) as totalResult,r.result result\n" +
            "                from  hiv_samples s\n" +
            "                JOIN hiv_results r ON s.id = r.sampleid\n" +
            "                JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
            "                join sample_types stt ON s.sampletypeid = stt.id\n" +
            "                WHERE st.status = 'APPROVED' AND labId = #{labId} AND S.testTypeId = #{testTypeId}\n" +
            "                group by r.result\n" +
            "                order by r.result\n" +
            "                )\n" +
            "                SELECT sum(TOTALRESULT) lessthan2 FROM Q\n" +
            "                where result in('<20','<50')\n" +
            "\n" +
            "\n" +
            "                 )X,\n" +
            "\n" +
            "                 (\n" +
            "with q as (select  count(r.id) as totalResult,r.result result\n" +
            "                from  hiv_samples s\n" +
            "                JOIN hiv_results r ON s.id = r.sampleid\n" +
            "                JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
            "                join sample_types stt ON s.sampletypeid = stt.id\n" +
            "                 WHERE st.status = 'APPROVED' AND labId = #{labId} AND S.testTypeId = #{testTypeId}\n" +
            "                group by r.result\n" +
            "                order by r.result\n" +
            "                )\n" +
            "                SELECT TOTALRESULT belowCutOff FROM Q\n" +
            "                where result ='<=1000'\n" +
            "\n" +
            "                 )y")
    List<HashMap<String, Object>> getVLResults(@Param("testTypeId")Long testTypeId,@Param("labId") Long labId);


   /* @Select(" \n" +
            "               select case when (suppressed is null and  notSuppressed is null ) then null else age end as age ,suppressed as suppressed,notSuppressed as notSuppressed " +
            " from crosstab(\n" +
            "               \n" +
            "                $$SELECT 'Less Than 2' age, result,total FROM (\n" +
            "                WITH Q as (select age,sum(totalResult) total,result from(\n" +
            "                (select date_part('year',age(p.dob)) age,count(s.id) totalResult,result\n" +
            "                 from  hiv_samples s\n" +
            "                 JOIN patients p on P.ID =S.PATIENTAUTOID\n" +
            "                JOIN hiv_results r ON s.id = r.sampleid\n" +
            "                JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
            "                WHERE st.status = 'APPROVED' AND labId =${labId}  AND s.testTypeId = ${testTypeId}\n" +
            "                group by p.dob,result\n" +
            "                order by p.dob)\n" +
            "                )m\n" +
            "                group by age,result\n" +
            "                order by age\n" +
            "                )\n" +
            "                SELECT age, SUM(total) total,\n" +
            "                Case when MAX(RESULT)  IN ('<20','<50') then 'Suppressed' else 'Not Suppressed' END AS result\n" +
            "                FROM Q\n" +
            "                WHERE age <=2 AND RESULT IN ('<20','<50')\n" +
            "                group by age\n" +
            "                UNION ALL\n" +
            "                SELECT MAX(age) age, sum(total) total ,\n" +
            "                Case when MAX(RESULT) NOT IN ('<20','<50') then 'Not Suppressed' else 'Suppressed' END AS result\n" +
            "                 from q\n" +
            "                WHERE age <=2 AND RESULT not in ('<20','<50') \n" +
            "\n" +
            "                )M\n" +
            "             group by 1,2,3  ORDER BY 1,2$$,\n" +
            "             $$VALUES('Suppressed'),('Not Suppressed')$$\n" +
            "             ) as newtable (\n" +
            "              age varchar,suppressed integer,notSuppressed  integer\n" +
            "              ) ")*/

    @Select(" \n" +
            "select  age ,suppressed as suppressed,notSuppressed as notSuppressed\n" +
            "             from crosstab(\n" +
            "                           $$SELECT 'Less Than 2' age,  RESULT,total FROM (\n" +
            "                           WITH Q as (select age,sum(totalResult) total,substring(RESULT FROM '[0-9]+') RESULT from(\n" +
            "                            (select date_part('year',age(p.dob)) age,count(s.id) totalResult,result\n" +
            "                             from  hiv_samples s\n" +
            "                             JOIN patients p on P.ID =S.PATIENTAUTOID\n" +
            "                            JOIN hiv_results r ON s.id = r.sampleid\n" +
            "                            JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
            "                            WHERE st.status = 'APPROVED'" +
            "                           AND labId =${labId}  AND s.testTypeId = ${testTypeId} \n" +
            "                            group by p.dob,result\n" +
            "                            order by p.dob)\n" +
            "                            )m\n" +
            "                            group by age,result\n" +
            "                            order by age\n" +
            "                            )\n" +
            "                            SELECT age, SUM(total) total,\n" +
            "                            Case when MAX(RESULT::INT) <=50 then 'Suppressed' else 'Not Suppressed' END AS result\n" +
            "                            FROM Q\n" +
            "                            WHERE  age <= 2 AND RESULT::INT <=50\n" +
            "                            group by age\n" +
            "                            UNION ALL\n" +
            "                            SELECT MAX(age) age, sum(total) total ,\n" +
            "                            Case when MAX(RESULT::INT) >50 then 'Not Suppressed' else 'Suppressed' END AS result\n" +
            "                             from q\n" +
            "                            WHERE age <= 2  AND RESULT::INT >50 \n" +
            "                           )M\n" +
            "                        group by 1,2,3  ORDER BY 1,2$$,\n" +
            "                         $$VALUES('Suppressed'),('Not Suppressed')$$\n" +
            "                         ) as newtable (\n" +
            "                          age varchar,suppressed integer,notSuppressed  integer\n" +
            "                          )\n" +
            "                          where suppressed is not null and notSuppressed is not null\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "                          ")
    HashMap<String, Object> getByAgeLessthan2(@Param("testTypeId") Long testTypeId,@Param("labId") Long labId);


  /*  @Select(" \n" +
            "               select case when (suppressed is null and  notsuppressed is null ) then null else age end as age ,suppressed,notsuppressed from crosstab(\n" +
            "               \n" +
            "                $$SELECT '2 to 5' age, result,total FROM (\n" +
            "                WITH Q as (select age,sum(totalResult) total,result from(\n" +
            "                (select date_part('year',age(p.dob)) age,count(s.id) totalResult,result\n" +
            "                 from  hiv_samples s\n" +
            "                 JOIN patients p on P.ID =S.PATIENTAUTOID\n" +
            "                JOIN hiv_results r ON s.id = r.sampleid\n" +
            "                JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
            "                WHERE st.status = 'APPROVED' AND labId =${labId}  AND s.testTypeId = ${testTypeId}\n" +
            "                group by p.dob,result\n" +
            "                order by p.dob)\n" +
            "                )m\n" +
            "                group by age,result\n" +
            "                order by age\n" +
            "                )\n" +
            "                SELECT age, SUM(total) total,\n" +
            "                Case when MAX(RESULT)  IN ('<20','<50') then 'Suppressed' else 'Not Suppressed' END AS result\n" +
            "                FROM Q\n" +
            "                WHERE (age between 3  and 5) AND RESULT IN ('<20','<50')\n" +
            "                group by age\n" +
            "                UNION ALL\n" +
            "                SELECT MAX(age) age, sum(total) total ,\n" +
            "                Case when MAX(RESULT) NOT IN ('<20','<50') then 'Not Suppressed' else 'Suppressed' END AS result\n" +
            "                 from q\n" +
            "                WHERE (age between 3  and 5) AND RESULT not in ('<20','<50') \n" +
            "\n" +
            "                )M\n" +
            "             group by 1,2,3  ORDER BY 1,2$$,\n" +
            "             $$VALUES('Suppressed'),('Not Suppressed')$$\n" +
            "             ) as newtable (\n" +
            "              age varchar,Suppressed integer,notSuppressed integer\n" +
            "              )")*/
        @Select("\n" +
                "select  age ,suppressed as suppressed,notSuppressed as notSuppressed\n" +
                "             from crosstab(\n" +
                "                           $$SELECT 'Less Than 2' age,  RESULT,total FROM (\n" +
                "                           WITH Q as (select age,sum(totalResult) total,substring(RESULT FROM '[0-9]+') RESULT from(\n" +
                "                            (select date_part('year',age(p.dob)) age,count(s.id) totalResult,result\n" +
                "                             from  hiv_samples s\n" +
                "                             JOIN patients p on P.ID =S.PATIENTAUTOID\n" +
                "                            JOIN hiv_results r ON s.id = r.sampleid\n" +
                "                            JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
                "                            WHERE st.status = 'APPROVED' \n" +
                "                           AND labId =${labId}  AND s.testTypeId = ${testTypeId} \n" +
                "                            group by p.dob,result\n" +
                "                            order by p.dob)\n" +
                "                            )m\n" +
                "                            group by age,result\n" +
                "                            order by age\n" +
                "                            )\n" +
                "                            SELECT age, SUM(total) total,\n" +
                "                            Case when MAX(RESULT::INT) <=50 then 'Suppressed' else 'Not Suppressed' END AS result\n" +
                "                            FROM Q\n" +
                "                            WHERE  age <= 3 and age <=5 AND RESULT::INT <=50\n" +
                "                            group by age\n" +
                "                            UNION ALL\n" +
                "                            SELECT MAX(age) age, sum(total) total ,\n" +
                "                            Case when MAX(RESULT::INT) >50 then 'Not Suppressed' else 'Suppressed' END AS result\n" +
                "                             from q\n" +
                "                            WHERE age <= 3 and age <=5  AND RESULT::INT >50 \n" +
                "                           )M\n" +
                "                        group by 1,2,3  ORDER BY 1,2$$,\n" +
                "                         $$VALUES('Suppressed'),('Not Suppressed')$$\n" +
                "                         ) as newtable (\n" +
                "                          age varchar,suppressed integer,notSuppressed  integer\n" +
                "                          )\n" +
                "                          where suppressed is not null and notSuppressed is not null\n" +
                "\n")
    HashMap<String, Object> getByAgeLessThan5(@Param("testTypeId") Long testTypeId,@Param("labId") Long labId);

    /*@Select(" \n" +
            "               select case when (suppressed is null and  notsuppressed is null ) then null else age end as age ,suppressed,notSuppressed from crosstab(\n" +
            "               \n" +
            "                $$SELECT '5 to 14' age, result,total FROM (\n" +
            "                WITH Q as (select age,sum(totalResult) total,result from(\n" +
            "                (select date_part('year',age(p.dob)) age,count(s.id) totalResult,result\n" +
            "                 from  hiv_samples s\n" +
            "                 JOIN patients p on P.ID =S.PATIENTAUTOID\n" +
            "                JOIN hiv_results r ON s.id = r.sampleid\n" +
            "                JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
            "                WHERE st.status = 'APPROVED' AND labId =${labId}  AND s.testTypeId = ${testTypeId}\n" +
            "                group by p.dob,result\n" +
            "                order by p.dob)\n" +
            "                )m\n" +
            "                group by age,result\n" +
            "                order by age\n" +
            "                )\n" +
            "                SELECT age, SUM(total) total,\n" +
            "                Case when MAX(RESULT)  IN ('<20','<50') then 'Suppressed' else 'Not Suppressed' END AS result\n" +
            "                FROM Q\n" +
            "                WHERE (age between 6 and 14) AND RESULT IN ('<20','<50')\n" +
            "                group by age\n" +
            "                UNION ALL\n" +
            "                SELECT MAX(age) age, sum(total) total ,\n" +
            "                Case when MAX(RESULT) NOT IN ('<20','<50') then 'Not Suppressed' else 'Suppressed' END AS result\n" +
            "                 from q\n" +
            "                WHERE (age between 6  and 14) AND RESULT not in ('<20','<50') \n" +
            "\n" +
            "                )M\n" +
            "             group by 1,2,3  ORDER BY 1,2$$,\n" +
            "             $$VALUES('Suppressed'),('Not Suppressed')$$\n" +
            "             ) as newtable (\n" +
            "              age varchar,suppressed integer,notSuppressed integer\n" +
            "              )")*/
        @Select("\n" +
                "select  age ,suppressed as suppressed,notSuppressed as notSuppressed\n" +
                "             from crosstab(\n" +
                "                           $$SELECT 'Less Than 2' age,  RESULT,total FROM (\n" +
                "                           WITH Q as (select age,sum(totalResult) total,substring(RESULT FROM '[0-9]+') RESULT from(\n" +
                "                            (select date_part('year',age(p.dob)) age,count(s.id) totalResult,result\n" +
                "                             from  hiv_samples s\n" +
                "                             JOIN patients p on P.ID =S.PATIENTAUTOID\n" +
                "                            JOIN hiv_results r ON s.id = r.sampleid\n" +
                "                            JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
                "                            WHERE st.status = 'APPROVED' \n" +
                "                           AND labId =${labId}  AND s.testTypeId = ${testTypeId} \n" +
                "                            group by p.dob,result\n" +
                "                            order by p.dob)\n" +
                "                            )m\n" +
                "                            group by age,result\n" +
                "                            order by age\n" +
                "                            )\n" +
                "                            SELECT age, SUM(total) total,\n" +
                "                            Case when MAX(RESULT::INT) <=50 then 'Suppressed' else 'Not Suppressed' END AS result\n" +
                "                            FROM Q\n" +
                "                            WHERE  age <= 6 and age <=14 AND RESULT::INT <=50\n" +
                "                            group by age\n" +
                "                            UNION ALL\n" +
                "                            SELECT MAX(age) age, sum(total) total ,\n" +
                "                            Case when MAX(RESULT::INT) >50 then 'Not Suppressed' else 'Suppressed' END AS result\n" +
                "                             from q\n" +
                "                            WHERE age <= 6 and age <=14  AND RESULT::INT >50 \n" +
                "                           )M\n" +
                "                        group by 1,2,3  ORDER BY 1,2$$,\n" +
                "                         $$VALUES('Suppressed'),('Not Suppressed')$$\n" +
                "                         ) as newtable (\n" +
                "                          age varchar,suppressed integer,notSuppressed  integer\n" +
                "                          )\n" +
                "                          where suppressed is not null and notSuppressed is not null\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "                          ")
    HashMap<String, Object> getByAgeBetween5And14(@Param("testTypeId") Long testTypeId,@Param("labId") Long labId);


/*    @Select("\n" +
            "               select case when (suppressed is null and  notSuppressed is null ) then null else age end as age ,suppressed,notsuppressed from crosstab(\n" +
            "               \n" +
            "                $$SELECT '15 And Above' age, result,total FROM (\n" +
            "                WITH Q as (select age,sum(totalResult) total,result from(\n" +
            "                (select date_part('year',age(p.dob)) age,count(s.id) totalResult,result\n" +
            "                 from  hiv_samples s\n" +
            "                 JOIN patients p on P.ID =S.PATIENTAUTOID\n" +
            "                JOIN hiv_results r ON s.id = r.sampleid\n" +
            "                JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
            "                WHERE st.status = 'APPROVED' AND labId =${labId}  AND s.testTypeId = ${testTypeId}\n" +
            "                group by p.dob,result\n" +
            "                order by p.dob)\n" +
            "                )m\n" +
            "                group by age,result\n" +
            "                order by age\n" +
            "                )\n" +
            "                SELECT age, SUM(total) total,\n" +
            "                Case when MAX(RESULT)  IN ('<20','<50') then 'Suppressed' else 'Not Suppressed' END AS result\n" +
            "                FROM Q\n" +
            "                WHERE age >=15 AND RESULT IN ('<20','<50')\n" +
            "                group by age\n" +
            "                UNION ALL\n" +
            "                SELECT MAX(age) age, sum(total) total ,\n" +
            "                Case when MAX(RESULT) NOT IN ('<20','<50') then 'Not Suppressed' else 'Suppressed' END AS result\n" +
            "                 from q\n" +
            "                WHERE age >=15 AND RESULT not in ('<20','<50') \n" +
            "\n" +
            "                )M\n" +
            "             group by 1,2,3  ORDER BY 1,2$$,\n" +
            "             $$VALUES('Suppressed'),('Not Suppressed')$$\n" +
            "             ) as newtable (\n" +
            "              age varchar,suppressed integer,notSuppressed integer\n" +
            "              ) ")*/

        @Select("\n" +
                "select  age ,suppressed as suppressed,notSuppressed as notSuppressed\n" +
                "             from crosstab(\n" +
                "                           $$SELECT 'Less Than 2' age,  RESULT,total FROM (\n" +
                "                           WITH Q as (select age,sum(totalResult) total,substring(RESULT FROM '[0-9]+') RESULT from(\n" +
                "                            (select date_part('year',age(p.dob)) age,count(s.id) totalResult,result\n" +
                "                             from  hiv_samples s\n" +
                "                             JOIN patients p on P.ID =S.PATIENTAUTOID\n" +
                "                            JOIN hiv_results r ON s.id = r.sampleid\n" +
                "                            JOIN hiv_sample_status_changes st ON s.id = st.sampleid\n" +
                "                            WHERE st.status = 'APPROVED' \n" +
                "                           AND labId =${labId}  AND s.testTypeId = ${testTypeId} \n" +
                "                            group by p.dob,result\n" +
                "                            order by p.dob)\n" +
                "                            )m\n" +
                "                            group by age,result\n" +
                "                            order by age\n" +
                "                            )\n" +
                "                            SELECT age, SUM(total) total,\n" +
                "                            Case when MAX(RESULT::INT) <=50 then 'Suppressed' else 'Not Suppressed' END AS result\n" +
                "                            FROM Q\n" +
                "                            WHERE  age > 14 AND RESULT::INT <=50\n" +
                "                            group by age\n" +
                "                            UNION ALL\n" +
                "                            SELECT MAX(age) age, sum(total) total ,\n" +
                "                            Case when MAX(RESULT::INT) >50 then 'Not Suppressed' else 'Suppressed' END AS result\n" +
                "                             from q\n" +
                "                            WHERE  age >14  AND RESULT::INT >50 \n" +
                "                           )M\n" +
                "                        group by 1,2,3  ORDER BY 1,2$$,\n" +
                "                         $$VALUES('Suppressed'),('Not Suppressed')$$\n" +
                "                         ) as newtable (\n" +
                "                          age varchar,suppressed integer,notSuppressed  integer\n" +
                "                          )\n" +
                "                          where suppressed is not null and notSuppressed is not null\n" +

                "                          ")
    HashMap<String,Object>getByAgeAbove15(@Param("testTypeId") Long testTypeId,@Param("labId") Long labId);




    //Start Dashboard API

    @Select("\n" +
            "SELECT m.facilityName,m.femaleresult,x.maleResult FROM (\n" +
            "\n" +
            "SELECT f.id facilityId,F.NAME facilityName,count(*) femaleResult FROM hiv_sample_status_changes c \n" +
            "INNER JOIN \n" +
            "   (SELECT sampleID,REGEXP_REPLACE(COALESCE(result, '0'), '[^0-9]*' ,'0')::integer result \n" +
            "   FROM Hiv_RESULTS where result <> 'DETECTED')y ON C.sampleID = Y.sampleID\n" +
            "    INNER JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "    INNER JOIN patients p on s.patientautoid = p.id\n" +
            "    INNER JOIN facilities f ON s.facilityID = f.id\n" +
            "    WHere result > 1000 and  c.status = 'DISPATCHED' and p.gender IN('Male') and labid in( 19)\n" +
            "    and  (date_part('year',age(dob)) between #{startAge} and #{endAge} )\n" +
            "    group by F.NAME,f.id)M JOIN (\n" +
            "\n" +
            "    SELECT f.id facilityId,F.NAME facilityName,count(*) maleResult FROM hiv_sample_status_changes c \n" +
            "    INNER JOIN \n" +
            "   (SELECT sampleID,REGEXP_REPLACE(COALESCE(result, '0'), '[^0-9]*' ,'0')::integer result \n" +
            "   FROM Hiv_RESULTS where result <> 'DETECTED')y ON C.sampleID = Y.sampleID\n" +
            "    INNER JOIN hiv_samples s ON C.sampleid = s.id\n" +
            "    INNER JOIN patients p on s.patientautoid = p.id\n" +
            "    INNER JOIN facilities f ON s.facilityID = f.id\n" +
            "    WHere result > 1000 and  c.status = 'DISPATCHED' and p.gender IN('Female') and labid in( 19) and  \n" +
            "    \n" +
            "    (date_part('year',age(dob)) between #{startAge} and #{endAge}  )\n" +
            "    group by F.NAME,f.id)X ON M.facilityid =x.facilityID\n" +
            "\n")
    HashMap<String, Object>getDashboardAndReportByGender(@Param("labId") Long labId,@Param("result") String result,
                                                         @Param("startAge") String startAge,
                                                         @Param("endAge") String endAge);


    //End Dashboard API


}
