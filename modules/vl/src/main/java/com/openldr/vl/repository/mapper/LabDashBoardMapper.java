package com.openldr.vl.repository.mapper;

import com.openldr.vl.dto.HFDTO;
import com.openldr.vl.dto.LabDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 9/12/17.
 * Mapper for laboratory specific dashbaord
 */
@Repository
public interface LabDashBoardMapper {

    @Select(" \n" +
            "             SELECT \n" +
            "             COALESCE(\n" +
            "             ROUND(AVG(dateofSampleDispatch::DATE-sampleRequestDate::DATE ),0),0) C_R ,\n" + // RECEIVED-COLLECTION
            "             COALESCE( \n" +
            "             ROUND(AVG( (select modifiedDate from hiv_sample_status_changes x where status= 'RESULTED' AND x.sampleId = C.SAMPLEID)::DATE- dateofSampleDispatch::date),0),0) R_P,\n" + // IN PROGRESS - RECEIVING
            "             COALESCE(\n" +
            "             ROUND(AVG( (select modifiedDate from hiv_sample_status_changes x where status= 'APPROVED' AND x.sampleId = C.SAMPLEID)::DATE- dateofSampleDispatch::date),0),0) P_D,\n" + // APPROVED- IN PROGRESS
            "             COALESCE(\n" +
            "             ROUND(AVG((select modifiedDate from hiv_sample_status_changes x where status= 'APPROVED' AND x.sampleId = C.SAMPLEID)::DATE -sampleRequestDate::DATE ),0),0) c_d\n" +
            "             FROM hiv_sample_status_changes c     \n" +
            "             join hiv_samples s on s.id=c.sampleid\n" +
            "             join patients p on p.id=s.patientautoid \n" +
            "             join facilities f on f.id=s.facilityid \n" +
            "             JOIN hiv_results r ON s.id = r.sampleid\n" +
            "             where  labid = #{labId} AND C.STATUS = 'RECEIVED' ")
    List<Map<String,String>> getFacilityTAT(@Param("labId")Long labId);

    @Select("select \n" +
            "ROUND(AVG(s.datereceived::DATE-s.dateofsamplecollection::DATE),0) C_R,\n" +
            " ROUND(AVG((select modifiedDate from hiv_sample_status_changes x where status= 'RESULTED' AND x.sampleId = s.id limit 1)::DATE-s.datereceived::DATE),0) R_P,\n" +
            " ROUND(AVG(\n" +
            "   (select modifieddate from hiv_sample_status_changes x where status= 'DISPATCHED' and x.sampleid = s.id limit 1)::DATE-\n" +
            "   (select modifieddate from hiv_sample_status_changes x where status= 'APPROVED' and x.sampleid = s.id limit 1)::DATE),0) P_D,\n" +
            " ROUND(AVG((select modifieddate from hiv_sample_status_changes x where status= 'DISPATCHED' and x.sampleid = s.id limit 1)::DATE-\n" +
            "  s.dateofsamplecollection::DATE),0) C_D\n" +
            "from hiv_samples s inner join facilities f on s.labid=f.id \n" +
            "and s.datereceived::DATE >= #{startDate} and S.datereceived::date <= #{endDate} and f.name like #{lab} \n" +
            "order by C_R, R_P, P_D, C_D ")
    LinkedHashMap<String, Object> getTAT(@Param("startDate")Date startDate, @Param("endDate") Date endDate, @Param("lab")String lab);

    @Select("SELECT f.id as id, f.name as name FROM facilities f INNER JOIN facility_types ft " +
            "ON f.typeid=ft.id WHERE ft.code='LAB' order by f.name DESC")
    List<LabDTO> loadLabs();

    @Select("with Q as \n" +
            "(\n" +
            "    (\n" +
            "        select \n" +
            "            EXTRACT(year from s.datereceived::date) as year,\n" +
            "            extract(month from s.datereceived::date) as mon_n,\n" +
            "            to_char(s.datereceived::date,'Mon') as mon,\n" +
            "            st.name as specimen,\n" +
            "            coalesce(count(*),0) as total\n" +
            "        from \n" +
            "            hiv_samples s join sample_types st on s.sampletypeid=st.id\n" +
            "             inner join hiv_test_types tt on s.testtypeid=tt.id" +
            "            join facilities f on s.labid=f.id \n" +
            "        where \n" +
            "            f.name like #{lab} and tt.code = 'VL' " +
            "            and s.datereceived::DATE >= #{startDate}::date and s.datereceived::DATE <= #{endDate}::date\n" +
            "        group by \n" +
            "            1,2,3,4 \n" +
            "        order by \n" +
            "            1,2,3,4,5\n" +
            "    )\n" +
            "    union all \n" +
            "    (\n" +
            "        select \n" +
            "            extract(year from month::DATE) as year, \n" +
            "            extract(month from month::date) as mon_n,\n" +
            "            to_char(month::date, 'Mon') as mon, \n" +
            "            specimen, \n" +
            "            total \n" +
            "        from \n" +
            "            (\n" +
            "                select \n" +
            "                    'Whole Blood' as specimen,\n" +
            "                    generate_series(date_trunc('month', #{startDate}::date),#{endDate}::date , '1 month')::date as month ,\n" +
            "                    0 as total\n" +
            "                union all \n" +
            "                select\n" +
            "                    'Dry Blood Sample' as specimen,\n" +
            "                    generate_series(date_trunc('month', #{startDate}::date),#{endDate}::date , '1 month')::date as month ,\n" +
            "                    0 as total\n" +
            "                union all \n" +
            "                select\n" +
            "                    'Plasma' as specimen,\n" +
            "                    generate_series(date_trunc('month', #{startDate}::date),#{endDate}::date , '1 month')::date as month ,\n" +
            "                    0 as total \n" +
            "                \n" +
            "            ) as m order by month\n" +
            "    )\n" +
            ")\n" +
            "select year, mon_n, mon, specimen, sum(total) as total from Q group by 1,2,3,4 order by 1,2,3,4,5")
    List<Map<String, String>> getTrendData(
            @Param("startDate") String startDate,
            @Param("endDate") String endDate,
            @Param("lab") String lab);

    @Select("    WITH Q AS\n" +
            "    (\n" +
            "        SELECT \n" +
            "            result::integer,\n" +
            "            gender \n" +
            "        FROM (\n" +
            "            SELECT \n" +
            "                CASE \n" +
            "                   \twhen r.result = '< 20 cp/ml' then '020'\n" +
            "                    WHEN RIGHT(r.result,5)='cp/mL' then left(r.result,-5)\n" +
            "                    when RIGHT(r.result,5)='cp/ml' then left(r.result,-5)   \n" +
            "                    when RIGHT(r.result,2)='CP' then left(r.result,-2) \n" +
            "                    when RIGHT(r.result,2)='cp' then left(r.result,-2) \n" +
            "                else\n" +
            "                    REGEXP_REPLACE(COALESCE(replace(r.result,',',''), '0'), '[^0-9]*' ,'0')\n" +
            "                end as result ,\n" +
            "                CASE\n" +
            "                    WHEN p.gender='Male' THEN 'M'\n" +
            "                    WHEN p.gender='Female' THEN 'F'\n" +
            "                ELSE  \n" +
            "                    'U'\n" +
            "                end as gender\n" +
            "            from \n" +
            "                hiv_results r inner join hiv_samples s on r.sampleid=s.id \n" +
            "            INNER JOIN  patients p on s.patientautoid=p.id \n" +
            "            inner join hiv_test_types tt on s.testtypeid=tt.id \n" +
            "            INNER JOIN facilities f on s.labid=f.id\n" +
            "            WHERE f.name like #{lab} and tt.code = 'VL' and p.gender is not null \n" +
            "            AND s.datereceived::DATE >= #{startDate}::date and  s.datereceived::DATE <=#{endDate}::date" +
            "            and r.result <>'Invalid' and r.result <> 'Failed'\n" +
            "        ) L\n" +
            "    )\n" +
            "            SELECT COALESCE(count(*),0) total, 'suppressed' resultType,'F' as gender FROM Q\n" +
            "            WHERE result < 1000  and gender ='F'\n" +
            "            UNION ALL\n" +
            "            SELECT COALESCE(count(*),0) total, 'suppressed' resultType,'M' as gender FROM Q\n" +
            "            WHERE result < 1000  and gender ='M'\n" +
            "            UNION ALL\n" +
            "            SELECT COALESCE(count(*),0) total, 'suppressed' resultType,'No Data' as gender FROM Q\n" +
            "            WHERE result < 1000  and gender <> 'M' and gender <> 'F'\n" +
            "            UNION ALL\n" +
            "            SELECT COALESCE(count(*),0) total, 'not suppressed' resultType,'F' as gender FROM Q\n" +
            "            WHERE result >= 1000  and gender ='F'\n" +
            "            UNION ALL\n" +
            "            SELECT COALESCE(count(*),0) total, 'not suppressed' resultType,'M' as gender FROM Q\n" +
            "            WHERE result >= 1000  and gender ='M'\n" +
            "            UNION ALL\n" +
            "            SELECT COALESCE(count(*),0) total, 'not suppressed' resultType,'No Data' as gender FROM Q\n" +
            "            WHERE result > 1000  and gender <> 'M' and gender <> 'F';\n")
    List<Map<String,String>> loadTestsByGender(@Param("startDate")Date startDate, @Param("endDate") Date endDate,@Param("lab")String lab);


    @Select("WITH Q AS \n" +
            "    (\n" +
            "        SELECT \n" +
            "            result::INTEGER,\n" +
            "            ageinyears::INTEGER \n" +
            "        FROM (\n" +
            "            SELECT \n" +
            "                CASE WHEN r.result = '< 20 cp/ml' THEN '020'\n" +
            "                    WHEN RIGHT(r.result,5)='cp/mL' THEN left(r.result,-5)\n" +
            "                    when RIGHT(r.result,5)='cp/ml' THEN left(r.result,-5)   \n" +
            "                    when RIGHT(r.result,2)='CP' THEN left(r.result,-2) \n" +
            "                    when RIGHT(r.result,2)='cp' THEN left(r.result,-2) \n" +
            "                else \n" +
            "                    REGEXP_REPLACE(COALESCE(replace(r.result,',',''),'0'), '[^0-9]*' ,'0')\n" +
            "                end as result ,\n" +
            "                date_part('year',age(p.dob)) ageinyears\n" +
            "            from \n" +
            "                hiv_results r \n" +
            "            INNER JOIN hiv_samples s on r.sampleid=s.id \n" +
            "            INNER JOIN patients p on s.patientautoid = p.id \n" +
            "            INNER JOIN hiv_test_types tt on s.testtypeid=tt.id\n" +
            "            INNER JOIN facilities f on s.labid=f.id\n" +
            "            where f.name like #{lab} and s.datereceived>=#{startDate}::date " +
            "            and s.datereceived <=#{endDate}::date and r.result <>'Invalid' " +
            "            and r.result <> 'Failed' and tt.code = 'VL' \n" +
            "        )L\n" +
            "    ) SELECT count(*) total, 'suppressed' resultType,'less 1 year' age FROM Q\n" +
            "    where result < 1000 and ageinyears < 1\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'suppressed' resultType,'1-4 years' age FROM Q\n" +
            "    where result < 1000 and (ageinyears >= 1 AND ageinyears <= 4)\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'suppressed' resultType,'5-9 years' age FROM Q\n" +
            "    where result < 1000 and (ageinyears >= 5 AND ageinyears <=9 )\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'suppressed' resultType,'10-14 years' age FROM Q\n" +
            "    where result < 1000 and (ageinyears >= 10 AND ageinyears <=14 )\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'suppressed' resultType,'15-19 years' age FROM Q\n" +
            "    where result < 1000 and (ageinyears >= 15 AND ageinyears <=19 )\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'suppressed' resultType,'20-24 years' age FROM Q\n" +
            "    where result < 1000 and (ageinyears >= 20 AND ageinyears <=24 )\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'suppressed' resultType,'25-49 years' age FROM Q\n" +
            "    where result < 1000 and (ageinyears >= 25 AND ageinyears <=49 )\n" +
            "    UNION ALL\n" +
            "    SELECT COALESCE(count(*),0) total, 'suppressed' resultType,'50+ years' age FROM Q\n" +
            "    where result < 1000 and (ageinyears >= 50)\n" +
            "    UNION ALL\n" +
            "    \n" +
            "    SELECT count(*) total , 'not suppressed' resultType,'less 1 year' age FROM Q\n" +
            "    where result >=1000 and ageinyears < 1\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total , 'not suppressed' resultType,'1-4 years' age FROM Q\n" +
            "    where result >= 1000 and (ageinyears >= 1 AND ageinyears <= 4)\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'not suppressed' resultType,'5-9 years' age FROM Q\n" +
            "    where result >= 1000 and (ageinyears >= 5 AND ageinyears <=9 )\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'not suppressed' resultType,'10-14 years' age FROM Q\n" +
            "    where result >= 1000 and (ageinyears >= 10 AND ageinyears <=14 )\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'not suppressed' resultType,'15-19 years' age FROM Q\n" +
            "    where result >= 1000 and (ageinyears >= 15 AND ageinyears <=19 )\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'not suppressed' resultType,'20-24 years' age FROM Q\n" +
            "    where result >= 1000 and (ageinyears >= 20 AND ageinyears <=24 )\n" +
            "    UNION ALL\n" +
            "    SELECT count(*) total, 'not suppressed' resultType,'25-49 years' age FROM Q\n" +
            "    where result >= 1000 and (ageinyears >= 25 AND ageinyears <=49 )\n" +
            "    UNION ALL\n" +
            "    SELECT COALESCE(count(*),0) total, 'not suppressed' resultType,'50+ years' age FROM Q\n" +
            "    where result >= 1000 and (ageinyears >= 50)")
    List<Map<String,String>>loadByAge(@Param("startDate")Date startDate, @Param("endDate") Date endDate,@Param("lab")String lab);

    @Select("WITH Q AS \n" +
            "(\n" +
            "    SELECT \n" +
            "        result::INTEGER \n" +
            "    FROM \n" +
            "        (\n" +
            "            SELECT \n" +
            "                CASE \n" +
            "                    WHEN r.result = '< 20 cp/ml' THEN '020'\n" +
            "                    WHEN RIGHT(r.result,5)='cp/mL' THEN LEFT(r.result,-5)\n" +
            "                    WHEN RIGHT(r.result,5)='cp/ml' THEN LEFT(r.result,-5)   \n" +
            "                    WHEN RIGHT(r.result,2)='CP' THEN LEFT(r.result,-2) \n" +
            "                    WHEN RIGHT(r.result,2)='cp' THEN LEFT(r.result,-2) \n" +
            "                ELSE \n" +
            "                    REGEXP_REPLACE(COALESCE(replace(r.result,',',''),'0'), '[^0-9]*' ,'0')\n" +
            "                END AS result \n" +
            "            FROM \n" +
            "                hiv_results r \n" +
            "            INNER JOIN hiv_samples s ON r.sampleid=s.id \n" +
            "            INNER JOIN hiv_test_types tt ON s.testtypeid=tt.id\n" +
            "            INNER JOIN facilities f ON s.labid=f.id\n" +
            "            where f.name LIKE #{lab} AND s.DATEreceived>=#{startDate}::DATE AND " +
            "                   s.DATEreceived <=#{endDate}::DATE AND r.result <>'Invalid' AND" +
            "                   r.result <> 'Failed' AND tt.code = 'VL' \n" +
            "    ) L\n" +
            ") \n" +
            "SELECT \n" +
            "    COALESCE(count(*),0) total, 'suppressed' resultType FROM Q WHERE  result < 1000\n" +
            "UNION ALL \n" +
            "SELECT\n" +
            "    COALESCE(count(*),0) total, 'not suppressed' resultType FROM Q WHERE result >=1000")
    List<Map<String , String>> loadByResult(
            @Param("startDate")Date startDate,
            @Param("endDate") Date endDate,
            @Param("lab")String lab);

    @Select("WITH Q AS \n" +
            "(\n" +
            "    SELECT \n" +
            "        rt.code  as code\n" +
            "    FROM \n" +
            "        hiv_testing_reasons rt \n" +
            "    INNER JOIN hiv_samples s ON s.testreasONid=rt.id\n" +
            "    INNER JOIN facilities f ON s.labid=f.id \n" +
            "    INNER JOIN hiv_test_types tt ON s.testtypeid=tt.id \n" +
            "    where f.name like #{lab} and tt.code = 'VL'\n" +
            "    AND s.datereceived::DATE <=#{endDate}::DATE and  s.datereceived::DATE >=#{startDate}::DATE\n" +
            ")\n" +
            "SELECT COALESCE(COUNT(*),0) total,'Baseline' justification FROM Q WHERE code='FIRST'\n" +
            "UNION ALL \n" +
            "SELECT COALESCE(COUNT(*),0) total, 'Routine VL' justification FROM Q WHERE code='REPEAT'\n" +
            "UNION ALL \n" +
            "SELECT COALESCE(COUNT(*),0) total, 'Suspected TX Failure' justification FROM Q WHERE code='FAILURE'")
    List<Map<String, String>> loadByJustification(
            @Param("startDate")Date startDate,
            @Param("endDate") Date endDate,
            @Param("lab")String lab
    );

    @Select("WITH S AS(\n" +
            "    WITH Q AS (\n" +
            "        SELECT \n" +
            "            result::integer,\n" +
            "            laboratory\n" +
            "        FROM (\n" +
            "            SELECT \n" +
            "                CASE \n" +
            "                    WHEN r.result = '< 20 cp/ml' THEN '020'\n" +
            "                    WHEN RIGHT(r.result,5)='cp/mL' THEN left(r.result,-5)\n" +
            "                    WHEN RIGHT(r.result,5)='cp/ml' THEN left(r.result,-5)   \n" +
            "                    WHEN RIGHT(r.result,2)='CP' THEN left(r.result,-2) \n" +
            "                    WHEN RIGHT(r.result,2)='cp' THEN left(r.result,-2) \n" +
            "                ELSE \n" +
            "                    REGEXP_REPLACE(COALESCE(replace(r.result,',',''), '0'), '[^0-9]*' ,'0')\n" +
            "                END AS result ,\n" +
            "                hf.name AS laboratory\n" +
            "            FROM\n" +
            "                hiv_results r\n" +
            "            INNER JOIN hiv_samples s ON r.sampleid=s.id\n" +
            "            INNER JOIN hiv_test_types tt ON s.testtypeid=tt.id \n" +
            "            INNER JOIN facilities hf ON s.labid=hf.id and  r.result <> 'Invalid' and r.result<>'Failed'\n" +
            "            WHERE tt.code = 'VL'  AND hf.name LIKE '%' AND s.datereceived::DATE >= #{startDate}::DATE AND  s.datereceived::DATE <=#{endDate}::DATE\n" +
            "        )L  \n" +
            "    )\n" +
            "    (SELECT  \n" +
            "        laboratory,\n" +
            "        'suppressed' resultType,\n" +
            "        count(*) total \n" +
            "    FROM \n" +
            "        Q\n" +
            "    WHERE result < 1000 GROUP BY laboratory)\n" +
            "    UNION ALL\n" +
            "    (SELECT  \n" +
            "        laboratory,\n" +
            "        'not suppressed' resultType,\n" +
            "        count(*) total\n" +
            "    FROM  Q \n" +
            "    WHERE result >=1000 GROUP BY laboratory)\n" +
            "    UNION ALL\n" +
            "    (SELECT DISTINCT f.name laboratory, 'suppressed' AS resulttype , 0 AS total FROM facilities f INNER JOIN facility_types ft ON f.typeid=ft.id WHERE ft.code='LAB' AND f.name LIKE '%'  ORDER BY f.name)\n" +
            "        UNION ALL\n" +
            "    (SELECT DISTINCT f.name laboratory, 'not suppressed' AS resulttype , 0 AS total FROM facilities f INNER JOIN facility_types ft ON f.typeid=ft.id WHERE ft.code='LAB' AND f.name LIKE '%'  ORDER BY f.name)\n" +
            ") SELECT laboratory,resultType,sum(total) AS total FROM S GROUP BY laboratory,resultType ORDER BY laboratory, resultType")
    List<Map<String,String>>loadByLaboratory(
            @Param("startDate") Date StartDate,
            @Param("endDate") Date endDate);

}
