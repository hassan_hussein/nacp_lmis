package com.openldr.vl.service;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.HIVTestType;
import com.openldr.core.service.HIVTestTypeService;
import com.openldr.vl.repository.ResultRepository;
import com.openldr.vl.repository.SampleRepository;
import com.openldr.vl.repository.VlDashboardRepository;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by hassan on 4/9/17.
 */
@Service
@NoArgsConstructor
public class VlDashboardService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VlDashboardService.class);
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    @Autowired
    HIVTestTypeService testTypeService;
    @Autowired
    SampleRepository sampleRepository;

    @Autowired
    VlDashboardRepository repository;

    public Map<String, Object> getLabTAT(Long userId) {
        Map<String, Object> tatSummaryList = null;
        try {
            Facility facility = sampleRepository.getHomeFacilityId(userId);
            HIVTestType testType = testTypeService.getFacilityDefault(userId);
            tatSummaryList = repository.getLabTATSummary(testType.getId(),facility.getId());
        } catch (Exception ex) {
            LOGGER.warn("Something has gone wrong ", ex);
        }

        return tatSummaryList;

    }

    public List<HashMap<String, Object>> getTestTrendForLab(Long userId) {
        Facility facility = sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);

        return repository.getTestTrendForLab(testType.getId(),facility.getId());
    }

    public List<HashMap<String, Object>> getVLResults(Long userId) {
        Facility facility = sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getVLResults(testType.getId(),facility.getId());
    }

    public List<HashMap<String, Object>> getTrack(Long userId) {
        List<HashMap<String, Object>> tracks;
        tracks = new ArrayList<HashMap<String, Object>>();
        Facility facility = sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        tracks.add(repository.getByAgeLessthan2(testType.getId(), facility.getId()));
        tracks.add(repository.getByAgeLesThan5(testType.getId(), facility.getId()));
        tracks.add(repository.getByAgeBetween5And14(testType.getId(), facility.getId()));
        tracks.add(repository.getByAgeAbove15(testType.getId(), facility.getId()));
        tracks.removeIf(Objects::isNull);
        return tracks;
    }


}
