package com.openldr.vl.service;

import com.openldr.vl.domain.RegimenLine;
import com.openldr.vl.repository.RegimenLineRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class RegimenLineService {

    @Autowired
    RegimenLineRepository repository;

    public void insert(RegimenLine regimenLine){
        repository.insert(regimenLine);
    }

    public void update(RegimenLine regimenLine)
    {
        repository.update(regimenLine);
    }

    public RegimenLine getById(Long id)
    {
        return  repository.getById(id);
    }

    public List<RegimenLine> getAll(){
        return repository.getAll();
    }

    public void save(RegimenLine regimenLine) {
        if(regimenLine.getId() ==null)
            insert(regimenLine);
        else
            update(regimenLine);
    }

    public List<RegimenLine> getByGroup(Long regimenGroupId){
        return repository.getByGroup(regimenGroupId);
    }
}
