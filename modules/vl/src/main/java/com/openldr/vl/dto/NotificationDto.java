package com.openldr.vl.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
@Getter
@Setter
public class NotificationDto {
    private String labNumber;
    private String orderId;
    private Long testTypeId;
    private String result;
    private String labName;
    private Date sampleCollectionDate;
    private Date registrationDate;
    private Date analysisDate;
    private Date resultDate;
    private String registeredBy;
    private String authorisedBy;
    private Date authorisedDate;
    private String testedBy;
    private Date resultedDate;


}
