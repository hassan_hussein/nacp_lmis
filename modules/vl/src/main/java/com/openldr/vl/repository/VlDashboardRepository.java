package com.openldr.vl.repository;

import com.openldr.vl.repository.mapper.VlDashboardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 4/9/17.
 */

@Component
public class VlDashboardRepository {

    @Autowired
   private VlDashboardMapper mapper;

    public Map<String, Object> getLabTATSummary(Long testTypeId,Long labId) {
        return mapper.getLabTATSummary(testTypeId,labId);
    }

    public List<HashMap<String, Object>>getTestTrendForLab(Long testTypeId,Long labId){
        return mapper.getTestTrendForLab(testTypeId,labId);
    }

    public List<HashMap<String, Object>>getVLResults(Long testTypeId,Long labId){
        return mapper.getVLResults(testTypeId,labId);
    }

    public HashMap<String, Object> getByAgeLessthan2(Long testTypeId,Long labId){
        return mapper.getByAgeLessthan2(testTypeId,labId);
    }

    public HashMap<String, Object> getByAgeLesThan5(Long testTypeId,Long labId){
        return mapper.getByAgeLessThan5(testTypeId,labId);
    }
    public HashMap<String, Object> getByAgeBetween5And14(Long testTypeId,Long labId){
        return mapper.getByAgeBetween5And14(testTypeId,labId);
    }

    public HashMap<String, Object> getByAgeAbove15(Long testTypeId,Long labId){
        return mapper.getByAgeAbove15(testTypeId,labId);
    }


}
