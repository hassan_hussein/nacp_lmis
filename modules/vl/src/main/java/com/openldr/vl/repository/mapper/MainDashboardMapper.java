package com.openldr.vl.repository.mapper;

import com.openldr.vl.dto.HFDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by hassan on 9/3/17.
 * Mapper for
 */
@Repository
public interface MainDashboardMapper {

    @Select("WITH Q AS (\n" +
            "    SELECT result::integer,requestid,ageindays::integer,ageinyears::INTEGER FROM (\n" +
            "      SELECT CASE WHEN RIGHT(limsrptresult,5)='cp/mL' OR RIGHT(limsrptresult,5)='cp/ml' OR RIGHT(limsrptresult,2)='CP' OR RIGHT(limsrptresult,2)='cp' then '0' else\n" +
            "      REGEXP_REPLACE(COALESCE(limsrptresult, '0'), '[^0-9]*' ,'0')\n" +
            "      end as result ,request.requestid,ageinyears,ageindays\n" +
            "      from nacp_requests request\n" +
            "      INNER JOIN nacp_results result ON request.RequestID = result.RequestID AND request.OBRSetID = result.OBRSetID\n" +
            "      INNER JOIN  hfr_facilities as hf on request.limsfacilitycode = hf.mohswid\n" +
            "      WHERE request.LIMSPanelCode = 'HIVVL' and hf.region like #{region}\n" +
            "    AND authoriseddatetime::DATE >= #{startDate} and  authoriseddatetime::DATE <=#{endDate}\n" +
            "    )L\n" +
            ")\n" +
            "SELECT count(*) total, 'suppressed' resultType,'less 1 year' age FROM Q\n" +
            "where result < 1000 and ageinyears < 1\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'suppressed' resultType,'1-4 years' age FROM Q\n" +
            "where result < 1000 and (ageinyears >= 1 AND ageinyears <= 4)\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'suppressed' resultType,'5-9 years' age FROM Q\n" +
            "where result < 1000 and (ageinyears >= 5 AND ageinyears <=9 )\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'suppressed' resultType,'10-14 years' age FROM Q\n" +
            "where result < 1000 and (ageinyears >= 10 AND ageinyears <=14 )\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'suppressed' resultType,'15-19 years' age FROM Q\n" +
            "where result < 1000 and (ageinyears >= 15 AND ageinyears <=19 )\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'suppressed' resultType,'20-24 years' age FROM Q\n" +
            "where result < 1000 and (ageinyears >= 20 AND ageinyears <=24 )\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'suppressed' resultType,'25-49 years' age FROM Q\n" +
            "where result < 1000 and (ageinyears >= 25 AND ageinyears <=49 )\n" +
            "UNION ALL\n" +
            "SELECT COALESCE(count(*),0) total, 'suppressed' resultType,'50+ years' age FROM Q\n" +
            "where result < 1000 and (ageinyears >= 50)\n" +
            "UNION ALL\n" +
            "\n" +
            "SELECT count(*) total , 'not suppressed' resultType,'less 1 year' age FROM Q\n" +
            "where result >=1000 and ageinyears < 1\n" +
            "UNION ALL\n" +
            "SELECT count(*) total , 'not suppressed' resultType,'1-4 years' age FROM Q\n" +
            "where result >= 1000 and (ageinyears >= 1 AND ageinyears <= 4)\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'not suppressed' resultType,'5-9 years' age FROM Q\n" +
            "where result >= 1000 and (ageinyears >= 5 AND ageinyears <=9 )\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'not suppressed' resultType,'10-14 years' age FROM Q\n" +
            "where result >= 1000 and (ageinyears >= 10 AND ageinyears <=14 )\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'not suppressed' resultType,'15-19 years' age FROM Q\n" +
            "where result >= 1000 and (ageinyears >= 15 AND ageinyears <=19 )\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'not suppressed' resultType,'20-24 years' age FROM Q\n" +
            "where result >= 1000 and (ageinyears >= 20 AND ageinyears <=24 )\n" +
            "UNION ALL\n" +
            "SELECT count(*) total, 'not suppressed' resultType,'25-49 years' age FROM Q\n" +
            "where result >= 1000 and (ageinyears >= 25 AND ageinyears <=49 )\n" +
            "UNION ALL\n" +
            "SELECT COALESCE(count(*),0) total, 'not suppressed' resultType,'50+ years' age FROM Q\n" +
            "where result >= 1000 and (ageinyears >= 50)")
    List<Map<String,String>>getByAge(@Param("startDate")Date startDate, @Param("endDate") Date endDate,@Param("region")String region);

    @Select("SELECT\n" +
            "            ROUND(AVG(REGISTEREDDATETIME::DATE-SPECIMENDATETIME::DATE ),0) C_R, " +
            "            ROUND(AVG(analysisdatetime::date - registereddatetime::date ),0) R_P, " +
            "            ROUND(AVG(authoriseddatetime::DATE - analysisdatetime::DATE ),0) p_d, " +
            "            ROUND(AVG(authoriseddatetime::DATE - SPECIMENDATETIME::DATE ),0) c_d " +
            "            FROM nacp_requests request " +
//            "            JOIN nacp_results result ON request.RequestID = result.RequestID " +
            "            INNER JOIN  hfr_facilities as hf on request.limsfacilitycode = hf.mohswid " +
            "            WHERE request.LIMSPanelCode = 'HIVVL'  AND hf.region like #{region} " +
            "            AND request.receivedDatetime::DATE >= #{startDate} and  request.receivedDatetime::DATE <=#{endDate} AND TRIM(request.limsspecimensourcecode) IN ('WHOLE','WHBD','B','Whole Blood','DBS' ,'dbs','DTS','PLASMA','FRPL') " +
            "            ORDER BY C_R ,R_P,P_D,C_D  "
    )
    LinkedHashMap<String, Object> getTAT(@Param("startDate")Date startDate, @Param("endDate") Date endDate,@Param("region")String region);

    @Select(" \n" +
            "SELECT limsspecimensourceCode sampleTypeCode,\n" +
            "count(*) test,to_char(receiveddatetime::date, 'Month') months\n" +
            "FROM nacp_requests request\n" +
            "where  date_part('year', receiveddatetime::date)\n" +
            "= 2017 and request.LIMSPanelCode = 'VLID' \n" +
            "group by limsspecimensourcecode,to_char(receiveddatetime::date, 'Month') \n" +
            "order by to_char(receiveddatetime::date, 'Month')  ")

     List<HashMap<String, Object>>getSampleTypesByYear(@Param("year") Long year);

    @Select("With Q as (\n" +
            "(select extract(year from receivedDatetime::DATE) as year,\n" +
            "   extract(month from receivedDatetime::DATE) as mon_n,\n" +
            "            to_char(receivedDatetime::DATE,'Mon') as mon,\n" +
            "            CASE\n" +
            "                 WHEN TRIM(limsspecimensourcecode) = ANY ('{WHOLE,WHBD,B,Whole Blood}') THEN 'Whole Blood'\n" +
            "                 WHEN TRIM(limsspecimensourcecode) = ANY ('{DBS,DTS,dbs}') THEN 'DBS'\n" +
            "                 WHEN TRIM(limsspecimensourcecode) = ANY ('{PLASMA,FRPL,PLASM}')  THEN 'Plasma'\n" +
            "            END as specimen,COALESCE(count(*),0) as total\n" +
            "            from nacp_requests join hfr_facilities on nacp_requests.limsfacilitycode = hfr_facilities.mohswid::varchar\n" +
            "            WHERE nacp_requests.limspanelcode='HIVVL' and hfr_facilities.region like #{region} AND\n" +
            "                  TRIM(nacp_requests.limsspecimensourcecode) = ANY ('{WHOLE,WHBD,B,Whole Blood,DBS,dbs,DTS,PLASMA,PLASM,FRPL}')\n" +
            "            AND receivedDatetime::DATE >= #{startDate}::date and  receivedDatetime::DATE <=#{endDate}::date\n" +
            "            group by 1,2,3,4 order by 1,2,3,4,5)\n" +
            "union all\n" +
            "(\n" +
            "Select extract(year from month::DATE) as year, extract(month from month::DATE) as mon_n, to_char(month::date, 'Mon') as mon,specimen, total from(\n" +
            "select 'Whole Blood' as specimen,\n" +
            "  generate_series(date_trunc('month', #{startDate}::date),#{endDate}::date, '1 month'\n" +
            ")::date as month,0 as total\n" +
            "UNION ALL\n" +
            "select 'DBS' as specimen,\n" +
            "       generate_series(date_trunc('month', #{startDate}::date),#{endDate}::date, '1 month'\n" +
            "       )::date as month,0 as total\n" +
            "UNION ALL\n" +
            "select 'Plasma' as specimen,\n" +
            "       generate_series(date_trunc('month', #{startDate}::date),#{endDate}::date, '1 month'\n" +
            "       )::date as month,0 as total) as m order by month))\n" +
            "Select year,mon_n, mon, specimen, sum(total) as total from Q\n" +
            "GROUP BY 1,2,3,4\n" +
            "ORDER BY 1,2,3,4,5")
    List<Map<String,String>> getTrendData(@Param("startDate")String startDate, @Param("endDate") String endDate,@Param("region")String region);

    @Select("WITH Q AS (\n" +
            "     SELECT result::integer,requestid,hl7sexcode FROM (\n" +
            "       SELECT CASE WHEN RIGHT(limsrptresult,5)='cp/mL' OR RIGHT(limsrptresult,5)='cp/ml' OR RIGHT(limsrptresult,2)='CP' OR RIGHT(limsrptresult,2)='cp' then '0' else\n" +
            "       REGEXP_REPLACE(COALESCE(limsrptresult, '0'), '[^0-9]*' ,'0')\n" +
            "       end as result ,request.requestid,hl7sexcode\n" +
            "       from nacp_requests request\n" +
            "       INNER JOIN nacp_results result ON request.RequestID = result.RequestID AND request.OBRSetID = result.OBRSetID\n" +
            "       INNER JOIN  hfr_facilities as hf on request.limsfacilitycode = hf.mohswid::varchar\n" +
            "       WHERE request.LIMSPanelCode = 'HIVVL' and result.limsobservationcode='HIVVM'  AND hl7sexcode is not null and hf.region like #{region}\n" +
            "       AND authoriseddatetime::DATE >= #{startDate} and  authoriseddatetime::DATE <=#{endDate}\n" +
            "     ) L\n" +
            ")\n" +
            "SELECT COALESCE(count(*),0) total, 'suppressed' resultType,'F' as gender FROM Q\n" +
            "WHERE result < 1000  and hl7sexcode ='F'\n" +
            "UNION ALL\n" +
            "SELECT COALESCE(count(*),0) total, 'suppressed' resultType,'M' as gender FROM Q\n" +
            "WHERE result < 1000  and hl7sexcode ='M'\n" +
            "UNION ALL\n" +
            "SELECT COALESCE(count(*),0) total, 'suppressed' resultType,'No Data' as gender FROM Q\n" +
            "WHERE result < 1000  and hl7sexcode <> 'M' and hl7sexcode <> 'F'\n" +
            "UNION ALL\n" +
            "SELECT COALESCE(count(*),0) total, 'not suppressed' resultType,'F' as gender FROM Q\n" +
            "WHERE result >= 1000  and hl7sexcode ='F'\n" +
            "UNION ALL\n" +
            "SELECT COALESCE(count(*),0) total, 'not suppressed' resultType,'M' as gender FROM Q\n" +
            "WHERE result >= 1000  and hl7sexcode ='M'\n" +
            "UNION ALL\n" +
            "SELECT COALESCE(count(*),0) total, 'not suppressed' resultType,'No Data' as gender FROM Q\n" +
            "WHERE result > 1000  and hl7sexcode <> 'M' and hl7sexcode <> 'F';")
    List<Map<String,String>> getByGender(@Param("startDate")Date startDate, @Param("endDate") Date endDate,@Param("region")String region);

    @Select("WITH Q AS (\n" +
            "  SELECT result::integer,requestid FROM (\n" +
            "    SELECT CASE WHEN RIGHT(limsrptresult,5)='cp/mL' OR RIGHT(limsrptresult,5)='cp/ml' OR RIGHT(limsrptresult,2)='CP' OR RIGHT(limsrptresult,2)='cp' then '0' else\n" +
            "    REGEXP_REPLACE(COALESCE(limsrptresult, '0'), '[^0-9]*' ,'0')\n" +
            "    end as result ,request.requestid\n" +
            "    FROM nacp_requests request\n" +
            "    INNER JOIN nacp_results result ON request.RequestID = result.RequestID AND request.OBRSetID = result.OBRSetID\n" +
            "    INNER JOIN  hfr_facilities as hf on request.limsfacilitycode = hf.mohswid::varchar\n" +
            "    WHERE request.LIMSPanelCode = 'HIVVL' and result.limsobservationcode='HIVVM' and hf.region like #{region}\n" +
            "    AND authoriseddatetime::DATE >= #{startDate} and  authoriseddatetime::DATE <=#{endDate}\n" +
            "  )L\n" +
            ")\n" +
            "SELECT COALESCE(count(*),0) total, 'suppressed' resultType FROM Q\n" +
            "where result < 1000\n" +
            "UNION ALL\n" +
            "SELECT COALESCE(count(*),0) total , 'not suppressed' resultType FROM Q\n" +
            "where result >= 1000")
    List<Map<String,String>> getByResult(@Param("startDate")Date startDate, @Param("endDate") Date endDate,@Param("region")String region);

    @Select("Select 0 as total, 'Baseline' justification\n" +
            "UNION ALL\n" +
            "Select 0 as total, 'Breast Feeding Mothers' justification\n" +
            "UNION ALL\n" +
            "Select 0 as total, 'Pregnant Mothers' justification\n" +
            "UNION ALL\n" +
            "Select 0 as total, 'Routine VL' justification\n" +
            "UNION ALL\n" +
            "Select 0 as total, 'Suspected TX Failure' justification\n" +
            "UNION ALL\n" +
            "Select coalesce(count(*),0) as total, 'No data' justification\n" +
            " FROM nacp_requests join hfr_facilities on nacp_requests.limsfacilitycode = hfr_facilities.mohswid::varchar\n" +
            " WHERE nacp_requests.limspanelcode='HIVVL' and hfr_facilities.region like #{region}\n" +
            " AND receivedDatetime::DATE >= #{startDate}::date and  receivedDatetime::DATE <=#{endDate}::date AND TRIM(nacp_requests.limsspecimensourcecode) = ANY ('{WHOLE,WHBD,B,Whole Blood,DBS,dbs,DTS,PLASMA,FRPL}')")
    List<Map<String,String>> getByJustification(@Param("startDate")Date startDate, @Param("endDate") Date endDate,@Param("region")String region);

    @Select("WITH S AS(\n" +
            "WITH Q AS (\n" +
            "    SELECT result::integer,requestid,region FROM (\n" +
            "    SELECT CASE WHEN RIGHT(limsrptresult,5)='cp/mL' OR RIGHT(limsrptresult,5)='cp/ml' OR RIGHT(limsrptresult,2)='CP' OR RIGHT(limsrptresult,2)='cp' then '0' else\n" +
            "    REGEXP_REPLACE(COALESCE(limsrptresult, '0'), '[^0-9]*' ,'0')\n" +
            "    end as result ,request.requestid,TRIM('DISA' FROM requestingFacilityCode)  code\n" +
            "    from nacp_requests request\n" +
            "    INNER JOIN nacp_results result ON request.RequestID = result.RequestID AND request.OBRSetID = result.OBRSetID\n" +
            "    inner join hfr_facilities as hf on request.limsfacilitycode = hf.mohswid::varchar\n" +
            "    WHERE request.LIMSPanelCode = 'HIVVL' AND result.limsobservationcode='HIVVM'  and hf.region like '%' AND authoriseddatetime::DATE >= #{startDate} and  authoriseddatetime::DATE <=#{endDate}\n" +
            "    )L  JOIN hfr_facilities hf ON hf.mohswid = L.code\n" +
            "   )\n" +
            "  SELECT  region,'suppressed' resultType,count(*) total FROM Q\n" +
            "  where result < 1000\n" +
            "  group by region\n" +
            "  UNION ALL\n" +
            "  SELECT  region,'not suppressed' resultType,count(*) total\n" +
            "from q\n" +
            "  where result >=1000\n" +
            "  group by region\n" +
            "  union all\n" +
            "(select  distinct region,'suppressed' as resulttype,0 as total from hfr_facilities where region like '%' order by region)\n" +
            "  Union all\n" +
            "(select  distinct region,'not suppressed' as resulttype,0 as total from hfr_facilities where region like '%' order by region))\n" +
            "select region,resultType,sum(total) as total from S\n" +
            "GROUP BY region,resultType\n" +
            "ORDER BY region, resultType")
    List<Map<String,String>>getByRegion(@Param("startDate") Date StartDate, @Param("endDate") Date endDate,@Param("region")String region);

        @Select("select distinct region from hfr_facilities order by region")
        List<HFDTO> getRegions();
}
