package com.openldr.vl.repository;

import com.openldr.vl.dto.HFDTO;
import com.openldr.vl.repository.mapper.MainDashboardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by hassan on 9/3/17.
 */
@Repository
public class MainDashboardRepository {
    @Autowired
    private MainDashboardMapper mapper;

    public LinkedHashMap<String,Object> getTAT(Date startDate, Date endDate,String region){
      return mapper.getTAT(startDate,endDate,region);
    }

    public List<HashMap<String,Object>>getSampleTypesByYear(Long year){
        return mapper.getSampleTypesByYear(year);
    }

    public List<Map<String, String>> getTrendData(String startDate, String endDate,String region) {
        return mapper.getTrendData(startDate,endDate,region);
    }

    public List<Map<String, String>> getByGender(Date startDate, Date endDate,String region) {
        return mapper.getByGender(startDate,endDate,region);
    }

    public List<Map<String, String>> getByAge(Date startDate, Date endDate,String region) {
        return mapper.getByAge(startDate,endDate,region);
    }
    public List<Map<String, String>> getByResult(Date startDate, Date endDate,String region) {
        return mapper.getByResult(startDate,endDate,region);
    }
    public List<Map<String, String>> getByJustification(Date startDate, Date endDate,String region) {
        return mapper.getByJustification(startDate,endDate,region);
    }

    public List<Map<String, String>> getByRegion(Date startDate, Date endDate,String region) {
        return mapper.getByRegion(startDate, endDate,region);
    }


    public List<HFDTO> getRegions(){
        return mapper.getRegions();
    }
}
