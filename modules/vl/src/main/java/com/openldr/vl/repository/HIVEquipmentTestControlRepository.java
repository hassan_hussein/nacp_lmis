package com.openldr.vl.repository;

import com.openldr.vl.domain.HIVEquipmentTestControl;
import com.openldr.vl.repository.mapper.HIVEquipmentTestControlMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class HIVEquipmentTestControlRepository {

    @Autowired
    private HIVEquipmentTestControlMapper mapper;

    public void insert(HIVEquipmentTestControl control){
        mapper.insert(control);
    }

    public void update(HIVEquipmentTestControl control)
    {
        mapper.update(control);
    }

    public HIVEquipmentTestControl getById(Long id)
    {
        return  mapper.getById(id);
    }

    public List<HIVEquipmentTestControl> getAll(){
        return mapper.getAll();
    }


}
