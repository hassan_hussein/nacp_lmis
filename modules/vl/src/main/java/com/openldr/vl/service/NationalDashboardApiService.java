package com.openldr.vl.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openldr.vl.dto.*;
import com.openldr.vl.repository.NationalDashboardApiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hassan on 8/9/17.
 * Service for Main dashboard
 */
@Service
@EnableAsync
public class NationalDashboardApiService {


    @Autowired
    private NationalDashboardApiRepository repository;
    private Boolean isFetchingRequest = false;

    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder buffer = new StringBuilder();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }

    private void processRequest(int page, int limit) {
        isFetchingRequest = true;
        //Long existingRequestRecord = repository.getExistingRequestRecord();

        // Long page = existingRequestRecord + 1;
        //   int limit = 1000;
        String url2 = "http://197.149.178.42:8181/mohsw/5db03e46aadbb336697a574f578b7e41/v1/json/requests/page/" + page + "/" + limit + "";
        String url = "http://197.149.178.42:8181/mohsw/5db03e46aadbb336697a574f578b7e41/v1/json/requests/page/" + page + "/" + limit + "/HIVVL ";
        ObjectMapper mapper = new ObjectMapper();

        URL RT;
        try {
            try {
                RT = new URL(url).toURI().toURL();
                NationalDashboardDashboard requests = mapper.readValue(RT, NationalDashboardDashboard.class);

                for (NationalAPI request : requests.getData()) {
                    NationalAPI existing = repository.getExistingRequestId(request.getRequestID());
                    if (existing == null)
                        repository.insertRequest(request);

                }
                if (page + 1 <= requests.getPages()) {

                    processRequest(page + 1, limit);
                } else
                    isFetchingRequest = false;


            } catch (URISyntaxException e) {
                e.printStackTrace();
                //processRequest(page,limit);

            }


        } catch (IOException ex) {
            ex.printStackTrace();
            processRequest(page, limit);
        }
    }

    //API TO GET request
    //@Scheduled(fixedRate = 1900000)
    // @Scheduled(cron = "0 0 00 * * ?")
    public void getRequestForDashboard() throws Exception {
        System.out.println("requests");
        if (!isFetchingRequest)
            processRequest(1, 1000);


        //NationalDashboardDashboard result = new Gson().fromJson(url, new TypeToken<NationalDashboardDashboard>() {}.getType());

    }

//     @Scheduled(fixedDelay = 600000)
    public void getResultsForDashboard() {
        System.out.println("Results..........start here");
        ObjectMapper mapper = new ObjectMapper();

        Long existingRequestRecord = repository.getExistingRequestRecord();

        Long start = existingRequestRecord + 1;
        Long end = start + 100;
        String url = "http://197.149.178.42:8181/mohsw/5db03e46aadbb336697a574f578b7e41/v1/json/results/page/1/11";

        URL RT ;
        try {
            try {
                RT = new URL(url).toURI().toURL();

                NationalResultDTO resultList = mapper.readValue(RT, NationalResultDTO.class);
                for (NACPResultAPI result : resultList.getData()) {
                    NACPResultAPI existingResults = repository.getExistingResults(result.requestID);
                    if (existingResults == null)
                        repository.insertResult(result);

                }
                System.out.println(resultList);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }


        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
//GET RESULT BY ID
    //@Scheduled(fixedDelay = 30000)
    //@Scheduled(fixedDelay = 900000, initialDelay = 400000)

//     @Scheduled(cron = "0 * 03 * * ?")
    public void getResultsForDashboard2() {
        System.out.println("Results..");
        ObjectMapper mapper = new ObjectMapper();

        List<NationalAPI> res = repository.getSaveSamples();

        if (!res.isEmpty()) {
            URL rt;
            for (NationalAPI api : res) {
                try {
                    String url3 = " http://197.149.178.42:8181/mohsw/5db03e46aadbb336697a574f578b7e41/v1/json/results/id/" + api.getRequestID() + "/1/1";
                    rt = new URL(url3).toURI().toURL();
                    NACPResultAPI[] resultList = mapper.readValue(rt, NACPResultAPI[].class);
                    if (!Arrays.asList(resultList).isEmpty()) {
                        for (NACPResultAPI ipa1 : Arrays.asList(resultList)) {
                            repository.insertResult(ipa1);
                            repository.updateRequestById(api.getRequestID());
                        }

                    }
                    System.out.println(Arrays.asList(resultList));

                } catch (MalformedURLException|URISyntaxException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //USED TO SEND REQUESTS FROM lims TO National Database
    //@Scheduled(fixedRate = 3000)
    // @Scheduled(cron = "0 0/5 17 * * ?")
    public void getAllRequestSamples() {
        System.out.println("SEnd Requests");
        List<NationalAPI> nationalAPIList = repository.getAllRequestSamples();
        String url2 = "http://197.149.178.42:8181/mohsw/5db03e46aadbb336697a574f578b7e41/v1/json/requests";
        if (!nationalAPIList.isEmpty()) {

            ObjectMapper mapper = new ObjectMapper();
            try {
                for (NationalAPI nationalAPI : nationalAPIList) {
                    String jsonInString = mapper.writeValueAsString(nationalAPI);

                    String urlParameters = "data=" + jsonInString;

                    byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
                    int postDataLength = postData.length;
                    //  String request = url2;
                    URL url = new URL(url2);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("charset", "utf-8");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                        wr.write(postData);

                        if (conn.getResponseCode() == 200) {
                            repository.updateSentData(nationalAPI.getOrderingNotes());
                        }
                        System.out.println(conn.getResponseCode());
                        System.out.println(conn.getResponseMessage());
                        System.out.println(jsonInString);
                    }
                    //end
                    // break;
                }


            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

    //USED TO SEND RESULTS TO NATINAL
    // @Scheduled(fixedDelay = 1200000, initialDelay = 300000)
    // @Scheduled(cron = "0 0/5 21 * * ?")
    public void sendResultToNational() throws IOException {
        System.out.println("Send Results to National");
        String resultUrl = "http://197.149.178.42:8181/mohsw/5db03e46aadbb336697a574f578b7e41/v1/json/results";

        List<NationalDashboardResult> results = repository.sendResultToNational();
        if (!results.isEmpty()) {
            System.out.println("started-------");
            ObjectMapper mapper = new ObjectMapper();
            for (NationalDashboardResult result : results) {

                String jsonInString;
                try {
                    jsonInString = mapper.writeValueAsString(result);

                    String urlParameters = "data=" + jsonInString;

                    byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
                    int postDataLength = postData.length;
                    URL url = new URL(resultUrl);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("charset", "utf-8");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                        wr.write(postData);
                        System.out.println(conn.getResponseMessage());
                        System.out.println(conn.getResponseCode());
                        if (conn.getResponseCode() == 200) {
                            System.out.println("List Updated");
                            repository.updateSentResultData(result.getResultId());
                        }
                        System.out.println("sending results.......");
                        System.out.println(conn.getResponseCode());
                        System.out.println(jsonInString);
                    }


                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }


                //end
                // break;

            }
        }

    }


    //API TO PULL HFR
    //@Scheduled(fixedDelay = 18000000, initialDelay = 3600000)
//    @Scheduled(cron = "0 0/5 12 * * ?")
    public void getHealthFacilities(){
        System.out.println("Insert Health Facilites");
        String url = "http://197.149.178.42/mohsw/4009ff1535effbb1ada7414929830687/v1/json/facilities";
        ObjectMapper mapper = new ObjectMapper();
        URL rt;
        try {
            rt = new URL(url).toURI().toURL();
            HFRFacilityDTO[] facilityDTOs = mapper.readValue(rt, HFRFacilityDTO[].class);
            if (!Arrays.asList(facilityDTOs).isEmpty()) {
                for (HFRFacilityDTO dto : facilityDTOs) {
                    HFRFacilityDTO dto1 = repository.getExistingHfr(dto.getId());
                    if (dto1 == null)
                        repository.insertHFR(dto);
                    }
                }
        } catch(IOException e){
            e.printStackTrace();
        }catch ( URISyntaxException e) {
            e.printStackTrace();
        }
    }

 /*   @Scheduled(cron = "")
    public void cronTask() throws InterruptedException, ExecutionException, TimeoutException {
        Future<Object> result = doSomething();
        result.get(timeout, TimeUnit.MILLISECONDS);
    }*/

  /*  @Async
    Future<Object> doSomething() {
        //what i should do
        //get ressources etc...


        System.out.println("Insert Health Facilites");
        String url = "http://197.149.178.42//mohsw/4009ff1535effbb1ada7414929830687/v1/json/facilities";
        ObjectMapper mapper = new ObjectMapper();
        URL rt = null;
        try {
            rt = new URL(url).toURI().toURL();
            HFRFacilityDTO[] facilityDTOs = mapper.readValue(rt, HFRFacilityDTO[].class);
            if(!Arrays.asList(facilityDTOs).isEmpty()){
                for (HFRFacilityDTO dto: facilityDTOs){
                    HFRFacilityDTO dto1 = repository.getExistingHfr(dto.getId());
                    if (dto1 == null)
                        repository.insertHFR(dto);
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }*/


}
