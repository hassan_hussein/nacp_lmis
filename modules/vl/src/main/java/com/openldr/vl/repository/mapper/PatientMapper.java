package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.Patient;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 6/7/17.
 */
@Repository
public interface PatientMapper {

    @Select(" SELECT * FROM patients where id = #{id}")
    Patient getById(@Param("id") Long id);
}
