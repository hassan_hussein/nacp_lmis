package com.openldr.vl.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.domain.BaseModel;
import com.openldr.core.domain.GeographicZone;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sample extends BaseModel {

    //REQUEST, ACCEPTED/REJECTED, TESTED
    private String status;
    private Long labId;
    private Long hubId;

//    On received at Lab
    private Long patientAutoId;

    @JsonProperty("name")
    private String patientPhoneNumber;
    private Patient patient;

    private Long testTypeId;

    private Long facilityId;
    private Long geographicZoneId;
    private GeographicZone geographicZone;

    private String nameOfCellLeader;
    private String batchNumber;

    private boolean patientIsPregnant;
    private boolean patientIsBreastFeeding;
    private boolean patientHasTb;
    private boolean patientUnderTbTreatment;

    private String sampleBarcodeNumber;

    private Long sampleTypeId;
    private SampleType sampleType;

    private Long patientRegimenId;
    private String otherRegimen;

    private Long drugAdherenceId;
    private DrugAdherence drugAdherence;

    private Date sampleRequestDate;
    private Long testReasonId;
    private VLTestingReason testingReason;
    private String reasonForRepeatTest;
    private String nameOfPersonRequestingTest;
    private String cadreOfPersonRequestingTest;
    private Date dateOfSampleCollection;
    private String nameOfPhlebotomist;
    private Date dateOfSampleDispatch;
    private String nameOfSampleDispatcher;
    private Date dateReceived;
    private Boolean isPending = false;
    private Boolean isSentToNationalRep;


    //When Accepted and print worksheet
    private Date testDate;
    private Long testedBy;
    private String labNumber;

    //When resulted and wait for verification
    private Long verifiedBy;
    private Date verifiedDate;
    private Date resultDate;

    private boolean formIncomplete;

    @JsonIgnore
    private String orderId;

}
