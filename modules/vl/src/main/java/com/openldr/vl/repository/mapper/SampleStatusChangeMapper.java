package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.SampleStatusChange;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 * Created by hassan on 4/3/17.
 */
@Repository
public interface SampleStatusChangeMapper {

    @Insert("  INSERT INTO hiv_sample_status_changes(\n" +
            "             sampleid, status, createdby, createddate, modifiedby, modifieddate)\n" +
            "    VALUES (#{sampleId}, #{status}, #{createdBy}, NOW(),#{modifiedBy}, NOW()) ")
    @Options(useGeneratedKeys = true)
    Integer insert(SampleStatusChange sampleStatusChange);

   @Update("UPDATE hiv_sample_status_changes\n" +
           "   SET  sampleid=#{sampleId}, status=#{status}, createdBy=#{createdBy}, createdDate=NOW(), modifiedby={modifiedBy}, \n" +
           "       modifieddate=NOW()\n" +
           " WHERE ID = #{id}")
    void update(SampleStatusChange change);
}
