package com.openldr.vl.dto;

import lombok.Data;

/**
 * Created by hassan on 9/12/17.
 */
@Data
public class HFDTO {

    private String id;
    private String mohSwid;
    private String region;
}
