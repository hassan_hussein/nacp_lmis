
package com.openldr.vl.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "DateTimeStamp",
        "Versionstamp",
        "LIMSDateTimeStamp",
        "LIMSVersionStamp",
        "RequestID",
        "OBRSetID",
        "OBXSetID",
        "OBXSubID",
        "LOINCCode",
        "HL7ResultTypeCode",
        "SIValue",
        "SIUnits",
        "SILoRange",
        "SIHiRange",
        "HL7AbnormalFlagCodes",
        "DateTimeValue",
        "CodedValue",
        "ResultSemiquantitive",
        "Note",
        "LIMSObservationCode",
        "LIMSObservationDesc",
        "LIMSRptResult",
        "LIMSRptUnits",
        "LIMSRptFlag",
        "LIMSRptRange",
        "LIMSCodedValue",
        "WorkUnits",
        "CostUnits"
})
@Data
public class NationalDashboardResult {

    @JsonProperty("DateTimeStamp")
    private String dateTimeStamp;
    @JsonProperty("Versionstamp")
    private String versionStamp;
    @JsonProperty("LIMSDateTimeStamp")
    private String lIMSDateTimeStamp;
    @JsonProperty("LIMSVersionStamp")
    private String lIMSVersionStamp;
    @JsonProperty("RequestID")
    private String requestID;
    @JsonProperty("OBRSetID")
    private String oBRSetID;
    @JsonProperty("OBXSetID")
    private String oBXSetID;
    @JsonProperty("OBXSubID")
    private String oBXSubID;
    @JsonProperty("LOINCCode")
    private String lOINCCode;
    @JsonProperty("HL7ResultTypeCode")
    private String hL7ResultTypeCode;
    @JsonProperty("SIValue")
    private String sIValue;
    @JsonProperty("SIUnits")
    private String sIUnits;
    @JsonProperty("SILoRange")
    private String sILoRange;
    @JsonProperty("SIHiRange")
    private String sIHiRange;
    @JsonProperty("HL7AbnormalFlagCodes")
    private String hL7AbnormalFlagCodes;
    @JsonProperty("DateTimeValue")
    private String dateTimeValue;
    @JsonProperty("CodedValue")
    private String codedValue;
    @JsonProperty("ResultSemiquantitive")
    private String resultSemiquantitive;
    @JsonProperty("Note")
    private String note;
    @JsonProperty("LIMSObservationCode")
    private String lIMSObservationCode;
    @JsonProperty("LIMSObservationDesc")
    private String lIMSObservationDesc;
    @JsonProperty("LIMSRptResult")
    private String lIMSRptResult;
    @JsonProperty("LIMSRptUnits")
    private String lIMSRptUnits;
    @JsonProperty("LIMSRptFlag")
    private String lIMSRptFlag;
    @JsonProperty("LIMSRptRange")
    private String lIMSRptRange;
    @JsonProperty("LIMSCodedValue")
    private String lIMSCodedValue;
    @JsonProperty("WorkUnits")
    private String workUnits;
    @JsonProperty("CostUnits")
    private String costUnits;
    @JsonIgnore
    private Long resultId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("DateTimeStamp")
    public String getDateTimeStamp() {
        return dateTimeStamp;
    }

    @JsonProperty("DateTimeStamp")
    public void setDateTimeStamp(String dateTimeStamp) {
        this.dateTimeStamp = dateTimeStamp;
    }

    @JsonProperty("Versionstamp")
    public String getVersionstamp() {
        return versionStamp;
    }

    @JsonProperty("Versionstamp")
    public void setVersionstamp(String versionstamp) {
        this.versionStamp = versionstamp;
    }

    @JsonProperty("LIMSDateTimeStamp")
    public String getLIMSDateTimeStamp() {
        return lIMSDateTimeStamp;
    }

    @JsonProperty("LIMSDateTimeStamp")
    public void setLIMSDateTimeStamp(String lIMSDateTimeStamp) {
        this.lIMSDateTimeStamp = lIMSDateTimeStamp;
    }

    @JsonProperty("LIMSVersionStamp")
    public String getLIMSVersionStamp() {
        return lIMSVersionStamp;
    }

    @JsonProperty("LIMSVersionStamp")
    public void setLIMSVersionStamp(String lIMSVersionStamp) {
        this.lIMSVersionStamp = lIMSVersionStamp;
    }

    @JsonProperty("RequestID")
    public String getRequestID() {
        return requestID;
    }

    @JsonProperty("RequestID")
    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    @JsonProperty("OBRSetID")
    public String getOBRSetID() {
        return oBRSetID;
    }

    @JsonProperty("OBRSetID")
    public void setOBRSetID(String oBRSetID) {
        this.oBRSetID = oBRSetID;
    }

    @JsonProperty("OBXSetID")
    public String getOBXSetID() {
        return oBXSetID;
    }

    @JsonProperty("OBXSetID")
    public void setOBXSetID(String oBXSetID) {
        this.oBXSetID = oBXSetID;
    }

    @JsonProperty("OBXSubID")
    public String getOBXSubID() {
        return oBXSubID;
    }

    @JsonProperty("OBXSubID")
    public void setOBXSubID(String oBXSubID) {
        this.oBXSubID = oBXSubID;
    }

    @JsonProperty("LOINCCode")
    public String getLOINCCode() {
        return lOINCCode;
    }

    @JsonProperty("LOINCCode")
    public void setLOINCCode(String lOINCCode) {
        this.lOINCCode = lOINCCode;
    }

    @JsonProperty("HL7ResultTypeCode")
    public String getHL7ResultTypeCode() {
        return hL7ResultTypeCode;
    }

    @JsonProperty("HL7ResultTypeCode")
    public void setHL7ResultTypeCode(String hL7ResultTypeCode) {
        this.hL7ResultTypeCode = hL7ResultTypeCode;
    }

    @JsonProperty("SIValue")
    public String getSIValue() {
        return sIValue;
    }

    @JsonProperty("SIValue")
    public void setSIValue(String sIValue) {
        this.sIValue = sIValue;
    }

    @JsonProperty("SIUnits")
    public String getSIUnits() {
        return sIUnits;
    }

    @JsonProperty("SIUnits")
    public void setSIUnits(String sIUnits) {
        this.sIUnits = sIUnits;
    }

    @JsonProperty("SILoRange")
    public String getSILoRange() {
        return sILoRange;
    }

    @JsonProperty("SILoRange")
    public void setSILoRange(String sILoRange) {
        this.sILoRange = sILoRange;
    }

    @JsonProperty("SIHiRange")
    public String getSIHiRange() {
        return sIHiRange;
    }

    @JsonProperty("SIHiRange")
    public void setSIHiRange(String sIHiRange) {
        this.sIHiRange = sIHiRange;
    }

    @JsonProperty("HL7AbnormalFlagCodes")
    public String getHL7AbnormalFlagCodes() {
        return hL7AbnormalFlagCodes;
    }

    @JsonProperty("HL7AbnormalFlagCodes")
    public void setHL7AbnormalFlagCodes(String hL7AbnormalFlagCodes) {
        this.hL7AbnormalFlagCodes = hL7AbnormalFlagCodes;
    }

    @JsonProperty("DateTimeValue")
    public String getDateTimeValue() {
        return dateTimeValue;
    }

    @JsonProperty("DateTimeValue")
    public void setDateTimeValue(String dateTimeValue) {
        this.dateTimeValue = dateTimeValue;
    }

    @JsonProperty("CodedValue")
    public String getCodedValue() {
        return codedValue;
    }

    @JsonProperty("CodedValue")
    public void setCodedValue(String codedValue) {
        this.codedValue = codedValue;
    }

    @JsonProperty("ResultSemiquantitive")
    public String getResultSemiquantitive() {
        return resultSemiquantitive;
    }

    @JsonProperty("ResultSemiquantitive")
    public void setResultSemiquantitive(String resultSemiquantitive) {
        this.resultSemiquantitive = resultSemiquantitive;
    }

    @JsonProperty("Note")
    public String getNote() {
        return note;
    }

    @JsonProperty("Note")
    public void setNote(String note) {
        this.note = note;
    }

    @JsonProperty("LIMSObservationCode")
    public String getLIMSObservationCode() {
        return lIMSObservationCode;
    }

    @JsonProperty("LIMSObservationCode")
    public void setLIMSObservationCode(String lIMSObservationCode) {
        this.lIMSObservationCode = lIMSObservationCode;
    }

    @JsonProperty("LIMSObservationDesc")
    public String getLIMSObservationDesc() {
        return lIMSObservationDesc;
    }

    @JsonProperty("LIMSObservationDesc")
    public void setLIMSObservationDesc(String lIMSObservationDesc) {
        this.lIMSObservationDesc = lIMSObservationDesc;
    }

    @JsonProperty("LIMSRptResult")
    public String getLIMSRptResult() {
        return lIMSRptResult;
    }

    @JsonProperty("LIMSRptResult")
    public void setLIMSRptResult(String lIMSRptResult) {
        this.lIMSRptResult = lIMSRptResult;
    }

    @JsonProperty("LIMSRptUnits")
    public String getLIMSRptUnits() {
        return lIMSRptUnits;
    }

    @JsonProperty("LIMSRptUnits")
    public void setLIMSRptUnits(String lIMSRptUnits) {
        this.lIMSRptUnits = lIMSRptUnits;
    }

    @JsonProperty("LIMSRptFlag")
    public String getLIMSRptFlag() {
        return lIMSRptFlag;
    }

    @JsonProperty("LIMSRptFlag")
    public void setLIMSRptFlag(String lIMSRptFlag) {
        this.lIMSRptFlag = lIMSRptFlag;
    }

    @JsonProperty("LIMSRptRange")
    public String getLIMSRptRange() {
        return lIMSRptRange;
    }

    @JsonProperty("LIMSRptRange")
    public void setLIMSRptRange(String lIMSRptRange) {
        this.lIMSRptRange = lIMSRptRange;
    }

    @JsonProperty("LIMSCodedValue")
    public String getLIMSCodedValue() {
        return lIMSCodedValue;
    }

    @JsonProperty("LIMSCodedValue")
    public void setLIMSCodedValue(String lIMSCodedValue) {
        this.lIMSCodedValue = lIMSCodedValue;
    }

    @JsonProperty("WorkUnits")
    public String getWorkUnits() {
        return workUnits;
    }

    @JsonProperty("WorkUnits")
    public void setWorkUnits(String workUnits) {
        this.workUnits = workUnits;
    }

    @JsonProperty("CostUnits")
    public String getCostUnits() {
        return costUnits;
    }

    @JsonProperty("CostUnits")
    public void setCostUnits(String costUnits) {
        this.costUnits = costUnits;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}