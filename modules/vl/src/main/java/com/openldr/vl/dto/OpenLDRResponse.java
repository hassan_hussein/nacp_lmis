package com.openldr.vl.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by dev2 on 2/1/2018.
 */
@Data
public class OpenLDRResponse {


    @Getter
    @Setter
    private int page;

    @Getter
    private int pages;

    @Getter
    @Setter
    private int total;

    @Getter
    @Setter
    private int limit;
}
