package com.openldr.vl.service;

import com.openldr.vl.domain.DrugAdherence;
import com.openldr.vl.repository.DrugAdherenceRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class DrugAdherenceService {

    @Autowired
    public DrugAdherenceRepository repository;

    public void insert(DrugAdherence adherence){
        repository.insert(adherence);
    }

    public void update(DrugAdherence adherence){
        repository.update(adherence);
    }

    public DrugAdherence getById(Long id){
        return repository.getById(id);
    }

    public List<DrugAdherence> getAll(){
        return repository.getAll();
    }

    public void save(DrugAdherence adherence) {
        if(adherence.getId() == null)
            insert(adherence);
        else
            update(adherence);
    }

    public DrugAdherence getByName(String name) {
        return repository.getByName(name);
    }

    public DrugAdherence getByCode(String code){
        return repository.loadByCode(code);
    }
}
