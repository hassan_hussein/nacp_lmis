package com.openldr.vl.repository;

import com.openldr.vl.domain.Regimen;
import com.openldr.vl.repository.mapper.RegimenMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class RegimenRepository {

    @Autowired
    private RegimenMapper mapper;

    public void insert(Regimen regimen){
        mapper.insert(regimen);
    }

    public void update(Regimen regimen)
    {
        mapper.update(regimen);
    }

    public Regimen getById(Long id)
    {
        return  mapper.getById(id);
    }

    public List<Regimen> getAll(){
        return mapper.getAll();
    }

    public List<Regimen> getByLine(Long regimenLineId){
        return mapper.getByLine(regimenLineId);
    }


    public Long delete(Long id) {
        return mapper.delete(id);
    }

    public List<Regimen>geAllInFull(){
        return mapper.geAllInFull();
    }

    public List<Regimen>geAllInFullById(Long id){
        return mapper.geAllInFullById(id);
    }

    public Regimen getByName(String name) {
        return mapper.getByName(name);
    }

    public Regimen loadByNameAndType(String name, int drugType) {
        return mapper.loadByNameAndType(name, drugType);
    }
}
