package com.openldr.vl.dto;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "DateTimeStamp",
        "Versionstamp",
        "LIMSDateTimeStamp",
        "LIMSVersionstamp",
        "RequestID",
        "OBRSetID",
        "LOINCPanelCode",
        "LIMSPanelCode",
        "LIMSPanelDesc",
        "HL7PriorityCode",
        "SpecimenDateTime",
        "RegisteredDateTime",
        "ReceivedDateTime",
        "AnalysisDateTime",
        "AuthorisedDateTime",
        "AdmitAttendDateTime",
        "CollectionVolume",
        "RequestingFacilityCode",
        "ReceivingFacilityCode",
        "LIMSPointOfCareDesc",
        "RequestTypeCode",
        "ICD10ClinicalInfoCodes",
        "ClinicalInfo",
        "HL7SpecimenSourceCode",
        "LIMSSpecimenSourceCode",
        "LIMSSpecimenSourceDesc",
        "HL7SpecimenSiteCode",
        "LIMSSpecimenSiteCode",
        "LIMSSpecimenSiteDesc",
        "WorkUnits",
        "CostUnits",
        "HL7SectionCode",
        "HL7ResultStatusCode",
        "RegisteredBy",
        "TestedBy",
        "AuthorisedBy",
        "OrderingNotes",
        "EncryptedPatientID",
        "AgeInYears",
        "AgeInDays",
        "HL7SexCode",
        "HL7EthnicGroupCode",
        "Deceased",
        "Newborn",
        "HL7PatientClassCode",
        "AttendingDoctor",
        "TestingFacilityCode",
        "ReferringRequestID",
        "Therapy",
        "LIMSAnalyzerCode",
        "TargetTimeDays",
        "TargetTimeMins",
        "LIMSRejectionCode",
        "LIMSRejectionDesc",
        "LIMSFacilityCode",
        "Repeated"
})
@Data
public class NationalAPI{

    @JsonProperty("DateTimeStamp")
    private String dateTimeStamp;
    @JsonProperty("Versionstamp")
    private String versionStamp;
    @JsonProperty("LIMSDateTimeStamp")
    private String lIMSDateTimeStamp;
    @JsonProperty("LIMSVersionstamp")
    private String lIMSVersionStamp;
    @JsonProperty("RequestID")
    private String requestID;
    @JsonProperty("OBRSetID")
    private String oBRSetID;
    @JsonProperty("LOINCPanelCode")
    private String lOINCPanelCode;
    @JsonProperty("LIMSPanelCode")
    private String lIMSPanelCode;
    @JsonProperty("LIMSPanelDesc")
    private String lIMSPanelDesc;
    @JsonProperty("HL7PriorityCode")
    private String hL7PriorityCode;
    @JsonProperty("SpecimenDateTime")
    private String specimenDateTime;
    @JsonProperty("RegisteredDateTime")
    private String registeredDateTime;
    @JsonProperty("ReceivedDateTime")
    private String receivedDateTime;
    @JsonProperty("AnalysisDateTime")
    private String analysisDateTime;
    @JsonProperty("AuthorisedDateTime")
    private String authorisedDateTime;
    @JsonProperty("AdmitAttendDateTime")
    private String admitAttendDateTime;
    @JsonProperty("CollectionVolume")
    private String collectionVolume;
    @JsonProperty("RequestingFacilityCode")
    private String requestingFacilityCode;
    @JsonProperty("ReceivingFacilityCode")
    private String receivingFacilityCode;
    @JsonProperty("LIMSPointOfCareDesc")
    private String lIMSPointOfCareDesc;
    @JsonProperty("RequestTypeCode")
    private String requestTypeCode;
    @JsonProperty("ICD10ClinicalInfoCodes")
    private String iCD10ClinicalInfoCodes;
    @JsonProperty("ClinicalInfo")
    private String clinicalInfo;
    @JsonProperty("HL7SpecimenSourceCode")
    private String hL7SpecimenSourceCode;
    @JsonProperty("LIMSSpecimenSourceCode")
    private String lIMSSpecimenSourceCode;
    @JsonProperty("LIMSSpecimenSourceDesc")
    private String lIMSSpecimenSourceDesc;
    @JsonProperty("HL7SpecimenSiteCode")
    private String hL7SpecimenSiteCode;
    @JsonProperty("LIMSSpecimenSiteCode")
    private String lIMSSpecimenSiteCode;
    @JsonProperty("LIMSSpecimenSiteDesc")
    private String lIMSSpecimenSiteDesc;
    @JsonProperty("WorkUnits")
    private String workUnits;
    @JsonProperty("CostUnits")
    private String costUnits;
    @JsonProperty("HL7SectionCode")
    private String hL7SectionCode;
    @JsonProperty("HL7ResultStatusCode")
    private String hL7ResultStatusCode;
    @JsonProperty("RegisteredBy")
    private String registeredBy;
    @JsonProperty("TestedBy")
    private String testedBy;
    @JsonProperty("AuthorisedBy")
    private String authorisedBy;
    @JsonProperty("OrderingNotes")
    private String orderingNotes;
    @JsonProperty("EncryptedPatientID")
    private String encryptedPatientID;
    @JsonProperty("AgeInYears")
    private String ageInYears;
    @JsonProperty("AgeInDays")
    private String ageInDays;
    @JsonProperty("HL7SexCode")
    private String hL7SexCode;
    @JsonProperty("HL7EthnicGroupCode")
    private String hL7EthnicGroupCode;
    @JsonProperty("Deceased")
    private String deceased;
    @JsonProperty("Newborn")
    private String newborn;
    @JsonProperty("HL7PatientClassCode")
    private String hL7PatientClassCode;
    @JsonProperty("AttendingDoctor")
    private String attendingDoctor;
    @JsonProperty("TestingFacilityCode")
    private String testingFacilityCode;
    @JsonProperty("ReferringRequestID")
    private String referringRequestID;
    @JsonProperty("Therapy")
    private String therapy;
    @JsonProperty("LIMSAnalyzerCode")
    private String lIMSAnalyzerCode;
    @JsonProperty("TargetTimeDays")
    private String targetTimeDays;
    @JsonProperty("TargetTimeMins")
    private String targetTimeMins;
    @JsonProperty("LIMSRejectionCode")
    private String lIMSRejectionCode;
    @JsonProperty("LIMSRejectionDesc")
    private String lIMSRejectionDesc;
    @JsonProperty("LIMSFacilityCode")
    private String lIMSFacilityCode;
    @JsonProperty("Repeated")
    private String repeated;
    @JsonIgnore
    private String analysisDateTimeCrtd;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("DateTimeStamp")
    public String getDateTimeStamp() {
        return dateTimeStamp;
    }

    @JsonProperty("DateTimeStamp")
    public void setDateTimeStamp(String dateTimeStamp) {
        this.dateTimeStamp = dateTimeStamp;
    }

    @JsonProperty("Versionstamp")
    public String getVersionstamp() {
        return versionStamp;
    }

    @JsonProperty("Versionstamp")
    public void setVersionstamp(String versionstamp) {
        this.versionStamp = versionstamp;
    }

    @JsonProperty("LIMSDateTimeStamp")
    public String getLIMSDateTimeStamp() {
        return lIMSDateTimeStamp;
    }

    @JsonProperty("LIMSDateTimeStamp")
    public void setLIMSDateTimeStamp(String lIMSDateTimeStamp) {
        this.lIMSDateTimeStamp = lIMSDateTimeStamp;
    }

    @JsonProperty("LIMSVersionstamp")
    public String getLIMSVersionstamp() {
        return lIMSVersionStamp;
    }

    @JsonProperty("LIMSVersionstamp")
    public void setLIMSVersionstamp(String lIMSVersionstamp) {
        this.lIMSVersionStamp = lIMSVersionstamp;
    }

    @JsonProperty("RequestID")
    public String getRequestID() {
        return requestID;
    }

    @JsonProperty("RequestID")
    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    @JsonProperty("OBRSetID")
    public String getOBRSetID() {
        return oBRSetID;
    }

    @JsonProperty("OBRSetID")
    public void setOBRSetID(String oBRSetID) {
        this.oBRSetID = oBRSetID;
    }

    @JsonProperty("LOINCPanelCode")
    public String getLOINCPanelCode() {
        return lOINCPanelCode;
    }

    @JsonProperty("LOINCPanelCode")
    public void setLOINCPanelCode(String lOINCPanelCode) {
        this.lOINCPanelCode = lOINCPanelCode;
    }

    @JsonProperty("LIMSPanelCode")
    public String getLIMSPanelCode() {
        return lIMSPanelCode;
    }

    @JsonProperty("LIMSPanelCode")
    public void setLIMSPanelCode(String lIMSPanelCode) {
        this.lIMSPanelCode = lIMSPanelCode;
    }

    @JsonProperty("LIMSPanelDesc")
    public String getLIMSPanelDesc() {
        return lIMSPanelDesc;
    }

    @JsonProperty("LIMSPanelDesc")
    public void setLIMSPanelDesc(String lIMSPanelDesc) {
        this.lIMSPanelDesc = lIMSPanelDesc;
    }

    @JsonProperty("HL7PriorityCode")
    public String getHL7PriorityCode() {
        return hL7PriorityCode;
    }

    @JsonProperty("HL7PriorityCode")
    public void setHL7PriorityCode(String hL7PriorityCode) {
        this.hL7PriorityCode = hL7PriorityCode;
    }

    @JsonProperty("SpecimenDateTime")
    public String getSpecimenDateTime() {
        return specimenDateTime;
    }

    @JsonProperty("SpecimenDateTime")
    public void setSpecimenDateTime(String specimenDateTime) {
        this.specimenDateTime = specimenDateTime;
    }

    @JsonProperty("RegisteredDateTime")
    public String getRegisteredDateTime() {
        return registeredDateTime;
    }

    @JsonProperty("RegisteredDateTime")
    public void setRegisteredDateTime(String registeredDateTime) {
        this.registeredDateTime = registeredDateTime;
    }

    @JsonProperty("ReceivedDateTime")
    public String getReceivedDateTime() {
        return receivedDateTime;
    }

    @JsonProperty("ReceivedDateTime")
    public void setReceivedDateTime(String receivedDateTime) {
        this.receivedDateTime = receivedDateTime;
    }

    @JsonProperty("AnalysisDateTime")
    public String getAnalysisDateTime() {
        if (StringUtils.isNotEmpty(analysisDateTime))
            return analysisDateTime;
        return getAnalysisDateTimeCrtd();
    }

    @JsonProperty("AnalysisDateTime")
    public void setAnalysisDateTime(String analysisDateTime) {
        this.analysisDateTime = analysisDateTime;
    }

    @JsonProperty("AuthorisedDateTime")
    public String getAuthorisedDateTime() {
        return authorisedDateTime;
    }

    @JsonProperty("AuthorisedDateTime")
    public void setAuthorisedDateTime(String authorisedDateTime) {
        this.authorisedDateTime = authorisedDateTime;
    }

    @JsonProperty("AdmitAttendDateTime")
    public String getAdmitAttendDateTime() {
        return admitAttendDateTime;
    }

    @JsonProperty("AdmitAttendDateTime")
    public void setAdmitAttendDateTime(String admitAttendDateTime) {
        this.admitAttendDateTime = admitAttendDateTime;
    }

    @JsonProperty("CollectionVolume")
    public String getCollectionVolume() {
        return collectionVolume;
    }

    @JsonProperty("CollectionVolume")
    public void setCollectionVolume(String collectionVolume) {
        this.collectionVolume = collectionVolume;
    }

    @JsonProperty("RequestingFacilityCode")
    public String getRequestingFacilityCode() {
        return requestingFacilityCode;
    }

    @JsonProperty("RequestingFacilityCode")
    public void setRequestingFacilityCode(String requestingFacilityCode) {
        this.requestingFacilityCode = requestingFacilityCode;
    }

    @JsonProperty("ReceivingFacilityCode")
    public String getReceivingFacilityCode() {
        return receivingFacilityCode;
    }

    @JsonProperty("ReceivingFacilityCode")
    public void setReceivingFacilityCode(String receivingFacilityCode) {
        this.receivingFacilityCode = receivingFacilityCode;
    }

    @JsonProperty("LIMSPointOfCareDesc")
    public String getLIMSPointOfCareDesc() {
        return lIMSPointOfCareDesc;
    }

    @JsonProperty("LIMSPointOfCareDesc")
    public void setLIMSPointOfCareDesc(String lIMSPointOfCareDesc) {
        this.lIMSPointOfCareDesc = lIMSPointOfCareDesc;
    }

    @JsonProperty("RequestTypeCode")
    public String getRequestTypeCode() {
        return requestTypeCode;
    }

    @JsonProperty("RequestTypeCode")
    public void setRequestTypeCode(String requestTypeCode) {
        this.requestTypeCode = requestTypeCode;
    }

    @JsonProperty("ICD10ClinicalInfoCodes")
    public String getICD10ClinicalInfoCodes() {
        return iCD10ClinicalInfoCodes;
    }

    @JsonProperty("ICD10ClinicalInfoCodes")
    public void setICD10ClinicalInfoCodes(String iCD10ClinicalInfoCodes) {
        this.iCD10ClinicalInfoCodes = iCD10ClinicalInfoCodes;
    }

    @JsonProperty("ClinicalInfo")
    public String getClinicalInfo() {
        return clinicalInfo;
    }

    @JsonProperty("ClinicalInfo")
    public void setClinicalInfo(String clinicalInfo) {
        this.clinicalInfo = clinicalInfo;
    }

    @JsonProperty("HL7SpecimenSourceCode")
    public String getHL7SpecimenSourceCode() {
        return hL7SpecimenSourceCode;
    }

    @JsonProperty("HL7SpecimenSourceCode")
    public void setHL7SpecimenSourceCode(String hL7SpecimenSourceCode) {
        this.hL7SpecimenSourceCode = hL7SpecimenSourceCode;
    }

    @JsonProperty("LIMSSpecimenSourceCode")
    public String getLIMSSpecimenSourceCode() {
        return lIMSSpecimenSourceCode;
    }

    @JsonProperty("LIMSSpecimenSourceCode")
    public void setLIMSSpecimenSourceCode(String lIMSSpecimenSourceCode) {
        this.lIMSSpecimenSourceCode = lIMSSpecimenSourceCode;
    }

    @JsonProperty("LIMSSpecimenSourceDesc")
    public String getLIMSSpecimenSourceDesc() {
        return lIMSSpecimenSourceDesc;
    }

    @JsonProperty("LIMSSpecimenSourceDesc")
    public void setLIMSSpecimenSourceDesc(String lIMSSpecimenSourceDesc) {
        this.lIMSSpecimenSourceDesc = lIMSSpecimenSourceDesc;
    }

    @JsonProperty("HL7SpecimenSiteCode")
    public String getHL7SpecimenSiteCode() {
        return hL7SpecimenSiteCode;
    }

    @JsonProperty("HL7SpecimenSiteCode")
    public void setHL7SpecimenSiteCode(String hL7SpecimenSiteCode) {
        this.hL7SpecimenSiteCode = hL7SpecimenSiteCode;
    }

    @JsonProperty("LIMSSpecimenSiteCode")
    public String getLIMSSpecimenSiteCode() {
        return lIMSSpecimenSiteCode;
    }

    @JsonProperty("LIMSSpecimenSiteCode")
    public void setLIMSSpecimenSiteCode(String lIMSSpecimenSiteCode) {
        this.lIMSSpecimenSiteCode = lIMSSpecimenSiteCode;
    }

    @JsonProperty("LIMSSpecimenSiteDesc")
    public String getLIMSSpecimenSiteDesc() {
        return lIMSSpecimenSiteDesc;
    }

    @JsonProperty("LIMSSpecimenSiteDesc")
    public void setLIMSSpecimenSiteDesc(String lIMSSpecimenSiteDesc) {
        this.lIMSSpecimenSiteDesc = lIMSSpecimenSiteDesc;
    }

    @JsonProperty("WorkUnits")
    public String getWorkUnits() {
        return workUnits;
    }

    @JsonProperty("WorkUnits")
    public void setWorkUnits(String workUnits) {
        this.workUnits = workUnits;
    }

    @JsonProperty("CostUnits")
    public String getCostUnits() {
        return costUnits;
    }

    @JsonProperty("CostUnits")
    public void setCostUnits(String costUnits) {
        this.costUnits = costUnits;
    }

    @JsonProperty("HL7SectionCode")
    public String getHL7SectionCode() {
        return hL7SectionCode;
    }

    @JsonProperty("HL7SectionCode")
    public void setHL7SectionCode(String hL7SectionCode) {
        this.hL7SectionCode = hL7SectionCode;
    }

    @JsonProperty("HL7ResultStatusCode")
    public String getHL7ResultStatusCode() {
        return hL7ResultStatusCode;
    }

    @JsonProperty("HL7ResultStatusCode")
    public void setHL7ResultStatusCode(String hL7ResultStatusCode) {
        this.hL7ResultStatusCode = hL7ResultStatusCode;
    }

    @JsonProperty("RegisteredBy")
    public String getRegisteredBy() {
        return registeredBy;
    }

    @JsonProperty("RegisteredBy")
    public void setRegisteredBy(String registeredBy) {
        this.registeredBy = registeredBy;
    }

    @JsonProperty("TestedBy")
    public String getTestedBy() {
        return testedBy;
    }

    @JsonProperty("TestedBy")
    public void setTestedBy(String testedBy) {
        this.testedBy = testedBy;
    }

    @JsonProperty("AuthorisedBy")
    public String getAuthorisedBy() {
        return authorisedBy;
    }

    @JsonProperty("AuthorisedBy")
    public void setAuthorisedBy(String authorisedBy) {
        this.authorisedBy = authorisedBy;
    }

    @JsonProperty("OrderingNotes")
    public String getOrderingNotes() {
        return orderingNotes;
    }

    @JsonProperty("OrderingNotes")
    public void setOrderingNotes(String orderingNotes) {
        this.orderingNotes = orderingNotes;
    }

    @JsonProperty("EncryptedPatientID")
    public String getEncryptedPatientID() {
        return encryptedPatientID;
    }

    @JsonProperty("EncryptedPatientID")
    public void setEncryptedPatientID(String encryptedPatientID) {
        this.encryptedPatientID = encryptedPatientID;
    }

    @JsonProperty("AgeInYears")
    public String getAgeInYears() {
        return ageInYears;
    }

    @JsonProperty("AgeInYears")
    public void setAgeInYears(String ageInYears) {
        this.ageInYears = ageInYears;
    }

    @JsonProperty("AgeInDays")
    public String getAgeInDays() {
        return ageInDays;
    }

    @JsonProperty("AgeInDays")
    public void setAgeInDays(String ageInDays) {
        this.ageInDays = ageInDays;
    }

    @JsonProperty("HL7SexCode")
    public String getHL7SexCode() {
        return hL7SexCode;
    }

    @JsonProperty("HL7SexCode")
    public void setHL7SexCode(String hL7SexCode) {
        this.hL7SexCode = hL7SexCode;
    }

    @JsonProperty("HL7EthnicGroupCode")
    public String getHL7EthnicGroupCode() {
        return hL7EthnicGroupCode;
    }

    @JsonProperty("HL7EthnicGroupCode")
    public void setHL7EthnicGroupCode(String hL7EthnicGroupCode) {
        this.hL7EthnicGroupCode = hL7EthnicGroupCode;
    }

    @JsonProperty("Deceased")
    public String getDeceased() {
        return deceased;
    }

    @JsonProperty("Deceased")
    public void setDeceased(String deceased) {
        this.deceased = deceased;
    }

    @JsonProperty("Newborn")
    public String getNewborn() {
        return newborn;
    }

    @JsonProperty("Newborn")
    public void setNewborn(String newborn) {
        this.newborn = newborn;
    }

    @JsonProperty("HL7PatientClassCode")
    public String getHL7PatientClassCode() {
        return hL7PatientClassCode;
    }

    @JsonProperty("HL7PatientClassCode")
    public void setHL7PatientClassCode(String hL7PatientClassCode) {
        this.hL7PatientClassCode = hL7PatientClassCode;
    }

    @JsonProperty("AttendingDoctor")
    public String getAttendingDoctor() {
        return attendingDoctor;
    }

    @JsonProperty("AttendingDoctor")
    public void setAttendingDoctor(String attendingDoctor) {
        this.attendingDoctor = attendingDoctor;
    }

    @JsonProperty("TestingFacilityCode")
    public String getTestingFacilityCode() {
        return testingFacilityCode;
    }

    @JsonProperty("TestingFacilityCode")
    public void setTestingFacilityCode(String testingFacilityCode) {
        this.testingFacilityCode = testingFacilityCode;
    }

    @JsonProperty("ReferringRequestID")
    public String getReferringRequestID() {
        return referringRequestID;
    }

    @JsonProperty("ReferringRequestID")
    public void setReferringRequestID(String referringRequestID) {
        this.referringRequestID = referringRequestID;
    }

    @JsonProperty("Therapy")
    public String getTherapy() {
        return therapy;
    }

    @JsonProperty("Therapy")
    public void setTherapy(String therapy) {
        this.therapy = therapy;
    }

    @JsonProperty("LIMSAnalyzerCode")
    public String getLIMSAnalyzerCode() {
        return lIMSAnalyzerCode;
    }

    @JsonProperty("LIMSAnalyzerCode")
    public void setLIMSAnalyzerCode(String lIMSAnalyzerCode) {
        this.lIMSAnalyzerCode = lIMSAnalyzerCode;
    }

    @JsonProperty("TargetTimeDays")
    public String getTargetTimeDays() {
        return targetTimeDays;
    }

    @JsonProperty("TargetTimeDays")
    public void setTargetTimeDays(String targetTimeDays) {
        this.targetTimeDays = targetTimeDays;
    }

    @JsonProperty("TargetTimeMins")
    public String getTargetTimeMins() {
        return targetTimeMins;
    }

    @JsonProperty("TargetTimeMins")
    public void setTargetTimeMins(String targetTimeMins) {
        this.targetTimeMins = targetTimeMins;
    }

    @JsonProperty("LIMSRejectionCode")
    public String getLIMSRejectionCode() {
        return lIMSRejectionCode;
    }

    @JsonProperty("LIMSRejectionCode")
    public void setLIMSRejectionCode(String lIMSRejectionCode) {
        this.lIMSRejectionCode = lIMSRejectionCode;
    }

    @JsonProperty("LIMSRejectionDesc")
    public String getLIMSRejectionDesc() {
        return lIMSRejectionDesc;
    }

    @JsonProperty("LIMSRejectionDesc")
    public void setLIMSRejectionDesc(String lIMSRejectionDesc) {
        this.lIMSRejectionDesc = lIMSRejectionDesc;
    }

    @JsonProperty("LIMSFacilityCode")
    public String getLIMSFacilityCode() {
        return lIMSFacilityCode;
    }

    @JsonProperty("LIMSFacilityCode")
    public void setLIMSFacilityCode(String lIMSFacilityCode) {
        this.lIMSFacilityCode = lIMSFacilityCode;
    }

    @JsonProperty("Repeated")
    public String getRepeated() {
        return repeated;
    }

    @JsonProperty("Repeated")
    public void setRepeated(String repeated) {
        this.repeated = repeated;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}