package com.openldr.vl.repository;

import com.openldr.vl.domain.DrugAdherence;
import com.openldr.vl.repository.mapper.DrugAdherenceMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class DrugAdherenceRepository {

    @Autowired
    private DrugAdherenceMapper mapper;

    public void insert(DrugAdherence drugAdherence){
        mapper.insert(drugAdherence);
    }

    public void update(DrugAdherence drugAdherence)
    {
        mapper.update(drugAdherence);
    }

    public DrugAdherence getById(Long id)
    {
        return  mapper.getById(id);
    }

    public List<DrugAdherence> getAll(){
        return mapper.getAll();
    }


    public DrugAdherence getByName(String name) {
        return mapper.getByName(name);
    }

    public DrugAdherence loadByCode(String code) {
        return mapper.loadByCode(code);
    }
}
