package com.openldr.vl.dto;

/**
 * Created by hassan on 9/11/17.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ID",
        "MohswID",
        "FacilityName",
        "CommonName",
        "Zone",
        "Region",
        "District",
        "Council",
        "Ward",
        "VillageStreet",
        "FacilityType",
        "OperatingStatus",
        "Ownership",
        "RegistrationNumber",
        "Latitude",
        "Longitude",
        "Location"
})
public class HFRFacilityDTO {

    @JsonProperty("ID")
    private String id;
    @JsonProperty("MohswID")
    private String mohswID;
    @JsonProperty("FacilityName")
    private String facilityName;
    @JsonProperty("CommonName")
    private Object commonName;
    @JsonProperty("Zone")
    private String zone;
    @JsonProperty("Region")
    private String region;
    @JsonProperty("District")
    private String district;
    @JsonProperty("Council")
    private String council;
    @JsonProperty("Ward")
    private String ward;
    @JsonProperty("VillageStreet")
    private String villageStreet;
    @JsonProperty("FacilityType")
    private String facilityType;
    @JsonProperty("OperatingStatus")
    private String operatingStatus;
    @JsonProperty("Ownership")
    private String ownership;
    @JsonProperty("RegistrationNumber")
    private Object registrationNumber;
    @JsonProperty("Latitude")
    private Object latitude;
    @JsonProperty("Longitude")
    private Object longitude;
    @JsonProperty("Location")
    private String location;

    @JsonProperty("ID")
    public String getId() {
        return id;
    }

    @JsonProperty("ID")
    public void setId(String iD) {
        this.id = iD;
    }

    @JsonProperty("MohswID")
    public String getMohswID() {
        return mohswID;
    }

    @JsonProperty("MohswID")
    public void setMohswID(String mohswID) {
        this.mohswID = mohswID;
    }

    @JsonProperty("FacilityName")
    public String getFacilityName() {
        return facilityName;
    }

    @JsonProperty("FacilityName")
    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    @JsonProperty("CommonName")
    public Object getCommonName() {
        return commonName;
    }

    @JsonProperty("CommonName")
    public void setCommonName(Object commonName) {
        this.commonName = commonName;
    }

    @JsonProperty("Zone")
    public String getZone() {
        return zone;
    }

    @JsonProperty("Zone")
    public void setZone(String zone) {
        this.zone = zone;
    }

    @JsonProperty("Region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("Region")
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonProperty("District")
    public String getDistrict() {
        return district;
    }

    @JsonProperty("District")
    public void setDistrict(String district) {
        this.district = district;
    }

    @JsonProperty("Council")
    public String getCouncil() {
        return council;
    }

    @JsonProperty("Council")
    public void setCouncil(String council) {
        this.council = council;
    }

    @JsonProperty("Ward")
    public String getWard() {
        return ward;
    }

    @JsonProperty("Ward")
    public void setWard(String ward) {
        this.ward = ward;
    }

    @JsonProperty("VillageStreet")
    public String getVillageStreet() {
        return villageStreet;
    }

    @JsonProperty("VillageStreet")
    public void setVillageStreet(String villageStreet) {
        this.villageStreet = villageStreet;
    }

    @JsonProperty("FacilityType")
    public String getFacilityType() {
        return facilityType;
    }

    @JsonProperty("FacilityType")
    public void setFacilityType(String facilityType) {
        this.facilityType = facilityType;
    }

    @JsonProperty("OperatingStatus")
    public String getOperatingStatus() {
        return operatingStatus;
    }

    @JsonProperty("OperatingStatus")
    public void setOperatingStatus(String operatingStatus) {
        this.operatingStatus = operatingStatus;
    }

    @JsonProperty("Ownership")
    public String getOwnership() {
        return ownership;
    }

    @JsonProperty("Ownership")
    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    @JsonProperty("RegistrationNumber")
    public Object getRegistrationNumber() {
        return registrationNumber;
    }

    @JsonProperty("RegistrationNumber")
    public void setRegistrationNumber(Object registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @JsonProperty("Latitude")
    public Object getLatitude() {
        return latitude;
    }

    @JsonProperty("Latitude")
    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("Longitude")
    public Object getLongitude() {
        return longitude;
    }

    @JsonProperty("Longitude")
    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("Location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("Location")
    public void setLocation(String location) {
        this.location = location;
    }

}