package com.openldr.vl.service;

import com.openldr.vl.domain.HIVEquipmentTestControl;
import com.openldr.vl.repository.HIVEquipmentTestControlRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class HIVEquipmentTestControlService {

    @Autowired
    HIVEquipmentTestControlRepository repository;

    public void insert(HIVEquipmentTestControl control){
        repository.insert(control);
    }

    public void update(HIVEquipmentTestControl control)
    {
        repository.update(control);
    }

    public HIVEquipmentTestControl getById(Long id)
    {
        return  repository.getById(id);
    }

    public List<HIVEquipmentTestControl> getAll(){
        return repository.getAll();
    }

    public void save(HIVEquipmentTestControl control) {
        if(control.getId() ==null)
            insert(control);
        else
            update(control);
    }

}
