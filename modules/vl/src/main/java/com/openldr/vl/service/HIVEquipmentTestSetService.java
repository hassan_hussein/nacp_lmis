package com.openldr.vl.service;

import com.openldr.vl.domain.HIVEquipmentTestControl;
import com.openldr.vl.domain.HIVEquipmentTestSet;
import com.openldr.vl.repository.HIVEquipmentTestSetRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class HIVEquipmentTestSetService {

    @Autowired
    HIVEquipmentTestSetRepository repository;

    @Autowired
    HIVEquipmentTestControlService controlService;

    public void insert(HIVEquipmentTestSet set){
        repository.insert(set);
    }

    public void update(HIVEquipmentTestSet set)
    {
        repository.update(set);
    }

    public HIVEquipmentTestSet getById(Long id)
    {
        return  repository.getById(id);
    }

    public List<HIVEquipmentTestSet> getAll(){
        return repository.getAll();
    }

    public void save(HIVEquipmentTestSet set) {
        if(set.getId() ==null)
            insert(set);
        else
            update(set);
        saveSetControls(set);
    }

    private void saveSetControls(HIVEquipmentTestSet set){
        for(HIVEquipmentTestControl control:set.getEquipmentTestControls()){
            control.setTestSetId(set.getId());
            controlService.save(control);
        }
    }

}
