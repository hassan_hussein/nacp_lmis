package com.openldr.vl.repository.mapper;

import com.openldr.core.domain.*;
import com.openldr.core.dto.FacilitySupervisor;
import com.openldr.vl.domain.*;
import com.openldr.vl.dto.HFRFacilityDTO;
import com.openldr.vl.dto.NotificationDto;
import com.openldr.vl.dto.SampleFromHubDTO;
import com.openldr.vl.dto.SampleTestDTO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public interface SampleMapper {

    @Insert({"INSERT INTO hiv_samples(testtypeid,patientautoid,patientphonenumber, facilityid,labid,hubId, geographiczoneid,nameofcellleader," +
            "                     patientispregnant,patientisbreastfeeding,patienthastb,patientundertbtreatment," +
            "                     samplebarcodenumber,sampletypeid,patientregimenid,samplerequestdate,testreasonid," +
            "                     reasonforrepeattest,otherregimen,drugadherenceid,nameofpersonrequestingtest,cadreofpersonrequestingtest," +
            "                     dateofsamplecollection,namephlebotomist,dateofsampledispatch,nameofsampledispatcher," +
            "                     datereceived,testdate,testedby,verifiedby,verifieddate,resultdate,status,labnumber," +
            "                     createdby, createddate, modifiedby, modifieddate,formincomplete,isPending,batchNumber,orderId) " +
            "            VALUES (#{testTypeId},#{patientAutoId},#{patientPhoneNumber},#{facilityId},#{labId},#{hubId},#{geographicZoneId},#{nameOfCellLeader}," +
            "                    #{patientIsPregnant}, #{patientIsBreastFeeding}, #{patientHasTb}, #{patientUnderTbTreatment}, " +
            "                    #{sampleBarcodeNumber}, #{sampleTypeId}, #{patientRegimenId}, #{sampleRequestDate}, #{testReasonId}," +
            "                    #{reasonForRepeatTest}, #{otherRegimen}, #{drugAdherenceId}, #{nameOfPersonRequestingTest}, #{cadreOfPersonRequestingTest}," +
            "                    #{dateOfSampleCollection},#{nameOfPhlebotomist},#{dateOfSampleDispatch},#{nameOfSampleDispatcher}, " +
            "                    #{dateReceived}, #{testDate},#{testedBy},#{verifiedBy},#{verifiedDate},#{resultDate},#{status}::vlsamplestatus,#{labNumber}," +
            "                    #{createdBy},NOW(),#{modifiedBy},NOW(),#{formIncomplete},#{isPending},#{batchNumber},#{orderId})"})
    @Options(useGeneratedKeys = true)
    Integer insertSample(Sample sample);

    @Select(" select * from hiv_samples  WHERE id = #{id}")
    @Results({
            @Result(property = "patient", column = "patientAutoId", javaType = Patient.class,
                    one = @One(select = "com.openldr.vl.repository.mapper.PatientMapper.getById")),
            @Result(
                    property = "geographicZone", column = "geographicZoneId", javaType = GeographicZone.class,
                    one = @One(select = "com.openldr.core.repository.mapper.GeographicZoneMapper.getGeographicZoneById")),
            @Result(
                    property = "sampleType", column = "sampleTypeId", javaType = SampleType.class,
                    one = @One(select = "com.openldr.vl.repository.mapper.SampleTypeMapper.getSampleTypeById")),

                    @Result(
                    property = "drugAdherence", column = "drugAdherenceId", javaType = DrugAdherence.class,
                    one = @One(select = "com.openldr.vl.repository.mapper.DrugAdherenceMapper.getById"))}
           )
      Sample getAllById(@Param("id") Long id);


    @Insert("INSERT INTO patients(patientid,dob,gender)" +
            "            VALUES(#{patientId},#{dob},#{gender})")
    @Options(useGeneratedKeys = true)
    Integer insertPatient(Patient patient);

    @Select("Select * from patients where patientid=#{patientId}")
    Patient getPatientById(@Param("patientId")String patientId);

    //getSampleForLab
    @Select("Select s.*, date_part('year',age(p.dob))  age, f.code || '-' || p.patientid as patientCTCId ,p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub,  gz.region, gz.district,st.name sample from hiv_samples s " +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and s.status=#{status}::vlsamplestatus order by s.orderid asc, s.id,s.datereceived  ")
    List<SampleDTO> getSamplesByStatus(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId, @Param("status")String status,RowBounds rowBounds);

    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub,  gz.region, gz.district,st.name sample from hiv_samples s " +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and s.status=#{status}::vlsamplestatus and s.labnumber LIKE #{labNumber} || '%' order by s.id,s.datereceived ")
    List<SampleDTO> getSamplesByStatus2(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId, @Param("status")String status,@Param("labNumber")String labNumber,RowBounds rowBounds);

    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub,  gz.region, gz.district,st.name sample from hiv_samples s " +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} and s.labid=#{labId} and s.batchnumber=#{batchNumber} and s.status=#{status}::vlsamplestatus order by s.orderid asc,s.id,s.datereceived  ")
    List<SampleDTO> getSamplesByStatusAndBatch(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId,@Param("batchNumber")String batchNumber, @Param("status")String status);

    @Update("Update hiv_samples set status=#{status}::vlsamplestatus where id=#{id}")
    Integer acceptSample(@Param("id")Long id,
                         @Param("status")String status);

    @Update("Update hiv_samples set labnumber=#{labNumber}, datereceived = NOW(), status=#{status}::vlsamplestatus where id=#{id}")
    Integer receiveSample(@Param("id")Long id,@Param("labNumber")String labNumber,
                         @Param("status")String status);

    @Update("Update hiv_samples set status=#{status}::vlsamplestatus,reasonForRejection=#{reason} where id=#{sampleId}")
    Integer rejectSample(@Param("sampleId")Long sampleId,
                         @Param("status")String status,
                         @Param("reason")String reason);

    @Update("Update hiv_samples set status=#{status}::vlsamplestatus where id=#{id}")
    Integer updateStatus(@Param("status")String status);

//    @Select("Select f.*  from facilities f join users u on f.id=u.facilityid where u.id=#{userId} limit 1")
    @Select("Select f.*  from facilities f join users u on f.id=u.facilityid where u.id=#{userId} limit 1")
    @Results(value = {
            @Result(property = "facilityType", column = "typeid",javaType = FacilityType.class,
                    one = @One(select = "getFacilityType"))
    })
    Facility getHomeFacilityId(Long userId);

    @Select("Select * from facility_types where id=#{id} limit 1")
    FacilityType getFacilityType(@Param("id")Long id);

    @Select("Select f.* from requisition_group_members rgm " +
            " join requisition_groups rg on rg.id=rgm.requisitiongroupid " +
            " join supervisory_nodes sn on sn.id=rg.supervisorynodeid " +
            " join facilities f on f.id=rgm.facilityid " +
            " join facility_types ft on ft.id=f.typeid " +
            " where sn.facilityid=#{facilityId} and ft.code='LAB'")
    List<Facility> getSupervisedLabs(@Param("facilityId")Long facilityId);

    @Select(
            "             SELECT DISTINCT F.*\n" +
            "                      FROM role_assignments  \n" +
            "                       JOIN supervisory_nodes on supervisory_nodes.id = role_assignments.supervisorynodeid  \n" +
            "                      JOIN users on users.id = role_assignments.userid AND users.active = true  \n" +
            "                      JOIN facilities f on f.id = supervisory_nodes.facilityId\n" +
            "                       WHERE users.facilityId = #{facilityId}\n")
    List<Facility> getFacilitySupervisors(@Param("facilityId")Long facilityId);


    //    @Select("Select * from hiv_samples where id=#{sampleId}")
    @Select("Select s.*, date_part('year',age(p.dob))  age,s.modifiedDate rejectedDate, p.patientid as patientId,f.name as facility,f.id facilityId,l.name as lab,h.name as hub,  gz.region, gz.district, st.name sample from hiv_samples s " +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.id=#{sampleId} order by s.id   ")
    SampleDTO getById(@Param("sampleId")Long sampleId);

    @Select("Select s.*, date_part('year',age(p.dob))  age,s.modifiedDate rejectedDate, p.patientid as patientId,f.name as facility,f.id facilityId,l.name as lab,h.name as hub,  gz.region, gz.district, st.name sample from hiv_samples s " +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.butchNumber=#{butchNumber} order by s.id   ")
    SampleDTO getByBatch(@Param("butchNumber")String butchNumber);

    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility, l.name as lab,h.name as hub, gz.region, gz.district, st.name sample from hiv_samples s " +
            " join hiv_test_worksheet_samples ws on ws.sampleid=s.id " +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where ws.worksheetid=#{workSheetId}  order by s.id ")
    SampleDTO getByWorkSheetId(@Param("workSheetId")Long workSheetId);


    @Select("Select * from hiv_samples where testtypeid=#{testTypeId} and labid=#{labId} and status=#{status}::vlsamplestatus order by id,datereceived LIMIT #{total} ")
    List<SampleDTO> getSampleForWorkSheet(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId, @Param("status")String status,@Param("total")int total);

    @Select("Select f.id, f.name ||'-'||gz.district as name from facilities f \n" +
            "join view_tz_geographic_zones gz on gz.districtid=f.geographiczoneid")
    List<Facility> getFacilities();

    @Update("Update hiv_samples set status=#{status}::vlsamplestatus where id=#{id}")
    Integer updateSampleStatus(@Param("status") String status,@Param("id")Long id);

    @Select("Select * from facilities where id=#{labId}")
    Facility getSampleLab(@Param("labId")Long labId);

//     @Select("SELECT labNumber FROM hiv_samples WHERE labid=#{labId} AND labNumber !='' ORDER BY id DESC LIMIT 1")
    @Select("SELECT MAX(labNumber) FROM hiv_samples WHERE labid=#{labId} and DATE(createddate)=CURRENT_DATE AND orderid IS NULL")
    String getLastLabNumber(@Param("labId")Long labId);

    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub,  gz.region, gz.district,st.name sample from hiv_samples s " +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} AND  s.hubid=#{hubId} and s.status=#{status}::vlsamplestatus order by s.id, s.dateReceived asc")
    List<SampleDTO> getSamplesForHub(@Param("testTypeId")Long testTypeId,@Param("hubId")Long hubId, @Param("status")String status,RowBounds rowBounds);

    @Select("Select count(id) as total from hiv_samples where testtypeid=#{testTypeId} and hubid=#{hubId} and status=#{status}::vlsamplestatus  ")
    Integer getTotalSamplesForHub(@Param("testTypeId")Long testTypeId,@Param("hubId")Long hubId, @Param("status")String status);

    @Select("SELECT COUNT(id) AS total FROM hiv_samples WHERE testtypeid=#{testTypeId} AND labid=#{labId} AND status=#{status}::vlsamplestatus  ")
    Integer getTotalSamplesForLab(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId, @Param("status")String status);

    @Select("Select count(id) as total from hiv_samples where testtypeid=#{testTypeId} and labid=#{labId} and status=#{status}::vlsamplestatus  ")
    Integer getTotalSamplesForLabByColumn(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId, @Param("status")String status);

    @Select(
            "select N.months,test,result suspected,rejected from\n" +
            "(\n" +
            "SELECT DISTINCT MONTHS,count(TEST) test,FACILITYid FROM\n" +
            "(\n" +
            "select S.ID, count(*) test,FACILITYid,\n" +
            "(select to_char(to_timestamp(to_char(EXTRACT('MONTH' FROM CREATEDDATE::timestamp::date\n" +
            "), '999'), 'MM'), 'Mon') MONTHs)\n" +
            " FROM hiv_samples s \n" +
/*
            "WHERE \n" +
*/
            " \n" +
            "group by  s.FACILITYid,CREATEDdate,s.id\n" +
            ")M\n" +
            "GROUP BY m.months,FACILITYid) N,\n" +
            "( SELECT SUM(RESULT) RESULT, SUM(REJECTED) REJECTED ,FACILITYiD, months from (\n" +
            "select case when result = 'DETECTED' THEN 1 ELSE 0 END AS result,\n" +
            "CASE WHEN result = 'REJECTED' THEN 1 ELSE 0 END AS REJECTED,FACILITYiD,MONTHS\n" +
            " FROM(\n" +
            "SELECT RESULT,S.STATUS,S.facilityid, \n" +
            "(select to_char(to_timestamp(to_char(EXTRACT('MONTH' FROM  S.CREATEDDATE::timestamp::date\n" +
            "), '999'), 'MM'), 'Mon') MONTHs)\n" +
            "from hiv_RESULTs r\n" +
            "JOIN hiv_samples  s on  r.sampleID = s.id \n" +
            "JOIN hiv_testing_reasons rs ON s.testreasonid = rs.id\n" +
            "\n" +
            "WHERE  S.STATUS in ('APPROVED','DISPATCHED')\n" +
            ")M\n" +
            ")W\n" +
            "group by months,facilityId\n" +
            ")R order by months ")
    List<SampleDashboardDTO>getTotalTestForDashboard(@Param("facilityId") Long facilityId,@Param("typeId")Long typeId);
    @Select("\n" +
            "SELECT SAMPLE,REQUEST,CASE WHEN REASON IS NULL then 0 else REASON end as reason FROM(\n" +
            "select tt.name sample,COUNT(*) request,s.sampletypeid  from hiv_samples s\n" +
            "join sample_types TT ON s.sampletypeid = TT.ID" +
            "  WHERE  testtypeid = #{typeId}  \n" +
            "group by tt.name,s.sampletypeid  )X LEFT JOIN \n" +
            "(SELECT COUNT(*) reason,RS.CODE, sampleTypeId FROM hiv_samples s\n" +
            "JOIN hiv_testing_reasons rs ON S.TESTreasonId = rs.id\n" +
            "WHERE RS.CODE = 'REPEAT' AND testtypeid = #{typeId} \n" +
            "group by RS.CODE,sampleTypeId )Y ON X.sampletypeid = Y.sampleTypeId ")

    List<SampleDashboardDTO>getTestBySampleForDashboard(@Param("typeId")Long typeId, @Param("facilityId") Long facilityId);

    /*@Select("SELECT DISTINCT facilityName,request,CASE WHEN REASON IS NULL then 0 else REASON end as REPEATED FROM(\n" +
            "select f.name facilityName,COUNT(*) request ,facilityId from hiv_samples s\n" +
            "join facilities f ON s.sampletypeid = f.id\n" +
            "where s.status = 'ACCEPTED' AND testtypeid = #{typeId}  \n" +
            "group by f.name ,facilityId )X LEFT JOIN \n" +
            "(SELECT COUNT(*) reason,RS.CODE, facilityId FROM hiv_samples s\n" +
            "JOIN hiv_testing_reasons rs ON S.TESTreasonId = rs.id\n" +
            "WHERE RS.CODE = 'REPEAT' " +
            "  AND testtypeid = #{typeId}  \n" +
            "\n" +
            "group by RS.CODE,FACILITYiD )Y ON X.facilityId = Y.FACILITYID ")*/

    @Select(" \n" +
            "SELECT DISTINCT facilityName,request,CASE WHEN REASON IS NULL then 0 else REASON end as REPEATED FROM(\n" +
            "            select f.name facilityName,COUNT(*) request ,facilityId from hiv_samples s\n" +
            "            join facilities f ON s.sampletypeid = f.id\n" +
            "            group by f.name ,facilityId )X LEFT JOIN \n" +
            "            (SELECT COUNT(*) reason,RS.CODE, facilityId FROM hiv_samples s\n" +
            "            JOIN hiv_testing_reasons rs ON S.TESTreasonId = rs.id\n" +
            "            group by RS.CODE,FACILITYiD )Y ON X.facilityId = Y.FACILITYID ")
    List<SampleDashboardDTO>getSampleByFacilityForDashboard(@Param("typeId")Long typeId, @Param("facilityId") Long facilityId);


    @Select("        Select s.*,s.modifiedDate rejectedDate, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub,  gz.region, gz.district,st.name sample from hiv_samples s   " +
                    " left join patients p on p.id=s.patientautoid   "+
                       " left join facilities f on f.id=s.facilityid  " +
                        " left join facilities l on l.id=s.labid   " +
                        " left join facilities h on h.id=s.hubid    " +
                        " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid  " +
                        " left join sample_types st on st.id=s.sampletypeid  "  +
                        " where testtypeid = #{typeId} AND  s.labid=#{labId} and s.status=#{status}::vlsamplestatus order by s.dateReceived asc    "
            )
    List<SampleDTO> getSampleRejectedForLab(@Param("typeId") Long typeId,@Param("labId")Long labId, @Param("status")String status,RowBounds rowBounds);

    @Select("  With q as ( Select s.id,\n" +
            "                         date_part('year',age(p.dob))  age from hiv_samples s  \n" +
            "                         join patients p on p.id=s.patientautoid \n" +
            "                         where testtypeid = #{typeId}  AND s.labid=#{labId}  )\n" +
            "                         select count(*)totalTest ,age from q\n" +
            "                         group by age ")
    List<SampleTestDTO>getSampleTestByAge(@Param("typeId") Long typeId, @Param("labId") Long labId);

    @Select("select labId from hiv_samples where facilityId = #{facilityId} order by s.id ")
    Long getLabIdForUser(@Param("facilityId") Long facilityId);

    @Select(" delete from hiv_sample_status_changes where sampleId= #{id}; " +
            "delete from hiv_samples where id = #{id}")
    Long delete(@Param("id") Long id);

    @Insert(" INSERT INTO sample_from_hubs(\n" +
            "            orderid, testcode, formtype, patientcenter, testreason, sampletype, \n" +
            "            samplerequesttime, samplerequestername, datesampletaken, datesamplesent, \n" +
            "            timesamplesent, phlebotomistname, phlebotomistcadre,dispatchername, testcomment, \n" +
            "            datereceivedlab, orderstatus, updatedat, createdat, patientcode, \n" +
            "            testcenter, hubcenter, orderedat, resultreceivedat, registerturnround, \n" +
            "            resultturnround, batchcode, transitstatus, ontransitat, receivedlabat, \n" +
            "            labcomment, registeredby, originallab, diseasename, istreatmentstarted, \n" +
            "            istreatmentcompleted, treatmentstartedon, symptomstatus, drugtype, \n" +
            "            drugname, drugadherance, usergroup, gender, pcreateddate, ispregnant, hastb,isontbtreatment, \n" +
            "            isbreastfeeding, patientid, pid, patientphone, patientnursephone, \n" +
            "            wardid, birthdate, createddate, results, createdby,isReceivedAtLAB,dateReceivedAtLab,isPending, hfr_code)\n" +
            "    VALUES (#{orderID}, #{testCode}, #{formType}, #{patientCenter}, #{testReason}, #{sampleType}, \n" +
            "            #{sampleRequestTime}, #{sampleRequesterName}, #{dateSampleTaken}, #{dateSampleSent}, \n" +
            "            #{timeSampleSent}, #{phlebotomistName}, #{phlebotomistCadre},#{dispatcherName}, #{dateSampleSent}, \n" +
            "            #{dateReceivedLab}, #{orderStatus}, #{updatedAt}, #{createdAt}, #{patientCode}, \n" +
            "            #{testCenter}, #{hubCenter}, #{orderedAt}, #{resultReceivedAt}, #{registerTurnRound}, \n" +
            "            #{resultTurnRound}, #{batchCode}, #{transitStatus}, #{onTransitAt}, #{receivedLabAt}, \n" +
            "            #{labComment}, #{registeredBy}, #{originalLab}, #{diseaseName}, #{isTreatmentStarted}, \n" +
            "            #{isTreatmentCompleted}, #{treatmentStartedOn}, #{symptomStatus}, #{drugType}, \n" +
            "            #{drugName}, #{drugAdherance}, #{userGroup},#{gender}, #{pcreatedDate}, #{isPregnant}, #{hasTB},#{isOnTBTreatment}, \n" +
            "            #{isBreastFeeding}, #{patientID}, #{pID}, #{patientPhone}, #{patientNursePhone}, \n" +
            "            #{wardId}, #{birthDate}, #{createdDate}, #{results}, #{createdBy},#{isReceivedAtLAB},NOW(),true, #{hfrID}); ")
    @Options(useGeneratedKeys = true)
    Integer insertSampleFromHub(SampleFromHubDTO hubDTO);

    @Select("SELECT * FROM sample_from_hubs where orderID = #{orderID} order by id desc limit 1")
    SampleFromHubDTO getByOrderId(@Param("orderID") String orderID);
    /* orderStatus =1::VARCHAR and isReceivedAtLAB=true and isPending = false*/
    @Select("select * from sample_from_hubs where isSaved=false")
    List<SampleFromHubDTO> getByStatus();

    @Update("update sample_from_hubs set isPending = false where orderID = #{orderID} ")
    String updateStatusFromHub(@Param("orderID") String orderID);
    @Select("SELECT * FROM sample_from_hubs")
    List<HashMap>getAllFrom();



    @Select("Select * from facilities where id=#{hubId}")
    Facility getSampleHub(@Param("hubId")Long hubId);


    @Select("Select s.*, date_part('year',age(p.dob))  age, p.patientid as patientId,f.name as facility,l.name as lab,h.name as hub,  gz.region, gz.district,st.name sample from hiv_samples s " +
            " left join patients p on p.id=s.patientautoid " +
            " left join facilities f on f.id=s.facilityid " +
            " left join facilities l on l.id=s.labid " +
            " left join facilities h on h.id=s.hubid " +
            " left join view_tz_geographic_zones gz on gz.districtid=s.geographiczoneid " +
            " left join sample_types st on st.id=s.sampletypeid " +
            " where s.testtypeid=#{testTypeId} and (s.labid=#{labId} or s.hubId = #{labId}) and s.batchnumber=#{batchNumber} and s.status=#{status}::vlsamplestatus order by s.id,s.datereceived  ")
    List<SampleDTO> getByStatusAndBatch(@Param("testTypeId")Long testTypeId,@Param("labId")Long labId,@Param("batchNumber")String batchNumber, @Param("status")String status);


    @Select("SELECT * FROM sample_from_hubs WHERE isSaved=false")
    @Results(value = {
            @Result(property = "hfrID", column = "hfr_code",javaType = String.class)})
    List<SampleFromHubDTO> getSampleFromHubList();

    @Update("UPDATE sample_from_hubs SET issaved=true WHERE orderId = #{orderId} ")
    void updateIsSaved(@Param("orderId") String orderId);

    @Update("update sample_from_hubs seT orderStatus=#{status}::VARCHAR  where orderId = #{orderId} ")
    void updateSendNotificationStatus(@Param("status")String status, @Param("orderId") String orderId);

    @Select("select * from hfr_facilities where lower(id) = #{patientCenter}")
    HFRFacilityDTO getHFRFacilities(@Param("patientCenter") String patientCenter);

    @Select("SELECT  " +
            "s.labnumber, s.orderid orderId, s.testtypeid testTypeId, r.result, f.name labName, s.dateofsamplecollection sampleCollectionDate, " +
            "(select x.createddate from hiv_sample_status_changes x where x.sampleid=s.id and x.status='RECEIVED') as registrationDate," +
            "r.testdate analysisDate, r.resultdate resultDate, " +
            "(SELECT u.firstname || ' ' || u.lastname FROM users u INNER JOIN hiv_sample_status_changes x ON x.createdby=u.id WHERE x.sampleid=s.id AND x.status='RECEIVED') AS registeredBy," +
            "(SELECT u.firstname || ' ' || u.lastname FROM users u INNER JOIN hiv_sample_status_changes x ON x.createdby=u.id WHERE x.sampleid=s.id AND x.status='APPROVED') AS authorisedBy," +
            "(SELECT x.createddate FROM hiv_sample_status_changes x WHERE x.sampleid=s.id AND x.status='APPROVED') AS authorisedDate," +
	    "r.testedby testedBy, r.createddate resultedDate " +
            " FROM hiv_samples s INNER JOIN hiv_results r ON r.sampleid=s.id " +
            " INNER JOIN facilities f ON s.labid=f.id " +
            " WHERE s.id=#{sampleId}")
    NotificationDto loadNotificationById(@Param("sampleId")Long sampleId);

    @Select("SELECT * FROM hiv_samples where labnumber=#{labnumber}")
    Sample getByLabnumber(@Param("labnumber") String labnumber);
}
