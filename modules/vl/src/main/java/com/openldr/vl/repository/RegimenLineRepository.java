package com.openldr.vl.repository;

import com.openldr.vl.domain.RegimenLine;
import com.openldr.vl.repository.mapper.RegimenLineMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class RegimenLineRepository {

    @Autowired
    private RegimenLineMapper mapper;

    public void insert(RegimenLine regimenLine){
        mapper.insert(regimenLine);
    }

    public void update(RegimenLine regimenLine)
    {
        mapper.update(regimenLine);
    }

    public RegimenLine getById(Long id)
    {
        return  mapper.getById(id);
    }

    public List<RegimenLine> getAll(){
        return mapper.getAll();
    }

    public List<RegimenLine> getByGroup(Long regimenGroupId){
        return mapper.getByGroup(regimenGroupId);
    }


}
