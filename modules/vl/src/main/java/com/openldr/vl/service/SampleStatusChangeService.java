package com.openldr.vl.service;

import com.openldr.vl.domain.SampleStatusChange;
import com.openldr.vl.repository.SampleStatusChangeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by hassan on 4/3/17.
 */
@Service
public class SampleStatusChangeService {

    @Autowired
    private SampleStatusChangeRepository repository;

    public void insert(SampleStatusChange sampleStatusChange){
        repository.insert(sampleStatusChange);
    }

    public void updateSampleStatusChange(SampleStatusChange change){

        repository.updateStatus(change);
    }

}
