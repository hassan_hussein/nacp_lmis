package com.openldr.vl.repository;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.HIVTestType;
import com.openldr.core.domain.Pagination;
import com.openldr.core.repository.FacilityRepository;
import com.openldr.core.repository.mapper.FacilityMapper;
import com.openldr.vl.domain.Patient;
import com.openldr.vl.domain.Sample;
import com.openldr.vl.domain.SampleDTO;
import com.openldr.vl.domain.SampleDashboardDTO;
import com.openldr.vl.dto.HFRFacilityDTO;
import com.openldr.vl.dto.NotificationDto;
import com.openldr.vl.dto.SampleFromHubDTO;
import com.openldr.vl.dto.SampleTestDTO;
import com.openldr.vl.repository.mapper.ResultMapper;
import com.openldr.vl.repository.mapper.SampleMapper;
import lombok.NoArgsConstructor;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@NoArgsConstructor
public class SampleRepository {

    @Autowired
    private SampleMapper mapper;

    @Autowired
    private FacilityMapper facilityMapper;

    @Autowired
    private ResultMapper resultMapper;

    public void insertSample(Sample sample){
        mapper.insertSample(sample);
    }

    public Sample getAllById(Long id){
        return mapper.getAllById(id);
    }
    public void insertPatient(Patient patient){
        if (getByPatientId(patient.getPatientId())==null)
        mapper.insertPatient(patient);
    }

    public Patient getByPatientId(String patientId){
        return mapper.getPatientById(patientId);
    }

    //getSampleForLab
    public List<SampleDTO> getSamplesByStatus(Long testTypeId,Long labId, String status,Pagination pagination) {
        if(status.equalsIgnoreCase("RESULTED") || status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("DISPATCHED"))
            return resultMapper.getResultedSamples(testTypeId,labId,status,pagination);
        else
            return mapper.getSamplesByStatus(testTypeId,labId, status, pagination);
    }

    public List<SampleDTO> getSamplesByStatusAndColumn(Long testTypeId,Long labId, String status,String column,Pagination pagination) {
        if(status.equalsIgnoreCase("RESULTED") || status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("DISPATCHED")) {
            return resultMapper.getResultedSamples2(testTypeId, labId, status, column, pagination);
        }else
            return mapper.getSamplesByStatus2(testTypeId,labId, status, column,pagination);
    }



    public List<SampleDTO> getSamplesForHub(Long testTypeId,Long hubId, String status,Pagination pagination) {

        if(status.equalsIgnoreCase("RESULTED") || status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("DISPATCHED"))
            return resultMapper.getResultedSamplesForHub(testTypeId,hubId,status,pagination);
        else
        return mapper.getSamplesForHub(testTypeId,hubId,status,pagination);
    }

    public List<SampleDTO> getSamplesByStatusAndBatch(Long testTypeId,Long labId, String batchNumber,String status) {

        return mapper.getSamplesByStatusAndBatch(testTypeId,labId,batchNumber,status);
    }
  public List<SampleDTO> getByStatusAndBatch(Long testTypeId,Long labId, String batchNumber,String status) {

        return mapper.getByStatusAndBatch(testTypeId, labId, batchNumber, status);
    }

    public void acceptSample(Long sampleId, String status ) {
        mapper.acceptSample(sampleId,status);
    }

    public Facility getHomeFacilityId(Long userId) {
        return facilityMapper.getHomeFacilityId(userId);
    }
    public void receiveSample(Long sampleId,String labNumber, String status ) {
        mapper.receiveSample(sampleId,labNumber,status);
    }

    public SampleDTO getById(Long sampleId) {
        return mapper.getById(sampleId);
    }

    public SampleDTO getByBatch(String sampleId) {
        return mapper.getByBatch(sampleId);
    }

    public List<SampleDTO> getSampleForWorkSheet(Long testTypeId,Long labId,String status,int total) {
        return mapper.getSampleForWorkSheet(testTypeId,labId,status,total);
    }

    public List<Facility> getFacilities(){
        return mapper.getFacilities();
    }

    public Integer updateSampleStatus(String status, Long id){
        return mapper.updateSampleStatus(status,id);
    }

    public void rejectSample(Long sampleId, String status, String reason) {
        mapper.rejectSample(sampleId,status,reason);
    }

    public List<Facility> getSupervisedLabs(Long facilityId){
        return mapper.getSupervisedLabs(facilityId);
    }

   public List<Facility> getFacilitySupervisors(Long facilityId){
        return mapper.getFacilitySupervisors(facilityId);
    }

    public Facility getSampleLab(Long labId) {
        return mapper.getSampleLab(labId);
    }

    public String getLastLabNumber(Long labId)
    {
        return mapper.getLastLabNumber(labId);
    }



    public Integer getTotalSamplesForHub(Long testTypeId,Long hubId, String status)
    {
        return mapper.getTotalSamplesForHub(testTypeId,hubId,status);
    }

    public Integer getTotalSamplesForLab(Long testTypeId,Long hubId, String status)
    {
        return mapper.getTotalSamplesForLab(testTypeId,hubId, status);
    }

    public List<SampleDashboardDTO>getTotalTestDoneForDashboard(Long facilityId,Long typeId){
        return mapper.getTotalTestForDashboard(facilityId, typeId);
    }
    public List<SampleDashboardDTO>getTestBySampleForDashboard(Long typeId,Long facilityId){
        return mapper.getTestBySampleForDashboard(typeId,facilityId);
    }

    public List<SampleDashboardDTO>getSampleByFacilityForDashboard(Long typeId,Long facilityId){
        return mapper.getSampleByFacilityForDashboard(typeId, facilityId);
    }

    public List<SampleDTO> getRejectedSamplesForLab(Long typeId,Long id1, String status,Pagination pagination) {
        return mapper.getSampleRejectedForLab(typeId,id1,status,pagination);
    }
    public List<SampleTestDTO> getSampleTestByAge(Long typeId,Long labId) {
        return mapper.getSampleTestByAge(typeId, labId);
    }

    public Long getLabIdForByUser(Long facilityId) {
        return mapper.getLabIdForUser(facilityId);
    }

    public Long delete(Long id) {
        return mapper.delete(id);
    }


    public void insertHubSample(SampleFromHubDTO hubDTO) {

        mapper.insertSampleFromHub(hubDTO);

    }

    public SampleFromHubDTO getByOrderId(String orderId){
        return mapper.getByOrderId(orderId);
    }

    public List<SampleFromHubDTO> getByStatus() {
        return mapper.getByStatus();
    }
    public List<HashMap>getAllFrom(){
        return mapper.getAllFrom();
    }

    public String updateStatusFromHub(String orderId){
        return mapper.updateStatusFromHub(orderId);
    }

    public Facility getSampleHub(Long hubId) {
        return mapper.getSampleHub(hubId);
    }

    public List<SampleFromHubDTO> getSampleFromHubList() {
        return mapper.getSampleFromHubList();
    }
    public void updateIsSaved(String orderID){
         mapper.updateIsSaved(orderID);
    }

    public void updateSendNotificationStatus( String orderID,String status) {
        System.out.println(orderID+" "+status);
        mapper.updateSendNotificationStatus(status, orderID);
    }

    public HFRFacilityDTO getHFRFacilities(String patientCenter) {
       return mapper.getHFRFacilities(patientCenter);
    }

    public NotificationDto loadNotificationById(Long sampleId) {
        return mapper.loadNotificationById(sampleId);
    }
}
