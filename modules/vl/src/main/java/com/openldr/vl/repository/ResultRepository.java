package com.openldr.vl.repository;

import com.openldr.core.domain.Pagination;
import com.openldr.vl.domain.SampleDTO;
import com.openldr.vl.domain.TestResult;
import com.openldr.vl.repository.mapper.ResultMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class ResultRepository {

    @Autowired
    private ResultMapper mapper;

    public List<SampleDTO> getResultedSamples(Long testTypeId,Long labId, String status,Pagination pagination){
        return mapper.getResultedSamples(testTypeId,labId,status,pagination);
    }
  public List<SampleDTO> getResultedSamplesList(Long testTypeId,Long labId, String status,Pagination pagination){
        return mapper.getResultedSamplesList(testTypeId,labId,status,pagination);
    }


    public Integer getTotalResulted(Long testTypeId,Long labId, String resultStatus) {
        return mapper.getTotalResulted(testTypeId,labId,resultStatus);
    }
    public Integer getTotalResultedHub(Long testTypeId,Long labId, String resultStatus) {
        return mapper.getTotalResultedHub(testTypeId,labId,resultStatus);
    }

    public List<SampleDTO> getResultedListSamples(Long testTypeId,Long labId, String status,Pagination pagination) {
        return mapper.getResultedListSamples(testTypeId, labId, status, pagination);
    }

    public List<SampleDTO> getDispatchedResultsForHub(Long testTypeId,Long hubId, String status,Pagination pagination) {
        return mapper.getDispatchedResultsForHub(testTypeId, hubId, status, pagination);
    }

    public TestResult getResultBySampleId(Long sampleId) {
        return mapper.getResultBySampleId(sampleId);
    }
}
