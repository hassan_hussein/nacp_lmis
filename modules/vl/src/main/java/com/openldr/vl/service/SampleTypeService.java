package com.openldr.vl.service;

import com.openldr.vl.domain.SampleType;
import com.openldr.vl.repository.SampleTypeRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class SampleTypeService {

    @Autowired
    SampleTypeRepository repository;

    public void insertSampleType(SampleType sampleType){
        repository.insertSampleType(sampleType);
    }

    public void updateSampleType(SampleType sampleType)
    {
        repository.updateSampleType(sampleType);
    }

    public SampleType getSampleTypeById(Long id)
    {
        return  repository.getSampleTypeById(id);
    }

    public List<SampleType> getAllSampleTypes(){
        return repository.getAllSampleTypes();
    }

    public void save(SampleType sampleType) {
        if(sampleType.getId() ==null)
            insertSampleType(sampleType);
        else
            updateSampleType(sampleType);
    }

    public SampleType getSampleTypeByName(String sampleType) {
        return repository.getSampleTypeByName(sampleType);
    }

    public SampleType getByCode(String code){
        return repository.loadByCode(code);
    }
}
