package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.HIVEquipmentTestSet;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HIVEquipmentTestSetMapper {

    @Insert({"INSERT INTO hiv_equipment_test_sets (testtypeid, equipmentid,numberofsamples, numberofcontrols) " +
            "                   Values (#{testTypeId},#{equipmentId},#{numberOfSamples},#{numberOfControls})"})
    @Options(useGeneratedKeys = true)
    Integer insert(HIVEquipmentTestSet set);

    @Insert({"UPDATE hiv_equipment_test_sets set testtypeid=#{testTypeId}, equipmentid=#{equipmentId}, numberofsamples=#{numberOfSamples},numberofcontrols=#{numberOfControls} WHERE id=#{id}"})
    Integer update(HIVEquipmentTestSet set);

    @Select("Select * from hiv_equipment_test_sets where id=#{id}")
            @Results({
                    @Result(property = "id", column = "id"),
                    @Result(property = "equipmentTestControls", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.vl.repository.mapper.HIVEquipmentTestControlMapper.getBySet"))

            })
    HIVEquipmentTestSet getById(@Param("id") Long id);

    @Select("Select * from hiv_equipment_test_sets")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "equipmentTestControls", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.vl.repository.mapper.HIVEquipmentTestControlMapper.getBySet"))

    })
    List<HIVEquipmentTestSet> getAll();

    @Select("Select * from hiv_equipment_test_sets where equipmentId=#{equipmentId} and testTypeId=#{testTypeId} LIMIT 1")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "equipmentTestControls", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.vl.repository.mapper.HIVEquipmentTestControlMapper.getBySet"))

    })
    HIVEquipmentTestSet getByEquipmentAndType(@Param("equipmentId")Long equipmentId, @Param("testTypeId")Long testTypeId);
}
