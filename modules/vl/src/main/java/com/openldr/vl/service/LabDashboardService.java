package com.openldr.vl.service;

import com.openldr.vl.dto.LabDTO;
import com.openldr.vl.repository.LabDashBoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 9/12/17.
 */

@Service
public class LabDashboardService {
@Autowired
LabDashBoardRepository repository;
    public List<Map<String,String>> getTAT(Long labId) {
        return repository.getTAT(labId);
    }

    public List<LabDTO> getLaboratories() {
        return repository.getLaboratories();
    }

    public LinkedHashMap<String, Object> getTAT(Date startDate, Date endDate, String lab) {
        return repository.getTAT(startDate, endDate, lab);
    }

    public List<Map<String, String>> getTrendData(String startDate, String endDate, String lab){
        return repository.getTrendData(startDate,endDate, lab);
    }

    public List<Map<String, String>> getByGender(Date startDate, Date endDate, String lab){
        return repository.getByGender(startDate, endDate, lab);
    }

    public List<Map<String, String>> getByAge(Date startDate, Date endDate, String lab){
        return repository.loadByAge(startDate, endDate, lab);
    }

    public List<Map<String, String>> load(Date startDate, Date endDate, String lab, String type){
        return repository.load(startDate, endDate, lab,type);
    }
}
