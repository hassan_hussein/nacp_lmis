package com.openldr.vl.service;

import com.openldr.vl.domain.HIVTestEquipment;
import com.openldr.vl.repository.HIVTestEquipmentRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class HIVTestEquipmentService {

    @Autowired
    HIVTestEquipmentRepository repository;

    public void insert(HIVTestEquipment equipment){
        repository.insert(equipment);
    }

    public void update(HIVTestEquipment equipment)
    {
        repository.update(equipment);
    }

    public HIVTestEquipment getById(Long id)
    {
        return  repository.getById(id);
    }

    public List<HIVTestEquipment> getAll(){
        return repository.getAll();
    }

    public void save(HIVTestEquipment equipment) {
        if(equipment.getId() ==null)
            insert(equipment);
        else
            update(equipment);
    }

}
