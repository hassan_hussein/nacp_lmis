package com.openldr.vl.service;

import com.openldr.vl.dto.HFDTO;
import com.openldr.vl.repository.MainDashboardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by hassan on 9/3/17.
 */
@Service
public class MainDashBoardService {

    @Autowired
    private MainDashboardRepository repository;

    public LinkedHashMap<String,Object> getTAT(Date startDate, Date endDate,String region){
        return repository.getTAT(startDate,endDate,region);
    }

    public List<HashMap<String,Object>>getSampleTypesByYear(Long year){
        return repository.getSampleTypesByYear(year);
    }

    public List<Map<String, String>> getTrendData(String startDate, String endDate,String region) {
        return repository.getTrendData(startDate,endDate, region);
    }
    public List<Map<String, String>> getByGender(Date startDate, Date endDate,String region) {
        return repository.getByGender(startDate,endDate,region);
    }

    public List<Map<String, String>> getByAge(Date startDate, Date endDate,String region) {
        return repository.getByAge(startDate,endDate,region);
    }
    public List<Map<String, String>> getByResult(Date startDate, Date endDate,String region) {
        return repository.getByResult(startDate,endDate,region);
    }
    public List<Map<String, String>> getByJustification(Date startDate, Date endDate,String region) {
        return repository.getByJustification(startDate,endDate,region);
    }
    public List<Map<String, String>> getByRegion(Date startDate, Date endDate,String region) {
        return repository.getByRegion(startDate, endDate,region);
    }
    public List<HFDTO>getRegions(){
        return repository.getRegions();
    }
}
