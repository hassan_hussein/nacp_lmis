package com.openldr.vl.repository;

import com.openldr.vl.dto.LabDTO;
import com.openldr.vl.repository.mapper.LabDashBoardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hassan on 9/12/17.
 */
@Repository
public class LabDashBoardRepository {
    @Autowired
    private LabDashBoardMapper mapper;

    public List<Map<String,String>> getTAT(Long labId) {
//        return mapper.getTAT(labId);
        return null;
    }

    public List<LabDTO> getLaboratories() {
        return mapper.loadLabs();
    }

    public LinkedHashMap<String, Object> getTAT(Date startDate, Date endDate, String lab) {
        return mapper.getTAT(startDate, endDate, lab);
    }

    public List<Map<String, String >> getTrendData(String startDate, String endDate, String lab){
        return mapper.getTrendData(startDate, endDate,lab);
    }

    public List<Map<String, String>> getByGender(Date startDate, Date endDate, String lab){
        return mapper.loadTestsByGender(startDate,endDate, lab);
    }

    public List<Map<String, String>> loadByAge(Date startDate, Date endDate, String lab){
        return mapper.loadByAge(startDate,endDate, lab);
    }

    public List<Map<String, String>> load(Date startDate, Date endDate, String lab, String type){
        switch (type){
            case "BY-RESULT":
                return mapper.loadByResult(startDate, endDate,lab);
            case "BY-JUSTIFICATION":
                return mapper.loadByJustification(startDate,endDate,lab);
            case "BY-LABORATORY":
                return mapper.loadByLaboratory(startDate,endDate);
            default:
                return null;
        }
    }
}
