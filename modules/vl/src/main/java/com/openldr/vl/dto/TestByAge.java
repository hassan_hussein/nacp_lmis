package com.openldr.vl.dto;

/**
 * Created by hassan on 7/13/17.
 */


import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Location",
        "Total",
        "LessThanAndEqualTo5",
        "GreaterThan5LessThanAndEqualTo15",
        "GreaterThan15",
        "Year",
        "Month"
})
@Data
public class TestByAge {

    @JsonProperty("Location")
    public String location;
    @JsonProperty("Total")
    public Integer total;
    @JsonProperty("LessThanAndEqualTo5")
    public Integer lessThanAndEqualTo5;
    @JsonProperty("GreaterThan5LessThanAndEqualTo15")
    public Integer greaterThan5LessThanAndEqualTo15;
    @JsonProperty("GreaterThan15")
    public Integer greaterThan15;
    @JsonProperty("Year")
    public Integer year;
    @JsonProperty("Month")
    public Integer month;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}