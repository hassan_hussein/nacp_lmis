package com.openldr.vl.repository.mapper;

import com.openldr.vl.dto.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 8/9/17.
 * Mapper for
 */
@Repository
public interface NationalDashboardApiMapper {

    @Select(" \n" +
            "   SELECT \n" +
            "    to_char(NOW()::timestamp, 'YYYY/MM/DD HH24:MI:SS') DateTimeStamp,'1.0.0' versionstamp, " +
            " to_char(c.createdDate::timestamp, 'YYYY/MM/DD HH24:MI:SS')    " +
            " limsdatetimestamp,'01.00.00.000' limsversionstamp,\n" +
            "     CASE WHEN length(labnumber)< 20 then  'TZNACP-'||labnumber else 'TZNACP-'||substring(labnumber,1,3)||split_part(labnumber, '-', 2) end as RequestID," +
            "'1' obrsetid,\n" +
            "\n" +
            "    '' LOINCPanelCode,\n" +
            " 'HIVVL' LIMSPanelCode,\n" +
            " 'HIV VIRAL LOAD' LIMSPanelDesc,\n" +
            "'R' HL7PriorityCode,\n" +
            "     dateofsamplecollection SpecimenDateTime,\n" +
            "   samplerequestdate  registereddatetime,\n" +
            "   dateofsampledispatch receiveddatetime,\n" +
            " dateofsampledispatch AnalysisDateTime,\n" +
            " dateofsampledispatch AuthorisedDateTime,\n" +
            " dateofsampledispatch AdmitAttendDateTime ,\n" +
            "   '0.0' CollectionVolume,\n" +
            "     f.code RequestingFacilityCode,\n" +
            "\n" +
//            "   (select f.code from facilities fm where fm.id = s.labId)\n" +
            "\n" +
            "(\n" +
            "SELECT f.code FROM SUPERVISORY_NODES sn\n" +
            "JOIN facilities f ON F.id = sn.facilityID and typeid =1 and sn.facilityID =s.labid LIMIT 1)\n" +
            "     \n" +
            "   ReceivingFacilityCode,'' LIMSPointOfCareDesc,'' RequestTypeCode,'' ICD10ClinicalInfoCodes,\n" +
            "   '' ClinicalInfo,'' HL7SpecimenSourceCode, st.code LIMSSpecimenSourceCode, st.name LIMSSpecimenSourceDesc,\n" +
            "  '' HL7SpecimenSiteCode,'' LIMSSpecimenSiteCode, '' LIMSSpecimenSiteDesc, '0.0' WorkUnits, '0.0' CostUnits,\n" +
            "  '' HL7SectionCode, '' HL7ResultStatusCode,namephlebotomist RegisteredBy, '' TestedBy,'' AuthorisedBy,\n" +
            "   labnumber OrderingNotes, (select patientid from patients where id = s.patientautoid) EncryptedPatientID,\n" +
            "   date_part('year',age(p.dob)) AgeInYears,(now()::date -p.dob::date) AgeInDays,\n" +
            "   substr(gender,0,2) HL7SexCode, '' HL7EthnicGroupCode,'false' Deceased,'false' Newborn, '' HL7PatientClassCode,\n" +
            "   nameofpersonrequestingtest AttendingDoctor,\n" +
            "(\n" +
            "SELECT f.code FROM SUPERVISORY_NODES sn\n" +
            "JOIN facilities f ON F.id = sn.facilityID and typeid =1 and sn.facilityID =s.labid LIMIT 1)\n" +
            "   TestingFacilityCode,f.code ReferringRequestID, '' Therapy, ''  LIMSAnalyzerCode\n" +
            " , '0' TargetTimeDays,'0' TargetTimeMins, '' LIMSRejectionCode, '' LIMSRejectionDesc,\n" +
            " F.ID LIMSFacilityCode, 0 Repeated \n" +
            "  \n" +
            "from HIV_SAMPLE_STATUS_CHANGES C\n" +
            "   INNER JOIN hiv_samples s on C.sampleID= s.id\n" +
            "   INNER JOIN FACILITIES F ON F.ID = S.facilityID\n" +
            "   inner join sample_types st on S.SAMPLETYPEID = st.id\n" +
            "   INNER JOIN patients p on p.id = s.patientautoid\n" +
            "   WHERE C.STATUS='RECEIVED' and issenttonationalrep=false \n ")
    List<NationalAPI> getAllRequestSamples();

    /*@Select("SELECT "+
            "to_char(sc.createdDate::timestamp, 'YYYY-MM-DD HH24:MI:SS') LIMSDateTimeStamp, "+
            "'01.00.00.000' limsVersionStamp,"+
            "CASE WHEN length(s.labnumber)< 20 then 'TZNACP-'||s.labnumber ellse 'TZNACP-'||substring(s.labnumber,1,3)||split_part(s.labnumber, '-', 2) end AS requestID,"+
            "'1' OBRSetID,"+
            "'' LOINCPanelCode,"+
            "'HIVVL' LIMSPanelCode,"+
            "'HIV VIRAL LOAD' LIMSPanelDesc,"+
            "'R' HL7PriorityCode,"+
            "to_char(s.dateofsamplecollection::timestamp, 'YYYY-MM-DD HH24:MI:SS') specimenDateTime,"+
            "to_char(s.createdDate::timestamp, 'YYYY-MM-DD HH24:MI:SS')  registeredDateTime,"+
            "to_char(s.datereceived::timestamp, 'YYYY-MM-DD HH24:MI:SS') receivedDateTime,"+
//            "to_char(s.testdate::timestamp, 'YYYY-MM-DD HH24:MI:SS') analysisDateTime,"+
            "(select to_char(y.createdDate::timestamp,'YYYY-MM-DD HH24:MI:SS') from hiv_sample_status_changes y where y.status='RESULTED' and y.sampleID=s.id limit 1) analysisDateTime,"+
            "to_char(sc.createdDate::timestamp, 'YYYY-MM-DD HH24:MI:SS') authorisedDateTime,"+
            "'' admitAttendDateTime ,"+
            "'0.0' collectionVolume,"+
            "	f.mohswid requestingFacilityCode,"+
            "(  SELECT f.mohswid FROM supervisory_nodes sn "+
            "   JOIN facilities f ON F.id = sn.facilityID AND typeid =1 AND sn.facilityID =s.labid LIMIT 1"+
            ") receivingFacilityCode,"+
            "f.name LIMSPointOfCareDesc,"+
            "'' RequestTypeCode,"+
            "'' ICD10ClinicalInfoCodes,"+
            "'' ClinicalInfo,"+
            "'' HL7SpecimenSourceCode, "+
            "st.code LIMSSpecimenSourceCode, "+
            "st.name LIMSSpecimenSourceDesc,"+
            "'' HL7SpecimenSiteCode,"+
            "'' LIMSSpecimenSiteCode, "+
            "'' LIMSSpecimenSiteDesc, "+
            "'0.0' WorkUnits, "+
            "'0.0' CostUnits,"+
            "'' HL7SectionCode,"+
            "'' HL7ResultStatusCode,"+
            "s.namephlebotomist RegisteredBy,"+
            "u.firstName ||' '|| u.lastName AS AuthorisedBy,"+
            "("+
            "  SELECT l.firstName||' '||l.lastName FROM users l INNER join hiv_test_worksheet_samples hws "+
            "  INNER join hiv_test_worksheets hw ON hw.id = hws.worksheetid ON hw.createdby=l.id"+
            "  where hws.sampleid=s.id) TestedBy,"+
            "s.labnumber OrderingNotes,"+
            "(select patientid FROM patients where id = s.patientautoid) EncryptedPatientID,"+
            "date_part('year',age(p.dob)) AgeInYears,"+
            "(now()::date -p.dob::date) AgeInDays,"+
            "substr(p.gender,0,2) HL7SexCode, "+
            "'' HL7EthnicGroupCode,"+
            "'false' Deceased,"+
            "'false' Newborn, "+
            "'' HL7PatientClassCode,"+
            "s.nameofpersonrequestingtest AttendingDoctor,"+
            " ("+
            "    SELECT f.mohswid FROM supervisory_nodes sn"+
            "   JOIN facilities f ON F.id = sn.facilityID AND typeid =1 AND sn.facilityID = s.labid LIMIT 1"+
            ") TestingFacilityCode,"+
            "s.labnumber ReferringRequestID,"+
            "'' Therapy, "+
            "''  LIMSAnalyzerCode, "+
            "'0' TargetTimeDays,"+
            "'0' TargetTimeMins, "+
            "'' LIMSRejectionCode, "+
            "'' LIMSRejectionDesc,"+
            "F.mohswid LIMSFacilityCode, "+
            "0 Repeated "+
            "FROM hiv_sample_status_changes sc "+
            "INNER JOIN hiv_samples s ON sc.sampleID= s.id "+
            "INNER JOIN facilities F ON F.ID = s.facilityID "+
            "INNER JOIN hiv_test_types tt on s.testtypeid=tt.id "+
            "INNER JOIN sample_types st ON s.sampletypeid = st.id  "+
            "INNER JOIN patients p ON p.id = s.patientautoid "+
            "INNER JOIN users u ON sc.createdby = u.id "+
            "WHERE sc.status='APPROVED' AND s.issenttonationalrep=false ORDER BY sc.id DESC LIMIT 30")*/

    @Select("SELECT\n" +
            "  to_char(sc.createdDate::timestamp, 'YYYY-MM-DD HH24:MI:SS.MS') LIMSDateTimeStamp,\n" +
            "  '01.00.00.000' limsVersionStamp,\n" +
            "  CASE WHEN length(s.labnumber)< 20 then 'TZNACP-'||s.labnumber else 'TZNACP-'||substring(s.labnumber,1,3)||split_part(s.labnumber, '-', 2) end AS requestID,\n" +
            "  '1' OBRSetID,\n" +
            "  '' LOINCPanelCode,\n" +
            "  'HIVVL' LIMSPanelCode,\n" +
            "  'HIV VIRAL LOAD' LIMSPanelDesc,\n" +
            "  'R' HL7PriorityCode,\n" +
            "  to_char(s.dateofsamplecollection::timestamp, 'YYYY-MM-DD HH24:MI:SS.MS') specimenDateTime,\n" +
            "  to_char(s.createdDate::timestamp, 'YYYY-MM-DD HH24:MI:SS.MS')  registeredDateTime,\n" +
            "  to_char(s.datereceived::timestamp, 'YYYY-MM-DD HH24:MI:SS.MS') receivedDateTime,\n" +
            "  (select to_char(r.resultdate::timestamp, 'YYYY-MM-DD HH24:MI:SS.MS') from hiv_results r where r.sampleid=s.id order by r.id limit 1) analysisDateTime,\n" +
            "  (select to_char(r.createddate::timestamp,'YYYY-MM-DD HH24:MI:SS.MS') from hiv_results r where r.sampleid=s.id order by r.id limit 1) analysisDateTimeCrtd,\n" +
            "  to_char(sc.createdDate::timestamp, 'YYYY-MM-DD HH24:MI:SS.MS') authorisedDateTime,\n" +
            "  '0.0' collectionVolume,\n" +
            "  f.mohswid requestingFacilityCode,\n" +
            "  (  SELECT f.mohswid FROM supervisory_nodes sn\n" +
            "    JOIN facilities f ON F.id = sn.facilityID AND typeid =1 AND sn.facilityID =s.labid LIMIT 1\n" +
            "  ) receivingFacilityCode,\n" +
            "  f.name LIMSPointOfCareDesc,\n" +
            "  '' RequestTypeCode,\n" +
            "  '' ICD10ClinicalInfoCodes,\n" +
            "  '' ClinicalInfo,\n" +
            "  '' HL7SpecimenSourceCode,\n" +
            "  st.code LIMSSpecimenSourceCode,\n" +
            "  st.name LIMSSpecimenSourceDesc,\n" +
            "  '' HL7SpecimenSiteCode,\n" +
            "  '' LIMSSpecimenSiteCode,\n" +
            "  '' LIMSSpecimenSiteDesc,\n" +
            "  '0.0' WorkUnits,\n" +
            "  '0.0' CostUnits,\n" +
            "  '' HL7SectionCode,\n" +
            "  '' HL7ResultStatusCode,\n" +
            "  (select ux.firstName ||' '|| ux.lastName from users ux inner join hiv_samples sx on sx.createdby=ux.id where sx.id=sc.sampleid) as RegisteredBy,\n" +
          // "--   (select * from users ux inner join hiv_samples sx on ux.id=sx.createdby where sx.id=s.id limit 1) RegisteredBy,\n" +
            "  u.firstName ||' '|| u.lastName AS AuthorisedBy,\n" +
           // "--  (SELECT l.firstName||' '||l.lastName FROM users l INNER join hiv_test_worksheet_samples hws INNER join hiv_test_worksheets hw ON hw.id = hws.worksheetid ON hw.createdby=l.id where hws.sampleid=s.id) TestedBy,\n" +
            "  r.testedby,\n" +
            "  s.labnumber OrderingNotes,\n" +
            "  (select patientid FROM patients where id = s.patientautoid) EncryptedPatientID,\n" +
            "  date_part('year',age(p.dob)) AgeInYears,\n" +
            "  (now()::date -p.dob::date) AgeInDays,\n" +
            "  substr(p.gender,0,2) HL7SexCode,\n" +
            "  '' HL7EthnicGroupCode,\n" +
            "  'false' Deceased,\n" +
            "  'false' Newborn,\n" +
            "  '' HL7PatientClassCode,\n" +
            "  s.nameofpersonrequestingtest AttendingDoctor,\n" +
            "  (\n" +
            "    SELECT f.mohswid FROM facilities f  where f.typeid =1 AND f.id= s.labid LIMIT 1\n" +
            "  ) TestingFacilityCode,\n" +
            "  s.labnumber ReferringRequestID,\n" +
            "  '' Therapy,\n" +
            "  ''  LIMSAnalyzerCode,\n" +
            "  '0' TargetTimeDays,\n" +
            "  '0' TargetTimeMins,\n" +
            "  '' LIMSRejectionCode,\n" +
            "  '' LIMSRejectionDesc,\n" +
            "  F.mohswid LIMSFacilityCode,\n" +
            "  0 Repeated\n" +
            "FROM hiv_sample_status_changes sc\n" +
            "  INNER JOIN hiv_samples s ON sc.sampleID= s.id\n" +
            "  INNER JOIN facilities F ON F.ID = s.facilityID\n" +
            "  INNER JOIN hiv_test_types tt on s.testtypeid=tt.id\n" +
            "  INNER JOIN sample_types st ON s.sampletypeid = st.id\n" +
            "  INNER JOIN patients p ON p.id = s.patientautoid\n" +
            "  INNER JOIN users u ON sc.createdby = u.id\n" +
            "  LEFT JOIN hiv_results r on r.sampleid=s.id\n" +
            "WHERE sc.status='APPROVED' AND s.issenttonationalrep=false ORDER BY sc.id DESC LIMIT 30")
    List<NationalAPI> loadUnsynchronizedSamples();

    @Select("SELECT "+
            "r.id as resultId,"+
            "to_char(r.createdDate::timestamp, " +
            "'YYYY/MM/DD HH24:MI:SS') limsdatetimestamp,"+
            "CASE WHEN length(s.labnumber)< 20 then  'TZNACP-'||s.labnumber else 'TZNACP-'||substring(s.labnumber,1,3)||split_part(s.labnumber, '-', 2) end as RequestID,"+
            "'1' obrsetid, "+
            "'1' OBXSetID,"+
            "'0' OBXSubID,"+
            "'0.01' SIValue,"+
            "r.unit SIUnits,"+
            "'0.0' SILoRange,"+
            "'0.0' SIHiRange,"+
            "'' HL7AbnormalFlagCodes,"+
            "'' CodedValue,"+
            "'0' ResultSemiquantitive,"+
            "'false' Note,"+
            "HIVVM LIMSObservationCode,"+
            "'HIV-VIRAL Load' LIMSObservationDesc,"+
            "r.result LIMSRptResult,"+
            "r.unit LIMSRptUnits,"+
            "'' LIMSRptFlag,"+
            "'' LIMSRptRange,"+
            "'' LIMSCodedValue,"+
            "'0.0' WorkUnits,"+
            "'0.0' CostUnits "+
            "FROM hiv_sample_status_changes c "+
            "INNER JOIN hiv_samples s ON c.sampleID= s.id "+
            "INNER JOIN hiv_results r ON s.id=r.sampleid "+
            "WHERE c.status='APPROVED' AND r.isResultSent = false LIMIT 20")
    List<NationalDashboardResult> loadUnsynchronizedResults();

    @Update("update hiv_samples set issenttonationalrep=true where labnumber=#{labnumber}")
    void updateSentDashboardNotification(@Param("labnumber") String orderingNotes);

    @Select("   SELECT \n" +
            "     \n" +
            "   to_char(NOW()::timestamp, 'YYYY/MM/DD HH24:MI:SS') DateTimeStamp "+
            "\n" +
            "\n" +
            "    ,'1.0.0' versionstamp, to_char(r.createdDate::timestamp, 'YYYY/MM/DD HH24:MI:SS') limsdatetimestamp,'01.00.00.000' limsversionstamp,\n" +
            "     CASE WHEN length(labnumber)< 20 then  'TZNACP-'||labnumber else 'TZNACP-'||substring(labnumber,1,3)||split_part(labnumber, '-', 2) end as RequestID,\n" +
            "    '1' obrsetid, '1' OBXSetID,'0' OBXSubID,'' LOINCCode,'' HL7ResultTypeCode,\n" +
            "     '0.01' SIValue ,unit SIUnits,'0.0' SILoRange,'0.0' SIHiRange,'' HL7AbnormalFlagCodes,\n" +
            "    '1900-01-01 00:00:00.0' DateTimeValue,'' CodedValue,'0' ResultSemiquantitive,\n" +
            "    'false' Note,r.id LIMSObservationCode,'SG' LIMSObservationDesc,result LIMSRptResult,unit LIMSRptUnits,\n" +
            "'' LIMSRptFlag,'' LIMSRptRange,'' LIMSCodedValue,'3.0' WorkUnits,'0.0' CostUnits\n" +
            "   \n" +
            "from HIV_SAMPLE_STATUS_CHANGES C\n" +
            "   INNER JOIN hiv_samples s on C.sampleID= s.id\n" +
            "   INNER JOIN hiv_results r on s.id=r.sampleid\n" +
            "   INNER JOIN FACILITIES F ON F.ID = S.facilityID\n" +
            "   WHERE C.STATUS='APPROVED' and isResultSent = false ")
    List<NationalDashboardResult> sendResultToNational();

    @Update("update hiv_results set isResultSent= true where id = #{id}::int")
    void updateSentResultData(@Param("id") Long id);

    @Insert(" INSERT INTO nacp_requests(\n" +
            "           datetimestamp, versionstamp, limsdatetimestamp, limsversionstamp, \n" +
            "            requestid, obrsetid, loincpanelcode, limspanelcode, limspaneldesc, \n" +
            "            hl7prioritycode, specimendatetime, registereddatetime, analysisdatetime, \n" +
            "            authoriseddatetime, collectionvolume, requestingfacilitycode, \n" +
            "            receivingfacilitycode, limspointofcaredesc, requesttypecode, \n" +
            "            icd10clinicalinfocodes, clinicalinfo, hl7specimensourcecode, \n" +
            "            limsspecimensourcecode, limsspecimensourcedesc, hl7specimensitecode, \n" +
            "            limsspecimensitecode, limsspecimensitedesc, workunits, costunits, \n" +
            "            hl7sectioncode, hl7resultstatuscode, registeredby, testedby, \n" +
            "            authorisedby, orderingnotes, encryptedpatientid, ageinyears, \n" +
            "            ageindays, hl7sexcode, hl7ethnicgroupcode, deceased, newborn, \n" +
            "            hl7patientclasscode, attendingdoctor, testingfacilitycode, referringrequestid, \n" +
            "            therapy, limsanalyzercode, targettimedays, targettimemins, limsrejectioncode, \n" +
            "            limsrejectiondesc, limsfacilitycode, repeated,receivedDateTime,admitAttendDateTime)\n" +
            "    VALUES ( #{dateTimeStamp}, #{versionStamp}, #{lIMSDateTimeStamp}, #{lIMSVersionStamp}, \n" +
            "            #{requestID}, #{oBRSetID}, #{lOINCPanelCode}, #{lIMSPanelCode}, #{lIMSPanelDesc}, \n" +
            "            #{HL7PriorityCode}, #{specimenDateTime}, #{registeredDateTime},#{analysisDateTime}, \n" +
            "            #{authorisedDateTime},#{collectionVolume}, #{requestingFacilityCode}, \n" +
            "            #{receivingFacilityCode}, #{lIMSPointOfCareDesc}, #{requestTypeCode}, \n" +
            "            #{iCD10ClinicalInfoCodes}, #{clinicalInfo}, #{hL7SpecimenSourceCode}, \n" +
            "            #{lIMSSpecimenSourceCode}, #{lIMSSpecimenSourceDesc}, #{hL7SpecimenSiteCode}, \n" +
            "            #{lIMSSpecimenSiteCode}, #{lIMSSpecimenSiteDesc}, #{workUnits}, #{costUnits}, \n" +
            "            #{HL7SectionCode}, #{hL7ResultStatusCode}, #{registeredBy}, #{testedBy}, \n" +
            "            #{authorisedBy}, #{orderingNotes}, #{encryptedPatientID}, #{ageInYears}::int,#{ageInDays}::int, \n" +
            "            #{hL7SexCode}, #{hL7EthnicGroupCode}, #{deceased}, #{newborn}, #{hL7PatientClassCode}, \n" +
            "            #{attendingDoctor}, #{testingFacilityCode}, #{referringRequestID}, #{therapy}, \n" +
            "            #{lIMSAnalyzerCode},#{targetTimeDays}, #{targetTimeMins}, #{lIMSRejectionCode}, #{lIMSRejectionDesc}, \n" +
            "            #{lIMSFacilityCode}, #{repeated}, #{receivedDateTime},#{admitAttendDateTime});\n  ")
    void insertRequest(NationalAPI result);


    @Insert("INSERT INTO nacp_results(\n" +
            "             datetimestamp, versionstamp, limsdatetimestamp, limsversionstamp, \n" +
            "            requestid, obrsetid, obxsetid, obxsubid, loinccode, hl7resulttypecode, \n" +
            "            sivalue, siunits, silorange, sihirange, hl7abnormalflagcodes, \n" +
            "            codedvalue, resultsemiquantitive, note, limsobservationcode, \n" +
            "            limsobservationdesc, limsrptresult, limsrptunits, limsrptflag, \n" +
            "            limsrptrange, limscodedvalue, workunits, costunits)\n" +
            "    VALUES ( #{dateTimeStamp}, #{versionStamp}, #{lIMSDateTimeStamp}, #{lIMSVersionStamp}, \n" +
            "            #{requestID}, #{oBRSetID}, #{oBXSetID}, #{oBXSubID}, #{lOINCCode}, #{hL7ResultTypeCode}, \n" +
            "            #{sIValue}, #{sIUnits}, #{sILoRange}, #{sIHiRange}, #{hL7AbnormalFlagCodes}, \n" +
            "             #{codedValue}, #{resultSemiquantitive}, #{note}, \n" +
            "            #{lIMSObservationCode}, #{lIMSObservationDesc}, #{lIMSRptResult}, #{lIMSRptUnits}, \n" +
            "            #{lIMSRptFlag}, #{lIMSRptRange}, #{lIMSCodedValue}, #{workUnits},#{costUnits});\n")
    void insertResults(NACPResultAPI result);


    @Select("select count(*) from nacp_results ")
    Long loadSampleResultsCount();

    @Select("select count(*) from nacp_requests ")
    Long getExistRequestRecord();

    @Select(" select * from nacp_requests where requestid = #{requestID} ")
    NationalAPI getExistingRequestId(@Param("requestID") String requestId);

    @Select(" select * from nacp_results where requestid = #{requestID} ")
    NACPResultAPI getExistingResutlId(@Param("requestID") String requestId);

    @Select("select requestID from nacp_requests where issaved=false ")
    List<NationalAPI> getSavedSamples();

    @Select("SELECT requestID FROM nacp_requests WHERE issaved=false OFFSET #{offset} limit #{fetchLimit}")
    List<NationalAPI> loadIncompleteSamples(@Param("offset") int offset, @Param("fetchLimit") int fetchLimit);

    @Select("SELECT count(id) FROM nacp_requests WHERE issaved=false")
    int loadIncompleteSampleCount();

    @Update(" update nacp_requests set issaved=true where requestID = #{requestId}")
    void updateRequests(@Param("requestId") String requestId);

    @Insert(" INSERT INTO hfr_facilities(\n" +
            "             id,mohswid, facilityname, commonname, zone, region, district, \n" +
            "            council, ward, villagestreet, facilitytype, operatingstatus, \n" +
            "            ownership, registrationnumber, latitude, longitude, location)\n" +
            "    VALUES (#{id}, #{mohswID}, #{facilityName}, #{commonName}, #{zone}, #{region}, #{district}, \n" +
            "            #{council}, #{ward}, #{villageStreet}, #{facilityType}, #{operatingStatus}, \n" +
            "            #{ownership}, #{registrationNumber}, #{latitude}, #{longitude}, #{location});\n ")
    @Options(useGeneratedKeys = true)
    void insertHFR(HFRFacilityDTO dto);

    @Select(" Select * from hfr_facilities where id=#{id} ")
    HFRFacilityDTO getExistingHfr(@Param("id") String id);

}
