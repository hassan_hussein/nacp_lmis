package com.openldr.vl.service;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.HIVTestType;
import com.openldr.core.domain.Pagination;
import com.openldr.core.service.HIVTestTypeService;
import com.openldr.vl.domain.HIVEquipmentTestSet;
import com.openldr.vl.domain.HIVTestWorkSheet;
import com.openldr.vl.domain.Sample;
import com.openldr.vl.domain.SampleDTO;
import com.openldr.vl.repository.HIVEquipmentTestSetRepository;
import com.openldr.vl.repository.SampleRepository;
import com.openldr.vl.repository.mapper.HIVTestWorkSheetMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@NoArgsConstructor
public class HIVTestWorkSheetService {

    public static final String receivedStatus="RECEIVED";
    public static final String acceptedStatus="ACCEPTED";
    public static final String INPROGRESS_STATUS="INPROGRESS";

    @Autowired
    private SampleRepository sampleRepository;

    @Autowired
    private HIVEquipmentTestSetRepository testSetRepository;

    @Autowired
    private HIVTestWorkSheetMapper workSheetMapper;

    @Autowired
    HIVTestTypeService testTypeService;


    public HIVTestWorkSheet prepare(Long userId,Long equipmentId){
        Facility facility=sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType=testTypeService.getFacilityDefault(userId);

        HIVTestWorkSheet workSheet=new HIVTestWorkSheet();
        workSheet.setLabId(facility.getId());
        workSheet.setEquipmentId(equipmentId);
        workSheet.setTestTypeId(testType.getId());

        HIVEquipmentTestSet set=testSetRepository.getByEquipmentAndType(equipmentId,testType.getId());
        workSheet.setSamples(getSamplesForWorkSheet(testType.getId(),facility.getId(),set.getNumberOfSamples()));
        workSheet.setControls(set.getEquipmentTestControls());

        return workSheet;

    }


    public HIVTestWorkSheet create(Long userId,HIVTestWorkSheet workSheet){
        Facility facility=sampleRepository.getHomeFacilityId(userId);
        if(null == workSheet.getLabId() && null != facility)
            workSheet.setLabId(facility.getId());
        workSheet.setCreatedBy(userId);
        workSheet.setModifiedBy(userId);
        workSheet.setWorkSheetNumber(getWorkSheetNumber());
        workSheetMapper.insert(workSheet);
        insertWorkSheetSamples(workSheet);

        return workSheet;

    }


    private void insertWorkSheetSamples(HIVTestWorkSheet workSheet) {
        for(Sample sample:workSheet.getSamples()){
            workSheetMapper.insetWorkSheetSamples(workSheet.getId(),sample.getId());
            sampleRepository.updateSampleStatus(INPROGRESS_STATUS,sample.getId());
        }
    }

    private List<SampleDTO> getSamplesForWorkSheet(Long testTypeId,Long labId,int total) {

        return sampleRepository.getSampleForWorkSheet(testTypeId,labId,acceptedStatus,total);
    }

    private String getWorkSheetNumber() {

        Date date = new Date();

        Long time = date.getTime();
        String timeString = time.toString();
        String workSheetNumber = timeString.substring(timeString.length() - 6);
        return workSheetNumber;
    }

    public List<HIVTestWorkSheet> getWorkSheet(Pagination page) {
        return workSheetMapper.getWorkSheet(page);
    }

    public HIVTestWorkSheet getById(Long workSheetId) {
        return workSheetMapper.getById(workSheetId);
    }
}
