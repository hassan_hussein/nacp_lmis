package com.openldr.vl.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Created by dev2 on 2/1/2018.
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class OpenLDRSampleResponse extends OpenLDRResponse{
    List<NationalAPI> data;
}
