package com.openldr.vl.repository;

import com.openldr.core.domain.Pagination;
import com.openldr.vl.domain.Sample;
import com.openldr.vl.dto.*;
import com.openldr.vl.repository.mapper.NationalDashboardApiMapper;
import com.openldr.vl.repository.mapper.SampleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hassan on 8/9/17.
 */
@Repository
public class NationalDashboardApiRepository {

    @Autowired
    private NationalDashboardApiMapper mapper;
    @Autowired
    private SampleMapper sampleMapper;

    public List<NationalAPI>getAllRequestSamples(){
        return mapper.getAllRequestSamples();
    }

    public List<NationalAPI>getUnsynchronizedSamples(){
        return mapper.loadUnsynchronizedSamples();
    }

    public List<NationalDashboardResult>getUnsychronizedResults(){
        return mapper.loadUnsynchronizedResults();
    }

    public List<NationalDashboardResult>sendResultToNational(){
        return mapper.sendResultToNational();
    }

    public void updateSentData(String orderingNotes) {
        mapper.updateSentDashboardNotification(orderingNotes);
    }
    public void updateSentResultData(Long id) {
        mapper.updateSentResultData(id);
    }

    public void insertRequest(NationalAPI result) {


        mapper.insertRequest(result);
    }

    public void insertResult(NACPResultAPI result) {

        mapper.insertResults(result);
    }


    public Long getSampleResultsCount() {
        return mapper.loadSampleResultsCount();
    }
    public Long getExistingRequestRecord() {
        return mapper.getExistRequestRecord();
    }

    public NationalAPI getExistingRequestId(String requestID) {
        return mapper.getExistingRequestId(requestID);
    }

    public NACPResultAPI getExistingResults(String requestID) {
        return mapper.getExistingResutlId(requestID);
    }

    public List<NationalAPI> getSaveSamples() {
        return mapper.getSavedSamples();
    }

    public List<NationalAPI> getIncompleteSamples(int offset, int limit){
        return mapper.loadIncompleteSamples(offset,limit);
    }

    public int getIncompleteSampleCount(){
        return mapper.loadIncompleteSampleCount();
    }

    public void  updateRequestById(String requestId){
        mapper.updateRequests(requestId);
    }

    public void  insertHFR(HFRFacilityDTO dto){
        mapper.insertHFR(dto);
    }

    public HFRFacilityDTO getExistingHfr(String id) {
        return mapper.getExistingHfr(id);
    }


    public List<NationalDashboardResult> loadProgramData(String labnumber,String requestId) {
        Sample sample = sampleMapper.getByLabnumber(labnumber);
        List<NationalDashboardResult> results = new ArrayList<>();
        Integer obxId = 2;
        if(sample.isPatientIsPregnant()){
            results.add(createResultObj(requestId,obxId,sample.getCreatedDate(),"","ARVPP","Is client pregnant","Yes"));
            obxId=obxId+1;
        }else{
            results.add(createResultObj(requestId,obxId,sample.getCreatedDate(),"","ARVPP","Is client pregnant","No"));
            obxId=obxId+1;
        }

        if(sample.isPatientIsBreastFeeding()){
            results.add(createResultObj(requestId,obxId,sample.getCreatedDate(),"","ARVBF","Is client breastfeeding","Yes"));
            obxId=obxId+1;
        }else{
            results.add(createResultObj(requestId,obxId,sample.getCreatedDate(),"","ARVBF","Is client breastfeeding","No"));
            obxId=obxId+1;
        }
        if(sample.getDrugAdherence()!=null) {
            results.add(createResultObj(requestId, obxId, sample.getCreatedDate(), sample.getDrugAdherence().getCode(), "ARVDA", "Drug Adherance", sample.getDrugAdherence().getName() ));
            obxId=obxId+1;
        }
        if(sample.isPatientHasTb()){
            results.add(createResultObj(requestId, obxId, sample.getCreatedDate(), "", "ARVTB", "Does the client have TB", "Yes"));
            obxId=obxId+1;
        }else {
            results.add(createResultObj(requestId, obxId, sample.getCreatedDate(), "", "ARVTB", "Does the client have TB", "No"));
            obxId = obxId + 1;
        }
        if(sample.isPatientUnderTbTreatment()){
            results.add(createResultObj(requestId, obxId, sample.getCreatedDate(), "", "ARTBT", "If Yes, on TB treatment?", "Yes"));
            obxId=obxId+1;
        }else {
            results.add(createResultObj(requestId, obxId, sample.getCreatedDate(), "", "ARTBT", "If Yes, on TB treatment?", "No"));
            obxId = obxId + 1;
        }
        if(sample.getTestingReason()!=null){
            results.add(createResultObj(requestId, obxId, sample.getCreatedDate(), sample.getTestingReason().getCode(), "HIVRQ", "Reasons for Testing", sample.getTestingReason().getName()));
            obxId = obxId + 1;
        }

        if(sample.getReasonForRepeatTest()!=null){
            results.add(createResultObj(requestId, obxId, sample.getCreatedDate(), "", "ARRTR", "If Repeat Test? Give Reason", sample.getReasonForRepeatTest()));
            obxId = obxId + 1;
        }
        return results;
    }

    private NationalDashboardResult createResultObj(String requestId, Integer obxSetId, Date created,String codedValue,
                                                    String obxCode,String obxDesc,String limsResult){
        NationalDashboardResult result = new NationalDashboardResult();
        result.setRequestID(requestId);
        result.setLIMSDateTimeStamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(created));
        result.setOBRSetID("1");
        result.setOBXSetID(obxSetId.toString());
        result.setOBXSubID("0");
        result.setSIValue("0.0");
        result.setSIUnits("");
        result.setSILoRange("");
        result.setSIHiRange("");
        result.setHL7AbnormalFlagCodes("");
        result.setLIMSCodedValue(codedValue);
        result.setResultSemiquantitive("0");
        result.setNote("true");
        result.setLIMSObservationCode(obxCode);
        result.setLIMSObservationDesc(obxDesc);
        result.setLIMSRptResult(limsResult);
//        result.setLIMSRptUnits("");
//        result.setLIMSRptFlag("");
//        result.setLIMSRptRange("");
//        result.


        return result;
    }
}
