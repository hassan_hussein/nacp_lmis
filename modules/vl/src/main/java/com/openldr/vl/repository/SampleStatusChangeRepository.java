package com.openldr.vl.repository;

import com.openldr.vl.domain.SampleStatusChange;
import com.openldr.vl.repository.mapper.SampleStatusChangeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by hassan on 4/3/17.
 */

@Component
public class SampleStatusChangeRepository {
    @Autowired
    private SampleStatusChangeMapper mapper;

    public void insert(SampleStatusChange sampleStatusChange){
        mapper.insert(sampleStatusChange);
    }

    public void updateStatus(SampleStatusChange change) {
        mapper.update(change);
    }
}
