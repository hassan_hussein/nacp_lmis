package com.openldr.vl.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by hassan on 8/24/17.
 */
@Data
public class NationalResultDTO {
    @Getter
    @Setter
    private int page;

    @Getter
    private int pages;

    @Getter
    @Setter
    private int total;

    @Getter
    @Setter
    private int limit;

    List<NACPResultAPI> data;
}
