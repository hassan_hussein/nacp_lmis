package com.openldr.vl.service;

import com.google.gson.Gson;
import com.openldr.core.utils.HashUtil;
import com.openldr.vl.domain.SRSResponse;
import com.openldr.vl.dto.NotificationDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hassan on 8/12/17.
 */
@Service
//@Async
@Slf4j
public class NotificationService {
    @Autowired
    private SampleService sampleService;
    @Value("${hub.prepost.url.result}")
    private String urlToPost;
    @Value("${hub.prepost.url}")
    private String urlForResponse;
    private CloseableHttpClient httpClient = OpenLDRService.getHttpClient("197.149.178.42", 80);

    public String encode(String stringToBeEncoded) {
        String end = "";
        try {
            end = URLEncoder.encode(stringToBeEncoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return end;
    }


    @Async
    public void sendNotification(String orderId, String status, String reason) {

        String method2 = "POST";
        String privateKey = "T3B2US1sy6WEHjSept4Fc34er45";
        String publicKey = "NACPLAB001563489034";

        int i = (int) (new Date().getTime() / 1000);

        JSONObject srsPayload = new JSONObject();
        srsPayload.put("orderID", orderId);

        if (status.equalsIgnoreCase("2")) {
            srsPayload.put("dateReceivedLab", i);
        } else {
            srsPayload.put("receivedLabAt", i);
            srsPayload.put("labComment", reason);
        }
        srsPayload.put("orderStatus", status);

        try {

            String stringForPostHash = method2 + privateKey + srsPayload.toString() + publicKey;
            String checksum = HashUtil.makeSHA256Hash(stringForPostHash);

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("ubu_attr", srsPayload.toString()));
            params.add(new BasicNameValuePair("key", publicKey));
            params.add(new BasicNameValuePair("checksum", checksum));

            HttpPost httpPost;

            httpPost = new HttpPost(urlForResponse);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPost.addHeader("charset", "utf-8");

            httpPost.setEntity(new UrlEncodedFormEntity(params));

            CloseableHttpResponse httpResp = httpClient.execute(httpPost);

            if (httpResp.getStatusLine().getStatusCode() == org.apache.http.HttpStatus.SC_OK) {
                log.info("Sent notification for {}|Response {}",
                        orderId, EntityUtils.toString(httpResp.getEntity()));
            } else {
                log.info("Failed to send notification for {}|Response {}",
                        orderId, EntityUtils.toString(httpResp.getEntity()));
            }
        } catch (IOException | NoSuchAlgorithmException e) {
            log.debug(" Failed sending update notification", e);
        }
    }


    public String sendResults(Long sampleId) {

        String _result = "301";
        NotificationDto s = sampleService.getNotificationById(sampleId);
        if (StringUtils.isEmpty(s.getOrderId())) {
            return _result;
        }

        String method2 = "POST";
        String privateKey = "T3B2US1sy6WEHjSept4Fc34er45";
        String publicKey = "NACPLAB001563489034";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        JSONObject srsPayload = new JSONObject();
        srsPayload.put("labno", s.getLabNumber());
        srsPayload.put("trackingID", s.getOrderId());
        srsPayload.put("testName", s.getTestTypeId() == 1L ? "HIV Viral Load" : "Early Infant Diagnosis");
        srsPayload.put("testCode", s.getTestTypeId() == 1L ? "HIVVL" : "EID");
        srsPayload.put("testDescription", s.getTestTypeId() == 1L ? "Viral Load testing" : "EID DBS Testing");
        srsPayload.put("testResult", s.getResult());
        srsPayload.put("location", s.getLabName());
        srsPayload.put("specimenDateTime", sdf.format(s.getSampleCollectionDate()));
        srsPayload.put("registeredDateTime", sdf.format(s.getRegistrationDate()));
        srsPayload.put("analysisDateTime", sdf.format(s.getResultDate()));
        srsPayload.put("authorisedDateTime", sdf.format(s.getAuthorisedDate()));
        srsPayload.put("registeredBy", s.getRegisteredBy());
        srsPayload.put("authorisedBy", s.getAuthorisedBy()); // who reviewed in evlims
        srsPayload.put("testedBy", s.getTestedBy()); // read from testing machine
        srsPayload.put("repeated", "0");
        srsPayload.put("timestamp", sdf.format(s.getResultedDate()));

        String ubu_attribuPost = srsPayload.toJSONString();

        String stringForPostHash = method2 + privateKey + ubu_attribuPost + publicKey;
        String checksum2 = null;
        try {
            checksum2 = HashUtil.makeSHA256Hash(stringForPostHash);

            HttpPost httpPost;

            httpPost = new HttpPost(urlToPost);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPost.addHeader("charset", "utf-8");
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("ubu_attr", ubu_attribuPost));
            params.add(new BasicNameValuePair("key", publicKey));
            params.add(new BasicNameValuePair("checksum", checksum2));

            httpPost.setEntity(new UrlEncodedFormEntity(params));

            CloseableHttpResponse httpResp = httpClient.execute(httpPost);

            Gson g = new Gson();
            if (httpResp.getStatusLine().getStatusCode() == org.apache.http.HttpStatus.SC_OK) {
                String payload =  EntityUtils.toString(httpResp.getEntity());  
                log.info("Sent results for {}|Response {}", s.getLabNumber(), payload);
                SRSResponse srsResponse = g.fromJson(payload, SRSResponse.class);
                //log.info("SRS Response {}", srsResponse);
                if (srsResponse.getStatus().equalsIgnoreCase("success")) {
                    //log.info("Resp: OK");
                    _result =  "200";
                } else {
                    //log.info("{}", srsResponse);
                    _result = "500";
                }
            } else {
                log.info("Failed to send {}|Response {}", s.getLabNumber(), EntityUtils.toString(httpResp.getEntity()));
                _result =  "500";
            }

        } catch (IOException | NoSuchAlgorithmException e) {
            log.debug("{}", e);
            _result =  "400";
        } catch (Exception e) {
            log.debug("{}", e);
            _result="500";
        }
	log.info(_result);
	return _result;
    }

}
