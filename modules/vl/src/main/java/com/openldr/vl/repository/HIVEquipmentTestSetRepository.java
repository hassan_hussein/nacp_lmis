package com.openldr.vl.repository;

import com.openldr.vl.domain.HIVEquipmentTestSet;
import com.openldr.vl.repository.mapper.HIVEquipmentTestSetMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class HIVEquipmentTestSetRepository {

    @Autowired
    private HIVEquipmentTestSetMapper mapper;

    public void insert(HIVEquipmentTestSet set){
        mapper.insert(set);
    }

    public void update(HIVEquipmentTestSet set)
    {
        mapper.update(set);
    }

    public HIVEquipmentTestSet getById(Long id)
    {
        return  mapper.getById(id);
    }

    public List<HIVEquipmentTestSet> getAll(){
        return mapper.getAll();
    }


    public HIVEquipmentTestSet getByEquipmentAndType(Long equipmentId, Long testTypeId) {
        return mapper.getByEquipmentAndType(equipmentId,testTypeId);
    }
}
