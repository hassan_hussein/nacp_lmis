package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.Regimen;
import com.openldr.vl.domain.RegimenLine;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegimenMapper {

    @Insert({"INSERT INTO regimens (regimenlineid,code, name,displayOrder) " +
            "                   Values (#{regimenLineId},#{code},#{name},#{displayOrder})"})
    @Options(useGeneratedKeys = true)
    Integer insert(Regimen regimen);

    @Insert({"UPDATE regimens set regimenlineid=#{regimenLineId},code=#{code}, name=#{name}, displayOrder=#{displayOrder} WHERE id=#{id}"})
    Integer update(Regimen regimen);

    @Select("Select * from regimens where id=#{id}")
    Regimen getById(@Param("id") Long id);

    @Select("Select * from regimens")
    List<Regimen> getAll();

    @Select("Select * from regimens where regimenlineid=#{regimenLineId}")
    List<Regimen> getByLine(@Param("regimenLineId")Long regimenLineId);

    @Select("Delete from regimens where id =#{id}")
    Long delete(@Param("id") Long id);

    @Select("select * from regimens")
    @Results(value = {
            @Result(property = "line", column = "regimenLineId",javaType = RegimenLine.class,
                    one = @One(select = "com.openldr.vl.repository.mapper.RegimenLineMapper.getById"))
    })
    List<Regimen>geAllInFull();

  @Select("select * from regimens where id = #{id}")
    @Results(value = {
            @Result(property = "line", column = "regimenLineId",javaType = RegimenLine.class,
                    one = @One(select = "com.openldr.vl.repository.mapper.RegimenLineMapper.getById"))
    })
    List<Regimen>geAllInFullById(@Param("id") Long id);

//    @Select("select * from regimens where name = #{name}")
    @Select("SELECT * FROM regimens WHERE  REPLACE(name, ' ', '') = REPLACE(#{name}, ' ', '')")
    Regimen getByName(String name);

    @Select("SELECT * FROM regimens WHERE regimenlineid = #{drugType} AND REPLACE(name,' ', '') = REPLACE(#{name}, ' ', '')")
    Regimen loadByNameAndType(@Param("name") String name, @Param("drugType")int drugType);
}
