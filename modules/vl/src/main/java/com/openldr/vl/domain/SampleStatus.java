package com.openldr.vl.domain;

/**
 * Created by hassan on 4/3/17.
 */
public enum SampleStatus {
    REQUEST,
    REGISTERED,
    PENDING,
    RECEIVED,
    REJECTED,
    ACCEPTED,
    TESTED,
    INPROGRESS,
    APPROVED,
    VERIFIED,
    DISPATCHED

}
