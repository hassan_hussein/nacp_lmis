package com.openldr.vl.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashBoardSampleTypes extends BaseModel
{
    @JsonProperty("RegisteredBy")
    private String registeredBy;
    @JsonProperty("Council")
    private String council;
    @JsonProperty("AgeInDays")
    private String ageInDays;
    @JsonProperty("TestName")
    private String testName;
    @JsonProperty("AuthorisedDateTime")
    private AuthorisedDateTime authorisedDateTime;
    @JsonProperty("TestCode")
    private String testCode;
    @JsonProperty("LIMSRejectionCode")
    private String lIMSRejectionCode;
    @JsonProperty("RegisteredDateTime")
    private RegisteredDateTime registeredDateTime;
    @JsonProperty("observationCode")
    private String ObservationCode;
    @JsonProperty("Region")
    private String region;
    @JsonProperty("RequestingFacilityCode")
    private String requestingFacilityCode;
    @JsonProperty("AuthorisedBy")
    private String aAuthorisedBy;
    @JsonProperty("LIMSDateTimeStamp")
    private LIMSDateTimeStamp lIMSDateTimeStamp;
    @JsonProperty("AgeInYears")
    private String ageInYears;
    @JsonProperty("SpecimenDateTime")
    private SpecimenDateTime specimenDateTime;
    @JsonProperty("ReceivedDateTime")
    private ReceivedDateTime receivedDateTime;
    @JsonProperty("Zone")
    private String zone;
    @JsonProperty("ObservationDescription")
    private String observationDescription;
    @JsonProperty("AnalysisDateTime")
    private AnalysisDateTime analysisDateTime;
    @JsonProperty("LIMSSpecimenSourceCode")
    private String lIMSSpecimenSourceCode;
    @JsonProperty("TestResult")
    private String testResult;
    @JsonProperty("HL7SexCode")
    private String hL7SexCode;
    @JsonProperty("LIMSRptUnits")
    private String lIMSRptUnits;
    @JsonProperty("LIMSSpecimenSourceDesc")
    private String lIMSSpecimenSourceDesc;
    @JsonProperty("Rejected")
    private String rejected;
    @JsonProperty("FacilityName")
    private String facilityName;
    @JsonProperty("FacilityCode")
    private String FacilityCode;
    @JsonProperty("Index")
    private String index;
    @JsonProperty("District")
    private String district;
    @JsonProperty("LabNumber")
    private String labNumber;
    @JsonProperty("TestedBy")
    private String testedBy;
    @JsonProperty("Repeated")
    private String Repeated;

    private String receivedMonth;

    private String year;

}

