package com.openldr.vl.service;

import com.openldr.vl.domain.RegimenGroup;
import com.openldr.vl.repository.RegimenGroupRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class RegimenGroupService {

    @Autowired
    RegimenGroupRepository repository;

    public void insert(RegimenGroup regimenGroup){
        repository.insert(regimenGroup);
    }

    public void update(RegimenGroup regimenGroup)
    {
        repository.update(regimenGroup);
    }

    public RegimenGroup getById(Long id)
    {
        return  repository.getById(id);
    }

    public List<RegimenGroup> getAll(){
        return repository.getAll();
    }

    public void save(RegimenGroup regimenGroup) {
        if(regimenGroup.getId() ==null)
            insert(regimenGroup);
        else
            update(regimenGroup);
    }
}
