package com.openldr.vl.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openldr.core.domain.Facility;
import com.openldr.core.dto.HFRDTO;
import com.openldr.core.repository.FacilityRepository;
import com.openldr.vl.dto.NACPResultAPI;
import com.openldr.vl.dto.NationalAPI;
import com.openldr.vl.dto.NationalDashboardResult;
import com.openldr.vl.dto.OpenLDRSampleResponse;
import com.openldr.vl.repository.NationalDashboardApiRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Service with cron jobs to synchronise data with NACP's OpenLDR
 * Created by Remmy Mseya on 2/1/2018.
 */
@Service
@EnableAsync
public class OpenLDRService {

    private static final String OPENLDR_URL = "http://197.149.178.42:8181/mohsw/5db03e46aadbb336697a574f578b7e41/v1/json";
    private static Logger logger = LoggerFactory.getLogger(OpenLDRService.class);
    @Autowired
    NationalDashboardApiRepository repository;
    @Autowired
    private FacilityRepository facilityRepository;

    private boolean isSyncingRequests = false;
    private boolean isFetchingResults = false;

    private int lastTraversedPage = 0;

    @Scheduled(cron = "0 0/5 * * * ?")
    public void synchronizeSamples() {
        logger.info("Fetching unsynced samples");
        List<NationalAPI> pendingSamples = repository.getUnsynchronizedSamples();
        if (!pendingSamples.isEmpty()) {
            ObjectMapper mapper = new ObjectMapper();


            CloseableHttpClient httpClient = getHttpClient("197.149.178.42", 8181);
            try {
                HttpPost httpPost;

                for (NationalAPI sample : pendingSamples) {
                    String jsonStr = mapper.writeValueAsString(sample);
                    logger.info("Syncing Sample {}\n{} ", sample.getRequestID(),jsonStr);
                    logger.debug("data {}", jsonStr);

                    httpPost = new HttpPost(OPENLDR_URL.concat("/requests"));

                    httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
                    httpPost.addHeader("charset", "utf-8");

                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("data", jsonStr));

                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                    CloseableHttpResponse httpResp = httpClient.execute(httpPost);
                    logger.debug("Response for {} is {}-{}",sample.getRequestID(),
                            httpResp.getStatusLine().getStatusCode(),httpResp.getStatusLine().getReasonPhrase());

                    if (httpResp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                        logger.info("Synced sample {} | {} ", sample.getRequestID(), sample.getOrderingNotes());
                        repository.updateSentData(sample.getOrderingNotes());
                        syncProgramData(sample.getOrderingNotes(),sample.getRequestID());
                    }

                    logger.debug("Resp Body {}",EntityUtils.toString(httpResp.getEntity()));

                }
            } catch (IOException e) {
                logger.debug("Error sending Sample to openLDR", e);
            } finally {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    logger.debug("Error closing httpclient", e);
                }
            }
        } else {
            logger.info("No pending samples");
        }
    }

    @Async
    private void syncProgramData(String labnumber,String requestId){
        logger.info("Fetching program data for sample {}|{}",labnumber,requestId);
        List<NationalDashboardResult> programData = repository.loadProgramData(labnumber,requestId);
        if (!programData.isEmpty()) {
            ObjectMapper mapper = new ObjectMapper();
            CloseableHttpClient httpClient = getHttpClient("197.149.178.42", 8181);
            try {
                HttpPost httpPost;

                for (NationalDashboardResult result : programData) {
                    String jsonStr = mapper.writeValueAsString(result);
                    logger.info("Syncing  {}'s {} data ", result.getRequestID(),result.getLIMSCodedValue());

                    httpPost = new HttpPost(OPENLDR_URL.concat("/results"));

                    httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
                    httpPost.addHeader("charset", "utf-8");

                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("data", jsonStr));

                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                    CloseableHttpResponse httpResp = httpClient.execute(httpPost);

                    if (httpResp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                        logger.info("Synced program data {} | {} ", result.getRequestID(), result.getLIMSObservationCode());
                    }
                }
            } catch (IOException e) {
                logger.debug("Error sending Sample "+labnumber+" program data to openLDR", e);
            } finally {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    logger.debug("Error closing httpclient", e);
                }
            }
        } else {
            logger.info("No pending samples");
        }

    }


    @Scheduled(cron = "0 0/4 * * * ?")
    public void synchronizeResults() {
        logger.info("Fetching unsynced results");
        List<NationalDashboardResult> pendingResults = repository.getUnsychronizedResults();
        if (!pendingResults.isEmpty()) {
            ObjectMapper mapper = new ObjectMapper();


            CloseableHttpClient httpClient = getHttpClient("197.149.178.42", 8181);
            try {
                HttpPost httpPost;

                for (NationalDashboardResult result : pendingResults) {
                    String jsonStr = mapper.writeValueAsString(result);
                    logger.info("Syncing Result {} ", result.getRequestID());

                    httpPost = new HttpPost(OPENLDR_URL.concat("/results"));

                    httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
                    httpPost.addHeader("charset", "utf-8");

                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("data", jsonStr));

                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                    CloseableHttpResponse httpResp = httpClient.execute(httpPost);

                    if (httpResp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                        logger.info("Synced Result {} | {} ", result.getRequestID(), result.getLIMSObservationCode());
                        repository.updateSentResultData(result.getResultId());
                    }
                }
            } catch (IOException e) {
                logger.debug("Error sending Results to openLDR", e);
            } finally {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    logger.debug("Error closing httpclient", e);
                }
            }
        } else {
            logger.info("No pending results");
        }
    }

//    @Scheduled(cron = "0 0/3 * * * ?") // in prod, sync every day at top of hour for 15mins
    public void synchronizeOpenLDRData() {
        logger.info("Synchronizing OpenLDR requests data for Dashboard");
        // Get results count we have so far

        int limit = 1000;

        Long count = repository.getExistingRequestRecord();
//        int openLDRCount = getOpendLDRRecordCount();

//        int pages = openLDRCount/limit;
        int currentPage = count.intValue()/limit;

        if (currentPage == 0){
            currentPage = 1;
        }

        if(lastTraversedPage!=0){
            currentPage = lastTraversedPage;
        }

        if (!isSyncingRequests) {
            fetchOpendLDRData(currentPage, limit);
        }else{
            logger.info("Another process is syncing OpenLDR requests");
        }
    }

    private int getOpendLDRRecordCount(){
        CloseableHttpClient httpClient = getHttpClient("197.149.178.42", 8181);
        HttpGet httpGet = new HttpGet(OPENLDR_URL.concat("/requests/page/1/1/HIVVL"));
        CloseableHttpResponse httpResp;
        try {
            httpResp = httpClient.execute(httpGet);

            int respCode = httpResp.getStatusLine().getStatusCode();
            if (respCode == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                OpenLDRSampleResponse resultResponse =
                        mapper.readValue(EntityUtils.toString(httpResp.getEntity()), OpenLDRSampleResponse.class);

                return resultResponse.getTotal();
            }

        } catch (IOException e) {
            logger.debug("Error fetching result for {}", e);
        }

        return 0;
    }

//    @Scheduled(cron = "0 0/2 * * * ?")
    public void synchronizeOpenLDRResults() {
        int offset = 0;
        int limit = 100;
        if (!isFetchingResults) {
            logger.info("Synchronizing OpenLDR sample results for Dashboard");
            fetchOpenLDRSampleResult(offset, limit);
        } else{
            logger.info("Another process is already syncing OpenLDR result");
        }
    }

    private void fetchOpenLDRSampleResult(int offset, int limit) {
        isFetchingResults = true;
        logger.debug("Fetching result for samples from {} to {}", offset, limit);

        List<NationalAPI> incompleteSamples = repository.getIncompleteSamples(offset, limit);

        if (!incompleteSamples.isEmpty()) {
            for (NationalAPI sample : incompleteSamples) {
                fetchOpenLDRSampleResult(sample.getRequestID());
            }
        } else {
            int nextOffset = offset + limit;
            if (nextOffset < repository.getIncompleteSampleCount()) {
                fetchOpenLDRSampleResult(nextOffset, limit);
            } else {
                isFetchingResults = false;
                logger.info("Finished Results sync process!");
            }
        }
    }

    public void processResult(NACPResultAPI result) {

//        logger.info("Fetched {} sample results", results.size());

//        for (NACPResultAPI result : results) {
            NACPResultAPI existingResult = repository.getExistingResults(result.requestID);
            if (existingResult == null) {
                logger.info("Fetched result for sample: {}", result.requestID);
                repository.insertResult(result);
                repository.updateRequestById(result.requestID);
                // TO-DO send event to fetch and update sample from API
//            }
        }
    }

    private void fetchOpenLDRSampleResult(String requestId) {
        CloseableHttpClient httpClient = getHttpClient("197.149.178.42", 8181);
        HttpGet httpGet;

        httpGet = new HttpGet(OPENLDR_URL.
                concat("/results/id/").
                concat(requestId).
                concat("/1/1"));

        try {
            CloseableHttpResponse httpResp = httpClient.execute(httpGet);
            int respCode = httpResp.getStatusLine().getStatusCode();
            if (respCode == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                List<NACPResultAPI> results = mapper.readValue(EntityUtils.toString(httpResp.getEntity()), new TypeReference<List<NACPResultAPI>>() {
                });
                for (NACPResultAPI result: results) {
                    processResult(result);
                }
            } else {
                logger.info("Failed to fetch results for {}, HttpError {}\n",
                        requestId, respCode,EntityUtils.toString(httpResp.getEntity()));
            }
        } catch (IOException e) {
            logger.debug("Error fetching result for {}", requestId, e);
            isFetchingResults = false;
        }
    }

    private boolean checkIfSampleExist(NationalAPI sample){
        NationalAPI existingSample = repository.getExistingRequestId(sample.getRequestID());
        return existingSample != null;
    }

    private void fetchOpendLDRData(int start, int limit) {
        logger.info("Start page {}, with limit {}", start, limit);
        isSyncingRequests = true;
        CloseableHttpClient httpClient = getHttpClient("197.149.178.42", 8181);

        HttpGet httpGet;

        httpGet = new HttpGet(OPENLDR_URL.
                concat("/requests/page/").
                concat(start + "").
                concat("/").
                concat(limit + "").
                concat("/HIVVL"));
        try {
            CloseableHttpResponse httpResp = httpClient.execute(httpGet);
            int respCode = httpResp.getStatusLine().getStatusCode();
            if (respCode == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                OpenLDRSampleResponse resultResponse =
                        mapper.readValue(EntityUtils.toString(httpResp.getEntity()), OpenLDRSampleResponse.class);

                logger.info("Fetched {} requests", resultResponse.getLimit());
                List<NationalAPI> samples = resultResponse.getData();
                boolean firstExists=false, lastExists= false;

//                if(samples.size()>0) {
//                    firstExists = checkIfSampleExist(samples.get(0));
//                    lastExists = checkIfSampleExist(samples.get(samples.size() - 1));
//                }
//                if(!firstExists && !lastExists){
                for (NationalAPI sample : resultResponse.getData()) {
                    //NationalAPI existingSample = repository.getExistingRequestId(sample.getRequestID());
                    // if (existingSample == null) {
                    if(!checkIfSampleExist(sample)){
                        logger.info("Fetched Sample: {}", sample.getRequestID());
                        String facilityCode = sample.getLIMSFacilityCode();
                        String hfrLimsaFacilityCode = getHFRCode(facilityCode);
                        String hfrRequestingFacilityCode = getHFRCode(sample.getRequestingFacilityCode());
                        String hfrReceivingFacilityCode = getHFRCode(sample.getReceivingFacilityCode());

                        if(StringUtils.isNotEmpty(hfrLimsaFacilityCode)) {
                            sample.setLIMSFacilityCode(hfrLimsaFacilityCode);
                        }else{
                            logger.info("RequestID {} with facilityCode {} didn't have known hfr code for 'LIMSFacility",
                                    sample.getRequestID(),facilityCode);
//                            continue;
                        }

                        if(StringUtils.isNotEmpty(hfrRequestingFacilityCode)){
                            sample.setRequestingFacilityCode(hfrRequestingFacilityCode);
                        }

                        if(StringUtils.isNotEmpty(hfrReceivingFacilityCode)){
                            sample.setReceivingFacilityCode(hfrReceivingFacilityCode);
                        }

                        repository.insertRequest(sample);
                    }else{
                        logger.debug("Sample {} exists",sample.getRequestID());
                    }
                }
                if (start + 1 <= resultResponse.getPages()) {
                    lastTraversedPage = start;
                    fetchOpendLDRData(start + 1, limit);

                } else {
                    isSyncingRequests = false;
                    logger.info("Finished fetching sample requests from OpenLDR");

                }
//            }else{
//                    logger.info("Page {} assumed to be existing in db", start);
//                }
        } else {
            logger.info("Failed to fetch requests, HttpError {}\n", respCode,EntityUtils.toString(httpResp.getEntity()));
            lastTraversedPage = start;
        }
    } catch (IOException e) {
        logger.debug("Error fetching OpenLDR sample requests", e);
        // fetchOpendLDRData(start + 1, limit);
        lastTraversedPage = start;
        isSyncingRequests = false;
    }
}

    private String getHFRCode(String facilityCode) {
        // ID integer -> Morogoro, Mtwara, Iringa, Mt Meru
        // String- HFR code -> others, dont fetch XXXXX-X
        // String - DISA code -> DISA labs, fetch from HFR Facilities db AAAA
        String hfrCode = facilityCode;
        try{
            String disaCodePattern = "[a-zA-Z0-9]+";
            String hfrCodePattern = "[0-9]{6}\\-[0-9]{1}";
            String evLIMSPattern = "\\d+";
            String oldDisaCodePattern = "(DISA)[a-zA-Z0-9]+";

            if(Pattern.matches(evLIMSPattern,facilityCode)){

                Facility facility = facilityRepository.getById(Long.parseLong(facilityCode));
                logger.debug("Found facility {}",facility);
                if(facility!=null)
                    hfrCode = facility.getMohSwId();
            }else if(Pattern.matches(oldDisaCodePattern,facilityCode)){
                HFRDTO fDto = facilityRepository.getByDISAId(facilityCode.substring(4));
                logger.debug("Found facility {}",fDto);
                if(fDto!=null)
                    hfrCode = fDto.getMohSwId();
            }else if(Pattern.matches(disaCodePattern, facilityCode)){
                HFRDTO fDto = facilityRepository.getByDISAId(facilityCode);
                logger.debug("Found facility {}",fDto);
                if(fDto!=null)
                    hfrCode = fDto.getMohSwId();
            }else if(Pattern.matches(hfrCodePattern,facilityCode)) {
                hfrCode = facilityCode;
            }
        }catch(Exception e){
            logger.debug("{}",e);
        }
        return hfrCode;
    }

     static CloseableHttpClient getHttpClient(String hostname, int port) {

        PoolingHttpClientConnectionManager connectionManager
                = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(5);
        connectionManager.setDefaultMaxPerRoute(4);

        HttpHost host = new HttpHost(hostname, port);
        connectionManager.setMaxPerRoute(new HttpRoute(host), 5);

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(10 * 1000) // how long to wait to make a tcp connection
                .setSocketTimeout(30 * 1000) // how long to wait to get bytestream
                .build();


        return HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(connectionManager)
                .build();
    }


}
