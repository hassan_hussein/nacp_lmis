package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.HIVTestEquipment;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HIVTestEquipmentMapper {

    @Insert({"INSERT INTO hiv_test_equipments (code, name,displayOrder) " +
            "                   Values (#{code},#{name},#{displayOrder})"})
    @Options(useGeneratedKeys = true)
    Integer insert(HIVTestEquipment equipment);

    @Insert({"UPDATE hiv_test_equipments set code=#{code}, name=#{name}, displayOrder=#{displayOrder} WHERE id=#{id}"})
    Integer update(HIVTestEquipment equipment);

    @Select("Select * from hiv_test_equipments where id=#{id}")
    HIVTestEquipment getById(@Param("id") Long id);

    @Select("Select * from hiv_test_equipments")
    List<HIVTestEquipment> getAll();

}
