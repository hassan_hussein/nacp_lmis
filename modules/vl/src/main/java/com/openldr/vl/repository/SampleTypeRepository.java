package com.openldr.vl.repository;

import com.openldr.vl.domain.SampleType;
import com.openldr.vl.repository.mapper.SampleTypeMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class SampleTypeRepository {

    @Autowired
    private SampleTypeMapper mapper;

    public void insertSampleType(SampleType sampleType){
        mapper.insertSampleType(sampleType);
    }

    public void updateSampleType(SampleType sampleType)
    {
        mapper.updateSampleType(sampleType);
    }

    public SampleType getSampleTypeById(Long id)
    {
        return  mapper.getSampleTypeById(id);
    }

    public List<SampleType> getAllSampleTypes(){
        return mapper.getAllSampleTypes();
    }


    public SampleType getSampleTypeByName(String name) {
        return mapper.getSampleTypeByName(name);
    }

    public  SampleType loadByCode(String code){
        return  mapper.loadByCode(code);
    }
}
