package com.openldr.vl.dto;

import lombok.Data;

/**
 * Created by hassan on 9/12/17.
 */
@Data
public class LabDTO {
    private Long id;
    private String name;
}
