package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.RegimenLine;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegimenLineMapper {

    @Insert({"INSERT INTO regimen_lines (regimengroupid,code, name,displayOrder) " +
            "                   Values (#{code},#{name},#{displayOrder})"})
    @Options(useGeneratedKeys = true)
    Integer insert(RegimenLine regimenLine);

    @Insert({"UPDATE regimen_lines set regimengroupid=#{regimenGroupId},code=#{code}, name=#{name}, displayOrder=#{displayOrder} WHERE id=#{id}"})
    Integer update(RegimenLine regimenLine);

    @Select("Select * from regimen_lines where id=#{id}")
    RegimenLine getById(@Param("id") Long id);

    @Select("Select * from regimen_lines")
    List<RegimenLine> getAll();

    @Select("Select * from regimen_lines where regimengroupid=#{regimenGroupId}")
    List<RegimenLine> getByGroup(@Param("regimenGroupId") Long regimenGroupId);
}
