package com.openldr.vl.repository.mapper;

import com.openldr.vl.dto.DashBoardSampleTypes;
import com.openldr.vl.dto.DashboardDTO;
import com.openldr.vl.dto.TestByAge;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hassan on 5/6/17.
 */
@Repository
public interface DashboardMapper {

    @Insert("INSERT INTO tat_view(\n" +
            "            facility, labno, test, observationcode, observationdescription, \n" +
            "            result, specimendate, receiveddate, registereddate, resulteddate, \n" +
            "            authoriseddate)\n" +
            "    VALUES ( #{facility}, #{labNo},#{test}, #{observationCode}, #{observationDescription}, #{result}, \n" +
            "            #{specimenDate}::date, #{receivedDate}::date, #{registeredDate}::date, #{resultedDate}::date, #{authorisedDate}::date\n" +
            "            ); ")
    Integer insert(DashboardDTO dashboardDTO);

    @Insert(" INSERT INTO dashboard_sample_types(\n" +
            "             ageinyears, analysisdatetime, authoriseddatetime, council, \n" +
            "            district, facilitycode, facilityname, hl7sexcode, limsdatetimestamp, \n" +
            "            limsrejectioncode, limsrptunits, limsspecimensourcecode, limsspecimensourcedesc, \n" +
            "            labNumber, observationCode, observationDescription, receivedDateTime, \n" +
            "            region, registeredDateTime, rejected, repeated, specimenDateTime, \n" +
            "            testresult, zone, createddate,monthReceived,year)\n" +
            "    VALUES (#{ageInYears}::int, #{analysisDateTime.date}::date, #{authorisedDateTime.date}::date, #{council}, \n" +
            "            #{district}, #{facilityCode}, #{facilityName}, #{hL7SexCode}, #{lIMSDateTimeStamp.date}::date, \n" +
            "            #{lIMSRejectionCode}, #{lIMSRptUnits}, #{lIMSSpecimenSourceCode}, #{lIMSSpecimenSourceDesc}, \n" +
            "            #{labNumber}, #{observationCode}, #{observationDescription}, #{receivedDateTime.date}::DATE, \n" +
            "            #{region}, #{registeredDateTime.date}::DATE, #{rejected}, #{repeated}::int, #{specimenDateTime.date}::DATE, \n" +
            "            #{testResult}, #{zone},NOW(), to_char((#{registeredDateTime.date}::DATE)::date , 'Month'),(SELECT EXTRACT(YEAR from #{registeredDateTime.date}::DATE))  );")
    @Options(useGeneratedKeys = true)
    Integer saveDashBoardBySampleType(DashBoardSampleTypes s);

    @Select("select * from dashboard_sample_types where labNumber = #{labNumber} and ageInYears=#{ageInYears}::int ")
    DashBoardSampleTypes getDashboardSampleTypesBy(@Param("labNumber") String labNumber,@Param("ageInYears") String ageInYears);

   //@Select("SELECT * FROM dashboard_sample_types")
// where lIMSDateTimeStamp >=#{startDate}::DATE and lIMSDateTimeStamp <=#{endDate}::DATE
   @Select("select * from dashboard_sample_types")
    List<HashMap<String,Object>>getAllDasboardBySample(@Param("startDate")String startDate,@Param("endDate")String endDate);

    @Select("select * from dashboard_sample_types ")
    List<DashBoardSampleTypes>getAllDasboardBySample2();


    @Insert("INSERT INTO dashboard_test_by_ages(\n" +
            "             location, total, lessthanandequalto5, greaterthan5lessthanandequalto15, \n" +
            "            greaterthan15, year, month, createddate)\n" +
            "    VALUES (#{location}, #{total}, #{lessThanAndEqualTo5}, #{greaterThan5LessThanAndEqualTo15}, \n" +
            "            #{greaterThan15}, #{year}, #{month},NOW());")
    @Options(useGeneratedKeys = true)
    Integer insertTestByAge(TestByAge s);

    @Select(" select * from  dashboard_test_by_ages where location = #{location} and month = #{month} and year=#{year}")
    List<TestByAge> getByLocationAndYear(@Param("location") String location,@Param("month") Long month , @Param("year") Long year);

    @Select("select * from dashboard_test_by_ages wher year = #{year} ")
    List<HashMap<Object,String>>getAllByYearAndMonth(@Param("year") Long year);

}
