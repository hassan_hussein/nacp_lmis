package com.openldr.vl.service;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.HIVTestType;
import com.openldr.core.domain.Pagination;
import com.openldr.core.dto.AutomaticUploadResultDTO;
import com.openldr.core.dto.LogTagDTO;
import com.openldr.core.service.HIVTestTypeService;
import com.openldr.core.service.RocheVLResultService;
import com.openldr.vl.domain.SampleDTO;
import com.openldr.vl.domain.TestResult;
import com.openldr.vl.repository.ResultRepository;
import com.openldr.vl.repository.SampleRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class ResultService {

    public static final String RESULT_STATUS="RESULTED";
    public static final String APPROVED_STATUS="APPROVED";
    public static final String DISPATCHED_STATUS="DISPATCHED";

    @Autowired
    SampleRepository sampleRepository;

    @Autowired
    ResultRepository repository;

    @Autowired
    HIVTestTypeService testTypeService;

    @Autowired
    RocheVLResultService resultService;

    public List<SampleDTO> getResultedSamples(Long userId,Pagination pagination){
        Facility facility=sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType=testTypeService.getFacilityDefault(userId);
       return  repository.getResultedSamples(testType.getId(),facility.getId(),RESULT_STATUS,pagination);
    }

    public List<SampleDTO> getResultedListSamples(Long userId,Pagination pagination){
        Facility facility=sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType=testTypeService.getFacilityDefault(userId);
        System.out.println(testType);
       return  repository.getResultedListSamples(testType.getId(),facility.getId(),RESULT_STATUS,pagination);
    }

    public Integer getTotalResulted(Long userId, String resultStatus) {
        Facility facility=sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType=testTypeService.getFacilityDefault(userId);
        return  repository.getTotalResulted(testType.getId(),facility.getId(), resultStatus);

    }  public Integer getTotalResultedHub(Long userId, String resultStatus) {
        Facility facility=sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType=testTypeService.getFacilityDefault(userId);
        return  repository.getTotalResultedHub(testType.getId(), facility.getId(), DISPATCHED_STATUS);

    }

    public void automaticUpload(List<AutomaticUploadResultDTO> resultDTO) {
       
        for(AutomaticUploadResultDTO dto :resultDTO) resultService.saveAutoUplaod(dto);
    }

    public void automaticUpload(AutomaticUploadResultDTO resultDTO){
        resultService.saveAutoUpload(resultDTO);
    }

    public List<SampleDTO> getResultedSamplesList(Long userId,Pagination pagination){
        Facility facility=sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType=testTypeService.getFacilityDefault(userId);
        return  repository.getResultedSamplesList(testType.getId(), facility.getId(), RESULT_STATUS, pagination);
    }

    public List<SampleDTO> getDispatchedResultsForHub(Long userId,Pagination pagination){
        Facility facility=sampleRepository.getHomeFacilityId(userId);
        HIVTestType testType=testTypeService.getFacilityDefault(userId);
        return  repository.getDispatchedResultsForHub(testType.getId(), facility.getId(), RESULT_STATUS, pagination);
    }

    public void uploadLogTag(List<LogTagDTO> logTagDTOs) {
            resultService.saveLogTag(logTagDTOs);

    }

    public List<LogTagDTO> getLogTags(String startDate, String endDate, Pagination pagination) {
        return resultService.getLogTags(startDate, endDate, pagination);
    }

    public TestResult getResultBySampleId(Long sampleId) {
        return repository.getResultBySampleId(sampleId);
    }
}
