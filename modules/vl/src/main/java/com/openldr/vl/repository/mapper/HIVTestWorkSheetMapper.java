package com.openldr.vl.repository.mapper;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.HIVTestType;
import com.openldr.core.domain.User;
import com.openldr.vl.domain.HIVTestWorkSheet;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HIVTestWorkSheetMapper {

    @Insert("Insert into hiv_test_worksheets(worksheetnumber,labid, equipmentid,testtypeid,createdby, createddate, modifiedby,modifieddate) " +
            " Values(#{workSheetNumber},#{labId},#{equipmentId},#{testTypeId},#{createdBy},NOW(),#{modifiedBy},NOW())")
    @Options(useGeneratedKeys = true)
    Integer insert(HIVTestWorkSheet workSheet);


    @Insert("Insert into hiv_test_worksheet_samples(worksheetid, sampleid) " +
            " Values(#{workSheetId},#{sampleId})")
    void insetWorkSheetSamples(@Param("workSheetId")Long workSheetId, @Param("sampleId")Long sampleId);

    @Select("Select * from hiv_test_worksheets")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "samples", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.vl.repository.mapper.SampleMapper.getByWorkSheetId"))
    })
    List<HIVTestWorkSheet> getWorkSheet(RowBounds rowBounds);

    @Select("Select * from hiv_test_worksheets where id=#{workSheetId} LIMIT 1 ")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdDate", column = "createddate"),
            @Result(property = "testType", column = "testtypeid", javaType = HIVTestType.class,
                    one = @One(select = "com.openldr.core.repository.mapper.HIVTestTypeMapper.getById")
            ),
            @Result(property = "laboratory", column = "labid", javaType = Facility.class,
                    one = @One(select = "com.openldr.core.repository.mapper.FacilityMapper.getLWById")
            ),
            @Result(property = "creator",column = "createdby", javaType = User.class,
                    one = @One(select = "com.openldr.core.repository.mapper.UserMapper.getById")
            ),
            @Result(property = "samples", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.vl.repository.mapper.SampleMapper.getByWorkSheetId")
            )
    })
    HIVTestWorkSheet getById(@Param("workSheetId")Long workSheetId);
}
