package com.openldr.vl.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * Created by hassan on 5/6/17.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardDTO extends BaseModel {

@JsonProperty("Facility")
private String facility;
@JsonProperty("LabNo")
private String labNo;
@JsonProperty("Test")
private String test;
@JsonProperty("ObservationCode")
private String observationCode;
@JsonProperty("ObservationDescription")
private String observationDescription;
@JsonProperty("Result")
private String result;
@JsonProperty("SpecimenDate ")
private String specimenDate;

@JsonProperty("ReceivedDate")
private String receivedDate;
@JsonProperty("RegisteredDate")
private String registeredDate;
@JsonProperty("ResultedDate")
private String resultedDate;
@JsonProperty("AuthorisedDate")
private String authorisedDate;

   /* DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    public String setSpecimenDate(){
        return df.format(this.specimenDate);
    }*/

   /* public String setReceivedDate(){
        return df.format(this.receivedDate);
    }

    public String setRegisteredDate(){
        return df.format(this.registeredDate);
    }

    public String setResultedDate(){
        return df.format(this.resultedDate);
    }
    public String setAuthorisedDate(){
        return df.format(this.authorisedDate);
    }*/
}
