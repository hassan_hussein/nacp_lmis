package com.openldr.vl.repository;

import com.openldr.vl.domain.HIVTestEquipment;
import com.openldr.vl.repository.mapper.HIVTestEquipmentMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class HIVTestEquipmentRepository {

    @Autowired
    private HIVTestEquipmentMapper mapper;

    public void insert(HIVTestEquipment equipment){
        mapper.insert(equipment);
    }

    public void update(HIVTestEquipment equipment)
    {
        mapper.update(equipment);
    }

    public HIVTestEquipment getById(Long id)
    {
        return  mapper.getById(id);
    }

    public List<HIVTestEquipment> getAll(){
        return mapper.getAll();
    }


}
