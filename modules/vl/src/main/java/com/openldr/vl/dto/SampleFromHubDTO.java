package com.openldr.vl.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.openldr.core.domain.BaseModel;
import com.openldr.vl.domain.Sample;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * Created by hassan on 5/6/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@EqualsAndHashCode(callSuper=false)
public class SampleFromHubDTO extends BaseModel{


    //FROM HUB

    private String orderID;
    private String testCode;
    private String formType;
    private String patientCenter;
    private String testReason;
    private String sampleType;
    private String sampleRequestTime;
    private String sampleRequesterName;
    private Date dateSampleTaken;
    private String timeSampleTaken;
    private Date dateSampleSent;
    private String timeSampleSent;
    private  String phlebotomistName;
    private String phlebotomistCadre;
    private String dispatcherName;
    private String testComment;
    // private Date createdDate;
    private Date dateReceivedLab;
    private String orderStatus;
    private Date updatedAt;
    private Date createdAt;
    private String patientCode;
    private String testCenter;
    private String hubCenter;
    //private Date createdBy;
    private String results;
    private Date orderedAt;
    private Date resultReceivedAt;
    private Date registerTurnRound;
    private Date resultTurnRound;
    private String batchCode;
    private String transitStatus;
    private String onTransitAt;
    private String receivedLabAt;
    private String labComment;
    private String registeredBy;
    private String originalLab;
    private String diseaseName;
    private String hasTB;
    private String isOnTBTreatment;
    private String isTreatmentStarted;
    private String isTreatmentCompleted;
    private String treatmentStartedOn;
    private String symptomStatus;
    private String drugType;
    private String drugName;
    private String hfrID;
    private String drugAdherance;
    private String userGroup;
    private String gender;
    private  Date pcreatedDate;
    @JsonProperty("isPregnancy")
    private String isPregnant;
    private String isBreastFeeding;
    private String patientID;
    private Long pID;
    private String patientPhone;
    private String patientNursePhone;
    private String wardId;
    private String birthDate;
    private Boolean isReceivedAtLAB = false;
    private Date dateReceivedAtLab;
    private Boolean isPending= true;
    private String leader;
    private Boolean isSaved;

}
