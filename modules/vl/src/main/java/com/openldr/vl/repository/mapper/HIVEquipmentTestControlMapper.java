package com.openldr.vl.repository.mapper;

import com.openldr.vl.domain.HIVEquipmentTestControl;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HIVEquipmentTestControlMapper {

    @Insert({"INSERT INTO hiv_equipment_test_controls (testsetid, name) " +
            "                   Values (#{testSetId},#{name})"})
    @Options(useGeneratedKeys = true)
    Integer insert(HIVEquipmentTestControl control);

    @Insert({"UPDATE hiv_equipment_test_controls set testsetid=#{testSetId},name=#{name} WHERE id=#{id}"})
    Integer update(HIVEquipmentTestControl control);

    @Select("Select * from hiv_equipment_test_controls where id=#{id}")
    HIVEquipmentTestControl getById(@Param("id") Long id);

    @Select("Select * from hiv_equipment_test_controls where testsetid=#{testSetId}")
    List<HIVEquipmentTestControl> getBySet(@Param("testSetId") Long testSetId);

    @Select("Select * from hiv_equipment_test_controls")
    List<HIVEquipmentTestControl> getAll();

}
