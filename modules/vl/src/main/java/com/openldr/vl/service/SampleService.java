package com.openldr.vl.service;

import com.openldr.core.domain.Facility;
import com.openldr.core.domain.HIVTestType;
import com.openldr.core.domain.Pagination;
import com.openldr.core.domain.User;
import com.openldr.core.exception.DataException;
import com.openldr.core.repository.SMSRepository;
import com.openldr.core.repository.mapper.FacilityTypeMapper;
import com.openldr.core.service.*;
import com.openldr.core.utils.HashUtil;
import com.openldr.vl.domain.*;
import com.openldr.vl.dto.NotificationDto;
import com.openldr.vl.dto.SampleFromHubDTO;
import com.openldr.vl.dto.SampleTestDTO;
import com.openldr.vl.repository.SampleRepository;
import com.openldr.vl.repository.mapper.PatientMapper;
import lombok.NoArgsConstructor;
import lombok.Synchronized;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
@NoArgsConstructor
public class SampleService {

    public static final String receivedStatus = "RECEIVED";
    public static final String acceptedStatus = "ACCEPTED";
    public static final String rejectedStatus = "REJECTED";
    public static final String registered = "REGISTERED";
    public static final String inprogressStatus = "INPROGRESS";
    @Value("${hub.prepost.url}")
    private String urlToPost;

    private static Logger logger = LoggerFactory.getLogger(SampleService.class);

    @Autowired
    SampleRepository repository;

    @Autowired
    HIVTestTypeService testTypeService;

    @Autowired
    SampleTypeService typeService;

    @Autowired
    private UserService userService;

    @Autowired
    private SMSRepository smsRepository;

    @Autowired
    private MessageService messageService;

    @Autowired
    private ResultService resultService;

    @Autowired
    private FacilityService facilityService;

    @Autowired
    private SampleStatusChangeService statusChangeService;
    @Autowired
    private RegimenService regimenService;
    @Autowired
    private DrugAdherenceService adherenceService;

    @Autowired
    private PatientMapper patientMapper;
    @Autowired
    private GeographicZoneService zoneService;
    @Autowired
    private FacilityTypeMapper facilityTypeMapper;

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private ProgramSupportedService programSupportedService;

    @Autowired
    private ProgramService programService;

    @Autowired
    private VLTestingReasonService testingReasonService;

    public String getBatchNumber(Facility lab) {

        if (lab != null) {
            StringBuffer sb =new StringBuffer(
                    lab.getCode().substring(0, 3).toUpperCase());
            sb.append(new SimpleDateFormat("YYYYMMdd").format(new Date()));

            return sb.toString() ;
        } else
            return "no lab number registered";

    }

    public void insertSample(Sample sample, Long userId) {

        Facility homeFacility=facilityService.getHomeFacility(userId);

        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        sample.setTestTypeId(testType.getId());
        String patientID = sample.getPatient().getPatientId();

        if(patientID.contains("-")){
            patientID = patientID.replaceAll("-","");
        }
        Patient existing = repository.getByPatientId(patientID);

        if (existing != null) {
            sample.setPatientAutoId(existing.getId());
        } else {
            Patient newPatient = sample.getPatient();
            newPatient.setPatientId(patientID);

            repository.insertPatient(newPatient);

            sample.setPatientAutoId(newPatient.getId());
        }

        sample.setCreatedBy(userId);
        sample.setModifiedBy(userId);
        if (!homeFacility.getSdp()) {
            sample.setIsPending(true);
            sample.setStatus(registered);
            sample.setBatchNumber(getBatchNumber(homeFacility));
            repository.insertSample(sample);

            SampleStatusChange change = new SampleStatusChange(sample, SampleStatus.REGISTERED, userId);
            statusChangeService.insert(change);

        } else {
            sample.setStatus(receivedStatus);
            sample.setLabNumber(getLabNumber(sample));
            repository.insertSample(sample);
            SampleStatusChange change = new SampleStatusChange(sample, SampleStatus.RECEIVED, userId);
            statusChangeService.insert(change);

        }


    }
    public Sample getAllById(Long id){
     return repository.getAllById(id);
    }

    public List<SampleDTO> getSamplesForLab(Long userId, String status,String column, Pagination pagination) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        //getSampleForLab
        if (StringUtils.isEmpty(column)) {
            return repository.getSamplesByStatus(testType.getId(), facility.getId(), status, pagination);
        }else{
            return repository.getSamplesByStatusAndColumn(testType.getId(), facility.getId(), status,column, pagination);
        }
    }


    public List<SampleDTO> getSamplesForHub(Long userId, String status, Pagination pagination) {

        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getSamplesForHub(testType.getId(), facility.getId(), status, pagination);
    }

    public List<SampleDTO> getSamplesForLabByBatch(Long userId,String batchNumber, String status) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getSamplesByStatusAndBatch(testType.getId(), facility.getId(),batchNumber, status);
    }

    public List<SampleDTO> getByStatusAndBatch(Long userId,String batchNumber, String status) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getByStatusAndBatch(testType.getId(), facility.getId(), batchNumber, status);
    }
    @Synchronized
    public  void receiveSample(Long sampleId,String labNumber, String status ) {
        repository.receiveSample(sampleId,labNumber,status);

    }
    public void receiveSamplesByBatch(Long userId, String batchNumber,String status){
        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        List<SampleDTO> samples=repository.getSamplesByStatusAndBatch(testType.getId(), facility.getId(),batchNumber, status);
        for(SampleDTO sample:samples){
            sample.setPatient(patientMapper.getById(sample.getPatientAutoId()));
            String labNumber= getLabNumber(sample);
            receiveSample(sample.getId(),labNumber,receivedStatus);
            SampleStatusChange change = new SampleStatusChange(sample, SampleStatus.RECEIVED, userId);
            statusChangeService.insert(change);
        }
    }

    public void acceptSample(Long userId, Long sampleId) {
        repository.acceptSample(sampleId, acceptedStatus);
        Sample sample = getById(sampleId);
        SampleStatusChange change = new SampleStatusChange(sample, SampleStatus.ACCEPTED, userId);
        statusChangeService.insert(change);
        Sample s = repository.getById(sampleId);
        if (s.getOrderId() !=null) {
            notificationService.sendNotification(s.getOrderId(), "3", null);
        }
    }

    public Integer updateSampleStatus(String status, Long sampleId, Long userId) {
        //SEND EMAIL to Facilities
        // prepareToDispatchToUsers(id);
        return repository.updateSampleStatus(status, sampleId);

    }

    public Integer updateSampleApprovedStatus(String status, Long id, Long userId) {
        //SEND EMAIL to Facilities
        // prepareToDispatchToUsers(id);

        Sample sample = getById(id);

        SampleStatusChange change = new SampleStatusChange(sample, SampleStatus.APPROVED, userId);

        statusChangeService.insert(change);

        Integer x = repository.updateSampleStatus(status, id);

       

        return x;
    }

    public Integer updateDispatchSampleStatus(String status, Long id, Long userId) {
        //SEND EMAIL to Facilities
        //prepareToDispatchToUsers(id);
        Sample sample = getById(id);
        SampleStatusChange change = new SampleStatusChange(sample, SampleStatus.DISPATCHED, userId);
        statusChangeService.insert(change);

        return repository.updateSampleStatus(status, id);

    }

    private String getLabNumber(Facility lab) {
        String labNumber;
        String lastLabN = repository.getLastLabNumber(lab.getId());
        String[] parts = lastLabN.split("-");
        String lastLabNumber = parts[0];

        if (lastLabNumber == null)
            labNumber = "000001";
        else {

            Long lastLabNumberInt = Long.valueOf(lastLabNumber);

            Long lastLab = ++lastLabNumberInt;

            int padding = (6 - lastLabNumber.length());
            if (padding > 0) {
                if (padding <= 2)
                    padding = 4;
                labNumber = StringUtils.leftPad(String.valueOf(lastLab), padding, "0");
            } else {
                labNumber = lastLabNumberInt.toString();
            }
        }
        //  String labNumber = timeString.substring(timeString.length() - 6);
        return labNumber;
    }

    public NotificationDto getNotificationById(Long sampleId) {
        return repository.loadNotificationById(sampleId);
    }

    public SampleDTO getById(Long sampleId) {
        return repository.getById(sampleId);
    }
    public SampleDTO getByBatch(String batchNumber) {
        return repository.getByBatch(batchNumber);
    }

    public List<Facility> getFacilities() {
        return repository.getFacilities();
    }

    public List<Sample> getBackLogSamples(Long userId) {
        return null;
    }

    @Transactional
    public void rejectSample(Long userId, Long sampleId, String reason) {
        repository.rejectSample(sampleId, rejectedStatus, reason);
        Sample sample = getById(sampleId);
        SampleStatusChange change = new SampleStatusChange(sample, SampleStatus.REJECTED, userId);
        statusChangeService.insert(change);
        //prepareToSendSMS(sampleId);
        if(null != sample.getOrderId()){
           notificationService.sendNotification(sample.getOrderId(),"5",reason);
        }
    }

    public String prepareToSendSMS(Long sampleId) {

        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        SampleDTO sampleDTO = repository.getById(sampleId);

        System.out.println(sampleDTO);

        String rejectedDate = simpleDateFormat.format(sampleDTO.getRejectedDate());
        String message = " ";
        message = " Sample Test of PatientID: " + sampleDTO.getPatientId() + " Health Facility  " + sampleDTO.getFacility() + " has been Rejected On " +
                " " + rejectedDate + "  Reason : ' " + sampleDTO.getReasonForRejection() + " '";

        if (sampleDTO.getFacilityId() != null) {
            User user = userService.getByFacilityId(sampleDTO.getFacilityId());
            if (user != null) {
                if (user.getCellPhone() != null) {
                    userService.sendSMS(user.getCellPhone(), sampleDTO.getLab(), message);
                    userService.createEmailMessageForSamples(user, message, messageService.message("sample.rejected.mail.subject"));
                    smsRepository.SaveSMSMessage("1", message, user.getCellPhone(), new Date(), true);
                } else {
                    message = " No CellPhone has been registered for this User facility";
                    return message;
                }
            }

        }
        return message;

    }


    public String prepareToDispatchToUsers(Long sampleId) {
        if (sampleId != null) {

            String pattern = "dd-MM-yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            SampleDTO sampleDTO = repository.getById(sampleId);
            if (sampleDTO != null) {

                String resultDate = simpleDateFormat.format(sampleDTO.getResultDate());
                String message = " ";
                message = " Majibu ya Mgojwa mwenye ID: " + sampleDTO.getPatientId()
                        + sampleDTO.getFacility() + " yametoka tarehe :  " + resultDate;


                if (sampleDTO.getFacilityId() != null) {
                    User user = userService.getByFacilityId(sampleDTO.getFacilityId());
                    if (user != null) {
                        if (user.getCellPhone() != null) {
                            userService.sendSMS(user.getCellPhone(), sampleDTO.getLab(), message);
                            userService.createEmailMessageForSamples(user, message, messageService.message("sample.result.mail.subject"));
                            smsRepository.SaveSMSMessage("1", message, user.getCellPhone(), new Date(), true);
                        } else {
                            message = " No CellPhone has been registered for this User facility";
                            return message;
                        }
                    }

                }

                return message;
            } else
                return "Sample Id not found";
        } else {

            return "No EMAIL SENT";
        }

    }

    public List<Facility> getSupervisedLabs(Long userId) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        return repository.getSupervisedLabs(facility.getId());
    }

    public List<Facility> getFacilitySupervisors(Long userId) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        return repository.getFacilitySupervisors(facility.getId());
    }

    public Facility getHomeFacility(Long userId) {
        return facilityService.getHomeFacilityId(userId);
    }

    public Integer getTotalSamplesForHub(Long userId, String status) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getTotalSamplesForHub(testType.getId(), facility.getId(), status);
    }

    public Integer getTotalSamplesForLab(Long userId, String status) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getTotalSamplesForLab(testType.getId(), facility.getId(), status);
    }

    public void testSample(Long userId, Long sampleId) {
        repository.acceptSample(sampleId, acceptedStatus);
    }

    public List<SampleDashboardDTO> getTotalTestDoneForDashboard(Long userId) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getTotalTestDoneForDashboard(facility.getId(), testType.getId());
    }

    public List<SampleDashboardDTO> getTestBySampleForDashboard(Long userId) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getTestBySampleForDashboard(testType.getId(), facility.getId());
    }

    public List<SampleDashboardDTO> getSampleByFacilityForDashboard(Long userId) {
        Facility facility = repository.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getSampleByFacilityForDashboard(testType.getId(), facility.getId());
    }

    public List<SampleDTO> getRejectedSamplesForLab(Long userId, String status, Pagination pagination) {
        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getRejectedSamplesForLab(testType.getId(), facility.getId(), status, pagination);
    }

    public List<SampleTestDTO> getSampleTestByAge(Long userId) {

        Facility facility = facilityService.getHomeFacilityId(userId);
        HIVTestType testType = testTypeService.getFacilityDefault(userId);
        return repository.getSampleTestByAge(testType.getId(), facility.getId());

    }

    public String getGeneratedAnotherAlgorithm(Sample sample) {
         Facility lab;

        if(sample.getLabId() != null)
              lab= repository.getSampleLab(sample.getLabId());
             else
             lab = repository.getSampleHub(sample.getHubId());

        String labCode = " ";
        StringBuffer buffer = new StringBuffer();

        if (lab != null) {
            labCode = lab.getCode().substring(0, 3).toUpperCase();

            Date date = new Date();

            Calendar cal = Calendar.getInstance();

            cal.setTime(date);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH) + 1;
            int day = cal.get(Calendar.DAY_OF_MONTH);

            String compareDateChange = "";
            String patient = sample.getPatient().getPatientId();
            NumberFormat f = new DecimalFormat("00");
            String dt = String.valueOf(f.format(day));
            String todayDateValue = buffer.append(labCode).append(year).append((month < 10 ? ("0" + month) : (month)))
                    .append((day < 10) ? dt : day).append('-').append(patient).toString();
            System.out.println("My Values");
            System.out.println(todayDateValue);
            //Take the today Date for comparison
            buffer = new StringBuffer();
            return todayDateValue;
        } else
            return "no lab number registered";

    }
    
    @Synchronized
    public   String getLabNumber(Sample sample) {

        Facility lab = repository.getSampleLab(sample.getLabId());

        String labCode = " ";
        StringBuffer buffer = new StringBuffer();

        if (lab != null) {
            labCode = lab.getCode().substring(0, 3).toUpperCase();
            
            if(StringUtils.isNotBlank(sample.getOrderId())) {
            	return buffer.append(labCode).append(sample.getOrderId()).toString();
            }
            Date date = new Date();

            String prefix = buffer.append(labCode).append(new SimpleDateFormat("YYYYMMdd").format(date)).toString();

            String labNumber = "";
            String lastLabNumber = repository.getLastLabNumber(lab.getId());

            if (lastLabNumber == null) {
                labNumber = prefix + "-0001";
            } else {
                // Split between lab-date identifier and counter by '-'
                String[] parts = lastLabNumber.split("-");
                String labDate = parts[0],counter = parts[1];
                if (labDate.equalsIgnoreCase(prefix)) {

                    int _ct = Integer.parseInt(counter);
                    _ct = _ct+1;

                    String labNum = StringUtils.leftPad(String.valueOf(_ct),4 ,"0");

                    buffer = new StringBuffer();
                    labNumber = buffer.append(prefix).append("-").append(labNum).toString();

                } else {
                    buffer = new StringBuffer();
                    labNumber = buffer.append(prefix).append("-0001").toString();
                }
            }
            return labNumber;
        } else
            return "no lab number registered";
    }

    public Long deleteSample(Long id) {
        return repository.delete(id);

    }

    public void syncHubSamples(List<SampleFromHubDTO> samples){
        if(null!=samples && !samples.isEmpty()){
            SimpleDateFormat sdf,sdf2;
            for(SampleFromHubDTO sample: samples){
                SampleFromHubDTO _local = repository.getByOrderId(sample.getOrderID());
                if(_local == null){
                    sample.setIsReceivedAtLAB(true);

                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                    sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    String dateSampleTakenStr = sdf.format(sample.getDateSampleTaken());
                    try {
                        sample.setDateSampleTaken(sdf2.parse(dateSampleTakenStr + " " + sample.getTimeSampleTaken()));
                    }catch(ParseException e){
                        logger.debug("Failed to parse date {}",e);
                    }
                    //sample.setPatientCenter(sample.getHfrID());

                    repository.insertHubSample(sample);
                    logger.info("Saved [Sample {} {} {} {}]",
                            sample.getBatchCode(),
                            sample.getOrderID(),
                            sample.getPatientCenter(),
                            sample.getTestCenter(),
                            sample.getHfrID());
                }else{
//                    notificationService.sendNotification(_local);
                    notificationService.sendNotification(_local.getOrderID(), "2", null);
                }
            }
        }
        saveHubSamples();
    }

    public void updateSendNotificationStatus(String orderId,String status){
        repository.updateSendNotificationStatus(orderId,status);
    }

    private void saveHubSamples(){
        List<SampleFromHubDTO> hubSamples = repository.getSampleFromHubList();

        if(!hubSamples.isEmpty()){

            for(SampleFromHubDTO hubSample: hubSamples) {
                try {
                    if (hubSample.getSampleType().equalsIgnoreCase("EID"))
                        continue;
                    Sample sample = new Sample();
                    Facility testingFacility = null, patientFacility = null;

                    try {
                        testingFacility = facilityService.getFacilityByCode(hubSample.getTestCenter());
                    } catch (DataException e) {
                        logger.info("Failed to get test facility {} for sample {}: Sample was not saved",
                                hubSample.getTestCenter(),
                                hubSample.getOrderID());
                        continue;
                    }

                    try {
                        patientFacility = facilityService.getByHFRId(hubSample.getHfrID());
                    } catch (DataException e) {
                        logger.info("Failed to get patient's facility {} for sample {}: Sample was not saved", // email if need be
                                hubSample.getPatientCenter(),
                                hubSample.getOrderID());
                        continue;
                    }

                    HIVTestType testType = testTypeService.getById(hubSample.getFormType().equalsIgnoreCase("HIVVL") ? 1l : 2l);
                    String adheranceStr = hubSample.getDrugAdherance();

                    String drugAdherenceCode = "FAIR";

                    if (adheranceStr.equalsIgnoreCase("Good")) {
                        drugAdherenceCode = "GOOD";
                    } else if (adheranceStr.equalsIgnoreCase("fair")) {
                        drugAdherenceCode = "FAIR";
                    } else if (adheranceStr.equalsIgnoreCase("bad") || adheranceStr.equalsIgnoreCase("poor")) {
                        drugAdherenceCode = "POOR";
                    }

                    DrugAdherence drugAdherence = adherenceService.getByCode(drugAdherenceCode);
                    Patient existing = null;

                    String patientID = hubSample.getPatientID();
                    if (patientID.contains("-")) {
                        patientID = patientID.replaceAll("-", "");
                    }

                    existing = repository.getByPatientId(patientID);

                    if (existing != null) {
                        sample.setPatientAutoId(existing.getId());
                    } else {
                        Patient patient = new Patient();
                        patient.setPatientId(patientID);

                        try {
                            patient.setDob(
                                    new SimpleDateFormat("yyyy-MM-dd").
                                            parse(hubSample.getBirthDate())
                            );
                        } catch (ParseException pe) {
                            logger.debug("Error setting dob", pe);
                        }
                        patient.setGender(hubSample.getGender());
                        repository.insertPatient(patient);
                        sample.setPatientAutoId(patient.getId());
                    }

                    sample.setLabId(testingFacility.getId());
                    sample.setTestTypeId(testType.getId());
                    sample.setFacilityId(patientFacility.getId());
                    sample.setGeographicZoneId(patientFacility.getGeographicZone().getId());
                    sample.setNameOfCellLeader(hubSample.getLeader());
                    sample.setPatientPhoneNumber(hubSample.getPatientPhone());
                    if (hubSample.getGender().equalsIgnoreCase("Female")) {
                        if (hubSample.getIsPregnant() != null) {
                            sample.setPatientIsPregnant(hubSample.getIsPregnant().equalsIgnoreCase("NO") ? false : true);
                        } else {
                            sample.setPatientIsPregnant(false);
                        }

                        if (hubSample.getIsBreastFeeding() != null) {
                            sample.setPatientIsBreastFeeding(hubSample.getIsBreastFeeding().equalsIgnoreCase("NO") ? false : true);
                        } else {
                            sample.setPatientIsBreastFeeding(false);
                        }
                    } else {
                        sample.setPatientIsBreastFeeding(false);
                        sample.setPatientIsPregnant(false);
                    }

                    if (hubSample.getHasTB() != null) {
                        sample.setPatientHasTb(hubSample.getHasTB().equalsIgnoreCase("YES") ? true : false);
                        if (hubSample.getIsOnTBTreatment() != null) {
                            sample.setPatientUnderTbTreatment(
                                    hubSample.getIsOnTBTreatment().equalsIgnoreCase("NO") ? false : true);
                        } else {
                            sample.setPatientUnderTbTreatment(false);
                        }
                    } else {
                        sample.setPatientHasTb(false);
                        sample.setPatientUnderTbTreatment(false);
                    }

                    sample.setSampleBarcodeNumber(hubSample.getBatchCode());

                    String code = "";

                    if (hubSample.getSampleType().equalsIgnoreCase("plasma")) {
                        code = "PSM";
                    } else if (hubSample.getSampleType().equalsIgnoreCase("wholeblood")) {
                        code = "WB";
                    } else if (hubSample.getSampleType().equalsIgnoreCase("dbs")) {
                        code = "DBS";
                    }
                    SampleType sampleType1 = typeService.getByCode(code);
                    if (sampleType1 != null)
                        sample.setSampleTypeId(sampleType1.getId());

                    int drugType = Integer.parseInt(hubSample.getDrugType());
                    Regimen regimen = regimenService.getByNameAndType(hubSample.getDrugName(), drugType);

                    if (regimen != null)
                        sample.setPatientRegimenId(regimen.getId());

                    sample.setSampleRequestDate(hubSample.getDateSampleTaken());

                    String srsReason = hubSample.getTestReason();
                    String reason =
                            srsReason.equalsIgnoreCase("firsttest") ? "FIRST" :
                                    srsReason.equalsIgnoreCase("secondtest") ? "REPEAT" : "FAILURE";
                    VLTestingReason testingReason = testingReasonService.getByCode(reason);
                    sample.setTestReasonId(testingReason.getId());

                    sample.setReasonForRepeatTest("");// TO-DO
                    sample.setOtherRegimen(""); // We should put it regimens option for other, then below field should be filled
                    sample.setDrugAdherenceId(drugAdherence.getId());
                    sample.setNameOfPersonRequestingTest(hubSample.getSampleRequesterName());
                    sample.setCadreOfPersonRequestingTest(hubSample.getPhlebotomistCadre());

                    sample.setDateOfSampleCollection(hubSample.getDateSampleTaken());
                    sample.setNameOfPhlebotomist(hubSample.getPhlebotomistName());
                    sample.setDateOfSampleDispatch(hubSample.getDateSampleSent());
                    sample.setNameOfSampleDispatcher(hubSample.getDispatcherName());
                    sample.setLabNumber(null);
                    sample.setDateReceived(null);//hubSample.getDateReceivedAtLab());
                    sample.setTestDate(null);
                    sample.setTestedBy(null);
                    sample.setVerifiedBy(null);
                    sample.setResultDate(null);
                    sample.setModifiedBy(1L);
                    sample.setCreatedBy(1L);
                    sample.setIsPending(true);
                    sample.setStatus(registered);
                    sample.setBatchNumber(hubSample.getBatchCode());
                    sample.setHubId(null);
                    sample.setOrderId(hubSample.getOrderID());
                    repository.insertSample(sample);

                    SampleStatusChange change = new SampleStatusChange(sample, SampleStatus.REGISTERED, 1L);
                    statusChangeService.insert(change);
                    logger.info("Saved [Order ID: {}]", hubSample.getOrderID());

                    repository.updateIsSaved(hubSample.getOrderID());

//                 notificationService.sendNotification(hubSample);
                    notificationService.sendNotification(sample.getOrderId(), "2", null);
                } catch (Exception e) {
                    logger.debug("Failed to register sample {}|Error {}",hubSample.getOrderID(), e.getMessage());
                }
            }
        }
    }

  //  @Transactional
    public void saveSampleFromHub(List<SampleFromHubDTO> sampleDTO) {

        SampleFromHubDTO hubDTO = new SampleFromHubDTO();
        if (sampleDTO != null) {
            for (SampleFromHubDTO sample : sampleDTO) {

                hubDTO.setBatchCode(sample.getBatchCode());
                hubDTO.setBirthDate(sample.getBirthDate());
                hubDTO.setCreatedAt(sample.getCreatedAt());
                hubDTO.setDateReceivedLab(sample.getDateReceivedLab());
                hubDTO.setDateSampleSent(sample.getDateSampleSent());
                hubDTO.setDateSampleTaken(sample.getDateSampleTaken());
                hubDTO.setDiseaseName(sample.getDiseaseName());
                hubDTO.setDispatcherName(sample.getDispatcherName());
                hubDTO.setDrugAdherance(sample.getDrugAdherance());
                hubDTO.setDrugName(sample.getDrugName());
                hubDTO.setDrugType(sample.getDrugType());
                hubDTO.setGender(sample.getGender());
                hubDTO.setFormType(sample.getFormType());
                hubDTO.setHubCenter(sample.getHubCenter());
                hubDTO.setIsBreastFeeding(sample.getIsBreastFeeding());
                hubDTO.setOrderID(sample.getOrderID());
                hubDTO.setTestCode(sample.getTestCode());
                hubDTO.setPatientCenter(sample.getPatientCenter());
                hubDTO.setTestReason(sample.getTestReason());
                hubDTO.setSampleRequestTime(sample.getSampleRequestTime());
                hubDTO.setSampleRequesterName(sample.getSampleRequesterName());
                hubDTO.setPhlebotomistName(sample.getPhlebotomistName());
                hubDTO.setDispatcherName(sample.getDispatcherName());
                hubDTO.setOrderStatus(sample.getOrderStatus());
                hubDTO.setUpdatedAt(sample.getUpdatedAt());
                hubDTO.setPatientCode(sample.getPatientCode());
                hubDTO.setTestCenter(sample.getTestCenter());
                hubDTO.setTestComment(sample.getTestComment());
                hubDTO.setOrderedAt(sample.getOrderedAt());
                hubDTO.setResultReceivedAt(sample.getResultReceivedAt());
                hubDTO.setRegisterTurnRound(sample.getRegisterTurnRound());
                hubDTO.setResultTurnRound(sample.getResultTurnRound());
                hubDTO.setTransitStatus(sample.getTransitStatus());
                hubDTO.setOnTransitAt(sample.getOnTransitAt());
                hubDTO.setReceivedLabAt(sample.getReceivedLabAt());
                hubDTO.setRegisteredBy(sample.getRegisteredBy());
                hubDTO.setOriginalLab(sample.getOriginalLab());
                hubDTO.setUserGroup(sample.getUserGroup());
                hubDTO.setGender(sample.getGender());
                hubDTO.setPcreatedDate(sample.getPcreatedDate());
                hubDTO.setIsPregnant(sample.getIsPregnant());
                hubDTO.setPatientID(sample.getPatientID());
                hubDTO.setPID(sample.getPID());
                hubDTO.setPatientPhone(sample.getPatientPhone());
                hubDTO.setWardId(sample.getWardId());
                hubDTO.setPatientNursePhone(sample.getPatientNursePhone());
                hubDTO.setIsReceivedAtLAB(true);
                hubDTO.setSampleType(sample.getSampleType());

                SampleFromHubDTO items2 = repository.getByOrderId(hubDTO.getOrderID());
                if (items2 == null) {

                    repository.insertHubSample(hubDTO);
                    logger.info("saving {}",hubDTO);
                }

            }

        }

        List<SampleFromHubDTO>sampleFromHubDTOs = repository.getSampleFromHubList();
        if(!sampleFromHubDTOs.isEmpty()){

            for(SampleFromHubDTO sample:sampleFromHubDTOs){

                Sample sa = new Sample();
                Facility facility =null,facility3=null;
                try {
                    facility = facilityService.getFacilityByCode(sample.getTestCenter());
                }catch(DataException e){
                    logger.info("Failed to get facility {}",sample.getTestCenter());
                    continue;
                }
                logger.info("Testing Center: {} Facility code: {}", sample.getTestCenter(),sample.getPatientCenter());
                try {
                    facility3 = facilityService.getFacilityByUbunifuId(sample.getPatientCenter());
                }catch(DataException e){
                    logger.info("Failed to get facility {}", sample.getPatientCenter());
                    continue;
                }
                logger.info("Patient Facility: {}", facility3.getCode());
                HIVTestType testType = testTypeService.getById(1L);
                Patient existing = null;
                if(sample.getPatientID().contains("-")) {
                    String[]tokens = sample.getPatientID().split("-");
                     existing = repository.getByPatientId(tokens[2]);
                }else
                existing=repository.getByPatientId(sample.getPatientID().substring(sample.getPatientID().length() - 6));

                DrugAdherence drugAdherence= adherenceService.getByName(sample.getDrugAdherance());


                if (existing != null) {
                    sa.setPatientAutoId(existing.getId());
                } else {

                    Patient p =new Patient();
                    System.out.println("PatentID");

                   // p.setPatientId(sample.getPatientID());
                    if(sample.getPatientID().contains("-")){
                        String[]tokens = sample.getPatientID().split("-");
                        System.out.println(tokens[0]);
                        System.out.println(tokens[1]);
                        p.setPatientId(tokens[2]);
                    }else{
                        p.setPatientId(sample.getPatientID().substring(sample.getPatientID().length() - 6));
                        System.out.println(p);
                    }


                 /*   if (sample.getPatientID().length() == 5) {
                        p.setPatientId(sample.getPatientID());
                    } else if (sample.getPatientID().length() > 5) {
                        p.setPatientId(sample.getPatientID().substring(sample.getPatientID().length() - 5));
                    } else {
                        // whatever is appropriate in this case
                        throw new IllegalArgumentException("word has less than 3 characters!");
                    }*/
                    System.out.println("Date 1");

                    String startDateString = sample.getBirthDate();
                    System.out.println("Date dsnnamamam");
                    System.out.println(startDateString);

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date startDate;
                    try {
                       // Date d = new Date(startDateString);
                        System.out.println("Date dsnnamamam");

                        startDate = df.parse(startDateString);
                        p.setDob(startDate);
                        //System.out.println(d);


                        System.out.println(p);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    p.setGender(sample.getGender());
                    repository.insertPatient(p);
                    sa.setPatientAutoId(p.getId());
                }

                // send notification after we've have saved
                //sendNotification(sample);

                System.out.println(sa);

                sa.setLabId(facility.getId());

                sa.setTestTypeId(testType.getId());

                sa.setFacilityId(facility3.getId());
                sa.setGeographicZoneId(facility3.getGeographicZone().getId());
                sa.setNameOfCellLeader(sample.getLeader());
                sa.setPatientPhoneNumber(sample.getPatientPhone());
               // if(Objects.equals(sample.getIsPregnancy().toLowerCase(), "no".toLowerCase())){
                    sa.setPatientIsPregnant(false);
              //  }else
                   // sa.setPatientIsPregnant(true);


               // if(Objects.equals(sample.getIsBreastFeeding().toLowerCase(), "No".toLowerCase())){
                    sa.setPatientIsBreastFeeding(false);
               // }else
                 //   sa.setPatientIsBreastFeeding(true);
                sa.setPatientHasTb(false);
                sa.setPatientUnderTbTreatment(false);
                sa.setSampleBarcodeNumber(sample.getBatchCode());

                SampleType sampleType1 = typeService.getSampleTypeByName(sample.getSampleType());
                if(sampleType1 !=null)
                    sa.setSampleTypeId(sampleType1.getId());
                Regimen regimen = regimenService.getByName(sample.getDrugName());
                if(regimen!=null)
                    sa.setPatientRegimenId(regimen.getId());
                sa.setSampleRequestDate(sample.getDateSampleTaken());
                sa.setTestReasonId(1L);
                sa.setReasonForRepeatTest(sample.getTestReason());
                sa.setOtherRegimen(sample.getDrugName());
                sa.setDrugAdherenceId(1L);
                sa.setNameOfPersonRequestingTest(sample.getSampleRequesterName());
                sa.setCadreOfPersonRequestingTest(sample.getPhlebotomistName());
                sa.setDateOfSampleCollection(sample.getDateSampleTaken());
                sa.setNameOfPhlebotomist(sample.getPhlebotomistName());
                sa.setDateOfSampleDispatch(sample.getDateSampleSent());
                sa.setNameOfSampleDispatcher(sample.getDispatcherName());
                sa.setLabNumber(null);
                sa.setDateReceived(sample.getDateReceivedAtLab());
                sa.setTestDate(new Date());
                sa.setTestedBy(null);
                sa.setVerifiedBy(null);
                sa.setResultDate(new Date());
                sa.setModifiedBy(1L);
                sa.setCreatedBy(1L);
                sa.setIsPending(true);
                sa.setStatus(registered);
                sa.setBatchNumber(sample.getBatchCode());
                sa.setHubId(null);
                sa.setOrderId(sample.getOrderID());
                repository.insertSample(sa);
                SampleStatusChange change = new SampleStatusChange(sa, SampleStatus.REGISTERED, 1L);
                statusChangeService.insert(change);
                System.out.println("Reached to this end");
                repository.updateIsSaved(sample.getOrderID());

                System.out.println(sa);
                //repository.insertSample(sa);

            }

        }

    }
    public String encode(String stringToBeEncoded){
        String end="";
        try {
            end= URLEncoder.encode(stringToBeEncoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return end;

    }

    public List<SampleFromHubDTO> getAllByStatus() {
        return repository.getByStatus();

    }
    public List<HashMap>getAllFrom(){
        return repository.getAllFrom();
    }

    public String updateStatusFromHub(String orderId){
        return repository.updateStatusFromHub(orderId);
    }
}

