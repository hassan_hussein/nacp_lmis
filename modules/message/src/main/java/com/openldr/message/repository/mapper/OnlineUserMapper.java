package com.openldr.message.repository.mapper;

import com.openldr.message.domain.ConnectedAgent;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chrispinus on 7/29/16.
 */
@Repository
public interface OnlineUserMapper {

    @Insert("INSERT INTO online_users (userid,name,status) VALUES(#{userId} ,#{name},#{status})")
    @Options(useGeneratedKeys = true)
    public void saveConnectedUser(ConnectedAgent agent);

    @Update("UPDATE online_users set status=#{status} where userid=#{userId}")
    public void updateUserStatus(ConnectedAgent agent);

    @Select("Select * from online_users where userid=#{userId} LIMIT 1")
    public ConnectedAgent getByUserId(@Param("userId") Long userId);


    @Select("Select u.id as id, u.firstname as firstName, u.lastname as lastName, u.jobtitle as jobTitle, o.status as status " +
            " from online_users o " +
            " join users u on o.userid=u.id")
    List<ConnectedAgent> getAll();
}
