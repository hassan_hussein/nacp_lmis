package com.openldr.message.service;


import com.openldr.message.domain.ConnectedAgent;
import com.openldr.message.domain.OnlineUser;
import com.openldr.message.repository.OnlineUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OnlineUserService {

    @Autowired
    OnlineUserRepository repository;

 /*   @Autowired
    SimpMessagingTemplate messagingTemplate;*/

    public void saveConnectedUser(ConnectedAgent agent) {
        ConnectedAgent exist = repository.getByUserId(agent.getUserId());
        if (exist == null) {
            repository.saveConnectedUser(agent);
        } else {
            repository.updateUserStatus(agent);
        }

    }

    public List<ConnectedAgent> getOnlineUsers() {
        return repository.getAll();

    }

    public void updateLogoutUser(Long userId, String status) {
        System.out.println(userId);
        ConnectedAgent agent = new ConnectedAgent();
        // User user=userService.getByUserName(name);
        agent.setUserId(userId);
        agent.setStatus(status);
        repository.updateUserStatus(agent);
      //  messagingTemplate.convertAndSend("/topic/online", new OnlineUser(getOnlineUsers()));
    }
}
