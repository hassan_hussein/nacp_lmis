package com.openldr.message.controller;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.message.domain.ConnectedAgent;
import com.openldr.message.domain.ConnectedClient;
import com.openldr.message.domain.OnlineUser;
import com.openldr.message.service.OnlineUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MessageController {


   /* @Autowired
    SimpMessagingTemplate messagingTemplate;*/

    @Autowired
    OnlineUserService service;

    @MessageMapping("/ws")
    @SendTo("/topic/connected-client")
    public ConnectedClient connectedClient(ConnectedAgent agent) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new ConnectedClient(agent.getName(), "Default", "Default");
    }

    @MessageMapping("/wsForward")
    @SendTo("/topic/forward")
    public String forward() throws Exception {
        return "NEW_QUEUE";
    }

    @MessageMapping("/wsOnline")
    @SendTo("/topic/online")
    public OnlineUser onlineUser(ConnectedAgent agent) throws Exception {
        service.saveConnectedUser(agent);
        return new OnlineUser(service.getOnlineUsers());
    }

    @RequestMapping(value = "/message/logout/{userId}/{status}", method = RequestMethod.PUT)
    public ResponseEntity<OpenLdrResponse> logout(@PathVariable Long userId, @PathVariable String status) {
        service.updateLogoutUser(userId, status);
        return OpenLdrResponse.response("ok", "ok");
    }

}
