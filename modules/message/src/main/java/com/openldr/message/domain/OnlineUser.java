package com.openldr.message.domain;

import com.openldr.core.domain.BaseModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class OnlineUser extends BaseModel {

    private List<ConnectedAgent> agents;

    private ConnectedAgent agent;

    private String status;

    public OnlineUser(List<ConnectedAgent> connectedAgents) {
        this.agents = connectedAgents;
    }

    public OnlineUser(String status) {
        this.status = status;
    }

    public OnlineUser(ConnectedAgent agent) {
        this.agent = agent;
    }
}
