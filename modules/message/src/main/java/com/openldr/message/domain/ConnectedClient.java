package com.openldr.message.domain;

import lombok.Data;

@Data
public class ConnectedClient {

    private String bridgeStatus;

    private String callerNumber;

    private String extensionNumber;

    public ConnectedClient(String bridgeStatus, String callerNumber, String extensionNumber) {
        this.bridgeStatus = bridgeStatus;
        this.callerNumber = callerNumber;
        this.extensionNumber = extensionNumber;
    }

}
